using System;
using System.Data;
using System.Text;
using RMYH.DBUtility;
using RMYH.Model;
using Sybase.Data.AseClient;
namespace RMYH.DAL
{
	/// <summary>
	/// 数据访问类:TB_SYSMENU
	/// </summary>
	public partial class TB_SYSMENUDAL
	{
		public TB_SYSMENUDAL()
		{}
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperOra.GetMaxID("ID", "TB_SYSMENU");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from TB_SYSMENU");
            strSql.Append(" where ID=:ID ");
            AseParameter[] parameters = {
                    new AseParameter(":ID",AseDbType.Integer,4)};
            parameters[0].Value = ID;

            return DbHelperOra.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(RMYH.Model.TB_SYSMENUModel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into TB_SYSMENU(");
            strSql.Append("ID,PARID,PAGEPATH,TITLE,SEQUENCE)");
            strSql.Append(" values (");
            strSql.Append(":ID,:PARID,:PAGEPATH,:TITLE,:SEQUENCE)");
            AseParameter[] parameters = {
                    new AseParameter(":ID", AseDbType.Integer,4),
                    new AseParameter(":PARID", AseDbType.Integer,4),
                    new AseParameter(":PAGEPATH", AseDbType.VarChar,200),
                    new AseParameter(":TITLE", AseDbType.VarChar,100),
                    new AseParameter(":SEQUENCE", AseDbType.Integer,4)};
            parameters[0].Value = model.ID;
            parameters[1].Value = model.PARID;
            parameters[2].Value = model.PAGEPATH;
            parameters[3].Value = model.TITLE;
            parameters[4].Value = model.SEQUENCE;

            DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TB_SYSMENUModel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update TB_SYSMENU set ");
            strSql.Append("PARID=:PARID,");
            strSql.Append("PAGEPATH=:PAGEPATH,");
            strSql.Append("TITLE=:TITLE,");
            strSql.Append("SEQUENCE=:SEQUENCE");
            strSql.Append(" where ID=:ID ");
            AseParameter[] parameters = {
                    new AseParameter(":PARID",AseDbType.Integer,4),
                    new AseParameter(":PAGEPATH",AseDbType.VarChar,200),
                    new AseParameter(":TITLE", AseDbType.VarChar,100),
                    new AseParameter(":SEQUENCE",AseDbType.Integer,4),
                    new AseParameter(":ID",AseDbType.Integer,4)};
            parameters[0].Value = model.PARID;
            parameters[1].Value = model.PAGEPATH;
            parameters[2].Value = model.TITLE;
            parameters[3].Value = model.SEQUENCE;
            parameters[4].Value = model.ID;

            int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TB_SYSMENU ");
            strSql.Append(" where ID=:ID ");
            AseParameter[] parameters = {
                    new AseParameter(":ID",AseDbType.Integer,4)};
            parameters[0].Value = ID;

            int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TB_SYSMENU ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperOra.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TB_SYSMENUModel GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,PARID,PAGEPATH,TITLE,SEQUENCE from TB_SYSMENU ");
            strSql.Append(" where ID=:ID ");
            AseParameter[] parameters = {
                    new AseParameter(":ID",AseDbType.Integer,4)};
            parameters[0].Value = ID;

            TB_SYSMENUModel model = new TB_SYSMENUModel();
            DataSet ds = DbHelperOra.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PARID"].ToString() != "")
                {
                    model.PARID = int.Parse(ds.Tables[0].Rows[0]["PARID"].ToString());
                }
                model.PAGEPATH = ds.Tables[0].Rows[0]["PAGEPATH"].ToString();
                model.TITLE = ds.Tables[0].Rows[0]["TITLE"].ToString();
                if (ds.Tables[0].Rows[0]["SEQUENCE"].ToString() != "")
                {
                    model.SEQUENCE = int.Parse(ds.Tables[0].Rows[0]["SEQUENCE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,PARID,PAGEPATH,TITLE,SEQUENCE,NBBM ");
            strSql.Append(" FROM TB_SYSMENU ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperOra.Query(strSql.ToString());
        }
        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OracleParameter[] parameters = {
                    new OracleParameter(":tblName", OracleType.VarChar, 255),
                    new OracleParameter(":fldName", OracleType.VarChar, 255),
                    new OracleParameter(":PageSize", OracleType.Number),
                    new OracleParameter(":PageIndex", OracleType.Number),
                    new OracleParameter(":IsReCount", OracleType.Clob),
                    new OracleParameter(":OrderType", OracleType.Clob),
                    new OracleParameter(":strWhere", OracleType.VarChar,1000),
                    };
            parameters[0].Value = "TB_SYSMENU";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperOra.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

		#endregion  Method
	}
}

