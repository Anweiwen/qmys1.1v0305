﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RMYH.DBUtility;
using System.Data;
using System.Collections;
using Sybase.Data.AseClient;

namespace RMYH.DAL.CDQX
{
    public partial class BBFXXT_BBCDQXDAL
    {
        public BBFXXT_BBCDQXDAL()
        {
 
        }
        #region Method
        /// <summary>
        /// 判断当前用户是否为用户组
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <returns></returns>
        public bool IsUSEROrGroup(string USERDM)
        {
            bool Flag = true;
            string sql = "SELECT * FROM TB_GROUP WHERE GROUP_ID='" + USERDM + "'";
            //判断被选择用户是用户组还是用户
            if (DbHelperOra.Query(sql).Tables[0].Rows.Count <= 0)
            {
                Flag = false;
            }
            return Flag;

        }
        /// <summary>
        /// 根据用户组代码获取旗下的用户
        /// </summary>
        /// <param name="GROUP_ID">用户组代码</param>
        /// <returns></returns>
        public DataSet GetUSERDMByGroupDM(string GROUP_ID)
        {
            DataSet DS = new DataSet();
            if (GROUP_ID != "")
            {
                string SQL = "SELECT USER_ID FROM TB_USERGROUP WHERE GROUP_ID='"+GROUP_ID+"'";
                DS=DbHelperOra.Query(SQL);
            }
            return DS;
        }
        /// <summary>
        /// 获取当前用户或者用户组的报表权限
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="Flag">标识（true代表用户组；false代表用户）</param>
        /// <param name="BZ">标识（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public DataSet GetBBBMByUSERDM(string USERDM,bool Flag,string BZ)
        {
            string sql = "";
            if (Flag == true)
            {
                sql = "SELECT BBBM FROM TB_BBGROUPQX WHERE GROUP_ID='" + USERDM + "' AND BZ='"+BZ+"'";
            }
            else
            {
                sql = "SELECT BBBM FROM TB_BBUSERQX WHERE USER_ID='" + USERDM + "'AND BZ='"+BZ+"'";
            }
            return DbHelperOra.Query(sql);
        }
        /// <summary>
        /// 判断用户是否有此报表菜单的权限
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="BBBM">报表代码</param>
        /// <param name="F">标识（true代表用户组；false代表用户）</param>
        /// <param name="BZ">标识报表或者菜单（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public bool IsHavingBBByUSERDM(string USERDM,string BBBM,bool F,string BZ)
        {
            string sql="";
            bool Flag = false;
            if (F == true)
            {
                sql = "SELECT * FROM TB_BBGROUPQX WHERE GROUP_ID='" + USERDM + "' AND BBBM='" + BBBM + "' AND BZ='"+BZ+"'";
            }
            else
            {
                sql = "SELECT * FROM TB_BBUSERQX WHERE USER_ID='" + USERDM + "' AND BBBM='" + BBBM + "' AND BZ='"+BZ+"'";
            }
            if (DbHelperOra.Query(sql).Tables[0].Rows.Count > 0)
            {
                Flag = true;
            }
            return Flag;

        }
        /// <summary>
        /// 将权限数据集赋给相应的用户或者用户组
        /// </summary>
        /// <param name="USERDM">用户或者用户组</param>
        /// <param name="DS">权限数据集</param>
        /// <param name="Flag">是否为用户组（true代表是用户组；false代表是用户）</param>
        /// <param name="BZ">标识报表或者菜单（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public int InSertBBQX(string USERDM,DataSet DS,bool Flag,string BZ)
        {
            int Rtn = 0;
            ArrayList List = new ArrayList();
            List<String> ArrList = new List<String>();
            //获取权限数据的sql语句
            GetSqlCDQX(DS, ref ArrList, ref List, USERDM, Flag,BZ);
            Rtn=DbHelperOra.ExecuteSql(ArrList.ToArray(), List);
            return Rtn;
        }
        /// <summary>
        /// 获取需插入权限表的sql语句集合
        /// </summary>
        /// <param name="DS">权限数据集</param>
        /// <param name="ArrList">sql语句数据集</param>
        /// <param name="List">参数集合</param>
        /// <param name="USERDM">用户代码</param>
        /// <param name="Flag">是否为用户组（true代表是用户组；false代表是用户）</param>
        public void GetSqlCDQX(DataSet DS, ref List<String> ArrList, ref ArrayList List, string USERDM,bool Flag,string BZ)
        {
            //用于删除用户组以及旗下的用户权限的变量
            ArrayList Lst = new ArrayList();
            List<String> Arr = new List<String>();
            //定义用户组下的用户代码字段、和删除用户权限的sql语句字段
            string ID = "", USSql = "";
            //当前赋权限为用户组
            if (Flag == true)
            {
                DataSet DT = new DataSet();
                DT = GetUSERDMByGroupDM(USERDM);
                //删除用户组下所有赋予其旗下的用户的权限
                for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                {
                    if (ID == "")
                    {
                        ID += DT.Tables[0].Rows[j]["USER_ID"].ToString();
                    }
                    else
                    {
                        ID = ID + "','" + DT.Tables[0].Rows[j]["USER_ID"].ToString();
                    }
                }
                if (ID != "")
                {
                    ID = "'" + ID + "'";
                    USSql = "DELETE FROM TB_BBUSERQX WHERE USER_ID IN("+ID+") AND BBBM IN(SELECT BBBM FROM TB_BBGROUPQX WHERE GROUP_ID=@GROUP_ID AND BZ=@BZ) AND BZ=@BZ";
                    Arr.Add(USSql);
                    AseParameter[] DelUS = {
                            new AseParameter("@GROUP_ID",AseDbType.VarChar,40),
                            new AseParameter("@BZ",AseDbType.VarChar,2)
                            };
                    DelUS[0].Value = USERDM;
                    DelUS[1].Value = BZ;
                    Lst.Add(DelUS);
                }
                //删除之前用户组的权限
                string DelGSql = "DELETE FROM TB_BBGROUPQX WHERE GROUP_ID=@GROUP_ID AND BZ=@BZ";
                Arr.Add(DelGSql);
                AseParameter[] DelGr = {
                    new AseParameter("@GROUP_ID",AseDbType.VarChar,40),
                    new AseParameter("@BZ",AseDbType.VarChar,2)
                    };
                DelGr[0].Value = USERDM;
                DelGr[1].Value = BZ;
                Lst.Add(DelGr);
                int Len=DbHelperOra.ExecuteSql(Arr.ToArray(),Lst);
                //赋予用户组新的权限
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string BBBM = DS.Tables[0].Rows[i]["BBBM"].ToString();
                    string GSql = "INSERT INTO TB_BBGROUPQX(GROUP_ID,BBBM,BZ) VALUES(@GROUP_ID,@BBBM,@BZ)";
                    ArrList.Add(GSql);
                    AseParameter[] Gr = {
                    new AseParameter("@GROUP_ID",AseDbType.VarChar,40),
                    new AseParameter("@BBBM",AseDbType.VarChar,40),
                    new AseParameter("@BZ",AseDbType.VarChar,2)
                    };
                    Gr[0].Value = USERDM;
                    Gr[1].Value = BBBM;
                    Gr[2].Value = BZ;
                    List.Add(Gr);
                    //将用户组的权限插入到用户的权限表中
                    for(int k=0;k<DT.Tables[0].Rows.Count;k++)
                    {
                        string USER_ID=DT.Tables[0].Rows[k]["USER_ID"].ToString().Trim();
                        //判断此用户有没有此报表权限
                        if (IsHavingBBByUSERDM(USER_ID, BBBM,false,BZ) == false)
                        {
                            string USql = "INSERT INTO TB_BBUSERQX(USER_ID,BBBM,BZ) VALUES(@USER_ID,@BBBM,@BZ)";
                            ArrList.Add(USql);
                            AseParameter[] US = {
                            new AseParameter("@USER_ID",AseDbType.VarChar,40),
                            new AseParameter("@BBBM",AseDbType.VarChar,40),
                            new AseParameter("@BZ",AseDbType.VarChar,2)
                            };
                            US[0].Value = USER_ID;
                            US[1].Value = BBBM;
                            US[2].Value = BZ;
                            List.Add(US);
                        }
                    }
                }
            }
            else
            {
                //删除用户之前的报表权限
                USSql = "DELETE FROM TB_BBUSERQX WHERE USER_ID=@USERDM AND BZ=@BZ";
                ArrList.Add(USSql);
                AseParameter[] DelUS = {
                    new AseParameter("@USERDM",AseDbType.VarChar,40),
                    new AseParameter("@BZ",AseDbType.VarChar,2)
                    };
                DelUS[0].Value = USERDM;
                DelUS[1].Value = BZ;
                List.Add(DelUS);
                //赋予新的权限
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string BBBM = DS.Tables[0].Rows[i]["BBBM"].ToString();
                    string USql = "INSERT INTO TB_BBUSERQX(USER_ID,BBBM,BZ) VALUES(@USER_ID,@BBBM,@BZ)";
                    ArrList.Add(USql);
                    AseParameter[] US = {
                    new AseParameter("@USER_ID",AseDbType.VarChar,40),
                    new AseParameter("@BBBM",AseDbType.VarChar,40),
                    new AseParameter("@BZ",AseDbType.VarChar,2)
                    };
                    US[0].Value = USERDM;
                    US[1].Value = BBBM;
                    US[2].Value = BZ;
                    List.Add(US);
                }
            }
        }
        /// <summary>
        /// 向TB_BBBZXMBM中插入数据
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMFL">项目分类代码</param>
        /// <param name="XMBZBM">项目标准编码</param>
        /// <param name="CCJB">层次界别</param>
        /// <param name="YJDBZ">叶子节点标识（1代表是，0代表不是叶子节点）</param>
        /// <param name="FBDM">父节点代码</param>
        /// <returns></returns>
        public int InsertXMFLMX(string ID,string XMFL,string XMBZBM,int CCJB,string YJDBZ,string FBDM,string xmmc)
        {
            string sql = "INSERT INTO TB_BBBZXMBM(ID,XMFL,XMBZBM,CCJB,YJDBZ,FBDM,XMMC,MCBM) VALUES('" + ID + "','" + XMFL + "','" + XMBZBM + "'," + CCJB + ",'" + YJDBZ + "','" + FBDM + "','" + xmmc + "','" + xmmc + "')";
            return DbHelperOra.ExecuteSql(sql);
        }
        /// <summary>
        /// 根据主键ID获取CCJB字段值
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public int GetCCJBByID(string ID)
        {
            int CCJB = -1;
            DataSet DS = new DataSet();
            if (ID != "")
            {
                string sql = "SELECT CCJB FROM TB_BBBZXMBM WHERE ID='"+ID+"'";
                DS=DbHelperOra.Query(sql);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    CCJB = int.Parse(DS.Tables[0].Rows[0]["CCJB"].ToString())+1;
                }
            }
            return CCJB;
        }
        /// <summary>
        /// 判断当前的项目标准编码是否存在
        /// </summary>
        /// <param name="XMBZBM">项目标准编码</param>
        /// <returns></returns>
        public bool IsHavingXMBZBM(string XMBZBM)
        {
            bool Flag = false;
            DataSet DS = new DataSet();
            string sql = "SELECT * FROM TB_BBBZXMBM WHERE XMBZBM='"+XMBZBM+"'";
            DS = DbHelperOra.Query(sql);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Flag = true;
            }
            return Flag;
        }
        /// <summary>
        /// 根据主键ID获取项目标识代码
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public string GetXMBZDMByID(string ID)
        {
            string XMBZBM ="";
            DataSet DS = new DataSet();
            if (ID != "")
            {
                string sql = "SELECT XMBZBM FROM TB_BBBZXMBM WHERE ID='" + ID + "'";
                DS = DbHelperOra.Query(sql);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    XMBZBM = DS.Tables[0].Rows[0]["XMBZBM"].ToString();
                }
            }
            return XMBZBM;
        }
        /// <summary>
        /// 根据ID更新相关的项目明细相关的信息
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMBZBM">项目标准代码</param>
        /// <param name="XMMC">项目名称</param>
        /// <param name="IsGXBM">是否更新标准编码</param>
        /// <returns></returns>
        public int UpdateXMMXByID(string ID, string XMBZBM, string XMMC, bool IsGXBM)
        {
            int Flag=0;
            if (ID != "")
            {
                string SQL = "";
                //判断是否需要跟新项目标准编码
                if (IsGXBM == true)
                {
                    SQL = "UPDATE TB_BBBZXMBM SET XMBZBM='" + XMBZBM + "',XMMC='" + XMMC + "',MCBM='" + XMMC + "' WHERE ID='" + ID + "'";
                }
                else
                {
                    SQL = "UPDATE TB_BBBZXMBM SET XMMC='" + XMMC + "',MCBM='" + XMMC + "' WHERE ID='" + ID + "'";
                }
                Flag=DbHelperOra.ExecuteSql(SQL);
            }
            return Flag;
        }
        /// <summary>
        /// 根据ID更新项目标编码
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMBZBM">项目标准代码</param>
        /// <returns></returns>
        public int UpdateXMBZBMByID(string ID,string XMBZBM)
        {
            int Flag = 0;
            if (ID != "")
            {
                string SQL = "UPDATE TB_BBBZXMBM SET XMBZBM='" + XMBZBM + "' WHERE ID='" + ID + "'";
                Flag = DbHelperOra.ExecuteSql(SQL);
            }
            return Flag;
        }
        /// <summary>
        /// 根据年份和核算中心代码查询相关的信息
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetHSZXInfo(string HSZXDM,string YY)
        {
            StringBuilder strSql = new StringBuilder();
            //strSql.Append("SELECT HSZXDM,HSZXMC FROM TB_HSZXZD"); 
            //strSql.Append(" WHERE NBBM LIKE (SELECT NBBM FROM TB_HSZXZD WHERE HSZXDM='"+HSZXDM+"' AND YY='"+YY+"')");
            //strSql.Append(" AND YJDBZ='1'");
            strSql.Append("SELECT HSZXDM,HSZXMC FROM TB_HSZXZD WHERE HSZXDM='" + HSZXDM + "' AND YY='" + YY + "'");
            return DbHelperOra.Query(strSql.ToString());
        }
        /// <summary>
        /// 根据年份和核算中心代码查询相关的产品类型信息
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetCPLXInfo(string HSZXDM, string YY,string jhlx)
        {
            //计划  AND YJDBZ='1'  AND XMDH_A IN(3,5,6)
            if (jhlx == "1")
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("SELECT ZYDM DM,ZYMC MC,7 BS FROM TB_JHZYML WHERE HSZXDM='" + HSZXDM + "' AND YJDBZ='1'");
                strSql.Append(" UNION");
                strSql.Append(" SELECT CONVERT(VARCHAR,XMDH_A) DM,XMMC MC,XMDH_A BS FROM XT_CSSZ WHERE XMFL='CPBS' AND XMDH_A <>1 ");
                strSql.Append(" ORDER BY DM");
                return DbHelperOra.Query(strSql.ToString());
            }
             //实际
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("SELECT ZYDM DM,ZYMC MC,7 BS  FROM TB_ZYML WHERE HSZXDM='" + HSZXDM + "' AND YY='" + YY + "' AND YJDBZ='1'");
                strSql.Append(" UNION");
                strSql.Append(" SELECT CONVERT(VARCHAR,XMDH_A) DM,XMMC MC,XMDH_A BS FROM XT_CSSZ WHERE XMFL='CPBS' AND XMDH_A <>1 ");
                strSql.Append(" ORDER BY DM");
                return DbHelperOra.Query(strSql.ToString());
            }
        }
        /// <summary>
        /// 向表中插入数据
        /// </summary>
        /// <param name="ID">项目ID</param>
        /// <param name="DM">实际代码</param>
        /// <returns></returns>
        public int InsertABCM(string ID, string DM,string hszxdm)
        {
            int Rtn = 0;
            ArrayList List = new ArrayList();
            List<String> ArrList = new List<String>();
            //获取权限数据的sql语句
            GetABCMSql(ref ArrList, ref List,ID,DM,hszxdm);
            Rtn = DbHelperOra.ExecuteSql(ArrList.ToArray(), List);
            return Rtn;
        }
        /// <summary>
        /// 获取需插入权限表的sql语句集合
        /// </summary>
        /// <param name="ArrList">sql语句数据集</param>
        /// <param name="List">参数集合</param>
        /// <param name="ID">项目代码</param>
        /// <param name="DM">与ABDM对应的列表主键</param>
        public void GetABCMSql(ref List<String> ArrList, ref ArrayList List, string ID,string DM,string hszxdm)
        {
            string [] BM = DM.Split(',');
            for (int j = 0; j < BM.Length; j++)
            {
                string sql = "INSERT INTO TB_BBBZXMBMDY(ID,DYABCMDM,HSZXDM)VALUES(@ID,@DYABCMDM,@HSZXDM)";
                ArrList.Add(sql);
                AseParameter[] ABCM = {
                new AseParameter("@ID",AseDbType.VarChar,40),
                new AseParameter("@DYABCMDM",AseDbType.VarChar,40),
                new AseParameter("@HSZXDM",AseDbType.VarChar,40)
                };
                ABCM[0].Value = ID;
                ABCM[1].Value = BM[j].ToString();
                ABCM[2].Value = hszxdm;
                List.Add(ABCM);
            }
        }
        /// <summary>
        /// 根据ID 获取项目分类明细的相关信息
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public DataSet GetBZBMInfoByID(string ID)
        {
            DataSet DS = new DataSet();
            if (ID != "")
            {
                string sql = "SELECT CCJB,YJDBZ,FBDM FROM TB_BBBZXMBM WHERE ID='"+ID+"'";
                DS=DbHelperOra.Query(sql);
            }
            return DS;
        }
        /// <summary>
        /// 根据表名字段值条件，查询相关的字段内容数据集
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="CXZD">查询字段</param>
        /// <param name="TJ">条件字段</param>
        /// <param name="TJValues">条件字段值</param>
        /// <returns>返回数据集</returns>
        public DataSet GetData(string TableName,string CXZD,string TJ,string TJValues)
        {
            DataSet DS = new DataSet();
            if (TJValues != "")
            {
                string sql = "SELECT "+CXZD+" FROM "+TableName+" WHERE "+TJ+" = '"+TJValues+"'";
                DS=DbHelperOra.Query(sql);
            }
            return DS;
        }
        /// <summary>
        /// 判断当前菜单是否为父节点报表
        /// </summary>
        /// <param name="ID">报表ID</param>
        /// <param name="NAME">报表名称</param>
        /// <returns></returns>
        public bool ISBBMX(string ID, string NAME)
        {
            bool Flag = false;
            if (ID != "")
            {
                string SQL = "SELECT * FROM BB_MLGLMX WHERE ID='"+ID+"' AND MLMXMC='"+NAME+"' ";
                if (DbHelperOra.Query(SQL).Tables[0].Rows.Count > 0)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        /// <summary>
        /// 根据报表代码以及年份获取此报表下面所有的列
        /// </summary>
        /// <param name="YY">年份</param>
        /// <param name="COLBM">报表代码</param>
        /// <returns></returns>
        public DataSet GetCoLNameDS(string YY,string COLBM)
        {
            DataSet DS = new DataSet();
            string sql = "SELECT COLBM,XMMC FROM TB_WLTBZDSZ S WHERE COLBM LIKE '"+COLBM+"%' AND COLBM<>'"+COLBM+"' AND YY='"+YY+"'";
            DS=DbHelperOra.Query(sql);
            return DS;
        }
        /// <summary>
        /// 获取产品代码
        /// </summary>
        /// <returns></returns>
        public string GetCPDM()
        {
            string CPDM = "";
            DataSet DS = new DataSet();
            string sql = "SELECT PREVSTR+CONVERT(VARCHAR,CONVERT(NUMERIC,CURRENTDM)+1) FROM TB_CURRENTDM WHERE DMCLASS='8'";
            DS=DbHelperOra.Query(sql);
            if (DS.Tables[0].Rows.Count > 0)
            {
                CPDM = DS.Tables[0].Rows[0][0].ToString();
                string UPDM = "UPDATE TB_CURRENTDM SET CURRENTDM=CONVERT(VARCHAR,CONVERT(NUMERIC,CURRENTDM)+1) WHERE DMCLASS='8'";
                int rtn=DbHelperOra.ExecuteSql(UPDM);
            }
            return CPDM;
        }
        /// <summary>
        /// 新增物料的相关操作
        /// </summary>
        /// <param name="WLName">物料名称</param>
        /// <param name="JLDW">计量单位</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <param name="MM">月份</param>
        /// <param name="DD">天数</param>
        /// <param name="USERDM">当前登录人代码</param>
        /// <param name="ZYDM">作业代码</param>
        /// <param name="COLBM">报表编码</param>
        /// <returns></returns>
        public int InsertWL(string WLName,string JLDW,string HSZXDM,string YY,string MM,string DD,string USERDM,string ZYDM,string COLBM,string BZNR)
        {
            int Flag = 0;
            ArrayList Arr=new ArrayList();
            List<string> List = new List<string>();
            //获取新增物料的相关操作的sql语句
            GetWLSql(ref List,ref Arr, WLName, JLDW, HSZXDM, YY, MM, DD, USERDM, ZYDM, COLBM,BZNR);
            Flag=DbHelperOra.ExecuteSql(List.ToArray(), Arr);
            return Flag;
        }
        /// <summary>
        /// 拼接插入更新到货单表、自检物资检验单的sql语句
        /// </summary>
        /// <returns></returns>
        /// <param name="Arr">用于存储参数的ArryList集合</param>
        /// <param name="List">用于存储sql语句的List集合</param>
        /// <param name="WLName">物料名称</param>
        /// <param name="JLDW">计量单位</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <param name="MM">月份</param>
        /// <param name="DD">天数</param>
        /// <param name="USERDM">当前登录人代码</param>
        /// <param name="ZYDM">作业代码</param>
        /// <param name="COLBM">报表编码</param>
        public void GetWLSql(ref List<string> List, ref ArrayList Arr, string WLName, string JLDW, string HSZXDM, string YY, string MM, string DD, string USERDM, string ZYDM, string COLBM,string BZNR)
        {
            string CPDM=GetCPDM();
            string CPBM = GetCPDM();
            //项产品编码表里面插入新增物料
            string CP = "INSERT INTO TB_CPBM(BZBM,CPDM,ZYDM,CPBM,CPMC,CPBS,ZJF,PROSDM,SJDJ,JHDJ,JLDW,BZ,CPHYBS,XHBZ,JGLBZ,FYYSDM,ZFJSFS,CWCPBM,SYBZ,CPSCBS,CPNBBM,C_ORDER,WLLBDM,HSZXDM,YY,CLDYBZ,CCJB,YJDBZ,FBDM,JSXS,ZZSL,XFSL,CPBZBM,SFYLG,RKXM,CBXMBM,CWHSFS,FLBH,CWHSBM,DHDW,DJTRLDM,DJXS) VALUES (NULL,@CPDM, NULL,@CPBM,@WLName, 6, NULL, NULL, NULL, NULL,@JLDW, '0', NULL, 2, 0, NULL, 2, NULL, '1', NULL, NULL, 0, '-1',@HSZXDM,@YY, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', '', NULL, '', '', '', NULL, NULL)";
            List.Add(CP);
            AseParameter[] parameters = {
                new AseParameter("@CPDM", AseDbType.VarChar,40),
                new AseParameter("@CPBM", AseDbType.VarChar,40),
                new AseParameter("@WLName", AseDbType.VarChar,60),
                new AseParameter("@JLDW", AseDbType.VarChar,40),
                new AseParameter("@HSZXDM", AseDbType.VarChar,40),
                new AseParameter("@YY", AseDbType.VarChar,4)
            };
            parameters[0].Value = CPDM;
            parameters[1].Value = CPBM;
            parameters[2].Value = WLName;
            parameters[3].Value = JLDW; 
            parameters[4].Value = HSZXDM; 
            parameters[5].Value = YY;
            Arr.Add(parameters);
            //向用户物料权限表里面插入用户权限数据
            string QXSql = "INSERT INTO TB_YHWLQX (YY, YHBH, ZYDM, COLBM, WLDM) VALUES (@YY,@USERDM,@ZYDM,@COLBM,@WLDM) ";
            List.Add(QXSql);
            AseParameter[] QX = {
                new AseParameter("@YY", AseDbType.VarChar,4),
                new AseParameter("@USERDM", AseDbType.VarChar,40),
                new AseParameter("@ZYDM", AseDbType.VarChar,40),
                new AseParameter("@COLBM", AseDbType.VarChar,40),
                new AseParameter("@WLDM", AseDbType.VarChar,40)
                };
            QX[0].Value = YY;
            QX[1].Value = USERDM;
            QX[2].Value = ZYDM; 
            QX[3].Value = COLBM;
            QX[4].Value = CPDM;
            Arr.Add(QX);
            //向备注表中插入数据
            string BZSql = "INSERT INTO TB_WLBZZD(YY,ZYDM,WLDM,BZID,BZNR)VALUES(@YY,@ZYDM,@WLDM,@BZID,@BZNR)";
            List.Add(BZSql);
            AseParameter[] BZ = {
                new AseParameter("@YY", AseDbType.VarChar,4),
                new AseParameter("@ZYDM", AseDbType.VarChar,40),
                new AseParameter("@WLDM", AseDbType.VarChar,40),
                new AseParameter("@BZID", AseDbType.VarChar,4),
                new AseParameter("@BZNR", AseDbType.VarChar,80)
                };
            BZ[0].Value = YY;
            BZ[1].Value = ZYDM;
            BZ[2].Value = CPDM;
            BZ[3].Value = "1";
            BZ[4].Value = BZNR;
            Arr.Add(BZ);

            //向物料日录入表中插入数据
            //string LRSql = "INSERT INTO TB_WLRSJLR(YY, MM, DD, HSZXDM, ZYDM, WLDM, COLBM, SL, BZID, GSBZ, SYDAYS, HSZQDM)VALUES (@YY,@MM,@DD,@HSZXDM,@ZYDM,@WLDM,@COLBM,0,'1','1',NULL,'5')";
            //List.Add(LRSql);
            //AseParameter[] LR = {
            //    new AseParameter("@YY", AseDbType.VarChar,4),
            //    new AseParameter("@MM", AseDbType.VarChar,2),
            //    new AseParameter("@DD", AseDbType.VarChar,2),
            //    new AseParameter("@HSZXDM", AseDbType.VarChar,40),
            //    new AseParameter("@ZYDM", AseDbType.VarChar,40),
            //    new AseParameter("@WLDM", AseDbType.VarChar,40),
            //    new AseParameter("@COLBM", AseDbType.VarChar,40)
            //    };
            //LR[0].Value = YY;
            //LR[1].Value = MM;
            //LR[2].Value = DD;
            //LR[3].Value = HSZXDM;
            //LR[4].Value = ZYDM;
            //LR[5].Value = CPDM;
            //LR[6].Value = COLBM;
            //Arr.Add(LR);
        }
        /// <summary>
        /// 将项目名称以及公式，插入到数据库
        /// </summary>
        /// <param name="GSStr">项目以及公式的字符串</param>
        /// <param name="FUNCID">模板代码</param>
        /// <returns></returns>
        public int InsertGS(string GSStr,string FUNCID)
        {
            int Flag = 0;
            string[] ES;
            string sqlStr = "",ID="";
            string[] GS = GSStr.Split('^');
            TB_ZDSXBDAL BLL = new TB_ZDSXBDAL();
            ArrayList ArrList = new ArrayList();
            List<string> SqlList = new List<string>();
            //删除之前的导入的公式
            sqlStr = "DELETE FROM TB_FUNCCOLUMNSZ WHERE FUNCID=@FUNCID";
            SqlList.Add(sqlStr);
            AseParameter[] DelGS = {
                    new AseParameter("@FUNCID",AseDbType.VarChar,8)
                    };
            DelGS[0].Value = FUNCID;
            ArrList.Add(DelGS);
            for (int i = 0; i < GS.Length; i++)
            {
                sqlStr = "INSERT INTO TB_FUNCCOLUMNSZ(ID,FUNCID,XMMC,GSVALUE) VALUES(@ID,@FUNCID,@XMMC,@GSVALUE)";
                SqlList.Add(sqlStr);
                AseParameter[] GSP = {
                    new AseParameter("@ID",AseDbType.VarChar,8),
                    new AseParameter("@FUNCID",AseDbType.VarChar,8),
                    new AseParameter("@XMMC",AseDbType.VarChar,50),
                    new AseParameter("@GSVALUE",AseDbType.VarChar,100)
                    };
                ID = BLL.GetSeed("TB_FUNCCOLUMNSZ", "ID");
                GSP[0].Value = ID;
                GSP[1].Value = FUNCID;
                //获取项目名称以及公式
                ES=GS[i].Split(',');
                GSP[2].Value = ES[0].ToString();
                GSP[3].Value = ES[1].ToString();
                ArrList.Add(GSP);
            }
            Flag=DbHelperOra.ExecuteSql(SqlList.ToArray(), ArrList);
            return Flag;
        }
        /// <summary>
        /// 将Excel中的数据，插入到数据库
        /// </summary>
        /// <param name="GSStr">项目以及公式的字符串</param>
        /// <param name="FUNCID">模板代码</param>
        /// <param name="Data">日期</param>
        /// <param name="TableName">数据表名称</param>
        /// <returns></returns>
        public int InsertDataByGS(string GSStr, string FUNCID,string Data,string TableName)
        {
            int Flag = 0;
            string[] ES;
            string sqlStr = "", ID = "";
            string[] GS = GSStr.Split('+');
            TB_ZDSXBDAL BLL = new TB_ZDSXBDAL();
            ArrayList ArrList = new ArrayList();
            List<string> SqlList = new List<string>();
            //删除之前的导入的公式
            sqlStr = "DELETE FROM " + TableName + " WHERE FUNCID=@FUNCID AND Data=@Data";
            SqlList.Add(sqlStr);
            AseParameter[] DelGS = {
                    new AseParameter("@FUNCID",AseDbType.VarChar,8),
                    new AseParameter("@Data",AseDbType.VarChar,8)
                    };
            DelGS[0].Value = FUNCID;
            DelGS[1].Value = Data;
            ArrList.Add(DelGS);
            for (int i = 0; i < GS.Length; i++)
            {
                sqlStr = "INSERT INTO " + TableName + "(ID,FUNCID,XMMC,DataValue,Data) VALUES(@ID,@FUNCID,@XMMC,@DataValue,@Data)";
                SqlList.Add(sqlStr);
                AseParameter[] GSP = {
                    new AseParameter("@ID",AseDbType.VarChar,8),
                    new AseParameter("@FUNCID",AseDbType.VarChar,8),
                    new AseParameter("@XMMC",AseDbType.VarChar,50),
                    new AseParameter("@DataValue",AseDbType.Double,8),
                    new AseParameter("@Data",AseDbType.VarChar,20)
                    };
                ID = BLL.GetSeed(TableName, "ID");
                GSP[0].Value = ID;
                GSP[1].Value = FUNCID;
                //获取项目名称以及公式
                ES = GS[i].Split(',');
                GSP[2].Value = ES[0].ToString();
                if (ES[1].ToString().Equals("") || ES[1].ToString().Equals(" ") || ES[1].ToString().Equals(null))
                {
                    GSP[3].Value = DBNull.Value;
                }
                else
                {
                    GSP[3].Value = ES[1].ToString();
                }
                GSP[4].Value = Data;
                ArrList.Add(GSP);
            }
            Flag = DbHelperOra.ExecuteSql(SqlList.ToArray(), ArrList);
            return Flag;
        }
        /// <summary>
        /// 根据sql语句查询相关的数据
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetDataBySql(string HSZXDM,string YY) 
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT HSZXDM,HSZXMC FROM TB_HSZXZD WHERE NBBM LIKE (");
            strSql.Append(" SELECT NBBM FROM TB_HSZXZD WHERE HSZXDM='" + HSZXDM + "' AND  YY='" + YY + "')+'%'");
            strSql.Append(" AND YJDBZ='1' AND YY='" + YY + "'");
            return DbHelperOra.Query(strSql.ToString());
        }
        /// <summary>
        /// 根据项目代码和采集装置的名称删除记录
        /// </summary>
        /// <param name="ID">需删除的主键的字符串</param>
        /// <param name="EID">需修改的主键字符串</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="WLDM">物料代码</param>
        /// <returns></returns>
        public int DelDWL(string ID,string EID,string HSZXDM,string WLDM)
        {
            string[] CJ;
            int Flag = 0,F=-1;
            string DelSql = "",InsSql="",DM="",WLSql="";
            ArrayList ArrList = new ArrayList();
            List<string> SqlList = new List<string>();
            //判断电物料代码是否存在，不存在的话，插入。
            DM=getDWLByHSZXDM(HSZXDM);
            if ( DM== "")
            {
                WLSql = "INSERT INTO TB_CJDWLDYB(HSZXDM,WLDM) VALUES(@HSZXDM,@WLDM)";
                SqlList.Add(WLSql);
                AseParameter[] WL = {
                            new AseParameter("@HSZXDM",AseDbType.VarChar,40),
                            new AseParameter("@WLDM",AseDbType.VarChar,40)
                            };
                WL[0].Value = HSZXDM;
                WL[1].Value = WLDM;
                ArrList.Add(WL);
                F = 1;
            }
            else
            {
                //如果存在的话，判断和当前的值，是否相等，不相等的话，更新字段
                if (!DM.Equals(WLDM))
                {
                    WLSql="UPDATE TB_CJDWLDYB SET WLDM=@WLDM WHERE HSZXDM=@HSZXDM";
                    SqlList.Add(WLSql);
                    AseParameter[] WL = {
                            new AseParameter("@HSZXDM",AseDbType.VarChar,40),
                            new AseParameter("@WLDM",AseDbType.VarChar,40)
                            };
                    WL[0].Value = HSZXDM;
                    WL[1].Value = WLDM;
                    ArrList.Add(WL);
                    F = 1;
                }
            }
            if (ID != "" || EID != "")
            {
                //删除采集装置
                if (ID != "")
                {
                    string[] Arr = ID.Split(',');
                    for (int i = 0; i < Arr.Length; i++)
                    {
                        //判断采集装置是否存在
                        if (IsExistDWL(Arr[i].ToString()))
                        {
                            DelSql = "DELETE FROM TB_CJZZDY WHERE ZYDM=@ZYDM";
                            SqlList.Add(DelSql);
                            AseParameter[] GSP = {
                            new AseParameter("@ZYDM",AseDbType.VarChar,40),
                            };
                            GSP[0].Value = Arr[i].ToString();
                            ArrList.Add(GSP);
                            F = 1;
                        }
                    }
                }
                //修改采集装置
                if (EID != "")
                {
                    string[] Arr = EID.Split('^');
                    for (int j = 0; j < Arr.Length; j++)
                    {
                        CJ = Arr[j].Split(',');
                        //判断当前采集装置是否存在，存在的话，修改此装置名称，不存在的话，插入装置
                        if (IsExistDWL(CJ[0].ToString()))
                        {
                            InsSql = "UPDATE TB_CJZZDY SET CJZYMC=@CJZYMC WHERE ZYDM=@ZYDM";
                            SqlList.Add(InsSql);
                            AseParameter[] Ins = {
                            new AseParameter("@ZYDM",AseDbType.VarChar,40),
                            new AseParameter("@CJZYMC",AseDbType.VarChar,100)
                            };
                            Ins[0].Value = CJ[0].ToString();
                            Ins[1].Value = CJ[1].ToString();
                            ArrList.Add(Ins);
                            F = 1;
                        }
                        else
                        {
                            InsSql = "INSERT INTO TB_CJZZDY(HSZXDM,ZYDM,CJZYMC) VALUES(@HSZXDM,@ZYDM,@CJZYMC)";
                            SqlList.Add(InsSql);
                            AseParameter[] Ins = {
                            new AseParameter("@ZYDM",AseDbType.VarChar,40),
                            new AseParameter("@CJZYMC",AseDbType.VarChar,100),
                            new AseParameter("@HSZXDM",AseDbType.VarChar,40)
                            };
                            Ins[0].Value = CJ[0].ToString();
                            Ins[1].Value = CJ[1].ToString();
                            Ins[2].Value = HSZXDM;
                            ArrList.Add(Ins);
                            F = 1;
                        }
                    }
                }
            }
            if (F != -1)
            {
                Flag = DbHelperOra.ExecuteSql(SqlList.ToArray(), ArrList);
            }
            return Flag;
        }
        /// <summary>
        /// 根据核算中心代码和作业代码查询相关采集项目是否存在
        /// </summary>
        /// <param name="ZYDM">作业代码</param>
        /// <returns></returns>
        public bool IsExistDWL(string ZYDM)
        {
            bool Flag = false;
            string sql = "SELECT CJZYMC FROM TB_CJZZDY WHERE ZYDM='" + ZYDM + "'";
            if (DbHelperOra.Query(sql).Tables[0].Rows.Count > 0)
            {
                Flag = true;
            }
            return Flag;
        }
        /// <summary>
        /// 根据核算中心代码获取物料代码
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <returns></returns>
        public string getDWLByHSZXDM(string HSZXDM)
        {
            string WLDM = "";
            if (HSZXDM != "")
            {
                DataSet DS = new DataSet();
                string sql = "SELECT WLDM FROM TB_CJDWLDYB WHERE HSZXDM='" + HSZXDM + "'";
                DS = DbHelperOra.Query(sql);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    WLDM = DS.Tables[0].Rows[0]["WLDM"].ToString();
                }
            }
            return WLDM;
        }
        #endregion
    }
}
