﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using RMYH.DBUtility;
using System.Data;

namespace RMYH.DAL
{
    public partial class TB_YSTZDAL
    {
        public TB_YSTZDAL() { }

        #region 预算模板调整时，插入预算调整记录

        /// <summary>
        /// 预算调整时，向预算记录表中插入相关的记录
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <param name="JSDM">调整人所属角色</param>
        /// <param name="USERDM">调整人</param>
        /// <param name="List">返回存储SQL的List<String>集合</param>
        public void InsertYSTZJL(string JHFADM, string MBDM, string ZT, string JSDM, string USERDM, ref List<String> List, string MenuBZ)
        {
            string SQL = "";
            DataSet DS = new DataSet();
            //插入、更新调整记录
            if (ZT == "1")
            {
                if (MenuBZ == "TZSB")
                {
                    SQL = "SELECT TZGZBH FROM TB_YSTZJL WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JSDM + "'";
                    //如果存在此模板调整的记录的话，则更新调整人和调整时间
                    if (DbHelperOra.Query(SQL).Tables[0].Rows.Count > 0)
                    {
                        SQL = "UPDATE TB_YSTZJL SET TZGZBH='" + USERDM + "',TZTIME='" + DateTime.Now.ToString() + "',MBZT='" + ZT + "' WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JSDM + "'";
                    }
                    else
                    {
                        SQL = "INSERT INTO TB_YSTZJL(JHFADM,MBDM,MBZT,ZTIME,JSDM,TZGZBHT)VALUES('" + JHFADM + "','" + MBDM + "','" + ZT + "','" + DateTime.Now.ToString() + "','" + JSDM + "','" + USERDM + "')";
                    }

                    List.Add(SQL);
                }
                
            }
            else 
            {
                SQL = "UPDATE TB_YSTZJL SET MBZT='"+ZT+"' WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "'";
                List.Add(SQL);
            }
        }
        #endregion

        #region 预算模板调整时，插入预算调整记录

        /// <summary>
        /// 预算调整时，向预算记录表中插入相关的记录
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <param name="JSDM">调整人所属角色</param>
        /// <param name="USERDM">调整人</param>
        /// <param name="List">返回存储SQL的List<String>集合</param>
        public void InsertTZJL(string JHFADM, string MBDM, string ZT, string JSDM, string USERDM, ref ArrayList List, string MenuBZ)
        {
            string SQL = "";
            DataSet DS = new DataSet();
            //插入、更新调整记录
            if (ZT == "1")
            {
                if (MenuBZ == "YSTZ")
                {
                    SQL = "SELECT TZGZBH FROM TB_YSTZJL WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JSDM + "'";
                    //如果存在此模板调整的记录的话，则更新调整人和调整时间
                    if (DbHelperOra.Query(SQL).Tables[0].Rows.Count > 0)
                    {
                        SQL = "UPDATE TB_YSTZJL SET TZGZBH='" + USERDM + "',TZTIME='" + DateTime.Now.ToString() + "',MBZT='" + ZT + "' WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JSDM + "'";
                    }
                    else
                    {
                        SQL = "INSERT INTO TB_YSTZJL(JHFADM,MBDM,MBZT,ZTIME,JSDM,TZGZBHT)VALUES('" + JHFADM + "','" + MBDM + "','" + ZT + "','" + DateTime.Now.ToString() + "','" + JSDM + "','" + USERDM + "')";
                    }

                    List.Add(SQL);
                }

            }
            else
            {
                SQL = "UPDATE TB_YSTZJL SET MBZT='" + ZT + "' WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "'";
                List.Add(SQL);
            }
        }
        #endregion

        #region 查询调整前方案和使用流程

        /// <summary>
        /// 查询调整前方案和使用流程
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>返回DataSet数据集</returns>
        public DataSet getOldJHFA(string JHFADM)
        {
            string SQL = "SELECT OLDJHFADM,J.LCDM,L.HSZXDM FROM TB_JHFA J,TB_YSLCMC L WHERE J.LCDM=L.LCDM AND JHFADM='"+JHFADM+"'";
            return DbHelperOra.Query(SQL);
        }
        #endregion

        #region 向调整内容以及调整内容日志表中插入新记录

        /// <summary>
        /// 向调整内容以及调整内容日志表中插入新记录
        /// </summary>
        /// <param name="List">SQL语句集合</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="USERDM">当前操作人</param>
        public void InsertYSTZNR(ref List<String> List, string JHFADM, string MBDM, string JSDM, string USERDM,string LCDM,string OldFADM)
        {
            string SQL = "";
            //先删除之前保存的数据
            SQL = "DELETE FROM TB_YSTZNR WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "'";
            List.Add(SQL);
            if (LCDM != "" && OldFADM != "")
            {
                SQL = "INSERT INTO TB_YSTZNR(JHFADM,MBDM,XMDM,XMFL,ZYDM,JSDX,FVALUE,LVALUE,JLDW,SJDX)";
                SQL += " SELECT B.JHFADM,B.MBDM,B.XMDM,B.XMFL,B.ZYDM,B.SJDX,A.VALUE FVALUE,B.VALUE LVALUE,B.JLDW,B.SJDX FROM TB_BBFYSJ A,TB_BBFYSJ B";
                SQL += " WHERE A.JHFADM='" + OldFADM + "' AND B.JHFADM='" + JHFADM + "' AND A.MBDM='" + MBDM + "' AND B.MBDM='" + MBDM + "'";
                SQL += " AND A.MBDM=B.MBDM AND A.XMDM=B.XMDM AND A.SJDX=B.SJDX";
                SQL += " AND A.XMFL=B.XMFL AND A.JSDX=B.JSDX and A.ZYDM=B.ZYDM";
                SQL += " AND ISNULL(A.VALUE,0)<>ISNULL(B.VALUE,0)";
                List.Add(SQL);

                //获取插入调整内容日志表的SQL语句
                SQL = GetInsertTZNRMXSql(JHFADM, MBDM, JSDM, USERDM, LCDM);
                //如果调整内容日志表有变动的数据，则插入
                if (DbHelperOra.Query(SQL).Tables[0].Rows.Count > 0)
                {
                    //先删除之前保存的数据
                    SQL = "DELETE FROM TB_YSTZNRMX WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' JSDM='" + JSDM + "'";
                    List.Add(SQL);
                    //插入新的调整记录日志
                    InsertTZNRMX(ref List, JHFADM, MBDM, JSDM, USERDM, LCDM);
                }
            }
            
        } 
        #endregion

        #region 向调整内容日志表里面插入数据
        /// <summary>
        /// 向调整内容日志表里面插入数据
        /// </summary>
        /// <param name="List">SQL语句集合</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="USERDM">当前操作人</param>
        public void InsertTZNRMX(ref List<String> List, string JHFADM, string MBDM, string JSDM, string USERDM,string LCDM)
        {
            string SQL = "";
            string rtn=GetInsertTZNRMXSql(JHFADM,MBDM,JSDM,USERDM,LCDM);
            if (rtn != "")
            {
                SQL = "INSERT INTO TB_YSTZNRMX(JHFADM,MBDM,XMDM,XMFL,ZYDM,JSDX,FVALUE,LVALUE,JLDW,SJDX,JSDM,TZR)";
                SQL += rtn;
                List.Add(SQL);
            }
        } 
        #endregion


        #region 获取插入调整内容日志表的SQL语句
        /// <summary>
        /// 获取插入调整内容日志表的SQL语句
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="USERDM">当前操作人</param>
        /// <param name="LCDM">流程代码</param>
        public string GetInsertTZNRMXSql(string JHFADM, string MBDM, string JSDM, string USERDM, string LCDM)
        {
            string SQL = "";
            DataSet DT = new DataSet();
            SQL = "SELECT MBDM,JSDM,CZLX FROM TB_YSLCNode N,TB_YSJSXGMB G";
            SQL += " WHERE N.LCDM='" + LCDM + "' AND  N.FBJSDM='" + JSDM + "'";
            SQL += " AND N.LCDM=G.LCDM AND N.JSDM=G.JSDM AND N.HSZXDM=G.HSZXDM";
            DT = DbHelperOra.Query(SQL);
            DataRow[] DR = DT.Tables[0].Select("MBDM='" + MBDM + "'");
            if (DR.Length > 0)
            {
                SQL = " SELECT N.JHFADM,N.MBDM,N.XMDM,N.XMFL,N.ZYDM,N.JSDX,M.LVALUE FVALUE,N.LVALUE,N.JLDW,N.SJDX,'" + JSDM + "','" + USERDM + "' FROM TB_YSTZNRMX M,TB_YSTZNR N";
                SQL += " WHERE M.JHFADM='" + JHFADM + "' AND N.JHFADM='" + JHFADM + "' AND M.MBDM=" + MBDM + "' AND N.MBDM='" + MBDM + "' AND M.JSDM='" + DR[0]["JSDM"].ToString() + "'";
                SQL += " AND M.JHFADM=N.JHFADM AND M.MBDM=N.MBDM AND M.XMDM=N.XMDM AND M.SJDX=N.SJDX";
                SQL += " AND M.XMFL=N.XMFL AND M.JSDX=N.JSDX AND M.ZYDM=N.ZYDM";
                SQL += " AND ISNULL(M.LVALUE,0)<>ISNULL(N.LVALUE,0)";
            }
            else
            {
                SQL = " SELECT JHFADM,MBDM,XMDM,XMFL,ZYDM,JSDX,FVALUE,LVALUE,JLDW,SJDX,'" + JSDM + "','" + USERDM + "' FROM TB_YSTZNR";
                SQL += " WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "'";
            }
            return SQL;
        } 
        #endregion

        #region  删除调整模板相关项目的所有上级相关模板审批记录

        /// <summary>
        /// 删除调整模板相关项目的所有上级相关模板审批记录
        /// </summary>
        /// <param name="List">SQL语句集合</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="LCDM">流程代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        public void DelXGMBAllPar(ref List<String> List, string JHFADM, string MBDM, string LCDM, string HSZXDM)
        {
            string SQL = "", MB = "";
            //删除调整模板相关的填报和审批记录
            SQL = " DELETE FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND LCDM='" + LCDM + "' AND DQMBDM='" + MBDM + "' AND HSZXDM='" + HSZXDM + "'";
            List.Add(SQL);

            //获取调整模板相关项目的上级模板以及角色信息
            DataSet DS = GetTZMBPar(JHFADM, MBDM, LCDM);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //如果存在上级的话，则表明此模板相关的项目在别的模板中存在，则只删除相关模板的审批记录
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                    SQL = " DELETE FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND LCDM='" + LCDM + "' AND DQMBDM='" + MB + "' AND HSZXDM='" + HSZXDM + "'";
                    List.Add(SQL);
                    //递归MB以及角色的的父节点
                    RecursionPar(ref List, JHFADM, MBDM, MB, LCDM, HSZXDM);
                }
            }
            else
            {
                DS.Tables.Clear();
                DS = GetParMB(HSZXDM, LCDM, MBDM);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                        SQL = " DELETE FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND LCDM='" + LCDM + "' AND DQMBDM='" + MB + "' AND HSZXDM='" + HSZXDM + "'";
                        List.Add(SQL);
                        //递归MB以及角色的的父节点
                        RecursionPar(ref List, JHFADM, MBDM, MB, LCDM, HSZXDM);
                    }
                }
            }
        } 
        #endregion

        #region 递归删除相关父模板的审批记录

        /// <summary>
        /// 递归删除相关父模板的审批记录
        /// </summary>
        /// <param name="List">SQL集合</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">调整模板代码</param>
        /// <param name="MB">当前删除模板代码</param>
        /// <param name="LCDM">流程代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        public void RecursionPar(ref List<String> List, string JHFADM, string MBDM, string MB, string LCDM, string HSZXDM)
        {
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            string SQL = "", DM = "";
            SQL = "SELECT DISTINCT A.MBDM,D.JSDM FROM TB_BBFYSJ A, TB_YSJSMBZMB D ";
            SQL += " WHERE A.JHFADM='" + JHFADM + "' AND A.XMDM IN(SELECT XMDM FROM TB_YSTZNR WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "')";
            SQL += " AND A.MBDM=D.MBDM AND D.CHILDMBDM='" + MB + "' AND D.LCDM =" + int.Parse(LCDM) + " AND  HSZXDM='" + HSZXDM + "'";
            DS.Tables.Clear();
            DS = DbHelperOra.Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DM = DS.Tables[0].Rows[i]["MBDM"].ToString();
                    SQL = " DELETE FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND LCDM='" + LCDM + "' AND DQMBDM='" + DM + "' AND HSZXDM='" + HSZXDM + "'";
                    List.Add(SQL);
                    //递归MB以及角色的的父节点
                    RecursionPar(ref List, JHFADM, MBDM, MB, LCDM, HSZXDM);
                }
            }
            else
            {
                DT.Tables.Clear();
                DT = GetParMB(HSZXDM, LCDM, MB);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Tables[0].Rows.Count; i++)
                    {
                        MB = DT.Tables[0].Rows[i]["MBDM"].ToString();
                        SQL = " DELETE FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND LCDM='" + LCDM + "' AND DQMBDM='" + MB + "' AND HSZXDM='" + HSZXDM + "'";
                        List.Add(SQL);
                        //递归MB以及角色的的父节点
                        RecursionPar(ref List, JHFADM, MBDM, MB, LCDM, HSZXDM);
                    }
                }
            }
        } 
        #endregion

        #region 获取调整模板相关项目的上级模板以及角色信息
        /// <summary>
        /// 获取调整模板相关项目的上级模板以及角色信息
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="LCDM">流程代码</param>
        /// <returns></returns>
        public DataSet GetTZMBPar(string JHFADM, string MBDM, string LCDM)
        {
            string SQL = "";
            SQL = "SELECT DISTINCT A.MBDM,D.JSDM FROM TB_BBFYSJ A, TB_YSJSMBZMB D ";
            SQL += " WHERE A.JHFADM='" + JHFADM + "' AND A.XMDM IN(SELECT XMDM FROM TB_YSTZNR WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "')";
            SQL += " AND A.MBDM=D.MBDM AND D.CHILDMBDM='" + MBDM + "' AND D.LCDM =" + int.Parse(LCDM);
            return DbHelperOra.Query(SQL);
        } 
        #endregion


        #region 查询某模板的关联下级模板
        /// <summary>
        /// 查询某模板的关联下级模板
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="LCDM">角色代码</param>
        /// <returns></returns>
        public DataSet GetParMB(string HSZXDM,string LCDM,string MBDM)
        {
            string SQL = "";
            SQL = " SELECT DISTINCT MBDM,JSDM FROM TB_YSJSMBZMB WHERE HSZXDM='"+HSZXDM+"' AND LCDM=" + int.Parse(LCDM) + " AND CHILDMBDM='" + MBDM + "'";
            return DbHelperOra.Query(SQL);
        }
        #endregion

        #region 预算调整填报或者审批处理事件
        /// <summary>
        /// 预算调整填报或者审批处理事件
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="USERDM">用户代码</param>
        /// <param name="MenuType">菜单类型</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <returns>返回0表示执行失败；返回1表示执行成功</returns>
        public int YSTZSBOrSP(string JHFADM, string MBDM, string JSDM, string ZT, string USERDM, string MenuType)
        {
            int rtn = 0;
            DataSet DS = new DataSet();
            List<String> List = new List<String>();
            string LCDM = "", HSZXDM = "",OldJHFADM="";
            try
            {
                DS = getOldJHFA(JHFADM);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    LCDM = DS.Tables[0].Rows[0]["LCDM"].ToString();
                    HSZXDM = DS.Tables[0].Rows[0]["HSZXDM"].ToString();
                    OldJHFADM = DS.Tables[0].Rows[0]["OLDJHFADM"].ToString();
                }
                //预算模板调整时，插入或者更新预算调整记录
                InsertYSTZJL(JHFADM, MBDM, ZT, JSDM, USERDM, ref List, MenuType);
                //向调整内容以及调整内容日志表中插入新记录
                InsertYSTZNR(ref List, JHFADM, MBDM, JSDM, USERDM, LCDM, OldJHFADM);
                //删除调整模板相关项目的所有上级相关模板审批记录
                DelXGMBAllPar(ref List, JHFADM, MBDM, LCDM, HSZXDM);
                //调一下小牛的上报方法


                rtn = DbHelperOra.ExecuteSql(List.ToArray(), null);
            }
            catch (Exception E)
            {
                throw E;
            }
            return rtn;
        } 
        #endregion

        #region 获取插上个节点的模板修改项目列表
        /// <summary>
        /// 获取插上个节点的模板修改项目列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        ///<param name="TZBZ">调整页面标识（TZSB表示上报；TZSP表示审批）</param>
        public string GetLastTZMX(string JHFADM, string MBDM, string JSDM, string TZBZ)
        {
            int Total = 0;
            DataSet DS = new DataSet();
            StringBuilder Str = new StringBuilder();
            string SQL = "", LCDM = "", HSZXDM = "";
            //获取方案的绑定的流程代码、核算中心以及被调整前的方案
            DS = getOldJHFA(JHFADM);
            if (DS.Tables[0].Rows.Count > 0)
            {
                LCDM = DS.Tables[0].Rows[0]["LCDM"].ToString();
                HSZXDM = DS.Tables[0].Rows[0]["HSZXDM"].ToString();
            }
            //判断当前操作的页面是预算调整还是预算审批
            if (TZBZ == "TZSB")
            {
                Str.Append("{\"total\":" + Total + ",");
                Str.Append("\"rows\":[");
                Str.Append("]}");
            }
            else
            {
                SQL = "SELECT MBDM,JSDM,CZLX FROM TB_YSLCNode N,TB_YSJSXGMB G";
                SQL += " WHERE N.LCDM='" + LCDM + "' AND  N.FBJSDM='" + JSDM + "'";
                SQL += " AND N.LCDM=G.LCDM AND N.JSDM=G.JSDM AND N.HSZXDM=G.HSZXDM";
                DS.Tables.Clear();
                DS = DbHelperOra.Query(SQL);
                DataRow[] DR = DS.Tables[0].Select("MBDM='" + MBDM + "'");
                if (DR.Length > 0)
                {
                    SQL = "SELECT ZYMC,D.XMMC+'-'+C.XMMC MC,H.XMMC SJDX,E.XMMC JSDX,R.FVALUE,R.LVALUE,G.DWMC FROM TB_YSTZNRMX R,TB_JHZYML M,TB_XMXX C,TB_XMXX D,TB_JSDX E,TB_JLDW G,XT_CSSZ H ";
                    SQL += " WHERE R.JHFADM='" + JHFADM + "' AND R.MBDM='" + MBDM + "' AND R.JSDM='" + DR[0]["JSDM"] + "'";
                }
                else
                {
                    SQL = " SELECT ZYMC,D.XMMC+'-'+C.XMMC MC,H.XMMC SJDX,E.XMMC JSDX,R.FVALUE,R.LVALUE,G.DWMC FROM TB_YSTZNRMX R,TB_YSLCNode N,TB_YSJSMBZMB G,TB_BBFYSJ A,TB_JHZYML M,TB_XMXX C,TB_XMXX D,TB_JSDX E,TB_JLDW G,XT_CSSZ H ";
                    SQL += " WHERE R.JHFADM='" + JHFADM + "'";
                    SQL += " AND R.MBDM=G.CHILDMBDM AND G.LCDM='" + LCDM + "' AND G.JSDM='" + JSDM + "' AND G.MBDM='" + MBDM + "'";
                    SQL += " AND R.JSDM=N.JSDM AND N.LCDM='" + LCDM + "' AND N.FBJSDM='" + JSDM + "' ";
                    SQL += " AND R.JHFADM=A.JHFADM AND R.XMDM=A.XMDM  AND A.JHFADM='" + JHFADM + "'  AND A.MBDM='" + MBDM + "'";
                }
                SQL += " AND R.ZYDM=M.ZYDM";
                SQL += " AND R.XMDM=C.CMDM";
                SQL += " AND R.XMFL=D.XMDM";
                SQL += " AND R.SJDX=H.ZFCS AND H.XMFL='YSSJDX'";
                SQL += " AND R.JSDX=E.DM";
                SQL += " AND R.JLDW=G.DWDM";

                DS.Tables.Clear();
                DS = DbHelperOra.Query(SQL);
                //获取查询条件的总行数
                Total = DS.Tables[0].Rows.Count;
                Str.Append("{\"total\":" + Total + ",");
                Str.Append("\"rows\":[");
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Str.Append("{");
                    Str.Append("\"ZYMC\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["ZYMC"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"MC\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["MC"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"SJDX\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["SJDX"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"JSDX\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["JSDX"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"FVALUE\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["FVALUE"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"LVALUE\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["LVALUE"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"DWMC\":");
                    Str.Append("\"" + DS.Tables[0].Rows[i]["DWMC"].ToString() + "\"");
                    if (i < DS.Tables[0].Rows.Count - 1)
                    {
                        Str.Append("},");
                    }
                    else
                    {
                        Str.Append("}");
                    }
                }
                Str.Append("]}");
            }
            return Str.ToString();
        } 
        #endregion

        #region 获取流程待处理模板的所有上级节点模板的ID值

        /// <summary>
        /// 获取流程待处理模板的所有上级节点模板的ID值
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetNotDealAllParID(string JHFADM)
        {
            DataSet DS = new DataSet();
            string rtn = "", SQL = "", LCDM = "", HSZXDM = "";
            //获取方案的绑定的流程代码、核算中心以及被调整前的方案
            DS = getOldJHFA(JHFADM);
            if (DS.Tables[0].Rows.Count > 0)
            {
                LCDM = DS.Tables[0].Rows[0]["LCDM"].ToString();
                HSZXDM = DS.Tables[0].Rows[0]["HSZXDM"].ToString();
                SQL = "SELECT DQMBDM,JSDM FROM TB_YSLCSB WHERE JHFADM='" + JHFADM + "' AND STATE=-1 AND LCDM=" + int.Parse(LCDM) + " AND HSZXDM='" + HSZXDM + "'";
                DS = DbHelperOra.Query(SQL);
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    //递归其父节点
                    RecursionNotDealAllPar(LCDM, DS.Tables[0].Rows[0]["DQMBDM"].ToString(), DS.Tables[0].Rows[0]["JSDM"].ToString(), HSZXDM, ref rtn);
                }
            }
            return rtn;
        } 
        #endregion

        #region 递归获取流程待处理模板的所有上级模板的相关信息

        /// <summary>
        /// 递归获取流程待处理模板的所有上级模板的相关信息
        /// </summary>
        /// <param name="LCDM">流程代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="rtn">上级节点模板的ID值</param>
        public void RecursionNotDealAllPar(string LCDM, string MBDM, string JSDM, string HSZXDM, ref string rtn)
        {
            string SQL = "", ID = "";
            DataSet DS = new DataSet();
            SQL = "SELECT ID,N.FBJSDM FROM TB_YSLCNode N,TB_YSJSXGMB G";
            SQL += " WHERE N.LCDM=" + int.Parse(LCDM) + " AND N.HSZXDM='" + HSZXDM + "' AND N.JSDM='" + JSDM + "'";
            SQL += " AND N.FBJSDM=G.JSDM AND N.LCDM=G.LCDM AND N.HSZXDM=G.HSZXDM AND G.MBDM='" + MBDM + "'";
            DS.Tables.Clear();
            DS = DbHelperOra.Query(SQL);
            //判断当前模板是否为此模板审批的最顶级；大于0说明它不是最高级；否则它就是别的模板的关联下级模板
            if (DS.Tables[0].Rows.Count > 0)
            {
                ID = DS.Tables[0].Rows[0]["ID"].ToString();
                rtn = rtn == "" ? "'" + ID + "'" : rtn.IndexOf("'" + ID + "'") == -1 ? rtn + ",'" + ID + "'" : rtn;
                //递归其父节点
                RecursionNotDealAllPar(LCDM, MBDM, DS.Tables[0].Rows[0]["FBJSDM"].ToString(), HSZXDM, ref rtn);
            }
            else
            {
                SQL = "SELECT M.ID,M.MBDM,M.JSDM FROM TB_YSJSMBZMB G,TB_YSJSXGMB M";
                SQL += " WHERE G.LCDM='" + int.Parse(LCDM) + "' AND G.HSZXDM='" + HSZXDM + "' AND G.CHILDMBDM='" + MBDM + "'";
                SQL += " AND G.MBDM=M.MBDM AND G.JSDM=M.JSDM AND M.LCDM='" + int.Parse(LCDM) + "' AND M.HSZXDM='" + HSZXDM + "'";
                DS.Tables.Clear();
                DS = DbHelperOra.Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        ID = DS.Tables[0].Rows[i]["ID"].ToString();
                        rtn = rtn == "" ? "'" + ID + "'" : rtn.IndexOf("'" + ID + "'") == -1 ? rtn + ",'" + ID + "'" : rtn;
                        //递归其父节点
                        RecursionNotDealAllPar(LCDM, DS.Tables[0].Rows[i]["MBDM"].ToString(), DS.Tables[0].Rows[0]["JSDM"].ToString(), HSZXDM, ref rtn);
                    }
                }
            }
        } 
        #endregion
    }
}
