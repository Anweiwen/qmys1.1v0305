using System;
using System.Data;
using System.Text;
using Sybase.Data.AseClient;
using RMYH.DBUtility;//Please add references
using System.Collections;
using System.Web;
using System.Web.Security;
using RMYH.Common;
using System.IO;
using System.Collections.Generic;
namespace RMYH.DAL
{
    /// <summary>
    /// 数据访问类:TB_ZDSXB
    /// </summary>
    public partial class TB_ZDSXBDAL
    {
        public TB_ZDSXBDAL()
        { }
        #region  Method
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public RMYH.Model.TB_ZDSXBModel GetModel(string XMDM)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select XMDM,MKDM,TABLENAME,FIELDNAME,ZDZWM,SFXS,SJLX,JYGS,SRFS,SRKJ,SQL,LEN,ISONLY,DISPORDER,NULLS,XSZD,BCZD,SFBC,DEFAULTVALUE,PRIMARYKEY,COLUMNWIDTH,SFHZLD,WITHTO,INDEXZH,FORMATDISP,SFHSDW from TB_ZDSXB ");
            strSql.Append(" where XMDM=@XMDM ");
            AseParameter[] parameters = {
                    new AseParameter("@XMDM",AseDbType.Char,50)};
            parameters[0].Value = XMDM;

            RMYH.Model.TB_ZDSXBModel model = new RMYH.Model.TB_ZDSXBModel();
            DataSet ds = DbHelperOra.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model.XMDM = ds.Tables[0].Rows[0]["XMDM"].ToString();
                model.MKDM = ds.Tables[0].Rows[0]["MKDM"].ToString();
                model.TABLENAME = ds.Tables[0].Rows[0]["TABLENAME"].ToString();
                model.FIELDNAME = ds.Tables[0].Rows[0]["FIELDNAME"].ToString();
                model.ZDZWM = ds.Tables[0].Rows[0]["ZDZWM"].ToString();
                if (ds.Tables[0].Rows[0]["SFXS"].ToString() != "")
                {
                    model.SFXS = int.Parse(ds.Tables[0].Rows[0]["SFXS"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SJLX"].ToString() != "")
                {
                    model.SJLX = int.Parse(ds.Tables[0].Rows[0]["SJLX"].ToString());
                }
                if (ds.Tables[0].Rows[0]["JYGS"].ToString() != "")
                {
                    model.JYGS = int.Parse(ds.Tables[0].Rows[0]["JYGS"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SRFS"].ToString() != "")
                {
                    model.SRFS = int.Parse(ds.Tables[0].Rows[0]["SRFS"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SRKJ"].ToString() != "")
                {
                    model.SRKJ = int.Parse(ds.Tables[0].Rows[0]["SRKJ"].ToString());
                }
                //if(ds.Tables[0].Rows[0]["SQL"].ToString()!="")
                //{
                //model.SQL=(byte[])ds.Tables[0].Rows[0]["SQL"];
                model.SQL = ds.Tables[0].Rows[0]["SQL"].ToString();
                //}
                if (ds.Tables[0].Rows[0]["LEN"].ToString() != "")
                {
                    model.LEN = int.Parse(ds.Tables[0].Rows[0]["LEN"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ISONLY"].ToString() != "")
                {
                    model.ISONLY = int.Parse(ds.Tables[0].Rows[0]["ISONLY"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DISPORDER"].ToString() != "")
                {
                    model.DISPORDER = int.Parse(ds.Tables[0].Rows[0]["DISPORDER"].ToString());
                }
                if (ds.Tables[0].Rows[0]["NULLS"].ToString() != "")
                {
                    model.NULLS = int.Parse(ds.Tables[0].Rows[0]["NULLS"].ToString());
                }
                model.XSZD = ds.Tables[0].Rows[0]["XSZD"].ToString();
                model.BCZD = ds.Tables[0].Rows[0]["BCZD"].ToString();
                if (ds.Tables[0].Rows[0]["SFBC"].ToString() != "")
                {
                    model.SFBC = int.Parse(ds.Tables[0].Rows[0]["SFBC"].ToString());
                }
                model.DEFAULTVALUE = ds.Tables[0].Rows[0]["DEFAULTVALUE"].ToString();
                if (ds.Tables[0].Rows[0]["PRIMARYKEY"].ToString() != "")
                {
                    model.PRIMARYKEY = int.Parse(ds.Tables[0].Rows[0]["PRIMARYKEY"].ToString());
                }
                if (ds.Tables[0].Rows[0]["COLUMNWIDTH"].ToString() != "")
                {
                    model.COLUMNWIDTH = int.Parse(ds.Tables[0].Rows[0]["COLUMNWIDTH"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SFHZLD"].ToString() != "")
                {
                    model.SFHZLD = int.Parse(ds.Tables[0].Rows[0]["SFHZLD"].ToString());
                }
                model.WITHTO = ds.Tables[0].Rows[0]["WITHTO"].ToString();
                model.INDEXZH = ds.Tables[0].Rows[0]["INDEXZH"].ToString();
                model.FORMATDISP = ds.Tables[0].Rows[0]["FORMATDISP"].ToString();
                if (ds.Tables[0].Rows[0]["SFHSDW"].ToString() != "")
                {
                    model.SFHSDW = int.Parse(ds.Tables[0].Rows[0]["SFHSDW"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select XMDM,MKDM,TABLENAME,FIELDNAME,ZDZWM,SFXS,SJLX,JYGS,SRFS,SRKJ,SQL,LEN,ISONLY,DISPORDER,NULLS,XSZD,BCZD,SFBC,DEFAULTVALUE,PRIMARYKEY,COLUMNWIDTH,SFHZLD,WITHTO,INDEXZH,FORMATDISP,SFHSDW ");
            strSql.Append(" FROM TB_ZDSXB ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by DISPORDER asc");
            return DbHelperOra.Query(strSql.ToString());
        }
        #endregion  Method

        #region 公用方法
        /// <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="SQL">SQL语句</param>
        /// <returns></returns>
        public DataSet Query(string SQL)
        {
            return DbHelperOra.Query(SQL);
        }
        /// <summary>
        /// 查询指定项目代码中满足条件的数据
        /// </summary>
        /// <param name="XMDM">项目代码</param>
        /// <param name="IDName">待执行SQL语句中主键字段</param>
        /// <param name="IDValue">主键字段对应的条件值</param>
        /// <returns></returns>
        public DataSet Query(string XMDM, string IDName, string IDValue)
        {
            DataSet RetDataSet = null;
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select SQL FROM tb_tablesx WHERE XMDM='" + XMDM + "'");
                DataSet ds = DbHelperOra.Query(strSql.ToString());
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    RetDataSet = DbHelperOra.Query(ds.Tables[0].Rows[0][0].ToString() + " and " + IDName + "='" + IDValue + "'");

                }
            }
            catch
            {
            }
            return RetDataSet;
        }
        /// <summary>
        /// 公用更新数据方法
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="filedname">字段名,字段间以“,”隔开</param>
        /// <param name="filedvalue">字段值，字段间以“|”隔开</param>
        /// <param name="id">主键名</param>
        /// <param name="idvalue">主键值</param>
        /// <param name="extfiledname">隐藏保存字段</param>
        /// <param name="extvalue">隐藏保存值</param>
        /// <returns></returns>
        public string Update(string xmdm, string tablename, string filedname, string[] filedvalue, string id, string[] idvalue, string[] extfiledname, string[] extvalue)
        {
            ArrayList arrcmdParms = new ArrayList();
            string[] arrSQLString = new string[idvalue.Length];
            for (int k = 0; k < idvalue.Length; k++)
            {
                string strret = "";
                string strAddValue = "";
                string[] arrfiledname = filedname.Split(',');
                string[] arrfiledvalue = filedvalue[k].Split('|');
                StringBuilder strSql = new StringBuilder();
                DataSet ds;
                if (xmdm != "")
                    ds = GetList(" MKDM='" + xmdm + "' AND SFBC=1");
                else
                    ds = GetList(" TABLENAME='" + tablename + "' AND SFBC=1");
                //唯一性判断
                DataRow[] dr = ds.Tables[0].Select("ISONLY='0'");
                for (int i = 0; i < dr.Length; i++)
                {
                    for (int j = 0; j < arrfiledname.Length; j++)
                    {
                        if (arrfiledname[j].ToLower() == dr[i]["XSZD"].ToString().ToLower())
                        {
                            if (DbHelperOra.Query("SELECT " + dr[i]["XSZD"].ToString() + " FROM " + tablename + " WHERE PAR IN(SELECT PAR FROM " + tablename + " WHERE " + id + "='" + idvalue[k] + "') AND upper(" + dr[i]["XSZD"].ToString() + ")=upper('" + arrfiledvalue[j] + "') AND " + id + " !='" + idvalue[k] + "'").Tables[0].Rows.Count > 0)
                            {
                                strret = "0" + "同层次节点内已存在 " + arrfiledvalue[j];
                                return strret;
                            }
                        }
                    }
                }
                dr = ds.Tables[0].Select("ISONLY='1'");
                for (int i = 0; i < dr.Length; i++)
                {
                    for (int j = 0; j < arrfiledname.Length; j++)
                    {
                        if (arrfiledname[j].ToLower() == dr[i]["XSZD"].ToString().ToLower())
                        {
                            for (int m = 0; m < filedvalue.Length; m++)//判断当前记录是否在将要更新或者新增的数据中存在
                            {
                                if (m != k && arrfiledvalue[j].ToLower() == filedvalue[m].Split('|')[j].ToLower())
                                {
                                    strret = "0" + "表中已存在 " + arrfiledvalue[j];
                                    return strret;
                                }
                            }
                            if (DbHelperOra.Query("SELECT " + dr[i]["XSZD"].ToString() + " FROM " + tablename + " WHERE upper(" + dr[i]["XSZD"].ToString() + ")=upper('" + arrfiledvalue[j] + "') AND " + id + (idvalue[k] == "" ? " IS NOT NULL " : " !='" + idvalue[k] + "'")).Tables[0].Rows.Count > 0)
                            {
                                strret = "0" + "表中已存在 " + arrfiledvalue[j];
                                return strret;
                            }


                        }
                    }
                }
                AseParameter[] parameters;
                if (idvalue[k].Trim() == "")//判断当前记录是否新增
                {
                    bool f = true;
                    parameters = new AseParameter[ds.Tables[0].Rows.Count];
                    strSql.Append("insert into ");
                    strSql.Append(tablename);
                    strSql.Append("(");
                    strAddValue = " values(";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        strAddValue += "@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString();
                        strSql.Append(ds.Tables[0].Rows[i]["FIELDNAME"].ToString());
                        if (i < ds.Tables[0].Rows.Count - 1)
                        {
                            strSql.Append(",");
                            strAddValue += ",";
                        }
                    }
                    strSql.Append(")");
                    strSql.Append(strAddValue + ")");
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        f = true;
                        string strValue = "";
                        if (id == ds.Tables[0].Rows[i]["FIELDNAME"].ToString())//获取唯一主键值
                        {

                            //判断当前数据的类型0代表数字；1代表字符串
                            if (ds.Tables[0].Rows[i]["SJLX"].ToString().Trim() == "0")
                            {
                                strValue = GetSeed(tablename, id);
                                parameters[i] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), decimal.Parse(strValue));
                                parameters[i].AseDbType = AseDbType.Numeric;
                            }
                            else
                            {
                                strValue = GetStringPrimaryKey();
                                parameters[i] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), strValue);
                            }
                            continue;
                        }
                        for (int s = 0; s < arrfiledname.Length; s++)//获取界面控件值
                        {
                            if (ds.Tables[0].Rows[i]["FIELDNAME"].ToString() == arrfiledname[s])
                            {
                                strValue = arrfiledvalue[s];
                                f = false;
                                break;
                            }
                        }
                        if (extfiledname != null)//获取自定义默认参数值
                            for (int m = 0; m < extfiledname.Length; m++)
                            {
                                if (extfiledname[m] == ds.Tables[0].Rows[i]["FIELDNAME"].ToString())
                                {
                                    strValue = extvalue[m];
                                    f = false;
                                    break;
                                }
                            }
                        if (f)//获取系统默认参数值
                        {
                            if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString().Trim() == "RMYH_DATE")//默认值为系统时间
                            {
                                strValue = DateTime.Now.ToString(ds.Tables[0].Rows[i]["FORMATDISP"].ToString().Trim() == "" ? "yyyy-MM-dd HH:mm:ss" : ds.Tables[0].Rows[i]["FORMATDISP"].ToString().ToLower().Replace("mm", "MM"));
                            }
                            else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString().Trim() == "RMYH_USERDM")//默认值为当前登录用户
                                strValue = HttpContext.Current.Session["USERDM"].ToString();
                            else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString().Trim() == "RMYH_DEPTDM")
                                strValue = HttpContext.Current.Session["DEPTDM"].ToString();
                            else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString().Trim() == "RMYH_WORKDEPTDM")//默认值为当前登录工作部门
                                strValue = HttpContext.Current.Session["WORK_DEPT"].ToString();
                            else
                                strValue = ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString();
                        }
                        //判断当前数据的类型0代表数字；1代表字符串
                        if (ds.Tables[0].Rows[i]["SJLX"].ToString().Trim() == "0")
                        {
                            parameters[i] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), decimal.Parse(strValue));
                            parameters[i].AseDbType = AseDbType.Numeric;
                        }
                        else
                        {
                            parameters[i] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), strValue);
                        }
                        //parameters[i] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), strValue);
                    }
                }
                else//当前记录为修改
                {
                    strSql.Append("update ");
                    strSql.Append(tablename);
                    strSql.Append(" set ");
                    int intlength = 0;
                    for (int i = 0; i < arrfiledname.Length; i++)
                    {
                        if (ds.Tables[0].Select("FIELDNAME='" + arrfiledname[i] + "'").Length > 0)
                        {
                            strSql.Append(arrfiledname[i] + "=@" + arrfiledname[i] + ",");
                            intlength++;
                        }
                    }
                    strSql.Remove(strSql.Length - 1, 1);
                    //判断主键值的数据类型
                    if (GetDataType(xmdm, id) == "0")
                    {
                        strSql.Append(" where " + id + "=" + int.Parse(idvalue[k]));
                    }
                    else
                    {
                        strSql.Append(" where " + id + "='" + idvalue[k] + "'");
                    }

                    parameters = new AseParameter[intlength];
                    int m = 0;
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = m; j < arrfiledname.Length; j++)
                        {
                            if (ds.Tables[0].Select("FIELDNAME='" + arrfiledname[j] + "'").Length > 0)
                            {
                                //判断当前列的数据类型
                                if (GetDataType(xmdm, arrfiledname[j]) == "0")
                                {
                                    parameters[i] = new AseParameter("@" + arrfiledname[j], decimal.Parse(arrfiledvalue[j]));
                                    parameters[i].AseDbType = AseDbType.Numeric;
                                }
                                else
                                {
                                    parameters[i] = new AseParameter("@" + arrfiledname[j], arrfiledvalue[j]);
                                }
                                m++;
                                break;
                            }
                            m++;
                        }
                    }
                }
                arrSQLString[k] = strSql.ToString();
                arrcmdParms.Add(parameters);

            }
            int rows = DbHelperOra.ExecuteSql(arrSQLString, arrcmdParms);
            return rows.ToString();
        }
        /// <summary>
        /// 根据模块代码，获取某列的数据类型
        /// </summary>
        /// <param name="MKDM">模块代码</param>
        /// <param name="ColumnName">列名称</param>
        /// <returns></returns>
        public string GetDataType(string MKDM, string ColumnName)
        {
            DataSet DS = new DataSet();
            DS = Query("SELECT SJLX FROM TB_ZDSXB WHERE MKDM='" + MKDM + "' AND FIELDNAME='" + ColumnName + "'");
            return DS.Tables[0].Rows.Count > 0 ? DS.Tables[0].Rows[0]["SJLX"].ToString().Trim() : "";
        }
        /// <summary>
        ///  取得种子值
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string GetSeed(string TableName, string Key)
        {
            string strsql = "select FILEVALUE from RMYH_SEED where TABLENAME='" + TableName + "' AND FILEDNAME='" + Key + "'";
            DataSet ds = DbHelperOra.Query(strsql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                strsql = "update RMYH_SEED set FILEVALUE='" + (int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1).ToString() + "'  where TABLENAME='" + TableName + "' AND FILEDNAME='" + Key + "'";
                DbHelperOra.ExecuteSql(strsql);
                return (int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1).ToString();
            }
            else
            {
                strsql = "insert into  RMYH_SEED(TABLENAME,FILEDNAME,FILEVALUE) values('" + TableName + "','" + Key + "','1')";
                DbHelperOra.ExecuteSql(strsql);
                return "1";
            }
        }
        /// <summary>
        /// 返回自动生成的字符串主键值
        /// </summary>
        /// <returns></returns>
        public string GetStringPrimaryKey()
        {
            //定义临时变量、主键变量
            string PrimaryKey = "", BM = "";
            using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
            {
                DataSet DS = new DataSet();
                using (AseCommand cmd = new AseCommand())
                {
                    try
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        AseTransaction tx = connection.BeginTransaction();
                        cmd.Transaction = tx;
                        try
                        {
                            //查询当前最大的代码值
                            cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='16'";
                            AseDataAdapter command = new AseDataAdapter(cmd);
                            //使用DataSet前，先清除所有的Tables
                            DS.Tables.Clear();
                            command.Fill(DS, "ds");
                            if (DS.Tables[0].Rows.Count > 0)
                            {
                                BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                                //获取当前代码的长度
                                int Len = BM.Length;
                                //获取当前最大的代码，然后+1
                                BM = (int.Parse(BM) + 1).ToString().Trim();
                                //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                                BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                                cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='16'";
                                //执行更新代码操作
                                int Flag = cmd.ExecuteNonQuery();
                                //提交事务
                                tx.Commit();
                                if (Flag > 0)
                                {
                                    PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                                }
                            }
                        }
                        catch (Sybase.Data.AseClient.AseException E)
                        {
                            tx.Rollback();
                            throw new Exception(E.Message);
                        }
                        finally
                        {
                            cmd.Dispose();
                            connection.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            return PrimaryKey;
        }
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="tablename">需要添加数据的表名</param>
        /// <param name="XMDM">项目代码</param>
        /// <param name="IndexFile">表实际主键，一般为ID</param>
        /// <param name="IndexValue">主键值</param>
        /// <param name="IsTheChild">当前是否添加子节点</param>
        /// <param name="IsChild">是否多层树状结构</param>
        /// <returns></returns>
        public bool AddData(string tablename, string XMDM, string IndexFile, string IndexValue, bool IsChild, bool IsTheChild, string[] strfiled, string[] strvalue, string BM)
        {
            DataSet ds = GetList(string.Format(" MKDM='{0}' and TABLENAME='{1}'  and  DEFAULTVALUE is not null", XMDM, tablename));
            StringBuilder strSql = new StringBuilder();
            int intchild = 1;//层次结构的数据需要额外添加参数
            strSql.Append("insert into ");
            strSql.Append(tablename);
            strSql.Append("(" + IndexFile + "");
            for (int i = 0; i < strfiled.Length; i++)
            {
                strSql.Append("," + strfiled[i]);
            }
            intchild = intchild + strfiled.Length;
            if (IsChild)
            {
                strSql.Append(",PAR");
                strSql.Append(",CCJB");
                intchild = intchild + 2;

            }

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                strSql.Append("," + ds.Tables[0].Rows[i]["FIELDNAME"].ToString());
            }
            if (BM != "")
            {
                strSql.Append("," + BM);
            }
            strSql.Append(") values(@" + IndexFile);
            for (int i = 0; i < strfiled.Length; i++)
            {
                strSql.Append(",@" + strfiled[i]);
            }
            if (IsChild)
            {
                strSql.Append(",@PAR");
                strSql.Append(",@CCJB");
            }
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                strSql.Append(",@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString());
            }
            string indexid = GetSeed(tablename, IndexFile);
            if (BM != "")
            {
                if (IndexValue == "")
                    strSql.Append(",'" + indexid + "'");
                else
                    strSql.Append(",'" + getBM(tablename, IsTheChild, IndexValue, IndexFile, BM, indexid) + "'");
            }
            strSql.Append(")");

            AseParameter[] parameters = new AseParameter[ds.Tables[0].Rows.Count + intchild];
            parameters[0] = new AseParameter("@" + IndexFile, indexid);
            for (int i = 0; i < strfiled.Length; i++)
            {
                parameters[i + 1] = new AseParameter(":" + strfiled[i], strvalue[i]);
            }
            if (IsChild)
            {
                if (IndexValue == "")//
                {
                    parameters[1 + strfiled.Length] = new AseParameter("@PAR", "0");
                    parameters[2 + strfiled.Length] = new AseParameter("@CCJB", "1");
                }
                else
                {
                    string[] NBBMPIR = getNBBM(tablename, IsTheChild, IndexValue, IndexFile);
                    parameters[1 + strfiled.Length] = new AseParameter("@PAR", NBBMPIR[0]);
                    parameters[2 + strfiled.Length] = new AseParameter("@CCJB", NBBMPIR[1]);
                }
            }
            for (int i = 0; i < parameters.Length - intchild; i++)
            {
                if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString() == "RMYH_DATE")//默认值为系统时间 
                {
                    parameters[i + intchild] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), DateTime.Now.ToString(ds.Tables[0].Rows[i]["FORMATDISP"].ToString() == "" ? "yyyy-MM-dd HH:mm:ss" : ds.Tables[0].Rows[i]["FORMATDISP"].ToString().ToLower().Replace("mm", "MM")));
                }
                else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString() == "RMYH_USERDM")//默认值为当前登录用户
                    parameters[i + intchild] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), HttpContext.Current.Session["USERDM"].ToString());
                else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString() == "RMYH_DEPTDM")//默认值为当前登录用户部门
                    parameters[i + intchild] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), HttpContext.Current.Session["DEPTDM"].ToString());
                else if (ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString() == "RMYH_WORKDEPTDM")//默认值为当前登录工作部门
                    parameters[i + intchild] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), HttpContext.Current.Session["WORK_DEPT"].ToString());
                else
                    parameters[i + intchild] = new AseParameter("@" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString(), ds.Tables[0].Rows[i]["DEFAULTVALUE"].ToString());
            }
            int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);

            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tablename"></param>
        /// <param name="NBBMFiled"></param>
        /// <param name="IsTheChild"></param>
        /// <param name="IndexValue"></param>
        /// <param name="IndexFile"></param>
        /// <returns>数组中存放的数据依次为NBBMFiled、PARID、CCJB</returns>
        private string getBM(string tablename, bool IsTheChild, string IndexValue, string IndexFile, string BM, string BMValues)
        {
            string rtn = "";
            DataSet ds = DbHelperOra.Query("SELECT " + IndexFile + ",PAR," + BM + ",CCJB FROM " + tablename + " WHERE  " + IndexFile + "='" + IndexValue + "'");
            string strNBBMFiled = "";//ds.Tables[0].Rows[0][NBBMFiled].ToString();
            //string strYJDBZ = ds.Tables[0].Rows[0]["YJDBZ"].ToString();
            if (!IsTheChild)
            {
                if (ds.Tables[0].Rows[0]["CCJB"].ToString() == "1")//
                {
                    rtn = BMValues;
                }
                else
                {
                    ds = DbHelperOra.Query("SELECT MAX(" + BM + ") FROM " + tablename + " WHERE  PAR='" + ds.Tables[0].Rows[0]["PAR"].ToString() + "'");
                    strNBBMFiled = ds.Tables[0].Rows[0][0].ToString();
                    if (strNBBMFiled.Length > 4)
                        rtn = strNBBMFiled.Substring(0, strNBBMFiled.Length - 4) + (int.Parse(strNBBMFiled.Substring(strNBBMFiled.Length - 4)) + 1).ToString().PadLeft(4, '0');
                    else
                        rtn = (int.Parse(strNBBMFiled) + 1).ToString().PadLeft(4, '0');
                }
            }
            else
            {
                rtn = ds.Tables[0].Rows[0][BM].ToString();
                ds = DbHelperOra.Query("SELECT MAX(" + BM + ")  FROM " + tablename + " WHERE PAR='" + ds.Tables[0].Rows[0][IndexFile].ToString() + "'");
                if (ds.Tables[0].Rows[0][0].ToString() != "")
                {
                    strNBBMFiled = ds.Tables[0].Rows[0][0].ToString();
                    if (strNBBMFiled.Length > 4)
                        rtn = strNBBMFiled.Substring(0, strNBBMFiled.Length - 4) + (int.Parse(strNBBMFiled.Substring(strNBBMFiled.Length - 4)) + 1).ToString().PadLeft(4, '0');
                    else
                        rtn = (int.Parse(strNBBMFiled) + 1).ToString().PadLeft(4, '0');
                }
                else
                {
                    rtn += "0001";
                }
            }
            return rtn;
        }
        /// <summary>
        ///  删除数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="DM">主键值，唯一ID</param>
        /// <param name="filedname">主键字段名</param>
        /// <param name="YY">当前登录核算年度</param>
        /// <returns>返回结果，第一位表示成功与否，1表示成功，0表示失败，0后面的字符串表示失败的原因</returns>
        public string DeleteData(string tablename, string DM, string filedname)
        {
            string retVal = "A";
            try
            {
                //查询和TableName相关联的外键表的信息
                DataSet ds = DbHelperOra.Query("SELECT TABLENAME,FKTABLENAME,FKZD,WJZD,INFORMATION,SFKDEL,SFDELYY,TORDER FROM TB_DELETE WHERE TABLENAME='" + tablename + "' ORDER BY TORDER ");
                DataSet dscheck = new DataSet();
                string strSQL = "";
                string deleteSQL = "";
                string isnullwhere = "";
                string CXTJ = "";
                ArrayList arrdelete = new ArrayList();
                string[] strarrFiled;
                //主键值
                string[] arrDM = DM.Split(',');
                for (int ii = 0; ii < arrDM.Length; ii++)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        isnullwhere = "";
                        strSQL = "";
                        CXTJ = "";
                        //外键表字段（第一个逗号之前是主键，其余的都是条件）
                        strarrFiled = ds.Tables[0].Rows[i]["WJZD"].ToString().Split(',');
                        //是否可直接删除，0代表不能直接删除；如果没有数据的话，可直接删除；1代表可直接删除
                        if (ds.Tables[0].Rows[i]["SFKDEL"].ToString() == "0")
                        {
                            //关联主表和外键表
                            strSQL += " AND T1." + ds.Tables[0].Rows[i]["FKZD"].ToString() + "=T2." + strarrFiled[0];

                            for (int m = 1; m < strarrFiled.Length; m++)
                            {
                                //判断当前字段不能为空
                                if (strarrFiled[m].ToString() != "" && strarrFiled[m].ToString() != null)
                                {
                                    //判断当前条件，是不是联合条件（联合条件用+连接)
                                    if (strarrFiled[m].ToString().IndexOf('+') > -1)
                                    {
                                        CXTJ = strarrFiled[m];
                                    }
                                    else
                                    {
                                        //非空字段
                                        isnullwhere += " AND " + strarrFiled[m] + "<>'0'";
                                    }
                                }
                            }
                            if (CXTJ != "")
                            {
                                //删除联合条件相加等于0
                                strSQL = "SELECT " + CXTJ + " FROM " + tablename + " T1," + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + " T2 WHERE T1." + filedname + " ='" + arrDM[ii] + "' " + strSQL;
                                dscheck = DbHelperOra.Query(strSQL);
                                if (dscheck.Tables[0].Rows.Count > 0)
                                {
                                    if (dscheck.Tables[0].Rows[0][0].ToString() != "0")
                                    {
                                        retVal = "0" + ds.Tables[0].Rows[i]["INFORMATION"].ToString();
                                        break;
                                    }
                                    else
                                    {
                                        deleteSQL = "DELETE FROM " + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + " WHERE  " + strarrFiled[0] + "='" + arrDM[ii] + "' ";
                                        //删除时，是否带年条件
                                        if (ds.Tables[0].Rows[i]["SFDELYY"].ToString() == "1")
                                        {
                                            deleteSQL += " AND YY='" + HttpContext.Current.Session["YY"].ToString() + "'";
                                        }
                                        arrdelete.Add(deleteSQL);
                                    }
                                }
                            }
                            else
                            {
                                strSQL = "SELECT * FROM " + tablename + " T1," + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + " T2 WHERE T1." + filedname + " ='" + arrDM[ii] + "' " + strSQL;
                                dscheck = DbHelperOra.Query(strSQL);
                                if (dscheck.Tables[0].Select("1=1 " + isnullwhere).Length > 0)
                                {
                                    retVal = "0" + ds.Tables[0].Rows[i]["INFORMATION"].ToString();
                                    break;
                                }
                                else if (dscheck.Tables[0].Rows.Count > 0)
                                {
                                    deleteSQL = "DELETE FROM " + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + " WHERE  " + strarrFiled[0] + "='" + arrDM[ii] + "' ";

                                    //删除时，是否带年条件
                                    if (ds.Tables[0].Rows[i]["SFDELYY"].ToString() == "1")
                                    {
                                        deleteSQL += " AND YY='" + HttpContext.Current.Session["YY"].ToString() + "'";
                                    }
                                    arrdelete.Add(deleteSQL);
                                }
                            }
                        }
                        else
                        {
                            //等于1代表可以直接删除
                            strSQL = "SELECT * FROM " + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + "  WHERE " + strarrFiled[0] + " ='" + arrDM[ii] + "'";
                            dscheck = DbHelperOra.Query(strSQL);
                            if (dscheck.Tables[0].Rows.Count > 0)
                            {
                                deleteSQL = "DELETE FROM " + ds.Tables[0].Rows[i]["FKTABLENAME"].ToString() + " WHERE " + strarrFiled[0] + "='" + arrDM[ii] + "'";
                                //删除时，是否带年条件
                                if (ds.Tables[0].Rows[i]["SFDELYY"].ToString() == "1")
                                {
                                    deleteSQL += " AND YY='" + HttpContext.Current.Session["YY"].ToString() + "'";
                                }
                                arrdelete.Add(deleteSQL);
                            }
                        }
                    }
                }
                //如果retVal==A表示，是可以删除的，如果不等于A，表示不可删除，并携带不可删除的提示信息
                if (retVal == "A")
                {
                    try
                    {
                        arrdelete.Add("DELETE FROM " + tablename + " WHERE " + filedname + " in ('" + DM.Replace(",", "','") + "')");
                        DbHelperOra.ExecuteSqlTran(arrdelete);
                        retVal = "1";
                    }
                    catch (Exception e)
                    {
                        retVal = "0" + e.Message;
                    }
                }
            }
            catch (Exception exp)
            {
                retVal = "0" + exp.Message;
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tablename"></param>
        /// <param name="NBBMFiled"></param>
        /// <param name="IsTheChild"></param>
        /// <param name="IndexValue"></param>
        /// <param name="IndexFile"></param>
        /// <returns>数组中存放的数据依次为NBBMFiled、PARID、CCJB</returns>
        private string[] getNBBM(string tablename, bool IsTheChild, string IndexValue, string IndexFile)
        {
            string[] rtn = new string[2];
            DataSet ds = DbHelperOra.Query("SELECT " + IndexFile + " ,PAR,CCJB FROM " + tablename + " WHERE  " + IndexFile + "='" + IndexValue + "'");
            if (!IsTheChild)
            {
                rtn[0] = ds.Tables[0].Rows[0]["PAR"].ToString();
                rtn[1] = ds.Tables[0].Rows[0]["CCJB"].ToString();
            }
            else
            {
                rtn[0] = IndexValue;
                rtn[1] = (int.Parse(ds.Tables[0].Rows[0]["CCJB"].ToString()) + 1).ToString();
            }
            return rtn;
        }
        /// <summary>
        /// 系统登录
        /// </summary>
        /// <param name="uid">登录名</param>
        /// <param name="pwd">密码</param>
        /// <param name="Year">年份</param>
        /// <returns></returns>
        public string toLogin(string uid, string pwd, string Year)
        {
            string bolrtn = "false";
            DataSet ds = null;
            try
            {
                ds = Query("SELECT USER_ID,CC_NO,USER_PSW,USER_NAME FROM TB_USER WHERE USER_LOGIN='" + uid + "'");

                if (ds != null && ds.Tables[0].Rows.Count == 1)
                {
                    if (pwd.Length != 32)
                        pwd = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "MD5");
                    if (System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(ds.Tables[0].Rows[0]["USER_PSW"].ToString(), "MD5") == pwd)
                    {
                        if (ds.Tables[0].Rows[0]["USER_ID"].ToString() != "SYS")
                        {
                            if (Query("SELECT * FROM TB_HSZXZD WHERE HSZXDM='" + ds.Tables[0].Rows[0]["CC_NO"].ToString() + "' AND YY='" + Year + "'").Tables[0].Rows.Count <= 0)
                            {
                                bolrtn = "未查到该用户所属的核算中心，请做核算中心年复制！";
                            }
                            else
                            {
                                HttpContext.Current.Session["USERDM"] = ds.Tables[0].Rows[0]["USER_ID"].ToString();
                                HttpContext.Current.Session["PSW"] = ds.Tables[0].Rows[0]["USER_PSW"].ToString();
                                HttpContext.Current.Session["HSZXDM"] = ds.Tables[0].Rows[0]["CC_NO"].ToString();
                                HttpContext.Current.Session["USERNAME"] = ds.Tables[0].Rows[0]["USER_NAME"].ToString();
                                HttpContext.Current.Session["YY"] = Year;
                                bolrtn = "true";
                            }
                        }
                        else
                        {
                            HttpContext.Current.Session["USERDM"] = ds.Tables[0].Rows[0]["USER_ID"].ToString();
                            HttpContext.Current.Session["PSW"] = ds.Tables[0].Rows[0]["USER_PSW"].ToString();
                            HttpContext.Current.Session["HSZXDM"] = ds.Tables[0].Rows[0]["CC_NO"].ToString();
                            HttpContext.Current.Session["USERNAME"] = ds.Tables[0].Rows[0]["USER_NAME"].ToString();
                            HttpContext.Current.Session["YY"] = Year;
                            bolrtn = "true";
                        }
                    }
                    else
                    {
                        bolrtn = "密码错误！";
                    }
                }
                else
                {
                    bolrtn = "该用户不存在！";
                }
            }
            catch (Exception E)
            {
                throw new Exception(E.Message);
            }
            return bolrtn;

        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldpwd">旧密码</param>
        /// <param name="newpwd">新密码</param>
        /// <returns></returns>
        public int ChangePWD(string oldpwd, string newpwd)
        {
            try
            {
                return DbHelperOra.ExecuteSql("update TB_USER set USER_PSW='" + newpwd + "' where USER_ID='" + HttpContext.Current.Session["USERDM"].ToString() + "' and USER_PSW='" + oldpwd + "' ");
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// 获取菜单权限列表
        /// </summary>
        public DataSet GetListMenu(string USERDM)
        {
            try
            {
                DataSet DS = new DataSet();
                StringBuilder strSql = new StringBuilder();
                StringBuilder Sql = new StringBuilder();
                if (USERDM == "SYS")
                {
                    strSql.Append("SELECT MENUID,MENUCAPTION,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU");
                    //strSql.Append(" WHERE MENUID IN('04','0401','0402','0403','0404','0405','0406','0407','0408') AND ACTIONRIGHT='BBFX'");
                    strSql.Append(" WHERE MENUID<>'04' AND ACTIONRIGHT='BBFX'");
                    DS = DbHelperOra.Query(strSql.ToString());
                }
                else
                {
                    strSql.Append("SELECT MENUID,MENUCAPTION,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM TB_BBUSERQX QX");
                    strSql.Append(" LEFT JOIN SYS_MENU S ON S.MENUID=QX.BBBM AND ACTIONRIGHT='BBFX'");
                    strSql.Append(" WHERE BZ='0' AND USER_ID='" + USERDM + "'");
                    DS = DbHelperOra.Query(strSql.ToString());
                    //如果用户权限为空则查询用户的组权限
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        Sql.Append("SELECT MENUID,MENUCAPTION,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM TB_BBGROUPQX QX");
                        Sql.Append(" LEFT JOIN SYS_MENU S ON S.MENUID=QX.BBBM AND ACTIONRIGHT='BBFX'");
                        Sql.Append(" WHERE GROUP_ID IN(SELECT GROUP_ID FROM TB_USERGROUP  WHERE USER_ID='" + USERDM + "') AND BZ='0'");
                        DS = DbHelperOra.Query(Sql.ToString());
                    }
                }
                return DS;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 获取菜单权限列表
        /// </summary>
        public DataSet GetAllMenuByUSERDM(string USERDM)
        {
            try
            {
                DataSet DS = new DataSet();
                StringBuilder strSql = new StringBuilder();
                StringBuilder Sql = new StringBuilder();
                strSql.Append("SELECT ID ID,MLMXMC NAME,MX.MLDM PAR,CONVERT(VARCHAR,CONVERT(INT,CCJB)+1) CCJB FROM BB_MLGLMX MX,BB_MLGL ML,TB_BBUSERQX U");
                strSql.Append(" WHERE ML.MLDM=MX.MLDM AND U.BBBM=MX.ID AND U.BZ='1' AND USER_ID='" + USERDM + "'");
                strSql.Append(" ORDER BY PAR");
                DS = DbHelperOra.Query(strSql.ToString());
                //如果用户权限为空则查询用户的组权限
                if (DS.Tables[0].Rows.Count == 0)
                {
                    Sql.Append("SELECT ID ID,MLMXMC NAME,MX.MLDM PAR,CONVERT(VARCHAR,CONVERT(INT,CCJB)+1) CCJB FROM BB_MLGLMX MX,BB_MLGL ML,TB_BBGROUPQX U");
                    Sql.Append(" WHERE ML.MLDM=MX.MLDM AND U.BBBM=MX.ID AND U.BZ='1' AND U.GROUP_ID IN(SELECT GROUP_ID FROM TB_USERGROUP  WHERE USER_ID='" + USERDM + "')");
                    Sql.Append(" ORDER BY PAR");
                    DS = DbHelperOra.Query(Sql.ToString());
                }
                return DS;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 获取用户自定义菜单
        /// </summary>
        /// <returns></returns>
        public DataSet GetCustomMenu()
        {
            string sql = "select b.PAGEDM,b.PAGEPATH,b.TITLE from RMYH_USERCUSTOM a left join RMYH_MENU b on a.PAGEDM=b.PAGEDM where a.USERDM ='" + HttpContext.Current.Session["USERDM"] + "'";
            return DbHelperOra.Query(sql);
        }
        /// <summary>
        /// 执行简单的查询语句
        /// </summary>
        /// <param name="tablename">查询表名</param>
        /// <param name="fileds">字段名，多个字段时以逗号隔开</param>
        /// <returns></returns>
        public DataSet getDataSet(string tablename, string fileds, string where)
        {
            string sql = "select " + fileds + " from " + tablename;
            if (where != "")
                sql += " where " + where;
            return DbHelperOra.Query(sql);
        }
        /// <summary>
        /// 批量执行SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <param name="cmdParms">条件字符串</param>
        /// <returns></returns>
        public int ExecuteSql(string[] sqlString, ArrayList cmdParms)
        {
            return DbHelperOra.ExecuteSql(sqlString, cmdParms);
        }
        /// <summary>
        /// 执行单条SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <returns></returns>
        public int ExecuteSql(string sqlString)
        {
            return DbHelperOra.ExecuteSql(sqlString);
        }
        /// <summary>
        /// 数据更新（带自定义行）
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="hFiled">行字段</param>
        /// <param name="lFiled">列字段</param>
        /// <param name="strValues">参数值，行记录以“,”隔开，数据之间以“|”隔开，数据间的顺序为 行代码|列代码|值</param>
        /// <param name="YYNNDD">时间字段，不允许为空</param>
        /// <returns></returns>
        public int UpdateHang(string tablename, string hFiled, string lFiled, string strValues, string YYNNDD)
        {
            try
            {
                string[] arrValues = strValues.Split(',');
                string[] arrSQL = new string[arrValues.Length * 2];
                for (int i = 0; i < arrValues.Length; i++)
                {
                    string[] arrV = arrValues[i].Split('|');
                    arrSQL[i * 2] = "delete " + tablename + " where " + hFiled + "='" + arrV[0] + "' and " + lFiled + "='" + arrV[1] + "' and YYNNDD='" + YYNNDD + "'";
                    arrSQL[i * 2 + 1] = "insert into " + tablename + "(" + hFiled + "," + lFiled + ",JE,YYNNDD,CREATETIME,CREATEUSER) values('" + arrV[0] + "','" + arrV[1] + "','" + arrV[2] + "','" + YYNNDD + "','" + DateTime.Now.ToString() + "','" + HttpContext.Current.Session["USERDM"].ToString() + "')";
                }
                return DbHelperOra.ExecuteSql(arrSQL, null);
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// 添加审批状态
        /// </summary>
        /// <param name="SPXMDM">待审批项目代码</param>
        /// <param name="XMDM">流程项目代码</param>
        /// <param name="zt">审批状态</param>
        /// <param name="SPYJ">审批意见</param>
        /// <returns>0：执行异常；1：执行成功；2：当前用户没有审批权利；3：此单据已经审批结束；</returns>
        public int addSH(string SPXMDM, string XMDM, int zt, string SPYJ)
        {
            int intRtn = 0;
            try
            {
                string sqlinsertTJ = "";
                string sqlupdate = "";
                string strdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string sqlinsert = "insert into RMYH_SPZT(SPZTDM,SPXMDM,XMDM,DQLCDM,NEXTLCDM,STATE,CREATETIME,CREATEUSER,SPSJ,SPR,SPYJ) ";
                //DataSet ds = DbHelperOra.Query("select b.SPXMDM,b.XMDM,b.DQLCDM,b.NEXTLCDM,b.STATE,b.CREATETIME,b.CREATEUSER,a.SPSM,b.LCDM,b.SPSM,B.XH,SPR FROM RMYH_SPXMLC A LEFT JOIN RMYH_SPZT B ON A.XMDM=B.XMDM AND B.SPXMDM='" + SPXMDM + "' WHERE A.XMDM='" + XMDM + "' ORDER BY B.CREATETIME,A.XH");
                DataSet ds = DbHelperOra.Query("select b.SPZTDM,b.SPXMDM,b.XMDM,b.DQLCDM,b.NEXTLCDM,b.STATE,b.CREATETIME,b.CREATEUSER,a.SPSM,a.LCDM,a.SPSM,a.XH,a.SPR FROM RMYH_SPXMLC A LEFT JOIN RMYH_SPZT B ON A.XMDM=B.XMDM AND B.SPXMDM='" + SPXMDM + "' and b.DQLCDM=a.LCDM WHERE A.XMDM='" + XMDM + "' ORDER BY B.CREATETIME,A.XH");
                DataRow[] dr = ds.Tables[0].Select("STATE='-1'");
                if (dr.Length == 1)//判断当前审批记录是否存在
                {
                    if (dr[0]["SPR"].ToString().Contains(HttpContext.Current.Session["USERDM"].ToString()))//判断当前审批人是否为登录人
                    {
                        if (zt == 0)//审批不同意
                        {
                            sqlupdate = "update RMYH_SPZT set STATE='" + zt + "',SPR='" + HttpContext.Current.Session["USERDM"].ToString() + "',SPSJ='" + strdate + "',SPYJ='" + SPYJ + "' where SPZTDM='" + dr[0]["SPZTDM"].ToString() + "'";
                            dr = ds.Tables[0].Select("XH='" + (int.Parse(dr[0]["XH"].ToString()) - 1) + "'");
                            sqlinsert += " values('" + GetSeed("RMYH_SPZT", "SPZTDM") + "','" + SPXMDM + "','" + XMDM + "'";//审批状态代码、审批项目代码和流程项目代码
                            sqlinsert += ",'" + dr[0]["DQLCDM"].ToString() + "'";//当前流程代码
                            sqlinsert += ",'" + dr[0]["NEXTLCDM"].ToString() + "'";//下一步流程代码
                            sqlinsert += ",'-1','" + strdate + "','" + HttpContext.Current.Session["USERDM"].ToString() + "','','','')";//提交状态，提交时间，提交人
                        }
                        else if (zt == 1)//审批同意
                        {
                            sqlupdate = "update RMYH_SPZT set STATE='" + zt + "',SPR='" + HttpContext.Current.Session["USERDM"].ToString() + "',SPSJ='" + strdate + "',SPYJ='" + SPYJ + "' where SPZTDM='" + dr[0]["SPZTDM"].ToString() + "'";
                            if (dr[0]["NEXTLCDM"].ToString() == "0")//判断是否为最后一审批环节,若非最后一审批环节，记录下环节审批信息
                            {
                                sqlinsert = "";
                            }
                            else
                            {
                                sqlinsert += " values('" + GetSeed("RMYH_SPZT", "SPZTDM") + "','" + SPXMDM + "','" + XMDM + "'";//审批状态代码、审批项目代码和流程项目代码
                                sqlinsert += ",'" + dr[0]["NEXTLCDM"].ToString() + "'";//当前流程代码
                                DataRow[] drlast = ds.Tables[0].Select("XH='" + (int.Parse(dr[0]["XH"].ToString()) + 2) + "'");
                                sqlinsert += ",'" + (drlast.Length > 0 ? drlast[0]["LCDM"].ToString() : "0") + "'";//下一步流程代码
                                sqlinsert += ",'-1','" + strdate + "','" + HttpContext.Current.Session["USERDM"].ToString() + "','','','')";//提交状态，提交时间，提交人
                            }
                        }
                    }
                    else
                    {
                        intRtn = 2;
                        sqlinsert = "";
                        //登录用户无审批权利
                    }
                }
                else
                {
                    dr = ds.Tables[0].Select("STATE<>'-1'");//判断单据是否结束
                    if (dr.Length > 0)
                    {
                        intRtn = 3;
                        // 此单据已审批结束！
                    }
                    else//填报人提交
                    {
                        sqlinsertTJ = sqlinsert + " values('" + GetSeed("RMYH_SPZT", "SPZTDM") + "','" + SPXMDM + "','" + XMDM + "'";//审批状态代码、审批项目代码和流程项目代码
                        sqlinsertTJ += ",'" + ds.Tables[0].Select("XH='1'")[0]["LCDM"].ToString() + "'";//当前流程代码
                        sqlinsertTJ += ",'" + ds.Tables[0].Select("XH='2'")[0]["LCDM"].ToString() + "'";//下一步流程代码
                        sqlinsertTJ += ",'1','" + strdate + "','" + HttpContext.Current.Session["USERDM"].ToString() + "','" + strdate + "','" + HttpContext.Current.Session["USERDM"].ToString() + "','填报提交')";//提交状态，提交时间，提交人

                        sqlinsert += " values('" + GetSeed("RMYH_SPZT", "SPZTDM") + "','" + SPXMDM + "','" + XMDM + "'";//审批状态代码、审批项目代码和流程项目代码
                        sqlinsert += ",'" + ds.Tables[0].Select("XH='2'")[0]["LCDM"].ToString() + "'";//当前流程代码
                        if (ds.Tables[0].Select("XH='3'").Length > 0)
                            sqlinsert += ",'" + ds.Tables[0].Select("XH='3'")[0]["LCDM"].ToString() + "'";//下一步流程代码
                        else
                            sqlinsert += ",'0'";
                        sqlinsert += ",'-1','" + strdate + "','" + HttpContext.Current.Session["USERDM"].ToString() + "','','','')";//提交状态，提交时间，提交人
                    }
                }
                if (sqlupdate != "" && sqlinsert != "")
                {
                    DbHelperOra.ExecuteSql(new string[] { sqlupdate, sqlinsert }, null);
                }
                else if (sqlupdate != "")
                {
                    DbHelperOra.ExecuteSql(sqlupdate);
                }
                else if (sqlinsert != "")
                {
                    DbHelperOra.ExecuteSql(new string[] { sqlinsertTJ, sqlinsert }, null);
                }
                intRtn = 1;
            }
            catch
            {

            }
            return intRtn;
        }

        /// <summary>
        /// 删除之前因异常，产生的数据
        /// </summary>
        /// <param name="ArrList">SQL集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级代码</param>
        public void DelErrorData(ref List<string> ArrList, ref ArrayList List, string MBDM, string JHFADM, string JSDM, string CJBM)
        {
            string SQL = " DELETE FROM TB_MBLCSB WHERE DQMBDM=@MBDM AND JSDM='" + JSDM.Replace("'", "''") + "' AND JHFADM=@JHFADM AND CJBM=@CJBM AND STATE=-1";
            ArrList.Add(SQL);
            AseParameter[] Del = { 
                    new AseParameter("@MBDM", AseDbType.VarChar, 40),
                    new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                    new AseParameter("@CJBM", AseDbType.VarChar, 40)
                };
            Del[0].Value = MBDM;
            Del[1].Value = JHFADM;
            Del[2].Value = CJBM;
            List.Add(Del);
        }
        /// <summary>
        /// 模板数据上报
        /// </summary>
        /// <param name="ZT">1 填报上报;2 审批;3 打回</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>Flag大于1，表示执行成功；Flag等于0，表示的执行不成功；Flag等于-1，表示没有权限；Flag等于-2，表示流程没有发起;Flag等于-3表示数据有重复;Flag等于-4表示模板作为其他模板的子节点时角色对应不一致</returns>
        public int ZTTJ(int ZT, string MBDM, string JHFADM, string DHMBDM, string MBLB, string DHYY, string SPYJ, string ISZLC)
        {
            string YSLX = "", CJBM = "";
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            //定义存储参数的集合对象
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            //返回当前上报执行结果
            int Flag = 0;
            //获取该方案是否走流程信息：
            DT.Tables.Clear();
            //DS = getDataSet("TB_JHFA", "ISNULL(SCBS,0) ISZLC", "JHFADM='" + JHFADM + "'");
            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    ISZLC = DS.Tables[0].Rows[0]["ISZLC"].ToString().Trim();
            //}
            //如果是不走流程的则在上报的时候更新状态：
            if (ISZLC.Equals("0") && ZT == 1)
            {
                UpateTBState(JHFADM, MBDM, ref ArrList, ref List, "1");
                return ExecuteSql(ArrList.ToArray(), List);
            }
            //清除DS中的所有表
            DT.Tables.Clear();
            //获取预算类型
            //string MBLX = GetYSLX(MBLB);
            //获取预算流程发起相关的信息
            DS = getDataSet("TB_YSLCFS", "YSLX,ISFQ", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count <= 0)
            {
                //表示流程没有发起
                Flag = -2;
            }
            else
            {
                if (DS.Tables[0].Rows[0]["ISFQ"].ToString().Trim() == "0")
                {
                    //表示流程没有发起
                    Flag = -2;
                }
                else
                {
                    YSLX = DS.Tables[0].Rows[0]["YSLX"].ToString().Trim();
                    //CZLX为1时，表示为上报
                    if (ZT == 1)
                    {
                        //清空之前的所有虚表
                        DS.Tables.Clear();
                        //模板提交
                        DS = Query("SELECT MBDM,QZMBDM,LX,LC.JSDM,CJBM FROM TB_YSMBLC LC,TB_JSYH YH WHERE QZMBDM='" + MBDM + "' AND LX='0' AND MBLX='" + YSLX + "' AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'");
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            //定义输出参数
                            string JSDM = "", BM;
                            //定义防重复提交标记
                            bool IsExist = false, IsJSError = false;
                            //获取层级编码
                            CJBM = DS.Tables[0].Rows[0]["CJBM"].ToString();
                            if (getDataSet("TB_MBLCSB", "JSDM,CJBM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND CJBM='" + CJBM + "'").Tables[0].Rows.Count > 0)
                            {
                                //表示打回后，重新上报
                                UpateAndInsertMB(JHFADM, MBDM, ref ArrList, ref List, ZT, out JSDM, out BM, MBLB,SPYJ);
                            }
                            else
                            {
                                //判断当前提交的数据数据是否已经存在（防重复提交）
                                if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND JSDM='" + DS.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + CJBM + "'").Tables[0].Rows.Count > 0)
                                {
                                    //表示已经提交，不能重复提交
                                    //Flag = -3;
                                    //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入 （单林海的需求 2016.08.24）！
                                    DelErrorData(ref ArrList, ref List, MBDM, JHFADM, DS.Tables[0].Rows[0]["JSDM"].ToString(), CJBM);
                                }
                                //提交模板数据
                                SCCurrentTB(ref ArrList, ref List, MBDM, DS.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM, CJBM,SPYJ);

                            }
                            if (JSDM == "-1")
                            {
                                Flag = -1;
                            }
                            else
                            {
                                //查询以当前模板的前置模板的模板
                                DT = GetQZMBByDM(CJBM, DS.Tables[0].Rows[0]["JSDM"].ToString(), MBDM, YSLX);
                                //如果以当前模板为前置模板的模板都已审批完成，则还生成下级待审批的记录
                                SCNextTB(DT, ref ArrList, ref List, MBDM, JHFADM, DS.Tables[0].Rows[0]["JSDM"].ToString(), YSLX, CJBM, ref IsExist, ref IsJSError);
                                if (IsExist == true)
                                {
                                    Flag = -3;
                                }
                                else
                                {
                                    if (IsJSError == false)
                                    {
                                        //如果是走流程并且没审批的上报时更新状态：
                                        if (ISZLC.Equals("1") && IsNoSp(MBDM))
                                        {
                                            UpateTBState(JHFADM, MBDM, ref ArrList, ref List, "1");
                                        }
                                    }
                                    else
                                    {
                                        //表明模板的角色对应有误
                                        Flag = -4;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //表示没有填报的权限
                            Flag = -1;
                        }
                    }
                    else if (ZT == 2 || ZT == 3)     // 审批或者打回当前模板操作
                    {
                        //定义输出参数
                        string JSDM, BM;
                        //更新审批或者打回信息
                        UpateAndInsertMB(JHFADM, MBDM, ref ArrList, ref List, ZT, out JSDM, out BM, MBLB,SPYJ);
                        if (JSDM == "-1" || JSDM == "")
                        {
                            //表示当前操作人没有权限
                            Flag = -1;
                        }
                        else
                        {
                            if (ZT == 2)
                            {
                                bool IsExist = false, IsJSError = false;
                                //查询以当前模板的前置模板的模板
                                DT = GetQZMBByDM(BM, JSDM, MBDM, YSLX);
                                //提交当前模板（如果以当前模板为前置模板的模板都已审批完成，则还生成下级待审批的记录）
                                SCNextTB(DT, ref ArrList, ref List, MBDM, JHFADM, JSDM, YSLX, BM, ref IsExist, ref IsJSError);
                                if (IsExist == true)
                                {
                                    //表示当前提交数据已经存在，不能重复提交数据
                                    Flag = -3;
                                }
                                else
                                {
                                    if (IsJSError == true)
                                    {
                                        //表示当前模板作为别的模板的子节点时角色代码对应不一致
                                        Flag = -4;
                                    }
                                    else
                                    {
                                        //如果是走流程的则在审批的时候更新状态：
                                        if (ISZLC.Equals("1") && (!IsNoSp(MBDM)))
                                        {
                                            UpateTBState(JHFADM, MBDM, ref ArrList, ref List, "1");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bool IsExist = false, IsJSError = false;
                                //执行打回操作
                                SCDHMB(ref ArrList, ref List, DHMBDM, JHFADM, YSLX, MBDM, JSDM, ref IsExist, ref IsJSError,DHYY);
                                if (IsExist == true)
                                {
                                    //表示当前提交数据已经存在，不能重复提交数据
                                    Flag = -3;
                                }
                                else
                                {
                                    if (IsJSError == true)
                                    {
                                        //表示此模板作为别的模板的子节点时，角色代码没有对应错误
                                        Flag = -4;
                                    }
                                    else
                                    {
                                        //如果是打回时候则更新状态为无效：
                                        UpateTBState(JHFADM, MBDM, ref ArrList, ref List, "0");
                                    }
                                }
                            }

                        }
                    }
                    //等于-1表示没有权限;等于-3代表有重复数据
                    if (Flag != -1 && Flag != -3 && Flag != -4)
                    {
                        Flag = ExecuteSql(ArrList.ToArray(), List);
                    }
                }
            }
            return Flag;
        }
        /// <summary>
        /// 根据角色代码获取用户代码
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetUSERDMByJSDM(string JSDM)
        {
            string USERDM = "";
            DataSet DS = new DataSet();
            if (JSDM != "")
            {
                DS = Query("SELECT USERID FROM TB_JSYH WHERE JSDM IN(" + JSDM + ")");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {

                        if (USERDM == "")
                        {
                            USERDM = DS.Tables[0].Rows[i]["USERID"].ToString();
                        }
                        else
                        {
                            USERDM = USERDM + "','" + DS.Tables[0].Rows[i]["USERID"].ToString();
                        }
                    }
                }
            }
            return USERDM != "" ? "'" + USERDM + "'" : USERDM;
        }
        /// <summary>
        /// 根据角色代码获取用户代码
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetJSDMByUSERDM(string JSDM)
        {
            string DM = "";
            DataSet DS = new DataSet();
            if (JSDM != "")
            {
                JSDM = "'" + JSDM.Replace(",", "','") + "'";
                DS = Query("SELECT JSDM FROM TB_JSYH WHERE JSDM IN(" + JSDM + ") AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "'");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DM = DS.Tables[0].Rows[0]["JSDM"].ToString().Trim();
                }
            }
            return DM;
        }
        /// <summary>
        ///  查询以当前模板为前置模板的模板的所有子节点（不包括当前模板）
        /// </summary>
        /// <param name="QZMBDM">前置模板代码</param>
        /// <param name="YSLX">预算类型</param>
        /// <param name="ZT">状态</param>
        /// <returns></returns>
        public DataSet GetQZMBByDM(string CJBM, string JSDM, string MBDM, string YSLX)
        {
            string SQL = "";
            if (CJBM.Length == 4)
            {
                JSDM = JSDM.Replace("'", "''");
                //查询和当前模板相同模板代码和角色代码的其他节点信息
                SQL = "SELECT MBDM,QZMBDM,LX,JSDM,MBLX,CJBM FROM TB_YSMBLC WHERE JSDM='" + JSDM + "' AND QZMBDM='" + MBDM + "' AND CJBM<>'" + CJBM + "' AND MBLX='" + YSLX + "'";
            }
            else
            {
                SQL = "SELECT MBDM,QZMBDM,LX,JSDM,MBLX,CJBM FROM TB_YSMBLC WHERE CJBM LIKE '" + CJBM.Substring(0, CJBM.Length - 4) + "%'  AND CJBM<>'" + CJBM + "' AND MBLX='" + YSLX + "' AND DATALENGTH(CJBM)=" + CJBM.Length;
            }
            return Query(SQL);
        }
        /// <summary>
        /// 填报或者审批当前模板
        /// </summary>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="QZMBDM">前置模板</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="JSDM">角色代码</param>
        public void SCCurrentTB(ref List<string> ArrList, ref ArrayList List, string QZMBDM, string JSDM, string JHFADM, string CJBM,string SPYJ)
        {
            string SQL = "";
            //将当前模板提交
            string PrimaryKey = GetStringPrimaryKey();
            SQL = "INSERT INTO TB_MBLCSB(SPZTDM,DQMBDM,STATE,CREATEUSER,CREATETIME,CLSJ,CLR,JHFADM,JSDM,CJBM,SPYJ)VALUES(@SPZTDM,@DQMBDM,@STATE,@CREATEUSER,@CREATETIME,@CLSJ,@CLR,@JHFADM,@JSDM,@CJBM,@SPYJ)";
            ArrList.Add(SQL);
            AseParameter[] MB = { 
                new AseParameter("@SPZTDM", AseDbType.VarChar, 40),
                new AseParameter("@DQMBDM", AseDbType.VarChar, 40),
                new AseParameter("@STATE", AseDbType.Integer),
                new AseParameter("@CREATEUSER", AseDbType.VarChar, 40),
                new AseParameter("@CREATETIME", AseDbType.VarChar, 20),
                new AseParameter("@CLSJ", AseDbType.VarChar, 20),
                new AseParameter("@CLR", AseDbType.VarChar, 40),
                new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                new AseParameter("@JSDM", AseDbType.VarChar, 40),
                new AseParameter("@CJBM", AseDbType.VarChar, 40),
                new AseParameter("@SPYJ", AseDbType.Image)
            };
            MB[0].Value = PrimaryKey;
            MB[1].Value = QZMBDM;
            MB[2].Value = 1;
            MB[3].Value = HttpContext.Current.Session["USERDM"].ToString();
            MB[4].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            MB[5].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            MB[6].Value = HttpContext.Current.Session["USERDM"].ToString();
            MB[7].Value = JHFADM;
            MB[8].Value = JSDM;
            MB[9].Value = CJBM;
            //将字符串转化成字节数据，存入Image类型的字段
            byte[] YJ = Encoding.Default.GetBytes(SPYJ);
            MB[10].Value = YJ;
            List.Add(MB);
        }
        /// <summary>
        /// 填报或者审批当前模板
        /// </summary>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="QZMBDM">前置模板</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="JSDM">角色代码</param>
        public void SCNextSP(ref List<string> ArrList, ref ArrayList List, string MBDM, string JSDM, string JHFADM, string CJBM)
        {
            string SQL = "";
            //将当前模板提交
            string PrimaryKey = GetStringPrimaryKey();
            SQL = "INSERT INTO TB_MBLCSB(SPZTDM,DQMBDM,STATE,CREATEUSER,CREATETIME,JHFADM,JSDM,CJBM)VALUES(@SPZTDM,@DQMBDM,@STATE,@CREATEUSER,@CREATETIME,@JHFADM,@JSDM,@CJBM)";
            ArrList.Add(SQL);
            AseParameter[] MB = { 
                new AseParameter("@SPZTDM", AseDbType.VarChar, 40),
                new AseParameter("@DQMBDM", AseDbType.VarChar, 40),
                new AseParameter("@STATE", AseDbType.Integer),
                new AseParameter("@CREATEUSER", AseDbType.VarChar, 40),
                new AseParameter("@CREATETIME", AseDbType.VarChar, 20),
                new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                new AseParameter("@JSDM", AseDbType.VarChar, 40),
                new AseParameter("@CJBM", AseDbType.VarChar, 40)
            };
            MB[0].Value = PrimaryKey;
            MB[1].Value = MBDM;
            MB[2].Value = -1;
            MB[3].Value = HttpContext.Current.Session["USERDM"].ToString();
            MB[4].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            MB[5].Value = JHFADM;
            MB[6].Value = JSDM;
            MB[7].Value = CJBM;
            List.Add(MB);
        }
        /// <summary>
        /// 审批模板数据
        /// </summary>
        ///<param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        public void UpateAndInsertMB(string JHFADM, string MBDM, ref List<string> ArrList, ref ArrayList List, int ZT, out string JSDM, out string CJBM, string MBLB,string SPYJ)
        {
            string SQL = "";
            DataSet DS = new DataSet();
            if (ZT == 1)
            {
                SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH,TB_YSMBLC LC WHERE SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND LC.LX='0' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
            }
            else
            {
                SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
            }
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                SQL = "UPDATE TB_MBLCSB SET CLSJ=@CLSJ,CLR=@CLR,STATE=@STATE,SPYJ=@SPYJ WHERE JHFADM=@JHFADM AND DQMBDM=@MBDM AND STATE=-1 AND JSDM=@JSDM";
                ArrList.Add(SQL);
                AseParameter[] MB = { 
                    new AseParameter("@CLSJ", AseDbType.VarChar, 20),
                    new AseParameter("@CLR", AseDbType.VarChar, 40),
                    new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                    new AseParameter("@MBDM", AseDbType.VarChar, 40),
                    new AseParameter("@JSDM", AseDbType.VarChar, 40),
                    new AseParameter("@STATE", AseDbType.Integer),
                    new AseParameter("@SPYJ", AseDbType.Image)
                };
                MB[0].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                MB[1].Value = HttpContext.Current.Session["USERDM"].ToString();
                MB[2].Value = JHFADM;
                MB[3].Value = MBDM;
                //输出参数
                CJBM = DS.Tables[0].Rows[0]["CJBM"].ToString().Trim();
                JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString().Trim();
                MB[4].Value = JSDM;
                //ZT=1、表示打回重新提交；2表示审批同意；等于3表示打回；
                MB[5].Value = (ZT == 2 || ZT == 1) ? 1 : 0;

                //将字符串转化成字节数据，存入Image类型的字段
                byte[] YJ = Encoding.Default.GetBytes(SPYJ);
                MB[6].Value = YJ;
                List.Add(MB);
            }
            else
            {
                //表示当前为预算编制或者预算分解上报报表的前置模板在执行，上报报表可以执行打回操作
                CJBM = GetParCJBM(JHFADM, MBDM, MBLB);
                if (CJBM == "")
                {
                    JSDM = "-1";
                }
                else
                {
                    JSDM = getDataSet("TB_YSMBLC", "JSDM", "CJBM='" + CJBM + "'").Tables[0].Rows[0]["JSDM"].ToString();
                }
            }
        }


        /// <summary>
        /// 更新填报数据状态
        /// </summary>
        ///<param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        public void UpateTBState(string JHFADM, string MBDM, ref List<string> ArrList, ref ArrayList List, string State)
        {
            string SQL = "UPDATE TB_BBFYSJ SET STATE=@STATE WHERE JHFADM=@JHFADM AND MBDM=@MBDM";
            ArrList.Add(SQL);
            AseParameter[] MB = { 

                    new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                    new AseParameter("@MBDM", AseDbType.VarChar, 40),
                            new AseParameter("@STATE", AseDbType.VarChar, 2)
                };
            MB[0].Value = JHFADM;
            MB[1].Value = MBDM;
            MB[2].Value = State;
            List.Add(MB);
        }

        /// <summary>
        /// 生成下一步模板审批数据
        /// </summary>
        /// <param name="DS">需要生成的模板数据集</param>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="QZMBDM">前置模板</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="YJ">意见</param>
        public void SCNextTB(DataSet DS, ref List<string> ArrList, ref ArrayList List, string MBDM, string JHFADM, string JSDM, string YSLX, string CJBM, ref bool IsExist, ref bool JSStr)
        {
            DataSet DT = new DataSet();
            DataSet DD = new DataSet();
            string SQL = "", BM = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                bool F = true;
                //如果CJBM的长度为4的话，则表明它是根节点；查看它是不是别的节点的子节点
                if (CJBM.Length == 4)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        BM = DS.Tables[0].Rows[i]["CJBM"].ToString();
                        SQL = "SELECT MBDM,QZMBDM,LX,JSDM,MBLX,CJBM FROM TB_YSMBLC WHERE CJBM LIKE '" + BM.Substring(0, BM.Length - 4) + "%'  AND CJBM<>'" + BM + "' AND DATALENGTH(CJBM)=" + BM.Length;
                        //清空之前的虚表
                        DT.Tables.Clear();
                        DT = Query(SQL);
                        if (DT.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                            {
                                F = true;
                                DD.Tables.Clear();
                                //根据层级编码，获取模板和角色信息
                                DD = getDataSet("TB_YSMBLC", "QZMBDM,JSDM", "CJBM='" + DT.Tables[0].Rows[j]["CJBM"].ToString() + "'");
                                //判断当前节点的兄弟节点以及兄弟节点的子节点，是不是被打回后，重新提交
                                if (getDataSet("TB_MBLCSB", "CJBM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "'  AND STATE=-1").Tables[0].Rows.Count > 0)
                                {
                                    F = false;
                                    break;
                                }
                                else
                                {
                                    string Flag = "-1";
                                    //递归查询，子节点是否存在打回待处理任务
                                    recursionChirldMB(DT.Tables[0].Rows[j]["CJBM"].ToString(), YSLX, ref Flag, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), DD.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM);
                                    //表示有待处理的子节点模板
                                    if (Flag == "1")
                                    {
                                        F = false;
                                        break;
                                    }
                                    else
                                    {
                                        //当前节点既没有兄弟节点，兄弟节点也没有子节点，则查询之前有没有审批的记录
                                        SQL = "SELECT * FROM TB_MBLCSB WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "'  AND STATE=1";
                                        if (Query(SQL).Tables[0].Rows.Count <= 0)
                                        {
                                            F = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (F == true)
                            {
                                DD.Tables.Clear();
                                //根据层级编码，获取模板和角色信息
                                DD = getDataSet("TB_YSMBLC", "QZMBDM,JSDM,CJBM", "CJBM='" + BM.Substring(0, BM.Length - 4) + "'");
                                //判断当前提交的数据数据是否已经存在（防重复提交）
                                if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + DD.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                                {
                                    //IsExist = true;
                                    //break;
                                    //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入！
                                    DelErrorData(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["JSDM"].ToString(), DD.Tables[0].Rows[0]["CJBM"].ToString());
                                }
                                //生成下一个审批记录
                                SCNextSP(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), DD.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["CJBM"].ToString());
                            }
                        }
                        else
                        {
                            //如果当前查询节点，没有兄弟节点，则直接生成父节点的审批记录
                            DD.Tables.Clear();
                            //根据层级编码，获取模板和角色信息
                            DD = getDataSet("TB_YSMBLC", "QZMBDM,JSDM,CJBM", "CJBM='" + BM.Substring(0, BM.Length - 4) + "'");
                            //判断当前提交的数据数据是否已经存在（防重复提交）
                            if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + DD.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                            {
                                //IsExist = true;
                                //break;

                                //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入！
                                DelErrorData(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["JSDM"].ToString(), DD.Tables[0].Rows[0]["CJBM"].ToString());
                            }
                            //生成下一个审批记录
                            SCNextSP(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), DD.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["CJBM"].ToString());
                        }

                    }
                }
                else
                {
                    for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                    {
                        F = true;
                        //判断当前节点的兄弟节点，是不是被打回后，重新提交
                        if (getDataSet("TB_MBLCSB", "CJBM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DS.Tables[0].Rows[j]["QZMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[j]["JSDM"].ToString().Replace("'", "''") + "'  AND STATE=-1").Tables[0].Rows.Count > 0)
                        {
                            F = false;
                            break;
                        }
                        else
                        {
                            string Flag = "-1";
                            //递归查询，子节点是否存在打回待处理任务
                            recursionChirldMB(DS.Tables[0].Rows[j]["CJBM"].ToString(), YSLX, ref Flag, DS.Tables[0].Rows[j]["QZMBDM"].ToString(), DS.Tables[0].Rows[j]["JSDM"].ToString(), JHFADM);
                            //表示有待处理的子节点模板
                            if (Flag == "1")
                            {
                                F = false;
                                break;
                            }
                            else
                            {
                                //当前节点既没有兄弟节点，兄弟节点也没有子节点，则查询之前有没有审批的记录
                                SQL = "SELECT * FROM TB_MBLCSB WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + DS.Tables[0].Rows[j]["QZMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[j]["JSDM"].ToString().Replace("'", "''") + "'  AND STATE=1";
                                if (Query(SQL).Tables[0].Rows.Count <= 0)
                                {
                                    F = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (F == true)
                    {
                        DD.Tables.Clear();
                        //根据层级编码，获取模板和角色信息
                        DD = getDataSet("TB_YSMBLC", "QZMBDM,JSDM,CJBM", "CJBM='" + CJBM.Substring(0, CJBM.Length - 4) + "'");
                        //判断当前提交的数据数据是否已经存在（防重复提交）
                        if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + DD.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                        {
                            //IsExist = true;
                            //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入！
                            DelErrorData(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["JSDM"].ToString(), DD.Tables[0].Rows[0]["CJBM"].ToString());
                        }
                        //生成下一个审批记录
                        SCNextSP(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), DD.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["CJBM"].ToString());
                    }
                }
            }
            else
            {
                //查询当前模板的兄弟模板，如果没有且当前节点不是根节点，则生成父节点的审批记录
                if (CJBM.Length != 4)
                {
                    DD.Tables.Clear();
                    //根据层级编码，获取模板和角色信息
                    DD = getDataSet("TB_YSMBLC", "QZMBDM,JSDM,CJBM", "CJBM='" + CJBM.Substring(0, CJBM.Length - 4) + "'");
                    //判断当前提交的数据数据是否已经存在（防重复提交）
                    if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DD.Tables[0].Rows[0]["QZMBDM"].ToString() + "' AND JSDM='" + DD.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + DD.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                    {
                        //IsExist = true;
                        //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入！
                        DelErrorData(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["JSDM"].ToString(), DD.Tables[0].Rows[0]["CJBM"].ToString());
                    }
                    //生成下一个审批记录
                    SCNextSP(ref ArrList, ref List, DD.Tables[0].Rows[0]["QZMBDM"].ToString(), DD.Tables[0].Rows[0]["JSDM"].ToString(), JHFADM, DD.Tables[0].Rows[0]["CJBM"].ToString());
                }
                else
                {
                    //当前模板为4位的时候，表示为根节点
                    //则存在两种情况：此模板没有挂接到别的模板下（即不作为别的模板的子节点，则不需要做任何操作）；再就是挂接到别的模板下但是角色代码对接错误（即在别的模板下，模板代码相同，但是角色不同，这种情况要给予提示！）
                    string SQLNEXT = "SELECT DISTINCT JSDM FROM TB_YSMBLC WHERE  QZMBDM='" + MBDM + "' AND MBLX='" + YSLX + "' AND CJBM NOT IN(SELECT CJBM FROM TB_YSMBLC WHERE CJBM LIKE '%" + CJBM + "%' AND MBLX='" + YSLX + "')";
                    //表明此模板作为别的模板的子节点时，模板对应不一致
                    if (Query(SQLNEXT).Tables[0].Rows.Count > 0)
                    {
                        JSStr = true;
                    }
                }
            }
        }
        /// <summary>
        /// 查询当前模板有没有待审批的子节点
        /// </summary>
        /// <param name="PAR">层级编码（父节点）</param>
        /// <param name="MBLX">模板类型</param>
        /// <param name="Flag">待审批标记（1代表存在待审批子节点；-1表示不存在待审批子节点模板）</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        public void recursionChirldMB(string PAR, string MBLX, ref string Flag, string MBDM, string JSDM, string JHFADM)
        {
            DataSet DS = new DataSet();
            string MB = "", JS = "", CJ = "", LX = "";
            string SQL = "SELECT QZMBDM MBDM,JSDM,CJBM,CCJB,LX FROM TB_YSMBLC LC WHERE  SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "'";
            DS = Query(SQL);
            //判断其有没有子节点
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                    JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                    CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                    LX = DS.Tables[0].Rows[i]["LX"].ToString();
                    //判断子节点是否有审批打回上报或者审批的记录
                    if (getDataSet("TB_MBLCSB", "CJBM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + MB + "' AND JSDM='" + JS.Replace("'", "''") + "'  AND STATE=-1").Tables[0].Rows.Count > 0)
                    {
                        //说明其子节点有打回上报或者审批的记录（递归的出口） 
                        Flag = "1";
                        break;
                    }
                    else
                    {
                        if (LX != "0")
                        {
                            //递归调用，查询前置模板为填报类型的模板
                            recursionChirldMB(CJ, MBLX, ref Flag, MB, JS, JHFADM);
                        }
                    }
                }
            }
            else
            {
                //说明其子节点不是填报模板（递归的出口） 
                DS.Tables.Clear();

                DS = getDataSet("TB_YSMBLC", "QZMBDM MBDM,JSDM,CJBM,LX", "QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + PAR + "' AND MBLX='" + MBLX + "'");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                        JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                        CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                        LX = DS.Tables[0].Rows[i]["LX"].ToString();

                        //判断子节点是否有审批打回上报或者审批的记录
                        if (getDataSet("TB_MBLCSB", "CJBM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + MB + "' AND JSDM='" + JS.Replace("'", "''") + "'  AND STATE=-1").Tables[0].Rows.Count > 0)
                        {
                            //说明其子节点有打回上报或者审批的记录（递归的出口） 
                            Flag = "1";
                            break;
                        }
                        else
                        {
                            if (LX != "0")
                            {
                                //递归调用，查询前置模板为填报类型的模板
                                recursionChirldMB(CJ, MBLX, ref Flag, MB, JS, JHFADM);
                            }
                        }
                    }
                }
            }
        }




        /// <summary>
        /// 填报或者审批当前模板
        /// </summary>
        /// <param name="ArrList">sql语句集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="QZMBDM">前置模板</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="JSDM">角色代码</param>
        public void SCDHMB(ref List<string> ArrList, ref ArrayList List, string QZMBDM, string JHFADM, string YSLX, string MBDM, string JSDM, ref bool IsExist, ref bool JSStr,string DHYY)
        {
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            string SQL = "", DM = "", JS = "", CJ = "", CJBM = "", Flag = "-1";
            string[] DH = QZMBDM.Split('|');
            for (int i = 0; i < DH.Length; i++)
            {
                DM = ""; JS = ""; CJ = "";
                string[] D = DH[i].Split('&');
                CJBM = CJBM == "" ? D[2].ToString() : CJBM + "," + D[2].ToString();
                //判断当前节点的子节点或者本身是不是填报模板
                if (getDataSet("TB_YSMBLC", "CJBM", "CJBM LIKE '" + D[2].ToString() + "%' AND LX='0' AND MBLX='" + YSLX + "'").Tables[0].Rows.Count > 0)
                {
                    DM = D[0].ToString();
                    JS = D[1].ToString();
                    CJ = D[2].ToString();
                }
                else
                {
                    //递归查询起子节点是不是填报模板
                    recursionDHMB(D[2], YSLX, ref Flag, D[0].ToString(), D[1].ToString(), D[0].ToString(), D[1].ToString());
                    //表示是填报模板
                    if (Flag == "1")
                    {
                        DM = D[0].ToString();
                        JS = D[1].ToString();
                        CJ = D[2].ToString();
                    }
                    else if (Flag == "-2")
                    {
                        //表示不是填报模板
                        DT.Tables.Clear();
                        DT = getDataSet("TB_YSMBLC", "CJBM", "QZMBDM='" + D[0].ToString() + "' AND JSDM='" + D[1].ToString().Replace("'", "''") + "' AND CJBM<>'" + D[2].ToString() + "' AND MBLX='" + YSLX + "'");
                        if (DT.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                            {
                                DM = D[0].ToString();
                                JS = D[1].ToString();
                                //递归查询起子节点是不是填报模板
                                recursionDHMB(DT.Tables[0].Rows[j]["CJBM"].ToString(), YSLX, ref Flag, D[0].ToString(), D[1].ToString(), D[0].ToString(), D[1].ToString());
                                if (Flag == "1")
                                {
                                    CJ = DT.Tables[0].Rows[j]["CJBM"].ToString();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            //当前模板为4位的时候，表示为根节点
                            //则存在两种情况：此模板没有挂接到别的模板下（即不作为别的模板的子节点，则不需要做任何操作）；再就是挂接到别的模板下但是角色代码对接错误（即在别的模板下，模板代码相同，但是角色不同，这种情况要给予提示！）
                            string SQLNEXT = "SELECT DISTINCT JSDM FROM TB_YSMBLC WHERE  QZMBDM='" + MBDM + "' AND MBLX='" + YSLX + "' AND CJBM NOT IN(SELECT CJBM FROM TB_YSMBLC WHERE CJBM LIKE '%" + CJBM + "%' AND MBLX='" + YSLX + "')";
                            //表明此模板作为别的模板的子节点时，模板对应不一致
                            if (Query(SQLNEXT).Tables[0].Rows.Count > 0)
                            {
                                JSStr = true;
                            }
                        }
                    }
                }
                //判断当前提交的数据数据是否已经存在（防重复提交）
                if (getDataSet("TB_MBLCSB", "DQMBDM", "JHFADM='" + JHFADM + "' AND DQMBDM='" + DM + "' AND JSDM='" + JS.ToString().Replace("'", "''") + "' AND STATE=-1 AND CJBM='" + CJ + "'").Tables[0].Rows.Count > 0)
                {
                    //IsExist = true;
                    //break;
                    //如果之前因异常情况，产生的垃圾数据，提交时，先将其删除，在插入！
                    DelErrorData(ref ArrList, ref List, DM, JHFADM, JS, CJ);
                }
                //表示此模板作为别的模板的子节点时，角色代码没有对应错误
                if (JSStr == false)
                {
                    //将当前模板提交
                    string PrimaryKey = GetStringPrimaryKey();
                    SQL = "INSERT INTO TB_MBLCSB(SPZTDM,DQMBDM,STATE,CREATEUSER,CREATETIME,JHFADM,JSDM,CJBM)VALUES(@SPZTDM,@DQMBDM,@STATE,@CREATEUSER,@CREATETIME,@JHFADM,@JSDM,@CJBM)";
                    ArrList.Add(SQL);
                    AseParameter[] MB = { 
                        new AseParameter("@SPZTDM", AseDbType.VarChar, 40),
                        new AseParameter("@DQMBDM", AseDbType.VarChar, 40),
                        new AseParameter("@STATE", AseDbType.Integer),
                        new AseParameter("@CREATEUSER", AseDbType.VarChar, 40),
                        new AseParameter("@CREATETIME", AseDbType.VarChar, 20),
                        new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                        new AseParameter("@JSDM", AseDbType.VarChar, 40),
                        new AseParameter("@CJBM", AseDbType.VarChar, 40)
                    };
                    MB[0].Value = PrimaryKey;
                    MB[1].Value = DM;
                    MB[2].Value = -1;
                    MB[3].Value = HttpContext.Current.Session["USERDM"].ToString();
                    MB[4].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    MB[5].Value = JHFADM;
                    MB[6].Value = JS;
                    MB[7].Value = CJ;
                    List.Add(MB);

                    if (DHYY != "" && DHYY != null)
                    {
                        string [] DHJS = JS.Replace("'","").Split(',');
                        for (int k = 0; k < DHJS.Length; k++)
                        {
                            SQL = "INSERT INTO TB_MESSAGE (SJ,FSUSER,JSJSDM,INFO) VALUES(@SJ,@FSUSER,@JSJSDM,@INFO)";
                            ArrList.Add(SQL);
                            AseParameter[] Data = { 
                            new AseParameter("@SJ", AseDbType.VarChar, 20),
                            new AseParameter("@FSUSER", AseDbType.VarChar, 40),
                            new AseParameter("@JSJSDM", AseDbType.VarChar, 100),
                            new AseParameter("@INFO", AseDbType.VarChar, 40)
                            };
                            Data[0].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            Data[1].Value = HttpContext.Current.Session["USERDM"].ToString();
                            Data[2].Value = DHJS[0].ToString();
                            Data[3].Value = getDataSet("TB_YSBBMB", "MBMC", "MBDM='" + DM + "'").Tables[0].Rows[0]["MBMC"].ToString()+" "+DHYY;
                            List.Add(Data);
                        }
                    }           
                }
            }
            ////表示此模板作为别的模板的子节点时，角色代码没有对应错误
            //if (JSStr == false)
            //{
            //    //将打回的历史记录写入表中，方便下次打回时默认选中
            //    DS.Tables.Clear();
            //    DS = getDataSet("TB_MBDHLS", "DHCJBM", "JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "'");
            //    if (DS.Tables[0].Rows.Count > 0)
            //    {
            //        if (DS.Tables[0].Rows[0]["DHCJBM"].ToString() != CJBM)
            //        {
            //            UpdateORInsertMBDHLS(ref ArrList, ref List, 0, JHFADM, MBDM, JSDM, CJBM);
            //        }
            //    }
            //    else
            //    {
            //        UpdateORInsertMBDHLS(ref ArrList, ref List, 1, JHFADM, MBDM, JSDM, CJBM);
            //    }
            //}
        }
        public void recursionDHMB(string PAR, string MBLX, ref string Flag, string MBDM, string JSDM, string M, string J)
        {
            DataSet DS = new DataSet();
            string MB = "", JS = "", CJ = "", LX = "";
            string SQL = "SELECT QZMBDM MBDM,JSDM,CJBM,CCJB,LX FROM TB_YSMBLC LC WHERE  SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                    JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                    CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                    LX = DS.Tables[0].Rows[i]["LX"].ToString();
                    if (LX != "0")
                    {
                        //递归调用，查询前置模板为填报类型的模板
                        recursionDHMB(CJ, MBLX, ref Flag, MB, JS, M, J);
                    }
                    else
                    {
                        //说明其子节点是填报模板（递归的出口） 
                        Flag = "1";
                        break;
                    }
                }
            }
            else
            {
                //说明其子节点不是填报模板（递归的出口） 
                DS.Tables.Clear();
                if (MBDM == M && JSDM == J)
                {
                    Flag = "-2";
                }
                else
                {
                    DS = getDataSet("TB_YSMBLC", "QZMBDM MBDM,JSDM,CJBM,LX", "QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + PAR + "' AND MBLX='" + MBLX + "'");
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                        JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                        CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                        LX = DS.Tables[0].Rows[i]["LX"].ToString();
                        if (LX != "0")
                        {
                            //递归调用，查询前置模板为填报类型的模板
                            recursionDHMB(CJ, MBLX, ref Flag, MB, JS, M, J);
                        }
                        else
                        {
                            //说明其子节点是填报模板（递归的出口） 
                            Flag = "1";
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 将选中的打回模板信息存入到表中
        /// </summary>
        /// <param name="ArrList">数组集合</param>
        /// <param name="List">参数集合</param>
        /// <param name="FLag">Flag等于0代表更新；等于1表示插入</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级编码</param>
        public void UpdateORInsertMBDHLS(ref List<string> ArrList, ref ArrayList List, int FLag, string JHFADM, string MBDM, string JSDM, string CJBM)
        {
            string SQL = "";
            if (FLag == 0)
            {
                SQL = "UPDATE TB_MBDHLS SET DHCJBM=@CJBM WHERE JHFADM=@JHFADM AND MBDM=@MBDM AND JSDM=@JSDM";
                ArrList.Add(SQL);
            }
            else
            {
                SQL = "INSERT INTO TB_MBDHLS(DHCJBM,JHFADM,MBDM,JSDM)VALUES(@CJBM,@JHFADM,@MBDM,@JSDM)";
                ArrList.Add(SQL);
            }
            AseParameter[] MB = { 
                        new AseParameter("@CJBM", AseDbType.VarChar, 200),
                        new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                        new AseParameter("@MBDM", AseDbType.VarChar,40),
                        new AseParameter("@JSDM", AseDbType.VarChar, 40)
                    };
            MB[0].Value = CJBM;
            MB[1].Value = JHFADM;
            MB[2].Value = MBDM;
            MB[3].Value = JSDM;
            List.Add(MB);
        }
        /// <summary>
        /// 根据模板代码返回模板设计的角色信息
        /// </summary>
        /// <param name="MBDM">模板代码</param>
        /// <returns></returns>
        public DataTable GetDHJS(string MBDM)
        {
            string[] JS;
            string JSDM = "", JSStr = "";
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            //添加列
            DT.Columns.Add("JSDM", Type.GetType("System.String"));
            DT.Columns.Add("JSNAME", Type.GetType("System.String"));
            string SQL = "SELECT JSDM FROM TB_YSMBLC WHERE MBDM='" + MBDM + "'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    JSStr = "";
                    JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString().Trim();
                    DataRow DR = DT.NewRow(); ;
                    DR["JSDM"] = JSDM;
                    JS = JSDM.Split(',');
                    for (int j = 0; j < JS.Length; j++)
                    {
                        JSStr = JSStr == "" ? Query("SELECT JSDM,NAME FROM TB_JIAOSE WHERE JSDM=" + JS[j]).Tables[0].Rows[0]["NAME"].ToString() : JSStr + "," + Query("SELECT JSDM,NAME FROM TB_JIAOSE WHERE JSDM='" + JS[j] + "'").Tables[0].Rows[0]["NAME"].ToString();
                    }
                    DR["JSNAME"] = JSStr;
                    DT.Rows.Add(DR);
                }
            }
            return DT;
        }
        /// <summary>
        /// 获取部门及用户信息列表
        /// </summary>
        /// <returns></returns>
        public DataSet getDeptUser()
        {
            //string sql = "SELECT CCODE BM,CNAME MC FROM MIS.MIS_ORG ORDER BY CCODE";
            string sql = "SELECT DEPTDM BM,DEPTNAME MC FROM RMYH_DEPT_INFO ORDER BY DEPTDM";
            return DbHelperOra.Query(sql);
        }

        public void addSPTX(string SPXMDM, string XMDM)
        {

            DataSet ds = DbHelperOra.Query("SELECT XMMC FROM RMYH_SPXM WHERE XMDM='" + XMDM + "'");
            string message = "您当前有一条“'" + ds.Tables[0].Rows[0][0].ToString() + "'”项目代码为" + SPXMDM + "的文件需要处理！";


        }

        /// <summary>
        /// 获取菜单标签
        /// </summary>
        /// <returns></returns>
        public string getMenuTitle(string strurl)
        {
            string sql = "SELECT TITLE FROM RMYH_MENU WHERE PAGEPATH = '" + strurl.Substring(1) + "'";
            DataSet ds = DbHelperOra.Query(sql);
            if (ds.Tables[0].Rows.Count == 1)
                return ds.Tables[0].Rows[0][0].ToString();
            return "";
        }
        /// <summary>
        /// 查看操作人未审批或者已审批的模板
        /// </summary>
        /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
        /// <param name="MBLX">模板类型 (TB_YSBBMB.MBLX) </param>
        /// <param name="IsDone">0代表未做；1代表已做</param>
        /// <returns></returns>
        public DataSet GetMB(int CZLX, string MBLX, int IsDone, string JHFADM)
        {
            string SQL = "", YSLX = "";
            DataSet DS = new DataSet();
            //获取预算流程发起相关的信息
            DS = getDataSet("TB_YSLCFS", "YSLX,ISFQ", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0]["ISFQ"].ToString().Trim() == "1")
                {
                    YSLX = DS.Tables[0].Rows[0]["YSLX"].ToString().Trim();
                    //表示当前是填报类型
                    if (CZLX == 1)
                    {
                        if (IsDone == 0)
                        {
                            SQL = "SELECT MB.MBDM,MBBM,MBMC,LC.JSDM FROM TB_YSBBMB MB,TB_YSMBLC LC,TB_JSYH YH WHERE MB.MBDM=QZMBDM AND MB.MBLX='" + MBLX + "' AND  LC.LX='0' AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%' ";
                        }
                        else
                        {
                            SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='0' AND JHFADM='" + JHFADM + "' AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%' AND STATE=1";
                        }
                    }
                    else
                    {
                        if (IsDone == 0)
                        {
                            SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='1' AND JHFADM='" + JHFADM + "' AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%' AND STATE=-1";
                        }
                        else
                        {
                            SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='1' AND JHFADM='" + JHFADM + "' AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%' AND STATE=1";
                        }
                    }
                }
            }
            DS = Query(SQL);
            return DS;
        }
        /// <summary>
        /// 查看操作人未审批或者已审批的模板
        /// </summary>
        /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
        /// <param name="MBLX">模板类型 (TB_YSBBMB.MBLX) </param>
        /// <param name="IsDone">-1代表未做0,1代表已做</param>
        /// <param name="_MBLB">模板类别</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <returns></returns>
        public DataTable GetMBCZQX(string MBDM, int CZLX, string IsDone, string JHFADM, string _MBLB,string ISZLC,string USERDM,string HSZXDM)
        {
            string SQL = "";
            DataSet D = new DataSet();
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DT.Columns.Add("MBDM", Type.GetType("System.String"));
            DT.Columns.Add("XGQX", Type.GetType("System.String"));
            DT.Columns.Add("MBXGQX", Type.GetType("System.String"));
            DT.Columns.Add("SBQX", Type.GetType("System.String"));
            DT.Columns.Add("RETURNQX", Type.GetType("System.String"));
            DT.Columns.Add("SPQX", Type.GetType("System.String"));
            DT.Columns.Add("PRINTQX", Type.GetType("System.String"));
            DT.Columns.Add("XZTBBZ", Type.GetType("System.String"));
            //获取预算类型
            string MBLX = GetYSLX(_MBLB);
            //定义权限的变量
            string XGQX = "0", MBXGQX = "0", SBQX = "0", RETURNQX = "0", SPQX = "0", PRINTQX = "0", YSLX = "",XZTBBZ="1";
            //根据计划方案代码获取预算类型
            YSLX = GetYSLXByJHFADM(JHFADM);
            //查询用户的模板操作权限
            SQL = "SELECT MBDM,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX FROM TB_MBJSQX QX,TB_JSYH YH WHERE QX.JSDM=YH.JSDM AND MBDM='" + MBDM + "' AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "'";
            DS = Query(SQL);
            DataRow DR = DT.NewRow();
            DR["MBDM"] = MBDM.ToString();
            //判断当前计划方案是否走流程（true代表走流程；false代表不走流程）
            if (ISZLC.Trim().Equals("0"))
            {
                string LX = "";
                //获取模板的模板类型
                if (MBDM != "")
                {
                    DataSet T = getDataSet("TB_YSBBMB", "MBLX", "MBDM='" + MBDM + "'");
                    LX = T.Tables[0].Rows.Count > 0 ? T.Tables[0].Rows[0]["MBLX"].ToString() : "";
                }
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    //如果当前模板类型为1，2，3，4，5，7中的一种，且不走流程，如果有上报按钮的权限，则将权限放出
                    //模板类型为1时，点击上报按钮时，将数据写入TB_JHZYTR,TB_JHZYCC中
                    //模板类型为2,3,4,5,7时，点击上报按钮，将数据写入TB_BBFYSJ中
                    if ("1,2,3,4,5,7".IndexOf(LX) > -1)
                    {
                        SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                    }
                    XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                }
            }
            else
            {
                //表示当前是填报类型
                if (CZLX == 1)
                {
                    if (IsDone == "-1")
                    {
                        if (YSLX != "-1" && YSLX != "")
                        {
                            //表示上报后被打回

                            SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='0'  AND YS.MBLX='" + YSLX + "' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'";
                            D.Tables.Clear();
                            D = Query(SQL);
                            if (D.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                {
                                    XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                    SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                }
                            }
                            else
                            {
                                //查看当前模板是否之前是否有填报历史

                                SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='0' AND YS.MBLX='" + YSLX + "'  AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'";
                                D.Tables.Clear();
                                D = Query(SQL);
                                if (D.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                    {
                                        MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                        PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                    }
                                }
                                else
                                {
                                    //首次填报

                                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                    {
                                        XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                        SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                                        MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                        PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //表示已上报提交
                        SQL = "SELECT * FROM TB_MBLCSB LC,TB_YSMBLC YS,TB_JSYH YH WHERE LC.DQMBDM=YS.QZMBDM AND LC.JSDM=YS.JSDM AND YS.LX='0' AND YS.MBLX='" + YSLX + "' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'";
                        D.Tables.Clear();
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                            }
                        }
                    }
                }
                else
                {
                    //待审批
                    if (IsDone == "-1")
                    {

                        SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH,TB_YSMBLC LC WHERE SB.CJBM=LC.CJBM AND LC.LX<>'0' AND LC.MBLX='" + YSLX + "' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                                SPQX = SPQX != "1" ? DS.Tables[0].Rows[i]["SPQX"].ToString().Trim() : SPQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                            }
                        }
                        else
                        {
                            bool F = false;
                            StringBuilder StrSql = new StringBuilder();
                            //上报报表和预算分解报表在前置模板没有做完时，可以打回之前审批的模板（单总修改的需求（2016.08.09））
                            if (_MBLB == "baseys" || _MBLB == "resoys")
                            {
                                //if (_MBLB == "baseys")
                                //{
                                //    LX = "4";
                                //}
                                //else
                                //{
                                //    LX = "5";
                                //}
                                //StrSql.Append("SELECT L.MBDM FROM TB_YSMBLC L,TB_MBLCSB S,TB_YSBBMB M WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1  AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + YSLX + "' AND S.DQMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + LX + "'");
                                //StrSql.Append(" UNION");
                                //StrSql.Append(" SELECT L.MBDM FROM TB_YSMBLC L,TB_YSBBMB M");
                                //StrSql.Append("  WHERE L.MBDM='"+MBDM+"' AND L.LX='0' AND AND L.MBLX='" + YSLX + "'");
                                //StrSql.Append("   AND L.QZMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + LX + " ");
                                //StrSql.Append("   AND NOT EXISTS(SELECT * FROM TB_MBLCSB S WHERE JHFADM='"+JHFADM+"' AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND STATE=1)");
                                //GetDCLQZMBStr(JHFADM, MBDM, _MBLB, IsDone);
                                if (GetDCLQZMBStr(JHFADM, MBDM, _MBLB, IsDone) != "")
                                {
                                    F = true;
                                }
                                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                {
                                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                    //上报报表和预算分解报表在前置模板没有做完时，可以打回之前审批的模板（单总修改的需求（2016.08.09））
                                    if (F == true)
                                    {
                                        XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                        RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                {
                                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                }
                            }
                        }
                    }
                    else
                    {
                        //已经审批过
                        SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH,TB_YSMBLC LC WHERE SB.CJBM=LC.CJBM AND LC.LX<>'0' AND LC.MBLX='" + YSLX + "' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            bool Flag = false;
                            //如果是流程的最后一步，审批后可以允许在待执行里执行打回操作（单林海的需求 2018.08.25）
                            if (IsLastOne(JHFADM, MBDM))
                            {
                                Flag = true;
                            }
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                if (Flag == true)
                                {
                                    RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                                }
                            }
                        }
                    }
                }
            }
            DR["XGQX"] = XGQX;
            DR["MBXGQX"] = MBXGQX;
            DR["SBQX"] = SBQX;
            DR["RETURNQX"] = RETURNQX;
            DR["SPQX"] = SPQX;
            DR["PRINTQX"] = PRINTQX;
            //判断当前用户是否非受限用户（true代表是受限用户；false代表是非受限用户）
            if (IsSQUSER(USERDM, HSZXDM) == true)
            {
                TimeSpan TS = TimeSpan.Zero;
                string JZSJ = "", SXBZ = "";
                //获取受限相关信息
                string SX = GetXZInfo(JHFADM);
                if (SX != "")
                {
                    string []Arr = SX.Split(',');
                    JZSJ = Arr[0].ToString().Trim();
                    SXBZ = Arr[1].ToString().Trim();
                    if (SXBZ == "1")
                    {
                        if (JZSJ != "" && JZSJ != "-1")
                        {
                            TS = DateTime.Parse(JZSJ.Replace("/", "-")) - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00");
                            if (TS.Days >= 0 && TS.Hours >= 0 && TS.Minutes >= 0)
                            {
                                XZTBBZ = "1";
                            }
                            else
                            {
                                XZTBBZ = "0";
                            }
                        }
                    }
                }
            }
            DR["XZTBBZ"] = XZTBBZ;
            DT.Rows.Add(DR);
            return DT;
        }
        /// <summary>
        /// 根据方案代码，获取预算类型（YSLX=-1表示流程没有发起）
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetYSLXByJHFADM(string JHFADM)
        {
            string YSLX = "";
            DataSet DS = new DataSet();
            //获取预算流程发起相关的信息
            DS = getDataSet("TB_YSLCFS", "YSLX,ISFQ", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count <= 0)
            {
                //表示流程没有发起
                YSLX = "-1";
            }
            else
            {
                if (DS.Tables[0].Rows[0]["ISFQ"].ToString().Trim() == "0")
                {
                    //表示流程没有发起
                    YSLX = "-1";
                }
                else
                {
                    YSLX = DS.Tables[0].Rows[0]["YSLX"].ToString().Trim();
                }
            }
            return YSLX;
        }
        /// <summary>
        /// 递归调用，查询模板类型的填报的模板代码字符串
        /// </summary>
        /// <param name="MBDM"></param>
        /// <returns></returns>
        public string GetDHMB(string MBDM, string JHFADM, string MBLB)
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DT.Columns.Add("MBDM");
            DT.Columns.Add("MBMC");
            DT.Columns.Add("JSDM");
            DT.Columns.Add("CJBM");
            DT.Columns.Add("LX");
            StringBuilder MBStr = new StringBuilder();
            string SQL = "", CJBM = "", YSLX = "", JSDM = "";
            string MB = "", MC = "", JS = "", CJ = "", LX = "", JSNAME = "";
            YSLX = GetYSLXByJHFADM(JHFADM);
            if (YSLX == "-1" || YSLX == "")
            {
                MBStr.Append("-2");
            }
            else
            {
                //查询当前模板的审批记录
                SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    CJBM = DS.Tables[0].Rows[0]["CJBM"].ToString();
                    JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString();
                    JSNAME = GetJSNameByJSDM(JSDM);
                }
                else
                {
                    //上报报表和预算分解报表在前置模板没有做完时，可以打回之前审批的模板（单总修改的需求（2016.08.09））

                    //获取层级编码
                    //CJBM = GetCJBM(JHFADM, MBDM, MBLB);
                    CJBM = GetParCJBM(JHFADM, MBDM, MBLB);
                    if (CJBM != "")
                    {
                        JSDM = getDataSet("TB_YSMBLC", "JSDM", "CJBM='" + CJBM + "'").Tables[0].Rows[0]["JSDM"].ToString();
                        JSNAME = GetJSNameByJSDM(JSDM);
                    }
                }
                if (CJBM != "" && JSDM != "")
                {
                    string MBMC = getDataSet("TB_YSBBMB", "MBMC", "MBDM='" + MBDM + "'").Tables[0].Rows[0]["MBMC"].ToString();
                    MBStr.Append("[");
                    MBStr.Append("{");
                    MBStr.Append("\"id\":\"" + MBDM + "\"");
                    MBStr.Append(",");
                    MBStr.Append("\"text\":\"" + MBMC + "----" + JSNAME + "\"");
                    MBStr.Append(",");
                    //自定义属性
                    MBStr.Append("\"attributes\": {\"CJBM\": \"" + CJBM + "\",\"JSDM\": \"" + JSDM + "\"}");
                    //string C=GetDHCJBM(JHFADM);
                    //递归获取字节点的Json字符串
                    recursion(CJBM, ref MBStr, YSLX, MBDM, JSDM, ref DT, JHFADM, MBLB);

                    //遍历相同模板和角色的不同层级编码的其它节点
                    while (DT.Rows.Count > 0)
                    {
                        DataTable T = new DataTable();
                        T.Columns.Add("MBDM");
                        T.Columns.Add("MBMC");
                        T.Columns.Add("JSDM");
                        T.Columns.Add("CJBM");
                        T.Columns.Add("LX");
                        MBStr.Append("}");
                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            MB = DT.Rows[i]["MBDM"].ToString();
                            MC = DT.Rows[i]["MBMC"].ToString();
                            JS = DT.Rows[i]["JSDM"].ToString();
                            CJ = DT.Rows[i]["CJBM"].ToString();
                            LX = DT.Rows[i]["LX"].ToString();
                            JSNAME = GetJSNameByJSDM(JS);
                            //拼接子节点的Json字符串
                            MBStr.Append(",");
                            MBStr.Append("{");
                            MBStr.Append("\"id\":\"" + MB + "\"");
                            MBStr.Append(",");
                            MBStr.Append("\"text\":\"" + MC + "----" + JSNAME + "\"");
                            MBStr.Append(",");
                            //自定义属性
                            MBStr.Append("\"attributes\": {\"CJBM\": \"" + CJ + "\",\"JSDM\": \"" + JS + "\"}");
                            if (LX != "0")
                            {
                                //递归调用，查询前置模板为填报类型的模板
                                recursion(CJ, ref MBStr, YSLX, MB, JS, ref T, JHFADM, MBLB);
                            }
                            if (i < DT.Rows.Count - 1)
                            {
                                MBStr.Append("}");
                            }
                        }
                        DT = T;
                    }
                    MBStr.Append("}]");
                }
            }
            return MBStr.ToString();
        }
        /// <summary>
        /// 根据角色代码，获取角色名称
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetJSNameByJSDM(string JSDM)
        {
            string JSName = "";
            DataSet DS = new DataSet();
            if (JSDM != "" && JSDM != null)
            {
                string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
                DS = Query(SQL);
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (JSName == "")
                    {
                        JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                    }
                    else
                    {
                        JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                    }
                }
            }
            return JSName;
        }
        /// <summary>
        /// 获取层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLB">模板类别</param>
        /// <returns></returns>
        public string GetCJBM(string JHFADM, string MBDM, string MBLB)
        {
            string SQL = "", MBLX = "", CJBM = "";
            //获取预算类型
            string YSLX = GetYSLXByJHFADM(JHFADM);
            DataSet DS = new DataSet();
            if (MBLB == "baseys" || MBLB == "resoys")
            {
                MBLX = MBLB == "baseys" ? " AND A.MBLX='4'" : " AND A.MBLX='5'";
                SQL = " SELECT DISTINCT SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4) CJBM FROM TB_YSMBLC LC,TB_YSBBMB A,TB_MBJSQX QX,TB_JSYH YH";
                SQL += "  WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA,TB_YSMBLC C WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.DQMBDM=C.QZMBDM AND SB.JSDM=C.JSDM AND C.MBDM='" + MBDM + "' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1) ";
                SQL += "  AND LC.MBDM='" + MBDM + "' AND LC.MBLX='" + YSLX + "'";
                SQL += "  AND YH.USERID='" + HttpContext.Current.Session["USERDM"] + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%' AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'";
                SQL += "  AND LC.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'";
                SQL += MBLX;
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    CJBM = DS.Tables[0].Rows[0]["CJBM"].ToString();
                }
            }
            return CJBM;
        }
        /// <summary>
        /// 递归调用，查询类型为填报类型的模板
        /// </summary>
        /// <param name="PAR">模板代码</param>
        /// <param name="MBStr">拼接的前置模板代码字符串</param>
        public void recursion(string PAR, ref StringBuilder MBStr, string MBLX, string MBDM, string JSDM, ref DataTable DT, string JHFADM, string MBLB)
        {
            string MB = "", MC = "", JS = "", CJ = "", LX = "", SQL = "", JSNAME = "";
            DataSet DS = new DataSet();
            if (MBLB == "baseys" || MBLB == "resoys")
            {
                SQL = "SELECT DISTINCT QZMBDM MBDM,MBMC,LC.JSDM,LC.CJBM,CCJB,LX FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "' AND LC.QZMBDM IN(SELECT DISTINCT DQMBDM FROM TB_MBLCSB SB WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)";
                //去除现在正在待处理的模板
                SQL += "AND NOT EXISTS (SELECT * FROM TB_YSMBLC C1,TB_YSMBLC C2 WHERE C1.MBLX='" + MBLX + "' AND C1.QZMBDM=C2.QZMBDM AND C1.JSDM=C2.JSDM AND SUBSTRING(C2.CJBM,1,DATALENGTH(C2.CJBM)-4)='" + PAR + "' AND C2.MBLX='" + MBLX + "' AND EXISTS(SELECT * FROM TB_MBLCSB S WHERE  S.JHFADM='" + JHFADM + "' AND S.STATE=-1 AND S.CJBM LIKE '%'+C1.CJBM+'%') AND LC.QZMBDM=C2.QZMBDM AND LC.JSDM=C2.JSDM AND LC.CJBM=C2.CJBM)";
                //表示上报报表或者分解报表的顶节点
                if (PAR.Length == 4) 
                {
                    DataSet T = new DataSet();
                    string DM = getDataSet("TB_YSMBLC","QZMBDM","CJBM='"+PAR+"'").Tables[0].Rows[0]["QZMBDM"].ToString();
                    T = Query("SELECT * FROM TB_YSMBLC WHERE SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='"+PAR+"' AND MBDM=QZMBDM AND MBDM='"+DM+"'");
                    if (T.Tables[0].Rows.Count > 0)
                    {
                        //如果打回的是顶节点的下级，则排除此节点，刷出其他的子节点
                        if (Query("SELECT * FROM TB_MBLCSB WHERE JHFADM='" + JHFADM + "' AND SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='" + PAR + "' AND STATE=-1").Tables[0].Rows.Count > 0)
                        {
                            SQL += " UNION";
                            SQL += " SELECT DISTINCT QZMBDM MBDM,MBMC,LC.JSDM,LC.CJBM,CCJB,LX FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "' AND LC.QZMBDM IN(SELECT DISTINCT DQMBDM FROM TB_MBLCSB SB WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1) AND LC.CJBM NOT IN(SELECT CJBM FROM TB_MBLCSB WHERE JHFADM='" + JHFADM + "' AND SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='" + PAR + "' AND STATE=-1)";
                        }
                        else
                        {
                            SQL += " UNION";
                            SQL += " SELECT DISTINCT QZMBDM MBDM,MBMC,LC.JSDM,LC.CJBM,CCJB,LX FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "' AND LC.QZMBDM IN(SELECT DISTINCT DQMBDM FROM TB_MBLCSB SB WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)";
                        }
                    }
                }
            }
            else
            {
                SQL = "SELECT DISTINCT QZMBDM MBDM,MBMC,LC.JSDM,LC.CJBM,CCJB,LX FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + PAR + "' AND LC.MBLX='" + MBLX + "' AND LC.CJBM IN(SELECT DISTINCT CJBM FROM TB_MBLCSB SB WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1 AND NOT EXISTS(SELECT * FROM TB_MBLCSB S WHERE S.JHFADM='"+JHFADM+"' AND STATE=-1 AND S.CJBM LIKE '%'+SB.CJBM+'%'))";
                //if (CJBM != "")
                //{
                //    SQL += " AND LC.CJBM NOT IN('" + CJBM + "')";
                //}
            }
            DS.Tables.Clear();
            //查询当前模板的fu
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                    MC = DS.Tables[0].Rows[i]["MBMC"].ToString();
                    JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                    CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                    LX = DS.Tables[0].Rows[i]["LX"].ToString();
                    //获取角色名称
                    JSNAME = GetJSNameByJSDM(JS);
                    //拼接子节点的Json字符串
                    if (i == 0)
                    {
                        MBStr.Append(",");
                        MBStr.Append("\"children\": [");
                    }
                    MBStr.Append("{");
                    MBStr.Append("\"id\":\"" + MB + "\"");
                    MBStr.Append(",");
                    MBStr.Append("\"text\":\"" + MC + "----" + JSNAME + "\"");
                    MBStr.Append(",");
                    //自定义属性
                    MBStr.Append("\"attributes\": {\"CJBM\": \"" + CJ + "\",\"JSDM\": \"" + JS + "\"}");
                    if (LX != "0")
                    {
                        //递归调用，查询前置模板为填报类型的模板
                        recursion(CJ, ref MBStr, MBLX, MB, JS, ref DT, JHFADM, MBLB);
                    }
                    if (i < DS.Tables[0].Rows.Count - 1)
                    {
                        MBStr.Append("},");
                    }
                    else
                    {
                        MBStr.Append("}]");
                    }
                }
            }
            else
            {
                SQL = "SELECT QZMBDM MBDM,MBMC,JSDM,CJBM,CCJB,LX FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND LC.MBLX='" + MBLX + "' AND QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + PAR + "'";
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        MB = DS.Tables[0].Rows[i]["MBDM"].ToString();
                        MC = DS.Tables[0].Rows[i]["MBMC"].ToString();
                        JS = DS.Tables[0].Rows[i]["JSDM"].ToString();
                        CJ = DS.Tables[0].Rows[i]["CJBM"].ToString();
                        LX = DS.Tables[0].Rows[i]["LX"].ToString();

                        if (DT.Select("MBDM='" + MB + "' AND JSDM='" + JS.Replace("'", "''") + "'", "CJBM").Length <= 0)
                        {
                            DataRow DR = DT.NewRow();

                            DR["MBDM"] = MB;
                            DR["MBMC"] = MC;
                            DR["JSDM"] = JS;
                            DR["CJBM"] = CJ;
                            DR["LX"] = LX;
                            DT.Rows.Add(DR);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 根据及计划方案代码获取是否走流程标记
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>true代表走流程；false代表不走流程</returns>
        public bool IsLCSP(string JHFADM)
        {
            bool Flag;
            string SQL = "SELECT SCBS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
            if (Query(SQL).Tables[0].Rows[0]["SCBS"].ToString() == "1")
            {
                Flag = true;
            }
            else
            {
                Flag = false;
            }
            return Flag;
        }
        /// <summary>
        /// 根据计划方案代码获取方案表示（月、年、季度、其他）
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetMBZQByJHFADM(string JHFADM)
        {
            string MBZQDM = "";
            if (JHFADM != "")
            {
                DataSet DS = new DataSet();
                string SQL = "SELECT FABS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    MBZQDM = DS.Tables[0].Rows[0]["FABS"].ToString();
                }
            }
            return MBZQDM;
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <param name="_mblb">模板类型</param>
        /// <returns></returns>
        public string GetMBListSQL(string JHFADM, string ISDone, string _mblb,string ZYDM)
        {
            string sqlmblx = "";
            string lx = "0", MBLX = GetYSLX(_mblb);

            if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "respfj")
            {
                sqlmblx = " AND  A.MBLX IN('1','2','3')";
            }
            else if (_mblb == "baseys" || _mblb == "respys" || _mblb == "respsp")
            {
                sqlmblx = " AND  A.MBLX='4'";
            }
            else if (_mblb == "resosb")
            {
                sqlmblx = " AND  A.MBLX IN('7','1','3')";
            }
            else if (_mblb == "resoys")
            {
                sqlmblx = " AND  A.MBLX='5'";
            }
            else if (_mblb == "resosp")
            {
                sqlmblx = " AND  A.MBLX IN('7','1','3','5')";
            }
            else if (_mblb == "otheryd")
            {
                sqlmblx = " AND  A.MBLX='6'";
            }
            else if (_mblb == "otherdy")
            {
                sqlmblx = " AND  A.MBLX='8'";
            }
            StringBuilder StrSql = new StringBuilder();
            if (JHFADM.Equals(""))
            {
                return "";
            }

            if (_mblb == "respfj" || _mblb == "otheryd" || _mblb == "otherdy")
            {
                if (_mblb == "respfj")
                {
                    StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC, NULL JSDM,NULL CJBM,CASE ZT WHEN '1' THEN '已修改' WHEN '2' THEN '计算错误' WHEN '3' THEN '已计算' ELSE '未批复' END ZT  FROM TB_YSBBMB A");
                    StrSql.Append(" JOIN TB_JHZYML Z ON A.ZYDM=Z.ZYDM");
                    StrSql.Append(" LEFT JOIN TB_PFMBZT ZT ON A.MBDM=ZT.MBDM AND ZT.JHFADM='"+JHFADM+"'");
                    StrSql.Append(" LEFT JOIN XT_CSSZ B ON A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                    StrSql.Append(" LEFT JOIN XT_CSSZ D ON A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                    StrSql.Append(" WHERE A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(sqlmblx);
                }
                else
                {
                    StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC, NULL JSDM,NULL CJBM  FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                    StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND QX.CXQX='1'");
                    StrSql.Append(" AND A.MBDM=QX.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT QZMBDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "')");
                    StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                    StrSql.Append(" AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                    StrSql.Append(sqlmblx);
                    StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                }
                if (ZYDM != "0")
                {
                    StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                }
            }
            else
            {
                //判断当前计划方案是否走流程
                if (IsLCSP(JHFADM))
                {
                    //列表刷新前，先将模板流程表的角色和审批表里的角色更新成一致的
                    string SQL = "UPDATE TB_MBLCSB SET JSDM=T2.JSDM FROM TB_MBLCSB T1,TB_YSMBLC T2 WHERE T1.DQMBDM=T2.QZMBDM AND T1.CJBM=T2.CJBM AND T1.JSDM<>T2.JSDM";
                    ExecuteSql(SQL);

                    //此处为添加发起流程的模版：
                    if (_mblb == "basesb" || _mblb == "resosb" || _mblb == "respys")
                    {
                        //待处理：
                        if (ISDone == "-1")
                        {
                            //流程发起
                            StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,LC.JSDM,LC.CJBM  FROM TB_YSMBLC LC,TB_YSBBMB A,");
                            StrSql.Append("TB_YSLCFS FS,TB_JHFA FA,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D,TB_MBJSQX QX,TB_JHZYML Z ");
                            StrSql.Append(" WHERE LC.QZMBDM=A.MBDM AND LC.LX='" + lx + "' AND LC.MBLX='" + MBLX + "'");
                            StrSql.Append(" AND LC.MBLX=FS.YSLX AND FS.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.JHFADM='" + JHFADM + "' AND FS.ISFQ=1 AND FA.SCBS=1");
                            StrSql.Append(" AND QZMBDM NOT IN (SELECT DISTINCT DQMBDM FROM TB_YSMBLC LC,TB_MBLCSB SB WHERE SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND SB.JHFADM='" + JHFADM + "' AND STATE=1 AND LX='" + lx + "')");
                            StrSql.Append(" AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'");
                            StrSql.Append(" AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            StrSql.Append(" AND D.XMFL='FAZQ'");
                            StrSql.Append(sqlmblx);
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                            StrSql.Append(" UNION");

                        }
                        //此处为添加在模版流程中的模版(主要是审批类)：
                        StrSql.Append(" SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB,TB_JSYH YH,TB_YSBBMB A,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D,TB_MBJSQX QX,TB_YSMBLC LC,TB_JHZYML Z,TB_YSLCFS FS");
                        StrSql.Append(" WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1");
                        StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FS.ISFQ=1");
                        StrSql.Append(" AND SB.JHFADM='" + JHFADM + "' AND SB.STATE IN(" + ISDone + ") AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
                        StrSql.Append(" AND SB.DQMBDM=QX.MBDM AND SB.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                        StrSql.Append(" AND LC.QZMBDM=SB.DQMBDM  AND LC.JSDM=SB.JSDM AND LC.CJBM=SB.CJBM AND LC.LX='" + lx + "' AND LC.MBLX='" + MBLX + "'");
                        StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                        StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                        StrSql.Append(" AND D.XMFL='FAZQ'");
                        StrSql.Append(sqlmblx);
                        StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                        if (ZYDM != "0")
                        {
                            StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                        }
                        //ISDone != "-1"表明不是查询待执行的记录，而是查询已执行的记录
                        if (ISDone != "-1")
                        {
                            StrSql.Append("  AND  NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_MBLCSB S WHERE  S.JHFADM='" + JHFADM + "' AND S.JHFADM=FA.JHFADM AND S.CJBM LIKE LC.CJBM+'%' AND STATE=-1)");
                        }
                    }
                    else
                    {
                        //此处为添加在模版流程中的模版(主要是审批类)：
                        StrSql.Append(" SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB,TB_JSYH YH,TB_YSBBMB A,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D,TB_MBJSQX QX,TB_YSMBLC LC,TB_JHZYML Z,TB_YSLCFS FS");
                        StrSql.Append(" WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1");
                        StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FS.ISFQ=1");
                        StrSql.Append(" AND SB.JHFADM='" + JHFADM + "' AND SB.STATE IN(" + ISDone + ") AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
                        StrSql.Append(" AND SB.DQMBDM=QX.MBDM AND SB.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                        StrSql.Append(" AND LC.QZMBDM=SB.DQMBDM  AND LC.JSDM=SB.JSDM AND LC.CJBM=SB.CJBM AND LC.LX<>'" + lx + "' AND LC.MBLX='" + MBLX + "'");
                        StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                        StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                        StrSql.Append(" AND D.XMFL='FAZQ'");
                        StrSql.Append(sqlmblx);
                        StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                        if (ZYDM != "0")
                        {
                            StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                        }

                        //ISDone != "-1"表明不是查询待执行的记录，而是查询已执行的记录
                        if (ISDone != "-1")
                        {
                            StrSql.Append("  AND  NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_MBLCSB S WHERE  S.JHFADM='" + JHFADM + "' AND S.JHFADM=FA.JHFADM AND S.CJBM LIKE LC.CJBM+'%' AND STATE=-1)");
                            //上报报表下面有填报模板
                            if (_mblb == "baseys" || _mblb == "resoys")
                            {
                                string ML = "";
                                if (_mblb == "baseys")
                                {
                                    ML = "4";
                                }
                                else
                                {
                                    ML = "5";
                                }

                                StrSql.Append(" AND SB.DQMBDM NOT IN(SELECT DISTINCT L.MBDM FROM TB_YSMBLC L,TB_YSBBMB M,TB_JHZYML Z");
                                StrSql.Append(" WHERE L.LX='0' AND L.MBLX='" + MBLX + "' AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX='" + ML + "'");
                                StrSql.Append(" AND M.ZYDM=Z.ZYDM");
                                if (ZYDM != "0")
                                {

                                    StrSql.Append(" AND M.ZYDM='" + ZYDM + "'");
                                }
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_MBLCSB S1 WHERE JHFADM='" + JHFADM + "' AND S1.DQMBDM=L.QZMBDM AND S1.JSDM=L.JSDM AND S1.CJBM=L.CJBM AND STATE=1");
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_MBLCSB B WHERE B.JHFADM='" + JHFADM + "' AND B.DQMBDM=S1.DQMBDM AND S1.JSDM=B.JSDM AND S1.CJBM=B.CJBM AND STATE=-1)))");
                            }
                            string MBDM = GetDHCJBM(JHFADM);
                            if (MBDM != "")
                            {
                                MBDM = "'" + MBDM + "'";
                                StrSql.Append(" AND SB.CJBM NOT IN(" + MBDM + ")");
                            }
                            StrSql.Append(" AND SB.CJBM IN(SELECT MIN(CJBM) FROM TB_MBLCSB S1,TB_MBJSQX Q1,TB_JSYH Y1 WHERE S1.DQMBDM=SB.DQMBDM  AND S1.JHFADM='" + JHFADM + "' AND STATE=1 AND S1.DQMBDM=Q1.MBDM AND S1.JSDM LIKE '%'''+Q1.JSDM+'''%' AND Q1.CXQX='1'  AND S1.JSDM LIKE '%'''+Y1.JSDM+'''%' AND Y1.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "')");

                        }
                    }
                    //将前置模板还在审批，没到这一步的和不走流程的模板也刷出来
                    if (ISDone == "-1")
                    {
                        if (_mblb == "baseys" || _mblb == "resoys")
                        {
                            string DM = "", ML = "";
                            if (_mblb == "baseys")
                            {
                                ML = "4";
                            }
                            else
                            {
                                ML = "5";
                            }
                            //查询上报报表或者预算分解上报报表模板
                            //string sql = "SELECT DISTINCT LC.MBDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_JHFA FA,TB_YSLCFS FS";
                            //sql += " WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)";
                            //sql += " AND LC.MBLX=FS.YSLX AND LC.MBLX='" + MBLX + "' AND FS.JHFADM='" + JHFADM + "' AND FS.ISFQ=1";
                            //sql += " AND FS.JHFADM=FA.JHFADM AND FA.SCBS=1";
                            //sql += " AND LC.MBDM=MB.MBDM AND MB.MBZQ=FA.FABS AND MB.MBLX='" + ML + "' ";
                            //DataSet T = new DataSet();
                            //T.Tables.Clear();
                            //T = Query(sql);
                            //if (T.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int i = 0; i < T.Tables[0].Rows.Count; i++)
                            //    {
                            //        DM = DM == "" ? T.Tables[0].Rows[i]["MBDM"].ToString() : DM + "','" + T.Tables[0].Rows[i]["MBDM"].ToString();
                            //    }
                            //    DM = "'" + DM + "'";
                            //    StrSql.Append(" UNION");
                            //    StrSql.Append("  SELECT DISTINCT A.MBLX,LC.QZMBDM,B.XMMC,A.MBMC,A.MBWJ,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC FROM TB_YSMBLC LC,TB_YSBBMB A,TB_MBJSQX QX,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D");
                            //    StrSql.Append("  WHERE LC.QZMBDM IN(" + DM + ") AND LC.MBLX='" + MBLX + "'");
                            //    StrSql.Append("  AND LC.QZMBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            //    StrSql.Append("  AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            //    StrSql.Append("  AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            //    StrSql.Append("  AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A)  AND B.XMFL='YSMBLX'");
                            //    StrSql.Append("  AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A) AND D.XMFL='FAZQ'");
                            //}
                            bool IsOver;
                            string Q1 = " SELECT L.MBDM FROM TB_YSMBLC L,TB_MBLCSB S,TB_YSBBMB M WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1  AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + MBLX + "' AND S.DQMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + ML + "'";
                            Q1 += " UNION";
                            Q1 += " SELECT L.MBDM FROM TB_YSMBLC L,TB_YSBBMB M";
                            Q1 += "  WHERE L.LX='0' AND L.MBLX='" + MBLX + "'";
                            Q1 += " AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX='" + ML + "'";
                            Q1 += " AND NOT EXISTS(SELECT * FROM TB_MBLCSB S WHERE JHFADM='" + JHFADM + "' AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND STATE=1)";
                            Q1 += " UNION";
                            Q1 += "  SELECT L.MBDM FROM TB_YSMBLC L,TB_YSBBMB M ";
                            Q1 += " WHERE L.LX='0' AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'";
                            Q1 += " AND NOT EXISTS (SELECT DISTINCT S.DQMBDM FROM TB_MBLCSB S,TB_YSMBLC LC WHERE  S.JHFADM='" + JHFADM + "' AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.CJBM=LC.CJBM)";
                            if (Query(Q1).Tables[0].Rows.Count > 0)
                            {
                                IsOver = false;
                            }
                            else
                            {
                                IsOver = true;
                            }
                            StrSql.Append(" UNION");
                            StrSql.Append(" SELECT DISTINCT A.MBLX,LC.QZMBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,LC.JSDM,LC.CJBM FROM TB_YSMBLC LC,TB_YSBBMB A,TB_JHFA F,TB_YSLCFS FQ,TB_MBJSQX QX,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE LC.QZMBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            StrSql.Append(" AND LC.MBLX='" + MBLX + "'");
                            //判断当前的流程是否已经走完
                            if (IsOver == true)
                            {
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_JHFA FA,TB_YSLCFS FS");
                                StrSql.Append(" WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1");
                                StrSql.Append("   AND SB.JHFADM=FA.JHFADM AND FA.SCBS=1");
                                StrSql.Append("   AND SB.DQMBDM=MB.MBDM AND FA.FABS=MB.MBZQ AND MB.MBLX='" + ML + "'");
                                StrSql.Append("   AND FA.JHFADM=FS.JHFADM AND FA.YSBS=FS.YSLX AND FS.ISFQ=1");
                                StrSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND LC.JSDM=SB.JSDM AND LC.CJBM=SB.CJBM)");
                            }
                            StrSql.Append("   AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            StrSql.Append("   AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            StrSql.Append("   AND LC.MBLX=FQ.YSLX AND FQ.JHFADM='" + JHFADM + "' AND FQ.ISFQ=1");
                            StrSql.Append("   AND F.JHFADM=FQ.JHFADM AND F.JHFADM='" + JHFADM + "' AND F.SCBS=1");
                            StrSql.Append("   AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A)  AND B.XMFL='YSMBLX'");
                            StrSql.Append("   AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A) AND D.XMFL='FAZQ'");
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                            string CJ = GetLastCJBM(JHFADM);
                            if (CJ != "")
                            {
                                CJ = "'" + CJ + "'";
                                StrSql.Append("   AND LC.CJBM NOT IN(" + CJ + ")");
                            }
                            //在生成上报时，如果顶节点和他的子节点是相同的，只加载下级列表，不加载顶节点列表
                            if (IsOver == false)
                            {
                                StrSql.Append("  AND LC.CJBM IN(");
                                StrSql.Append(" SELECT DISTINCT SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4) FROM TB_YSMBLC L1,TB_YSMBLC L2,TB_YSBBMB M1,TB_YSBBMB M2");
                                StrSql.Append("  WHERE L1.QZMBDM=L2.QZMBDM AND L1.JSDM=L2.JSDM AND L1.MBLX=L2.MBLX AND L1.MBDM='0' AND SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4)<>NULL");
                                StrSql.Append("    AND L1.MBLX='" + MBLX + "' AND L2.MBLX='" + MBLX + "'");
                                StrSql.Append("    AND L1.QZMBDM=M1.MBDM AND M1.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                                StrSql.Append("    AND L2.QZMBDM=M2.MBDM AND M1.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                                StrSql.Append(" )");
                            }
                            StrSql.Append("  AND LC.CJBM IN(SELECT MAX(CJBM) FROM TB_YSMBLC L1,TB_MBJSQX Q1,TB_JSYH Y1 WHERE L1.QZMBDM=LC.QZMBDM  AND L1.MBLX='" + MBLX + "' AND L1.QZMBDM=Q1.MBDM AND L1.JSDM LIKE '%'''+Q1.JSDM+'''%' AND Q1.CXQX='1'  AND L1.JSDM LIKE '%'''+Y1.JSDM+'''%' AND Y1.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "')");

                            //查询
                            //StrSql.Append(" UNION");
                            //StrSql.Append(" SELECT DISTINCT MB.MBLX,LC.MBDM,B.XMMC,MB.MBMC,MB.MBWJ,MB.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_MBJSQX QX,TB_JSYH YH,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D,TB_YSLCFS FS");
                            //StrSql.Append(" WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)");
                            //StrSql.Append(" AND LC.MBLX=FS.YSLX AND LC.MBLX='" + MBLX + "' AND FS.JHFADM='" + JHFADM + "' AND FS.ISFQ=1");
                            //StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FA.SCBS=1 ");
                            //StrSql.Append(" AND LC.MBDM=MB.MBDM AND MB.MBZQ=FA.FABS");
                            //StrSql.Append(" AND LC.MBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            //StrSql.Append(" AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            //StrSql.Append(" AND MB.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            //StrSql.Append(" AND B.XMFL='YSMBLX' AND MB.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            //StrSql.Append(" AND D.XMFL='FAZQ'");


                            //查询不在流程范围内且用户有权限的模板
                            StrSql.Append(" UNION");
                            StrSql.Append(" SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC,NULL JSDM,NULL CJBM FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND CXQX='1' AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "'");
                            StrSql.Append(" AND QX.MBDM NOT IN (SELECT  DISTINCT QZMBDM FROM TB_YSMBLC LC,TB_JSYH JS WHERE USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM LIKE '%'''+JS.JSDM+'''%')");
                            StrSql.Append(" AND QX.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            StrSql.Append(" AND D.XMFL='FAZQ'");
                            StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT QZMBDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "')");
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                        }
                    }
                }
                else
                {
                    if (ISDone == "-1")
                    {

                        if (_mblb != "basesp" && _mblb != "respsp" && _mblb != "resosp")
                        {
                            //此处为不在流程中的模版处理：
                            StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC,NULL JSDM,NULL CJBM  FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND QX.CXQX='1'");
                            StrSql.Append(" AND A.MBDM=QX.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                            StrSql.Append(" AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                            StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT QZMBDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "')");
                            StrSql.Append(sqlmblx);
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                        }
                    }
                }
            }
            return StrSql.ToString();
        }
        /// <summary>
        /// 获取还未处理的前置模板
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLB">模板代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <returns></returns>
        public string GetDCLQZMBStr(string JHFADM, string MBDM, string MBLB, string ISDone)
        {
            int t = 1;
            string MBStr = "", DM = "", MBTJ = "";
            if (ISDone == "-1")
            {
                //表示当前模板不走流程或者流程走到此模板的前置模板
                if (Query("SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "'  AND SB.DQMBDM='" + MBDM + "' AND SB.STATE IN(" + ISDone + ")").Tables[0].Rows.Count == 0)
                {
                    string LX;   //预算标识（3代表预算上报、5代表预算分解）
                    DataSet DS = new DataSet();
                    StringBuilder StrSql = new StringBuilder();
                    if (MBLB == "baseys")
                    {
                        LX = "3";
                        MBTJ = " AND A.MBLX='4'";
                    }
                    else
                    {
                        LX = "5";
                        MBTJ = " AND A.MBLX='5'";
                    }
                    //AND S.DQMBDM=SB.DQMBDM AND S.JSDM=SB.JSDM AND S.CJBM=SB.CJBM
                    StrSql.Append(" SELECT DISTINCT QZMBDM FROM TB_YSMBLC LC,TB_YSBBMB A");
                    StrSql.Append(" WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA,TB_YSMBLC C WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.DQMBDM=C.QZMBDM AND SB.JSDM=C.JSDM AND C.MBDM='" + MBDM + "' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1");
                    StrSql.Append(" AND NOT EXISTS(SELECT DISTINCT DQMBDM FROM TB_MBLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1 AND S.CJBM LIKE '%'+SB.CJBM+'%'))");
                    StrSql.Append(" AND LC.MBDM='" + MBDM + "' AND LC.MBLX='" + LX + "'");
                    StrSql.Append(" AND LC.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(MBTJ);
                    DS = Query(StrSql.ToString());
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                        {
                            DM = DM == "" ? DS.Tables[0].Rows[i]["QZMBDM"].ToString() : DM + "','" + DS.Tables[0].Rows[i]["QZMBDM"].ToString();
                        }
                        DM = "'" + DM + "'";
                        DS.Tables.Clear();
                        DS = Query("SELECT DISTINCT MBMC,LC.JSDM FROM TB_YSMBLC LC,TB_YSBBMB A WHERE QZMBDM NOT IN (" + DM + ") AND LC.MBDM='" + MBDM + "' AND LC.MBLX='" + LX + "' AND LC.QZMBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND LC.QZMBDM<>'" + MBDM + "'");
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                            {
                                //MBStr = MBStr == "" ? DS.Tables[0].Rows[j]["MBMC"].ToString() : ((j != 0 && j % 2 != 0) ? MBStr + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() : MBStr + "\r\n" + DS.Tables[0].Rows[j]["MBMC"].ToString());
                                MBStr = MBStr == "" ? t.ToString() + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() + "----" + GetJSNameByJSDM(DS.Tables[0].Rows[j]["JSDM"].ToString()) : MBStr + "<br>" + t.ToString() + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() + "----" + GetJSNameByJSDM(DS.Tables[0].Rows[j]["JSDM"].ToString());
                                t++;
                            }
                        }
                    }
                    else
                    {

                        MBStr = Query("SELECT MBMC FROM TB_YSBBMB WHERE MBDM='" + MBDM + "' AND MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'").Tables[0].Rows[0]["MBMC"].ToString() + "的所有前置模板";
                    }
                }
            }
            //MBStr = MBStr == "" ? MBStr : MBStr + "<br><br>未处理！";
            return MBStr;
        }
        /// <summary>
        /// 获取预算类型
        /// </summary>
        /// <param name="_mblb">模板类别</param>
        /// <returns></returns>
        public string GetYSLX(string _mblb)
        {
            string MBLX = "";
            //设置预算类型（3代表预算上报；4代表预算批复；5代表预算分解）
            if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "baseys")
            {
                MBLX = "3";
            }
            else if (_mblb == "respfj" || _mblb == "respys" || _mblb == "respsp")
            {
                MBLX = "4";
            }
            else if (_mblb == "resosb" || _mblb == "resoys" || _mblb == "resosp")
            {
                MBLX = "5";
            }
            return MBLX;
        }
        /// <summary>
        /// 执行审批状态还原方法
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public int SPZTHY(string JHFADM, string MBDM, string JSDM, string CJBM)
        {
            int Flag = 0;
            string LX = "";
            DataSet DS = new DataSet();
            //定义存储参数的集合对象
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            //删除选中的审批记录
            DelSPZT(JHFADM, MBDM, JSDM, CJBM, ref ArrList, ref List);
            //如果当前打回模板是填报模板的话，则不需要生成状态为-1的记录
            DS = Query("SELECT LX FROM TB_YSMBLC WHERE CJBM='" + CJBM + "' AND MBLX='" + GetYSBSByJHFADM(JHFADM) + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                LX = DS.Tables[0].Rows[0]["LX"].ToString();
                if (LX != "0" && LX != "" && LX != null)
                {
                    //生成新的审批记录
                    SCNextSP(ref ArrList, ref List, MBDM, JSDM, JHFADM, CJBM);
                }
                //递归生成审批状态还原SQL语句
                GetZTHYSQL(JHFADM, MBDM, JSDM, CJBM, ref ArrList, ref List);
                Flag = DbHelperOra.ExecuteSql(ArrList.ToArray(), List);
            }
            return Flag;
        }
        /// <summary>
        /// 删除指定的审批记录
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="ArrList">SQL集合</param>
        /// <param name="List">参数集合</param>
        public void DelSPZT(string JHFADM, string MBDM, string JSDM, string CJBM, ref List<String> ArrList, ref ArrayList List)
        {
            string SQL = "DELETE FROM TB_MBLCSB WHERE JHFADM=@JHFADM AND DQMBDM=@MBDM AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM=@CJBM";
            ArrList.Add(SQL);
            AseParameter[] Del = { 
                            new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                            new AseParameter("@MBDM", AseDbType.VarChar, 40),
                            new AseParameter("@CJBM", AseDbType.VarChar,40)
                        };
            Del[0].Value = JHFADM;
            Del[1].Value = MBDM;
            Del[2].Value = CJBM;
            List.Add(Del);
        }
        /// <summary>
        /// 根据计划方案代码获取预算标识
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetYSBSByJHFADM(string JHFADM)
        {
            string YSBS = "";
            if (JHFADM != "")
            {
                DataSet DS = new DataSet();
                string SQL = "SELECT YSBS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    YSBS = DS.Tables[0].Rows[0]["YSBS"].ToString();
                }
            }
            return YSBS;
        }
        /// <summary>
        /// 获取状态还原的SQL语句
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="ArrList">SQL集合</param>
        /// <param name="List">参数集合</param>
        public void GetZTHYSQL(string JHFADM, string MBDM, string JSDM, string CJBM, ref List<String> ArrList, ref ArrayList List)
        {
            string SQL = "";
            //表示删除的是根节点（此时判断当前操作结点是不是别的模板的子节点，如果是的话，一并将其删除）
            if (CJBM.Length != 4)
            {
                SQL = "SELECT DISTINCT JHFADM,DQMBDM,SB.JSDM,SB.CJBM FROM TB_YSMBLC LC,TB_MBLCSB SB WHERE LC.CJBM='" + CJBM.Substring(0, CJBM.Length - 4) + "' AND LC.MBLX='" + GetYSBSByJHFADM(JHFADM) + "' AND LC.QZMBDM=SB.DQMBDM AND LC.JSDM=SB.JSDM AND LC.CJBM=SB.CJBM AND SB.JHFADM='" + JHFADM + "'";
                DataSet DS = new DataSet();
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    //删除选中的审批记录
                    DelSPZT(JHFADM, DS.Tables[0].Rows[0]["DQMBDM"].ToString(), DS.Tables[0].Rows[0]["JSDM"].ToString(), DS.Tables[0].Rows[0]["CJBM"].ToString(), ref ArrList, ref List);
                    //递归
                    GetZTHYSQL(JHFADM, DS.Tables[0].Rows[0]["DQMBDM"].ToString(), DS.Tables[0].Rows[0]["JSDM"].ToString(), DS.Tables[0].Rows[0]["CJBM"].ToString(), ref ArrList, ref List);
                }
            }
            else
            {
                //查询有没有作为别的模板的子节点
                SQL = "SELECT DISTINCT JHFADM,DQMBDM,SB.JSDM,SB.CJBM FROM TB_YSMBLC LC,TB_MBLCSB SB";
                SQL += " WHERE LC.CJBM IN(SELECT DISTINCT SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4) FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + CJBM + "' AND MBLX='" + GetYSBSByJHFADM(JHFADM) + "') AND LC.MBLX='" + GetYSBSByJHFADM(JHFADM) + "'";
                SQL += "  AND LC.QZMBDM=SB.DQMBDM AND LC.JSDM=SB.JSDM AND LC.CJBM=SB.CJBM AND SB.JHFADM='" + JHFADM + "'";
                DataSet DT = new DataSet();
                DT.Tables.Clear();
                DT = Query(SQL);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Tables[0].Rows.Count; i++)
                    {
                        //删除选中的审批记录
                        DelSPZT(JHFADM, DT.Tables[0].Rows[i]["DQMBDM"].ToString(), DT.Tables[0].Rows[i]["JSDM"].ToString(), DT.Tables[0].Rows[i]["CJBM"].ToString(), ref ArrList, ref List);
                        //删除当前模板作为别的模板子节点的父节点
                        GetZTHYSQL(JHFADM, MBDM, JSDM, DT.Tables[0].Rows[i]["CJBM"].ToString(), ref ArrList, ref List);
                    }
                }
            }
        }
        /// <summary>
        /// 根据计划方案代码，获取已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetIsOverMBDM(string JHFADM)
        {
            string MBDM = "";
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            string SQL = "SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB WHERE JHFADM='" + JHFADM + "' AND STATE=-1";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MBDM = MBDM == "" ? DS.Tables[0].Rows[i]["DQMBDM"].ToString() : MBDM + "','" + DS.Tables[0].Rows[i]["DQMBDM"].ToString();
                    //递归
                    DGMBDM(JHFADM, ref MBDM, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["DQMBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString());
                }
            }
            return MBDM;
        }
        /// <summary>
        /// 递归查询已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBStr">模板代码字符串</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        public void DGMBDM(string JHFADM, ref string MBStr, string CJBM, string MBDM, string JSDM)
        {
            string SQL = "";
            DataSet DT = new DataSet();
            if (CJBM.Length != 4)
            {
                SQL = " SELECT DISTINCT S.DQMBDM,S.JSDM,S.CJBM FROM TB_YSMBLC L,TB_MBLCSB S WHERE S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.CJBM='" + CJBM.ToString().Substring(0, CJBM.ToString().Length - 4) + "' AND L.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND S.STATE=1 AND S.JHFADM='" + JHFADM + "'";

            }
            else
            {
                SQL = " SELECT DISTINCT S.DQMBDM,S.JSDM,S.CJBM FROM TB_YSMBLC L,TB_MBLCSB S WHERE QZMBDM IN(SELECT MBDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + CJBM + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "') AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND JHFADM='" + JHFADM + "' AND STATE=1  AND L.MBLX='" + GetYSLXByJHFADM(JHFADM) + "'";

            }
            DT = Query(SQL);
            if (DT.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                {
                    MBStr = MBStr + "','" + DT.Tables[0].Rows[j]["DQMBDM"].ToString();
                    //递归
                    DGMBDM(JHFADM, ref MBStr, DT.Tables[0].Rows[j]["CJBM"].ToString(), DT.Tables[0].Rows[j]["DQMBDM"].ToString(), DT.Tables[0].Rows[j]["JSDM"].ToString());
                }
            }
        }
        /// <summary>
        /// 根据计划方案代码，获取已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetParCJBM(string JHFADM, string MBDM, string MBLX)
        {
            string CJ = "";
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            StringBuilder StrSql = new StringBuilder();
            //当前流程都已走完，要求顶节点还可以打回的情况
            if (IsLastOne(JHFADM, MBDM))
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB,TB_YSMBLC LC,TB_JSYH YH ");
                strSql.Append(" WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1");
                strSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND LC.MBDM='0'");
                strSql.Append("   AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
                DS = Query(strSql.ToString());
                if (DS.Tables[0].Rows.Count > 0)
                {
                    CJ = DS.Tables[0].Rows[0]["CJBM"].ToString();
                }
            }
            else
            {
                string SQL = "SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB WHERE JHFADM='" + JHFADM + "' AND STATE=-1";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        if (CJ != "")
                        {
                            break;
                        }
                        else
                        {
                            //递归
                            DGCJBM(JHFADM, ref CJ, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["DQMBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString(), MBDM);
                        }
                    }
                }
                if (CJ == "")
                {
                    if (MBLX == "baseys" || MBLX == "resoys")
                    {
                        string ML = "";
                        if (MBLX == "baseys")
                        {
                            ML = "4";
                        }
                        else
                        {
                            ML = "5";
                        }
                        StrSql.Append(" SELECT DISTINCT SUBSTRING(L.CJBM,1,DATALENGTH(L.CJBM)-4) CJBM FROM TB_YSMBLC L,TB_YSBBMB M");
                        StrSql.Append(" WHERE L.LX='0' AND L.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX='" + ML + "'");
                        StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_MBLCSB S WHERE JHFADM='" + JHFADM + "' AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND STATE=1");
                        StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_MBLCSB B WHERE B.JHFADM='" + JHFADM + "' AND B.DQMBDM=S.DQMBDM AND S.JSDM=B.JSDM AND S.CJBM=B.CJBM AND STATE=-1))");
                        DS.Tables.Clear();
                        DS = Query(StrSql.ToString());
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            CJ = DS.Tables[0].Rows[0]["CJBM"].ToString();
                        }
                    }
                }
            }
            return CJ;
        }
        /// <summary>
        /// 递归查询已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBStr">模板代码字符串</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        public void DGCJBM(string JHFADM, ref string MBStr, string CJBM, string MBDM, string JSDM, string DM)
        {
            string SQL = "", BM = "";
            DataSet DT = new DataSet();
            if (CJBM.Length != 4)
            {
                BM = CJBM.ToString().Substring(0, CJBM.ToString().Length - 4);
                SQL = " SELECT DISTINCT QZMBDM MBDM,JSDM,CJBM,MB.MBLX FROM TB_YSMBLC LC,TB_YSBBMB MB  WHERE LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'  AND CJBM='" + BM + "' AND LC.MBLX='" + GetYSLXByJHFADM(JHFADM) + "'";
                DT = Query(SQL);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    if (DT.Tables[0].Rows[0]["MBDM"].ToString() == DM)
                    {
                        MBStr = DT.Tables[0].Rows[0]["CJBM"].ToString();
                    }
                    else
                    {
                        //递归
                        DGCJBM(JHFADM, ref MBStr, DT.Tables[0].Rows[0]["CJBM"].ToString(), DT.Tables[0].Rows[0]["MBDM"].ToString(), DT.Tables[0].Rows[0]["JSDM"].ToString(), DM);
                    }
                }
            }
            else
            {
                SQL = " SELECT DISTINCT LC.MBDM,JSDM,CJBM,MB.MBLX FROM TB_YSMBLC LC,TB_YSBBMB MB  WHERE LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'  AND QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + CJBM + "' AND LC.MBLX='" + GetYSLXByJHFADM(JHFADM) + "'";
                DT = Query(SQL);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                    {
                        if (DT.Tables[0].Rows[j]["MBDM"].ToString() == DM)
                        {
                            MBStr = DT.Tables[0].Rows[j]["CJBM"].ToString().Substring(0, DT.Tables[0].Rows[j]["CJBM"].ToString().Length - 4);
                            break;
                        }
                        else
                        {
                            //递归
                            DGCJBM(JHFADM, ref MBStr, DT.Tables[0].Rows[j]["CJBM"].ToString(), DT.Tables[0].Rows[j]["MBDM"].ToString(), DT.Tables[0].Rows[j]["JSDM"].ToString(), DM);
                        }
                    }
                }

            }
        }
        /// <summary>
        /// 根据计划方案代码，获取已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetDHCJBM(string JHFADM)
        {
            string MBDM = "";
            DataSet DS = new DataSet();
            DataSet DT = new DataSet();
            string SQL = "SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB WHERE JHFADM='" + JHFADM + "' AND STATE=-1";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    MBDM = MBDM == "" ? DS.Tables[0].Rows[i]["CJBM"].ToString() : MBDM + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                    //递归
                    DGDHCJ(JHFADM, ref MBDM, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["DQMBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString());
                }
            }
            return MBDM;
        }
        /// <summary>
        /// 递归查询已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBStr">模板代码字符串</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        public void DGDHCJ(string JHFADM, ref string MBStr, string CJBM, string MBDM, string JSDM)
        {
            string SQL = "", BM = "";
            DataSet DT = new DataSet();
            DataSet DS = new DataSet();
            if (CJBM.Length != 4)
            {
                BM = CJBM.ToString().Substring(0, CJBM.ToString().Length - 4);
                SQL = " SELECT DISTINCT S.DQMBDM,S.JSDM,S.CJBM FROM TB_YSMBLC L,TB_MBLCSB S WHERE S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.CJBM='" + BM + "' AND L.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND S.STATE=1 AND S.JHFADM='" + JHFADM + "'";
                DT = Query(SQL);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    MBStr = MBStr + "','" + DT.Tables[0].Rows[0]["CJBM"].ToString();
                    if (BM.Length == 4)
                    {
                        SQL = "SELECT DISTINCT SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4) CJBM FROM TB_YSMBLC WHERE QZMBDM='" + DT.Tables[0].Rows[0]["DQMBDM"].ToString() + "' AND JSDM='" + DT.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND CJBM<>'" + DT.Tables[0].Rows[0]["CJBM"].ToString() + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "'";
                        DS.Tables.Clear();
                        DS = Query(SQL);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                MBStr = MBStr + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                            }
                        }
                    }
                    //递归
                    DGDHCJ(JHFADM, ref MBStr, DT.Tables[0].Rows[0]["CJBM"].ToString(), DT.Tables[0].Rows[0]["DQMBDM"].ToString(), DT.Tables[0].Rows[0]["JSDM"].ToString());
                }

            }
            else
            {
                SQL = " SELECT DISTINCT S.DQMBDM,S.JSDM,S.CJBM FROM TB_YSMBLC L,TB_MBLCSB S WHERE QZMBDM IN(SELECT MBDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + CJBM + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "') AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND JHFADM='" + JHFADM + "' AND STATE=1  AND L.MBLX='" + GetYSLXByJHFADM(JHFADM) + "'";
                DT.Tables.Clear();
                DT = Query(SQL);
                if (DT.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < DT.Tables[0].Rows.Count; j++)
                    {
                        MBStr = MBStr + "','" + DT.Tables[0].Rows[j]["CJBM"].ToString();
                        //递归
                        DGDHCJ(JHFADM, ref MBStr, DT.Tables[0].Rows[j]["CJBM"].ToString(), DT.Tables[0].Rows[j]["DQMBDM"].ToString(), DT.Tables[0].Rows[j]["JSDM"].ToString());
                    }
                }

            }

        }
        /// <summary>
        /// 判断该模版是否是没有审批只有上报
        /// </summary>
        /// <param name="MBDM">模板代码</param>
        public Boolean IsNoSp(string MBDM)
        {
            DataSet DS = new DataSet();
            DS = DbHelperOra.Query("SELECT * FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND (MBDM='" + MBDM + "' OR MBDM='0') AND LX='1'");
            if (DS.Tables[0].Rows.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 判断当前节点是不是最后一个节点
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns>true代表是；false代表不是</returns>
        public bool IsLastOne(string JHFADM,string MBDM)
        {
            bool Flag = false;
            DataSet DS = new DataSet();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB,TB_YSMBLC LC,TB_JSYH YH ");
            strSql.Append(" WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1");
            strSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND LC.MBDM='0'");
            strSql.Append("   AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
            DS = Query(strSql.ToString());
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (Query("SELECT * FROM TB_YSMBLC WHERE QZMBDM='" + DS.Tables[0].Rows[0]["DQMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND CJBM<>'" + DS.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count == 0)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        /// <summary>
        /// 判断当前节点是不是最后一个节点
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns>true代表是；false代表不是</returns>
        public string GetLastCJBM(string JHFADM)
        {
            DataSet DT = new DataSet();
            string MBDM = "", JSDM = "", CJBM = "",CJ="";
            StringBuilder strSql = new StringBuilder();
             //已经审批过
            string SQL = "SELECT DISTINCT SB.CJBM,SB.JSDM,SB.DQMBDM FROM TB_MBLCSB SB ,TB_JSYH YH,TB_YSMBLC LC WHERE SB.CJBM=LC.CJBM AND LC.LX<>'0' AND LC.MBLX='" +  GetYSLXByJHFADM(JHFADM) + "' AND JHFADM='" + JHFADM + "' AND LC.MBDM='0' AND  STATE=1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
            DT = Query(SQL);
            if (DT.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DT.Tables[0].Rows.Count; i++)
                {
                    MBDM = DT.Tables[0].Rows[i]["DQMBDM"].ToString();
                    JSDM = DT.Tables[0].Rows[i]["JSDM"].ToString();
                    CJBM = DT.Tables[0].Rows[i]["CJBM"].ToString();
                    if (Query("SELECT * FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND CJBM<>'" + CJBM + "'").Tables[0].Rows.Count == 0)
                    {
                        CJ = CJ == "" ? CJBM : CJ + "','" + CJBM;
                    }
                }
            }
            return CJ;
        }
        /// <summary>
        /// 查询计划方案下所有的模板状态
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ZYDM">作业代码</param>
        /// <returns></returns>
        public string GetAllMBCXJSON(string JHFADM,string ZYDM,string MBMC,string HSZXDM,string ZT)
        {
            int Total = 0;
            DataSet dsValue = new DataSet();
           
            StringBuilder Str = new StringBuilder();
            StringBuilder strSql = new StringBuilder();
            string COLUMNNAME = "", JSNAME = "",MBZQ="",MBLX="",DM="";
            //CJBM = GetDCLCJBM(JHFADM, "", MBZQ, MBLX);
            DataSet DS = getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
            }

            //当状态为全部、未处理、已处理时，才会获取待填报和待审批的所有上级模板代码
            if (ZT.Equals("-1") || ZT.Equals("2") || ZT.Equals("3"))
            {
                //获取当前待填报、待处理以及其所有父节点所有的层级编码
                DM = GetDCLParMB(JHFADM, "", MBZQ, MBLX);
            }
            //当状态为全部时，加载下面所有的sql语句
            //当状态为未处理或者全部时
            if (ZT.Equals("-1") || ZT.Equals("2"))
            {
                strSql.Append(" SELECT DISTINCT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'未处理' STATE, CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.QZMBDM=A.QZMBDM AND NL.JSDM=A.JSDM AND NL.CJBM<>A.CJBM AND NL.MBLX='" + MBLX + "' AND NL.MBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND NL.MBLX='" + MBLX + "' AND NL.QZMBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') END NEXTMBMC,CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM IN(SELECT DISTINCT SUBSTRING(N.CJBM,1,DATALENGTH(N.CJBM)-4) FROM TB_YSMBLC N  WHERE N.QZMBDM=A.QZMBDM AND N.JSDM=A.JSDM AND N.MBLX='" + MBLX + "' AND N.CJBM<>A.CJBM) AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT L.JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') END NEXTJSDM,NULL NEXTJSNAME,A.CJBM FROM  TB_YSBBMB B,TB_YSMBLC A,TB_JHZYML C");
                strSql.Append("  WHERE A.QZMBDM IN ('"+DM.Replace(",","','")+"')");
                strSql.Append("  AND A.QZMBDM=B.MBDM AND A.CJBM IN(SELECT MAX(CJBM) FROM TB_YSMBLC WHERE QZMBDM=B.MBDM AND MBLX='" + MBLX + "' AND CJBM NOT IN(SELECT L1.CJBM FROM TB_YSMBLC L1,TB_YSMBLC L2,TB_YSBBMB B1 WHERE L1.JSDM=L2.JSDM AND L2.QZMBDM=L1.QZMBDM AND L1.CJBM<>L2.CJBM AND L1.CJBM LIKE '%'+SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4)+'%' AND L1.MBLX='" + MBLX + "' AND L2.MBLX='" + MBLX + "' AND B1.MBDM=L1.QZMBDM AND B1.MBZQ='" + MBZQ + "')) AND A.MBLX='" + MBLX + "'");
                strSql.Append("  AND A.LX<>'0'");
                strSql.Append("  AND B.ZYDM=C.ZYDM");
                if (ZYDM != "0")
                {
                    strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
                }
                strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
            }
            //当状态为已处理或者全部时
            if (ZT.Equals("3") || ZT.Equals("-1"))
            {
                if (!strSql.ToString().Trim().Equals(""))
                {
                    strSql.Append(" UNION");
                }
                strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'已处理' STATE, CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.QZMBDM=A.DQMBDM AND NL.JSDM=A.JSDM AND NL.CJBM<>A.CJBM AND NL.MBLX='" + MBLX + "' AND NL.MBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND NL.MBLX='" + MBLX + "' AND NL.QZMBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') END NEXTMBMC,CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM IN(SELECT DISTINCT SUBSTRING(N.CJBM,1,DATALENGTH(N.CJBM)-4) FROM TB_YSMBLC N  WHERE N.QZMBDM=A.DQMBDM AND N.JSDM=A.JSDM AND N.MBLX='" + MBLX + "' AND N.CJBM<>A.CJBM) AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT L.JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') END NEXTJSDM,NULL NEXTJSNAME,A.CJBM FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C");
                strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='" + MBZQ + "'");
                strSql.Append(" AND A.CLSJ IN(SELECT MAX(CLSJ) FROM TB_MBLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.DQMBDM=A.DQMBDM AND S.DQMBDM NOT IN('" + DM.Replace(",", "','") + "') AND NOT EXISTS(SELECT * FROM TB_MBLCSB S2 WHERE S2.JHFADM='" + JHFADM + "' AND  S2.DQMBDM=S.DQMBDM AND STATE=-1 ))");
                //strSql.Append(" AND A.CLSJ IN(SELECT MAX(CLSJ) FROM TB_MBLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.DQMBDM=A.DQMBDM AND NOT EXISTS(SELECT * FROM TB_MBLCSB S2 WHERE S2.JHFADM='" + JHFADM + "' AND  S2.DQMBDM=S.DQMBDM AND STATE=-1 ))");
                strSql.Append(" AND A.JHFADM='" + JHFADM + "'");
                strSql.Append("  AND B.ZYDM=C.ZYDM");
                if (ZYDM != "0")
                {
                    strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
                }
                strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
            }
            //当状态为待填报或者全部时
            if (ZT.Equals("0") || ZT.Equals("-1"))
            {
                if (!strSql.ToString().Trim().Equals(""))
                {
                    strSql.Append(" UNION");
                }
                strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待填报' STATE, CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.QZMBDM=A.QZMBDM AND NL.JSDM=A.JSDM AND NL.CJBM<>A.CJBM AND NL.MBLX='" + MBLX + "' AND NL.MBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND NL.MBLX='" + MBLX + "' AND NL.QZMBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') END NEXTMBMC,CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM IN(SELECT DISTINCT SUBSTRING(N.CJBM,1,DATALENGTH(N.CJBM)-4) FROM TB_YSMBLC N  WHERE N.QZMBDM=A.QZMBDM AND N.JSDM=A.JSDM AND N.MBLX='" + MBLX + "' AND N.CJBM<>A.CJBM) AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT L.JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') END NEXTJSDM,NULL NEXTJSNAME,A.CJBM  FROM  TB_YSMBLC A,TB_YSBBMB B,TB_JHZYML C,TB_YSLCFS FS");
                strSql.Append(" WHERE A.QZMBDM=B.MBDM AND A.MBLX='"+MBLX+"' AND B.MBZQ='"+MBZQ+"'  AND A.MBLX=FS.YSLX AND FS.ISFQ=1 AND FS.JHFADM='"+JHFADM+"' AND LX='0'");
                strSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND S.JHFADM='"+JHFADM+"' AND S.DQMBDM=A.QZMBDM AND S.JSDM=A.JSDM AND S.CJBM=A.CJBM AND STATE=1 AND LX='0' AND L.MBLX='"+MBLX+"')");
                strSql.Append("  AND B.ZYDM=C.ZYDM");
                if (ZYDM != "0")
                {
                    strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
                }
                strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
                strSql.Append(" UNION");
                strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待填报' STATE, CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.QZMBDM=A.DQMBDM AND NL.JSDM=A.JSDM AND NL.CJBM<>A.CJBM AND NL.MBLX='" + MBLX + "' AND NL.MBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND NL.MBLX='" + MBLX + "' AND NL.QZMBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') END NEXTMBMC,CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM IN(SELECT DISTINCT SUBSTRING(N.CJBM,1,DATALENGTH(N.CJBM)-4) FROM TB_YSMBLC N  WHERE N.QZMBDM=A.DQMBDM AND N.JSDM=A.JSDM AND N.MBLX='" + MBLX + "' AND N.CJBM<>A.CJBM) AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT L.JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') END NEXTJSDM,NULL NEXTJSNAME,A.CJBM  FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C,TB_YSMBLC LC");
                strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='"+MBZQ+"' AND A.JHFADM='"+JHFADM+"' AND A.STATE=-1 AND A.DQMBDM=LC.QZMBDM AND A.JSDM=A.JSDM AND A.CJBM=LC.CJBM AND LC.MBLX='"+MBLX+"' AND LC.LX='0'");
                strSql.Append("  AND B.ZYDM=C.ZYDM");
                if (ZYDM != "0")
                {
                    strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
                }
                strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
            }
            //当状态为待审核或者全部时
            if (ZT.Equals("1") || ZT.Equals("-1"))
            {
                if (!strSql.ToString().Trim().Equals(""))
                {
                    strSql.Append(" UNION");
                }
                strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待审核' STATE, CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.QZMBDM=A.DQMBDM AND NL.JSDM=A.JSDM AND NL.CJBM<>A.CJBM AND NL.MBLX='" + MBLX + "' AND NL.MBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT NM.MBMC FROM TB_YSMBLC NL,TB_YSBBMB NM WHERE NL.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND NL.MBLX='" + MBLX + "' AND NL.QZMBDM=NM.MBDM AND NM.MBZQ='" + MBZQ + "') END NEXTMBMC,CASE DATALENGTH(A.CJBM) WHEN 4  THEN (SELECT DISTINCT JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM IN(SELECT DISTINCT SUBSTRING(N.CJBM,1,DATALENGTH(N.CJBM)-4) FROM TB_YSMBLC N  WHERE N.QZMBDM=A.DQMBDM AND N.JSDM=A.JSDM AND N.MBLX='" + MBLX + "' AND N.CJBM<>A.CJBM) AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') ELSE (SELECT DISTINCT L.JSDM FROM TB_YSMBLC L,TB_YSBBMB M WHERE L.CJBM=SUBSTRING(A.CJBM,1,DATALENGTH(A.CJBM)-4)  AND L.MBLX='" + MBLX + "' AND L.QZMBDM=M.MBDM AND M.MBZQ='" + MBZQ + "') END NEXTJSDM,NULL NEXTJSNAME,A.CJBM  FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C,TB_YSMBLC LC");
                strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='"+MBZQ+"'  AND A.JHFADM='"+JHFADM+"' AND A.STATE=-1 AND A.DQMBDM=LC.QZMBDM AND A.JSDM=A.JSDM AND A.CJBM=LC.CJBM AND LC.MBLX='"+MBLX+"' AND LC.LX<>'0'");
                strSql.Append("  AND B.ZYDM=C.ZYDM");
                if (ZYDM != "0")
                {
                    strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
                }
                strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
            }
            strSql.Append("  ORDER BY C.ZYMC,STATE");
            dsValue = Query(strSql.ToString());
            //获取查询条件的总行数
            Total = dsValue.Tables[0].Rows.Count;
            Str.Append("{\"total\":" + Total + ",");
            Str.Append("\"rows\":[");
            for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
            {
                Str.Append("{");
                for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
                {
                    COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                    if (COLUMNNAME == "DQJSDM" || COLUMNNAME == "NEXTJSDM")
                    {
                        JSNAME = GetJSNameByJSDM(dsValue.Tables[0].Rows[i][COLUMNNAME].ToString());
                    }
                    Str.Append("\"" + COLUMNNAME + "\"");
                    Str.Append(":");
                    if (COLUMNNAME == "DQJSNAME" || COLUMNNAME == "NEXTJSNAME")
                    {
                        Str.Append("\"" + JSNAME + "\"");
                    } 
                    else
                    {
                        Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                    }
                    if (j < dsValue.Tables[0].Columns.Count - 1)
                    {
                        Str.Append(",");
                    }
                }
                if (i < dsValue.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}");
                }
            }
            Str.Append("]}");
            return Str.ToString();
        }
        /// <summary>
        /// 根据模板关联下级模板
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetDCLChirldMBName(string JHFADM,string MBDM,string JSDM) {
            int t = 1;
            string MBMC = "",MBZQ="",MBLX="",SQL="";
            DataSet DS = getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
            }
            //获取待填报和待审批的层级编码以及父节点的层级编码
            //string CJ="'"+GetDCLCJBM(JHFADM, "", MBZQ, MBLX)+"'";
            SQL = "SELECT DISTINCT CHILDMBDM,CHILDJSDM,CHILDCJBM,MBMC FROM TB_YSMBXJB JB,TB_YSMBLC LC,TB_YSBBMB MB WHERE JB.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' AND LC.QZMBDM='" + MBDM + "' AND LC.JSDM='"+JSDM.Replace("'","''")+"' AND JB.CHILDMBDM=MB.MBDM AND MB.MBZQ='"+MBZQ+"'";
            DS.Tables.Clear();
            DS=Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                //
                //SELECT * FROM TB_MBLCSB S1 WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + DS.Tables[0].Rows[i]["CHILDMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[i]["CHILDJSDM"].ToString().Replace("'", "''") + "' AND STATE=1 AND CJBM NOT IN("+CJ+")
                if (Query("SELECT * FROM TB_MBLCSB S1 WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + DS.Tables[0].Rows[i]["CHILDMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[i]["CHILDJSDM"].ToString().Replace("'", "''") + "' AND STATE=1 AND NOT EXISTS(SELECT * FROM TB_MBLCSB S2 WHERE JHFADM='" + JHFADM + "' AND STATE=-1 AND S2.CJBM LIKE '%+S1.CJBM+%')").Tables[0].Rows.Count == 0)
                {
                    MBMC = MBMC == "" ? t.ToString() + "、" + DS.Tables[0].Rows[i]["MBMC"].ToString() + "----" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["CHILDJSDM"].ToString()) : MBMC + "<br>" + t.ToString() + "、" + DS.Tables[0].Rows[i]["MBMC"].ToString() + "----" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["CHILDJSDM"].ToString());
                    t++;
                }
            }
            //if (MBMC != "") {
            //    MBMC = MBMC + "<br><br>" + "还未处理！";
            //}
            return MBMC;
        }
        /// <summary>
        /// 查询待处理模板的所有上级的层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns></returns>
        public string GetDCLCJBM(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            string CJStr = "";
            DataSet DS = new DataSet();
            StringBuilder StrSql = new StringBuilder();
            StrSql.Append("SELECT DISTINCT LC.CJBM,LC.QZMBDM MBDM,LC.JSDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHFA FA");
            StrSql.Append(" WHERE LC.MBLX='" + MBLX + "' AND LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND LC.MBLX=FS.YSLX AND FS.ISFQ=1  AND FA.SCBS=1 AND FA.JHFADM=FS.JHFADM AND MB.MBZQ=FA.FABS AND LX='0' AND FA.JHFADM='" + JHFADM + "'");
            StrSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + MBLX + "' AND S.JHFADM=FA.JHFADM AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.JHFADM='" + JHFADM + "' AND STATE=1 AND LX='0'");
            StrSql.Append(" AND NOT EXISTS (SELECT * FROM TB_YSMBLC C,TB_MBLCSB B WHERE B.DQMBDM=S.DQMBDM AND B.JSDM=S.JSDM AND B.CJBM=S.CJBM AND B.JHFADM=S.JHFADM AND  B.DQMBDM=C.QZMBDM AND B.JSDM=C.JSDM AND B.CJBM=C.CJBM AND C.MBLX='" + MBLX + "' AND B.JHFADM='" + JHFADM + "' AND STATE=-1 AND LX='0'))");
            StrSql.Append(" UNION");
            StrSql.Append(" SELECT DISTINCT SB.CJBM,SB.DQMBDM MBDM,SB.JSDM FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_YSMBLC LC");
            StrSql.Append(" WHERE SB.DQMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=-1");
            StrSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=SB.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' ");
            DS = Query(StrSql.ToString());
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DGDCLCJBM(ref CJStr, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["MBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString(), MBLX);
                }
            }
            return CJStr;
        }
        /// <summary>
        /// 获取待处理模板以及其所有上级的层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns></returns>
        public string GetDCLAndParCJBM(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            string CJStr = "";
            DataSet DS = new DataSet();
            StringBuilder StrSql = new StringBuilder();
            StrSql.Append("SELECT DISTINCT LC.CJBM,LC.QZMBDM MBDM,LC.JSDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHFA FA");
            StrSql.Append(" WHERE LC.MBLX='" + MBLX + "' AND LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND LC.MBLX=FS.YSLX AND FS.ISFQ=1  AND FA.SCBS=1 AND FA.JHFADM=FS.JHFADM AND MB.MBZQ=FA.FABS AND LX='0' AND FA.JHFADM='" + JHFADM + "'");
            StrSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + MBLX + "' AND S.JHFADM=FA.JHFADM AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.JHFADM='" + JHFADM + "' AND STATE=1 AND LX='0'");
            StrSql.Append(" AND NOT EXISTS (SELECT * FROM TB_YSMBLC C,TB_MBLCSB B WHERE B.DQMBDM=S.DQMBDM AND B.JSDM=S.JSDM AND B.CJBM=S.CJBM AND B.JHFADM=S.JHFADM AND  B.DQMBDM=C.QZMBDM AND B.JSDM=C.JSDM AND B.CJBM=C.CJBM AND C.MBLX='" + MBLX + "' AND B.JHFADM='" + JHFADM + "' AND STATE=-1 AND LX='0'))");
            StrSql.Append(" UNION");
            StrSql.Append(" SELECT DISTINCT SB.CJBM,SB.DQMBDM MBDM,SB.JSDM FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_YSMBLC LC");
            StrSql.Append(" WHERE SB.DQMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=-1");
            StrSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=SB.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' ");
            DS = Query(StrSql.ToString());
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    CJStr = CJStr == "" ? DS.Tables[0].Rows[i]["CJBM"].ToString() : CJStr + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                    DGDCLCJBM(ref CJStr, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["MBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString(), MBLX);
                }
            }
            return CJStr;
        }
        /// <summary>
        /// 递归查询待处理模板的所有上级的层级编码
        /// </summary>
        /// <param name="CJStr">层级编码字符串</param>
        /// <param name="CJBM">层级编码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="MBLX">模板类型</param>
        public void DGDCLCJBM(ref string CJStr, string CJBM, string MBDM, string JSDM, string MBLX)
        {
            string SQL = "", CJ = "";
            DataSet DS = new DataSet();
            if (CJBM.Length == 4)
            {
                //CJStr = CJStr == "" ? CJBM : CJStr + "','" + CJBM;
                SQL = "SELECT DISTINCT CJBM,QZMBDM MBDM,JSDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "' AND QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM<>'" + CJBM + "'";
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        DGDCLCJBM(ref CJStr, DS.Tables[0].Rows[i]["CJBM"].ToString(), DS.Tables[0].Rows[i]["MBDM"].ToString(), DS.Tables[0].Rows[i]["JSDM"].ToString(), MBLX);
                    }
                }
            }
            else
            {
                CJ = CJBM.Substring(0, CJBM.Length - 4);
                CJStr = CJStr == "" ? CJ : CJStr + "','" + CJ;
                SQL = "SELECT DISTINCT CJBM,QZMBDM MBDM,JSDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "' AND CJBM='" + CJ + "'";
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DGDCLCJBM(ref CJStr, DS.Tables[0].Rows[0]["CJBM"].ToString(), DS.Tables[0].Rows[0]["MBDM"].ToString(), DS.Tables[0].Rows[0]["JSDM"].ToString(), MBLX);
                }
            }
        }
        /// <summary>
        /// 获取角色代码和模板代码相同且层级编码的相关信息
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetCJBM(string JSDM, string MBDM, string CJBM,string JHFADM,string YSLX)
        {
            string CJStr = "",MBLX="";
            DataSet DS = new DataSet();
            if (YSLX == "")
            {
                MBLX = GetYSLXByJHFADM(JHFADM);
            }
            else
            {
                MBLX = YSLX;
            }
            string SQL = "SELECT CJBM FROM TB_YSMBLC WHERE JSDM='" + JSDM.Replace("'", "''") + "' AND QZMBDM='" + MBDM + "' AND CJBM<>'" + CJBM + "' AND MBLX='"+MBLX+"'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    CJStr = CJStr == "" ? DS.Tables[0].Rows[i]["CJBM"].ToString() : CJStr + "," + DS.Tables[0].Rows[i]["CJBM"].ToString();
                }
            }
            return CJStr;
        }
        /// <summary>
        /// 获取角色代码和模板代码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetMBANDJS(string CJBM)
        {
            string Str = "";
            DataSet DS = new DataSet();
            string SQL = "SELECT JSDM,QZMBDM FROM TB_YSMBLC WHERE CJBM='" + CJBM + "'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Str = DS.Tables[0].Rows[0]["JSDM"].ToString() + "@" + DS.Tables[0].Rows[0]["QZMBDM"].ToString();
            }
            return Str;
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetChird(string CJBM,string JHFADM,string YSLX)
        {
            string CJStr = "";
            GetChirdCJBM(CJBM, ref CJStr,JHFADM,YSLX);
            return CJStr;
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public void GetChirdCJBM(string CJBM, ref string CJStr,string JHFADM,string YSLX)
        {
            string MBLX = "";
            DataSet DS = new DataSet();
            if (YSLX == "")
            {
                MBLX = GetYSLXByJHFADM(JHFADM);
            }
            else
            {
                MBLX = YSLX;
            }
            string SQL = "SELECT DISTINCT L1.CJBM FROM TB_YSMBLC L1,TB_YSMBLC L2";
            SQL += " WHERE L1.QZMBDM=L2.QZMBDM AND L2.JSDM=L1.JSDM AND L1.MBLX=L2.MBLX AND L1.CJBM<>L2.CJBM AND SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4)='" + CJBM + "' AND L1.MBLX='" + MBLX + "' AND L2.MBLX='" + MBLX + "'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    CJStr = CJStr == "" ? DS.Tables[0].Rows[i]["CJBM"].ToString() : CJStr + "," + DS.Tables[0].Rows[i]["CJBM"].ToString();
                    //递归CJBM下面所有的顶节点
                    GetChirdCJBM(DS.Tables[0].Rows[i]["CJBM"].ToString(), ref CJStr,JHFADM,YSLX);
                }
            }
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetPar(string CJBM,string JHFADM,string YSLX)
        {
            string CJStr = "";
            GetParCJBM(CJBM, ref CJStr,JHFADM,YSLX);
            return CJStr;
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public void GetParCJBM(string CJBM, ref string CJStr,string JHFADM,string YSLX)
        {
            string BM = "", MBLX="";
            DataSet DS = new DataSet();
            if (YSLX == "")
            {
                MBLX = GetYSLXByJHFADM(JHFADM);
            }
            else
            {
                MBLX = YSLX;
            }
            string SQL = "SELECT DISTINCT L1.CJBM FROM TB_YSMBLC L1,TB_YSMBLC L2";
            SQL += " WHERE L1.QZMBDM=L2.QZMBDM AND L2.JSDM=L1.JSDM AND L1.MBLX=L2.MBLX AND L1.CJBM<>L2.CJBM AND L2.CJBM='" + CJBM + "' AND L1.MBLX='"+MBLX+"' AND L2.MBLX='"+MBLX+"'";
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    BM = DS.Tables[0].Rows[i]["CJBM"].ToString().Substring(0, DS.Tables[0].Rows[i]["CJBM"].ToString().Length - 4);
                    CJStr = CJStr == "" ? BM : CJStr + "," + BM;
                    //递归CJBM下面所有的顶节点
                    GetParCJBM(BM, ref CJStr,JHFADM,YSLX);
                }
            }
        }
        /// <summary>
        /// 获取当前模板下的所有模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <returns></returns>
        public string GetAllChirldCJBM(string CJBM,string MBDM,string JSDM,string MBLX,string LX) {
            string CJ =CJBM;
            recursionChirldCJBM(CJBM, MBDM, JSDM, MBLX, ref CJ,LX);
            return CJ;
        }
        /// <summary>
        /// 递归当前模板下的所有模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <param name="CJ">返回的层及编码字符串</param>
        /// <returns></returns>
        public void recursionChirldCJBM(string CJBM, string MBDM, string JSDM, string MBLX,ref string CJ,string LX)
        {
            string SQL = "";
            DataSet DS = new DataSet();
            if (LX.Trim() != "0")
            {
                //CJBM LIKE '" + CJBM + "%' AND CJBM<>'" + CJBM + "' AND
                SQL = "SELECT QZMBDM,JSDM,CJBM,LX FROM TB_YSMBLC WHERE  SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='"+CJBM+"'";
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        CJ = CJ + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                        //如果当前模板不是填报模板，进入递归，直到递归到子节点的填报模板为止
                        if (DS.Tables[0].Rows[i]["LX"].ToString().Trim() != "0")
                        {
                            recursionChirldCJBM(DS.Tables[0].Rows[i]["CJBM"].ToString().Trim(), DS.Tables[0].Rows[i]["QZMBDM"].ToString().Trim(), DS.Tables[0].Rows[i]["JSDM"].ToString().Trim(), MBLX, ref CJ, DS.Tables[0].Rows[i]["LX"].ToString().Trim());
                        }

                    }
                }
                else
                {
                    SQL = "SELECT QZMBDM,JSDM,CJBM,LX FROM TB_YSMBLC WHERE QZMBDM='"+MBDM+"' AND JSDM='"+JSDM.Replace("'","''")+"' AND MBLX='"+MBLX+"' AND CJBM<>'"+CJBM+"'";
                    DS.Tables.Clear();
                    DS = Query(SQL);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                        {
                            CJ = CJ + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                            //如果当前模板不是填报模板，进入递归，直到递归到子节点的填报模板为止
                            if (DS.Tables[0].Rows[i]["LX"].ToString().Trim() != "0")
                            {
                                recursionChirldCJBM(DS.Tables[0].Rows[i]["CJBM"].ToString().Trim(), DS.Tables[0].Rows[i]["QZMBDM"].ToString().Trim(), DS.Tables[0].Rows[i]["JSDM"].ToString().Trim(), MBLX, ref CJ, DS.Tables[0].Rows[i]["LX"].ToString().Trim());
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 获取当前模板下的所有父节点模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <returns></returns>
        public string GetAllParCJBM(string CJBM, string MBDM, string JSDM, string MBLX)
        {
            string CJ = CJBM;
            recursionParCJBM(CJBM, MBDM, JSDM, MBLX, ref CJ);
            return CJ;
        }
        /// <summary>
        /// 递归当前模板下的所有模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <param name="CJ">返回的层及编码字符串</param>
        /// <returns></returns>
        public void recursionParCJBM(string CJBM, string MBDM, string JSDM, string MBLX, ref string CJ)
        {
            string SQL = "";
            DataSet DS = new DataSet();
            if (CJBM.Length != 4)
            {
                SQL = "SELECT QZMBDM,JSDM,CJBM FROM TB_YSMBLC WHERE CJBM='" + CJBM.Substring(0, CJBM.Length - 4) + "'";
                DS.Tables.Clear();
                DS = Query(SQL);
                CJ = CJ == "" ? DS.Tables[0].Rows[0]["CJBM"].ToString() : CJ + "','" + DS.Tables[0].Rows[0]["CJBM"].ToString();
                recursionParCJBM(DS.Tables[0].Rows[0]["CJBM"].ToString().Trim(), DS.Tables[0].Rows[0]["QZMBDM"].ToString().Trim(), DS.Tables[0].Rows[0]["JSDM"].ToString().Trim(), MBLX, ref CJ);
            }
            else
            {
                SQL = "SELECT QZMBDM,JSDM,CJBM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND MBLX='" + MBLX + "' AND CJBM<>'" + CJBM + "'";
                DS.Tables.Clear();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                         CJ = CJ == "" ? DS.Tables[0].Rows[i]["CJBM"].ToString() : CJ + "','" + DS.Tables[0].Rows[i]["CJBM"].ToString();
                        //如果当前模板不是填报模板，进入递归，直到递归到子节点的填报模板为止
                         recursionParCJBM(DS.Tables[0].Rows[i]["CJBM"].ToString().Trim(), DS.Tables[0].Rows[i]["QZMBDM"].ToString().Trim(), DS.Tables[0].Rows[i]["JSDM"].ToString().Trim(), MBLX, ref CJ);
                    }
                }
            }
        }
        /// <summary>
        /// 获取非受限的角色代码
        /// </summary>
        /// <returns></returns>
        public string getNotQXJSDM(string HSZXDM)
        {
            string JSDM = "";
            DataSet DS = new DataSet();
            DS=Query("SELECT JSDM FROM TB_MBSJXZYH WHERE HSZXDM='"+HSZXDM+"'");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                JSDM = JSDM == "" ? DS.Tables[0].Rows[i]["JSDM"].ToString() : JSDM + "','" + DS.Tables[0].Rows[i]["JSDM"].ToString();
            }
            JSDM = JSDM == "" ? JSDM : "'" + JSDM + "'";
            return JSDM;
        }
        /// <summary>
        /// 根据计划方案获取方案的截止时间
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetJZSJByJHFADM(string JHFADM)
        {
            string JZSJ="";
            DataSet DS = new DataSet();
            if (JHFADM != "")
            {
                DS = Query("SELECT ISNULL(TBSJXZ,'-1') TBSJXZ FROM TB_YSLCFS WHERE JHFADM='" + JHFADM + "'");
                if (DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TBSJXZ"].ToString() != "-1" && DS.Tables[0].Rows[0]["TBSJXZ"].ToString().Trim().Equals("")==false)
                {
                    JZSJ = DS.Tables[0].Rows[0]["TBSJXZ"].ToString();
                }
            }
            return JZSJ;
        }
        /// <summary>
        /// 判断当前用户是否是受截止时间限制的用户
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <returns></returns>
        public bool IsSQUSER(string USERDM,string HSZXDM)
        {
            bool Flag = true;
            DataSet DS = new DataSet();
            if (Query("SELECT * FROM TB_JSYH WHERE USERID='" + USERDM + "' AND JSDM IN(SELECT JSDM FROM TB_MBSJXZYH WHERE HSZXDM='" + HSZXDM + "')").Tables[0].Rows.Count > 0)
            {
                Flag = false;
            }
            return Flag;
        }
        /// <summary>
        /// 根据计划方案获取流程截止时间和受限标志
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetXZInfo(string JHFADM)
        {
            string Str = "",SQL="";
            DataSet DS = new DataSet();
            if (JHFADM != "")
            {
                SQL = "SELECT ISNULL(TBSJXZ,'-1') TBSJXZ,ISNULL(XZTBBZ,'0') XZTBBZ FROM TB_YSLCFS WHERE JHFADM='"+JHFADM+"'";
                DS=Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    Str = DS.Tables[0].Rows[0]["TBSJXZ"].ToString() + "," + DS.Tables[0].Rows[0]["XZTBBZ"].ToString();
                }
            }
            return Str;
        }
        /// <summary>
        /// 函数功能：获取当前待处理的模板以及所有的上级模板的层及编码
        /// 编程思想：通过GetDCLAndParCJBM函数获取所有待处理模板以及上级模板的层级编码，通过这些层及编码查询当前JHFADM代码的计划方案有没有做完
        ///           具体SQL语句包含两部分：当前还没有填报的模板、当前处于流程中在途的模板（即：在TB_MBLCSB表中等于JHFADM的记录中，STATE=-1的记录）
        ///           如果查询的数据集不为空的话，表示当前还有未处理的记录，否则表示当前计划方案的流程已经走完
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>true代表流程已经完成；false代表流程还未完成</returns>
        public bool IsLCOver(string JHFADM)
        {
            //定义返回值变量
            bool Flag=false;
            //定义计划方案的模板周期、预算类型变量
            string MBZQ = "", MBLX = "";
            //定义存取查询
            StringBuilder StrSql = new StringBuilder();
            //获取模板周期和预算类型
            DataSet DS = getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
            }
            StrSql.Append("SELECT DISTINCT LC.CJBM,LC.QZMBDM MBDM,LC.JSDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHFA FA");
            StrSql.Append(" WHERE LC.MBLX='" + MBLX + "' AND LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "'  AND LC.MBLX=FS.YSLX AND FS.ISFQ=1  AND FA.SCBS=1 AND FA.JHFADM=FS.JHFADM AND MB.MBZQ=FA.FABS AND LX='0' AND FA.JHFADM='" + JHFADM + "'");
            StrSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + MBLX + "' AND S.JHFADM=FA.JHFADM AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.JHFADM='" + JHFADM + "' AND STATE=1 AND LX='0'");
            StrSql.Append(" AND NOT EXISTS (SELECT * FROM TB_YSMBLC C,TB_MBLCSB B WHERE B.DQMBDM=S.DQMBDM AND B.JSDM=S.JSDM AND B.CJBM=S.CJBM AND B.JHFADM=S.JHFADM AND  B.DQMBDM=C.QZMBDM AND B.JSDM=C.JSDM AND B.CJBM=C.CJBM AND C.MBLX='" + MBLX + "' AND B.JHFADM='" + JHFADM + "' AND STATE=-1 AND LX='0'))");
            StrSql.Append(" UNION");
            StrSql.Append(" SELECT DISTINCT SB.CJBM,SB.DQMBDM MBDM,SB.JSDM FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_YSMBLC LC");
            StrSql.Append(" WHERE SB.DQMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=-1");
            StrSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=SB.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' ");
            Flag = Query(StrSql.ToString()).Tables[0].Rows.Count > 0 ? false : true;
            return Flag;
        }
        /// <summary>
        /// 根据模板代码获取包括自身以及它的关联上级模板
        /// </summary>
        /// <param name="MBDM">模板代码字符串（逗号隔开）</param>
        /// <param name="MBLX">预算类型</param>
        /// <returns>模板代码以及关联上级模板代码数据集</returns>
        public DataSet GetMBDMByGLXJMB(string MBDM,string MBLX)
        {
            StringBuilder StrSql = new StringBuilder();
            StrSql.Append(" SELECT DISTINCT XJ.MBDM FROM TB_YSMBXJB XJ,TB_YSMBLC LC WHERE  XJ.CHILDMBDM=LC.QZMBDM AND LC.MBLX='"+MBLX+"' AND CHILDMBDM IN('"+MBDM.Replace(",","','")+"')");
            return Query(StrSql.ToString());
        }
        /// <summary>
        /// 函数功能：根据模板代码，获取其所有父节点的模板代码（相同的模板代码，取层级编码最短的递归）
        /// 编程思想：通过模板代码获取流程表中层级编码最短的结点信息，依次递归，直到顶节点
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码（逗号隔开）</param>
        /// <returns>返回模板代码拼接的字符串（用逗号隔开）</returns>
        public string GetAllParMB(string JHFADM,string MB)
        {
            bool Flag=false;
            DataSet DS = new DataSet();
            string MBLX = "", MBStr = "",DM="";
            List<List<string>> OBJ = new List<List<string>>();
            List<List<string>> Arr = new List<List<string>>();
            List<List<string>> MBString = new List<List<string>>();
            DS = getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                //获取预算类型（3：预算上报；4：预算批复；5：预算分解）
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
                string[] M = MB.Split(',');
                for (int i = 0; i < M.Length; i++)
                {
                    Arr.Add(new List<string>());
                    //递归查询所有的父节点的模板代码（相同模板的结点，查找层级编码长度最小的父节点信息）
                    recursionParMB(M[i].ToString(), MBLX, ref Arr,false);
                    //如果递归出来的列表不存在，则移除
                    if (Arr[Arr.Count - 1].Count == 0)
                    {
                        Arr.RemoveAt(Arr.Count-1);
                    }
                }
                //获取当前模板的所有关联上级模板，依次循环
                DS = GetMBDMByGLXJMB(MB, MBLX);
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Arr.Add(new List<string>());
                    //递归查询所有的父节点的模板代码（相同模板的结点，查找层级编码长度最小的父节点信息）
                    recursionParMB(DS.Tables[0].Rows[i]["MBDM"].ToString(), MBLX, ref Arr,true);
                    //如果递归出来的列表不存在，则移除
                    if (Arr[Arr.Count - 1].Count == 0)
                    {
                        Arr.RemoveAt(Arr.Count-1);
                    }
                }
                if (Arr.Count > 0)
                {
                    //将第一个列表中的顶节点拿出来，和其它列表中比较，构建输出父节点模板代码列表
                    DM = Arr[0][Arr[0].Count - 1].ToString().Trim();
                    //将顶节点、队列的长度、和队列的下标放入新的列表中存储
                    OBJ.Add(new List<string>() { DM,"C"+Arr[0].Count.ToString(),"0" });
                    for (int j = 1; j < Arr.Count; j++)
                    {
                        Flag = false;
                        if (DM == Arr[j][Arr[j].Count - 1].ToString().Trim())
                        {
                            if (OBJ[0].Find(a => (a == j.ToString())) == null || OBJ[0].Find(a => (a == j.ToString())) == "")
                            {
                                OBJ[0].Add(j.ToString());
                                //将同节点的队列长度最长的，存入第二个字段中
                                if (int.Parse(OBJ[0][1].ToString().Trim().Substring(1)) < Arr[j].Count)
                                {
                                    OBJ[0][1] = "C"+Arr[j].Count.ToString();
                                }
                            }
                        }
                        else
                        {
                            if (OBJ.Count == 1)
                            {
                                //如果和第一个结点的父节点不同的话，创建一个新的列表
                                OBJ.Add(new List<string>() { Arr[j][Arr[j].Count - 1].ToString().Trim(),"C"+ Arr[j].Count.ToString(), j.ToString() });
                            }
                            else
                            {
                                for (int n = 1; n < OBJ.Count; n++)
                                {
                                    if (OBJ[n][0].ToString().Trim() == Arr[j][Arr[j].Count - 1].ToString().Trim())
                                    {
                                        if (OBJ[n].Find(a => (a == j.ToString())) == null || OBJ[n].Find(a => (a == j.ToString())) == "")
                                        {
                                            OBJ[n].Add(j.ToString());
                                            //将同节点的队列长度最长的，存入第二个字段中
                                            if (int.Parse(OBJ[n][1].ToString().Trim().Substring(1)) < Arr[j].Count)
                                            {
                                                OBJ[n][1] = "C"+Arr[j].Count.ToString();
                                            }
                                        }
                                        Flag = true;
                                        break;
                                    }
                                }
                                if (Flag == false)
                                {
                                    //如果和当前列表中所有的顶节点不同的话，创建一个新的列表
                                    OBJ.Add(new List<string>() { Arr[j][Arr[j].Count - 1].ToString().Trim(),"C"+Arr[j].Count.ToString(), j.ToString() });
                                }
                            }

                        }
                    }
                    //循环输出构建树（顶节点不同的话）
                    for (int k = 0; k < OBJ.Count; k++)
                    {
                        string Str = "" ;
                        int Len = int.Parse(OBJ[k][1].ToString().Trim().Substring(1));
                        //循环按层输出所有列表每层上的结点信息
                        for (int q = 0; q <= Len-2; q++)
                        {
                            //循环同顶节点的所有列的同层节点
                            for (int l = 2; l < OBJ[k].Count; l++)
                            {
                                if (q <= Arr[int.Parse(OBJ[k][l].ToString())].Count - 2)
                                {
                                    Str = Str=="" ? Arr[int.Parse(OBJ[k][l].ToString())][q].ToString() :(Str.IndexOf(Arr[int.Parse(OBJ[k][l].ToString())][q].ToString()) > -1 ? Str : Str + "," + Arr[int.Parse(OBJ[k][l].ToString())][q].ToString());
                                } 
                            }
                        }
                        MBStr = MBStr == "" ? Str + "," + OBJ[k][0].ToString().Trim() : MBStr + ","+Str+","+ OBJ[k][0].ToString().Trim();
                    }
                }

            }
            return MBStr;
        }
        /// <summary>
        /// 函数功能：递归查询当前模板的父节点的模板代码
        /// 编程思想：通过模板代码获取流程表中层级编码最短的结点信息，依次递归，直到顶节点
        /// </summary>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">模板类型</param>
        /// <param name="Arr">存储每个模板上级模板列表</param>
        /// <param name="IsXJMB">是否为关联上级模板标志（关联上级模板上级首次递归时，需将节点加入到列表中）</param>
        /// <param name="MBStr">当前模板所有模板的父节点模板代码</param>
        public void recursionParMB(string MBDM, string MBLX, ref List<List<string>> Arr,bool IsXJMB)
        {
            DataSet DS = new DataSet();
            string SQL = "", CJBM = "", QZMBDM = "", JSDM = "",DM="";
            //查询模板代码等于MBDM的流程表中的CJBM长度最小的结点信息
            SQL = " SELECT CJBM,QZMBDM,JSDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND MBLX='" + MBLX + "' AND DATALENGTH(CJBM)=(SELECT MIN(DATALENGTH(CJBM)) FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND MBLX='" + MBLX + "')";
            //清空DS数据集中所有的虚表
            DS.Tables.Clear();
            DS = Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                CJBM = DS.Tables[0].Rows[0]["CJBM"].ToString();
                QZMBDM = DS.Tables[0].Rows[0]["QZMBDM"].ToString();
                JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString();
                //判断当前递归，是否是递归的关联上级模板，如果是的话，首次需要将关联上级加入列表中
                if (IsXJMB == true)
                {
                    if (Arr[Arr.Count-1].Find(a => (a == QZMBDM)) == null || Arr[Arr.Count-1].Find(a => (a == QZMBDM)) == "")
                    {
                        Arr[Arr.Count-1].Add(QZMBDM);
                    }
                    //首次加入后，之后就不需要在执行了
                    IsXJMB = false;
                }
                //判断当模板是否为顶节点（顶节点的）
                if (CJBM.Trim().Length == 4)
                {
                    SQL = "SELECT CJBM,QZMBDM,JSDM,MBDM FROM TB_YSMBLC WHERE QZMBDM='" + QZMBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND MBLX='" + MBLX + "' AND CJBM<>'" + CJBM + "'";
                    //清空DS数据集中所有的虚表
                    DS.Tables.Clear();
                    DS = Query(SQL);
                    //如果数据集不为空的话，说明它有父节点
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        DM = DS.Tables[0].Rows[0]["MBDM"].ToString();
                        if (Arr[Arr.Count-1].Find(a => (a == DM)) == null || Arr[Arr.Count-1].Find(a => (a == DM)) == "")
                        {
                            Arr[Arr.Count-1].Add(DM);
                        }
                        recursionParMB(DM, MBLX, ref Arr,IsXJMB);
                    }
                }
                else
                {
                    SQL = "SELECT CJBM,QZMBDM,JSDM,MBDM FROM TB_YSMBLC WHERE CJBM='"+CJBM.Substring(0,CJBM.Length-4)+"'";
                    //清空DS数据集中所有的虚表
                    DS.Tables.Clear();
                    DS = Query(SQL);
                    //如果数据集不为空的话，说明它有父节点
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        DM = DS.Tables[0].Rows[0]["QZMBDM"].ToString();
                        //MBStr = MBStr == "" ? DM : (MBStr.IndexOf(DM) > -1 ? MBStr : MBStr + "," + DM);
                        if (Arr[Arr.Count-1].Find(a => (a == DM)) == null || Arr[Arr.Count-1].Find(a => (a == DM)) == "")
                        {
                            Arr[Arr.Count-1].Add(DM);
                        }
                        recursionParMB(DM, MBLX, ref Arr,IsXJMB);
                    }
                }
            }
        }
        /// <summary>
        /// 函数功能：根据待填报和待审核，获取所有的上级模板的模板代码
        /// 编程思想：查询待填报、待审核的信息（sql语句），递归每一个待填报、待审批的模板代码，直至顶节点！
        ///           每个待处理模板的上级模板都存储到List<string>当中
        ///           最后，通过两层循环，将重复的上级模板代码去掉，合成一个字符串
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns>返回用逗号隔开的所有上级模板代码</returns>
        public string GetDCLParMB(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            string CJStr = "";
            DataSet DS = new DataSet();
            StringBuilder StrSql = new StringBuilder();
            List<List<string>> Arr = new List<List<string>>();
            StrSql.Append("SELECT DISTINCT LC.CJBM,LC.QZMBDM MBDM,LC.JSDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHFA FA");
            StrSql.Append(" WHERE LC.MBLX='" + MBLX + "' AND LC.QZMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND LC.MBLX=FS.YSLX AND FS.ISFQ=1  AND FA.SCBS=1 AND FA.JHFADM=FS.JHFADM AND MB.MBZQ=FA.FABS AND LX='0' AND FA.JHFADM='" + JHFADM + "'");
            StrSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + MBLX + "' AND S.JHFADM=FA.JHFADM AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.JHFADM='" + JHFADM + "' AND STATE=1 AND LX='0'");
            StrSql.Append(" AND NOT EXISTS (SELECT * FROM TB_YSMBLC C,TB_MBLCSB B WHERE B.DQMBDM=S.DQMBDM AND B.JSDM=S.JSDM AND B.CJBM=S.CJBM AND B.JHFADM=S.JHFADM AND  B.DQMBDM=C.QZMBDM AND B.JSDM=C.JSDM AND B.CJBM=C.CJBM AND C.MBLX='" + MBLX + "' AND B.JHFADM='" + JHFADM + "' AND STATE=-1 AND LX='0'))");
            StrSql.Append(" UNION");
            StrSql.Append(" SELECT DISTINCT SB.CJBM,SB.DQMBDM MBDM,SB.JSDM FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_YSMBLC LC");
            StrSql.Append(" WHERE SB.DQMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND MB.MBMC LIKE '%" + MBMC + "%' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=-1");
            StrSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=SB.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' ");
            DS = Query(StrSql.ToString());
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Arr.Add(new List<string>());
                //递归查询所有的父节点的模板代码（相同模板的结点，查找层级编码长度最小的父节点信息）
                recursionParMB(DS.Tables[0].Rows[i]["MBDM"].ToString(), MBLX, ref Arr, false);
                //如果递归出来的列表不存在，则移除
                if (Arr[Arr.Count - 1].Count == 0)
                {
                    Arr.RemoveAt(Arr.Count - 1);
                }
            }
            if (Arr.Count > 0)
            {
                for (int i = 0; i < Arr.Count; i++)
                {
                    for (int j = 0; j < Arr[i].Count; j++)
			        {
			            CJStr = CJStr == "" ? Arr[i][j].ToString() : (CJStr.IndexOf(Arr[i][j].ToString()) > -1 ? CJStr : CJStr + "," + Arr[i][j].ToString());
			        }
                   
                }
            }
            return CJStr;
        }
        #endregion  Method
    }
}

