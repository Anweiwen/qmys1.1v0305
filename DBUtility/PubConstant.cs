using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;
using System.Data;

namespace RMYH.DBUtility
{
    public class PubConstant
    {

        /// <summary>
        /// 获取连接字符串【得到的是Webconfig文件中appSettings节点下key=uiteaccount节点的value值】
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                string _connectionString = ConfigurationSettings.AppSettings["boomlinkconnection"];

                //临时添加为程序连接多个数据库用

                //HttpContext.Current.Application["DataBase"].ToString();
                //string DataBase = HttpContext.Current.Request.Cookies["DataBase"].Value;
                //_connectionString = _connectionString.ToString().Replace("@DataBase", DataBase).Trim();

                //_connectionString=DESEncrypt.Decrypt(_connectionString);
                return _connectionString;
            }
        }
        /// <summary>
        /// 获取访问Java报表TomCat的端口号
        /// </summary>
        public static string TomcatPort
        {
            get
            {
                string TomcatPort = ConfigurationSettings.AppSettings["TomcatPort"];

                return TomcatPort;
            }
        }
        public static string getDBType
        {
            get
            {
                string _dbtype = ConfigurationSettings.AppSettings["DBType"];
                //_connectionString=DESEncrypt.Decrypt(_connectionString);
                if (string.IsNullOrEmpty(_dbtype))
                    _dbtype = "0";
                return _dbtype;
            }
        }
        /// <summary>
        /// 根据参数得到web.config里配置项的appSettings字符串。
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public static string GetConnectionString(string configName)
        {
            string connectionString = ConfigurationSettings.AppSettings[configName];
            string ConStringEncrypt = ConfigurationSettings.AppSettings["ConStringEncrypt"];
            if (ConStringEncrypt == "true")
            {
                //connectionString = DESEncrypt.Decrypt(connectionString);
            }
            return connectionString;
        }

        /// <summary>
        /// 单点登录标志
        /// </summary>
        public static string SsoLogin
        {
            get
            {
                string TomcatPort = ConfigurationSettings.AppSettings["SsoLogin"];

                return TomcatPort;
            }
        }
    }
}
