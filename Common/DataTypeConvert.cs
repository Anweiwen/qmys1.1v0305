﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;
using System.Runtime.InteropServices;

namespace RMYH.Common
{
    public class DataTypeConvert
    {
        /// <summary>
        /// 将DataTable转化成List<T> 类型
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="DT">要转化的DataTable数据集</param>
        /// <returns></returns>
        public static List<T> ConvertDataTableToList<T>(DataRow[] Rows, List<DataColumn> listColumns) where T : new()
        {
            Type type = typeof(T);
            List<T> list = new List<T>();
            PropertyInfo[] propertyInfos = type.GetProperties();  //获取泛型的属性
            T t;
            foreach (DataRow dr in Rows)
            {
                t = new T();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    try
                    {
                        DataColumn dColumn = listColumns.Find(name => name.ToString().ToUpper() == propertyInfo.Name.ToUpper());  //查看是否存在对应的列名
                        if (dColumn != null)
                        {
                            if (dr[propertyInfo.Name].ToString()!="")
                            {
                                propertyInfo.SetValue(t, dr[propertyInfo.Name], null);  //赋值
                            }
                            else
                            {
                                propertyInfo.SetValue(t, dr[propertyInfo.Name].ToString(), null);  //赋值
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                list.Add(t);
            }
            return list;
        }

        /// <summary>
        /// 将数字转化成英文字母
        /// </summary>
        /// <param name="colIndex">数字</param>
        /// <returns></returns>
        public static string GetCharByNumber(int colIndex)
        {
            if (colIndex < 1)
            {
                return "";
            }
            string str = "", result = "", A = "A";
            while (colIndex != 0)
            {
                int num = colIndex % 26; // 取余
                int c = int.Parse(Encoding.ASCII.GetBytes(A)[0].ToString()) + num - 1;
                colIndex = (int)Math.Floor(colIndex / 26d); //返回值小于等于其数值参数的最大整数值。
                // 对于26的特殊处理
                if (num == 0)
                {
                    str = "Z";
                    colIndex -= 1; //退位
                }
                else
                {
                    byte[] array = new byte[1];
                    array[0] = (byte)Convert.ToUInt32(c);
                    str = Encoding.ASCII.GetString(array);
                }
                // 3.插入
                //result += str;
                result = result == "" ? str : result + " " + str;

            }
            if (result.Length > 1)
            {
                string[] Arr = result.Split(' ').ToArray();
                Array.Reverse(Arr);
                result = string.Join("", Arr);
            }
            return result;
        }


        /// <summary>
        /// 将列字母转化为数字
        /// </summary>
        /// <param name="Str">列字母</param>
        /// <returns></returns>
        public static int GetNumberByChar(string Str)
        {
            int rtn=0;
            Str = Str.ToUpper(); 
            int BaseNum = int.Parse(Encoding.ASCII.GetBytes("A")[0].ToString());  //找到A的码表大小
            for (var i = 0; i < Str.Length; i++) {//遍历每个位置
                rtn = rtn * 26 + int.Parse(Encoding.ASCII.GetBytes(Str)[0].ToString()) - BaseNum + 1;
            }
            return rtn;
        }

        /// <summary>
        /// 将对象转换为byte数组
        /// </summary>
        /// <param name="obj">被转换对象</param>
        /// <returns>转换后byte数组</returns>
        public static byte[] Object2Bytes(object obj)
        {
            byte[] buff = new byte[Marshal.SizeOf(obj)];
            IntPtr ptr = Marshal.UnsafeAddrOfPinnedArrayElement(buff, 0);
            Marshal.StructureToPtr(obj, ptr, true);
            return buff;
        }

        /// <summary>
        /// 将byte数组转换成对象
        /// </summary>
        /// <param name="buff">被转换byte数组</param>
        /// <param name="typ">转换成的类名</param>
        /// <returns>转换完成后的对象</returns>
        public static object Bytes2Object(byte[] buff, Type typ)
        {
            IntPtr ptr = Marshal.UnsafeAddrOfPinnedArrayElement(buff, 0);
            return Marshal.PtrToStructure(ptr, typ);
        }
    }
}
