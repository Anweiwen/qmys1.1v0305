using System;
using System.Diagnostics;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using NPOI.HSSF.Util;
using NPOI.SS.Util;
using System.Text;


namespace RMYH.Common
{
    /// <summary>
    /// 操作EXCEL导出数据报表的类
    /// 2006.4
    /// </summary>
    public class DataToExcel
    {

        /// <summary>
        /// 操作EXCEL导出数据报表的类
        /// 
        /// </summary>
        public DataToExcel()
        {
        }


        #region 操作EXCEL的一个类(需要Excel.dll支持)

        private int titleColorindex = 15;
        /// <summary>
        /// 标题背景色
        /// </summary>
        public int TitleColorIndex
        {
            set { titleColorindex = value; }
            get { return titleColorindex; }
        }

        private DateTime beforeTime;			//Excel启动之前时间
        private DateTime afterTime;				//Excel启动之后时间

        #region 创建一个Excel示例
        /// <summary>
        /// 创建一个Excel示例
        /// </summary>
        public void CreateExcel()
        {
            Excel.Application excel = new Excel.Application();
            excel.Application.Workbooks.Add(true);
            excel.Cells[1, 1] = "第1行第1列";
            excel.Cells[1, 2] = "第1行第2列";
            excel.Cells[2, 1] = "第2行第1列";
            excel.Cells[2, 2] = "第2行第2列";
            excel.Cells[3, 1] = "第3行第1列";
            excel.Cells[3, 2] = "第3行第2列";

            //保存
            excel.ActiveWorkbook.SaveAs("./tt.xls",Excel.XlFileFormat.xlExcel9795, null, null, false, false, Excel.XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);
            //打开显示
            excel.Visible = true;
            //			excel.Quit();
            //			excel=null;            
            //			GC.Collect();//垃圾回收
        }
        #endregion

        #region 将DataTable的数据导出显示为报表
        /// <summary>
        /// 将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string OutputExcel(System.Data.DataTable dt, string strTitle,System.Data.DataTable dtTitle )
        {
            beforeTime = DateTime.Now;

            Excel.Application excel;
            Excel._Workbook xBk;
            Excel._Worksheet xSt;
            string FilePath = HttpRuntime.AppDomainAppPath + "file1\\";
            
            int rowIndex = 4;
            int colIndex = 0;

            excel = new Excel.ApplicationClass();
            xBk = excel.Workbooks.Add(true);
            xSt = (Excel._Worksheet)xBk.ActiveSheet;

            //取得列标题			
            for (int i = 0; i < dtTitle.Rows.Count; i++)
            {
                colIndex++;
                excel.Cells[4, colIndex] = dtTitle.Rows[i]["ZDZWM"].ToString();

                //设置标题格式为居中对齐
                xSt.get_Range(excel.Cells[4, colIndex], excel.Cells[4, colIndex]).Font.Bold = true;
                xSt.get_Range(excel.Cells[4, colIndex], excel.Cells[4, colIndex]).HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xSt.get_Range(excel.Cells[4, colIndex], excel.Cells[4, colIndex]).Select();
                xSt.get_Range(excel.Cells[4, colIndex], excel.Cells[4, colIndex]).Interior.ColorIndex = titleColorindex;//19;//设置为浅黄色，共计有56种
            }
            //取得表格中的数据			
            foreach (DataRow row in dt.Rows)
            {
                rowIndex++;
                colIndex = 0;
                for (int i = 0; i < dtTitle.Rows.Count; i++)
                {
                    colIndex++;
                    //if (col.DataType == System.Type.GetType("System.DateTime"))
                    //{
                    //    excel.Cells[rowIndex, colIndex] = (Convert.ToDateTime(row[col.ColumnName].ToString())).ToString("yyyy-MM-dd");
                    //    xSt.get_Range(excel.Cells[rowIndex, colIndex], excel.Cells[rowIndex, colIndex]).HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;//设置日期型的字段格式为居中对齐
                    //}
                    //else
                        if (row[dtTitle.Rows[i]["FIELDNAME"].ToString()].GetType() == System.Type.GetType("System.String"))
                        {
                            excel.Cells[rowIndex, colIndex] = "'" + row[dtTitle.Rows[i]["FIELDNAME"].ToString()].ToString();
                            xSt.get_Range(excel.Cells[rowIndex, colIndex], excel.Cells[rowIndex, colIndex]).HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;//设置字符型的字段格式为居中对齐
                        }
                        else
                        {
                            excel.Cells[rowIndex, colIndex] = row[dtTitle.Rows[i]["FIELDNAME"].ToString()].ToString();
                        }
                }
            }

            //加载一个合计行			
            //int rowSum = rowIndex + 1;
            //int colSum = 2;
            //excel.Cells[rowSum, 2] = "合计";
            //xSt.get_Range(excel.Cells[rowSum, 2], excel.Cells[rowSum, 2]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            //设置选中的部分的颜色			
            //xSt.get_Range(excel.Cells[rowSum, colSum], excel.Cells[rowSum, colIndex]).Select();
            //xSt.get_Range(excel.Cells[rowSum,colSum],excel.Cells[rowSum,colIndex]).Interior.ColorIndex =Assistant.GetConfigInt("ColorIndex");// 1;//设置为浅黄色，共计有56种

            //取得整个报表的标题			
            excel.Cells[2, 1] = strTitle;

            //设置整个报表的标题格式			
            xSt.get_Range(excel.Cells[2, 1], excel.Cells[2, 2]).Font.Bold = true;
            xSt.get_Range(excel.Cells[2, 1], excel.Cells[2, 2]).Font.Size = 22;

            //设置报表表格为最适应宽度			
            xSt.get_Range(excel.Cells[4, 1], excel.Cells[rowIndex, colIndex]).Select();
            xSt.get_Range(excel.Cells[4, 1], excel.Cells[rowIndex, colIndex]).Columns.AutoFit();

            //设置整个报表的标题为跨列居中			
            xSt.get_Range(excel.Cells[2, 1], excel.Cells[2, colIndex]).Select();
            xSt.get_Range(excel.Cells[2, 1], excel.Cells[2, colIndex]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenterAcrossSelection;

            //绘制边框			
            xSt.get_Range(excel.Cells[4, 1], excel.Cells[rowIndex, colIndex]).Borders.LineStyle = 1;
            //xSt.get_Range(excel.Cells[4, 2], excel.Cells[rowIndex, 2]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = Excel.XlBorderWeight.xlThick;//设置左边线加粗
            //xSt.get_Range(excel.Cells[4, 2], excel.Cells[4, colIndex]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight = Excel.XlBorderWeight.xlThick;//设置上边线加粗
            //xSt.get_Range(excel.Cells[4, colIndex], excel.Cells[rowIndex, colIndex]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = Excel.XlBorderWeight.xlThick;//设置右边线加粗
            //xSt.get_Range(excel.Cells[rowIndex, 2], excel.Cells[rowIndex, colIndex]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThick;//设置下边线加粗



            afterTime = DateTime.Now;

            //显示效果			
            //excel.Visible=true;			
            //excel.Sheets[0] = "sss";

            ClearFile(FilePath);
            string filename = strTitle + DateTime.Now.ToString("yyyyMMddHHmmss") + new Random().Next(100, 999) + ".xls";
            excel.ActiveWorkbook.SaveAs(FilePath + filename, Excel.XlFileFormat.xlExcel9795, null, null, false, false, Excel.XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);
            //excel.ActiveWorkbook.SaveAs(FilePath + filename);
            //wkbNew.SaveAs strBookName;
            //excel.Save(strExcelFileName);

            #region  结束Excel进程

            //需要对Excel的DCOM对象进行配置:dcomcnfg


            //excel.Quit();
            //excel=null;            

            xBk.Close(null, null, null);
            excel.Workbooks.Close();
            excel.Quit();


            //注意：这里用到的所有Excel对象都要执行这个操作，否则结束不了Excel进程
            //			if(rng != null)
            //			{
            //				System.Runtime.InteropServices.Marshal.ReleaseComObject(rng);
            //				rng = null;
            //			}
            //			if(tb != null)
            //			{
            //				System.Runtime.InteropServices.Marshal.ReleaseComObject(tb);
            //				tb = null;
            //			}
            if (xSt != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xSt);
                xSt = null;
            }
            if (xBk != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xBk);
                xBk = null;
            }
            if (excel != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
                excel = null;
            }
            GC.Collect();//垃圾回收
            #endregion

            return filename;

        }
        #endregion

        #region Kill Excel进程

        /// <summary>
        /// 结束Excel进程
        /// </summary>
        public void KillExcelProcess()
        {
            Process[] myProcesses;
            DateTime startTime;
            myProcesses = Process.GetProcessesByName("Excel");

            //得不到Excel进程ID，暂时只能判断进程启动时间
            foreach (Process myProcess in myProcesses)
            {
                startTime = myProcess.StartTime;
                if (startTime > beforeTime && startTime < afterTime)
                {
                    myProcess.Kill();
                }
            }
        }
        #endregion

        #endregion

        #region 将DataTable的数据导出显示为报表(不使用Excel对象，使用COM.Excel)

        #region 使用示例
        /*使用示例：
		 * DataSet ds=(DataSet)Session["AdBrowseHitDayList"];
			string ExcelFolder=Assistant.GetConfigString("ExcelFolder");
			string FilePath=Server.MapPath(".")+"\\"+ExcelFolder+"\\";
			
			//生成列的中文对应表
			Hashtable nameList = new Hashtable();
			nameList.Add("ADID", "广告编码");
			nameList.Add("ADName", "广告名称");
			nameList.Add("year", "年");
			nameList.Add("month", "月");
			nameList.Add("browsum", "显示数");
			nameList.Add("hitsum", "点击数");
			nameList.Add("BrowsinglIP", "独立IP显示");
			nameList.Add("HitsinglIP", "独立IP点击");
			//利用excel对象
			DataToExcel dte=new DataToExcel();
			string filename="";
			try
			{			
				if(ds.Tables[0].Rows.Count>0)
				{
					filename=dte.DataExcel(ds.Tables[0],"标题",FilePath,nameList);
				}
			}
			catch
			{
				//dte.KillExcelProcess();
			}
			
			if(filename!="")
			{
				Response.Redirect(ExcelFolder+"\\"+filename,true);
			}
		 * 
		 * */

        #endregion

        /// <summary>
        /// 将DataTable的数据导出显示为报表(不使用Excel对象)
        /// </summary>
        /// <param name="dt">数据DataTable</param>
        /// <param name="strTitle">标题</param>
        /// <param name="FilePath">生成文件的路径</param>
        /// <param name="nameList"></param>
        /// <returns></returns>
        public string DataExcel(System.Data.DataTable dt, string strTitle, string FilePath, Hashtable nameList)
        {
            COM.Excel.cExcelFile excel = new COM.Excel.cExcelFile();
            ClearFile(FilePath);
            string filename = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".xls";
            excel.CreateFile(FilePath + filename);
            excel.PrintGridLines = false;

            COM.Excel.cExcelFile.MarginTypes mt1 = COM.Excel.cExcelFile.MarginTypes.xlsTopMargin;
            COM.Excel.cExcelFile.MarginTypes mt2 = COM.Excel.cExcelFile.MarginTypes.xlsLeftMargin;
            COM.Excel.cExcelFile.MarginTypes mt3 = COM.Excel.cExcelFile.MarginTypes.xlsRightMargin;
            COM.Excel.cExcelFile.MarginTypes mt4 = COM.Excel.cExcelFile.MarginTypes.xlsBottomMargin;

            double height = 1.5;
            excel.SetMargin(ref mt1, ref height);
            excel.SetMargin(ref mt2, ref height);
            excel.SetMargin(ref mt3, ref height);
            excel.SetMargin(ref mt4, ref height);

            COM.Excel.cExcelFile.FontFormatting ff = COM.Excel.cExcelFile.FontFormatting.xlsNoFormat;
            string font = "宋体";
            short fontsize = 9;
            excel.SetFont(ref font, ref fontsize, ref ff);

            byte b1 = 1,
                b2 = 12;
            short s3 = 12;
            excel.SetColumnWidth(ref b1, ref b2, ref s3);

            string header = "页眉";
            string footer = "页脚";
            excel.SetHeader(ref header);
            excel.SetFooter(ref footer);


            COM.Excel.cExcelFile.ValueTypes vt = COM.Excel.cExcelFile.ValueTypes.xlsText;
            COM.Excel.cExcelFile.CellFont cf = COM.Excel.cExcelFile.CellFont.xlsFont0;
            COM.Excel.cExcelFile.CellAlignment ca = COM.Excel.cExcelFile.CellAlignment.xlsCentreAlign;
            COM.Excel.cExcelFile.CellHiddenLocked chl = COM.Excel.cExcelFile.CellHiddenLocked.xlsNormal;

            // 报表标题
            int cellformat = 1;
            //			int rowindex = 1,colindex = 3;					
            //			object title = (object)strTitle;
            //			excel.WriteValue(ref vt, ref cf, ref ca, ref chl,ref rowindex,ref colindex,ref title,ref cellformat);

            int rowIndex = 1;//起始行
            int colIndex = 0;



            ////取得列标题				
            //foreach (DataColumn colhead in dt.Columns)
            //{
            //    colIndex++;
            //    string name = colhead.ColumnName.Trim();
            //    object namestr = (object)name;
            //    IDictionaryEnumerator Enum = nameList.GetEnumerator();
            //    while (Enum.MoveNext())
            //    {
            //        if (Enum.Key.ToString().Trim() == name)
            //        {
            //            namestr = Enum.Value;
            //        }
            //    }
            //    excel.WriteValue(ref vt, ref cf, ref ca, ref chl, ref rowIndex, ref colIndex, ref namestr, ref cellformat);
            //}

            //取得表格中的数据			
            foreach (DataRow row in dt.Rows)
            {
                rowIndex++;
                colIndex = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    colIndex++;
                    if (col.DataType == System.Type.GetType("System.DateTime"))
                    {
                        object str = (object)(Convert.ToDateTime(row[col.ColumnName].ToString())).ToString("yyyy-MM-dd"); ;
                        excel.WriteValue(ref vt, ref cf, ref ca, ref chl, ref rowIndex, ref colIndex, ref str, ref cellformat);
                    }
                    else
                    {
                        object str = (object)row[col.ColumnName].ToString();
                        excel.WriteValue(ref vt, ref cf, ref ca, ref chl, ref rowIndex, ref colIndex, ref str, ref cellformat);
                    }
                }
            }
            int ret = excel.CloseFile();

            //			if(ret!=0)
            //			{
            //				//MessageBox.Show(this,"Error!");
            //			}
            //			else
            //			{
            //				//MessageBox.Show(this,"请打开文件c:\\test.xls!");
            //			}
            return filename;

        }

        #endregion

        #region  清理过时的Excel文件

        private void ClearFile(string FilePath)
        {
            String[] Files = System.IO.Directory.GetFiles(FilePath);
            if (Files.Length > 50)
            {
                for (int i = 0; i < 20; i++)
                {
                    try
                    {
                        System.IO.File.Delete(Files[i]);
                    }
                    catch
                    {
                    }

                }
            }
        }
        #endregion

        //Datatable导出Excel
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt">数据集</param>
        /// <param name="ExcelFileName">文件名称</param>
        /// <param name="HeaderName">文件标题</param>
        /// <returns></returns>
        public string DataToExcelByNPOI(System.Data.DataTable dt, string ExcelFileName,string HeaderName)
        {
            IWorkbook WK;
            try
            {
                //获取最后一个小数点所在的位置
                int L = ExcelFileName.LastIndexOf('.');
                //获取文件的扩展名
                string ExtendStr = ExcelFileName.Substring(L + 1);
                //如果是2003以前的版本的话，用HSSFWorkbook实例操作Excel文件，否则用XSSFWorkbook实例才做Excel
                if (ExtendStr.Equals("xls"))
                {
                    WK = new HSSFWorkbook();
                }
                else
                {
                    WK = new XSSFWorkbook();
                }
                //创建Excel文件的sheet表
                ISheet sheet = WK.CreateSheet("Sheet1");
                sheet.AddMergedRegion(new CellRangeAddress(0,0,0,dt.Columns.Count-1));
                
                //创建列名称一行
                IRow HeaderRow = sheet.CreateRow(0);
                //设置行高度
                HeaderRow.Height = 35*20;
                ICell HeaderCell = HeaderRow.CreateCell(0);
                //设置单元格内容
                HeaderCell.SetCellValue(HeaderName);
                //创建头单元格属性对象
                ICellStyle HeadercellStyle = WK.CreateCellStyle();

                //设置边框属性
                HeadercellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                HeadercellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

                
                //设置字体
                NPOI.SS.UserModel.IFont HeaderFont = WK.CreateFont();
                HeaderFont.Boldweight = (short)FontBoldWeight.Bold;
                HeadercellStyle.SetFont(HeaderFont);
                //将上面设置的样式赋值给头一个单元格
                HeaderCell.CellStyle = HeadercellStyle;


                //用column name 作为列名
                int icolIndex = 0;
                //创建列名称一行
                IRow ColumnRow = sheet.CreateRow(1);
                //设置行高
                ColumnRow.Height = 30 * 20;
                //创建列头单元格属性对象
                ICellStyle ColCellStyle = WK.CreateCellStyle();

                //设置边框属性
                ColCellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                ColCellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                ColCellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                ColCellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                ColCellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                ColCellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

                ColCellStyle.FillPattern = FillPattern.SolidForeground;
                ColCellStyle.FillForegroundColor = HSSFColor.LightCornflowerBlue.Index;

                ColCellStyle.SetFont(HeaderFont);

                foreach (DataColumn item in dt.Columns)
                {
                    ICell cell = ColumnRow.CreateCell(icolIndex);
                    cell.SetCellValue(item.ColumnName);
                    cell.CellStyle = ColCellStyle;
                    icolIndex++;
                }
                //创建单元格属性对象
                ICellStyle CS1 = WK.CreateCellStyle();
                
                //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                CS1.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                CS1.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                CS1.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                CS1.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                CS1.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                CS1.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

                //创建字体对象
                NPOI.SS.UserModel.IFont cellfont = WK.CreateFont();
                cellfont.Boldweight = (short)FontBoldWeight.Normal;
                CS1.SetFont(cellfont);


                //创建单元格属性对象
                ICellStyle CS2 = WK.CreateCellStyle();

                //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                CS2.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                CS2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                CS2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                CS2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                CS2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                CS2.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

                CS2.FillPattern = FillPattern.SolidForeground;
                CS2.FillForegroundColor = HSSFColor.LightGreen.Index;
                CS2.SetFont(cellfont);


                //建立内容行
                int iRowIndex = 2;
                int iCellIndex = 0;
                foreach (DataRow Rowitem in dt.Rows)
                {
                    IRow DataRow = sheet.CreateRow(iRowIndex);
                    DataRow.Height = 20 * 20;
                    foreach (DataColumn Colitem in dt.Columns)
                    {

                        ICell cell = DataRow.CreateCell(iCellIndex);
                        cell.SetCellValue(Rowitem[Colitem].ToString());
                        if (DataRow.RowNum % 2 == 0)
                        {
                            cell.CellStyle = CS1;
                        }
                        else
                        {
                            cell.CellStyle = CS2;
                        }
                      
                        iCellIndex++;
                    }
                    iCellIndex = 0;
                    iRowIndex++;
                }

                //自适应列宽度
                for (int i = 0; i < icolIndex; i++)
                {
                    sheet.AutoSizeColumn(i);
                }



                //for (int columnNum = 0; columnNum <= dt.Columns.Count-1; columnNum++)
                //{
                //    int columnWidth = sheet.GetColumnWidth(columnNum) / 256;//获取当前列宽度  
                //    for (int rowNum = 2; rowNum <= sheet.LastRowNum; rowNum++)//在这一列上循环行  
                //    {
                //        IRow currentRow = sheet.GetRow(rowNum);
                //        ICell currentCell = currentRow.GetCell(columnNum);
                //        int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;//获取当前单元格的内容宽度  
                //        if (columnWidth < length + 1)
                //        {
                //            columnWidth = length + 1;
                //        }//若当前单元格内容宽度大于列宽，则调整列宽为当前单元格宽度，后面的+1是我人为的将宽度增加一个字符  
                //    }
                //    sheet.SetColumnWidth(columnNum, columnWidth * 256);
                //}





                string ExcelPath = HttpRuntime.AppDomainAppPath + "ExcelFile\\";
                if (!System.IO.Directory.Exists(ExcelPath))
                {
                    System.IO.Directory.CreateDirectory(ExcelPath);
                }
                ExcelFileName = ExcelFileName.Substring(0, L)+"("+DateTime.Now.ToString("yyyyMMddHHmmss")+")."+ExtendStr;
                //写Excel
                using (FileStream FS = new FileStream(ExcelPath + ExcelFileName, FileMode.OpenOrCreate))
                {
                    WK.Write(FS);
                    FS.Flush();
                    FS.Close();
                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally 
            { 
                WK = null; 
            }
            return ExcelFileName;
        }
        /// <summary>
        /// 将EXcel文件的数据转化成DataTable
        /// </summary>
        /// <param name="FileName">上传到服务器上的路径</param>
        /// <param name="SheetName">需要导入Excel中Sheet名称</param>
        /// <param name="Index">Sheet表中的起始行</param>
        /// <returns></returns>
        public System.Data.DataTable ExcelToData(string FileName,string SheetName,int Index)
        {
            System.Data.DataTable DT = new System.Data.DataTable();
            try
            {
                IWorkbook wk;
                HSSFFormulaEvaluator E;
                if (SheetName == "")
                {
                    SheetName = "sheet1";
                }
                using (FileStream fs = File.OpenRead(@"" + FileName))   //打开myxls.xls文件
                {
                    //获取文件的扩展名
                    int L = FileName.LastIndexOf('.');
                    string ExtendStr = FileName.Substring(L + 1);
                    //如果是2003以前的版本的话，用HSSFWorkbook实例操作Excel文件，否则用XSSFWorkbook实例才做Excel
                    if (ExtendStr.Equals("xls"))
                    {
                        //把xls文件中的数据写入wk中
                        wk = new HSSFWorkbook(fs);
                        E = new HSSFFormulaEvaluator(wk);
                    }
                    else
                    {
                        //把xls文件中的数据写入wk中
                        wk = new XSSFWorkbook(fs);
                        E = new HSSFFormulaEvaluator(wk);
                    }
                    //获取名称为StyleSheet的Sheet表
                    ISheet sheet = wk.GetSheet(SheetName);
                    //LastRowNum 是当前表的总行数
                    for (int j = Index; j <= sheet.LastRowNum; j++)  
                    {
                        IRow row = sheet.GetRow(j);   //获取Sheet表中当前行数据
                        if (row != null)
                        {
                            for (int k = 0; k <= row.LastCellNum; k++)  //获取当前行的总单元格数
                            {
                                ICell cell = row.GetCell(k);   //获取单元格
                                cell = E.EvaluateInCell(cell);
                                if (cell != null)
                                {
                                    DT.Rows[j][k] = cell.ToString();
                                }
                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                GC.Collect();
                //如果上次上传的文件，没有被删除的话，先删除上次的文件
                if (FileName!="")
                {
                    if (File.Exists(FileName))
                    {
                        DelFile(FileName);
                    }
                }
            }
            return DT;
        }
        /// <summary>
        /// 删除之前上传的Excel文件
        /// </summary>
        /// <param name="ExcelName">Excel文件名</param>
        /// <returns></returns>
        public void DelFile(string ExcelName)
        {
            //若文件存在，将其删除
            if (File.Exists(ExcelName))
            {
                File.Delete(ExcelName);
            }
        }
    }
}
