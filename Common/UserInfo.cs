﻿using System;
using System.Collections.Generic;
using System.Text;


namespace BoomLink.Common
{
    [Serializable]
    public class UserInfo
    {
        #region proprety
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UserID
        { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName
        { set; get; }

        /// <summary>
        /// 公司编号
        /// </summary>
        public string EnteID
        { set; get; }
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName
        { set; get; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public string isSystem
        { set; get;}

        /// <summary>
        /// 是否有效
        /// </summary>
        public string ValidFlag
        {
            get;
            set;
        }

        /// <summary>
        /// 是否可用
        /// </summary>
        public string isUse
        {
            get;
            set;
        }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LogTime
        {
            get;
            set;
        }
        /// <summary>
        /// 角色ID
        /// </summary>
        public string ActorID
        {
            get;
            set;
        }
        /// <summary>
        /// Emil
        /// </summary>
        public string EMail
        {
            get;
            set;
        }
        /*在线客服相关参数*/
        /// <summary>
        /// 在线客服ID
        /// </summary>
        public string SOUserID
        { get; set; }

        /// <summary>
        /// 在线客服名称
        /// </summary>
        public string SOUserName
        { get; set; }


        /*在线客服相关参数*/

        /// <summary>
        /// LoginToken
        /// </summary>
        public string LoginToken
        {
            get;
            set;
        }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// 用户类型 01-平台用户 02-企业用户
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 相关坐席信息
        /// </summary>
        public Nullable<int> FFID { get; set; }

        /// <summary>
        /// 系统产品ID
        /// </summary>
        public Nullable<int> FPRODID { get; set; }
        /// <summary>
        /// 标识
        /// </summary>
        public string Nameplate
        {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg
        {
            get;
            set;
        }

        private ConfigInfo configInfo = new ConfigInfo();
        /// <summary>
        /// 系统配置信息
        /// </summary>
        public ConfigInfo ConfigInfo
        {
            get { return configInfo; }
            set { configInfo = value; }
        }

        private CAgentCall hTGTCAgentCallCon=new CAgentCall();

        public CAgentCall HTGTCAgentCallCon
        {
            get { return hTGTCAgentCallCon; }
            set { hTGTCAgentCallCon = value; }
        }

        #endregion
    }

    [Serializable]
    public class ConfigInfo
    {
        public ConfigInfo()
        {
         
        }

        private List<ProdURL> listProdURL = new List<ProdURL>();

        /// <summary>
        /// 产品URL地址集合
        /// </summary>
        public List<ProdURL> ProdURLCollections
        {
            get { return listProdURL; }
            set { listProdURL = value; }
        }

        /// <summary>
        /// 服务器地址导航
        /// </summary>
        //public ServerSetParam ServerSet { get; set; }
    }

    [Serializable]
    public class CAgentCall
    {
        public string EpsCode { get; set; }

        public string AgtID { get; set; }

        public string SIPExt { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        public string CCSIP { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public string CCSPort { get; set; }

        /// <summary>
        /// 主叫
        /// </summary>
        public string ANI { get; set; }

        /// <summary>
        /// 被叫
        /// </summary>
        public string DNIS { get; set; }
    }

    [Serializable]
    public class ProdURL
    {
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProdName { get; set; }

        /// <summary>
        /// 产品URL地址
        /// </summary>
        public string URL { get; set; }
    }
}
