﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
namespace RMYH.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class CHtml
    {
        private bool _IsCheckBox = false;
        private bool _AutoSeq = false;
        private string _FieldText="";
        private string _FieldName="";
        private string _PkField="";
        private string _LinkField="";
        private string _LinkJS="";
        /// <summary>
        /// 
        /// </summary>
        public bool IsCheckBox
        {
            set { _IsCheckBox = value; }
            get { return _IsCheckBox; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool AutoSeq
        {
            set { _AutoSeq = value; }
            get { return _AutoSeq; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldText
        {
            set { _FieldText = value; }
            get { return _FieldText; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldName
        {
            set { _FieldName = value; }
            get { return _FieldName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PkField
        {
            set { _PkField = value; }
            get { return _PkField; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LinkField
        {
            set { _LinkField = value; }
            get { return _LinkField; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LinkJS
        {
            set { _LinkJS = value; }
            get { return _LinkJS; }
        }
        /// <summary>
        /// 获得显示数据源
        /// </summary>
        /// <param name="page">this</param>
        /// <param name="ds">ds</param>
        /// <param name="FileId">数据库列名‘,’分开,自增长ID放最前面</param>
        /// <param name="FileName">需要展示的列名‘,’分开</param>
        /// <param name="operation">操作</param>
        /// <returns></returns>
        public string GetHtml(DataSet ds, string FileId, string FileName, string operation)
        {
            string strhtml;
            string[] arrId = FileId.Split(',');
            string[] arrName = FileName.Split(',');
            string strTR = "";
            try
            {
                //string js = "function CheckAll(obj){ var objs=document.getElementsByName(\"checkbox\"); var checks=obj.checked; for(var i=0;i< objs.length;i++){objs[i].checked=checks;}}";
                //MessageBox.ResponseScript(page, js);   
                strhtml = "<table width=\"770\"  id=\"tb1\" border=\"1\" bordercolor=\"#3799CE\" cellspacing=\"0\">";
                //表头
                strTR = "<tr>";
                strTR += "<td " + (_IsCheckBox ? "" : "style = \"display:none\"") + "><input type=\"checkbox\" onclick=\"CheckAll(this)\" />全选</td>";
                for (int i = 1; i < arrName.Length; i++)
                {
                    strTR += "<td>" + arrName[i] + "</td>";
                }
                strTR += "</tr>";
                strhtml += strTR;
                //添加数据
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    strTR = "<tr>";
                    strTR += "<td " + (_IsCheckBox ? "" : "style = \"display:none\"") + "><input name=\"checkbox\" type=\"checkbox\" value=\"chk" + ds.Tables[0].Rows[i][arrId[0]].ToString() + "\" /></td>";
                    for (int j = 1; j < arrId.Length; j++)
                    {
                        strTR += "<td>" + (ds.Tables[0].Rows[i][arrId[j]] != System.DBNull.Value ? ds.Tables[0].Rows[i][arrId[j]].ToString() : "&nbsp;") + "</td>";
                    }
                    strTR += "</tr>";
                    strhtml += strTR;
                }
                strhtml += "</table>";

            }
            catch (Exception exp)
            {
                strhtml = "";
            }
            return strhtml;
        }
        #region 打开列表记录
        /// <summary>
        /// 生成列表
        /// </summary>
        /// <param name="strRowHtml">生成的列表字符串</param>
        /// <returns></returns>
        public string PrintWebList(DataSet ds)
        {
            string strRowHtml = ""; 
            string sRet = "";
            try
            {
                sRet = "<table width=\"770\"  id=\"tb1\" border=\"0\" bordercolor=\"#3799CE\" cellspacing=\"0\"><tr class='td3'>" + "\r\n";
                if (_IsCheckBox)
                {
                    sRet += "<td>";
                    sRet += "<input type='checkbox' id='chkall' name='chkall' onclick=\"CheckAll(this)\">";
                    sRet += "</td>" + "\r\n";
                }
                if (_AutoSeq)
                {
                    sRet += "<td>序号</td>" + "\r\n";
                }
                string[] ArryText = _FieldText.Split(',');
                //string[] ArryStyle = _ItemStyle.Split(',');
                string[] ArryFieldName = _FieldName.Split(',');
                for (int i = 0; i < ArryText.Length; i++)
                {
                    sRet += "<td align='center'>" + ArryText[i] + "</td>" + "\r\n";
                }
                string[] arrLinkField = _LinkField.Split(',');
                string[] arrLinkJS = _LinkJS.Split(',');
                if (_LinkField != "")
                {
                    for (int i = 0; i < arrLinkField.Length; i++)
                    {
                        sRet += "<td>&nbsp;</td>";
                    }
                }
                sRet += "</tr>" + "\r\n";
                strRowHtml = sRet;
                System.Data.DataTable tb = ds.Tables[0];
                //获得符合条件的行标识
                string sQueryIDs = "";

                //生成每行Html
                for (int i = 0; i < tb.Rows.Count; i++)
                {
                    sRet = "<tr class='white'>";
                    if (_IsCheckBox)
                    {
                        sRet += "<td nowrap style=\"width:10px;align:center\">";
                        sRet += "<input type='checkbox' name='checkbox'" + "value=\"chk" + tb.Rows[i][_PkField].ToString() + "\"/>";
                        sRet += "</td>" + "\r\n";
                    }
                    for (int j = 0; j < ArryFieldName.Length; j++)
                    {
                        string sShowVal = "&nbsp;";	//防止空值显示时没有表格边框
                        if (System.Convert.IsDBNull(tb.Rows[i][ArryFieldName[j]]) == false)
                        {
                            if (tb.Rows[i][ArryFieldName[j]].ToString().Trim() != "")
                            {
                                sShowVal = tb.Rows[i][ArryFieldName[j]].ToString();
                            }
                        }
                        //改变时间的输出格式为 "yyyy-MM-dd HH:mm:ss"
                        if (tb.Rows[i][ArryFieldName[j]].GetType() == Type.GetType("System.DateTime"))
                        {
                            if (sShowVal != "&nbsp;")
                            {
                                sShowVal = Convert.ToDateTime(sShowVal).ToString("yyyy-MM-dd");
                            }
                        }
                        sRet += "<td title='" + sShowVal + "'>" + (sShowVal.Length < 21 ? sShowVal : (sShowVal.Substring(0, 20) + "...")) + "</td>" + "\r\n";
                    }
                    //添加连接字段
                    string strLink = "";
                    if (_LinkField!="")
                    {
                        for (int n = 0; n < arrLinkField.Length; n++)
                        {
                            strLink += "<td><a href=\"javascript:" + arrLinkJS[n] + "('" + tb.Rows[i][_PkField].ToString() + "')\">" + arrLinkField[n] + "</a></td>";
                        }
                    }
                    sRet += strLink;
                    sRet += "</tr>" + "\r\n";
                    strRowHtml += sRet;

                }
                tb.Dispose();
                tb = null;
                sRet = "";
                strRowHtml += "</table>";
            }
            catch (Exception exp)
            {
                sRet = exp.Message;
            }
            return strRowHtml;
        }
        #endregion
    }
}
