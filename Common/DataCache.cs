using System;
using System.Web;

namespace RMYH.Common
{
	/// <summary>
	/// 缓存相关的操作类
    /// 2006.4.1
	/// </summary>
	public class DataCache
	{
		/// <summary>
		/// 获取当前应用程序指定CacheKey的Cache值
		/// </summary>
		/// <param name="CacheKey"></param>
		/// <returns></returns>
		public static object GetCache(string CacheKey)
		{
            return HttpRuntime.Cache.Get(CacheKey);

		}
        /// <summary>
        /// 清除Cache值
        /// </summary>
        /// <param name="CacheKey"></param>
        /// <returns></returns>
        public static void DelCache(string CacheKey)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            if (objCache.Get(CacheKey) != null)
                objCache.Remove(CacheKey);

        }
		/// <summary>
		/// 设置当前应用程序指定CacheKey的Cache值
		/// </summary>
		/// <param name="CacheKey"></param>
		/// <param name="objObject"></param>
		public static void SetCache(string CacheKey, object objObject)
		{
			HttpRuntime.Cache.Insert(CacheKey, objObject);
		}

		/// <summary>
		/// 设置当前应用程序指定CacheKey的Cache值
        /// param"CacheKey"
        /// param"objObject"
        /// </summary>
	
		public static void SetCache(string CacheKey, object objObject, DateTime absoluteExpiration,TimeSpan slidingExpiration )
		{
            if (objObject == null)
                return;
			HttpRuntime.Cache.Insert(CacheKey, objObject,null,absoluteExpiration,slidingExpiration);
		}

        //移除所有的缓存
        public static void RemoveAllCache()
        {
            var cache = HttpRuntime.Cache;
            var cacheEnum = cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cache.Remove(cacheEnum.Key.ToString());
            }
        }
	}
}
