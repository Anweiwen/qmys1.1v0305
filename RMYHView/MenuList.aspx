﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MenuList.aspx.cs" Inherits="MenuList" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
  
        <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td>
            <input type="button" class="button5" value="添加同级" style="width:80px; " onclick ="jsAddData(false)" />
            </td>
            <td>
            &nbsp;
            <input type="button" class="button5" value="添加下级" style="width:80px;" onclick ="jsAddData(true)" />
            </td>
            <td>
            &nbsp;
            <input id="Button11" class="button5"  type="button" style="width:60px;" value="展开"  onclick="TreeZK()"/>
            </td>
            <td>
            &nbsp;
            <input id="Button1" class="button5"  type="button" value="收起" style="width:60px;"  onclick="TreeSQ()"/>&nbsp;&nbsp;
            </td>
            <td><input type="button" class="button5" value="删除" style="width:60px;" onclick="DelXMBM()" />&nbsp;&nbsp;</td>
            <td><input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:80px;"/>&nbsp;&nbsp;</td>
             <td>
            &nbsp;
            <input id="Button2" class="button5"  type="button" value="保存" style="width:60px;"  onclick="Saves()"/>&nbsp;&nbsp;
            </td>
         </tr>
     </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input type="hidden" id="hidindexid" value="FUNCID" />
    <input type="hidden"   id="hidcheckid" />
    <input type="hidden"   id="HidTrid" />
    <script type ="text/javascript">
        function getlist(objtr, objid, intimagecount) {
            var rtnstr = MenuList.LoadList(objtr, objid, intimagecount).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divListView").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
        }
        function jsAddData(flag) 
        {
            if (flag && $("#hidcheckid").val() == "") 
            {
                alert('请选择父节点！');
                return;
            }
            var rtn = MenuList.AddData(flag, $("#hidcheckid").val(), "").value;
            if (rtn > 0) 
            {
                alert("添加成功！");
                getlist('', '', '');
                ZK();
                return;
            }
            else 
            {
                alert("添加失败！");
                return;
            }
        }
        function EditData(obj, flag) {
            if (flag == "0") 
            {
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "Images/Tree/edit.jpg";
            }
            else if (flag == "1") 
            {
                if (isNaN(obj.value)) 
                {
                    alert("请输入数字类型数据");
                    obj.value = "";
                    obj.focus();
                }
                else 
                {
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "Images/Tree/edit.jpg";
                }
            }
        }
        function Del() {
            if ($("#hidcheckid").val() != "")
                $("tr td[name^=td" + $("#hidindexid").val() + "]").each(function () { if ($(this).html() == $("#hidcheckid").val()) { $("#" + $(this).parent()[0].id + " img[name=readimage]").attr("src", "Images/Tree/delete.gif"); } });
        }
        function Cdel() {
            if ($("#hidcheckid").val() != "")
                $("tr td[name^=td" + $("#hidindexid").val() + "]").each(function () { if ($(this).html() == $("#hidcheckid").val()) { $("#" + $(this).parent()[0].id + " img[name=readimage][src$='delete.gif']").attr("src", "Images/Tree/noexpand.gif"); } });
        }
        function DelXMBM() {

            var ID = $("#hidcheckid").val();
            if (ID == "") 
            {
                alert("请选择要删除的项！");
                return;
            }
            else 
            {                 
                Del();
            }

        }
        function Saves() 
        {
            var Str = "", SID = "";
            var E = $("#divListView img[src$='edit.jpg']");
            var D = $("#divListView img[src$='delete.gif']");
            if (E.length == 0 && D.length == 0) 
            {
                alert("您没有更新或者删除的项！");
                return;
            }
            for (var i = 0; i < E.length; i++) 
            {
                var trid = E[i].parentNode.parentNode.id;
                if (trid != "") 
                {
                    var FUNCID = $("#" + trid).find("td[name='tdFUNCID']")[0].innerHTML;
                    var NAME = $("#" + trid + " input[name^='txtMENUCAPTION']").val();
                    var Path = $("#" + trid + " input[name^='txtDLLNAME']").val();
                    if (Str == "") 
                    {
                        Str = FUNCID + "," + NAME + "," + Path;
                    }
                    else 
                    {
                        Str = Str + "|" + FUNCID + "," + NAME + "," + Path; ;
                    }
                }
            } 
            for (var j = 0; j < D.length; j++) 
            {
                var trid = D[j].parentNode.parentNode.id;
                if (trid != "") 
                {
                    var FUNCID = $("#" + trid).find("td[name='tdFUNCID']")[0].innerHTML;
                    var MENUID = $("#" + trid).find("td[name='tdMENUID']")[0].innerHTML;
                    SID = SID == "" ? FUNCID + "," + MENUID : SID + "|" + FUNCID + "," + MENUID;
                }
            }
            var rtn = MenuList.UpdateORDel(Str, SID).value;
            if (rtn > 0) 
            {
                alert("保存成功！");
                getlist('', '', '');
                ZK();
                return;
            }
            else 
            {
                alert("保存失败！");
                return;
            }
        }
        function onselects(obj) {
            $("tr[name^='trdata']").css("backgroundColor", "");
            var trid = obj.parentNode.parentNode.id;
            $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
            $("#HidTrid").val(trid);
        }
        function TreeZK() {
            var tr = $("#divListView img[src$='tplus.gif']");
            for (var i = 0; i < tr.length; ) 
            {
                var trid = tr[i].parentNode.parentNode.id;
                ToExpand(tr[i], trid);
                i = 0;
                tr = $("#divListView img[src$='tplus.gif']");
            }
        }
        function ZK() {
            var tr = $("#divListView img[src$='tplus.gif']");
            for (var i = 0; i < tr.length;i++) {
                var trid = tr[i].parentNode.parentNode.id;
                if (parseInt($("#" + trid).find("td[name^=tdCCJB]").html()) < 2) {
                    ToExpand(tr[i], trid);
                }
            }
        }
        function TreeSQ() 
        {
            var tr = $("#divListView img[src$='tminus.gif']");
            for (var i = 0; i < tr.length; i++) 
            {
                var trid = tr[i].parentNode.parentNode.id;
                var MENUID = $("#" + trid + " td[name^='tdMENUID']").html();
                if (MENUID.length == 2) 
                {
                    ToExpand(tr[i], trid);
                }
            }
        }
        function ToExpand(obj, id) {
            var trs;
            if (obj.src.indexOf("tminus.gif") > -1) 
            {
                obj.src = "Images/Tree/tplus.gif";
                trs = $("#divListView tr");
                for (var i = 0; i < trs.length; i++) 
                {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) 
                    {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) 
                        {
                            objimg[objimg.length - 2].src = "Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) 
                        {
                            objimg[objimg.length - 2].src = "Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lminus.gif") > -1) 
            {
                obj.src = "Images/Tree/lplus.gif";
                trs = $("#divListView tr");
                for (var i = 0; i < trs.length; i++) 
                {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) 
                    {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) 
                        {
                            objimg[objimg.length - 2].src = "Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) 
                        {
                            objimg[objimg.length - 2].src = "Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lplus.gif") > -1) 
            {
                obj.src = "Images/Tree/lminus.gif";
                trs = $("#divListView tr");
                for (var i = 0; i < trs.length; i++) 
                {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) 
                    {
                        trs[i].style.display = "";
                    }
                }
            }
            else if (obj.src.indexOf("tplus.gif") > -1) 
            {
                obj.src = "Images/Tree/tminus.gif";
                trs = $("#divListView tr");
                var istoload = true;
                for (var i = 0; i < trs.length; i++) 
                {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) 
                    {
                        trs[i].style.display = "";
                        istoload = false;
                    }
                }
                if (istoload == true) {
                    getlist(id, $("tr[id=" + id + "] td[name$='tdMENUID']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                    trs = $("#divListView tr");
                    for (var i = 0; i < trs.length; i++) 
                    {
                        if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) 
                        {
                            trs[i].style.display = "";
                        }
                    }
                }
            }
            $("#divListView td[name$='tdMENUID']").attr("style","text-align:left");
        }
        $(document).ready(function () {
            getlist('', '', '');
        });   
     </script>
</asp:Content>

