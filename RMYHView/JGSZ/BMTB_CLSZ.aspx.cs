﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class JGSZ_BMTB_CLSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JGSZ_BMTB_CLSZ));
            //遍历树节点
            TreeView1.ExpandAll();
            gettree();
        }
    }
    public void gettree()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        strSql.Append("select XMDM,XMMC,case when FBDH='' then '0' else FBDH end PAR FROM TB_XMXX  ");
        DS = bll.Query(strSql.ToString());
        crertetree(TreeView1.Nodes, DS.Tables[0], "0");
    }
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("PAR='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["XMDM"].ToString();
            node.Text = row[i]["XMMC"].ToString();
            if (tb.Select("PAR='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:$('#" + hide_FBDM.ClientID + "').val('" + row[i]["XMDM"].ToString() + "');getlist('', '', '');";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }
}