﻿using System;
using System.IO;
using System.Web;
using System.Data;
using RMYH.BLL;
using System.Text;
using RMYH.DBUtility;
using System.Net;
using System.Collections.Specialized;
using System.Net.Sockets;
using System.Diagnostics;
using NPOI.XWPF.UserModel;
using System.Collections.Generic;


public partial class JHGLIndex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(JHGLIndex));
        try
        {
            if (PubConstant.SsoLogin == "true")
            {
                HidSSO.Value = "true"; //为index页面设置SSO标记
            }
            else
            {  
                HidYY.Value = HttpContext.Current.Session["YY"].ToString();
                HidSSO.Value = "false";
            }
            HidUSER.Value = HttpContext.Current.Session["USERDM"].ToString();
            HidHSZXDM.Value = HttpContext.Current.Session["HSZXDM"].ToString();
        } 
        catch
        {
            Response.Redirect("JHGLLogin.aspx");
        }
    }
    #region
    /// <summary>
    /// 获取用户的操作权限
    /// </summary>
    /// <returns></returns>
    public DataTable GetMenuQXList(string ParID)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder StrSql = new StringBuilder();
        string USERDM = HttpContext.Current.Session["USERDM"].ToString();
        StrSql.Append("SELECT MENUID,MENUCAPTION,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU");
        StrSql.Append(" WHERE  FUNCID IN (SELECT CONVERT(INT,FUNCID) FROM TB_MENUQX QX,TB_JSYH YH WHERE QX.JSDM=YH.JSDM AND USERID='" + USERDM + "') AND ACTIONRIGHT='YSFX' AND MENUID LIKE '" + ParID + "%' AND MENUID<>'" + ParID + "' ORDER BY MENUID ASC");
        DS = BLL.Query(StrSql.ToString());
        return GetCDList(USERDM, DS, ParID);
    }
    /// <summary>
    /// 获取用户的操作权限
    /// </summary>
    /// <returns></returns>
    public DataSet GetParMenuList()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder StrSql = new StringBuilder();
        string USERDM = HttpContext.Current.Session["USERDM"].ToString();
        StrSql.Append("SELECT MENUID ID,MENUCAPTION NAME,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU WHERE MENUID IN(");
        StrSql.Append(" SELECT DISTINCT SUBSTRING(MENUID,1,2) FROM SYS_MENU WHERE  FUNCID IN (SELECT CONVERT(INT,FUNCID) FROM TB_MENUQX QX,TB_JSYH YH WHERE QX.JSDM=YH.JSDM AND USERID='" + USERDM + "') AND ACTIONRIGHT='YSFX') AND ACTIONRIGHT='YSFX' ORDER BY MENUID ASC");
        return BLL.Query(StrSql.ToString());
    }
    /// <summary>
    /// 获取用户的菜单权限列表
    /// </summary>
    /// <param name="USERDM">当前登录人</param>
    /// <param name="DS">菜单数据集</param>
    /// <param name="LastPar">父节点</param>
    /// <returns></returns>
    public DataTable GetCDList(string USERDM, DataSet DS, string LastPar)
    {
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (USERDM != "SYS")
        {
            if (DS.Tables[0].Rows.Count > 0)
            {
                //定义数据列
                DT.Columns.Add("ID", Type.GetType("System.String"));
                DT.Columns.Add("NAME", Type.GetType("System.String"));
                DT.Columns.Add("URL", Type.GetType("System.String"));
                DT.Columns.Add("PAR", Type.GetType("System.String"));

                for (int i = 0; i < DS.Tables[0].Rows.Count; )
                {
                    //将菜单加到DataTable中
                    DataRow DR = DT.NewRow();
                    DR["ID"] = DS.Tables[0].Rows[i]["MENUID"].ToString();
                    DR["NAME"] = DS.Tables[0].Rows[i]["MENUCAPTION"].ToString();
                    DR["URL"] = DS.Tables[0].Rows[i]["DLLNAME"].ToString();
                    DR["PAR"] = DS.Tables[0].Rows[i]["PAR"].ToString();
                    DT.Rows.Add(DR);
                    string PAR = DS.Tables[0].Rows[i]["PAR"].ToString();
                    if (PAR != LastPar)
                    {
                        while (true)
                        {
                            DataSet S = new DataSet();
                            //获取父节点相关的信息
                            S = BLL.Query("SELECT MENUID,MENUCAPTION,DLLNAME,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU WHERE MENUID='" + PAR + "'  AND ACTIONRIGHT='YSFX'");
                            if (S.Tables[0].Rows.Count > 0)
                            {
                                //判断父节点是否已经加载到DataTable中
                                if (DT.Select("ID='" + S.Tables[0].Rows[0]["MENUID"].ToString() + "'").Length <= 0)
                                {
                                    //将父节点加到DataTable中
                                    DataRow R = DT.NewRow();
                                    R["ID"] = S.Tables[0].Rows[0]["MENUID"].ToString();
                                    R["NAME"] = S.Tables[0].Rows[0]["MENUCAPTION"].ToString();
                                    R["URL"] = S.Tables[0].Rows[0]["DLLNAME"].ToString();
                                    R["PAR"] = S.Tables[0].Rows[0]["PAR"].ToString();
                                    DT.Rows.Add(R);
                                    if (S.Tables[0].Rows[0]["PAR"].ToString() != "0")
                                    {
                                        PAR = S.Tables[0].Rows[0]["PAR"].ToString();
                                    }
                                    else
                                    {
                                        i++;
                                        break;
                                    }
                                }
                                else
                                {
                                    i++;
                                    break;
                                }
                            }
                            else
                            {
                                i++;
                                break;
                            }
                        }
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }
        else
        {
            DT = DS.Tables[0];
        }
        return DT;
    }
    /// <summary>
    /// 获取父菜单菜单列表
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMenuList()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        //获取根节点，将根节点渲染成手风琴式
        string USERDM = HttpContext.Current.Session["USERDM"].ToString();
        if (USERDM == "SYS")
        {
            DS = BLL.Query("SELECT MENUID ID,MENUCAPTION NAME FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND DATALENGTH(MENUID)=(SELECT MIN(DATALENGTH(MENUID)) FROM SYS_MENU WHERE ACTIONRIGHT='YSFX') ORDER BY MENUID ASC");
        }
        else
        {
            DS = GetParMenuList();
        }
        if (DS.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Str.Append(DS.Tables[0].Rows[i]["NAME"].ToString());
                Str.Append("$");
                Str.Append("<div id=Menu" + DS.Tables[0].Rows[i]["ID"].ToString() + " title=\"" + DS.Tables[0].Rows[i]["NAME"].ToString() + "\" style=\"padding:15px\">");
                Str.Append("<ul id=ulMenu" + DS.Tables[0].Rows[i]["ID"].ToString() + " class=\"easyui-tree\">");
                Str.Append("</ul>");
                Str.Append("</div>");
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("^");
                }
            }
        }
        return Str.ToString();
    }
    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string ParID)
    {
        StringBuilder Str = new StringBuilder();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //递归拼接子节点的Json字符串
        DataTable DT = new DataTable();
        //获取根节点，将根节点渲染成手风琴式
        string USERDM = HttpContext.Current.Session["USERDM"].ToString();
        if (USERDM == "SYS")
        {
            string sql = "SELECT MENUID ID,MENUCAPTION NAME,DLLNAME URL,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND MENUID LIKE '" + ParID + "%' AND MENUID<>'" + ParID + "' ORDER BY MENUID ASC";
            DT = BLL.Query(sql).Tables[0];

        }
        else
        {
            DT = GetMenuQXList(ParID);
        }
        RecursionChild(ParID, ref Str, DT);
        return Str.ToString();
    }
    /// <summary>
    /// 递归查询菜单子节点
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <param name="Str">拼接的JSON字符串</param>
    /// <param name="DT">数据集</param>
    private void RecursionChild(string ParID, ref StringBuilder Str, DataTable DT)
    {
        DataRow[] DR;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //SELECT MENUID ID,MENUCAPTION NAME,DLLNAME URL FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND SUBSTRING(MENUID,1,"+ParID.Length+")='"+ParID+"'
        //string sql = "SELECT MENUID ID,MENUCAPTION NAME,DLLNAME URL,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND MENUID LIKE '"+ParID+"%' AND MENUID<>'"+ParID+"'";      
        //清空之前所有Table表
        DR = DT.Select("PAR='" + ParID + "'", "ID");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["ID"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["NAME"].ToString() + "\"");
                if (dr["URL"].ToString() != "" && dr["URL"].ToString() != null)
                {
                    Str.Append(",");
                    Str.Append("\"attributes\": {\"url\": \"" + dr["URL"].ToString() + "\"}");
                }
                else
                {
                    Str.Append(",");
                    Str.Append("\"state\":\"closed\"");
                }
                //递归查询子节点
                RecursionChild(dr["ID"].ToString(), ref Str, DT);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 修改密码
    /// </summary>
    /// <param name="oldpwd">旧密码</param>
    /// <param name="newpwd">新密码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int chanagespwd(string oldpwd, string newpwd)
    {
        int rtn = -1;
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            rtn = bll.ChangePWD(oldpwd, newpwd);
        }
        catch (Exception E)
        {
            throw E;
        }
        return rtn;
    }
    /// <summary>
    /// 获取填报或者审批(已审批或者待审批）的模板列表
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="ISTB">填报或者审批标记（1代表填报；2代表审批）</param>
    /// <param name="ISDone">待操作或者已操作（-1代表待操作；0,1代表已操作）</param>
    /// <returns></returns>
    public string GetMBListSQL(string JHFADM, int ISTB, string ISDone)
    {
        StringBuilder StrSql = new StringBuilder();
        if (ISTB == 1)
        {
            if (ISDone == "-1")
            {
                StrSql.Append("SELECT * FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHFA FA,TB_JSYH YH,TB_MBJSQX QX  ");
                StrSql.Append(" WHERE LC.QZMBDM=MB.MBDM AND LC.LX='0'");
                StrSql.Append(" AND LC.MBLX=FS.YSLX AND FS.JHFADM=FA.JHFADM AND FS.ISFQ=1");
                StrSql.Append(" AND QZMBDM NOT IN (SELECT DISTINCT DQMBDM FROM TB_YSMBLC LC,TB_MBLCSB SB WHERE FA.JHFADM=SB.JHFADM AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND STATE=1 AND LX='0')");
                StrSql.Append(" AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM LIKE '%'''+YH.JSDM+'''%'");
                StrSql.Append(" AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                StrSql.Append(" UNION");
            }

        }
        StrSql.Append(" SELECT * FROM TB_MBLCSB SB,TB_JSYH YH,TB_YSBBMB MB,TB_JHFA FA,TB_MBJSQX QX");
        StrSql.Append(" WHERE SB.DQMBDM=MB.MBDM AND SB.JHFADM=FA.JHFADM");
        StrSql.Append("  AND SB.JHFADM='" + JHFADM + "' AND SB.STATE IN(" + ISDone + ") AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
        StrSql.Append("  AND SB.DQMBDM=QX.MBDM AND SB.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
        return StrSql.ToString();
    }
    /// <summary>
    /// 获取欢迎标语
    /// </summary>
    /// <returns></returns>
    public string GetUSERNAME()
    {
        return "您好，" + HttpContext.Current.Session["USERNAME"].ToString() + "！欢迎登录长庆石化公司精益计划管理系统";
    }
    #endregion
    #region 打开FineReport报表相关的代码
    /// <summary>
    /// 获取当前报表所要用的Tomcat的端口号
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetTomcatPort()
    {
        string Port = PubConstant.TomcatPort;
        return Port;
    }

    /// <summary>
    /// 获取本机的IP地址
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetLocalIPAddress()
    {
        //string strServerIP = "";
        //System.Net.IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
        //if (addressList.Length > 1)
        //{
        //    foreach (var address in addressList)
        //    {
        //        if (address.AddressFamily == AddressFamily.InterNetwork)
        //        {
        //            strServerIP = address.ToString();
        //            break;
        //        }
        //    }
        //}
        System.Net.IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
        string strNativeIP = "";
        string strServerIP = "";
        if (addressList.Length > 1)
        {
            strNativeIP = addressList[0].ToString();
            strServerIP = addressList[1].ToString();
        }
        else if (addressList.Length == 1)
        {
            strServerIP = addressList[0].ToString();
        }
        return strServerIP;
    }
    /// <summary>
    /// 获取帮助文档路径
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string loadWord()
    {

        string fileName = "长庆石化全面预算管理系统用户手册.doc";//客户端保存的文件名 
        string filePath = Server.MapPath("../Excels/Data/长庆石化全面预算管理系统用户手册.doc");//路径
        filePath = filePath.Replace("\\", "//");
        return filePath;
    }


    protected void Button4_Click(string name, string path)
    {
        string fileName = name;//客户端保存的文件名 
        string filePath = path;//路径

        //以字符流的形式下载文件 
        FileStream fs = new FileStream(filePath, FileMode.Open);
        byte[] bytes = new byte[(int)fs.Length];
        fs.Read(bytes, 0, bytes.Length);
        fs.Close();
        HttpContext.Current.Response.ContentType = "application/octet-stream";
        //通知浏览器下载文件而不是打开 
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
        HttpContext.Current.Response.BinaryWrite(bytes);
        HttpContext.Current.Response.Flush();
        //HttpContext.Current.Response.End();
    }


    /// <summary>
    /// 登录选择月份
    /// </summary>
    /// <param name="Y"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetDDLYear(string Y)
    {
        int Year = DateTime.Now.Year - 5;
        string YY = "";
        StringBuilder Str = new StringBuilder();
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();

        if (Y != null && Y != "")
        {
            YY = Y;
        }
        else
        {
            YY = DateTime.Now.Year.ToString();
        }

        Str.Append("[");
        for (int i = Year; i <= Year + 10; i++)
        {
            Str.Append("{\"id\":");
            Str.Append("\"" + i.ToString() + "\"" + ",");
            Str.Append("\"text\":");
            Str.Append("\"" + i.ToString() + "\"" + "}");
            if (i <= Year + 10 - 1)
            {
                Str.Append(",");
            }

        }
        Str.Append("]");

        return Str.ToString();
    }

    /// <summary>
    /// 设置登录月份
    /// </summary>
    /// <param name="Y"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public void SetLoginY(string Y)
    {
        HttpContext.Current.Session["YY"] = Y;
    }

    #endregion
}