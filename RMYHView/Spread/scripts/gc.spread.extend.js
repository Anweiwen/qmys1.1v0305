/**
* Created by LGS on 2017-05-17 .
*/
var searchmap = {};
var c = /^[a-zA-Z0-9\u4e00-\u9fa5]{1}/;  //首字符为字母,数字或汉字！
//var zydm = "",cpdm = "",jsdx="",xmfl="",sjdx="",jldw="";
//var rowacbs = undefined, colacbs = undefined

$(document).ready(function () {
    initSpreadSreach(spread);

    $('#selformula').combobox({
        onChange: function (n, o) {
            $.ajax({
                type: 'get',
                url: geturlpath() + 'ExcelData.ashx?action=liebiao&ID=' + $('#selformula').combobox('getValue'),
                async: true,
                success: function (result) {
                    document.getElementById("ggsz").innerHTML = "";
                    document.getElementById("ggsz").innerHTML += result;
                    $.parser.parse('#ggsz'); // parse the specified node  
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
    });

    //var comment = new GC.Spread.Sheets.Comments();
    //            var comment=new GC.Spread.Sheets.Comments.Comment;
    //            comment.text("new comment!");
    //            comment.backColor("yellow");
    //            comment.foreColor("green");
    //            //comment.displayMode(GC.Spread.Sheets.Comments.DisplayMode.HoverShown);
    //            comment.displayMode(false);
    //            spread.getActiveSheet().getCell(1,1).comment(comment);
    //            console.log("comment:"+spread.getActiveSheet().getCell(1,1).comment().text());
    //
    //
    //            var activeSheet = spread.getActiveSheet();
    //            activeSheet.getCell(1,1).watermark("lightgreen");


});

function initSpreadSreach(spread) {
    $("#searchNext").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {}; //清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 0;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookEnd(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookBefore(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        }

        if (searchResult != null && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
            spread.setActiveSheetIndex(searchResult.foundSheetIndex);
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var con = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).value();
                var resformat = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).formula();
                var coordinate = '$' + searchResult.foundRowIndex + '$' + searchResult.foundColumnIndex;
                var id = searchResult.foundRowIndex.toString() + searchResult.foundColumnIndex.toString();
                var format = resformat == null ? "" : resformat;
                document.getElementById("tablecontent").innerHTML = '<tr id="' + id + '"><td>' + spread.getSheet(searchResult.foundSheetIndex).name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                initTrClick(); //设置table的点击事件
                sheet.repaint();
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("未找到");
        }
    });
    $("#searchAll").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {}; //清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 1;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookEnd(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookBefore(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        }

        if (searchResult != null && JSON.stringify(searchResult) != "{}" && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
            //                    spread.setActiveSheetIndex(searchResult.foundSheetIndex);
            //                    var sheet = spread.getActiveSheet();
            //                    sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var res = '', n = 0;
                for (var key in searchResult) {
                    //填充查询结果到表格上展示。。。
                    var con = spread.getSheet(searchResult[key].foundSheetIndex).getCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex).value();
                    var resformat = spread.getSheet(searchResult[key].foundSheetIndex).getCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex).formula();
                    var coordinate = '$' + searchResult[key].foundRowIndex + '$' + searchResult[key].foundColumnIndex;
                    var id = searchResult[key].foundRowIndex.toString() + searchResult[key].foundColumnIndex.toString();
                    var format = resformat == null ? "" : resformat;
                    res += '<tr id="' + id + '"><td>' + spread.getSheet(searchResult[key].foundSheetIndex).name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                    if (n == 0) {//光标定位第一个元素
                        spread.setActiveSheetIndex(searchResult[key].foundSheetIndex);
                        var sheet = spread.getActiveSheet();
                        sheet.setActiveCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex);
                        n++;
                    }
                }
                document.getElementById("tablecontent").innerHTML = res;
                initTrClick(); //设置table的点击事件
                //sheet.repaint();
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("未找到");
        }
    });
    $("#replaceNext").click(function () {
        var sheet = "";
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        console.log(searchmap);
        if (JSON.stringify(searchmap) != "{}") { //if条件不支持IE8
            if (searchmap.foundSheetIndex != undefined) {//if条件判断searchmap是单个对象，不是map的{key:value}结构
                spread.setActiveSheet(spread.getSheet(searchmap.foundSheetIndex).name());
                sheet = spread.getActiveSheet();
                if (lookin == "值") {
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value(repval);
                    var id = searchmap.foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                }
                if (lookin == "公式") {
                    var val = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula(); //获取原来的公式
                    val = val.replace(new RegExp(oldval, "gi"), repval);
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula(val);
                    var id = searchmap[key].foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                    $("#" + id + "").children()[3].innerText = val; //更新table的公式
                }
            } else {
                if (lookin == "值") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value(repval);
                        var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                        $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value() //更新值
                        delete (searchmap[key]); //删除已经替换的元素
                        break;
                    }
                }
                if (lookin == "公式") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula(); //获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula(val); //替换单元格的公式
                            var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                            var RowIndex = searchmap[key].foundRowIndex;
                            var ColIndex = searchmap[key].foundColumnIndex;
                            var SheetIndex = searchmap[key].foundSheetIndex;
                            var FoundFlag = searchmap[key].searchFoundFlag;
                            delete (searchmap[key]); //删除已经替换的元素

                            searchmap[key] = {
                                foundRowIndex: RowIndex,
                                foundColumnIndex: ColIndex,
                                foundSheetIndex: SheetIndex,
                                foundString: val,
                                searchFoundFlag: FoundFlag
                            };
                        } else {
                            alert('首字符不能为非字母！');
                        }
                        ;

                        break;
                    }
                }
            }
        }
    });
    $("#replaceAll").click(function () {
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var sheet = spread.getActiveSheet();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        if (searchmap != []) { //判断searchmap是否为空，if条件不支持IE8
            if (searchmap.foundSheetIndex == undefined) {//if条件判断searchmap是map的{key:value}结构，不是单个对象
                if (lookin == "值") {
                    for (var i = 0, max = searchmap.length; i < max; i++) {
                        spread.setActiveSheet(spread.getSheet(searchmap[i].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).value(repval);
                        var id = searchmap[i].foundRowIndex.toString() + searchmap[i].foundColumnIndex.toString();
                        $("#" + id + "").children()[2].innerText = repval //更新值
                    }
                }
                if (lookin == "公式") {
                    for (var i = 0, max = searchmap.length; i < max; i++) {
                        spread.setActiveSheet(spread.getSheet(searchmap[i].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).formula(); //获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).formula(val); //替换单元格的公式
                            var id = searchmap[i].foundRowIndex.toString() + searchmap[i].foundColumnIndex.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                        } else {
                            alert('首字符不能为非字母！');
                            continue;
                        }
                    }
                }
            } else {

            }
        }
    });
}

function initCellPerperty() {

    //设置单元格属性
    $("#setDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var tag = $("#cellTagInputMessage").val();

        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                rowIndex = sel.row,
                colIndex = sel.col,
                rowCount = sel.rowCount,
                colCount = sel.colCount,
                maxRow = rowIndex + rowCount,
                maxColumn = colIndex + colCount,
                r, c;

            if (rowIndex === -1 && colIndex === -1) {
                sheet.tag(tag);
            } else if (rowIndex === -1) {
                for (c = colIndex; c < maxColumn; c++) {
                    sheet.setTag(-1, c, tag);
                }
            } else if (colIndex === -1) {
                for (r = rowIndex; r < maxRow; r++) {
                    sheet.setTag(r, -1, tag);
                }
            } else {
                for (r = rowIndex; r < maxRow; r++) {
                    for (c = colIndex; c < maxColumn; c++) {
                        sheet.setTag(r, c, tag);
                    }
                }
            }
        }

        sheet.resumePaint();
    });

    //获取单元格属性
    $("#getDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        var sel = selections[0],
            row = sel.row,
            col = sel.col,
            $tag = $("#cellTagInputMessage");

        if (row === -1 && col === -1) {
            $tag.val(sheet.tag());
        } else {
            $tag.val(sheet.getTag(row, col));
        }
    });

    //清除单元格属性
    $("#clearDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                row = sel.row,
                col = sel.col;

            if (row === -1 && col === -1) {
                sheet.tag(null);
            } else {
                sheet.clear(row, col, sel.rowCount, sel.colCount, spreadNS.SheetArea.viewport, spreadNS.StorageType.tag);
            }
        }

        sheet.resumePaint();
    });

    //查找单元格属性
    $("#searchTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        //'0':Column First (ZOrder)    "1":Row First (NOrder)
        var searchOrder = parseInt('0', 10);

        if (isNaN(searchOrder)) {
            return;
        }

        var condition = new spreadNS.Search.SearchCondition();
        condition.searchTarget = spreadNS.Search.SearchFoundFlags.cellTag;
        condition.searchString = $("#cellTagInputTitle").val();
        condition.findBeginRow = sheet.getActiveRowIndex();
        condition.findBeginColumn = sheet.getActiveColumnIndex();

        condition.searchOrder = searchOrder;
        if (searchOrder === 0) {
            condition.findBeginColumn++;
        } else {
            condition.findBeginRow++;
        }

        var result = sheet.search(condition);
        if (result.foundRowIndex < 0 && result.foundColumnIndex < 0) {
            condition.findBeginRow = 0;
            condition.findBeginColumn = 0;
            result = sheet.search(condition);
        }

        var row = result.foundRowIndex,
            col = result.foundColumnIndex;

        if (row < 0 && col < 0) {
            $("#cellTagInputMessage").val("未找到");
        }
        else {
            sheet.setActiveCell(row, col);
            $("#cellTagInputMessage").val(sheet.getTag(row, col));
        }
    });
}

/**
* Created by LI on 2017-05-17 .
* 获取当前文件url
*/
function geturlpath() {
    var href = window.parent.document.location.href;
    var h = href.split("/");
    href = "";
    for (i = 0; i < h.length - 1; i++) {
        href += h[i] + "/";
    }
    return href;
}

function openJsonOrExcel(row) {
    var path = geturlpath();
    $.ajax({
        type: "POST",
        url: path + 'ExcelData.ashx?action=GetUrlPath&' + row,
        success: function (result) {
            if (result == '1') {
                openJson(row);
            } else {
                openexcel(row);
            }
        },
        error: function (e) {
            alert(e);
        }
    });
}
function openExcelOrJson(row, isExist) {
    var path = geturlpath();
    if (ParamDic["MBLB"] == "queryflow" || ParamDic["MBLB"] == "basesb" || ParamDic["MBLB"] == "basesp") {
        path = "../Data/";
    }
    if (!isExist)
        path = path.substring(0, path.indexOf("Data")) + "ExcelMb/";
    $.ajax({
        type: "POST",
        url: path + 'ExcelData.ashx?action=GetUrlPath&' + row,
        success: function (result) {
            if (result == '1') {
                openJson(row, isExist);
            } else {
                openexcel(row, isExist);
            }
        },
        error: function (e) {
            alert(e);
        }
    });
}
/**
* Created by LI on 2017-05-17 .
* 请求服务器端excel在页面展示
*/
function openexcel(selected, IsExist) {
    var excelIo = new GC.Spread.Excel.IO();
    if (IsExist == undefined) {
        var tbName = selected.substring(selected.lastIndexOf("=") + 1);
    }
    // Download Excel file
    var path = window.document.location.pathname.substring(1);
    var Len = path.indexOf("/");
    var root = "/" + path.substring(0, Len + 1);
    if ((ParamDic["MBSZ"] == "ExcelData" && IsExist) || ParamDic["MBLB"] == "queryflow" || ParamDic["MBLB"] == "respys") {
        root = root + "Excels/Data/";
    }
    else {
        root = root + "Excels/ExcelMb/";
    }
    var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';

    xhr.onload = function (e) {
        if (this.status == 200) {
            //MySelfMethod();
            // get binary data as a response
            var blob = this.response;
            // convert Excel to JSON
            excelIo.open(blob, function (json) {
                json.tabStripVisible = true; //加上这句，spreadjs 的excel会显示sheet页。
                importJson(json);
                $("#toggleInspector").click();
                //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
                //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
                if ((ParamDic["MBSZ"] == "ExcelData" && IsExist != undefined) || ParamDic["MBLB"] == "queryflow" || ParamDic["MBLB"] == "respys") {
                    //如果存在数据页的话，则执行下面操作
                    if (IsExist) {
                        //打开Excel之后的操作
                        AfterOpen();
                        dialogshow('已填报文件加载完成!');
                        if (ParamDic["MBLB"] == "queryflow" || ParamDic["MBLB"] == "respys") return;
                        savetoS(true);
                    }
                    else {
                        //不存在执行下面操作
                        AfterOpenMb();
                        dialogshow('模板文件加载完成!');
                    }
                } else {
                    if (geturlpath().indexOf("ExcelMB") > -1) {
                        savetoS(true);
                    } else {
                        savetoS(false);
                        IsExcelOrJsonExisted(tbName, tbName.substring(0, tbName.lastIndexOf('.')) + ".json");
                    }
                }

            }, function (e) {
                // process error
                alert(e.errorMessage);
            }, {});
        }
    };

    xhr.send();

    //判断当前打开的时数据页时，需要打开模板页获取公式和样式
    if (ParamDic["MBSZ"] == "ExcelData" && IsExist != undefined) {
        OpenMBExcel();
    }
}

function OpenMBExcel() {
    var path = geturlpath();
    path = path.substring(0, path.indexOf("Data")) + "ExcelMb/";
    var url = path + 'ExcelData.ashx?action=GetExcelorJson&MBWJ=' + ParamDic["MBWJ"];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';

    xhr.onload = function (e) {
        if (this.status == 200) {
            var json = JSON.parse(this.response);
            //获取模板页的单元格数据
            HidMBJson = json.sheets;
            //获取模板页样式
            var sty = json.namedStyles;
            for (var i = 0; i < sty.length; i++) {
                HidMBStyle[sty[i].name] = sty[i];
            }
        }
    };
    xhr.send();
}




/**
* Created by LI on 2017-05-17 .
* 请求服务器端json数据，在页面展示
*/
function openJson(selected, IsExist) {
    var path = window.document.location.pathname.substring(1);
    var Len = path.indexOf("/");
    var root = "/" + path.substring(0, Len + 1);
    if (ParamDic["MBSZ"] == "ExcelData" && IsExist) {
        root = root + "Excels/Data/";
    }
    else {
        root = root + "Excels/ExcelMb/";
    }
    var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected + "&rand=" + Math.random();
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';

    xhr.onload = function (e) {
        if (this.status == 200) {
            //设置自定义公式
            MySelfMethod();
            // get binary data as a response
            importJson(JSON.parse(this.response));
            $("#toggleInspector").click();
            //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
            //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
            if (ParamDic["MBSZ"] == "ExcelData") {
                //如果存在数据页的话，则执行下面操作
                if (IsExist) {
                    //打开Excel之后的操作
                    AfterOpen();
                    dialogshow('已填报文件加载完成!');
                }
                else {
                    //不存在执行下面操作
                    AfterOpenMb();
                    dialogshow('模板文件加载完成!');
                }
            }
            savetoS(true);
            delSpacseValue();
        }
    };

    xhr.send();

    //判断当前打开的时数据页时，需要打开模板页获取公式和样式
    //    if (ParamDic["MBSZ"] == "ExcelData") {
    //        debugger;
    //        OpenMBExcel();
    //    }
}

//遍历每个sheet页的第一行，第一列的单元格，将单元格值是""，替换为null
function delSpacseValue() {
    var count = spread.getSheetCount();
    for (var i = 0; i < count; i++) {
        var sheet = spread.getSheet(i);
        var rowCount = sheet.getRowCount();
        var colCount = sheet.getColumnCount();
        for (var j = 0; j < rowCount; j++) {
            var value = sheet.getCell(j, 0).value();
            if (value != null) {
                if (typeof (value) == "string") if (value.trim() == "") { sheet.getCell(j, 0).value(null); }
            }
        }
        for (var k = 0; k < colCount; k++) {
            var value = sheet.getCell(0, k).value();
            if (value != null) {
                if (typeof (value) == "string") if (value.trim() == "") { sheet.getCell(0, k).value(null); }
            }
        }
    }
}

/**
* Created by LI on 2017-05-17 .
* 保存excel或json到服务器端
*/
function savetoS(con) {
    if (con) {
        var path = geturlpath();
        if (ParamDic["MBLB"] == "basesb" || ParamDic["MBLB"] == "basesp") {
            path = "../Data/";
        }
        saveJsonToServer("file", row, path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer', "yes"); //保存json
        savetoServer("file", row, path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer', "no");   //保存excel
    } else {
        var path = geturlpath();
        path = path.substring(0, path.indexOf("Data")) + "ExcelMB/";
        //        var row1=
        //            `MBSZ=EXCELMBSZ&MBDM=${ParamDic["MBDM"]}&MBWJ=${ParamDic["MBWJ"]}&MBMC=${ParamDic["MBMC"]}&rand=${new Date().getTime()}`;
        var row1 = "MBSZ=EXCELMBSZ&MBDM=" +
            ParamDic["MBDM"] +
            "&MBWJ=" +
            ParamDic["MBWJ"] +
            "&MBMC=" +
            ParamDic["MBMC"] +
            "&rand=" +
            new Date().getTime();
        saveJsonToServer("file", row1, path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer', "yes");
    }
}
//显示遮罩层
function showMask() {
    var hideobj = document.getElementById("hidebg");
    hideobj.style.display = "block"; //显示遮罩层
    hideobj.style.height = document.body.clientHeight + "px"; //设置遮罩层的高度为当前页面高度
}
//去除遮罩层
function hideMask() {
    document.getElementById("hidebg").style.display = "none";
}
/**
* 表格的单击和双击事件
* */
function initTrClick() {
    if ($("#tablecontent tr").length > 0) {
        $("#tablecontent tr").click(function (e) {
            spread.setActiveSheet($(e.target).parent().children()[0].innerText); //设置活动sheet页
            var x = $(e.target).parent().children()[1].innerText.split('$')[1];
            var y = $(e.target).parent().children()[1].innerText.split('$')[2];
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(parseInt(x), parseInt(y)); //设置活动单元格
        });

        $("#tablecontent tr").dblclick(function (e) {
            $("#workbook").val($(e.target).parent().children()[0].innerText);
            $("#cells").val($(e.target).parent().children()[1].innerText);
            $("#val").val($(e.target).parent().children()[2].innerText);
            $("#formula").val($(e.target).parent().children()[3].innerText);
            $('#myModal').modal(); //打开bootstroop dialog
        });
    }
}

function getSpanInfo(sheet, row, col) {
    var span = sheet.getSpans(new spreadNS.Range(row, col, 1, 1));
    if (span.length > 0) {
        return { rowSpan: span[0].rowCount, colSpan: span[0].colCount };
    } else {
        return { rowSpan: 1, colSpan: 1 };
    }
}

function getResultSearchinSheetEnd(searchCondition) {
    var sheet = spread.getActiveSheet();
    if (searchCondition.searchAll == 0) {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (searchCondition.searchAll == 1) {
        if (searchCondition.startSheetIndex == -1 && searchCondition.endSheetIndex == -1) {
            searchCondition.startSheetIndex = 0;
            searchCondition.endSheetIndex = spread.getSheetCount() - 1;
        } else {
            searchCondition.startSheetIndex = spread.getActiveSheetIndex();
            searchCondition.endSheetIndex = spread.getActiveSheetIndex();
        }
    }

    if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.zOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = sheet.getActiveRowIndex();
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex() + 1
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    } else if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.nOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = searchCondition.findBeginRow = sheet.getActiveRowIndex() + 1;
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex()
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    }

    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    }
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinSheetBefore(searchCondition) {
    var sheet = spread.getActiveSheet();
    searchCondition.startSheetIndex = spread.getActiveSheetIndex();
    searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.findBeginRow = sel.row;
        searchCondition.findBeginColumn = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    } else {
        searchCondition.rowStart = -1;
        searchCondition.columnStart = -1;
        searchCondition.findBeginRow = -1;
        searchCondition.findBeginColumn = -1;
        searchCondition.rowEnd = sheet.getActiveRowIndex();
        searchCondition.columnEnd = sheet.getActiveColumnIndex();
    }

    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookEnd(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = spread.getActiveSheetIndex() + 1;
    searchCondition.endSheetIndex = -1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookBefore(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = -1
    searchCondition.endSheetIndex = spread.getActiveSheetIndex() - 1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

//获取查找条件
function getSearchCondition() {
    var searchCondition = new spreadNS.Search.SearchCondition();
    var findWhat = $("div[data-name='searchontent'] input").val(); //$("#txtSearchWhat").prop("value");
    var within = $("div[id='within'] span[class='display']").text(); //$("#searchWithin").prop("value");
    var order = $("div[id='searchin'] span[class='display']").text(); //$("#searchOrder").prop("value");
    var lookin = $("div[id='lookin'] span[class='display']").text(); //$("#searchLookin").prop("value");
    var matchCase = $("div[data-name='matchCcase']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"; //$("#chkSearchMachCase").prop("checked");
    var matchEntire = $("div[data-name='matchExactly']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"; //$("#chkSearchMachEntire").prop("checked");
    var useWildCards = $("div[data-name='useWildcards']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"//$("#chkSearchUseWildCards").prop("checked");

    searchCondition.searchString = findWhat;
    if (within == "工作表") {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (order == "列") {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.nOrder;
    } else {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.zOrder;
    }
    if (lookin == "公式") {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellFormula;
    } else {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellText;
    }

    if (!matchCase) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.ignoreCase;
    }
    if (matchEntire) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.exactMatch;
    }
    if (useWildCards) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.useWildCards;
    }


    return searchCondition;
}

/***
* 获取浏览器版本信息
*  alert(browser);//IE 11.0
IE11以下： MSIE 10.0、MSIE9.0等
chrome：chrome/41.0.2272.89 [返回的是个数组]
firefox: firefox/42.0 [返回的是个数组]
*
* */
function getBrowserInfo() {
    var agent = navigator.userAgent.toLowerCase();
    //console.log(agent);
    //agent chrome : mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/41.0.2272.89 safari/537.36
    //agent firefox : mozilla/5.0 (windows nt 6.1; wow64; rv:42.0) gecko/20100101 firefox/42.0
    //agent  IE11: mozilla/5.0 (windows nt 6.1; wow64; trident/7.0; slcc2; .net clr 2.0.50727; .net clr 3.5.30729;
    //接上.net clr 3.0.30729; media center pc 6.0;infopath.2; .net4.0c; .net4.0e; rv:11.0) like gecko
    //agent  IE10:   mozilla/5.0(compatible; msie 10.0; windows nt 6.1; wow64; trident/6.0)
    var regStr_ie = /msie [\d.]+;/gi;
    var regStr_ff = /firefox\/[\d.]+/gi
    var regStr_chrome = /chrome\/[\d.]+/gi;
    var regStr_saf = /safari\/[\d.]+/gi;
    //IE11以下
    if (agent.indexOf("msie") > 0) {
        return agent.match(regStr_ie);
    }
    //IE11版本中不包括MSIE字段
    if (agent.indexOf("trident") > 0 && agent.indexOf("rv") > 0) {
        return "IE " + agent.match(/rv:(\d+\.\d+)/)[1];
    }
    //firefox
    if (agent.indexOf("firefox") > 0) {
        return agent.match(regStr_ff);
    }
    //Chrome
    if (agent.indexOf("chrome") > 0) {
        return agent.match(regStr_chrome);
    }
    //Safari
    if (agent.indexOf("safari") > 0 && agent.indexOf("chrome") < 0) {
        return agent.match(regStr_saf);
    }
}


//设置自定义公式
function MySelfMethod() {
    $.ajax({
        type: 'get',
        url: '../../Excels/Data/MBDataHandler.ashx',
        cache: false,
        async: false,
        data: {
            action: 'GetSelfFormula'
        },
        success: function (result) {
            var Arr = [];
            var sucjson = eval('(' + result + ')');
            if (sucjson) {
                for (var i = 0; i < sucjson["count"]; i++) {
                    var jsonrow = sucjson["grids"][i];
                    Arr[i] = (function () {
                        var MyFunction = function () { };
                        MyFunction.prototype = new GC.Spread.CalcEngine.Functions.Function("N_" + jsonrow["GS"], 0, 20);
                        MyFunction.prototype.evaluate = function () {
                            var C = MyFunction.prototype.name.indexOf("_");
                            return C == -1 ? "[" + MyFunction.prototype.name + "]" : "[" + MyFunction.prototype.name.substring(C + 1) + "]";
                        }
                        return MyFunction;
                    })();

                    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_" + jsonrow["GS"], new Arr[i]());
                }
            }
        },
        error: function (req) {
            $.messager.alert("提示框", req.responseText);
            return;
        }
    });
    //      debugger;
    //    var YSXMGS = function () {

    //    };
    //    YSXMGS.prototype = new GC.Spread.CalcEngine.Functions.Function("N_YSXMGS", 0, 100);
    //    YSXMGS.prototype.evaluate = function () {
    //        Len = YSXMGS.prototype.name.indexOf("_");
    //        return Len == -1 ? "[" + YSXMGS.prototype.name + "]" : "[" + YSXMGS.prototype.name.substring(Len + 1) + "]";
    //    }
    //    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_YSXMGS", new YSXMGS());

    //    var CWQSGS = function () {

    //    };
    //    CWQSGS.prototype = new GC.Spread.CalcEngine.Functions.Function("N_CWQSGS", 0, 100);
    //    CWQSGS.prototype.evaluate = function () {
    //        Len = CWQSGS.prototype.name.indexOf("_");
    //        return Len == -1 ? "[" + CWQSGS.prototype.name + "]" : "[" + CWQSGS.prototype.name.substring(Len + 1) + "]";
    //    }
    //    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_CWQSGS", new CWQSGS());
}




function prompt(rtn) {
    $.messager.show({
        title: '提示',
        msg: rtn,
        timeout: 1000,
        showType: 'slide'
    });
}


//easyui前端分页
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        data.originalRows = (data.rows);
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

