﻿var uiResource = {
    toolBar: {
        zoom: {
            title: "Zoom",
            zoomOption: {
                twentyFivePercentSize: "25%",
                fiftyPercentSize: "50%",
                seventyFivePercentSize: "75%",
                defaultSize: "100%",
                oneHundredTwentyFivePercentSize: "125%",
                oneHundredFiftyPercentSize: "150%",
                twoHundredPercentSize: "200%",
                threeHundredPercentSize: "300%",
                fourHundredPercentSize: "400%"
            }
        },
        clear: {
            title: "清除",
            clearActions: {
                clearAll: "清除所有",
                clearFormat: "清除格式"
            }
        },
        export: {
            title: "本地保存",
            exportActions: {
                exportJson: "保存到JSON",
                exportExcel: "保存到Excel"
            }
        },
        downloadTitle: "Save File",
        download: "Right Click To Download Linked File As...",
        showInspector: "Show Inspector",
        hideInspector: "Hide Inspector",
        importJson: "Import JSON",
        importFile: "本地打开",
        insertTable: "插入表格",
        insertPicture: "插入图片",
        insertComment: "插入注释",
        insertSparkline: "Insert Sparkline",
        insertSlicer: "Insert Slicer",
        serverSave:"服务器端保存",
        formulaSetup:"公式设置",
        transFormula:"公式翻译",
        rowProperty:"行属性设置",
        colProperty:"列属性设置",
        checkFormula:"校验公式",
        colHide:"隐藏列设置",
        HQZXMB:"获取最新模板",
        SCSJ:"生成数据",
        ZJSJ:"追加数据",
        JSBB:"计算报表",
        QQYJS:"全区域计算",
        HQHLSX:"获取行列属性",
        InsertRow:"插入变动行",
        DelBdRow:"删除变动行",
        BC:"保存",
        SB:"上报",
        SP:"审批",
        DH:"退回",
        DY:"打印"
    },
    tabs: {
        spread: "Spread",
        sheet: "工作表",
        cell: "单元格",
        table: "表格",
        data: "数据",
        comment: "注释",
        picture: "图片",
        sparklineEx: "图表",
        slicer: "切片器"
    },
    spreadTab: {
        general: {
            title: "常规",
            allowDragDrop: "允许拖放",
            allowDragFill: "允许拖动和填充",
            allowZoom: "允许缩放",
            allowOverfolow: "允许溢出",
            showDragFillSmartTag: "显示拖动填充智能标记"
        },
        calculation: {
            title: "计算",
            referenceStyle: {
                title: "行号列标表示",
                r1c1: "R1C1",
                a1: "A1"
            }
        },
        scrollBar: {
            title: "滚动条",
            showVertical: "垂直滚动条",
            showHorizontal: "水平滚动条",
            maxAlign: "最大滚动对齐",
            showMax: "滚动条显示最大",
            scrollIgnoreHidden: "滚动忽略隐藏行或列"
        },
        tabStip: {
            title: "选项卡",
            visible: "选项卡可见",
            newTabVisible: "新标签可见",
            editable: "选项卡编辑",
            showTabNavigation: "显示选项卡导航"
        },
        color: {
            title: "颜色",
            spreadBackcolor: "Spread 背景色",
            grayAreaBackcolor: "灰色区域背景颜色"
        },
        tip: {
            title: "提示",
            showDragDropTip: "显示拖放提示",
            showDragFillTip: "显示拖动填充提示",
            scrollTip: {
                title: "滚动提示",
                values: {
                    none: "无",
                    horizontal: "水平",
                    vertical: "垂直",
                    both: "水平垂直"
                }
            },
            resizeTip: {
                title: "调整提示",
                values: {
                    none: "无",
                    column: "列",
                    row: "行",
                    both: "行列"
                }
            }
        },
        sheets: {
            title: "工作表",
            sheetName: "工作表名称",
            sheetVisible: "工作表可见",
            DeleteSheet:"删除工作表"
        },
        cutCopy: {
            title: "剪切 / 粘贴",
            cutCopyIndicator: {
                visible: "显示指标",
                borderColor: "指示器边框颜色"
            },
            allowCopyPasteExcelStyle: "允许复制粘贴Excel样式",
            allowExtendPasteRange: "允许扩展粘贴范围",
            copyPasteHeaderOptions: {
                title: "标题选项",
                option: {
                    noHeaders: "无标题",
                    rowHeaders: "行标题",
                    columnHeaders: "列标题",
                    allHeaders: "全部标题"
                }
            }
        },
        spreadTheme: {
            title: "Spread 主题",
            theme: {
                title: "主题",
                option: {
                    spreadJS: "SpreadJS",
                    excel2013White: "Excel2013 White",
                    excel2013LightGray: "Excel2013 Light Gray",
                    excel2013DarkGray: "Excel2013 Dark Gray",
                    excel2016Colorful: "Excel2016 Colorful",
                    excel2016DarkGray: "Excel2016 Dark Gray"
                }
            }
        },
        resizeZeroIndicator: {
            title: "调整零指标",
            option: {
                defaultValue: "默认",
                enhanced: "增强"
            }
        }
    },
    sheetTab: {
        general: {
            title: "常规",
            rowCount: "行数",
            columnCount: "列数",
            name: "名称",
            tabColor: "标签颜色"
        },
        freeze: {
            title: "冻结",
            frozenRowCount: "标题行",
            frozenColumnCount: "标题列",
            trailingFrozenRowCount: "页脚行",
            trailingFrozenColumnCount: "页脚列",
            frozenLineColor: "颜色",
            freezePane: "冻结",
            unfreeze: "取消冻结"
        },
        gridLine: {
            title: "网格线",
            showVertical: "垂直可见",
            showHorizontal: "水平可见",
            color: "颜色"
        },
        header: {
            title: "标题",
            showRowHeader: "行标题可见",
            showColumnHeader: "列标题可见"
        },
        selection: {
            title: "选择",
            borderColor: "边框颜色",
            backColor: "背景颜色",
            hide: "隐藏选择",
            policy: {
                title: "策略",
                values: {
                    single: "单个",
                    range: "范围",
                    multiRange: "多范围"
                }
            },
            unit: {
                title: "单元",
                values: {
                    cell: "单元格",
                    row: "行",
                    column: "列"
                }
            }
        },
        protection: {
            title: "保护",
            protectSheet: "保护工作表",
            selectLockCells: "选择锁定单元格",
            selectUnlockedCells: "选择未锁定单元格",
            sort: "排序",
            useAutoFilter: "使用自动筛选",
            resizeRows: "调整行",
            resizeColumns: "调整列",
            editObjects: "编辑对象"
        }
    },
    cellTab: {
        style: {
            title: "样式",
            fontFamily: "字体",
            fontSize: "大小",
            foreColor: "前景色",
            backColor: "背景色",
            waterMark: "水印",
            cellPadding: "补白",
            cellLabel: {
                title: "标签选项",
                visibility: "可见",
                visibilityOption: {
                    auto: "自动",
                    visible: "可见",
                    hidden: "隐藏"
                },
                alignment: "调整",
                alignmentOption: {
                    topLeft: "顶部左对齐",
                    topCenter: "顶部居中",
                    topRight: "顶部右对齐",
                    bottomLeft: "底部左对齐",
                    bottomCenter: "底部居中",
                    bottomRight: "底部右对齐"
                },
                fontFamily: "字体",
                fontSize: "大小",
                foreColor: "前景色",
                labelMargin: "边缘"
            },
            borders: {
                title: "边框",
                values: {
                    bottom: "底框",
                    top: "顶端边框",
                    left: "左侧边框",
                    right: "右侧边框",
                    none: "无边框",
                    all: "全部边框",
                    outside: "外围框线",
                    thick: "厚框边框",
                    doubleBottom: "底端双边框",
                    thickBottom: "厚底边界",
                    topBottom: "上下边界",
                    topThickBottom: "上下粗边",
                    topDoubleBottom: "顶部和双底边界"
                }
            }
        },
        border: {
            title: "边框",
            rangeBorderLine: "线",
            rangeBorderColor: "颜色",
            noBorder: "无",
            outsideBorder: "外围框线",
            insideBorder: "内部框线",
            allBorder: "所有边框",
            leftBorder: "左侧边框",
            innerVertical: "垂直内框",
            rightBorder: "右侧边框",
            topBorder: "顶端边框",
            innerHorizontal: "内水平面",
            bottomBorder: "底框"
        },
        alignment: {
            title: "对齐方式",
            top: "顶部对其",
            middle: "居中对齐",
            bottom: "底部对其",
            left: "居左对齐",
            center: "垂直居中",
            right: "居右对齐",
            wrapText: "文字换行",
            decreaseIndent: "减少缩进",
            increaseIndent: "增加缩进"
        },
        format: {
            title: "格式化",
            commonFormat: {
                option: {
                    general: "一般",
                    number: "数字",
                    currency: "货币",
                    accounting: "会计",
                    shortDate: "短日期",
                    longDate: "长日期",
                    time: "时间",
                    percentage: "百分比",
                    fraction: "分数",
                    scientific: "科学",
                    text: "文本"
                }
            },
            percentValue: "0%",
            commaValue: " #,##0.00; (#,##0.00); \"-\"??;@",
            custom: "自定义",
            setButton: "Set"
        },
        merge: {
            title: "合并单元格",
            mergeCells: "合并",
            unmergeCells: "取消合并"
        },
        cellType: {
            title: "单元格类型"
        },
        conditionalFormat: {
            title: "条件格式",
            useConditionalFormats: "条件格式"
        },
        protection: {
            title: "保护",
            lock: "锁定",
            sheetIsProtected: "工作表已被保护",
            sheetIsUnprotected: "工作表未被保护"
        },
        findAndReplace: {
            title: "查找和替换",
            searchNextButton: "查找下一个",
            searchAllButton: "查找全部",
            replaceNextButton: "替换",
            replaceAllButton: "全部替换",
            searchContent: "查找内容",
            replaceContent: "替换内容",
            matchCcase: "区分大小写",
            matchExactly: "精确查找",
            useWildcards: "使用通配符",
            cellLabel: {
                title: "选项",
                within: "包含",
                withinOption: {
                    sheet: "工作表",
                    workbook: "工作簿"
                },
                lookin: "查找范围",
                lookinOption: {
                    values: "值",
                    formulas: "公式"
                },
                search: "搜索",
                searchOption: {
                    rows: "行",
                    columns: "列"
                },
                tableOption: {
                    col1: "工作表",
                    col2: "单元格",
                    col3: "值",
                    col4: "公式"
                }
            }
        }
    },
    tableTab: {
        tableStyle: {
            title: "Table Style",
            light: {
                light1: "light1",
                light2: "light2",
                light3: "light3",
                light7: "light7"
            },
            medium: {
                medium1: "medium1",
                medium2: "medium2",
                medium3: "medium3",
                medium7: "medium7"
            },
            dark: {
                dark1: "dark1",
                dark2: "dark2",
                dark3: "dark3",
                dark7: "dark7"
            }
        },
        general: {
            title: "General",
            tableName: "Name"
        },
        options: {
            title: "选项",
            filterButton: "过滤器 按钮",
            headerRow: "标题行",
            totalRow: "总计行",
            bandedRows: "带状行",
            bandedColumns: "带状列",
            firstColumn: "首列",
            lastColumn: "末列"
        }
    },
    dataTab: {
        sort: {
            title: "排序 & 过滤",
            asc: "排序 A-Z",
            desc: "排序 Z-A",
            filter: "过滤"
        },
        group: {
            title: "分组",
            group: "分组",
            ungroup: "取消分组",
            showDetail: "显示细节",
            hideDetail: "隐藏细节",
            showRowOutline: "显示行提纲",
            showColumnOutline: "显示列提纲"
        },
        dataValidation: {
            title: "数据有效性",
            setButton: "设置",
            clearAllButton: "清除所有",
            circleInvalidData: "圆框提示无效数据",
            setting: {
                title: "设置",
                values: {
                    validatorType: {
                        title: "验证种类",
                        option: {
                            anyValue: "任意值",
                            number: "数值",
                            list: "序列",
                            formulaList: "公式列表",
                            date: "日期",
                            textLength: "文本长度",
                            custom: "自定义"
                        }
                    },
                    ignoreBlank: "忽略空格",
                    validatorComparisonOperator: {
                        title: "操作",
                        option: {
                            between: "in",
                            notBetween: "not in",
                            equalTo: "=",
                            notEqualTo: "!=",
                            greaterThan: ">",
                            lessThan: "<",
                            greaterThanOrEqualTo: ">=",
                            lessThanOrEqualTo: "<="
                        }
                    },
                    number: {
                        minimum: "最小值",
                        maximum: "最大值",
                        value: "值",
                        isInteger: "是否整数"
                    },
                    source: "Source",
                    date: {
                        startDate: "开始日期",
                        endDate: "结束日期",
                        value: "值",
                        isTime: "是否时间"
                    },
                    formula: "公式"
                }
            },
            inputMessage: {
                title: "输入信息",
                values: {
                    showInputMessage: "选择单元格时显示",
                    title: "标题",
                    message: "信息"
                }
            },
            errorAlert: {
                title: "错误警告",
                values: {
                    showErrorAlert: "输入无效数据后显示",
                    alertType: {
                        title: "警告类型",
                        option: {
                            stop: "停止",
                            warning: "警告",
                            information: "信息"
                        }
                    },
                    title: "标题",
                    message: "信息"
                }
            }
        },
        cellTag: {
            title: "单元格标签",
            setButton: "设置标签",
            getButton: "获取标签",
            clearButton: "清除标签",
            searchButton: "查找标签",
            inputMessage: {
                title: "",
                values: {
                    title: "搜索标签包含:",
                    message: "单元格标签:"
                }
            }
        }
    },
    commentTab: {
        general: {
            title: "通用设置",
            dynamicSize: "动态大小",
            dynamicMove: "动态移动",
            lockText: "保护文本",
            showShadow: "显示阴影"
        },
        font: {
            title: "字体",
            fontFamily: "字体",
            fontSize: "大小",
            fontStyle: {
                title: "样式",
                values: {
                    normal: "标准",
                    italic: "文字斜体",
                    oblique: "文字倾斜",
                    inherit: "继承"
                }
            },
            fontWeight: {
                title: "Weight",
                values: {
                    normal: "normal",
                    bold: "bold",
                    bolder: "bolder",
                    lighter: "lighter"
                }
            },
            textDecoration: {
                title: "Decoration",
                values: {
                    none: "none",
                    underline: "underline",
                    overline: "overline",
                    linethrough: "linethrough"
                }
            }
        },
        border: {
            title: "Border",
            width: "Width",
            style: {
                title: "Style",
                values: {
                    none: "none",
                    hidden: "hidden",
                    dotted: "dotted",
                    dashed: "dashed",
                    solid: "solid",
                    double: "double",
                    groove: "groove",
                    ridge: "ridge",
                    inset: "inset",
                    outset: "outset"
                }
            },
            color: "Color"
        },
        appearance: {
            title: "Appearance",
            horizontalAlign: {
                title: "Horizontal",
                values: {
                    left: "left",
                    center: "center",
                    right: "right",
                    general: "general"
                }
            },
            displayMode: {
                title: "Display Mode",
                values: {
                    alwaysShown: "AlwaysShown",
                    hoverShown: "HoverShown"
                }
            },
            foreColor: "Forecolor",
            backColor: "Backcolor",
            padding: "Padding",
            zIndex: "Z-Index",
            opacity: "Opacity"
        }
    },
    pictureTab: {
        general: {
            title: "General",
            moveAndSize: "Move and size with cells",
            moveAndNoSize: "Move and don't size with cells",
            noMoveAndSize: "Don't move and size with cells",
            fixedPosition: "Fixed Position"
        },
        border: {
            title: "Border",
            width: "Width",
            radius: "Radius",
            style: {
                title: "Style",
                values: {
                    solid: "solid",
                    dotted: "dotted",
                    dashed: "dashed",
                    double: "double",
                    groove: "groove",
                    ridge: "ridge",
                    inset: "inset",
                    outset: "outset"
                }
            },
            color: "Color"
        },
        appearance: {
            title: "Appearance",
            stretch: {
                title: "Stretch",
                values: {
                    stretch: "Stretch",
                    center: "Center",
                    zoom: "Zoom",
                    none: "None"
                }
            },
            backColor: "Backcolor"
        }
    },
    sparklineExTab: {
        pieSparkline: {
            title: "PieSparkline Setting",
            values: {
                percentage: "Percentage",
                color: "Color ",
                setButton: "Set"
            }
        },
        areaSparkline: {
            title: "AreaSparkline Setting",
            values: {
                line1: "Line 1",
                line2: "Line 2",
                minimumValue: "Minimum Value",
                maximumValue: "Maximum Value",
                points: "Points",
                positiveColor: "Positive Color",
                negativeColor: "Negative Color",
                setButton: "Set"
            }
        },
        boxplotSparkline: {
            title: "BoxPlotSparkline Setting",
            values: {
                points: "Points",
                boxplotClass: "BoxPlotClass",
                scaleStart: "ScaleStart",
                scaleEnd: "ScaleEnd",
                acceptableStart: "AcceptableStart",
                acceptableEnd: "AcceptableEnd",
                colorScheme: "ColorScheme",
                style: "Style",
                showAverage: "Show Average",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        bulletSparkline: {
            title: "BulletSparkline Setting",
            values: {
                measure: "Measure",
                target: "Target",
                maxi: "Maxi",
                forecast: "Forecast",
                good: "Good",
                bad: "Bad",
                tickunit: "Tickunit",
                colorScheme: "ColorScheme",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        cascadeSparkline: {
            title: "CascadeSparkline Setting",
            values: {
                pointsRange: "PointsRange",
                pointIndex: "PointIndex",
                minimum: "Minimum",
                maximum: "Maximum",
                positiveColor: "ColorPositive",
                negativeColor: "ColorNegative",
                labelsRange: "LabelsRange",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        compatibleSparkline: {
            title: "CompatibleSparkline Setting",
            values: {
                data: {
                    title: "Data",
                    dataOrientation: "DataOrientation",
                    dateAxisData: "DateAxisData",
                    dateAxisOrientation: "DateAxisOrientation",
                    displayEmptyCellAs: "DisplayEmptyCellAs",
                    showDataInHiddenRowOrColumn: "Show data in hidden rows and columns"
                },
                show: {
                    title: "Show",
                    showFirst: "Show First",
                    showLast: "Show Last",
                    showHigh: "Show High",
                    showLow: "Show Low",
                    showNegative: "Show Negative",
                    showMarkers: "Show Markers"
                },
                group: {
                    title: "Group",
                    minAxisType: "MinAxisType",
                    maxAxisType: "MaxAxisType",
                    manualMin: "ManualMin",
                    manualMax: "ManualMax",
                    rightToLeft: "RightToLeft",
                    displayXAxis: "Display XAxis"
                },
                style: {
                    title: "Style",
                    negative: "Negative",
                    markers: "Markers",
                    axis: "Axis",
                    series: "Series",
                    highMarker: "High Marker",
                    lowMarker: "Low Marker",
                    firstMarker: "First Marker",
                    lastMarker: "Last Marker",
                    lineWeight: "Line Weight"
                },
                setButton: "Set"
            }
        },
        hbarSparkline: {
            title: "HbarSparkline Setting",
            values: {
                value: "Value",
                colorScheme: "ColorScheme",
                setButton: "Set"
            }
        },
        vbarSparkline: {
            title: "VarSparkline Setting",
            values: {
                value: "Value",
                colorScheme: "ColorScheme",
                setButton: "Set"
            }
        },
        paretoSparkline: {
            title: "ParetoSparkline Setting",
            values: {
                points: "Points",
                pointIndex: "PointIndex",
                colorRange: "ColorRange",
                highlightPosition: "HighlightPosition",
                target: "Target",
                target2: "Target2",
                label: "Label",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        pieSparkline: {
            title: "PieSparkline Setting",
            values: {
                percentage: "Percentage",
                color: "Color",
                setButton: "Set"
            }
        },
        scatterSparkline: {
            title: "ScatterSparkline Setting",
            values: {
                points1: "Points1",
                points2: "Points2",
                minX: "MinX",
                maxX: "MaxX",
                minY: "MinY",
                maxY: "MaxY",
                hLine: "HLine",
                vLine: "VLine",
                xMinZone: "XMinZone",
                xMaxZone: "XMaxZone",
                yMinZone: "YMinZone",
                yMaxZone: "YMaxZone",
                color1: "Color1",
                color2: "Color2",
                tags: "Tags",
                drawSymbol: "Draw Symbol",
                drawLines: "Draw Lines",
                dashLine: "Dash Line",
                setButton: "Set"
            }
        },
        spreadSparkline: {
            title: "SpreadSparkline Setting",
            values: {
                points: "Points",
                scaleStart: "ScaleStart",
                scaleEnd: "ScaleEnd",
                style: "Style",
                colorScheme: "ColorScheme",
                showAverage: "Show Average",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        stackedSparkline: {
            title: "StackedSparkline Setting",
            values: {
                points: "Points",
                colorRange: "ColorRange",
                labelRange: "LabelRange",
                maximum: "Maximum",
                targetRed: "TargetRed",
                targetGreen: "TargetGreen",
                targetBlue: "TargetBlue",
                targetYellow: "TargetYellow",
                color: "Color",
                highlightPosition: "HighlightPosition",
                textOrientation: "TextOrientation",
                textSize: "TextSize",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        variSparkline: {
            title: "VariSparkline Setting",
            values: {
                variance: "Variance",
                reference: "Reference",
                mini: "Mini",
                maxi: "Maxi",
                mark: "Mark",
                tickunit: "TickUnit",
                colorPositive: "ColorPositive",
                colorNegative: "ColorNegative",
                legend: "Legend",
                vertical: "Vertical",
                setButton: "Set"
            }
        },
        monthSparkline: {
            title: "MonthSparkline Setting"
        },
        yearSparkline: {
            title: "YearSparkline Setting"
        },
        monthYear: {
            data: "Data",
            month: "Month",
            year: "Year",
            emptyColor: "Empty Color",
            startColor: "Start Color",
            middleColor: "Middle Color",
            endColor: "End Color",
            colorRange: "Color Range",
            setButton: "set"
        },
        orientation: {
            vertical: "Vertical",
            horizontal: "Horizontal"
        },
        axisType: {
            individual: "Individual",
            custom: "Custom"
        },
        emptyCellDisplayType: {
            gaps: "Gaps",
            zero: "Zero",
            connect: "Connect"
        },
        boxplotClass: {
            fiveNS: "5NS",
            sevenNS: "7NS",
            tukey: "Tukey",
            bowley: "Bowley",
            sigma: "Sigma3"
        },
        boxplotStyle: {
            classical: "Classical",
            neo: "Neo"
        },
        paretoLabel: {
            none: "None",
            single: "Single",
            cumulated: "Cumulated"
        },
        spreadStyle: {
            stacked: "Stacked",
            spread: "Spread",
            jitter: "Jitter",
            poles: "Poles",
            stackedDots: "StackedDots",
            stripe: "Stripe"
        }
    },
    slicerTab: {
        slicerStyle: {
            title: "Slicer Style",
            light: {
                light1: "light1",
                light2: "light2",
                light3: "light3",
                light5: "light5",
                light6: "light6"
            },
            dark: {
                dark1: "dark1",
                dark2: "dark2",
                dark3: "dark3",
                dark5: "dark5",
                dark6: "dark6"
            }
        },
        general: {
            title: "General",
            name: "Name",
            captionName: "Caption Name",
            itemSorting: {
                title: "Item Sorting",
                option: {
                    none: "None",
                    ascending: "Ascending",
                    descending: "Descending"
                }
            },
            displayHeader: "Display Header"
        },
        layout: {
            title: "Layout",
            columnNumber: "Column Number",
            buttonHeight: "Button Height",
            buttonWidth: "Button Width"
        },
        property: {
            title: "Property",
            moveAndSize: "Move and size with cells",
            moveAndNoSize: "Move and don't size with cells",
            noMoveAndSize: "Don't move and size with cells",
            locked: "Locked"
        }
    },
    colorPicker: {
        themeColors: "主题色",
        standardColors: "标准色",
        noFills: "No Fills"
    },
    conditionalFormat: {
        setButton: "设置",
        ruleTypes: {
            title: "类型",
            highlightCells: {
                title: "突出显示单元格规则",
                values: {
                    cellValue: "单元格值",
                    specificText: "特定文本",
                    dateOccurring: "发生日期",
                    unique: "唯一",
                    duplicate: "重复"
                }
            },
            topBottom: {
                title: "顶部/底部规则",
                values: {
                    top10: "前10",
                    average: "平均"
                }
            },
            dataBars: {
                title: "数据栏",
                labels: {
                    minimum: "最小值",
                    maximum: "最大值",
                    type: "类型",
                    value: "值",
                    appearance: "外观",
                    showBarOnly: "仅显示栏",
                    useGradient: "使用渐变",
                    showBorder: "显示边框",
                    barDirection: "条形方向",
                    negativeFillColor: "底色",
                    negativeBorderColor: "负边框",
                    axis: "轴",
                    axisPosition: "位置",
                    axisColor: "颜色"
                },
                valueTypes: {
                    number: "数字",
                    lowestValue: "最小值",
                    highestValue: "最大值",
                    percent: "百分比",
                    percentile: "百分位数",
                    automin: "自动取最小值",
                    automax: "自动取最大值",
                    formula: "公式"
                },
                directions: {
                    leftToRight: "从左到右",
                    rightToLeft: "从右到左"
                },
                axisPositions: {
                    automatic: "自动",
                    cellMidPoint: "单元格中心点",
                    none: "无"
                }
            },
            colorScales: {
                title: "色标",
                labels: {
                    minimum: "最小值",
                    midpoint: "中心点",
                    maximum: "最大值",
                    type: "类型",
                    value: "值",
                    color: "颜色"
                },
                values: {
                    twoColors: "2色标",
                    threeColors: "3色标"
                },
                valueTypes: {
                    number: "数字",
                    lowestValue: "最小值",
                    highestValue: "最大值",
                    percent: "百分比",
                    percentile: "百分位数",
                    formula: "公式"
                }
            },
            iconSets: {
                title: "图标集",
                labels: {
                    style: "样式",
                    showIconOnly: "仅显示图标",
                    reverseIconOrder: "反向图标顺序",

                },
                types: {
                    threeArrowsColored: "三箭头着色",
                    threeArrowsGray: "三箭头灰色",
                    threeTriangles: "三三角",
                    threeStars: "三星",
                    threeFlags: "三旗",
                    threeTrafficLightsUnrimmed: "未修饰的红绿灯",
                    threeTrafficLightsRimmed: "有边框的交通灯",
                    threeSigns: "三标志",
                    threeSymbolsCircled: "带圆圈的三符号",
                    threeSymbolsUncircled: "三符号盘旋",
                    fourArrowsColored: "四箭头着色",
                    fourArrowsGray: "四箭头灰色",
                    fourRedToBlack: "四红黑",
                    fourRatings: "四星级",
                    fourTrafficLights: "四交通灯",
                    fiveArrowsColored: "五箭头着色",
                    fiveArrowsGray: "五箭头灰色",
                    fiveRatings: "五星",
                    fiveQuarters: "五季度",
                    fiveBoxes: "五盒"
                },
                valueTypes: {
                    number: "数字",
                    percent: "百分比",
                    percentile: "百分位数",
                    formula: "公式"
                }
            },
            removeConditionalFormat: {
                title: "无"
            }
        },
        operators: {
            cellValue: {
                types: {
                    equalsTo: "等于",
                    notEqualsTo: "不等于",
                    greaterThan: "大于",
                    greaterThanOrEqualsTo: "大于或等于",
                    lessThan: "小于",
                    lessThanOrEqualsTo: "小于或等于",
                    between: "介于",
                    notBetween: "不介于"
                }
            },
            specificText: {
                types: {
                    contains: "包含",
                    doesNotContain: "不包含",
                    beginsWith: "开始于",
                    endsWith: "结束于"
                }
            },
            dateOccurring: {
                types: {
                    today: "今天",
                    yesterday: "昨天",
                    tomorrow: "明天",
                    last7Days: "过去7天",
                    thisMonth: "本月",
                    lastMonth: "上月",
                    nextMonth: "下月",
                    thisWeek: "本周",
                    lastWeek: "上周",
                    nextWeek: "下周"
                }
            },
            top10: {
                types: {
                    top: "前",
                    bottom: "后"
                }
            },
            average: {
                types: {
                    above: "大于",
                    below: "小于",
                    equalOrAbove: "大于等于",
                    equalOrBelow: "小于等于",
                    above1StdDev: "大于1偏差值",
                    below1StdDev: "小于1偏差值",
                    above2StdDev: "大于2偏差值",
                    below2StdDev: "小于2偏差值",
                    above3StdDev: "大于3偏差值",
                    below3StdDev: "小于3偏差值"
                }
            }
        },
        texts: {
            cells: "仅格式化单元格:",
            rankIn: "格式化排名的值:",
            inRange: "所选范围内的值.",
            values: "格式化值:",
            average: "所选范围的平均值.",
            allValuesBased: "根据值设置所有单元格:",
            all: "全部格式化:",
            and: "和",
            formatStyle: "使用样式",
            showIconWithRules: "根据这些规则显示每个图标:"
        },
        formatSetting: {
            formatUseBackColor: "背景色",
            formatUseForeColor: "前景色",
            formatUseBorder: "边框"
        }
    },
    cellTypes: {
        title: "单元格类型",
        buttonCellType: {
            title: "按钮单元格",
            values: {
                marginTop: "顶部边距",
                marginRight: "右边距",
                marginBottom: "底部边距",
                marginLeft: "左边距",
                text: "文本",
                backColor: "背景色"
            }
        },
        checkBoxCellType: {
            title: "复选框单元格",
            values: {
                caption: "标题",
                textTrue: "选中",
                textIndeterminate: "不确定",
                textFalse: "未选中",
                textAlign: {
                    title: "对齐方式",
                    values: {
                        top: "顶部对齐",
                        bottom: "底部对齐",
                        left: "左对齐",
                        right: "右对齐"
                    }
                },
                isThreeState: "支持三种状态"
            }
        },
        comboBoxCellType: {
            title: "组合框单元格",
            values: {
                editorValueType: {
                    title: "编辑器值类型",
                    values: {
                        text: "文本",
                        index: "下标",
                        value: "数值"
                    }
                },
                itemsText: "项目文本",
                itemsValue: "项目值"
            }
        },
        hyperlinkCellType: {
            title: "超链接单元格",
            values: {
                linkColor: "链接颜色",
                visitedLinkColor: "访问过的链接颜色",
                text: "文本",
                linkToolTip: "链接工具提示"
            }
        },
        clearCellType: {
            title: "无"
        },
        setButton: "设置"
    },
    sparklineDialog: {
        title: "SparklineEx Setting",
        sparklineExType: {
            title: "Type",
            values: {
                line: "Line",
                column: "Column",
                winLoss: "Win/Loss",
                pie: "Pie",
                area: "Area",
                scatter: "Scatter",
                spread: "Spread",
                stacked: "Stacked",
                bullet: "Bullet",
                hbar: "Hbar",
                vbar: "Vbar",
                variance: "Variance",
                boxplot: "BoxPlot",
                cascade: "Cascade",
                pareto: "Pareto",
                month: "Month",
                year: "Year"
            }
        },
        lineSparkline: {
            dataRange: "Data Range",
            locationRange: "Location Range",
            dataRangeError: "Data range is invalid!",
            singleDataRange: "Data range should be in a single row or column.",
            locationRangeError: "Location range is invalid!"
        },
        bulletSparkline: {
            measure: "Measure",
            target: "Target",
            maxi: "Maxi",
            forecast: "Forecast",
            good: "Good",
            bad: "Bad",
            tickunit: "Tickunit",
            colorScheme: "ColorScheme",
            vertical: "Vertical"
        },
        hbarSparkline: {
            value: "Value",
            colorScheme: "ColorScheme"
        },
        varianceSparkline: {
            variance: "Variance",
            reference: "Reference",
            mini: "Mini",
            maxi: "Maxi",
            mark: "Mark",
            tickunit: "TickUnit",
            colorPositive: "ColorPositive",
            colorNegative: "ColorNegative",
            legend: "Legend",
            vertical: "Vertical"
        },
        monthSparkline: {
            year: "Year",
            month: "Month",
            emptyColor: "Empty Color",
            startColor: "Start Color",
            middleColor: "Middle Color",
            endColor: "End Color",
            colorRange: "Color Range"
        },
        yearSparkline: {
            year: "Year",
            emptyColor: "Empty Color",
            startColor: "Start Color",
            middleColor: "Middle Color",
            endColor: "End Color",
            colorRange: "Color Range"
        }
    },
    slicerDialog: {
        insertSlicer: "Insert Slicer"
    },
    passwordDialog: {
        title: "Password",
        error: "Incorrect Password!"
    },
    tooltips: {
        style: {
            fontBold: "粗体",
            fontItalic: "斜体",
            fontUnderline: "下划线",
            fontOverline: "上划线",
            fontLinethrough: "删除线"
        },
        alignment: {
            leftAlign: "向左对齐文本",
            centerAlign: "文本居中",
            rightAlign: "向右对齐文本",
            topAlign: "将文本与顶部对齐",
            middleAlign: "将文本对齐到中间",
            bottomAlign: "将文本与底部对齐",
            decreaseIndent: "减少缩进级别",
            increaseIndent: "增加缩进级别"
        },
        border: {
            outsideBorder: "外部边框",
            insideBorder: "内部边框",
            allBorder: "所有边框",
            leftBorder: "左边框",
            innerVertical: "内部垂直",
            rightBorder: "右边框",
            topBorder: "顶部边框",
            innerHorizontal: "内部水平",
            bottomBorder: "底部边框"
        },
        format: {
            percentStyle: "百分比样式",
            commaStyle: "逗号样式",
            increaseDecimal: "增加小数",
            decreaseDecimal: "减少小数"
        }
    },
    defaultTexts: {
        buttonText: "Button",
        checkCaption: "Check",
        comboText: "United States,China,Japan",
        comboValue: "US,CN,JP",
        hyperlinkText: "LinkText",
        hyperlinkToolTip: "Hyperlink Tooltip"
    },
    messages: {
        invalidImportFile: "Invalid file, import failed.",
        duplicatedSheetName: "Duplicated sheet name.",
        duplicatedTableName: "Duplicated table name.",
        rowColumnRangeRequired: "请选择行或列的范围.",
        imageFileRequired: "The file must be image!",
        duplicatedSlicerName: "Duplicated slicer name.",
        invalidSlicerName: "Slicer name is not valid."
    },
    contextMenu: {
        cutItem: "剪切",
        copyItem: "复制",
        pasteItem: "粘贴",
        deleteContentItem: "清除内容",
        insertItem: "插入",
        deleteItem: "删除",
        hideItem: "隐藏",
        displayItem: "取消隐藏",
        mergeItem: "合并",
        unmergeItem: "取消合并",
        InsertMarkItem:"插入批注",
        EditMarkItem:"编辑批注",
        DeleteMarkItem:"删除批注"
    },
    dialog: {
        ok: "确定",
        cancel: "取消"
    }
};

