﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Common;
using System.Text;
using RMYH.DBUtility;
using System.Reflection;

public partial class TesLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(TesLogin));
        Session.Clear();
        HttpCookie cookie = Request.Cookies["RMYH_LOGIN"];
        if (!IsPostBack)
        {
            if (cookie != null)
            {
                txtuid.Value = cookie.Values.Get("USER_LOGIN");
                hidyy.Value = cookie.Values.Get("YY");
                TxtDataBase.Value = cookie.Values.Get("DataBase");
            }
        }
    }
    protected void Text1_ServerClick(object sender, EventArgs e)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();    
            HttpCookie cookie = new HttpCookie("RMYH_LOGIN");
            cookie.Expires = DateTime.Now.AddYears(1);
            cookie.Values.Add("DataBase",TxtDataBase.Value);
            //Request.Cookies.Add(cookie);
            Response.Cookies.Add(cookie);
            string rtn = bll.toLogin(txtuid.Value, txtpwd.Value, hidyy.Value);
            if (rtn == "true")
            {
                string year = DateTime.Now.Year.ToString();
                string sql = "select * from sysobjects where name='BB_CZRZ_" + year + "'";
                DataSet set = bll.Query(sql);
                if (set.Tables[0].Rows.Count > 0)
                {
                    string time = DateTime.Now.ToString();
                    string czr = Session["USERDM"].ToString();
                    sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('登录成功','" + time + "','" + czr + "')";
                    int count=bll.ExecuteSql(sql);
                }
                else
                {
                    sql = "create table BB_CZRZ_" + year + "( ";
                    sql += "DM NUMERIC(10,0) IDENTITY,";
                    sql += "RZMC NCHAR(100), ";
                    sql += "MX NCHAR(100), ";
                    sql += "DATE NCHAR(100), ";
                    sql += "CZR NCHAR(100) ";
                    sql += ")";
                    int count = bll.ExecuteSql(sql);
                    if (count == -1)
                    {
                        string time = DateTime.Now.ToString();
                        string czr = Session["USERDM"].ToString();
                        sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('登录成功','" + time + "','" + czr + "')";
                        bll.ExecuteSql(sql);
                    }
                }
                cookie.Values.Add("USER_LOGIN", txtuid.Value);
                cookie.Values.Add("YY", hidyy.Value);
                Response.AppendCookie(cookie);
                Random rad = new Random();
                //MessageBox.Show(this,"登陆成功！");
                Response.Redirect("Index.aspx?rnd=" + rad.Next(), false);
            }
            else
            {
                MessageBox.Show(this, rtn);
            }
        }
        catch (Exception ee)
        {

            MessageBox.Show(this,ee.Message);
        }
    }
    #region
    /// <summary>
    /// 绑定大类
    /// </summary>
    [AjaxPro.AjaxMethod]
    public string GetDDLYear()
    {
        int Year=DateTime.Now.Year-5;
        string Str = "";
        for (int i = Year; i < Year+10; i++)
        {
            Str += i.ToString() + "," + i.ToString();
            if (i < Year+9)
            {
                Str += "+";
            }
        }
        return Str;
    }
    #endregion
}
