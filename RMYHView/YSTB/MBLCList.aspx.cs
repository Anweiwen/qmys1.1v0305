﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;

public partial class YSTB_MBLCList : System.Web.UI.Page
{
    protected string USERDM = "";
    protected string HSZXDM = "";
    protected string USERNAME = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_MBLCList));
        USERDM = HttpContext.Current.Session["USERDM"].ToString();
        HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        USERNAME = HttpContext.Current.Session["USERNAME"].ToString();
    }
    #region Ajax方法
    /// <summary>
    /// 获取方案类别相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ' ORDER BY XMDH_A");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["XMDH_A"].ToString() == "3" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取年份相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYY()
    {
        return HttpContext.Current.Session["YY"].ToString();
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetData(string FALX)
    {
        string Str = "";
        if (FALX == "2")
        {
            Str += "<option value='1'>第一季度</option>";
            Str += "<option value='2'>第二季度</option>";
            Str += "<option value='3'>第三季度</option>";
            Str += "<option value='4'>第四季度</option>";
        }
        else if (FALX == "3")
        {
            Str = "-1";
        }
        else
        {
            string MM = DateTime.Now.ToString("yyyy-MM").Split('-')[1].ToString();
            //月方案和其他方案
            for (int i = 1; i <= 12; i++)
            {
                if (i < 10)
                {
                    //默认选中当前月份
                    if (("0" + i.ToString()) == MM)
                    {
                        Str += "<option selected=true value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
                else
                {
                    if (i.ToString() == MM)
                    {
                        Str += "<option selected=true value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
            }
        }
        return Str;
    }
    /// <summary>
    /// 输入月份获取月份的中文名称
    /// </summary>
    /// <param name="MM">月份</param>
    /// <returns></returns>
    public string GetMonth(string MM)
    {
        string M = "";
        switch (MM)
        {
            case "1": M = "一月"; break;
            case "2": M = "二月"; break;
            case "3": M = "三月"; break;
            case "4": M = "四月"; break;
            case "5": M = "五月"; break;
            case "6": M = "六月"; break;
            case "7": M = "七月"; break;
            case "8": M = "八月"; break;
            case "9": M = "九月"; break;
            case "10": M = "十月"; break;
            case "11": M = "十一月"; break;
            case "12": M = "十二月"; break;
        }
        return M;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHFA(string FALX, string YY, string MM)
    {
        string Str = "", SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //选择月方案类型
        if (FALX == "1")
        {
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='1' AND SCBS=1";
        }
        else if (FALX == "2")
        {
            //选择季度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND JD='" + MM + "' AND FABS='2' AND SCBS=1";
        }
        else if (FALX == "3")
        {
            //选择年度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND FABS='3' AND SCBS=1";
        }
        else
        {
            //选择其他方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='4' AND SCBS=1";
        }
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            Str += "<option value='" + DS.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + DS.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
        }
        return Str;
    }
    /// <summary>
    /// 初始化部门
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitZY()
    {
        string res = "<option value='0' selected='selected'>全部</option>";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        ds = bll.Query("SELECT DISTINCT D.ZYDM,D.ZYMC FROM TB_YSBBMB C,TB_JHZYML D WHERE C.ZYDM=D.ZYDM");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";

            }
        }
        return res;
    }
    /// <summary>
    /// 获取当前系统的月份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYSYF(string YY)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string NN = BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='" + YY + "'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="MBDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBInfo(string MBDM)
    {
        string MBStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.getDataSet("TB_YSBBMB", "MBWJ,MBLX", "MBDM='" + MBDM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBStr = DS.Tables[0].Rows[0]["MBWJ"].ToString() + "@" + DS.Tables[0].Rows[0]["MBLX"].ToString();
        }
        return MBStr;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="MBDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFAInfo(string JHFADM)
    {
        string MBStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.getDataSet("TB_JHFA", "FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBStr = DS.Tables[0].Rows[0]["FABS"].ToString();
        }
        return MBStr;
    }
    /// <summary>
    /// 获取模板图片信息
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <returns>(返回流程执行情况（红色代表待处理、黄色代表将要执行、绿色代表已处理）)</returns>
    [AjaxPro.AjaxMethod]
    public string GetLCImage(string CJBM,string MBDM,string JHFADM)
    {
        int t = 0, BZ = 1;
        DataSet DS = new DataSet();
        DataSet DT = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string MBStr = "", MBZQ = "", MBLX = "", CJ = "";
        DS = BLL.Query("SELECT CJBM,JSDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' ORDER BY CJBM DESC");
        //判断当前模板有没有待审批的记录
        DT = BLL.Query("SELECT CJBM,JSDM FROM TB_MBLCSB WHERE DQMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND STATE=-1 AND JHFADM='" + JHFADM + "'");
        if (DT.Tables[0].Rows.Count > 0)
        {
            CJBM = DT.Tables[0].Rows[0]["CJBM"].ToString();
        }
        else
        {
            
            DT.Tables.Clear();
            //当前模板没有待审批记录的情况下，看看有没有已审批的记录，已审批的记录取李当前最近的(表示已经执行完成)
            DT = BLL.Query("SELECT CJBM,JSDM FROM TB_MBLCSB WHERE DQMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND STATE=1 AND JHFADM='" + JHFADM + "' AND CLSJ=(SELECT MAX(CLSJ) FROM TB_MBLCSB WHERE DQMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND STATE=1 AND JHFADM='"+JHFADM+"') ORDER BY CJBM ASC");
            if (DT.Tables[0].Rows.Count > 0)
            {
                DataSet D = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
                MBZQ = D.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = D.Tables[0].Rows[0]["YSBS"].ToString();
                CJ=BLL.GetDCLAndParCJBM(JHFADM, "", MBZQ, MBLX);
                //如果为空，说明流程已经走完
                if (CJ != "")
                {
                    CJ = "'" + CJ + "'";
                    if (CJ.IndexOf("'" + CJBM + "'") > -1)
                    {
                        BZ = 0;
                    }
                }
                CJBM = "";
            }
            else
            {
                DT.Tables.Clear();
                DT = BLL.Query("SELECT CJBM,JSDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND LX='0' ORDER BY CJBM DESC");
                if (DT.Tables[0].Rows.Count > 0)
                {
                    CJBM = DT.Tables[0].Rows[0]["CJBM"].ToString();
                }
                else
                {
                    BZ = 0;
                }
            }
        }
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (i < DS.Tables[0].Rows.Count - 1)
            {
                if (i == 0)
                {
                    if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ == 1)
                    {
                        MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    else
                    {
                        if (BZ == 0)
                        {
                            MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                        }
                        else
                        {
                            if (CJBM == "")
                            {
                                MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                            else if (CJBM.Trim().Length > DS.Tables[0].Rows[i]["CJBM"].ToString().Trim().Length)
                            {
                                MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                            else
                            {
                                MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                        }
                    }
                    t++;
                }
                else
                {
                    if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ == 1)
                    {
                        MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    else
                    {
                        if (BZ == 0)
                        {
                            MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                        }
                        else
                        {
                            if (CJBM == "")
                            {
                                MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                            else if (CJBM.Trim().Length > DS.Tables[0].Rows[i]["CJBM"].ToString().Trim().Length)
                            {
                                MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                            else
                            {
                                MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                            }
                        }
                    }
                    t++;
                }
            }
            else
            {
                if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ == 1)
                {
                    MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" />";
                }
                else
                {
                    if (BZ == 0)
                    {
                        MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" />";
                    }
                    else
                    {
                        if (CJBM == "")
                        {
                            MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" />";
                        }
                        else if (CJBM.Trim().Length > DS.Tables[0].Rows[i]["CJBM"].ToString().Trim().Length)
                        {
                            MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:yellow;width: 180px; height: 40px\" />";
                        }
                        else
                        {
                            MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:green;width: 180px; height: 40px\" />";
                        }
                    }
                }
                t++;
            }
        }
        return MBStr + "&nbsp;&nbsp;&nbsp;<span style=\"color:red;font-size:13px\">红色：待处理、黄色：将要执行、绿色：已处理</span>@" + t.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
    #endregion
}