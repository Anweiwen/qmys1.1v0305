﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MBLCList.aspx.cs" Inherits="YSTB_MBLCList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/demo.css"/>
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet"/>
    <link href="../CSS/style.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var cWidth = document.documentElement.clientWidth;
        var cHeight = document.documentElement.clientHeight;

        function Init() {
            ClearMb();
            document.getElementById("DivShow").innerHTML = "";
            $.ajax({
                type: 'get',
                url: 'MBLCListHandler.ashx',
                data: {
                    JHFADM: $("#SelJHFA").val(),
                    MBMC: $("#TxtMB").val(),
                    ZYDM: $("#SelZY").val(),
                    HSZXDM: '<%= HSZXDM %>',
                    ZT: $("#<%= DDLZT.ClientID %>").val()
                },
                async: true,
                cache: false,
                success: function(result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function() {
                    alert("加载失败!");
                }
            });
        }

        function InitDataGrid() {
            $('#TabMB').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                loadMsg: '正在加载数据.....',
                pageSize: 15,
                pageList: [10, 15, 20, 25, 30, 35, 40],
                onDblClickRow: function(rowIndex, rowData) {
                    var MM = "", JD = "", WJ = "", ML = "", LB = "", Arr;
                    var USERDM = '<%= USERDM %>';
                    var USERNAME = '<%= USERNAME %>';
                    var HSZXDM = '<%= HSZXDM %>';
                    var YS = YSTB_MBLCList.GetYSYF().value;
                    var MB = YSTB_MBLCList.GetMBInfo(rowData.MBDM).value;
                    if (MB != "") {
                        Arr = MB.split('@');
                        WJ = Arr[0].toString();
                        ML = Arr[1].toString();
                    }
                    LB = YSTB_MBLCList.GetFAInfo($("#SelJHFA").val()).value;
                    if ($("#SelFALX").val() != "3") {
                        if ($("#SelFALX").val() == "2") {
                            JD = $("#SelData").val();
                        } else {
                            MM = $("#SelData").val();
                        }
                    }
                    OpenMb(rowData.MBDM,
                        rowData.MBMC,
                        WJ,
                        $("#TxtYY").val(),
                        MM,
                        JD,
                        $("#SelJHFA").val(),
                        $("#SelJHFA")[0][$("#SelJHFA")[0].selectedIndex].text,
                        YS,
                        USERDM,
                        USERNAME,
                        "-1",
                        LB,
                        ML,
                        HSZXDM,
                        "1");
                },
                onClickRow: function(rowIndex, rowData) {
                    var rtn = YSTB_MBLCList.GetLCImage(rowData.CJBM, rowData.MBDM, $("#SelJHFA").val()).value;
                    var str = rtn.split('@');
                    var x = $(window).width() / 2 - (parseInt(str[1].toString()) * 280 - 100) / 2;
                    $("#DivShow").css("top", "5px");
                    $("#DivShow").css("left", (x > 0 ? x : 0) + "px");
                    document.getElementById("DivShow").innerHTML = str[0].toString();
                },
                columns: [
                    [
                        { field: 'DQJSDM', title: '当前角色代码', hidden: true },
                        { field: 'DQJSNAME', title: '当前环节', width: 80 },
                        { field: 'MBDM', title: '模版代码', hidden: true },
                        { field: 'MBMC', title: '模版名称', width: 120 },
                        { field: 'MBWJ', title: '模版文件', width: 120, hidden: true },
                        { field: 'ZYMC', title: '使用部门', width: 80 },
                        { field: 'STATE', title: '模板状态', width: 60 },
                        { field: 'NEXTJSDM', title: '后续模版代码', hidden: true },
                        { field: 'NEXTMBMC', title: '后续模版名称', width: 120 },
                        { field: 'NEXTJSNAME', title: '后续环节', width: 80, align: 'center' },
                        { field: 'CJBM', title: '层及编码', hidden: true }
                    ]
                ]
            });
        }

        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') { // is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function(pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh',
                        {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }

        function BindFALB() {
            var rtn = YSTB_MBLCList.GetFALB().value;
            $("#SelFALX").html(rtn);
        }

        function BindYY() {
            var rtn = YSTB_MBLCList.GetYY().value;
            $("#TxtYY").val(rtn);
        }

        function BindData() {
            var rtn = YSTB_MBLCList.GetData($("#SelFALX").val()).value;
            if (rtn == "-1") {
                $("#<%= LabMM.ClientID %>").attr("style", "display:none");
                $("#SelData").attr("style", "display:none");
            } else {
                if ($("#SelFALX").val() == "2") {
                    $("#<%= LabMM.ClientID %>").html("季度：");
                } else {
                    $("#<%= LabMM.ClientID %>").html("月份：");
                }
                $("#<%= LabMM.ClientID %>").attr("style", "display:display");
                $("#SelData").attr("style", "display:display");
                document.getElementById("SelData").options.length = 0
                $("#SelData").html(rtn);
            }

        }

        function BindJHFA() {
            var MM = "";
            if ($("#SelFALX").val() != "3") {
                MM = $("#SelData").val();
            }
            var rtn = YSTB_MBLCList.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM).value;
            $("#SelJHFA").html(rtn);
        }

        //初始化部门
        function InitZY() {
            var rtn = YSTB_MBLCList.InitZY().value;
            $("#SelZY").html(rtn);
        }

        function ChageFALX() {
            BindData();
            BindJHFA();
        }

        function GetControlsValue() {
            return $("#SelFALX").val() +
                "@" +
                $("#TxtYY").val() +
                "@" +
                $("#SelData").val() +
                "@" +
                $("#SelJHFA").val() +
                "@" +
                $("#SelZY").val() +
                "@" +
                $("#TxtMB").val();
        }

        function SetControlsValue(OBJ) {
            var Arr = OBJ.split('@');
            if ($("#SelFALX").val() == Arr[0] &&
                $("#TxtYY").val() == Arr[1] &&
                "" + $("#SelData").val() + "" == Arr[2] &&
                $("#SelJHFA").val() == Arr[3] &&
                $("#SelZY").val() == Arr[4] &&
                $("#TxtMB").val() == Arr[5]) {
                return;
            } else {
                $("#SelFALX").val(Arr[0]);
                ChageFALX();
                $("#TxtYY").val(Arr[1]);
                $("#SelData").val(Arr[2]);
                BindJHFA();
                $("#SelJHFA").val(Arr[3]);
                $("#SelZY").val(Arr[4]);
                $("#TxtMB").textbox("setValue", Arr[5]);
                document.getElementById("DivShow").innerHTML = "";
                $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
                InitDataGrid();
                Init();
            }
        }

        function OpenMb(MBDM,
            MBMC,
            MBWJ,
            YY,
            MM,
            JD,
            JHFADM,
            JHFANAME,
            YSYF,
            USERDM,
            USERNAME,
            ISDone,
            FALB,
            ML,
            HSZXDM,
            SFZLC) {
//            var url = "pageoffice://|" + geturlpath() + "../Excels/Data/MB.aspx?mbmc=" + getchineseurl(MBMC)
//                 + "&mbdm=" + MBDM + "&mbwj=" + getchineseurl(MBWJ) + "&yy=" + YY
//                 + "&nn=" + MM + "&jd=" + JD
//                 + "&fadm=" + JHFADM + "&fadm2="
//                 + "&famc=" + getchineseurl(JHFANAME)
//                 + "&ysyf=" + YSYF + "&userdm=" + USERDM
//                 + "&username=" + getchineseurl(USERNAME) + "&falb=" + FALB
//                 + "&mblb=queryflow&isexe=" + ISDone + "&mblx=" + ML
//                 + "&hszxdm=" + HSZXDM
            //                 + "&sfzlc=" + SFZLC;
            var url = "MBMC=" +
                getchineseurl(MBMC) +
                "&MBDM=" +
                MBDM +
                "&MBWJ=" +
                getchineseurl(MBWJ) +
                "&YY=" +
                YY +
                "&NN=" +
                MM +
                "&JD=" +
                JD +
                "&FADM=" +
                JHFADM +
                "&FADM2=" +
                "&FAMC=" +
                getchineseurl(JHFANAME) +
                "&YSYF=" +
                YSYF +
                "&USERDM=" +
                USERDM +
                "&USERNAME=" +
                getchineseurl(USERNAME) +
                "&FALB=" +
                FALB +
                "&MBLB=queryflow&ISEXE=" +
                ISDone +
                "&MBLX=" +
                ML +
                "&HSZXDM=" +
                HSZXDM +
                "&SFZLC=" +
                SFZLC;

            //如果不是IE浏览器
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            //window.location.href = url + "|||";
            var selected = $("#TabMB").datagrid('getSelected');
            OpenWebExcel(selected.MBDM, selected.MBMC, url, $("#SelFa").val(), $("#SelFa2").val());
        }

        //打开SpreadJS网页
        function OpenWebExcel(ID, MC, paramStr, FADM, FADM2) {
            parent.$("#HidMBDM").val(ID);
            if (parent.$('#tt').tabs('exists', MC)) {
                parent.$('#tt').tabs('select', MC);
                //如果Title为空，表示打开的是一般的菜单页，否则打开的是数据填报页
                if (FADM != "" && FADM != undefined && FADM != null) {
                    var NewFileName = "Jhfadm~" + FADM + "~Jhfadm2~" + FADM2 + "~Mb~" + ID;
                    var TabFrame = $('#tt').tabs('getTab', MC).find("iframe");
                    if (TabFrame.length > 0) {
                        var OldFileName = TabFrame.contents().find("#HidFAMB").val();
                        if (NewFileName != OldFileName) {
                            var path = window.document.location.pathname.substring(1);
                            var Len = path.indexOf("/");
                            var root = "/" + path.substring(0, Len + 1);
                            root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random();
                            var content = '<iframe scrolling="auto" id="' +
                                ID +
                                '"  frameborder="0"  src="' +
                                root +
                                '" style="width:100%;height:100%;"></iframe>';
                            var tab = $('#tt').tabs('getTab', MC);
                            $('#tt').tabs('update',
                                {
                                    tab: tab,
                                    options: {
                                        content: content
                                    }
                                });
                        }
                    }
                }
            } else {
                var path = window.document.location.pathname.substring(1);
                var Len = path.indexOf("/");
                var root = "/" + path.substring(0, Len + 1);
                root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random();;
                var content = '<iframe scrolling="auto" id="' +
                    ID +
                    '"  frameborder="0"  src="' +
                    root +
                    '" style="width:100%;height:100%;"></iframe>';
                parent.$('#tt').tabs('add',
                    {
                        title: MC,
                        content: content,
                        closable: true
                    });
            }
        }

        function initDownLoadMb() {
            //批量下载的datagrid，支持多选
            $('#DownLoadMb').datagrid({
                idField: 'MBDM',
                checkOnSelect: true,
                frozenColumns: [
                    [
                        {
                            field: 'ck',
                            checkbox: true
                        }
                    ]
                ],
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                singleSelect: false,
                striped: true,
                toolbar: [
                    {}, '-', {
                        text: '批量下载',
                        iconCls: 'icon-add',
                        handler: function() {
                            DownLoadMbs();
                        }
                    }, '-', {
                        text: '取消',
                        iconCls: 'icon-undo',
                        handler: function() {
                            $('#DownLoadMb').datagrid('unselectAll');
                        }
                    }
                ],
                loadMsg: '正在加载数据.....',
                columns: [
                    [
                        { field: 'MBDM', title: '模板代码', width: 50, align: 'center', hidden: true },
                        { field: 'MBMC', title: '模板名称', width: 120, align: 'left' },
                        { field: 'MBWJ', title: '模版文件', width: 70, align: 'center' },
                        { field: 'ZYMC', title: '作业名称', width: 70, align: 'center' },
                        { field: 'DQJSDM', title: '角色代码', width: 70, align: 'center', hidden: true },
                        { field: 'DQJSNAME', title: '角色名称', width: 70, align: 'center', hidden: true },
                        { field: 'STATE', title: '状态', width: 70, align: 'center', hidden: true },
                        {
                            field: 'SFCZ',
                            title: '是否存在',
                            width: 60,
                            align: 'center',
                            formatter: function(value, row, index) {
                                if (value == "模板不存在") {
                                    return "<font color='red'>" + value + "</font>";
                                } else {
                                    return value;
                                }
                            },
                            styler: function(value, row, index) {
                                if (value == "模板不存在") {
                                    return 'background-color:yellow;';
                                }
                            }
                        }
                    ]
                ],
                onHeaderContextMenu: function(e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show',
                        {
                            left: e.pageX,
                            top: e.pageY
                        });
                }
            });

        }


        //by liguosheng
        function getDownLoad() {
            //datagridloading($("#DownLoadMb"));
            $('#DownLoadMb').datagrid('unselectAll');
            $.ajax({
                type: 'get',
                url: 'MBLCListHandler.ashx',
                data: {
                    action: "GetDownLoadsList",
                    jhfadm: $("#SelJHFA").val(),
                    mbmc: $("#TxtMB").val(),
                    zydm: $("#SelZY").val(),
                    hszxdm: "<%= HSZXDM %>",
                    state: $("#DDLZT").val()
                },
                async: true,
                cache: false,
                success: function(result) {
                    if (result) {
                        $("#DownLoadMb").datagrid('loadData', eval("(" + result + ")"));
                    }
                    $('#DownDialog').dialog({
                        top: (cHeight - 380) / 2,
                        left: (cWidth - 600) / 2
                    })
                    $('#DownDialog').window('open');
                },
                error: function(req) {
                    //datagridloaded($("#DownLoadMb"));
                    BmShowWinError('装入模版错误!', req.responseText);
                }
            });

        }

        //by liguosheng
        function DownLoadMbs() {
            var files = '';
            var names = '';
            var zipName = '模板批量下载.zip';
            var rows = $('#DownLoadMb').datagrid('getSelections');
            if (rows.length == 0) {
                prompt("请选择模板文件！");
                return;
            }
            for (var i = 0; i < rows.length; i++) {
                files += rows[i].MBWJ + "," + rows[i].MBMC;
                if (i < rows.length - 1) {
                    files += '|';
                }
            }

            $('#jhfadm').attr('value', $("#SelJHFA").val());
            $('#excelFiles').attr('value', files);
            $('#zipFileName').attr('value', zipName);
            $('#MbDownload').submit();
        }

        $(window).resize(function() { //jquery 的resize事件，当浏览器大小改变时触发！           
            cWidth = document.documentElement.clientWidth;
            cHeight = document.documentElement.clientHeight;
            $('#DownDialog').window('move', { top: (cHeight - 380) / 2, left: (cWidth - 600) / 2 }); //easyui的dialog重新定位
        });

        //清空模版:
        function ClearMb() {
            $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
        }

        function prompt(rtn, wid, hg) {
            $.messager.show({
                title: '温馨提示',
                msg: rtn,
                width: wid,
                height: hg,
                timeout: 1000,
                showType: 'slide'
            });
        }
        //当剩余天数小于1天或者延迟的时候，将当前行字段设置成红色
//        function GSH(val, row) {
//            if (row.BZ == "1") {
//                return '<span style="color:red;">' + val + '</span>';
//            }
//            else {
//                return val;
//            }
//        }

        $(function() {
            BindFALB();
            BindYY();
            BindData();
            BindJHFA();
            InitZY();
            //            Init();
            InitDataGrid();
            ClearMb();
            initDownLoadMb(); //by liguosheng
        });
    </script>
    <style type="text/css">
        .style2 { height: 28px; }
    </style>
</head>
<body>
<form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
    <div region="north" border="true" style="height: 70px; padding: 1px; background: #B3DFDA;">
        <table>
            <tr>
                <td class="style2">
                    方案类别：<select id="SelFALX" onchange="ChageFALX()" style="width: 80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    年份：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({ lang: 'zh-cn', dateFmt: 'yyyy', onpicked: BindJHFA() })" style="width: 80px"/>&nbsp;&nbsp;
                    <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label>
                    <select id="SelData" onchange="BindJHFA()" style="width: 90px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    方案名称： <select id="SelJHFA" style="width: 120px;"></select>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="style2">
                    使用部门： <select id="SelZY" style="width: 120px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    状态：
                    <asp:DropDownList ID="DDLZT" Width="120px" runat="server">
                        <asp:ListItem Value="-1">----全部----</asp:ListItem>
                        <asp:ListItem Value="0">待填报</asp:ListItem>
                        <asp:ListItem Value="1">待审核</asp:ListItem>
                        <asp:ListItem Value="2">未处理</asp:ListItem>
                        <asp:ListItem Value="3">已处理</asp:ListItem>
                    </asp:DropDownList>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    模板名称：<input class="easyui-textbox" type="text" id="TxtMB" style="width: 120px"/>&nbsp;&nbsp;
                    <input type="button" class="button5" value="查询" style="width: 50px;" onclick="Init()"/>&nbsp;&nbsp;
                    <input type="button" class="button5" value="下载模板" style="width: 80px;" onclick="getDownLoad()"/>&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div id="DivCenter" region="center">
        <div id="DivMb" style="width: 100%; height: 100%;">
            <table id="TabMB" style="width: 100%; height: 100%;" title="预算模板流程查询" data-options="singleSelect:true">
            </table>
        </div>
        <div id="DownDialog" class="easyui-dialog" title="模板下载" style="width: 600px; height: 380px;" data-options="iconCls:'icon-save',resizable:true,modal:false,closed: true">
            <table id="DownLoadMb" style="width: 100%; height: 100%;"></table>
        </div>
    </div>
    <div region="south" style="height: 50px;">
        <div id="DivShow" runat="server" style="position: absolute; text-align: center">

        </div>
    </div>
</form>
<form id="MbDownload" method="post" action="MBLCListHandler.ashx?action=GetDownLoads" style="display: none">
    <input id="jhfadm" name="jhfadm" value="" type="hidden"/>
    <input id="excelFiles" name="excelFiles" value="" type="hidden"/>
    <input id="zipFileName" name="zipFileName" value="" type="hidden"/>
</form>
</body>
</html>