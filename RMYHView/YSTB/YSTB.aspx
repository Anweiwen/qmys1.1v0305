﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="YSTB.aspx.cs" Inherits="YSTB_YSTB" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> 
    <table>
    <tr>
        <td>
          <div> 预算类型：</div>
        </td>
        <td>
           <select id="SelYSLX" onchange="YSLXChage()"  style="width:90px;"></select>&nbsp;&nbsp;
        </td>
        <td>
          <span> </span>
        </td>
        <td>
            模板周期：<select id="SelMBZQ" onchange="getlist('','','')"  style="width:90px;"></select>&nbsp;
        </td>
        <td>
             <input type="button" class="button5" value="添加模板" style="width:70px; " onclick ="SelectQZMB(true)" />&nbsp;
        </td>
        <td>
             <input type="button" class="button5" value="添加前置模板" style="width:90px; " onclick ="SelectQZMB(false)" />&nbsp;
        </td>
        <td>
            <input id="Button11" class="button5"  type="button" style="width:50px;" value="展开"  onclick="TreeZK()"/>&nbsp;
        </td>
        <td>
            <input id="Button1" class="button5"  type="button" value="收起" style="width:50px;"  onclick="TreeSQ()"/>&nbsp;
        </td>
        <td>
            <input id="Button3" class="button5"  type="button" value="修改" style="width:50px;"  onclick="UPDATE()"/>&nbsp;
        </td>
        <td>
            <input type="button" class="button5" value="删除" style="width:50px;" onclick="DelQZMB()" />&nbsp;
        </td>
        <td>
            <input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:70px;"/>&nbsp;
        </td>
        <td>
            <input id="Button2" class="button5"  type="button" value="保存" style="width:50px;"  onclick="Saves()"/>
        </td>
         <td>
            <input id="Button4" class="button5"  type="button" value="关联下级模板" style="width:100px;"  onclick="OpenChirldSZ(true)"/>
        </td>
         <td>
            <input id="Button5" class="button5"  type="button" value="流程转移" style="width:100px;"  onclick="OpenMBZYLCList(true)"/>
        </td>
    </tr>
 </table>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divListView"></div>
    <div id="Win" class="easyui-window" title="模板选取" closed="true" style="width:950px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                模板名称：<input type="text" id="TxtMB" style="width:120px" />&nbsp;&nbsp;
                <input id="BtnMB" type="button" value="查询" onclick="GetMBList()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="Win(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="Win(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                <div id="QZMBXQ"></div>
             </div>
       </div>
     </div>
    <div id="WinJS" class="easyui-window" title="角色选取" closed="true" style="width:800px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
           <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                角色名称：<input type="text" id="TxtJS" style="width:120px" />&nbsp;&nbsp;
                <input type="button" value="查询" onclick="GetJSList()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
            <div data-options="region:'center'" style="width:100%; height:100%;">
                <div id="JSXQ"></div>
            </div>
        </div>
     </div>
      <div id="DivChirldMB" class="easyui-window" title="关联下级模板查询" minimizable="false" closed="true" style="width:950px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                <input type="button" value="添加" onclick="OpenTree(true,0)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="删除" onclick="DelChilrdMB()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="OpenChirldSZ(false)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                <div id="DivChirld" style="width: 100%; height:100%;"   data-options="singleSelect:false"></div>
             </div>
       </div>
     </div>
     <div id="DivTree" class="easyui-window" title="关联下级模板设置" closed="true" minimizable="false" style="width:900px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                <input type="button" value="确定" onclick="OpenTree(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="OpenTree(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                   <div id="divChirldList"></div>
             </div>
       </div>
     </div>
     <div id="DivZY" class="easyui-window" title="模板转移列表查询" closed="true" minimizable="false" style="width:950px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                <input type="button" value="添加" onclick="WinZY(true,0)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="OpenMBZYLCList(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="OpenMBZYLCList(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                   <div id="divZYList"></div>
             </div>
       </div>
     </div>
     <div id="DivAddZY" class="easyui-window" title="模板选取" closed="true" style="width:900px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                模板名称：<input type="text" id="TxtZYMB" style="width:120px" />&nbsp;&nbsp;
                <input type="button" value="查询" onclick="GetZYMBList()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="WinZY(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinZY(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                <div id="DivAddZYList"></div>
             </div>
       </div>
     </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input type="hidden" id="hidindexid" value="MBDM" />
    <input type="hidden"   id="hidcheckid" />
    <input type="hidden"   id="HidTrID" />
    <input type="hidden"   id="HidIsUPDATE" />
    <input type="hidden"   id="HidFarMBDM" />
    <input type="hidden"   id="HidBZ" />
    <input type="hidden"   id="HidJSDM" />
    <input  type="hidden" id="HidMBName"/>
    <input type="hidden"   id="HidJSNAME" />
    <input  type="hidden" id="HidLX"/>
    <input  type="hidden" id="HidFlag"/>
    <input  type="hidden" id="HidCCJB"/>
    <input  type="hidden" id="HidNAME"/>
    <input  type="hidden" id="HidCJBM"/>
    <input  type="hidden" id="HidJSBZ"/>

    <input type="hidden"   id="HidZYMB" />
    <input type="hidden"   id="HidZYJSDM" />
    <input type="hidden"   id="HidZYFarMBDM" />
    <input type="hidden"   id="HidZYCJBM" />
    <input type="hidden"   id="HidZYCCJB" />
    <input  type="hidden" id="HidZYTrID"/>

    <input type="hidden"   id="HidZYMBMB" />
    <input type="hidden"   id="HidZYMBJSDM" />
    <input type="hidden"   id="HidZYMBLX" />
    <input type="hidden" id="HidZYMBTrID"/>

    <script type ="text/javascript">
        var JSOBJ, ZYCJBM, ZYBZ = "", ZYStr = "";
        function getlist(objtr, objid, intimagecount) {
            var rtnstr = YSTB_YSTB.LoadList(objtr, objid, intimagecount, $("#SelYSLX").val(), $("#SelMBZQ").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divListView").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
//            GetJSName(objid);
        }
//        function GetJSName(PAR) {
//            if (PAR == "") 
//            {
//                var tr = $("#divListView tr");
//                for (var i = 1; i < tr.length; i++) {
//                    var trid = tr[i].id;
//                    var JSDM = $("#" + trid).find("td[name^='tdJSDM']");
//                    var DM = $("#" + trid).find("td[name^='tdDM']");
//                    if (JSDM[0].innerHTML != "") {
//                        JSDM[0].innerHTML = YSTB_YSTB.GetJSNameByJSDM(DM[0].innerHTML).value;
//                    }
//                }
//            } 
//            else {
//                var td = $("#divListView td[name=tdPARCJBM]").filter(function () { return this.innerHTML == "" + PAR + ""});
//                for (var i = 0; i < td.length; i++) {
//                    var trid = td[i].parentNode.id;
//                    var JSDM = $("#" + trid).find("td[name^='tdJSDM']");
//                    var DM = $("#" + trid).find("td[name^='tdDM']");
//                    if (JSDM[0].innerHTML != "") {
//                        JSDM[0].innerHTML = YSTB_YSTB.GetJSNameByJSDM(DM[0].innerHTML).value;
//                    }
//                }
//            }
//           

//        }
        function YSLXChage() {
            getlist('','','')
        }
        function GetMBList() {
            var rtnstr;
            if ($("#HidIsUPDATE").val() == "1") 
            {
                $("#TxtMB").val($("#HidNAME").val());
                $("#TxtMB").attr("disabled", "disabled");
                $("#BtnMB").attr("disabled", "disabled");
                rtnstr = YSTB_YSTB.LoadMBList($("#TxtMB").val(), $("#SelYSLX").val(), $("#SelMBZQ").val(), true).value;
                $("#HidNAME").val("");
            }
            else 
            {
                $("#TxtMB")[0].disabled = false;
                $("#BtnMB")[0].disabled = false;
                rtnstr = YSTB_YSTB.LoadMBList($("#TxtMB").val(), $("#SelYSLX").val(), $("#SelMBZQ").val(), false).value;
            }
            document.getElementById("QZMBXQ").innerHTML = rtnstr;
            $("#QZMBXQ INPUT[type=button][VALUE!='选取']").attr("style", "display:none");
        }
        function GetJSList() {
            var rtnstr = YSTB_YSTB.GetJSList($("#TxtJS").val()).value;
            document.getElementById("JSXQ").innerHTML = rtnstr;
            $("#JSXQ INPUT[type=button]").attr("style", "display:none")
        }
        function SelectQZMB(Flag) {
            if (Flag == false) 
            {
                if ($("#hidcheckid").val() == "") 
                {
                    alert("请选择要添加前置模板的项！");
                    return;
                }
                else 
                {
                    if ($("#HidLX").val() == "填报") {
                        alert("此模板是填报类型，不能添加前置模板！");
                        return;
                    } else {
                        Win(true, 0);
                    }
                }
            }
            else 
            {
                Win(true, 0);
            }
            $("#HidFlag").val(Flag);
        }
        function Win(Flag,BZ) {
            if (Flag == true) 
            {
                $("#Win").window('open');
                GetMBList();
            }
            else {
                if (BZ == 1) 
                {
                    var F = true;
                    var JSStr = "",CJBM="";
                    var IsExistMB = false, IsExist = false,IsJSZQ=false;
                    var CK = $("#QZMBXQ INPUT:checked");
                    if (CK.length == 0) 
                    {
                        alert("请勾选要选择的模板项！");
                        return;
                    }
                    for (var i = 0; i < CK.length; i++) 
                    {
                        JSStr = "";
                        var trid = CK[i].parentNode.parentNode.id;
                        var QZMBDM = $("#" + trid).find("td[name^='tdMBDM']")[0].innerHTML;
                        var JSDM = $("#" + trid + " input[name^='tselJSDM']").val();
                        var LX = $("#" + trid + " select[name^='selSBLX']").val();
                        if (JSDM == "" || JSDM == null) 
                        {
                            F = false;
                            break;
                        }
                        else 
                        {
                            var JS = JSDM.split(',');
                            for (var j = 0; j < JS.length; j++) 
                            {
                                if (JSStr == "") 
                                {
                                    JSStr = JS[j].split('.')[0];
                                } 
                                else 
                                {
                                    JSStr = JSStr + "," + JS[j].split('.')[0];
                                }
                            }
                            var rtn = YSTB_YSTB.ISExistQZMB($("#HidFarMBDM").val(), QZMBDM, JSStr,LX).value;
                            if (rtn == true) 
                            {
                                IsExistMB=true;
                                break;
                            }
                            //添加时做判断，修改不做判断
                            if ($("#HidIsUPDATE").val() != "1") {
                                CJBM = "";
                            } else {
                                CJBM = $("#HidCJBM").val();
                            }
                            var DM = YSTB_YSTB.JSSZSFZQ(QZMBDM, $("#SelYSLX").val(),CJBM).value;
                            if (DM!="" && DM!=null) {
                                if (DM != JSStr) {
                                    IsJSZQ = true;
                                    break;
                                }
                            }
                        }
                        //添加时做判断，修改不做判断
                        if ($("#HidIsUPDATE").val() != "1") {
                            var BZ = YSTB_YSTB.IsExistsMB(QZMBDM, $("#SelYSLX").val(),$("#HidCJBM").val()).value;
                            if (BZ == true) {
                                IsExist = true;
                                break;
                            }
                        }
                    }
                    if (F == true) 
                    {
                        if (IsExistMB == true) 
                        {
                            alert("此前置模板已经存在！");
                            return;
                        }
                        else if (IsExist == true) 
                        {
                            alert("此模板已作为别的模板的前置模板，不能重复添加！");
                            return;
                        }
                        else if (IsJSZQ == true) 
                        {
                            alert("此模板为交叉模板，角色设置不一致！");
                            return;
                        }
                        else 
                        {
                            $("#Win").window('close');
                        }
                    }
                    else 
                    {
                        alert("角色不能为空！");
                        return;
                    }
                }
                else 
                {
                    $("#HidBZ").val("1");
                    $("#Win").window('close');
                }
            }
        }
        function WinCloseEvent() {
            $("#Win").window({
                closable: false,
                collapsible: false,
                minimizable: false,
                maximizable: false,
                onBeforeClose: function () {
                    if ($("#HidBZ").val() != "1") 
                    {
                        var MBDM = "", JSStr = "";
                        var CK = $("#QZMBXQ INPUT:checked");
                        for (var i = 0; i < CK.length; i++) 
                        {
                            JSStr = "";
                            var trid = CK[i].parentNode.parentNode.id;
                            var DM = $("#" + trid).find("td[name^='tdMBDM']")[0].innerHTML;
                            var LX = $("#" + trid + " select[name^='selSBLX']").val();
                            var JSDM = $("#" + trid + " input[name^='tselJSDM']").val();
                            if (JSDM != "") {
                                var JS = JSDM.split(',');
                                for (var j = 0; j < JS.length; j++) {
                                    if (JSStr == "") {
                                        JSStr = JS[j].split('.')[0];
                                    }
                                    else {
                                        JSStr = JSStr + "','" + JS[j].split('.')[0];
                                    }
                                }
                                JSStr = "'" + JSStr + "'";
                            }
                            if (MBDM == "") {
                                MBDM = DM + "&" + LX + "&" + JSStr;
                            }
                            else {
                                MBDM = MBDM + "|" + DM + "&" + LX + "&" + JSStr;
                            }
                        }
                        if (MBDM != "") {
                            if ($("#HidIsUPDATE").val() == "1") {
                                var rtn = YSTB_YSTB.UPDATEMB(MBDM, $("#HidFarMBDM").val(), $("#hidcheckid").val(), $("#HidJSDM").val(), $("#SelYSLX").val()).value;
                                if (rtn > 0) {
                                    alert("操作成功！");
                                    $("#HidIsUPDATE").val("");
                                    $("#hidcheckid").val("");
                                    $("#HidJSDM").val("");
                                    $("#HidJSNAME").val("");
                                    $("#HidLX").val("");
                                    $("#HidMBName").val("");
                                    $("#HidFarMBDM").val("");
                                    $("#HidCCJB").val("");
                                    $("#HidTrID").val("");
                                    $("#TxtMB").val("");
                                    getlist('', '', '');
                                    return;
                                }
                                else {
                                    alert("操作失败！");
                                    $("#TxtMB").val("");
                                    return;
                                }
                            }
                            else {
                                if (confirm("确定要添加模板吗？")) {
                                    var rtn = YSTB_YSTB.AddQZMB(MBDM, $("#hidcheckid").val(), $("#HidFlag").val(), $("#HidFarMBDM").val(), $("#HidCCJB").val(), $("#SelYSLX").val(), $("#HidCJBM").val()).value;
                                    if (rtn > 0) {
                                        alert("操作成功！");
                                        $("#hidcheckid").val("");
                                        $("#HidJSDM").val("");
                                        $("#HidJSNAME").val("");
                                        $("#HidLX").val("");
                                        $("#HidMBName").val("");
                                        $("#HidFarMBDM").val("");
                                        $("#HidCCJB").val("");
                                        $("#HidTrID").val("");
                                        $("#TxtMB").val("");
                                        getlist('', '', '');
                                        return;
                                    }
                                    else {
                                        alert("操作失败！");
                                        $("#TxtMB").val("");
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        $("#HidBZ").val("");
                    }
                    $("#TxtMB").val("");
                    $("#HidIsUPDATE").val("");
                }
            });
        }
        function TreeZK() {
            var tr = $("#divListView img[src$='tplus.gif']");
            for (var i = 0; i < tr.length; ) {
                var trid = tr[i].parentNode.parentNode.id;
                ToExpand(tr[i], trid);
                i = 0;
                tr = $("#divListView img[src$='tplus.gif']");
            }
        }
        function TreeSQ() {
            var tr = $("#divListView img[src$='tminus.gif']");
            for (var i = 0; i < tr.length; i++) {
                var trid = tr[i].parentNode.parentNode.id;
                var CCJB = $("#" + trid + " td[name^='tdCCJB']").html();
                if (CCJB == 1) {
                    ToExpand(tr[i], trid);
                }
            }
        }
        function ToExpand(obj, id) {
            var trs;
            var Flag = 1;
            if ($("#" + obj.parentNode.parentNode.id + " input[type=checkbox]").length > 0) {
                Flag = 0;
                trs = $("#divChirldList tr");
            } else if ($("tr[id=" + obj.parentNode.parentNode.id + "] td[name$=tdISZY]").length > 0) {
                Flag = 2;
                trs = $("#divZYList tr");
            } else {
                trs = $("#divListView tr");
            }
            if (obj.src.indexOf("tminus.gif") > -1) {
                obj.src = "../Images/Tree/tplus.gif";
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lminus.gif") > -1) {
                obj.src = "../Images/Tree/lplus.gif";
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lplus.gif") > -1) {
                obj.src = "../Images/Tree/lminus.gif";
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
            else if (obj.src.indexOf("tplus.gif") > -1) {
                obj.src = "../Images/Tree/tminus.gif";
                var istoload = true;
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                        istoload = false;
                    }
                }
                if (istoload == true) 
                {
                    if (Flag == 0) 
                    {
                        getChirldList(id, $("tr[id=" + id + "] td[name$='tdCJBM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                        trs = $("#divChirldList tr");
                    } 
                    else if(Flag == 2) {
                        GetAllChirldCJBM(id, $("tr[id=" + id + "] td[name$='tdCJBM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                        trs = $("#divZYList tr");
                    }
                    else 
                    {
                        getlist(id, $("tr[id=" + id + "] td[name$='tdCJBM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                        trs = $("#divListView tr");
                    }
                    for (var i = 0; i < trs.length; i++) {
                        if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                            trs[i].style.display = "";
                        }
                    }
                }
            }
        }
        function onselects(obj) 
        {
            var trid = obj.parentNode.parentNode.id;
            if ($("tr[id=" + trid + "] td[name$=tdISZY]").length > 0) {
                $("#divZYList tr[name^='trdata']").css("backgroundColor", "");
                $("#HidZYMB").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
                $("#HidZYJSDM").val($("tr[id=" + trid + "] td[name$=tdDM]").html());
                $("#HidZYCJBM").val($("tr[id=" + trid + "] td[name$=tdCJBM]").html());
                if ($("tr[id=" + trid + "] td[name$=tdLX]").html() == "填报") {
                    alert("不能将填报类型的模板作为目标模板，请选择其他模板！");
                    $("#HidZYMB").val("");
                    $("#HidZYJSDM").val("");
                    $("#HidZYCJBM").val("");
                    return;
                }
                if (YSTB_YSTB.IsExistSBMB($("#HidZYMB").val(), $("#HidZYJSDM").val(), $("#HidZYCJBM").val(), $("#SelYSLX").val()).value == false) {
                    alert("此模板不能作为目标模板，请选择与之模板相同角色不同的其他模板！");
                    $("#HidZYMB").val("");
                    $("#HidZYJSDM").val("");
                    $("#HidZYCJBM").val("");
                    return;
                }
                else {
                    $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                }
            }
            else {
                $("#divListView tr[name^='trdata']").css("backgroundColor", "");
                $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
                $("#HidJSDM").val($("tr[id=" + trid + "] td[name$=tdDM]").html());
                $("#HidJSNAME").val(YSTB_YSTB.GetJSNameANDJSDM($("#HidJSDM").val()).value);
                $("#HidLX").val($("tr[id=" + trid + "] td[name$=tdLX]").html());
                $("#HidMBName").val($("tr[id=" + trid + "] td[name$=tdMBMC]").html());
                $("#HidFarMBDM").val($("tr[id=" + trid + "] td[name$=tdPAR]").html());
                $("#HidCJBM").val($("tr[id=" + trid + "] td[name$=tdCJBM]").html());
                $("#HidCCJB").val($("tr[id=" + trid + "] td[name$=tdCCJB]").html());
                $("#HidTrID").val(trid);
            }
        }
        function UPDATE() {
            var ID = $("#HidTrID").val();
            if (ID == "") {
                alert("请选择要修改的项！");
                return;
            }
            else 
            {
                $("#HidNAME").val($("#HidMBName").val());
                $("#HidIsUPDATE").val("1");
                Win(true,0);
                var MB = $("#QZMBXQ td[name^=tdMBDM]");
                for (var i = 0; i < MB.length; i++) 
                {
                    if (MB[i].innerHTML == $("#hidcheckid").val()) 
                    {
                        var trid = MB[0].parentNode.id;
                        var LX = $("#" + trid + " select[name^=selSBLX]");
                        if (LX[0][0].innerHTML == $("#HidLX").val()) 
                        {
                            LX[0][0].selected = true;
                        } else {
                            LX[0][1].selected = true;
                        }
                        var JS = $("#" + trid + " input[name^=tselJSDM]");
                        JS.val($("#HidJSNAME").val());
                    }
                }
            }
        }
        var MBMBDM, MBJSDM, MBTBLX;
        function EditData(obj, flag)
        {
            if (ZYBZ == "1") 
            {
                var JSStr = "";
                var ZYCK=$("#DivAddZYList INPUT:checked");
                if (ZYCK.length > 1) 
                {
                    alert("只能选择一个目标转移模板项！");
                    obj.checked = false;
                    return;
                }
                else 
                {
                    var trid = obj.parentNode.parentNode.id;
                    if ($("#" + obj.parentNode.parentNode.id).find("td[name^='tdJSMS']").length == 0) {
                        MBMBDM = $("#" + trid).find("td[name^='tdMBDM']")[0].innerHTML;
                        MBJSDM = $("#" + trid + " input[name^='tselJSDM']").val();
                        MBTBLX = $("#" + trid + " select[name^='selSBLX']").val();
                        if (MBTBLX == "0") {
                            alert("不能选择填报类型的模板作为转移的目标模板！");
                            var CK = $("#" + trid + " input[name^='chkISCHECK']")[0];
                            if (CK.checked == true) {
                                CK.checked = false;
                            }
                            return;
                        }
                        if (MBJSDM != "" && MBJSDM != null) {
                            var JS = MBJSDM.split(',');
                            for (var j = 0; j < JS.length; j++) {
                                if (JSStr == "") {
                                    JSStr = JS[j].split('.')[0];
                                }
                                else {
                                    JSStr = JSStr + "','" + JS[j].split('.')[0];
                                }
                            }
                        }
                        var rtn = YSTB_YSTB.ISExistZYMB("0", MBMBDM, "'" + JSStr + "'", MBTBLX).value;
                        if (rtn == true) {
                            alert("当前模板已经添加，不能重复添加！");
                            var CK = $("#" + trid + " input[name^='chkISCHECK']")[0];
                            if (CK.checked == true) {
                                CK.checked = false;
                            }
                            return;
                        }
                    }
//                    else 
//                    {
//                        //模板转移时，获取被选中的角色代码
//                        ZYJS = ZYJS == "" ? $("#" + obj.parentNode.parentNode.id).find("td[name=tdJSDM]").html() : ZYJS + "','" + $("#" + obj.parentNode.parentNode.id).find("td[name=tdJSDM]").html();
//                    }
                }
            }
            else 
            {
                if (flag == "0") {
                    if (obj.name == "chkISCHECK") {
                        if ($("#HidIsUPDATE").val() == "1") {
                            var CK = $("#QZMBXQ INPUT:checked");
                            if (CK.length > 1) {
                                alert("只能修改一行数据！");
                                obj.checked = false;
                                return;
                            }
                            else {
                                if ($("#" + obj.parentNode.parentNode.id).find("td[name^='tdMBDM']").length > 0) {
                                    var JSStr = "";
                                    var QZMBDM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdMBDM']").html();
                                    //                                var MBDM = $("#" + $("#HidParMBDM").val()).find("td[name^='tdMBDM']").html();
                                    var JSDM = $("#" + obj.parentNode.parentNode.id + " input[name^='tselJSDM']").val();
                                    var LX = $("#" + obj.parentNode.parentNode.id + " select[name^='selSBLX']").val();
                                    if (JSDM != "") {
                                        var JS = JSDM.split(',');
                                        for (var j = 0; j < JS.length; j++) {
                                            if (JSStr == "") {
                                                JSStr = JS[j].split('.')[0];
                                            } else {
                                                JSStr = JSStr + "','" + JS[j].split('.')[0];
                                            }
                                        }
                                    }
                                    var rtn = YSTB_YSTB.ISExistQZMB($("#HidFarMBDM").val(), QZMBDM, JSStr, LX).value;
                                    if (rtn == true) {
                                        alert("此前置模板已经存在不能重复选择！");
                                        obj.checked = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else {
                            var MBDM, CCJB = 1;
                            var QZMBDM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdMBDM']").html();
                            if ($("#HidFlag").val() == "true") {
                                if ($("#hidcheckid").val() == "") {
                                    MBDM = "0";
                                }
                                else {
                                    MBDM = $("#HidFarMBDM").val();
                                    CCJB = parseInt($("#HidCCJB").val());
                                }
                            }
                            else {
                                MBDM = $("#hidcheckid").val();
                                CCJB = parseInt($("#HidCCJB").val()) + 1;
                            }
                            if (YSTB_YSTB.ISExistXTMB(MBDM, QZMBDM, CCJB, $("#SelYSLX").val()).value == true) {
                                alert("同级不能添加相同模板！");
                                obj.checked = false;
                                return;
                            }
                        }
                    }
                    else if (obj.name == "selSBLX") {
                        if (obj.value == "0") {
                            if (YSTB_YSTB.IsExistChird($("#HidCJBM").val()).value) {
                                var LX = "";
                                if ($("#HidLX").val() == "填报") {
                                    LX = "0";
                                } else if ($("#HidLX").val() == "审核") {
                                    LX = "1";
                                } else {
                                    LX = "2";
                                }
                                alert("此模板有子节点，不能将其设置成填报类型！");
                                obj.value = LX;
                                return;
                            }
                        }
                    }
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
                }
            }
        }
        function DelQZMB() {

            var ID = $("#HidTrID").val();
            if (ID == "") 
            {
                alert("请选择要删除的项！");
                return;
            }
            else 
            {
                 Del();
            }

        }
        function Del() {
            if ($("#HidTrID").val() != "" && $("#hidcheckid").val() != "")
                $("tr[id=" + $("#HidTrID").val() + "] td[name$=td" + $("#hidindexid").val() + "]").each(function () { if ($(this).html() == $("#hidcheckid").val()) { $("#" + $(this).parent()[0].id + " img[name=readimage]").attr("src", "../Images/Tree/delete.gif"); } });
        }
        function Cdel() {
            if ($("#HidTrID").val() != "" && $("#hidcheckid").val() != "")
                $("tr[id=" + $("#HidTrID").val() + "] td[name$=td" + $("#hidindexid").val() + "]").each(function () { if ($(this).html() == $("#hidcheckid").val()) { $("#" + $(this).parent()[0].id + " img[name=readimage][src$='delete.gif']").attr("src", "../Images/Tree/noexpand.gif"); } });
        }
        function Saves() {
            var D = $("#divListView img[src$='delete.gif']");
            if (D.length == 0) {
                alert("您没有要保存的项！");
                return;
            }
            if (confirm("您要确定要执行删除操作吗？")) {
                var SID = "", MBDM = "", ParID = "", ParTrID = "", ParMBDM = "", QZMBDM = "",JSDM="",CJBM="",MBLX=""; 
                for (var j = 0; j < D.length; j++) 
                {
                    var trid = D[j].parentNode.parentNode.id;
                    if (trid != "") 
                    {
                        ParMBDM = $("#" + trid).find("td[name='tdPAR']")[0].innerHTML;
                        QZMBDM = $("#" + trid).find("td[name='tdMBDM']")[0].innerHTML;
                        JSDM = $("#" + trid).find("td[name='tdDM']")[0].innerHTML;
                        CJBM = $("#" + trid).find("td[name='tdCJBM']")[0].innerHTML;
                        if (YSTB_YSTB.IsExsitsChild(CJBM).value == true) {
                            alert("此模板有前置模板不能删除！");
                            return;
                        }
                        if (SID == "") {
                            SID = QZMBDM + "&" + ParMBDM+"&"+JSDM+"&"+CJBM;
                        }
                        else 
                        {
                            SID = SID + "|" + QZMBDM + "&" + ParMBDM + "&" + JSDM + "&" + CJBM;
                        }
                    }
                }
                var rtn = YSTB_YSTB.DelMB(SID,$("#SelYSLX").val()).value;
                if (rtn > 0) {
                    $("#hidcheckid").val("");
                    alert("保存成功！");
                    getlist('', '', '');
//                    TreeZK();
                    return;
                }
                else {
                    alert("保存失败！");
                    return;
                }
            }
        }
        function selectvalues(obj, id) {
            JSOBJ = obj;
            WinJS(true,0);
        }
        function WinJS(Flag, BZ) 
        {
            if (Flag == true) 
            {
                $("#WinJS").window('open');
                GetJSList();
            }
            else
            {
                if (BZ == -1) 
                {
                    $("#HidJSBZ").val("1");
                    JSOBJ = null;
                }
                else 
                {
                    var JSDM = "", JS = "";
                    var CK = $("#JSXQ INPUT:checked");
                    for (var i = 0; i < CK.length; i++) 
                    {
                        var trid = CK[i].parentNode.parentNode.id;
                        var DM = $("#" + trid).find("td[name^='tdJSDM']")[0].innerHTML;
                        var NAME = $("#" + trid).find("td[name^='tdNAME']")[0].innerHTML;
                        if (JSDM == "") 
                        {
                            JS = DM;
                            JSDM = DM + "." + NAME;
                        }
                        else 
                        {
                            JS = JS + "','" + DM;
                            JSDM = JSDM + "," + DM + "." + NAME;
                        }
                    }
                    if (CK.length > 0)
                        JSOBJ.parentNode.childNodes[0].value = JSDM;
                    $("#JSXQ img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
                    //
                    if (MBMBDM != "" && MBMBDM != null) 
                    {
                        var rtn = YSTB_YSTB.ISExistZYMB("0", MBMBDM, "'" + JS + "'", MBTBLX).value;
                        if (rtn == true) 
                        {
                            alert("当前角色的模板已经添加，不能重复添加！");
                            return;
                        }
                    }
                    $("#HidJSBZ").val("");
                }
                $("#WinJS").window('close');
            }
        }
        function WinJSCloseEvent() {
            $("#WinJS").window({
                closable: false,
                collapsible: false,
                minimizable: false,
                maximizable: false,
                onBeforeClose: function () {
//                    if ($("#HidJSBZ").val() != "1") 
//                    {
//                        var JSDM = "", JS = "";
//                        var CK = $("#JSXQ INPUT:checked");
//                        for (var i = 0; i < CK.length; i++) 
//                        {
//                            var trid = CK[i].parentNode.parentNode.id;
//                            var DM = $("#" + trid).find("td[name^='tdJSDM']")[0].innerHTML;
//                            var NAME = $("#" + trid).find("td[name^='tdNAME']")[0].innerHTML;
//                            if (JSDM == "") 
//                            {
//                                JS = DM;
//                                JSDM = DM + "." + NAME;
//                            }
//                            else 
//                            {
//                                JS = JS + "','" + DM;
//                                JSDM = JSDM + "," + DM + "." + NAME;
//                            }
//                        }
//                        if (CK.length > 0)
//                            JSOBJ.parentNode.childNodes[0].value = JSDM;
//                        $("#JSXQ img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
//                    }
//                    JSOBJ = null;
                }
            });
        }
        function BindYSLX() {
            var rtn = YSTB_YSTB.GetYSLX().value;
            $("#SelYSLX").html(rtn);
        }
        function BindMBZQ() {
            var rtn = YSTB_YSTB.GetMBZQ().value;
            $("#SelMBZQ").html(rtn);
        }
        function Init() {
            $.ajax({
                type: 'get',
                url: 'MBLCHandler.ashx',
                data: {
                    JHFADM: '',
                    MBMC: $("#HidCJBM").val(),
                    action: "Chirld",
                    ZYDM:'',
                    ZT:''
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#DivChirld").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function InitDataGrid() {
            $('#DivChirld').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageList: [10, 20, 30, 40, 50],
                columns: [[
                            { field: 'MBDM', title: '模板代码', hidden: true },
                            { field: 'MBMC', title: '当前模板', hidden: true },
                            { field: 'DQJSDM', title: '当前角色', hidden: true },
					        { field: 'DQJSNAME', title: '操作角色', hidden: true },
                            { field: 'CHILDMBDM', title: '关联下级模板代码', hidden: true },
					        { field: 'ZJMBMC', title: '下级模板名称', width: 180 },
					        { field: 'NEXTJSDM', title: '下一个角色', hidden: true },
                            { field: 'NEXTJSNAME', title: '后续角色', width: 180 },
                            { field: 'CJBM', title: '层级编码', hidden: true },
                            { field: 'CHILDCJBM', title: '子层级编码', hidden: true },
				        ]]
            });
        }
        function DelChilrdMB() {
            var CJ = "";
            var CH = $('#DivChirld').datagrid("getSelections");
            if (CH.length == 0) {
                alert("请选择要删除的项！");
                return;
            }
            if (confirm("您确定要删除吗？")) {
                for (var i = 0; i < CH.length; i++) {
                    CJ = CJ == "" ? CH[i].CJBM + "," + CH[i].CHILDCJBM : CJ + "|" + CH[i].CJBM + "," + CH[i].CHILDCJBM;
                }
                var rtn = YSTB_YSTB.DelChilrdMB(CJ).value;
                if (rtn > 0) {
                    alert("操作成功！");
                    Init();
                    return;
                } else {
                    alert("操作失败！");
                    return;
                }
            }
        }
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        function CheckBoxChange(obj) {
            if (obj.checked == true) 
            {
                var trid = obj.parentNode.parentNode.id;
                var CJBM = $("#" + trid).find("td[name^='tdCJBM']").html();
                var JSDM = $("#" + trid).find("td[name^='tdDM']").html();
                var MBDM = $("#" + trid).find("td[name^='tdMBDM']").html();
                if ($("#HidCJBM").val().indexOf(CJBM) > -1 || CJBM.indexOf($("#HidCJBM").val()) > -1) 
                {
                    alert("不能选择自己、子节点或者父节点模板！");
                    obj.checked = false;
                    return;
                }
                else if ($("#HidJSDM").val() == JSDM && $("#hidcheckid").val() == MBDM) {
                    alert("不能选择自己！");
                    obj.checked = false;
                    return;
                }
                var C3 = YSTB_YSTB.GetMBANDJS(CJBM.substring(0, 4)).value;
                var C2 = YSTB_YSTB.GetCJBM(JSDM, MBDM, CJBM, $("#SelYSLX").val()).value;
                var C1 = YSTB_YSTB.GetChird(CJBM, $("#SelYSLX").val()).value;
                var P1 = YSTB_YSTB.GetPar(CJBM.substring(0, 4), $("#SelYSLX").val()).value;
                var tr = $("#divChirldList input:checked");
                for (var i = 0; i < tr.length; i++) {
                    var id = tr[i].parentNode.parentNode.id;
                    var CJ = $("#" + id).find("td[name^='tdCJBM']").html();
                    var JS = $("#" + id).find("td[name^='tdDM']").html();
                    var MB = $("#" + id).find("td[name^='tdMBDM']").html();
                    if (CJ != CJBM) 
                    {
                        if (CJ.indexOf(CJBM) > -1 || CJBM.indexOf(CJ) > -1) 
                        {
                            alert("同一个根节点下不能选取多个模板！");
                            obj.checked = false;
                            return;
                        }
                        else if (JS == JSDM && MB == MBDM) 
                        {
                            alert("不能选取相同模板！");
                            obj.checked = false;
                            return;
                        }
                        else {
                            if (C2 != "") 
                            {
                                var BM = C2.split(',');
                                for (var j = 0; j < BM.length; j++) 
                                {
                                    if (BM[j].toString().indexOf(CJ) > -1 || CJ.indexOf(BM[j].toString()) > -1) 
                                    {
                                        alert("同一模板以及子节点不能选取多个！");
                                        obj.checked = false;
                                        return;
                                    }
                                }
                            }
                            else 
                            {
                                if (CJBM.length != 4) 
                                {
                                    if (C3 != "") 
                                    {
                                        var DM = C3.split('@');
                                        if (DM[0].toString() == JS && DM[1].toString() == MB) 
                                        {
                                            alert("同一模板以及子节点不能选取多个！");
                                            obj.checked = false;
                                            return;
                                        }
                                    }

                                }
                                if (C1 != "") 
                                {
                                    var BM = C1.split(',');
                                    for (var j = 0; j < BM.length; j++) 
                                    {
                                        if (BM[j].toString().indexOf(CJ) > -1 || CJ.indexOf(BM[j].toString()) > -1) {
                                            alert("同一模板以及子节点不能选取多个！");
                                            obj.checked = false;
                                            return;
                                        }
                                    }
                                }
                                if (P1 != "") 
                                {
                                    var BM = P1.split(',');
                                    for (var j = 0; j < BM.length; j++) {
                                        if (BM[j].toString().indexOf(CJ) > -1) {
                                            alert("同一模板以及子节点不能选取多个！");
                                            obj.checked = false;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
            }
            else {
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("edit.jpg") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/noexpand.gif"; 
            }
        }
        function OpenChirldSZ(Flag) 
        {
            if (Flag == true) 
            {
                var CJBM = $("#HidCJBM").val();
                if (CJBM != "" && CJBM != null) 
                {
                    $("#DivChirldMB").window('open');
                    Init();
                } else 
                {
                    alert("请选择要设置关联下级的项！");
                    return;
                }
            }
            else 
            {
                $("#DivChirldMB").window('close');
            }

        }
        function OpenTree(Flag, BZ) 
        {
            if (Flag == true) 
            {
                $("#DivTree").window('open');
                //打开关联下级模板树
                getChirldList('','','');
            }
            else 
            {
                if (BZ == 1) {
                    var Str = "";
                    var tr = $("#divChirldList input:checked");
                    if (tr.length == 0) {
                        alert("您没有选择要关联的下级模板!");
                        return;
                    }
                    for (var i = 0; i < tr.length; i++) {
                        var id = tr[i].parentNode.parentNode.id;
                        var CJBM = $("#" + id).find("td[name^='tdCJBM']").html();
                        var JSDM = $("#" + id).find("td[name^='tdDM']").html();
                        var MBDM = $("#" + id).find("td[name^='tdMBDM']").html();
                        Str = Str == "" ? MBDM + "&" + JSDM + "&" + CJBM : Str + "|" + MBDM + "&" + JSDM + "&" + CJBM;
                    }
                    var rtn = YSTB_YSTB.InsertChirldMB(Str, $("#hidcheckid").val(), $("#HidJSDM").val(), $("#HidCJBM").val()).value;
                    if (rtn > 0) {
                        alert("操作成功！");
                        Init();
                        $("#DivTree").window('close');
                        return;
                    }
                    else {
                        alert("操作失败！");
                        return;
                    }
                }
                else {
                    $("#DivTree").window('close');
                }
            }
        }
        function getChirldList(objtr, objid, intimagecount) {
            var rtnstr = YSTB_YSTB.LoadChirldList(objtr, objid, intimagecount, $("#SelYSLX").val(), $("#SelMBZQ").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divChirldList").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
            getCheckedMB();
        }
        function getCheckedMB() {
            var td,CK;
            var trid = "";
            var rtn = YSTB_YSTB.GetChildCJBM($("#hidcheckid").val(), $("#HidJSDM").val(), $("#HidCJBM").val()).value;
            if (rtn != "" && rtn != null) {
                var CJ = rtn.split(',');
                for (var i = 0; i < CJ.length; i++) {
                    td = $("#divChirldList td[name=tdCJBM]").filter(function () { return this.innerHTML == "" + CJ[i] + "" });
                    if (td.length > 0) {
                        trid = td[0].parentNode.id;
                        CK = $("#" + trid + " input[name=ISCHECK]");
                        if (CK[0].checked == false) {
                            CK[0].checked = true;
                        }
                    }
                }
            }
        }
        function GetSBLX() 
        {
            var LX="";
            if ($("#HidLX").val() == "填报") 
            { 
                LX="0";
            }
            else if ($("#HidLX").val() == "审核") 
            {
                LX = "1";
            }
            else if ($("#HidLX").val() == "生成预算表") 
            {
               LX = "2";
            }
           return LX;
        }
        function GetAllChirldCJBM(objtr, objid, intimagecount) {
            var rtnstr = YSTB_YSTB.GetAllChirldCJBM(objtr, objid, intimagecount, $("#SelYSLX").val(), $("#SelMBZQ").val(), ZYCJBM).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divZYList").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
        }
        function OpenMBZYLCList(Flag,IsOK) 
        {
            if (Flag == true) 
            {
                var CJBM = $("#HidCJBM").val();
                if (CJBM != "" && CJBM != null) 
                {
//                    var rtn = YSTB_YSTB.IsExistSPHistory($("#HidCJBM").val(), $("#SelYSLX").val()).value;
//                    if (rtn != "" && rtn != null) 
//                    {
//                        alert("该模板流程已经在" + rtn + "等计划方案中走流程了，请先将该模板流程还原或删除流程方案发起!");
//                        return;
//                    }
                    $("#DivZY").window('open');
                    ZYBZ = "1";
                    ZYCJBM = YSTB_YSTB.GetZYCJBM($("#HidCJBM").val(), $("#hidcheckid").val(), $("#HidJSDM").val(), GetSBLX(), $("#SelYSLX").val()).value;
                    GetAllChirldCJBM('', '', '');
                }
                else 
                {
                    alert("请选择要转移的模板项！");
                    return;
                }
            }
            else 
            {
                if (IsOK == "1") {
                    var ZYCJ = $("#HidZYCJBM").val();
                    if (ZYCJ != null && ZYCJ != "") 
                    {
                        if (confirm("您确定要模板转移吗？")) 
                        {
                            var rtn = YSTB_YSTB.MBZY(ZYCJ, $("#HidCJBM").val(), $("#HidZYMB").val(), ZYStr, $("#SelYSLX").val()).value;
                            if (rtn > 0) 
                            {
                                alert("转移成功！");
                                getlist('', '', '');
                                $("#HidCJBM").val("");
                            }
                            else 
                            {
                                alert("转移失败！");
                                return;
                            }
                        }
                    }
                    else 
                    {
                        alert("请选择要转移到的模板项！");
                        return;
                    }
                }
                ZYBZ = "";
                ZYStr = "";
                $("#DivZY").window('close');
            }

        }
        function GetZYMBList() {
            var rtnstr = YSTB_YSTB.LoadMBList($("#TxtZYMB").val(), $("#SelYSLX").val(), $("#SelMBZQ").val()).value;
            document.getElementById("DivAddZYList").innerHTML = rtnstr;
            $("#DivAddZYList INPUT[type=button][VALUE!='选取']").attr("style", "display:none");
        }
        function WinZY(Flag, BZ) 
        {
            if (Flag == true) 
            {
                $("#DivAddZY").window('open');
                GetZYMBList();
            }
            else 
            {
                if (BZ == 1) {
                    var JSStr = "";
                    var ZYCK = $("#DivAddZYList INPUT:checked");
                    if (ZYCK.length == 0) 
                    {
                        alert("请选中要转移到的目标模板项！");
                        return;
                    }
                    var trid = ZYCK[0].parentNode.parentNode.id;
                    var QZMBDM = $("#" + trid).find("td[name^='tdMBDM']")[0].innerHTML;
                    var JSDM = $("#" + trid + " input[name^='tselJSDM']").val();
                    var LX = $("#" + trid + " select[name^='selSBLX']").val();
                    if (JSDM == "" || JSDM == null) 
                    {
                        alert("角色不能为空！");
                        return;
                    }
                    else 
                    {
                        var JS = JSDM.split(',');
                        for (var j = 0; j < JS.length; j++) 
                        {
                            if (JSStr == "") 
                            {
                                JSStr = JS[j].split('.')[0];
                            }
                            else 
                            {
                                JSStr = JSStr + "','" + JS[j].split('.')[0];
                            }
                        }
                    }
                    ZYStr = QZMBDM + "|'" + JSStr + "'|" + LX;
                    if (confirm("您确定要模板转移吗？")) {
                        var rtn = YSTB_YSTB.MBZY("", $("#HidCJBM").val(), "", ZYStr, $("#SelYSLX").val()).value;
                        if (rtn > 0) {
                            alert("转移成功！");
                            getlist('', '', '');
                        }
                        else {
                            alert("转移失败！");
                            return;
                        }
                    }
                }
                ZYBZ = "";
                ZYStr = "";
                MBMBDM="";
                MBJSDM="";
                MBTBLX = "";
                $("#DivAddZY").window('close');
                $("#DivZY").window('close');
            }
        }
        $(document).ready(function () {
            BindYSLX();
            BindMBZQ();
            getlist('', '', '');
            WinCloseEvent();
            WinJSCloseEvent();
            InitDataGrid();
        });   
    </script>
</asp:Content>

