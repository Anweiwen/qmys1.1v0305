﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class YSTB_MBLCImage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_MBLCImage));
        //HidCJBM.Value = Request.QueryString["CJBM"].ToString();
        //HidMBDM.Value = Request.QueryString["MBDM"].ToString();
    }
    /// <summary>
    /// 获取模板流程的相关信息
    /// </summary>
    /// <param name="CJBM"></param>
    /// <param name="MBMC"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetLCImage(string CJBM,string MBDM,string ZT,string JHFADM)
    {
        int t = 0, BZ =1;
        string MBStr = "";
        DataSet DS = new DataSet();
        DataSet DT = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.Query("SELECT CJBM,JSDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' ORDER BY CJBM DESC");
        if (ZT == "0")
        {
            DT=BLL.Query("SELECT CJBM,JSDM FROM TB_MBLCSB WHERE DQMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND STATE=-1 AND JHFADM='"+JHFADM+"'");
            if (DT.Tables[0].Rows.Count > 0)
            {
                CJBM = DT.Tables[0].Rows[0]["CJBM"].ToString();
            }
            else
            {
                DT.Tables.Clear();
                DT = BLL.Query("SELECT CJBM,JSDM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' AND CJBM LIKE '" + CJBM.Substring(0, 4) + "%' AND LX='0' ORDER BY CJBM DESC");
                if (DT.Tables[0].Rows.Count > 0)
                {
                    CJBM = DT.Tables[0].Rows[0]["CJBM"].ToString();
                }
                else
                {
                    BZ = 0;
                }
            }
        }
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (i < DS.Tables[0].Rows.Count - 1)
            {
                if (i == 0)
                {
                    if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ==1)
                    {
                        MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    else
                    {
                        MBStr = "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:#ADD8E6;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    t++;
                }
                else
                {
                    if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ == 1)
                    {
                        MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    else
                    {
                        MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:#ADD8E6;width: 180px; height: 40px\" /><label style=\"width: 100px; height: 40px\">----------></label>";
                    }
                    t++;
                }
            }
            else
            {
                if (CJBM.Equals(DS.Tables[0].Rows[i]["CJBM"].ToString()) && BZ == 1)
                {
                    MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:Red;width: 180px; height: 40px\" />";
                }
                else
                {
                    MBStr = MBStr + "<input  type=\"button\"  value=" + GetJSNameByJSDM(DS.Tables[0].Rows[i]["JSDM"].ToString()) + "  style=\"background-color:#ADD8E6;width: 180px; height: 40px\" />";
                }
                t++;
            }
        }
        return MBStr+"@"+t.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
}