﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Collections;
using Sybase.Data.AseClient;
using System.Data;
using System.Text;
using RMYH.Model;

public partial class YSTB_YSLCFS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_YSLCFS));
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string YSLX)
    {
        return GetDataListstringMain("10144718", "", new string[] { ":YSLX" }, new string[] { YSLX }, false, "",YSLX);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string YSLX)
    {
        return getNewLine("10144718",false,"",YSLX);
    }
    /// <summary>
    /// 新增、更新或者删除记录
    /// </summary>
    /// <param name="NewStr">新增字符串</param>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="DelStr">删除字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UpdateORDel(string NewStr, string UpStr, string DelStr)
    {
        int Flag = 0,ISFQ=0;
        string YSLX = "",JHFADM = "", FA = "",LX = "",JZSJ="",TBBZ="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        if (UpStr != "" || DelStr != "" || NewStr != "")
        {
            //新增记录
            if (NewStr != "")
            {
                string[] Col = NewStr.Split('|');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split(',');
                    YSLX = ZD[0].ToString();
                    JHFADM = ZD[1].ToString();
                    ISFQ = int.Parse(ZD[2].ToString());
                    JZSJ = ZD[3].ToString();
                    TBBZ = ZD[4].ToString();
                    string NewSql = "INSERT INTO TB_YSLCFS (YSLX,JHFADM,ISFQ,TBSJXZ,XZTBBZ)VALUES(@YSLX,@JHFADM,@ISFQ,@TBSJXZ,@XZTBBZ)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                      new AseParameter("@YSLX", AseDbType.VarChar,4),
                      new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                      new AseParameter("@ISFQ", AseDbType.Integer),
                      new AseParameter("@TBSJXZ", AseDbType.VarChar, 50),
                      new AseParameter("@XZTBBZ", AseDbType.VarChar,3)
                    };
                    New[0].Value = YSLX;
                    New[1].Value = JHFADM;
                    New[2].Value = ISFQ;
                    New[3].Value = JZSJ;
                    New[4].Value = TBBZ;
                    List.Add(New);

                    //新增的方案，将公用的非受限角色拷贝过来
                    NewSql = "INSERT INTO TB_MBFAXZYH(JHFADM,JSDM,HSZXDM) SELECT  '" + JHFADM + "',JSDM,HSZXDM FROM TB_MBSJXZYH WHERE HSZXDM=@HSZXDM";
                    ArrList.Add(NewSql);
                    AseParameter[] NewJS = {
                      new AseParameter("@HSZXDM", AseDbType.VarChar,40)
                    };
                    NewJS[0].Value = HttpContext.Current.Session["HSZXDM"].ToString();
                    List.Add(NewJS);
                }
            }
            //更新记录
            if (UpStr != "")
            {
                string[] Col = UpStr.Split('|');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split(',');
                    YSLX = ZD[0].ToString();
                    JHFADM = ZD[1].ToString();
                    ISFQ = int.Parse(ZD[2].ToString());
                    LX = ZD[3].ToString();
                    FA = ZD[4].ToString();
                    JZSJ = ZD[5].ToString();
                    TBBZ = ZD[6].ToString();
                    string UPSql = "UPDATE TB_YSLCFS SET YSLX=@YSLX,JHFADM=@JHFADM,ISFQ=@ISFQ,TBSJXZ=@TBSJXZ,XZTBBZ=@XZTBBZ  WHERE YSLX=@LX AND JHFADM=@FA";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {               
                      new AseParameter("@YSLX", AseDbType.VarChar, 40),
                      new AseParameter("@JHFADM", AseDbType.VarChar,40),
                      new AseParameter("@ISFQ", AseDbType.Integer), 
                      new AseParameter("@LX", AseDbType.VarChar,4),
                      new AseParameter("@FA", AseDbType.VarChar, 40),
                      new AseParameter("@TBSJXZ", AseDbType.VarChar, 50),
                      new AseParameter("@XZTBBZ", AseDbType.VarChar,3)
                    };
                    UP[0].Value = YSLX;
                    UP[1].Value = JHFADM;
                    UP[2].Value = ISFQ;
                    UP[3].Value = LX;
                    UP[4].Value = FA;
                    UP[5].Value = JZSJ;
                    UP[6].Value = TBBZ;
                    List.Add(UP);

                    //如果当前修改的是计划方案代码，则修改方案非受限角色
                    if (FA.Equals(JHFADM))
                    {
                        string JS = "UPDATE TB_MBFAXZYH SET JHFADM=@JHFADM WHERE JHFADM=@FADM AND HSZXDM=@HSZXDM";
                        ArrList.Add(JS);

                        AseParameter[] UPJS = {               
                          new AseParameter("@JHFADM", AseDbType.VarChar, 40),
                          new AseParameter("@FADM", AseDbType.VarChar,40),
                          new AseParameter("@HSZXDM", AseDbType.VarChar,40)
                        };
                        UPJS[0].Value = JHFADM;
                        UPJS[1].Value = FA;
                        UPJS[2].Value = HttpContext.Current.Session["HSZXDM"].ToString();
                        List.Add(UPJS);
                    }

                }
            }
            //删除记录
            if (DelStr != "")
            {
                string[] Col = DelStr.Split('|');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split(',');
                    LX = ZD[0].ToString();
                    FA = ZD[1].ToString();
                    string DelSql = "DELETE FROM TB_YSLCFS WHERE YSLX=@LX AND JHFADM=@FA";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = 
                    {
                        new AseParameter("@LX", AseDbType.VarChar,4),
                        new AseParameter("@FA", AseDbType.VarChar, 40)
                    };
                    Del[0].Value = LX;
                    Del[1].Value = FA;
                    List.Add(Del);

                    //删除正在之前的审批历史
                    DelSql = "DELETE FROM TB_MBLCSB WHERE JHFADM=@FA";
                    ArrList.Add(DelSql);
                    AseParameter[] D = 
                    {
                        new AseParameter("@FA", AseDbType.VarChar, 40)
                    };
                    D[0].Value = FA;
                    List.Add(D);

                    //删除之前的默认打回的历史记录
                    DelSql = "DELETE FROM TB_MBDHLS WHERE JHFADM=@FA";
                    ArrList.Add(DelSql);
                    AseParameter[] DH = 
                    {
                        new AseParameter("@FA", AseDbType.VarChar, 40)
                    };
                    DH[0].Value = FA;
                    List.Add(DH);

                    //删除方案非受限角色
                    DelSql = "DELETE FROM TB_MBFAXZYH WHERE JHFADM=@FA AND HSZXDM=@HSZXDM";
                    ArrList.Add(DelSql);
                    AseParameter[] JS = 
                    {
                        new AseParameter("@FA", AseDbType.VarChar, 40),
                        new AseParameter("@HSZXDM", AseDbType.VarChar, 40)
                    };
                    JS[0].Value = FA;
                    JS[1].Value = HttpContext.Current.Session["HSZXDM"].ToString();
                    List.Add(JS);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        else
        {
            Flag = -1;
        }
        return Flag;
    }
    /// <summary>
    /// 根据预算类型，查询符合条件的计划方案下拉框字符串
    /// </summary>
    /// <param name="YSLX">预算类型</param>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string BindJHFA(string YSLX,string JHFADM)
    {
        bool Flag = false;
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YSBS='" + YSLX + "' AND SCBS=1");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["JHFADM"].ToString().Trim() == JHFADM.Trim())
                {
                    Flag = true;
                    Str += "<option selected=true value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
                else
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            if (JHFADM.Trim() != " ")
            {
                if (Flag == false)
                {
                    Str += "<option selected=true value=''>---请选择---</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYSLX()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSLX' AND XMDH_A IN(3,4,5)");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    #endregion
    #region 设置不受权限角色相关的代码

    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSList(string JSName)
    {
        return GetDataList.GetDataListstring("10144511", "", new string[] { ":NAME" }, new string[] { JSName }, false, "", "", "");
    }
    /// <summary>
    /// 新增、更新或者删除非受限角色
    /// </summary>
    /// <param name="NewStr">新增字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int AddJS(string NewStr,string F,string JHFADM)
    {
        int Flag = 0;
        string SQL = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        string HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        //判断当前是设置方案受限角色还是设置公共受限角色（1代表设置方案受限角色；空代表设置公共受限角色）
        if (F.Equals("1"))
        {
            //删除角色记录
            SQL = "DELETE FROM TB_MBFAXZYH  WHERE JHFADM=@FADM AND HSZXDM=@HSZXDM";
            ArrList.Add(SQL);
            AseParameter[] Del = {
                new AseParameter("@FADM", AseDbType.VarChar,40),
                new AseParameter("@HSZXDM", AseDbType.VarChar,40)
            };
            Del[0].Value = JHFADM;
            Del[1].Value = HSZXDM;
            List.Add(Del);
            if (NewStr != "")
            {
                //新增记录
                string[] Col = NewStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string NewSql = "INSERT INTO TB_MBFAXZYH (JSDM,HSZXDM,JHFADM)VALUES(@JSDM,@HSZXDM,@JHFADM)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                        new AseParameter("@JSDM", AseDbType.VarChar,40),
                        new AseParameter("@HSZXDM", AseDbType.VarChar, 40),
                        new AseParameter("@JHFADM", AseDbType.VarChar, 40)
                    };
                    New[0].Value = Col[i].ToString();
                    New[1].Value = HSZXDM;
                    New[2].Value = JHFADM;
                    List.Add(New);
                }
            }
        }
        else
        {
            //删除角色记录
            SQL = "DELETE FROM TB_MBSJXZYH  WHERE HSZXDM=@HSZXDM";
            ArrList.Add(SQL);
            AseParameter[] Del = {
                new AseParameter("@HSZXDM", AseDbType.VarChar,40)
            };
            Del[0].Value = HttpContext.Current.Session["HSZXDM"].ToString();
            List.Add(Del);
            if (NewStr != "")
            {
                //新增记录
                string[] Col = NewStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string NewSql = "INSERT INTO TB_MBSJXZYH (JSDM,HSZXDM)VALUES(@JSDM,@HSZXDM)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@JSDM", AseDbType.VarChar,40),
                    new AseParameter("@HSZXDM", AseDbType.VarChar, 40)
                };
                    New[0].Value = Col[i].ToString();
                    New[1].Value = HSZXDM;
                    List.Add(New);
                }
            }
        }
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 获取被选中的角色代码
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCheckedJS(string Flag,string JHFADM) {
        string JS = "",sql="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        
        if (Flag.Equals("1"))
        {
            sql = "SELECT JSDM FROM TB_MBFAXZYH WHERE JHFADM='" + JHFADM + "' AND HSZXDM='" + HttpContext.Current.Session["HSZXDM"].ToString() + "'";
        }
        else
        {
            sql = "SELECT JSDM FROM TB_MBSJXZYH WHERE HSZXDM='" + HttpContext.Current.Session["HSZXDM"].ToString() + "'";
        }
        DS = BLL.Query(sql);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            JS = JS == "" ? DS.Tables[0].Rows[i]["JSDM"].ToString() : JS + "," + DS.Tables[0].Rows[i]["JSDM"].ToString();
        }
        return JS;
    }
    /// <summary>
    /// 获取当前时间
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {       
        return DateTime.Now.ToString("yyyy-MM-dd HH:mm");
    }
    #endregion
    #region 发起列表刷新相关方法
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled,string YSLX)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\"");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                if (dr[i]["FIELDNAME"].ToString().Trim() == "JHFADM")
                {
                    string SQL = dr[i]["SQL"].ToString().Replace(":HSZXDM", "'" + HttpContext.Current.Session["HSZXDM"].ToString() + "'").Replace("YSBS='3'", "YSBS='" + YSLX + "'");
                    htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(SQL));
                } 
                else
                {
                    htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
                }
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled,YSLX);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled,string YSLX)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5  style=\"width:60px\" ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Trim() == "DQFASXJS")
                        {
                            //判断如果是当前方案非受限角色的话，则加载非受限角色名称
                            string JS = getFSXJS(dr[i]["JHFADM"].ToString(), HttpContext.Current.Session["HSZXDM"].ToString());
                            if (JS != "")
                            {
                                string[] SX = JS.Split('^');
                                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\"  name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + SX[1].ToString() + "\" >");
                                sb.Append(SX[1].ToString().Length > 20 ? SX[1].ToString().Substring(0, 20) + "..." : SX[1].ToString());
                            }
                            else
                            {
                                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\"  name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + JS + "\" >");
                                sb.Append("");
                            }
                        }
                        else
                        {
                            sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\"  name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                            if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Trim() == "TBSJXZ")
                            {
                                sb.Append("<input  name=\"TxtData\" value='" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "' onchange=\"DataChange(this)\" onclick=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'})\" style=\"width:150px\"  class=\"Wdate\"  />");
                            }
                            else if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Trim() == "SZSXJS")
                            {
                                //加载方案非受限角色设置按钮
                                sb.Append("<INPUT TYPE=BUTTON class=\"button5\" style=\"width:140px\"  VALUE=方案非受限角色设置 ONCLICK='SZJS(\"" + dr[i]["JHFADM"].ToString() + "\")'>");
                            }
                            else
                            {
                                sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                            }
                        }
                        sb.Append("</td>");
                    }
                }
                else
                {

                    //style=\"table-layout:fixed\"
                    sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\"  name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString(), ds.Tables[0].Rows[j]["FIELDNAME"].ToString(),YSLX) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Trim() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT   STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, string YSLX)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                if (dr[i]["FIELDNAME"].ToString().Trim() == "JHFADM")
                {
                    string SQL = dr[i]["SQL"].ToString().Replace(":HSZXDM", "'" + HttpContext.Current.Session["HSZXDM"].ToString() + "'").Replace("YSBS='3'", "YSBS='" + YSLX + "'");
                    htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(SQL));
                }
                else
                {
                    htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":HSZXDM","'"+HttpContext.Current.Session["HSZXDM"].ToString()+"'")));
                }
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Trim() == "TBSJXZ")
                        {
                            sb.Append("<input  name=\"TxtData\" onchange=\"DataChange(this)\" onclick=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'})\" style=\"width:150px\"  class=\"Wdate\"  />");
                        }
                        else
                        {
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        }
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择  
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Equals("YSLX"))
                                    {
                                        sb.Append("<select disabled=disabled  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    }
                                    else
                                    {
                                        sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    }
                                    
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString(), ds.Tables[0].Rows[j]["FIELDNAME"].ToString(), YSLX) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull, string FIELDNAME, string YSLX)
    {

        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            //预算类型默认选择页面上下拉框的值
            if (FIELDNAME.Trim() == "YSLX")
            {
                filedvalue = YSLX;
            }
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString().Trim())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 根据计划方案代码和核算中心代码获取非受限角色和名称
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="HSZXDM">核算中心代码</param>
    /// <returns>返回值包含两部分（角色代码和角色名称）两者之间用特殊符号^分割；角色代码和角色名称内部间用逗号隔开</returns>
    public static string getFSXJS(string JHFADM, string HSZXDM)
    {
        string JS = "", Name = "", JSStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT JSDM,NAME FROM TB_JIAOSE WHERE JSDM IN(SELECT JSDM FROM TB_MBFAXZYH WHERE JHFADM='" + JHFADM + "' AND HSZXDM='" + HSZXDM + "')";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                JS = JS == "" ? DS.Tables[0].Rows[i]["JSDM"].ToString() : JS + "," + DS.Tables[0].Rows[i]["JSDM"].ToString();
                Name = Name == "" ? DS.Tables[0].Rows[i]["NAME"].ToString() : Name + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            JSStr = JS + "^" + Name;
        }
        return JSStr;
    }
    #endregion
}