﻿using System;
using System.Collections.Generic;
using System.Web;
using Sybase.Data.AseClient;
using System.Data;
using System.Collections;
using RMYH.BLL;
using System.Text;
using RMYH.DBUtility;

public partial class YSTB_ZBXD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_ZBXD));
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string JHFADM)
    {
        return GetDataList.GetDataListstring("10144672", "", new string[] { ":JHFADM"}, new string[] { JHFADM}, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetXMList(string trid, string id, string intimgcount,string MBDM)
    {
        return GetDataList.GetDataListstring("10144598", "", new string[] {":MBDM"}, new string[] {MBDM}, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetPFFJList(string trid, string id, string intimgcount, string JHFADM, string PFJHFADM)
    {
        return GetDataList.GetDataListstring("10145749", "", new string[] { ":JHFADM", ":PFJHFADM" }, new string[] { JHFADM, PFJHFADM }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144672");
    }
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="NewStr">新增字符串</param>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="DelStr">删除字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UpdateORDel(string NewStr, string UpStr, string DelStr, string FYBDStr)
    {
        int Flag = 0;
        decimal Value,WCFW;
        string XMDM = "", MBDM = "",DYDM="",ZYDM = "",JHFADM="",FA = "", JSDX = "", JLDW = "", MB = "", ZY = "", XM = "",JS="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        if (UpStr != "" || DelStr != "" || NewStr != "")
        {
            //新增记录
            if (NewStr != "")
            {
                string[] Col = NewStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMDM = ZD[0].ToString().Split('.')[0].ToString();
                    JSDX = ZD[1].ToString();
                    MBDM = ZD[2].ToString();
                    JLDW = ZD[3].ToString();
                    ZYDM = ZD[4].ToString();
                    DYDM = "5";
                    Value = Convert.ToDecimal(ZD[6].ToString());
                    WCFW = Convert.ToDecimal(ZD[7].ToString());
                    JHFADM = ZD[8].ToString();
                    string NewSql = "INSERT INTO TB_CWZBXD (JHFADM,MBDM,ZYDM,XMDM,JSDX,VALUE,JLDW,WCFW,DYDM)VALUES(@JHFADM,@MBDM,@ZYDM,@XMDM,@JSDX,@VALUE,@JLDW,@WCFW,@DYDM)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                      new AseParameter("@JHFADM", AseDbType.VarChar,40),
                      new AseParameter("@MBDM", AseDbType.VarChar, 40),
                      new AseParameter("@ZYDM", AseDbType.VarChar,40),
                      new AseParameter("@XMDM", AseDbType.VarChar,50), 
                      new AseParameter("@JSDX", AseDbType.VarChar,20),
                      new AseParameter("@VALUE", AseDbType.Decimal, 22),
                      new AseParameter("@JLDW", AseDbType.VarChar,40),
                      new AseParameter("@WCFW", AseDbType.Decimal, 22),
                      new AseParameter("@DYDM", AseDbType.VarChar,40)
                    };
                    New[0].Value = JHFADM;
                    New[1].Value = MBDM;
                    New[2].Value = ZYDM;
                    New[3].Value = XMDM;
                    New[4].Value = JSDX;
                    New[5].Value = Value;
                    New[6].Value = JLDW;
                    New[7].Value = WCFW;
                    New[8].Value = DYDM;
                    List.Add(New);
                    //获取修改费用变动装置的sql语句
                    GetUPdateFYBDSQL(ref ArrList, ref List, XMDM, JHFADM, FYBDStr);
                }
            }
            //更新记录
            if (UpStr != "")
            {
                string[] Col = UpStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMDM = ZD[0].ToString().Split('.')[0].ToString();
                    JSDX = ZD[1].ToString();
                    MBDM = ZD[2].ToString();
                    JLDW = ZD[3].ToString();
                    DYDM = "5";
                    Value = Convert.ToDecimal(ZD[5].ToString());
                    WCFW = Convert.ToDecimal(ZD[6].ToString());
                    XM = ZD[7].ToString();
                    MB = ZD[8].ToString();
                    ZY = ZD[9].ToString();
                    JS = ZD[10].ToString();
                    FA = ZD[11].ToString();
                    string UPSql = "UPDATE TB_CWZBXD SET MBDM=@MBDM,DYDM=@DYDM,XMDM=@XMDM,JSDX=@JSDX,VALUE=@VALUE,JLDW=@JLDW,WCFW=@WCFW WHERE XMDM=@XM AND JHFADM=@FA";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {               
                      new AseParameter("@MBDM", AseDbType.VarChar, 40),
                      new AseParameter("@DYDM", AseDbType.VarChar,40),
                      new AseParameter("@XMDM", AseDbType.VarChar,50), 
                      new AseParameter("@JSDX", AseDbType.VarChar,20),
                      new AseParameter("@VALUE", AseDbType.Decimal, 22),
                      new AseParameter("@JLDW", AseDbType.VarChar,40),
                      new AseParameter("@XM", AseDbType.VarChar,50), 
                      new AseParameter("@FA", AseDbType.VarChar,40),
                      new AseParameter("@WCFW", AseDbType.Decimal, 22)
                    };
                    UP[0].Value = MBDM;
                    UP[1].Value = DYDM;
                    UP[2].Value = XMDM;
                    UP[3].Value = JSDX;
                    UP[4].Value = Value;
                    UP[5].Value = JLDW;
                    UP[6].Value = XM;
                    UP[7].Value = FA;
                    UP[8].Value = WCFW;
                    List.Add(UP);

                    //获取修改费用变动装置的sql语句

                    //删除之前旧的XMDM存储的费用变动装置SQL
                    GetDelFYBDSQL(ref ArrList, ref List, XM, FA);
                    //新增费用变动装置
                    GetUPdateFYBDSQL(ref ArrList, ref List, XMDM, FA, FYBDStr);
                }
            }
            //删除记录
            if (DelStr != "")
            {
                string[] Col = DelStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMDM = ZD[0].ToString().Split('.')[0].ToString();
                    MBDM = ZD[1].ToString();
                    ZYDM = ZD[2].ToString();
                    JSDX = ZD[3].ToString();
                    JHFADM=ZD[4].ToString();
                    string DelSql = "DELETE FROM TB_CWZBXD WHERE XMDM=@XM AND JHFADM=@FA";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = 
                    {
                        new AseParameter("@XM", AseDbType.VarChar,50), 
                        new AseParameter("@FA", AseDbType.VarChar,40)
                    };
                    Del[0].Value = XMDM;
                    Del[1].Value = JHFADM;
                    List.Add(Del);

                    //删除之前旧的XMDM存储的费用变动装置SQL
                    GetDelFYBDSQL(ref ArrList, ref List, XMDM, JHFADM);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        else
        {
            Flag = -1;
        }
        return Flag;
    }
    /// <summary>
    /// 获取修改费用变动的SQL语句
    /// </summary>
    /// <param name="ArrList">SQL集合</param>
    /// <param name="List">参数集合</param>
    /// <param name="XM">项目代码</param>
    /// <param name="FA">计划方案代码</param>
    public void GetDelFYBDSQL(ref List<string> ArrList, ref ArrayList List, string XM, string FA)
    {
        //删除之前的费用变动装置
        string DelSql = "DELETE FROM TB_FJZYXZ WHERE XMDM=@XM AND JHFADM=@FA";
        ArrList.Add(DelSql);
        AseParameter[] Del = 
        {
            new AseParameter("@XM", AseDbType.VarChar,40), 
            new AseParameter("@FA", AseDbType.VarChar,40)
        };
        Del[0].Value = XM;
        Del[1].Value = FA;
        List.Add(Del);
    }
    /// <summary>
    /// 获取修改费用变动的SQL语句
    /// </summary>
    /// <param name="ArrList">SQL集合</param>
    /// <param name="List">参数集合</param>
    /// <param name="XM">项目代码</param>
    /// <param name="FA">计划方案代码</param>
    /// <param name="Str">修改的费用变动字符串</param>
    public void GetUPdateFYBDSQL(ref List<string> ArrList, ref ArrayList List, string XM, string FA, string Str)
    {
        string ZY = "";
        if (Str != "")
        {
            string[] Col = Str.Split(',');
            for (int i = 0; i < Col.Length; i++)
            {
                ZY = Col[i].ToString();
                string NewSql = "INSERT INTO TB_FJZYXZ (JHFADM,ZYDM,XMDM)VALUES(@JHFADM,@ZYDM,@XMDM)";
                ArrList.Add(NewSql);
                AseParameter[] New = {
                    new AseParameter("@JHFADM", AseDbType.VarChar,40),
                    new AseParameter("@ZYDM", AseDbType.VarChar,40),
                    new AseParameter("@XMDM", AseDbType.VarChar,40)
                };
                New[0].Value = FA;
                New[1].Value = ZY;
                New[2].Value = XM;
                List.Add(New);
            }
        }
    }
    /// <summary>
    /// 根据指标代码，获取指标名称
    /// </summary>
    /// <param name="XMDM">指标代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetXMMC(string XMDM)
    {
        string Name = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT XMDM DM,XMMC NAME FROM V_XMZD WHERE XMDM='" + XMDM + "'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            Name = DS.Tables[0].Rows[0]["DM"].ToString() + "." + DS.Tables[0].Rows[0]["NAME"].ToString();
        }
        return Name;
    }
    /// <summary>
    /// 获取核算中心相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetHSZXDM()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT  HSZXDM,HSZXMC FROM TB_HSZXZD WHERE NBBM LIKE (SELECT B.NBBM FROM TB_USER A,TB_HSZXZD B WHERE USER_ID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND A.CC_NO=B.HSZXDM AND YY='" + HttpContext.Current.Session["YY"].ToString() + "')  AND YY='" + HttpContext.Current.Session["YY"].ToString() + "'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["HSZXDM"].ToString() + "'>" + ds.Tables[0].Rows[i]["HSZXMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取方案类别相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ' ORDER BY XMDH_A");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["XMDH_A"].ToString() == "3" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取年份相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYY()
    {
        //string Str = "";
        //int YY = int.Parse(HttpContext.Current.Session["YY"].ToString())-3;
        //try
        //{
        //    for (int i = 0; i < 5; i++)
        //    {
        //        Str += (int.Parse(HttpContext.Current.Session["YY"].ToString()) == (YY + i)) ? "<option selected=true value='" + (YY + i).ToString() + "'>" + (YY + i).ToString() + "</option>" : "<option value='" + (YY + i).ToString() + "'>" + (YY + i).ToString() + "</option>";
        //    }
        //}
        //catch
        //{

        //}
        return HttpContext.Current.Session["YY"].ToString();
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetData(string FALX)
    {
        string Str = "";
        if (FALX == "2")
        {
            Str += "<option value='1'>第一季度</option>";
            Str += "<option value='2'>第二季度</option>";
            Str += "<option value='3'>第三季度</option>";
            Str += "<option value='4'>第四季度</option>";
        }
        else if (FALX == "3")
        {
            Str = "-1";
        }
        else
        {
            string MM = DateTime.Now.ToString("yyyy-MM").Split('-')[1].ToString();
            //月方案和其他方案
            for (int i = 1; i <=12; i++)
            {
                if (i < 10)
                {
                    //默认选中当前月份
                    if (("0" + i.ToString()) == MM)
                    {
                        Str += "<option selected=true value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
                else
                {
                    if (i.ToString() == MM)
                    {
                        Str += "<option selected=true value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
            }
        }
        return Str;
    }
    /// <summary>
    /// 输入月份获取月份的中文名称
    /// </summary>
    /// <param name="MM">月份</param>
    /// <returns></returns>
    public string GetMonth(string MM) 
    {
        string M = "";
        switch (MM)
        {
            case "1": M = "一月"; break;
            case "2": M = "二月"; break;
            case "3": M = "三月"; break;
            case "4": M = "四月"; break;
            case "5": M = "五月"; break;
            case "6": M = "六月"; break;
            case "7": M = "七月"; break;
            case "8": M = "八月"; break;
            case "9": M = "九月"; break;
            case "10": M = "十月"; break;
            case "11": M = "十一月"; break;
            case "12": M ="十二月"; break;
        }
        return M;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHFA(string FALX,string YY,string MM)
    {
        string Str = "",SQL="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //选择月方案类型
        if (FALX == "1")
        {
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='"+YY+"' AND NN='"+MM+"' AND FABS='1' AND YSBS='5'";
        }
        else if (FALX == "2")
        {
            //选择季度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND JD='" + MM + "' AND FABS='2' AND YSBS='5'";
        }
        else if (FALX == "3")
        {
            //选择年度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND FABS='3' AND YSBS='5'";
        }
        else
        {
            //选择其他方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='4' AND YSBS='5'";
        }
        DS=BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            Str += "<option value='" + DS.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + DS.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
        }
        return Str;
    }
    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetHZXMJson()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        string sql = "SELECT ZYDM,ZYMC,CASE FBDM WHEN '' THEN '-1' ELSE FBDM END PAR ,CCJB,YJDBZ FROM TB_JHZYML WHERE SYBZ='1' ORDER BY ZYNBBM";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            //递归拼接子节点的Json字符串
            RecursionChild(DS.Tables[0], "-1", ref Str);
        }
        return Str.ToString();
    }
    /// <summary>
    /// 递归查询子节点
    /// </summary>
    /// <param name="DT">数据集</param>
    /// <param name="PAR">父节点</param>
    /// <param name="Str">需返回的JSON字符串</param>
    private void RecursionChild(DataTable DT, string PAR, ref StringBuilder Str)
    {
        DataRow[] DR;
        //查询父节点为PAR的子节点
        DR = DT.Select("PAR='" + PAR + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["ZYDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["ZYMC"].ToString() + "\"");
                Str.Append(",");
                //自定义属性
                Str.Append("\"attributes\": {\"PAR\": \"" + dr["PAR"].ToString() + "\",\"CCJB\": \"" + dr["CCJB"].ToString() + "\",\"YJDBZ\": \"" + dr["YJDBZ"].ToString() + "\"}");
                //递归查询子节点
                RecursionChild(DT, dr["ZYDM"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 获取选择的节点
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCheckedNode(string XMDM)
    {
        string DM = "",XM="",FA="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string[] ZJ = XMDM.Split(',');
        XM = ZJ[0].ToString();
        FA = ZJ[1].ToString();
        string SQL = "SELECT ZYDM FROM TB_FJZYXZ WHERE JHFADM='"+FA+"' AND XMDM='"+XM+"'";
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            DM = DM == "" ? DS.Tables[0].Rows[i]["ZYDM"].ToString() : DM + "," + DS.Tables[0].Rows[i]["ZYDM"].ToString();
        }
        return DM;
    }
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="NewStr">新增字符串</param>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="DelStr">删除字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int SaveBDZY(string XMDM, string Str)
    {
        int Flag = 0;
        string  ZY = "", XM = "",FA="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        string[] ZJ = XMDM.Split(',');
        XM=ZJ[0].ToString();
        FA=ZJ[1].ToString();
        //删除之前的费用变动装置
        string DelSql = "DELETE FROM TB_FJZYXZ WHERE XMDM=@XM AND JHFADM=@FA";
        ArrList.Add(DelSql);
        AseParameter[] Del = 
        {
            new AseParameter("@XM", AseDbType.VarChar,40), 
            new AseParameter("@FA", AseDbType.VarChar,40)
        };
        Del[0].Value = XM;
        Del[1].Value = FA;
        List.Add(Del);
        if (Str != "")
        {
            string[] Col = Str.Split(',');
            for (int i = 0; i < Col.Length; i++)
            {
                ZY=Col[i].ToString();
                string NewSql = "INSERT INTO TB_FJZYXZ (JHFADM,ZYDM,XMDM)VALUES(@JHFADM,@ZYDM,@XMDM)";
                ArrList.Add(NewSql);
                AseParameter[] New = {
                    new AseParameter("@JHFADM", AseDbType.VarChar,40),
                    new AseParameter("@ZYDM", AseDbType.VarChar,40),
                    new AseParameter("@XMDM", AseDbType.VarChar,40)
                };
                New[0].Value = FA;
                New[1].Value = ZY;
                New[2].Value = XM;
                List.Add(New);
            }
        }
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 获取预算申报的计划方案代码
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHFADM(string YY,string FADM)
    {
        string JHFADM = "-1";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string FABS = BLL.GetMBZQByJHFADM(FADM);
        string sql = "SELECT JHFADM FROM TB_JHFA WHERE YY='" + YY + "' AND NN='01'  AND FABS='" + FABS + "' AND YSBS='4' AND DELBZ='1'";
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            JHFADM = DS.Tables[0].Rows[0]["JHFADM"].ToString();
        }
        return JHFADM;
    }
    /// <summary>
    /// 更新财务指标批复值
    /// </summary>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Update(string UpStr, string JHFADM)
    {
        int Flag = 0, Count;
        decimal Value;
        string XMDM = "", YY = "", HSZXDM = "", FABS = "", Result = "";
        try
        {
            ArrayList List = new ArrayList();
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            List<String> ArrList = new List<String>();
            if (UpStr != "")
            {
                string[] Col = UpStr.Split(',');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMDM = ZD[0].ToString();
                    Value = Convert.ToDecimal(ZD[1].ToString());
                    string UPSql = "UPDATE TB_CWZBXD SET VALUE=@VALUE WHERE XMDM=@XM AND JHFADM=@FA";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {               
                    new AseParameter("@VALUE", AseDbType.Decimal, 22),
                    new AseParameter("@XM", AseDbType.VarChar,50), 
                    new AseParameter("@FA", AseDbType.VarChar,40)
                };
                    UP[0].Value = Value;
                    UP[1].Value = XMDM;
                    UP[2].Value = JHFADM;
                    List.Add(UP);
                }
                //修改财务指标批复值
                BLL.ExecuteSql(ArrList.ToArray(), List);
            }
            DataSet DS = BLL.getDataSet("TB_JHFA", "HSZXDM,FABS,YY", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                YY = DS.Tables[0].Rows[0]["YY"].ToString();
                FABS = DS.Tables[0].Rows[0]["FABS"].ToString();
                HSZXDM = DS.Tables[0].Rows[0]["HSZXDM"].ToString();
            }
            AseParameter[] parameters = {
                new AseParameter("@P_JHFADM", AseDbType.VarChar,40) ,
                new AseParameter("@P_YY", AseDbType.VarChar,4) ,
                new AseParameter("@P_NN", AseDbType.VarChar,2),
                new AseParameter("@P_FABS", AseDbType.VarChar,2) ,
                new AseParameter("@P_HSZXDM", AseDbType.VarChar,40) ,
                new AseParameter("@P_OUT", AseDbType.VarChar,200)
            };
            // 设置参数类型
            parameters[0].Value = JHFADM;
            parameters[1].Value = YY;
            parameters[2].Value = "01";
            parameters[3].Value = FABS;
            parameters[4].Value = HSZXDM;
            parameters[5].Direction = ParameterDirection.Output;
            //执行存储过程
            Flag = DbHelperOra.RunProcedure("FUN_CWFJYS", parameters, out Count);
            if (Flag == -1)
            {
                Result = Flag.ToString() + "@" + parameters[5].Value.ToString();
            }
            else
            {
                Result = Flag.ToString();
            }
        }
        catch (Exception E)
        {
            Result = "-2@" + E.Message;
        }
        return Result;
    }
    /// <summary>
    /// 按年份查询数据，如果本年没有数据，则将上一年的数据写入本年，VALUES除外
    /// </summary>
    /// <param name="YY">年份</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int InsertLastZB(string JHFADM)
    {
        int Flag = 0;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Flag = BLL.ExecuteSql("INSERT INTO TB_CWZBXD(JHFADM,MBDM,ZYDM,XMDM,JSDX,DYDM,VALUE) SELECT  '" + JHFADM + "',MBDM,ZYDM,XMDM,JSDX,DYDM,0 FROM TB_CWZBXD WHERE JHFADM IN(SELECT MAX(JHFADM) FROM TB_CWZBXD)");
        return Flag;
    }
    #endregion
}