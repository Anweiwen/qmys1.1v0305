﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;
using System.Collections;
using Sybase.Data.AseClient;

public partial class YSTB_HZXMSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_HZXMSZ));
    }
    #region
    /// <summary>
    /// 获取汇总项目的json字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetHZXMJson()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        string sql = "SELECT XMDM,XMMC,CCJB,YJDBZ,CASE PAR WHEN '' THEN '0' ELSE PAR END PAR,XMBM FROM TB_XMXX WHERE XMLX<>'13' ORDER BY XMBM";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        Str.Append("");
        if (DS.Tables[0].Rows.Count > 0)
        {
            //递归拼接子节点的Json字符串
            RecursionChild(DS.Tables[0],"0", ref Str);
        }
        return Str.ToString();
    }
    /// <summary>
    /// 递归查询子节点
    /// </summary>
    /// <param name="DT">数据集</param>
    /// <param name="PAR">父节点</param>
    /// <param name="Str">需返回的JSON字符串</param>
    private void RecursionChild(DataTable DT,string PAR,ref StringBuilder Str)
    {
        DataRow[] DR;
        //查询父节点为PAR的子节点
        DR = DT.Select("PAR='" + PAR + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString()=="")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["XMDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["XMMC"].ToString() + "\"");
                Str.Append(",");
                //自定义属性
                Str.Append("\"attributes\": {\"PAR\": \"" + dr["PAR"].ToString() + "\",\"XMBM\": \"" + dr["XMBM"].ToString() + "\",\"CCJB\": \"" + dr["CCJB"].ToString() + "\",\"YJDBZ\": \"" + dr["YJDBZ"].ToString() + "\"}");
                //递归查询子节点
                RecursionChild(DT,dr["XMDM"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 获取审批项目
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetHZXM()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT  XMDM,XMMC FROM TB_XMXX WHERE XMLX='13' AND YJDBZ='1' ORDER BY XMDM");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDM"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 插入汇总项目关联子项目设置表
    /// </summary>
    /// <param name="XM">项目代码字符串</param>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int InsertXM(string XM,string XMDM)
    {
        int Flag = 0;
        string sql="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        //先删除之前保存的历史记录
        sql = "DELETE FROM TB_YSJGFSZ WHERE HZXMDM=@XMDM";
        ArrList.Add(sql);
        AseParameter[] Del = {
            new AseParameter("@XMDM", AseDbType.VarChar, 50)
        };
        Del[0].Value = XMDM;
        List.Add(Del);
        if (XM != "")
        {
            //添加模板的前置模板
            string[] Col = XM.Split(',');
            for (int i = 0; i < Col.Length; i++)
            {
                sql = "INSERT INTO TB_YSJGFSZ(HZXMDM,BHXMDM) VALUES(@HZXMDM,@BHXMDM)";
                ArrList.Add(sql);
                AseParameter[] Data = {
                    new AseParameter("@HZXMDM", AseDbType.VarChar, 50),
                    new AseParameter("@BHXMDM", AseDbType.VarChar, 40)
                };
                Data[0].Value = XMDM;
                Data[1].Value = Col[i].ToString();
                List.Add(Data);
            }
        }
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 获取选择的节点
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCheckedNode(string XMDM) {
        string DM = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM='"+XMDM+"'";
        DS=BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            DM = DM == "" ? DS.Tables[0].Rows[i]["BHXMDM"].ToString() : DM + "," + DS.Tables[0].Rows[i]["BHXMDM"].ToString();
        }
        return DM;
    }
    #endregion
}