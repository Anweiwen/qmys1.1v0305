﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class YSTB_CWZBXD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_CWZBXD));
            //遍历树节点
            gettreelist();
            //展开树的节点
            TreeView1.ExpandAll();
        }
    }
    /// <summary>
    /// 查询用户和用户组的相关信息
    /// </summary>
    public void gettreelist()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        //查询项目分类的相关信息
        strSql.Append("SELECT ZYDM,ZYMC,CASE FBDM WHEN '' THEN '-1' ELSE FBDM END PAR ,CCJB,YJDBZ FROM TB_JHZYML WHERE SYBZ='1' ORDER BY ZYNBBM");
        DS = bll.Query(strSql.ToString());
        //遍历树
        CreateTreeViewRecursive(TreeView1.Nodes, DS.Tables[0], "-1");
    }
    /// <summary>
    /// 对用户组和用户树进行遍历
    /// </summary>
    /// <param name="tree">树节点结合</param>
    /// <param name="table">需要遍历的数据集</param>
    /// <param name="par">父节点</param>
    public void CreateTreeViewRecursive(TreeNodeCollection tree, DataTable table, string par)
    {
        DataRow[] row;
        row = table.Select("PAR='" + par + "'");
        TreeNode node;
        foreach (DataRow dr in row)
        {
            node = new TreeNode();
            node.Text = dr["ZYDM"].ToString();
            node.Value = dr["ZYMC"].ToString();
            row = table.Select("PAR='" + node.Value + "'");
            node.Expanded = false;
            if (row.Length == 0)
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            else
            {
                node.ImageUrl = "~/images/minus.gif";
            }
            node.NavigateUrl = "javascript:$('#" + HidZYDM.ClientID + "').val('" + dr["ZYDM"].ToString() + "');CWZBXD('" + dr["XMFL"].ToString() + "')";
            tree.Add(node);
            CreateTreeViewRecursive(node.ChildNodes, table, node.Value);
        }
    }
}