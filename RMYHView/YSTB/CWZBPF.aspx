﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CWZBPF.aspx.cs" Inherits="YSTB_CWZBPF" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
  <%--  核算中心：<select id="SelHSZX"  style="width:120px; display:none"></select>&nbsp;--%>
  <%--<select id="SelYY" onchange="BindJHFA()"  style="width:80px;"></select>&nbsp;--%>
  <table>
  <tr>
     <td>
         方案类别：<select id="SelFALX" disabled="disabled" onchange="ChageFALX()"  style="width:80px;"></select>&nbsp;
    </td>
    <td>
         年份：<input  type="text" id="TxtYY" class="Wdate"  onclick="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:YYChanged()})"  style="width:80px"  />&nbsp;
    </td>
    <td>
        <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label> 
        <select id="SelData" onchange="BindJHFA()"  style="width:90px;"></select>&nbsp;
    </td>
    <td>
        方案名称： <select id="SelJHFA" onchange="getlist('','','')"  style="width:150px;"></select>&nbsp;
    </td>
<%--    <td>
        <input type="button" class="button5" value="查询" style="width:50px; " onclick ="getlist('','','')" />&nbsp;
    </td>--%>
    <td>
       <input type="button" class="button5" value="添加" style="width:50px; " onclick ="jsAddData()" />&nbsp;
    </td>
    <td>
       <input type="button" class="button5" value="删除" style="width:50px;" onclick="Del()" />&nbsp;
    </td>
    <td>
       <input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:70px;"/>&nbsp;
    </td>
    <td>
       <input id="Button2" class="button5"  type="button" value="保存" style="width:50px;"  onclick="Saves()"/>&nbsp;
    </td>
   <%-- <td>
       <input id="Button1" class="button5"  type="button" value="保存费用变动分摊装置" style="width:150px;"  onclick="SaveBDZY()"/>
    </td>--%>
     <td>
       <input id="Button3" class="button5"  type="button" value="批复自动分解" style="width:150px;"  onclick="WinFJ(true, 0)"/>
    </td>
</tr>
</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style=" width:100%; height:100%;border: solid 1px #9ACB34; border-collapse:collapse">
    <tr>
       <td style ="width :100%;vertical-align: top;height:100%;border: solid 1px #9ACB34;">
            <div style="width:100%;height: 450px;" id="divListView"></div>
             <div id="WinXM" class="easyui-window" title="指标项目选取" closed="true" style="width:950px;height:430px;padding:20px;text-align: center; ">
                <div style=" text-align:left">
                   <%-- <input type="button" value="展开" onclick="TreeZK(1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                    <input type="button" value="收起" onclick="TreeSQ(1)" class="button5" style="width:60px" />&nbsp;&nbsp;      --%>
                    <input type="button" value="确定" onclick="Win(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                    <input type="button" value="退出" onclick="Win(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
                    <br /><br />
                    <div id="XMXQ"></div>
                </div>
            </div>
       </td>
       <td style="width:300px; height:100%;border: solid 1px #9ACB34;">
             <div style=" width:300px;height:100%;overflow: auto">
                 <div style="height:28px; width:298px; border: solid 1px #9ACB34; background-color: #77ADDB;  vertical-align:middle; text-align: left;">
                     <table>
                         <tr>
                              <td>费用变动分摊装置列表</td>
                              <td style=" width:50px; text-align:right;"><img src="../Content/themes/icons/redo.png" alt="全部展开" style=" cursor:pointer" onclick="TreeZD(true)" /></td>
                              <td style=" width:50px; text-align:right"><img src="../Content/themes/icons/undo.png" alt="全部收起" style=" cursor:pointer" onclick="TreeZD(false)" /></td>
                         </tr>
                     </table>
                 </div>
                 <br />
                 <ul id="tt"  class="easyui-tree" data-options="checkbox:true"></ul>
             </div>
      </td>
    </tr>
  </table> 
    <div id="WinPFFJ" class="easyui-window" title="批复自动分解" closed="true" style="width:750px;height:430px;padding:20px;text-align: center; ">
        <div style=" text-align:left">
            <input type="button" value="确定" onclick="WinFJ(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
            <input type="button" value="退出" onclick="WinFJ(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            <br /><br />
            <div id="DivFJ"></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input type="hidden" id="hidindexid" value="XM,FA" />
<input type="hidden"   id="hidcheckid" />
<input  type="hidden" id="hidNewLine"/>
<input type="hidden" id="HidZYDM" value="0" />
<input  type="hidden" id="HidXM"/>
<input  type="hidden" id="HidSelectedXM"/>
<input  type="hidden" id="HidSelectedZY"/>
<input type="hidden" id="HidTrID" />
<input type="hidden" id="HidJHFADM" />
<input type="hidden" id="HidMBDM" />
<script type="text/javascript">
    var XMOBJ;
    function getlist(objtr, objid, intimagecount) {

        var rtnstr = YSTB_CWZBPF.LoadList(objtr, objid,intimagecount,$("#SelJHFA").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divListView").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
        var tr = $("#divListView tr");
        if (tr.length == 1) 
        {
            if ($("#SelJHFA").val() != "" && $("#SelJHFA").val() != null) {
                InsertLastZB();
            }
        }
        else 
        {
            if (tr.length > 0) {
                LoadName();
            }
        }
    }
    //本年没有数据，将上年的数据插入本年
    function InsertLastZB() {
        var rtn = YSTB_CWZBPF.InsertLastZB($("#SelJHFA").val()).value;
        if (rtn > 0) {
            getlist('', '', '');
        }
    }
    function GetXMList(objtr, objid, intimagecount) {
        var rtnstr = YSTB_CWZBPF.GetXMList(objtr, objid, intimagecount, $("#HidMBDM").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("XMXQ").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
        $("#XMXQ td[name$='tdXMBM']").attr("style", "text-align:left");
    }
    function YYChanged() {
        BindJHFA();
        getlist('', '', '');
    }
    function GetPFFJList(objtr, objid, intimagecount) {
        var rtnstr = YSTB_CWZBPF.GetPFFJList(objtr, objid, intimagecount, $("#HidJHFADM").val(),$("#SelJHFA").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "") {
//            debugger;
//            var Reg="</tr>";
//            var Len1 = rtnstr.length;
//            var Len2 = rtnstr.replace(/"+Reg+"/g, '').length;
//            if ((Len1 - Len2) / 5 > 1) {
                var LastTr = "<tr id='99999999'><td width='60px'></td><td width='50px'></td><td style=\"display:none\"></td><td>总计：</td><td name=tdOV></td><td style=\"text-align:left\"  name=tdNEWVALUE></td><tr>";
                var Len = rtnstr.indexOf("</table>");
                rtnstr = rtnstr.substring(0, Len) + LastTr + rtnstr.substring(Len);
                document.getElementById("DivFJ").innerHTML = rtnstr;
                GetZJValue();
//            } else {
//                document.getElementById("DivFJ").innerHTML = rtnstr;
//            }
        }
    }
    function LoadZYML() {
        var rtnstr = YSTB_CWZBPF.GetHZXMJson().value;
        if (rtnstr != "" && rtnstr != null) {
            var Data = eval("(" + rtnstr + ")");
            $("#tt").tree("loadData", Data);
        }
        $("#tt").tree({
            lines: true
//            cascadeCheck: false,
//            onCheck: function (node, checked) {
//                //只有当节点被选中时，判断当前结点的父辈节点、子节点没有被选中
//                if (checked == true) {
//                    if (node.attributes.PAR != "-1") {
//                        //判断其子节点没有被选中的，如果有的话，则父节点和子节点不能同时被选中。
//                        if (node.attributes.YJDBZ != "1") {
//                            var c = RecursionChild(node);
//                            if (c == false) {
//                                alert("父节点和子节点不能同时被选中!");
//                                $("#tt").tree("uncheck", node.target);
//                                return;
//                            }
//                        }
//                        //如果是支节点，则判断其父节点没有被选中的，如果有，则该支节点和父节点不能同时选中
//                        var Flag = RecursionPar(node);
//                        if (Flag == false) {
//                            alert("父节点和子节点不能同时被选中!");
//                            $("#tt").tree("uncheck", node.target);
//                            return;
//                        }
//                    } else {
//                        var H = $("#tt").tree("getChecked");
//                        if (H.length > 1) {
//                            alert("父节点和子节点不能同时被选中!");
//                            $("#tt").tree("uncheck", node.target);
//                            return;
//                        }
//                    }
//                }
//            }
        });
    }
    //递归当前结点的父辈结点有没有被选中的
    //返回true表示没有被选中的；false表示有被选中的父辈节点
    function RecursionPar(node) 
    { 
        if(node.attributes.PAR != "-1") 
        {
            var n = $('#tt').tree("find", node.attributes.PAR);
            if (n.checked == true) {
                return false;
            }
            else 
            {
                var p = $('#tt').tree("find", node.attributes.PAR);
                var Flag = RecursionPar(p);
                if (Flag == false) {
                    return false;
                }
            }
            return true;
        }
        else {
            if (node.checked == true) {
                return false;
            } else {
                return true;
            }
        }
    }
    //递归当前结点的子节点有没有被选中的
    //返回true表示没有被选中的；false表示有被选中的父辈节点
    function RecursionChild(node) 
    {
        var c = node.children;
        if (c != undefined) 
        {
            for (var i = 0; i < c.length; i++) 
            {
                if (c[i]._checked == true) 
                {
                    return false;
                }
                else 
                {
                    if (c[i].attributes.YJDBZ != "1") 
                    {
                        var Flag=RecursionChild(c[i]);
                        if (Flag == false) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        else 
        {
           return true;
        }
    }
    function LoadName() {
        var tr = $("#divListView tr");
        for (var i = 1; i < tr.length; i++) {
            var trid = tr[i].id;
            var XM = $("#" + trid + " input[name=tselXMDM]");
            XM.val(YSTB_CWZBPF.GetXMMC(XM.val()).value);
        }
    }
    function TreeZD(Flag) {
        if (Flag == true) {
            $("#tt").tree("expandAll");
        } else {
            $("#tt").tree("collapseAll");
        }
    }
    function ChageFALX() {
        BindData();
        BindJHFA();
    }
//    function BindHSZX() {
//        var rtn = YSTB_CWZBPF.GetHSZXDM().value;
//        $("#SelHSZX").html(rtn);
//    }
    function BindFALB() {
        var rtn = YSTB_CWZBPF.GetFALB().value;
        $("#SelFALX").html(rtn);
    }
    function BindYY() {
        var rtn = YSTB_CWZBPF.GetYY().value;
        $("#TxtYY").val(rtn);
    }
    function BindData() {
        var rtn = YSTB_CWZBPF.GetData($("#SelFALX").val()).value;
        if (rtn == "-1") {
            $("#<%=LabMM.ClientID%>").attr("style", "display:none");
            $("#SelData").attr("style", "display:none");
        }
        else {
            if ($("#SelFALX").val() == "2") {
                $("#<%=LabMM.ClientID%>").html("季度：");
            }
            else 
            {
                $("#<%=LabMM.ClientID%>").html("月份：");
            }
            $("#<%=LabMM.ClientID%>").attr("style", "display:display");
            $("#SelData").attr("style", "display:display");
            document.getElementById("SelData").options.length = 0
            $("#SelData").html(rtn);
        }
        
    }
    function BindJHFA() {
        var MM = "";
        if ($("#SelFALX").val() != "3") {
            MM = $("#SelData").val();
        }
        var rtn = YSTB_CWZBPF.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM).value;
        $("#SelJHFA").html(rtn);
    }
    function jsAddData() {
        if ($("#SelJHFA").val() == "" || $("#SelJHFA").val() == null) {
            alert("方案名不能为空！");
            return;
        }
//        if ($("#HidZYDM").val()=="") {
//            alert("请选择作业目录下的子节点！");
//            return;
//        }
        //选中当前项目指标和计划方案代码所对应的作业代码
        //清除之前选中的项
        var R = $("#tt").tree("getRoots");
        for (var i = 0; i < R.length; i++) {
            $("#tt").tree("uncheck", R[i].target);
        }
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(YSTB_CWZBPF.AddData().value);
        var Len = $("#divListView tr").length;
        $("#divListView table").append($("#hidNewLine").val().replace('{000}', Len).replace('{000}', Len));
        var tr = $("#divListView tr");
        if (tr.length > 2) {
            var T = tr[tr.length - 2];
            var trid = T.id;
            //            var MBDM = $("#" + trid + " select[name=selMBDM]").val();
            //            $("#tr" + Len + " select[name=selMBDM]").val(MBDM);
            var WCFW = $("#" + trid + " INPUT[name^=txtWCFW]").val();
            $("#tr" + Len + " INPUT[name^=txtWCFW]").val(WCFW);

        }
    }
    function onselects(obj) {
        //表示当前操作的是指标项目列表
        if ($("#HidXM").val() == "-1") {
            $("#XMXQ tr[name^='trdata']").css("backgroundColor", "");
            var trid = obj.parentNode.parentNode.id;
            $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            $("#HidSelectedXM").val($("#" + trid).find("td[name^='tdXMDM']").html() + "." + $("#" + trid).find("td[name^='tdXMMC']").html());
        }
        else 
        {
            var MBDM = "", XMDM = "", JSDX = "",JHFADM="";
            $("tr[name^='trdata']").css("backgroundColor", "");
            var trid = obj.parentNode.parentNode.id;
            $("#HidTrID").val(trid);
            $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            if ($("tr[id=" + trid + "] img[src$='new.jpg']").length < 1) 
            {
                //获取联合主键的值XMDM,MBDM,JSDX,JHFADM
                var ZJ = $("#hidindexid").val().split(',');
                XMDM = $("tr[id=" + trid + "] input[name^=tsel" + ZJ[0].toString() + "]").val().toString().split('.')[0].toString();
                JHFADM = $("#" + trid).find("td[name^=td" + ZJ[1].toString() + "]").html();
                $("#hidcheckid").val( XMDM + "," + JHFADM);

                //选中当前项目指标和计划方案代码所对应的作业代码
                //清除之前选中的项
                var R = $("#tt").tree("getRoots");
                for (var i = 0; i < R.length; i++) {
                    $("#tt").tree("uncheck", R[i].target);
                }
                //选中对应的作业代码
                var rtn = YSTB_CWZBPF.GetCheckedNode($("#hidcheckid").val()).value;
                if (rtn != "" && rtn != null) 
                {

                    var DM = rtn.split(',');
                    for (var i = 0; i < DM.length; i++) 
                    {
                        var node = $('#tt').tree("find", DM[i].toString());
                        $("#tt").tree("check", node.target);
                    }
                    TreeZD(true);
                }
            }
            else {
                $("#hidcheckid").val("");
            }
        }
    }
    function selectvalues(obj, id) {
        XMOBJ = obj;
        var trid = obj.parentNode.parentNode.id;
        $("#HidMBDM").val($("tr[id=" + trid + "] select[name^=selMBDM]").val());
        Win(true, 0);
    }
    //Flag=true，表示打开窗体；等于false，表示关闭窗体
    //BZ等于1表示点击“确定”按钮；等于-1，表示点击“关闭”按钮
    function Win(Flag, BZ) {
        if (Flag == true) 
        {
            $("#HidXM").val("-1");
            $("#WinXM").window('open');
            GetXMList('', '', '');
        }
        else 
        {
            if (BZ == 1) 
            {
                if ($("#HidSelectedXM").val() != "") 
                {
                    XMOBJ.parentNode.childNodes[0].value = $("#HidSelectedXM").val();
                    if ($("#" + XMOBJ.parentNode.parentNode.id + " img[name=readimage]")[0].src.indexOf("new.jpg") == -1) 
                    {
                        $("#" + XMOBJ.parentNode.parentNode.id + " img[name=readimage]").attr("src", "../Images/Tree/edit.jpg");
                    }
                    $("#HidSelectedXM").val("");
                    $("#HidXM").val("");
                }
            }
            XMOBJ = null;
            $("#WinXM").window('close');
        }
    }
    function WinFJ(Flag, BZ) 
    {
        if (Flag == true) 
        {
            $("#WinPFFJ").window('open');
            var rtn = YSTB_CWZBPF.GetJHFADM($("#TxtYY").val(),$("#SelJHFA").val()).value;
            if (rtn == "-1") {
                alert("请先做当前年的申请预算!");
                return;
            }
            else 
            {
                $("#HidJHFADM").val(rtn);
                GetPFFJList('', '', '');
            }
        }
        else 
        {
            if (BZ == 1) 
            {
                var E = $("#DivFJ img[src$='edit.jpg']");
//                if (E.length == 0) {
//                    alert("您没有要分解的项！");
//                    return;
//                }
                var UpdateStr = "", XMDM = "",value="";
                //拼接修改的字符串
                for (var i = 0; i < E.length; i++) 
                {
                    var trid = E[i].parentNode.parentNode.id;
                    if (trid != "") 
                    {
                        XMDM = $("#" + trid).find("td[name^=tdXMDM]").html();
                        value = $("tr[id=" + trid + "] input[name^=txtNEWVALUE]").val();
                        if (UpdateStr == "") 
                        {
                            UpdateStr = XMDM + "|" + value;
                        }
                        else 
                        {
                            UpdateStr = UpdateStr + "," + XMDM + "|" + value;
                        }
                    }
                }
                var rtn = YSTB_CWZBPF.Update(UpdateStr, $("#SelJHFA").val()).value;
                if (rtn != "" && rtn != null) 
                {
                    var result = rtn.split("@");
                    if (result[0].toString() == "-1" || result[0].toString() == "-2") 
                    {
                        alert(result[1].toString());
                        getlist('', '', '');
                    }
                    else 
                    {
                        alert("分解成功");
                        getlist('', '', '');
                    }
                } 
                else 
                {
                    alert("分解失败！");
                    return;
                }
            }
            $("#HidJHFADM").val("");
            $("#WinPFFJ").window('close');
        }
    }
    function WinXMCloseEvent() {
        $("#WinXM").window({
            modal: true,
            closable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false
        });
    }
    function TreeZK(Flag) {
        var tr = $("#XMXQ img[src$='tplus.gif']");
        for (var i = 0; i < tr.length; ) {
            var trid = tr[i].parentNode.parentNode.id;
            ToExpand(tr[i], trid);
            i = 0;
            tr = $("#XMXQ img[src$='tplus.gif']");
        }
    }
    function TreeSQ(Flag) {
        var tr = $("#XMXQ img[src$='tminus.gif']");
        for (var i = 0; i < tr.length; i++) {
            var trid = tr[i].parentNode.parentNode.id;
            var PAR = $("#"+trid).find("td[name^='tdPAR']").html();
            if (PAR == 0) {
                ToExpand(tr[i], trid);
            }
        }
    }
    function ToExpand(obj, id) {
        var trs;
        if (obj.src.indexOf("tminus.gif") > -1) {
            obj.src = "../Images/Tree/tplus.gif";
            trs = $("#XMXQ tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lminus.gif") > -1) {
            obj.src = "../Images/Tree/lplus.gif";
            trs = $("#XMXQ tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lplus.gif") > -1) {
            obj.src = "../Images/Tree/lminus.gif";
            trs = $("#XMXQ tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                }
            }
        }
        else if (obj.src.indexOf("tplus.gif") > -1) {
            obj.src = "../Images/Tree/tminus.gif";
            trs = $("#XMXQ tr");
            var istoload = true;
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                    istoload = false;
                }
            }
            if (istoload == true) {
                GetXMList(id, $("tr[id=" + id + "] td[name$='tdXMDM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                trs = $("#XMXQ tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
        }
    }
    function Saves() {
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value.trim() == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                alert(strnullmessage);
                return;
            }
        }
        var E = $("#divListView img[src$='edit.jpg']");
        var D = $("#divListView img[src$='delete.gif']");
        var N = $("#divListView img[src$='new.jpg']");
        var NewStr = "", UpdateStr = "", DelStr = "", XMDM = "", MBDM = "", DYDM = "",  WCFW= "", JSDX = "", JLDW = "", Value = "", XM = "", MB = "", ZY = "",JS="",FA="",ZYDM = "";
        if (E.length == 0 && D.length == 0 && N.length == 0) {
            var ZJ = $("#hidcheckid").val();
            if (ZJ == "" || ZJ == null) 
            {
                alert("您没有新增、修改或者删除的项！");
                return;
            }
            else 
            {
                SaveBDZY();
                return;
            }
        }
        else 
        {
            if (N.length > 0) 
            {
                if ($("#SelJHFA").val() == "" || $("#SelJHFA").val() == null) 
                {
                    alert("方案名不能为空！");
                    return;
                }
            }
        }
        //拼接新增的相关信息字符串
        for (var i = 0; i < N.length; i++) {
            var trid = N[i].parentNode.parentNode.id;
            if (trid != "") {
                XMDM = $("tr[id=" + trid + "] input[name^=tselXMDM]").val();
                JSDX = $("tr[id=" + trid + "] select[name^=selJSDX]").val();
                MBDM = $("tr[id=" + trid + "] select[name^=selMBDM]").val();
                JLDW = $("tr[id=" + trid + "] select[name^=selJLDW]").val();
                DYDM = $("tr[id=" + trid + "] select[name^=selDYDM]").val();
                Value = $("#" + trid + " input[name^='txtVALUE']").val();
                WCFW = $("#" + trid + " input[name^='txtWCFW']").val();
                if (NewStr == "") {
                    NewStr = XMDM + "|" + JSDX + "|" + MBDM + "|" + JLDW + "|" + $("#HidZYDM").val() +"|"+DYDM+"|" + Value+"|"+WCFW+"|"+$("#SelJHFA").val();
                }
                else {
                    NewStr = NewStr + "," + XMDM + "|" + JSDX + "|" + MBDM + "|" + JLDW + "|" + $("#HidZYDM").val() + "|" + DYDM + "|" + Value + "|" + WCFW + "|" + $("#SelJHFA").val();
                }
            }
        }
        //拼接修改的字符串
        for (var i = 0; i < E.length; i++) {
            var trid = E[i].parentNode.parentNode.id;
            if (trid != "") {
                XMDM = $("tr[id=" + trid + "] input[name^=tselXMDM]").val();
                JSDX = $("tr[id=" + trid + "] select[name^=selJSDX]").val();
                MBDM = $("tr[id=" + trid + "] select[name^=selMBDM]").val();
                JLDW = $("tr[id=" + trid + "] select[name^=selJLDW]").val();
                DYDM = $("tr[id=" + trid + "] select[name^=selDYDM]").val();
                Value = $("#" + trid + " input[name^='txtVALUE']").val();
                WCFW = $("#" + trid + " input[name^='txtWCFW']").val();
                FA = $("#" + trid).find("td[name^=tdFA]").html();
                XM = $("#" + trid).find("td[name^=tdXM]").html();
                MB = $("#" + trid).find("td[name^=tdMB]").html();
                JS = $("#" + trid).find("td[name^=tdJS]").html();
                ZY = $("#" + trid).find("td[name^=tdZY]").html();
                if (UpdateStr == "") {
                    UpdateStr = XMDM + "|" + JSDX + "|" + MBDM + "|" + JLDW + "|" + DYDM + "|" + Value + "|" + WCFW + "|" + XM + "|" + MB + "|" + ZY+"|"+JS+"|"+FA;
                }
                else {
                    UpdateStr = UpdateStr + "," + XMDM + "|" + JSDX + "|" + MBDM + "|" + JLDW + "|" + DYDM + "|" + Value + "|" + WCFW + "|" + XM + "|" + MB + "|" + ZY + "|" + JS + "|" + FA;
                }
            }
        }
        //拼接删除的相关信息的字符串
        for (var j = 0; j < D.length; j++) {
            var trid = D[j].parentNode.parentNode.id;
            if (trid != "") {
                FA = $("#" + trid).find("td[name^=tdFA]").html();
                XM = $("#" + trid).find("td[name^=tdXM]").html();
                MB = $("#" + trid).find("td[name^=tdMB]").html();
                JS = $("#" + trid).find("td[name^=tdJS]").html();
                ZY = $("#" + trid).find("td[name^=tdZY]").html();

                if (DelStr == "") {
                    DelStr = XM + "|" + MB + "|" + ZY + "|" + JS + "|" + FA
                }
                else {
                    DelStr = DelStr + "," + XM + "|" + MB + "|" + ZY + "|" + JS + "|" + FA
                }
            }
        }
        if (E.length > 0 || N.length > 0) {
            var H = $("#tt").tree("getChecked");
            for (var i = 0; i < H.length; i++) {
                ZYDM = ZYDM == "" ? H[i].id.toString() : ZYDM + "," + H[i].id.toString();
            }
        }
        var rtn = YSTB_CWZBPF.UpdateORDel(NewStr, UpdateStr, DelStr,ZYDM).value;
        if (rtn > 0) {
            alert("保存成功！");
            getlist('', '', '');
            //清除之前选中的项
            var R = $("#tt").tree("getRoots");
            for (var i = 0; i < R.length; i++) {
                $("#tt").tree("uncheck", R[i].target);
            }
            $("#hidcheckid").val("");
            return;
        }
        else {
            alert("保存失败！");
            return;
        }
    }
    function Del() {
        if ($("#HidTrID").val() != "")
            $("#" + $("#HidTrID").val() + " img[name=readimage]").attr("src", "../Images/Tree/delete.gif");
    }
    function Cdel() {
        if ($("#HidTrID").val() != "")
            $("#" + $("#HidTrID").val() + " img[name=readimage]").attr("src", "../Images/Tree/noexpand.gif");
    }
    function SaveBDZY() {
//        if ($("#SelJHFA").val() == "") {
//            alert("计划方案代码不能为空!");
//            return;
//        }
        if ($("#hidcheckid").val() == "") {
            alert("请选择指标项目项!");
            return;
        }
        var ZYDM = "";
        var H = $("#tt").tree("getChecked");
        for (var i = 0; i < H.length; i++) {
            ZYDM = ZYDM == "" ? H[i].id.toString() : ZYDM + "," + H[i].id.toString();
        }
        var rtn = YSTB_CWZBPF.SaveBDZY($("#hidcheckid").val(), ZYDM).value;
        if (rtn > 0) {
            alert("保存成功!");
            return;
        } else {
            alert("保存失败!");
            return;
        }
    }
    function EditData(obj, flag) {
        if (flag == "0") 
        {
            //修改时选中当前行
            onselects(obj); 
            if (obj.name == "selMBDM") 
            {
                var trid = obj.parentNode.parentNode.id;
                $("tr[id=" + trid + "] input[name^=tselXMDM]").val("");

            }         
            if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
        }
        else if (flag == "1") 
        {
            if (isNaN(obj.value)) 
            {
                alert("请输入数字类型数据");
                obj.value = "";
                obj.focus();
            }
            else 
            {
                if (obj.name == "txtNEWVALUE") 
                {
                    GetZJValue();
                }
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
            }
            if (obj.name != "txtNEWVALUE") {
                //修改时选中当前行
                onselects(obj);
            }
        }
    }
    function GetZJValue() {
        var value = 0.0,tdOLDVALUE=0.0;
        var td = $("#DivFJ input[name^=txtNEWVALUE]");
        var td1 = $("#DivFJ td[name^=tdOLDVALUE]");
        for (var i = 0; i < td.length; i++) {
            value += parseFloat(td[i].value);
            tdOLDVALUE += parseFloat(td1[i].innerHTML);
        }
        $("tr[id='99999999']").find("td[name^=tdNEWVALUE]").html(value.toFixed(4).toString());
        $("tr[id='99999999']").find("td[name^=tdOV]").html(tdOLDVALUE.toFixed(4).toString());
    }
    $(document).ready(function () {
        BindFALB();
        BindYY();
        BindData();
        BindJHFA();
        LoadZYML();
        getlist('', '', '');
        WinXMCloseEvent();
    });
</script>
</asp:Content>

