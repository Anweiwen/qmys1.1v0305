﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class YSTB_LoadTree : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_LoadTree));
        HidYSLX.Value= Request.QueryString["YSLX"].ToString();
        HidMBZQ.Value = Request.QueryString["MBZQ"].ToString();
    }
}