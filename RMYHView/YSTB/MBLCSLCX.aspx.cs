﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using RMYH.BLL;
using System.Collections;

public partial class YSTB_MBLCSLCX : System.Web.UI.Page
{
    protected string USERDM = "";
    protected string HSZXDM = "";
    protected string USERNAME = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_MBLCSLCX));
        USERDM = HttpContext.Current.Session["USERDM"].ToString();
        HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        USERNAME = HttpContext.Current.Session["USERNAME"].ToString();
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string JHFADM,string MBMC)
    {
        string MBZQ = "", MBLX = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet DS=BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='"+JHFADM+"'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }
        return GetDataList.GetDataListstring("10145710", "", new string[] { ":JHFADM",":MBZQ",":MBLX",":MBMC" }, new string[] { JHFADM,MBZQ,MBLX,MBMC }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 获取方案类别相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ' ORDER BY XMDH_A");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["XMDH_A"].ToString() == "3" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取年份相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYY()
    {
        return HttpContext.Current.Session["YY"].ToString();
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetData(string FALX)
    {
        string Str = "";
        if (FALX == "2")
        {
            Str += "<option value='1'>第一季度</option>";
            Str += "<option value='2'>第二季度</option>";
            Str += "<option value='3'>第三季度</option>";
            Str += "<option value='4'>第四季度</option>";
        }
        else if (FALX == "3")
        {
            Str = "-1";
        }
        else
        {
            string MM = DateTime.Now.ToString("yyyy-MM").Split('-')[1].ToString();
            //月方案和其他方案
            for (int i = 1; i <= 12; i++)
            {
                if (i < 10)
                {
                    //默认选中当前月份
                    if (("0" + i.ToString()) == MM)
                    {
                        Str += "<option selected=true value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
                else
                {
                    if (i.ToString() == MM)
                    {
                        Str += "<option selected=true value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
            }
        }
        return Str;
    }
    /// <summary>
    /// 输入月份获取月份的中文名称
    /// </summary>
    /// <param name="MM">月份</param>
    /// <returns></returns>
    public string GetMonth(string MM)
    {
        string M = "";
        switch (MM)
        {
            case "1": M = "一月"; break;
            case "2": M = "二月"; break;
            case "3": M = "三月"; break;
            case "4": M = "四月"; break;
            case "5": M = "五月"; break;
            case "6": M = "六月"; break;
            case "7": M = "七月"; break;
            case "8": M = "八月"; break;
            case "9": M = "九月"; break;
            case "10": M = "十月"; break;
            case "11": M = "十一月"; break;
            case "12": M = "十二月"; break;
        }
        return M;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHFA(string FALX, string YY, string MM)
    {
        string Str = "", SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //选择月方案类型
        if (FALX == "1")
        {
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='1' AND SCBS=1";
        }
        else if (FALX == "2")
        {
            //选择季度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND JD='" + MM + "' AND FABS='2' AND SCBS=1";
        }
        else if (FALX == "3")
        {
            //选择年度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND FABS='3' AND SCBS=1";
        }
        else
        {
            //选择其他方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='4' AND SCBS=1";
        }
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            Str += "<option value='" + DS.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + DS.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
        }
        return Str;
    }
    /// <summary>
    /// 还原审批状态
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="JSDM">角色代码</param>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int SPZTHY(string JHFADM,string MBDM,string JSDM,string CJBM)
    {
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        return BLL.SPZTHY(JHFADM,MBDM,JSDM,CJBM); 
    }
    /// <summary>
    /// 查询管理员是否存在
    /// </summary>
    /// <param name="PSW">管理员密码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsExistUSER(string PSW)
    {
        bool Flag = false;
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        string SQL = "SELECT USER_ID FROM TB_USER WHERE USER_LOGIN='SYS' AND USER_PSW='"+PSW+"'";
        if (BLL.Query(SQL).Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        return Flag;
    }
    /// <summary>
    /// 初始化部门
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitZY()
    {
        string res = "<option value='0' selected='selected'>全部</option>";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        ds = bll.Query("SELECT DISTINCT D.ZYDM,D.ZYMC FROM TB_YSBBMB C,TB_JHZYML D WHERE C.ZYDM=D.ZYDM");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";

            }
        }
        return res;
    }
    /// <summary>
    /// 获取当前系统的月份
    /// </summary>
    /// <param name="YY">年份</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYSYF(string YY)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string NN = BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='" + YY + "'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBInfo(string MBDM)
    {
        string MBStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.getDataSet("TB_YSBBMB", "MBWJ,MBLX", "MBDM='" + MBDM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBStr = DS.Tables[0].Rows[0]["MBWJ"].ToString() + "@" + DS.Tables[0].Rows[0]["MBLX"].ToString();
        }
        return MBStr;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFAInfo(string JHFADM)
    {
        string MBStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.getDataSet("TB_JHFA", "FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBStr = DS.Tables[0].Rows[0]["FABS"].ToString();
        }
        return MBStr;
    }
    #endregion
}