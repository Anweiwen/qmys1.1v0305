﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class SYS_TB_SPXM : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_TB_SPXM));
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string XMMC)
    {
        return GetDataList.GetDataListstring("10144493", "", new string[] { ":XMMC" }, new string[] { XMMC }, false, trid, id, intimgcount);
    }

    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144493");
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_SPXM", id, "XMDM");
        return ret;
    }

    /// <summary>
    /// 更新数据
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="fileds"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.Update("10144493", "TB_SPXM", fileds, values, "XMDM", ID, new string[] { }, new string[] { });
        }
        catch
        {

        }
        return ret;
    }

    #endregion;
}