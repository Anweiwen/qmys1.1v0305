﻿<%@ WebHandler Language="C#" Class="Message" %>

using System;
using System.Web;
using System.Text;
using RMYH.BLL;
using RMYH.Model;
using System.Data;

public class Message : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string USERDM = context.Request.QueryString["USERDM"];
        string ACTION = context.Request.QueryString["action"];
        string QSSJ = context.Request.QueryString["QSSJ"];
        string JZSJ = context.Request.QueryString["JZSJ"];
        string NAME = context.Request.QueryString["NAME"];
        context.Response.Clear();
        string res = "";
        //获取待处理任务的Json字符串
        res = GetData(USERDM,ACTION,QSSJ,JZSJ,NAME);     
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取待处理任务的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetData(string USERDM,string Action,string QSSJ,string JZSJ,string NAME)
    {
        int Total = 0;
        string COLUMNNAME = "", JSNAME = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet dsValue = new DataSet();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        if (Action == "Msg")
        {
            model = tbll.GetModel("10145758");
            model.SQL = model.SQL.Replace(":QSSJ", "'" + QSSJ + "'");
            model.SQL = model.SQL.Replace(":JZSJ", "'" + JZSJ + "'");
            model.SQL = model.SQL.Replace(":USERDM", "'" + USERDM + "'");
            dsValue = bll.Query(model.SQL.ToString());
        }
        else if (Action == "History")
        {
            model = tbll.GetModel("10145763");
            model.SQL = model.SQL.Replace(":QSSJ", "'" + QSSJ + "'");
            model.SQL = model.SQL.Replace(":JZSJ", "'" + JZSJ + "'");
            model.SQL = model.SQL.Replace(":USERDM", "'" + USERDM + "'");
            dsValue = bll.Query(model.SQL.ToString());
        }
        else if (Action == "JS")
        {
            string SQL = "SELECT JSDM DM,NAME,JSMS FROM TB_JIAOSE WHERE NAME LIKE '%"+NAME+"%'";
            dsValue = bll.Query(SQL);
        }
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        Str.Append("{\"total\":" + Total + ",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
            {
                COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                if (COLUMNNAME == "JSDM")
                {
                    JSNAME = GetJSNameByJSDM(dsValue.Tables[0].Rows[i][COLUMNNAME].ToString());
                }
                Str.Append("\"" + COLUMNNAME + "\"");
                Str.Append(":");
                if (COLUMNNAME == "JSNAME")
                {
                    Str.Append("\"" + JSNAME + "\"");
                }
                else
                {
                    Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                }
                if (j < dsValue.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
            }
            if (i < dsValue.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (JSName == "")
            {
                JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            else
            {
                JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
        }
        return JSName;
    }
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }
}