﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class SYS_TB_SPXMLC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_TB_SPXMLC));
    }
    #region Ajax方法
    /// <summary>
    /// 获取审批项目
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getSPXM()
    {
        string rtn = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT '-1' XMDM,'---请选择---' XMMC UNION SELECT XMDM,XMMC FROM TB_SPXM ORDER BY XMDM");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                rtn += "<option value='" + ds.Tables[0].Rows[i]["XMDM"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return rtn;
    }

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string XMDM)
    {
        if (XMDM.Trim() == "-1")
        {
            XMDM = "";
        }
        return GetDataList.GetDataListstring("10144504", "", new string[] { ":XMDM" }, new string[] { XMDM }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSList(string JSName)
    {
        return GetDataList.GetDataListstring("10144511", "", new string[] { ":NAME" }, new string[] { JSName }, false, "", "", "");
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144504");
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_SPXMLC", id, "LCDM");
        return ret;
    }

    /// <summary>
    /// 更新数据
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="fileds"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string XMDM)
    {
        string ret = "";
        try
        {
            string strSPR = "";
            string[] arrvalues;
            string[] arrspr;
            for (int i = 0; i < values.Length; i++)
            {
                strSPR = "";
                arrvalues = values[i].Split('|');//取得审批人信息
                arrspr = arrvalues[2].Split(',');
                for (int j = 0; j < arrspr.Length; j++)
                {

                    strSPR += arrspr[j].Split('.')[0] + ",";

                }
                values[i] = arrvalues[0] + "|" + arrvalues[1] + "|" + (strSPR.Length > 0 ? strSPR.Substring(0, strSPR.Length - 1) : "");
            }
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.Update("10144504", "TB_SPXMLC", fileds, values, "LCDM", ID, new string[] { "XMDM" }, new string[] { XMDM });
        }
        catch
        {

        }
        return ret;
    }

    #endregion;
}