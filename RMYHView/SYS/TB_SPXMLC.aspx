﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TB_SPXMLC.aspx.cs" Inherits="SYS_TB_SPXMLC" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> 
    <br />
   项目名称：<select id="selSPXM" style="width:180px"></select>&nbsp;&nbsp;
   <input type="button" class="button5"  value="查询" style="width:60px" onclick ="getlist('','','')" />&nbsp;&nbsp;
   <input type="button" class="button5" value="添加" style="width:60px" onclick="jsAddData()" />&nbsp;&nbsp;
   <input type="button" class="button5" value="删除" style="width:60px" onclick="Del()" />&nbsp;&nbsp;
   <input type="button" class ="button5" value="取消删除" style="width:80px" onclick="Cdel()" />&nbsp;&nbsp;
   <input id="Button11" class="button5"  type="button" style="width:60px" value="保存"  onclick="Save()"/>
   <br /><br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divTreeListView"></div>
 <div id="Win" class="easyui-window" title="角色选取" closed="true" style="width:680px;height:400px;padding:20px;text-align: center; ">
    <div style=" text-align:left">
        角色名称：<input type="text" id="TxtJS" style="width:120px" />&nbsp;&nbsp;
        <input type="button" value="查询" onclick="GetJSList()" class="button5" style="width:60px" />&nbsp;&nbsp;
        <input type="button" value="确定" onclick="Win(false)" class="button5" style="width:60px" />&nbsp;&nbsp;
        <input type="button" value="退出" onclick="Win(false)" class="button5" style="width:60px" />&nbsp;&nbsp;         
        <br />        <br />
        <div id="JSXQ" style="overflow:auto"></div>
   </div>
 </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="LCDM" />
<input type="hidden" id="hidcheckid" />
<input type="hidden" id="hidNewLine" />  
<script type="text/javascript">
    var JSOBJ;
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = SYS_TB_SPXMLC.UpdateData(objid, objfileds, objvalues, $("#selSPXM").val()).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function Save() {
        if ($("#selSPXM").val() == "-1") 
        {
            alert("请先选择项目名称！");
            return;
        }
        else 
        {
            if (SetValues()) 
            {
                alert("保存成功！");
                getlist('', '', '');
                return;
            }
            else 
            {
                alert("保存失败！");
                return;
            }
        }
    }
    function jsDeleteData(obj) {
        var rtn = SYS_TB_SPXMLC.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));

    }
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = SYS_TB_SPXMLC.LoadList(objtr, objid, intimagecount, $("#selSPXM").val()).value;
        document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    function GetJSList() {
        var rtnstr = SYS_TB_SPXMLC.GetJSList($("#TxtJS").val()).value;
        document.getElementById("JSXQ").innerHTML = rtnstr;
        $("#JSXQ INPUT[type=button]").attr("style", "display:none")
    }
    function jsAddData() {
        if ($("#selSPXM").val() == "-1") {
            alert("请先选择项目名称！");
            return;
        }
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(SYS_TB_SPXMLC.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
    }
    function getSPXMjs() {
        $("#selSPXM").html(SYS_TB_SPXMLC.getSPXM().value);
    }
    function selectvalues(obj, id) {
        JSOBJ=obj;
        Win(true);
    }
    function Win(Flag) 
    {
        if (Flag == true) 
        {
            $("#Win").window('open');
            GetJSList();   
        }
        else 
        {
            $("#Win").window('close'); 
        }
    }
    function WinCloseEvent() {
        $("#Win").window({
            onBeforeClose: function () {
                var JSDM = "";
                var CK = $("#JSXQ INPUT:checked");
                for (var i = 0; i < CK.length; i++) {
                    var trid = CK[i].parentNode.parentNode.id;
                    var DM = $("#" + trid).find("td[name^='tdJSDM']")[0].innerHTML;
                    var NAME = $("#" + trid).find("td[name^='tdNAME']")[0].innerHTML;
                    if (JSDM == "") {
                        JSDM = DM + "." + NAME;
                    }
                    else {
                        JSDM = JSDM + "," + DM + "." + NAME;
                    }
                }
                if (CK.length > 0)
                    JSOBJ.parentNode.childNodes[0].value = JSDM;
                else
                    JSOBJ.parentNode.childNodes[0].value = "";
                $("#JSXQ img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
                JSOBJ = null;
            }
        });
    }
    $(document).ready(function () { 
        getSPXMjs(); 
        getlist('', '', ''); 
        WinCloseEvent(); 
    });
</script>
</asp:Content>


