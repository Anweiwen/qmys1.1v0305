﻿using System;
using System.Data;
using RMYH.BLL;
using System.Text;
using RMYH.DBUtility;
using System.Collections.Generic;
using System.Collections;
using Sybase.Data.AseClient;

public partial class SYS_MenuListQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_MenuListQX));
    }
    #region
    /// <summary>
    /// 获取用户列表
    /// </summary>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetUSERJson()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        //string sql = "SELECT JSDM ID,NAME,'0' PAR,'1' CCJB FROM TB_JIAOSE UNION SELECT USERID ID,USER_NAME NAME,JSDM PAR,'2' CCJB FROM TB_JSYH YH,TB_USER U WHERE YH.USERID=U.USER_ID";
        string sql = "SELECT JSDM ID,NAME,'0' PAR,'1' CCJB FROM TB_JIAOSE";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            //递归拼接子节点的Json字符串
            USERecursionChild(DS.Tables[0], "0", ref Str);
        }
        return Str.ToString();
    }
    /// <summary>
    /// 递归查询子节点
    /// </summary>
    /// <param name="DT">数据集</param>
    /// <param name="PAR">父节点</param>
    /// <param name="Str">需返回的JSON字符串</param>
    private void USERecursionChild(DataTable DT, string PAR, ref StringBuilder Str)
    {
        DataRow[] DR;
        //查询父节点为PAR的子节点
        DR = DT.Select("PAR='" + PAR + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["ID"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["NAME"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\": {\"PAR\": \"" + dr["PAR"].ToString() + "\",\"CCJB\": \"" + dr["CCJB"].ToString() + "\"}");
                //递归查询子节点
                USERecursionChild(DT, dr["ID"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 获取用户列表
    /// </summary>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetMenuListJson()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        string sql = "SELECT FUNCID ID,MENUID,MENUCAPTION NAME,DLLNAME URL,CASE DATALENGTH(MENUID) WHEN 2 THEN '0' ELSE SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2) END PAR,DATALENGTH(MENUID)/2 CCJB  FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' ORDER BY MENUID";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            //递归拼接子节点的Json字符串
            RecursionChild(DS.Tables[0], "0", ref Str);
        }
        return Str.ToString();
    }
    /// <summary>
    /// 递归查询子节点
    /// </summary>
    /// <param name="DT">数据集</param>
    /// <param name="PAR">父节点</param>
    /// <param name="Str">需返回的JSON字符串</param>
    private void RecursionChild(DataTable DT, string PAR, ref StringBuilder Str)
    {
        DataRow[] DR;
        string YJDBZ;
        //查询父节点为PAR的子节点
        DR = DT.Select("PAR='" + PAR + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                YJDBZ = "0";
                Str.Append(",\"YJDBZ\":\"" + YJDBZ + "\"}");
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["ID"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["NAME"].ToString() + "\"");
                //if (dr["URL"].ToString() != "" && dr["URL"].ToString() != null && dr["URL"].ToString().ToLower().IndexOf("aspx") > -1)
                //{
                    //YJDBZ = "1";
                    Str.Append(",");
                    Str.Append("\"attributes\": {\"url\": \"" + dr["URL"].ToString() + "\",\"MENUID\":\"" + dr["MENUID"].ToString() + "\",\"PAR\": \"" + dr["PAR"].ToString() + "\",\"CCJB\": \"" + dr["CCJB"].ToString() + "\"");
                //}
                //else
                //{
                //    YJDBZ = "0";
                //    Str.Append(",");
                //    Str.Append("\"attributes\": {\"url\": \"\",\"MENUID\":\"" + dr["MENUID"].ToString() + "\",\"PAR\": \"" + dr["PAR"].ToString() + "\",\"CCJB\": \"" + dr["CCJB"].ToString() + "\",\"YJDBZ\":\"" + YJDBZ + "\"}");
                //}
                //递归查询子节点
                RecursionChild(DT, dr["MENUID"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
        else
        {
            YJDBZ = "1";
            Str.Append(",\"YJDBZ\":\"" + YJDBZ + "\"}");
        }
    }
    /// <summary>
    /// 保存菜单权限
    /// </summary>
    /// <param name="USERDM">用户或者角色代码</param>
    /// <param name="IsJS">是否为角色</param>
    /// <param name="MenuStr">菜单字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int Save(string USERDM,string IsJS,string MenuStr)
    {
        int Flag = 0;
        DataSet DS = new DataSet();
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> ArrSql = new List<string>();
        string InstSql = "", BBSql = "";
        try
        {
            //判断被选择用户是用户组还是用户
            if (IsJS == "1")
            {              
                //删除角色权限
                BBSql = "DELETE FROM TB_MENUQX WHERE JSDM=@JSDM";
                ArrSql.Add(BBSql);
                AseParameter[] JSDel = 
                {
                    new AseParameter("@JSDM", AseDbType.VarChar, 40)
                };
                JSDel[0].Value = USERDM;
                List.Add(JSDel);

                ////删除角色下所有赋予其旗下的用户的权限
                //string USER_ID = "";
                //DataSet DT = new DataSet();
                ////获取角色下的所有用户
                //DT = GetUSERDMByJSDM(USERDM);
                //for (int i = 0; i < DT.Tables[0].Rows.Count; i++)
                //{
                //    USER_ID=USER_ID == "" ? DT.Tables[0].Rows[i]["USERID"].ToString() : USER_ID + "','" + DT.Tables[0].Rows[i]["USERID"].ToString();
                //}
                //if (USER_ID != "")
                //{
                //    USER_ID = "'" + USER_ID + "'";
                //    USSql = "DELETE FROM TB_BBUSERQX WHERE USER_ID IN(" + USER_ID + ") AND BBBM IN(SELECT BBBM FROM TB_BBGROUPQX WHERE GROUP_ID=@JSDM AND BZ='1') AND BZ='1'";
                //    ArrSql.Add(USSql);
                //    AseParameter[] UDel = 
                //    {
                //        new AseParameter("@JSDM", AseDbType.VarChar, 40)
                //    };
                //    UDel[0].Value = USERDM;
                //    List.Add(UDel);
                //}

                //将选中的菜单写入数据表中
                if (MenuStr != "")
                {
                    string[] CD = MenuStr.Split(',');
                    for (int j = 0; j < CD.Length; j++)
                    {
                        InstSql = "INSERT INTO TB_MENUQX(JSDM,FUNCID) VALUES(@JSDM,@FUNCID)";
                        ArrSql.Add(InstSql);
                        AseParameter[] InJS = 
                        {
                             new AseParameter("@JSDM", AseDbType.VarChar, 40),
                             new AseParameter("@FUNCID", AseDbType.VarChar, 40)
                        };
                        InJS[0].Value = USERDM;
                        InJS[1].Value = CD[j].ToString();
                        List.Add(InJS);
                        //for (int k = 0; k < DT.Tables[0].Rows.Count; k++)
                        //{
                        //    USSql = "INSERT INTO TB_BBUSERQX(USER_ID,BBBM,BZ) VALUES(@USERDM,@FUNCID,@BZ)";
                        //    ArrSql.Add(USSql);
                        //    AseParameter[] InU = 
                        //    {
                        //         new AseParameter("@USERDM", AseDbType.VarChar, 40),
                        //         new AseParameter("@FUNCID", AseDbType.VarChar, 40),
                        //         new AseParameter("@BZ", AseDbType.VarChar, 2)
                        //     };
                        //    InU[0].Value = USERDM;
                        //    InU[1].Value = CD[j].ToString();
                        //    InU[2].Value = "1";
                        //    List.Add(InU);
                        //}
                    }
                }   
            }
            //else
            //{
            //    //删除用户菜单授权
            //    BBSql = "DELETE FROM TB_BBUSERQX WHERE USER_ID=@USERDM AND BZ='1'";
            //    ArrSql.Add(BBSql);
            //    AseParameter[] UDel = 
            //    {
            //        new AseParameter("@USERDM", AseDbType.VarChar, 40)
            //    };
            //    UDel[0].Value = USERDM;
            //    List.Add(UDel);
            //    //将选中的菜单写入数据表中
            //    if (MenuStr != "")
            //    {
            //        string[] CD = MenuStr.Split(',');
            //        for (int j = 0; j < CD.Length; j++)
            //        {
            //            USSql = "INSERT INTO TB_BBUSERQX(USER_ID,BBBM,BZ) VALUES(@USERDM,@FUNCID,@BZ)";
            //            ArrSql.Add(USSql);
            //            AseParameter[] InU = 
            //                {
            //                     new AseParameter("@USERDM", AseDbType.VarChar, 40),
            //                     new AseParameter("@FUNCID", AseDbType.VarChar, 40),
            //                     new AseParameter("@BZ", AseDbType.VarChar, 2)
            //                 };
            //            InU[0].Value = USERDM;
            //            InU[1].Value = CD[j].ToString();
            //            InU[2].Value = "1";
            //            List.Add(InU);
            //        }
            //    }   
            //}
            Flag=DbHelperOra.ExecuteSql(ArrSql.ToArray(),List);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Flag;
    }
    /// <summary>
    /// 根据角色代码获取角色下的所有用户
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public DataSet GetUSERDMByJSDM(string JSDM)
    {
        DataSet DS = new DataSet();
        if (JSDM != "")
        {
            string SQL = "SELECT USERID FROM TB_JIAOSE WHERE JSDM='" + JSDM + "'";
            DS = DbHelperOra.Query(SQL);
        }
        return DS;
    }
    /// <summary>
    /// 根据角色代码获取其有权限的菜单列表
    /// </summary>
    /// <param name="DM">角色代码</param>
    /// <param name="IsJS">判是否是角色（1代表是角色）</param>
    [AjaxPro.AjaxMethod]
    public string getCheckedFUNCID(string DM,string IsJS)
    {
        string BBSql = "",FUNCID="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //判断被选择用户是用户组还是用户
        if (IsJS == "1")
        {
            BBSql = "SELECT FUNCID FROM TB_MENUQX WHERE JSDM='" + DM + "'";
        }
        //else
        //{
        //    BBSql = "SELECT DISTINCT BBBM FROM TB_BBGROUPQX WHERE GROUP_ID IN(SELECT JSDM FROM TB_JSYH WHERE USERID='"+DM+"') AND BZ='1' UNION SELECT BBBM FROM TB_BBUSERQX WHERE USER_ID='" + DM + "' AND BZ='1'";
        //}
        DS = bll.Query(BBSql);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            FUNCID = FUNCID == "" ? DS.Tables[0].Rows[i]["FUNCID"].ToString() : FUNCID + "," + DS.Tables[0].Rows[i]["FUNCID"].ToString();
        }
        return FUNCID;
    }
    /// <summary>
    /// 赋值角色权限
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <param name="JS">被复制的角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int FZQX(string JSDM,string JS) {
        int Flag = 0;
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> ArrSql = new List<string>();
        string InstSql = "", BBSql = "";
        try
        {
            //删除角色权限
            BBSql = "DELETE FROM TB_MENUQX WHERE JSDM=@JSDM";
            ArrSql.Add(BBSql);
            AseParameter[] JSDel = 
            {
                new AseParameter("@JSDM", AseDbType.VarChar, 40)
            };
            JSDel[0].Value = JSDM;
            List.Add(JSDel);

            //赋值角色权限
            InstSql = "INSERT INTO TB_MENUQX(JSDM,FUNCID) SELECT '"+JSDM+"' JSDM,FUNCID FROM TB_MENUQX WHERE JSDM=@JS";
            ArrSql.Add(InstSql);
            AseParameter[] InJS = 
            {
                new AseParameter("@JS", AseDbType.VarChar, 40)
            };
            InJS[0].Value = JS;
            List.Add(InJS);
            Flag=bll.ExecuteSql(ArrSql.ToArray(), List);
        }
        catch(Exception e)
        {
            throw e;
        }
        return Flag;
    }
    /// <summary>
    /// 判断当前要复制的角色有没有权限
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsExistQX(string JSDM)
    {
        bool Flag = false;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_MENUQX WHERE JSDM='"+JSDM+"'";
        if (bll.Query(SQL).Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        return Flag;
    }
    #endregion
}