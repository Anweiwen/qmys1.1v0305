﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="DCLMB.aspx.cs" Inherits="SYS_DCLMB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script type="text/javascript" src="../JS/MBDataLoader.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Timer;
        var Mblb = '<%= MBLB %>';
        function Init() {
            $.ajax({
                type: 'get',
                url: 'DCLMBHandler.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'DCL',
                    QSSJ: '',
                    JZSJ: '',
                    NAME: '',
                    HSZXDM: '<%=HSZXDM %>'
                },
                success: function (result) {
                    $('#TabMB').datagrid({
                        //                        rowStyler:function (index, row) {
                        //                            if (row.BZ == "1") {
                        //                                return 'background-color:Red;color:Red';
                        //                            }
                        //                        },
                        onDblClickRow: function (rowIndex, rowData) {
                            if ($("#TxtYY").val() == "") {
                                alert("预算月份不能为空!");
                                return;
                            }
                            var MB, MC, WJ, YY, MM, JD, FA, FANAME, YF, USERDM, USERNAME, MBLX, ISDone, LB, ML, HSZXDM, SFZLC, CJBM;
                            FA = rowData.JHFADM;
                            FANAME = rowData.JHFANAME;
                            MB = rowData.MBDM;
                            MC = rowData.MBMC;
                            WJ = rowData.MBWJ;
                            YY = rowData.YY;
                            MM = rowData.NN;
                            JD = rowData.JD;
                            YF = $("#TxtYY").val();
                            USERDM = '<%=USERDM %>',
                            USERNAME = '<%=USERNAME %>',
                            HSZXDM = '<%=HSZXDM %>',
                            MBLX = rowData.MBLX;
                            LB = rowData.FABS;
                            ML = rowData.ML;
                            ISDone = "-1";
                            SFZLC = rowData.SFZLC;
                            //                            CJBM = rowData.CJBM;
                            //打开模板
                            OpenMb(MB, MC, WJ, YY, MM, JD, FA, FANAME, YF, USERDM, USERNAME, MBLX, ISDone, LB, ML, HSZXDM, SFZLC);
                        }
                    });
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        //装入模板：
        function InitMb() {
            datagridloading($("#TabMB2"));
            Mblb = getMblb($("#SelExe").val());
            $.ajax({
                type: 'get',
                url: '../Excels/Data/ExcelData.ashx',
                data: {
                    action: "InitMb",
                    isexe: "0,1",
                    jhfadm: $("#SelFa").val(),
                    mblb: Mblb,
                    mbmc: "",
                    zydm: "0"
                },
                async: true,
                cache: false,
                success: function (result) {
                    $('#TabMB2').datagrid({
                        onDblClickRow: function (rowIndex, rowData) {
                            OpenMb2();
                        }
                    });
                    if (result) {
                        //                        $("#DataGrdMb").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', eval(result));
                        if ("<%= MBLB %>" == "respfj") {
                            $("#TabMB2").datagrid('loadData', eval(result));
                        } else {
                            $("#TabMB2").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', eval(result));
                        }
                    }
                    datagridloaded($("#TabMB2"));
                },
                error: function (req) {
                    datagridloaded($("#TabMB2"));
                    BmShowWinError('装入模板错误!', req.responseText);
                }
            });
        }
        //打开模板：
        function OpenMb2() {
            var selected = $("#TabMB2").datagrid('getSelected');
            if (selected) {
                //只有待执行的时候，才判断关联下级模板是否完成
                //                if ($("#SelExe").val() == "0") {
                //                    //只有走流程的才判断关联下级模板是否完成
                //                    if (selected.SFZLC == 1) {
                //                        //var MBStr = ExcelData.GetDCLMB(selected.MBDM, selected.JSDM, selected.HSZXDM, selected.LCDM, $('#SelFa').val()).value;
                //                        var MBStr = ExcelData.GetDCLMB($('#SelFa').val(), selected.MBDM, selected.JSDM).value;
                //                        //                        MBStr = "11";
                //                        if (MBStr != "" && MBStr != null) {
                //                            //                            debugger;
                //                            msgbox('以下部门尚未审核,请督促报送', MBStr, 'GetOpenMBURL()', null, 0, null, '确定', '否', 1);
                //                            //                            if (confirm(MBStr + "\r\n\r\n 您确定要打开吗？")) {
                //                            //                                GetOpenMBURL(selected);
                //                            //                            }
                //                        } else {
                //                            GetOpenMBURL(selected);
                //                        }
                //                    } else {
                //                        GetOpenMBURL(selected);
                //                    }
                //                } else {
                GetOpenMBURL(selected);
                //                }
            } else {
                alert("请先选择模板！");
            }
        }
        //获取打开模板的URL（张哥之前OpenMB方法里面的代码）
        function GetOpenMBURL(selected) {
            if (null == selected) {
                selected = $("#TabMB2").datagrid('getSelected');
            }
            var url = "MBSZ=ExcelData&MBMC=" +
                getchineseurl(selected.MBMC) +
                "&MBDM=" +
                selected.MBDM +
                "&MBWJ=" +
                getchineseurl(selected.MBWJ) +
                "&YY=" +
                $("#SelYear").val() +
                "&NN=" +
                formatmonth($("#SelMonth").val()) +
                "&JD=" +
                $("#SelQuarter").val() +
                "&FADM=" +
                $("#SelFa").val() +
                "&FADM2=" +
                $("#SelFa2").val() +
                "&FAMC=" +
                getchineseurl($("#SelFa").find("option:selected").text()) +
                "&YSYF=" +
                "11" +
                "&USERDM=<%= USERDM %>" +
                "&USERNAME=" +
                getchineseurl("<%= USERNAME %>") +
                "&FALB=" +
                $("#SelFalb").val() +
                "&MBLB=" + Mblb +
                "&ISEXE=" +
                "0,1" +
                "&MBLX=" +
                selected.MBLX +
                "&HSZXDM=" +
                "10070003" +
                "&SFZLC=" +
                selected.SFZLC +
                "&LOGINYY=<%= YY %>" +
                "&JSDM=" +
                selected.JSDM +
                "&LCDM=" +
                selected.LCDM +
                "&ACTIONRIGHT=" +
                selected.ACTIONRIGHT;

            //如果不是ie浏览器：
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            OpenWebExcel(selected.MBDM, selected.MBMC, url, $("#SelFa").val(), $("#SelFa2").val());
        }
        function getMblb(_val) {
            var mblb = "";
            if (_val == "0") {
                mblb = "basesb";
            } else if (_val == "1") {
                mblb = "basesp";
            }
            return mblb;
        }
        //打开错误提示对话框：
        function BmShowWinError(_title, _content, _isnotvisble) {
            $('#winerror').dialog({
                content: _title,
                modal: true,
                width: 400,
                height: 200,
                top: ($(window).height() - 200) * 0.5,
                left: ($(window).width() - 400) * 0.5,
                onClose: function () {
                    $('#winerror').dialog('options').content = '';
                },
                buttons: [
            {
                text: '查看明细',
                iconCls: 'icon-ok',
                handler: function () {
                    $('#winerror').dialog({
                        content: _content,
                        width: 800,
                        height: 400,
                        top: ($(window).height() - 400) * 0.5,
                        left: ($(window).width() - 800) * 0.5
                    });
                    $('.dialog-button').css('text-align', 'center');
                    $('#winerror').dialog('open');
                }
            }, {
                text: '关闭提示',
                iconCls: 'icon-cancel',
                handler: function () {
                    BmShowWinClose();
                }
            }
        ]
            });
            $('.dialog-button').css('text-align', 'center');
            $('#winerror').dialog('open');
        }
        //清空模版:
        function ClearMb() {
            $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
            $("#TabMB2").datagrid('loadData', { total: 0, rows: [] });
        }
        function InitMsg() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'Msg',
                    QSSJ: $("#StartTime").datebox("getValue"),
                    JZSJ: $("#EndTime").datebox("getValue"),
                    NAME: ''
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        result = result.replace(/(\r)+|(\n)+|(\r\n)+/g, "");
                        var Data = eval("(" + result + ")");
                        $("#TabMsg").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    window.clearInterval(Timer);
                    alert("加载失败!");

                }
            });
        }
        function InitMsgHistory() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'History',
                    QSSJ: $("#QSSJ").datebox("getValue"),
                    JZSJ: $("#JZSJ").datebox("getValue"),
                    NAME: ''
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMsgHistory").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function InitJS() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                singleSelect: false,
                checkbox: true,
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'JS',
                    QSSJ: $("#QSSJ").datebox("getValue"),
                    JZSJ: $("#JZSJ").datebox("getValue"),
                    NAME: $("#TxtJS").val()
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabJS").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        //          打开模版：模板参数

        //          MBDM      模板代码
        //          MBMC      模板名称
        //          MBWJ      模板文件
        //          YY        年份
        //          MM        月份
        //          JD        季度
        //          JHFADM    计划方案代码
        //          JHFANAME  计划方案名称
        //          YSYF      预算月份
        //          USERDM    用户代码
        //          USERNAME  用户名称
        //          MBLX      模板类别 basesb代表上报； basesp 代表审批
        //          ISDone    0代表待操作； 1代表已操作
        //          FALB      方案类别
        //          ML      模板类型
        function OpenMb(MBDM, MBMC, MBWJ, YY, MM, JD, JHFADM, JHFANAME, YSYF, USERDM, USERNAME, MBLX, ISDone, FALB, ML, HSZXDM, SFZLC) {
            //            var url = "pageoffice://|" + geturlpath() + "MB.aspx?mbmc=" + getchineseurl(MBMC)
            //                 + "&mbdm=" + MBDM + "&mbwj=" + getchineseurl(MBWJ) + "&yy=" + YY
            //                 + "&nn=" + MM + "&jd=" + JD
            //                 + "&fadm=" + JHFADM + "&fadm2="
            //                 + "&famc=" + getchineseurl(JHFANAME)
            //                 + "&ysyf=" + YSYF + "&userdm=" + USERDM
            //                 + "&username=" + getchineseurl(USERNAME) + "&falb=" + FALB
            //                 + "&mblb=" + MBLX + "&isexe=" + ISDone + "&mblx=" + ML
            //                 + "&hszxdm=" + HSZXDM
            //                 + "&sfzlc=" + SFZLC;
            //                 + "&cjbm=" + CJBM;
            var url = "MBMC=" + getchineseurl(MBMC)
                 + "&MBDM=" + MBDM + "&MBWJ=" + getchineseurl(MBWJ) + "&YY=" + YY
                 + "&NN=" + MM + "&JD=" + JD
                 + "&FADM=" + JHFADM + "&FADM2="
                 + "&FAMC=" + getchineseurl(JHFANAME)
                 + "&YSYF=" + YSYF + "&USERDM=" + USERDM
                 + "&USERNAME=" + getchineseurl(USERNAME) + "&FALB=" + FALB
                 + "&MBLB=" + MBLX + "&ISEXE=" + ISDone + "&MBLX=" + ML
                 + "&HSZXDM=" + HSZXDM
                 + "&SFZLC=" + SFZLC;
            //如果不是IE浏览器
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            //window.location.href = url + "|||";
            var selected = $("#TabMB").datagrid('getSelected');
            OpenWebExcel(selected.MBDM, selected.MBMC, url, $("#SelFa").val(), $("#SelFa2").val());
        }
        //打开SpreadJS网页
        function OpenWebExcel(ID, MC, paramStr, FADM, FADM2) {
            parent.$("#HidMBDM").val(ID);
            if (parent.$("#DivContent").tabs('exists', MC)) {
                parent.$("#DivContent").tabs('select', MC);
                //如果Title为空，表示打开的是一般的菜单页，否则打开的是数据填报页
                if (FADM != "" && FADM != undefined && FADM != null) {
                    var NewFileName = "Jhfadm~" + FADM + "~Jhfadm2~" + FADM2 + "~Mb~" + ID;
                    var TabFrame = $('#tt').tabs('getTab', MC).find("iframe");
                    if (TabFrame.length > 0) {
                        var OldFileName = TabFrame.contents().find("#HidFAMB").val();
                        if (NewFileName != OldFileName) {
                            var path = window.document.location.pathname.substring(1);
                            var Len = path.indexOf("/");
                            var root = "/" + path.substring(0, Len + 1);
                            root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random();
                            var content = '<iframe scrolling="auto" id="' +
                                ID +
                                '"  frameborder="0"  src="' +
                                root +
                                '" style="width:100%;height:100%;"></iframe>';
                            var tab = $('#tt').tabs('getTab', MC);
                            parent.$("#DivContent").tabs('update',
                                {
                                    tab: tab,
                                    options: {
                                        content: content
                                    }
                                });
                        }
                    }
                }
            } else {
                var path = window.document.location.pathname.substring(1);
                var Len = path.indexOf("/");
                var root = "/" + path.substring(0, Len + 1);
                root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random(); ;
                var content = '<iframe scrolling="auto" id="' +
                    ID +
                    '"  frameborder="0"  src="' +
                    root +
                    '" style="width:100%;height:100%;"></iframe>';
                parent.$("#DivContent").tabs('add',
                    {
                        title: MC,
                        content: content,
                        closable: true
                    });
            }
        }
        function BindMM() {
            $.ajax({
                type: 'get',
                url: 'InitDataHandler.ashx',
                async: true,
                cache: false,
                data: {
                    action: 'YS',
                    YY: '<%=YY %>'
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        $("#TxtYY").val(result);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function geturlpath() {
            var href = document.location.href;
            var h = href.split("/");
            href = "";
            for (i = 0; i < h.length - 2; i++) {
                href += h[i] + "/";
            }
            if (href != "") {
                href = href + "Excels/Data/";
            }
            return href;
        }
        function getchineseurl(_param) {
            return encodeURIComponent(encodeURIComponent(_param));
        }
        function WinJS(Flag, BZ) {
            if (Flag == true) {
                $("#WinJS").window('open');
                InitJS();
            }
            else {
                if (BZ == 1) {
                    var JSDM = "";
                    var CK = $("#TabJS").datagrid("getChecked");
                    if (CK.length == 0) {
                        alert("您没有选择要发送消息的角色!");
                        return;
                    }
                    for (var i = 0; i < CK.length; i++) {
                        var DM = CK[i].DM;
                        var NAME = CK[i].NAME;
                        if (JSDM == "") {
                            JSDM = DM + "." + NAME;
                        }
                        else {
                            JSDM = JSDM + "," + DM + "." + NAME;
                        }
                    }
                    $("#TxtJSDM").val(JSDM);
                }
                $("#WinJS").window('close');
            }
        }
        function WinMsg(Flag) {
            if (Flag == true) {
                $("#WinMsg").window('open');
                InitMsgHistory();
            }
            else {
                $("#WinMsg").window('close');
            }
        }
        function onQSSJ(data) {
            var StartTime = $("#QSSJ").datebox("getValue");
            var EndTime = $("#JZSJ").datebox("getValue");
            if (StartTime != null && StartTime != "") {
                if (StartTime > EndTime) {
                    alert("起始时间不能大于截止时间!");
                    $("#QSSJ").datebox("setValue", EndTime);
                    return;
                }
                else {
                    //时间改变后重新加载数据
                    InitMsgHistory();
                }
            }
            else {
                alert("起始时间不能为空！");
                return;
            }
        }
        function onJZSJ(date) {
            var StartTime = $("#QSSJ").datebox("getValue");
            var EndTime = $("#JZSJ").datebox("getValue");
            if (EndTime != null && EndTime != "") {
                if (EndTime < StartTime) {
                    alert("截止时间不能小于起始时间!");
                    $("#JZSJ").datebox("setValue", StartTime);
                    return;
                }
                else {
                    //时间改变后重新加载数据
                    InitMsgHistory();
                }
            }
            else {
                alert("截止时间不能为空！");
                return;
            }
        }
        function onStartTime(data) {
            var StartTime = $("#StartTime").datebox("getValue");
            var EndTime = $("#EndTime").datebox("getValue");
            if (StartTime != null && StartTime != "") {
                if (StartTime > EndTime) {
                    alert("起始时间不能大于截止时间!");
                    $("#StartTime").datebox("setValue", EndTime);
                    return;
                }
                else {
                    //时间改变后重新加载数据
                    InitMsg();
                }
            }
            else {
                alert("起始时间不能为空！");
                return;
            }
        }
        function onEndTime(date) {
            var StartTime = $("#StartTime").datebox("getValue");
            var EndTime = $("#EndTime").datebox("getValue");
            if (EndTime != null && EndTime != "") {
                if (EndTime < StartTime) {
                    alert("截止时间不能小于起始时间!");
                    $("#EndTime").datebox("setValue", StartTime);
                    return;
                }
                else {
                    //时间改变后重新加载数据
                    InitMsg();
                }
            }
            else {
                alert("截止时间不能为空！");
                return;
            }
        }
        function InitData() {
            $.ajax({
                type: 'get',
                url: 'InitDataHandler.ashx',
                async: true,
                cache: false,
                data: {
                    action: 'Data',
                    YY: ''
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        var QSSJ = "", JZSJ = "";
                        QSSJ = result.split('@')[0].toString();
                        JZSJ = result.split('@')[1].toString();

                        $("#StartTime").datebox("setValue", QSSJ);
                        $("#EndTime").datebox("setValue", JZSJ);

                        $("#QSSJ").datebox("setValue", QSSJ);
                        $("#JZSJ").datebox("setValue", JZSJ);

                        InitMsg();
                        Timer = window.setInterval("InitMsg()", 20000);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function SendMsg() {
            if ($("#TxtMsg").val() == "") {
                alert("发送的消息不能为空!");
                return;
            }
            if ($("#TxtJSDM").val() == "") {
                alert("请先选择要发送消息的角色！");
                return;
            }
            if (confirm("您确定要发送此消息吗?")) {
                var rtn = SYS_DCLMB.SendMsg($("#TxtJSDM").val(), $("#TxtMsg").val()).value;
                var Arr = rtn.split('@');
                if (parseInt(Arr[0].toString()) > 0) {
                    alert("发送成功！");
                    $("#TxtMsg").val("");
                    $("#TxtJSDM").val("");
                    InitMsg();
                    return;
                }
                else {
                    alert(Arr[1].toString());
                    return;
                }
            }
        }
        function GSH(val, row) {
            if (row.BZ == "1") {
                return '<span style="color:red;">' + val + '</span>';
            }
            else {
                return val;
            }
        }
        $(function () {
            BindMM();
            InitData();
            ClearMb();
            loadTgxx();
            InitFalb(false);
            InitYear();
            InitMonth();
            InitQuarter();
            InitFa();
            $("#SelFalb").change(function () {
                refresh();
                InitFa();
            });
            $("#SelFa").change(function () {
                ClearMb();
            });
            $("#SelMonth").change(function () {
                InitFa();
            });

            $("#SelYear").change(function () {
                InitFa();
            });

            $("#SelExe").change(function () {
                ClearMb();
            });
        });
        function InitFa() {
            $.ajax({
                type: 'get',
                url: '../Excels/Data/ExcelData.ashx',
                data: {
                    action: "GetFa",
                    hszxdm: "10070003",
                    yy: $("#SelYear").val(),
                    nn: $("#SelMonth").val(),
                    jd: $("#SelQuarter").val(),
                    fabs: $("#SelFalb").val(),
                    mblb: Mblb
                },
                async: true,
                cache: false,
                success: function (result) {
                    $("#SelFa").empty();
                    $("#SelFa").append(result);
                    $("#SelFa2").empty();
                    $("#SelFa2 ").append(result);
                    ClearMb();
                },
                error: function (req) {
                    BmShowWinError('装入方案错误!', req.responseText);
                }
            });
        }
        function refresh() {
            var faidx = $("#SelFalb").val();
            switch (parseInt(faidx)) {
                //月：                                                   
                case 1:
                    $("#DivMonth").css("display", "inline");
                    $("#DivQuarter").css("display", "none");
                    break;
                //季：                                                   
                case 2:
                    $("#DivMonth").css("display", "none");
                    $("#DivQuarter").css("display", "inline");
                    break;
                //年：                                                   
                case 3:
                    $("#DivMonth").css("display", "none");
                    $("#DivQuarter").css("display", "none");
                    break;
                //其他：                                                   
                case 4:
                    $("#DivMonth").css("display", "inline");
                    $("#DivQuarter").css("display", "none");
                    break;
            }
        }
        function InitFalb(_asy) {
            if (_asy === undefined)
                _asy = true;
            $.ajax({
                type: 'get',
                url: '../Excels/Data/ExcelData.ashx?action=InitFalb',
                async: _asy,
                cache: false,
                success: function (result) {
                    $("#SelFalb").empty();
                    $("#SelFalb").append(result);
                    $("#SelFalb").val("3");
                    refresh();
                    //                    InitFa();
                },
                error: function (req, info, obj) {
                    BmShowWinError('装入方案类别错误!', req.responseText);
                }
            });
        }
        //根据登录的年份，加载年下拉框：
        function InitYear() {
            var yy = parseInt('<%= YY %>');
            var s = "";
            for (var i = yy - 4; i < yy + 5; i++) {
                if (i == yy) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelYear").append(s);
        }
        //根据当前月份，加载月下拉框：
        function InitMonth() {
            var mm = parseInt('<%= MM %>');
            var s = "";
            for (var i = 1; i <= 12; i++) {
                if (i == mm) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelMonth").append(s);
        }
        //根据当前月份，加载季度下拉框：
        function InitQuarter() {
            var mm1 = parseInt('<%= MM %>');
            var q = Math.ceil(mm1 / 3);
            var s = "";
            for (var i = 1; i <= 4; i++) {
                if (i == q) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelQuarter").append(s);
        }
        /// <summary>牛森炎新加</summary>
        /// <summary>获取消息通告信息</summary>
        function loadTgxx() {
            var TgxxData = "";
            var rtn = SYS_DCLMB.loadTgxx().value;
            TgxxData = eval("(" + rtn + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', TgxxData);
        }
        /// <summary>牛森炎新加</summary>
        /// <summary>下载消息通告文件</summary>
        function Download() {
            //选中的行
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length > 0) {
                var data = [];
                for (var i = 0; i < rows.length; i++) {
                    var item = [];
                    item.push(rows[i].WJMC);
                    data.push(item);
                }
                var res = geturlpaths() + "../ExcelFile/TgxxFile/" + data.join(",");
                window.open(res);
            }
            else {
                alert("请选择要下载的文件");
            }
        }
        function geturlpaths() {
            var href = document.location.href;
            var h = href.split("/");
            href = "";
            for (i = 0; i < h.length - 1; i++) {
                href += h[i] + "/";
            }
            return href;
        }
    </script>
</head>
<body>
    <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%;
    height: 100%; top: 5px">
    <div data-options="region:'west'" title="" style="width: 50%;">
        <div class="easyui-tabs" style="width: 100%; height: 100%">
            <div title="待处理任务">
                <div class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
                    <div data-options="region:'north'" style="height: 60px; background: #ADD8E6; padding: 15px">
                        财务实际发生月份：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'MM'})"
                            style="width: 80px" />&nbsp;&nbsp;
                        <input type="button" value="查询" onclick="Init()" class="button5" style="width: 60px" />&nbsp;&nbsp;
                    </div>
                    <div id="DivMb" style="width: 100%; height: 100%;" data-options="region:'center'">
                        <%--   <%=GetDataGridList(1)%>--%>
                        <table id="TabMB" style="width: 100%; height: 100%;" class="easyui-datagrid" title=""
                            data-options="singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true, pageSize: 15,pageList: [10, 15, 20, 25, 30, 35, 40],">
                            <thead>
                                <tr>
                                    <th data-options="field:'SYTS',width:100,formatter:GSH">
                                        剩余工作日
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'BZ',width:0">
                                        标记
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'JHFADM',width:0">
                                        计划方案代码
                                    </th>
                                    <th data-options="field:'JHFANAME',width:150,formatter:GSH">
                                        方案名称
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'MBDM',width:0">
                                        模板代码
                                    </th>
                                    <th data-options="field:'MBMC',width:250,formatter:GSH">
                                        报表名称
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'JSDM',width:0">
                                        角色代码
                                    </th>
                                    <th data-options="field:'JSNAME',width:180,formatter:GSH">
                                        角色
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'CJBM',width:0">
                                        层级编码
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'MBWJ',width:0">
                                        模板文件
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'YY',width:0">
                                        年份
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'NN',width:0">
                                        月份
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'JD',width:0">
                                        季度
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'MBLX',width:0">
                                        上报类型
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'FABS',width:0">
                                        方案标识
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'ML',width:0">
                                        模板类型
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'SFZLC',width:0">
                                        是否走流程
                                    </th>
                                    <th data-options="field:'TBSJXZ',width:120,formatter:GSH">
                                        截止时间
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div title="已处理任务">
                <div class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
                    <div data-options="region:'north'" style="height: 90px; background: #ADD8E6; padding: 15px">
                        &nbsp; 执行状态：
                        <select id="SelExe" style="width: 70px; display: inline;">
                            <option value="0" selected="selected">已填报</option>
                            <option value="1">已审核</option>
                        </select>
                        &nbsp; 方案类别：
                        <select id="SelFalb" style="width: 55px; display: inline;">
                        </select>
                        &nbsp;查询期：
                        <select id="SelYear" style="width: 56px; display: inline;">
                        </select>
                        &nbsp;年
                        <div id="DivMonth" style="display: inline;">
                            <select id="SelMonth" style="width: 40px;">
                            </select>
                            &nbsp;月
                        </div>
                        <div id="DivQuarter" style="display: inline;">
                            <select id="SelQuarter" style="width: 40px;">
                            </select>
                            &nbsp;季
                        </div>
                        <div style="padding: 5px;">
                            &nbsp;计划方案：
                            <select id="SelFa" style="width: 230px; display: inline;">
                            </select>&nbsp;
                            <input id="BtnInitMb" type="button" value="查询" onclick="InitMb()" class="button5"
                                style="width: 60px" />&nbsp;&nbsp;
                        </div>
                    </div>
                    <div id="DivMb2" style="width: 100%; height: 100%;" data-options="region:'center'">
                        <table id="TabMB2" style="width: 100%; height: 100%;" class="easyui-datagrid" title=""
                            data-options="singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true, pageSize: 15,pageList: [10, 15, 20, 25, 30, 35, 40],">
                            <thead>
                                <tr>
                                    <th data-options="field:'MBDM',width:100,formatter:GSH">
                                        模板代码
                                    </th>
                                    <th data-options="field:'MBMC',width:250,formatter:GSH">
                                        报表名称
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'JSDM',width:0">
                                        角色代码
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'CJBM',width:0">
                                        层级编码
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'MBWJ',width:0">
                                        模板文件
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'MBLX',width:0">
                                        上报类型
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'SFZLC',width:0">
                                        是否走流程
                                    </th>
                                    <th data-options="field:'ZYMC',width:130,formatter:GSH">
                                        部门名称
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-options="region:'center'" title="消息公告" style="width: 50%;">
        <div class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
            <div style="height: 60px; background: #ADD8E6;" data-options="region:'north'">
                <table>
                    <tr>
                        <td>
                            起始时间：<input id="StartTime" style="width: 100px" class="easyui-datebox" data-options="onSelect:onStartTime" />&nbsp;
                            &nbsp; 结束时间：<input id="EndTime" style="width: 100px" class="easyui-datebox" data-options="onSelect:onEndTime" />
                            &nbsp; &nbsp;
                            <input id="Button5" class="button5" type="button" value="历史发送记录" style="width: 120px"
                                onclick="WinMsg(true)" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            发送内容：<input id="TxtMsg" type="text" style="width: 200px" />&nbsp;&nbsp;
                            <input type="text" id="TxtJSDM" disabled="disabled" style="width: 120px" />&nbsp;&nbsp;
                            <input id="Button1" class="button5" type="button" value="角色选择" style="width: 70px"
                                onclick="WinJS(true,0)" />&nbsp;
                            <input id="Button2" class="button5" type="button" value="发送" style="width: 50px"
                                onclick="SendMsg()" />&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <div id="DivMsg" style="width: 100%; height: 100%;" data-options="region:'center'">
                <%-- <%=GetDataGridList(2)%>--%>
                <table id="TabMsg" style="width: 100%; height: 100%;" class="easyui-datagrid" title=""
                    data-options="nowrap:false,singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]">
                    <thead>
                        <tr>
                            <th data-options="field:'SJ',width:100">
                                消息接收时间
                            </th>
                            <th data-options="field:'USER_NAME',width:50">
                                发送人
                            </th>
                            <th data-options="field:'INFO',width:250">
                                消息内容
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="DivDownLoad" data-options="region:'south'" title="消息通告文件下载" style="width: 100%;
                height: 30%;">
                <table id="dg" class="easyui-datagrid" style="height: 100%; width: 100%" data-options="autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: true,toolbar:'#tb'">
                    <thead>
                        <tr>
                            <th data-options="field:'WJMC',width:60">
                                文件名称
                            </th>
                        </tr>
                    </thead>
                </table>
                <div id="tb" style="padding: 5px; height: auto">
                    <div style="margin-bottom: 5px">
                        <input id="Button3" class="button5" type="button" value="下载" onclick="Download()" />
                    </div>
                </div>
            </div>
            <div id="WinJS" class="easyui-window" title="角色选取" closed="true" data-options="minimizable:false"
                style="width: 700px; height: 400px; padding: 10px; text-align: center;">
                <div style="text-align: left" class="easyui-layout" fit="true" style="width: 100%;
                    height: 100%;">
                    <div style="height: 30px; background: #ADD8E6;" data-options="region:'north'">
                        角色名称：<input type="text" id="TxtJS" style="width: 120px" />&nbsp;&nbsp;
                        <input type="button" value="查询" onclick="InitJS()" class="button5" style="width: 60px" />&nbsp;&nbsp;
                        <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
                        <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
                    </div>
                    <div id="JSXQ" data-options="region:'center'" style="width: 100%; height: 60%;">
                        <%--    <%=GetDataGridList(4)%>--%>
                        <table id="TabJS" style="width: 100%; height: 100%;" class="easyui-datagrid" title=""
                            data-options="idField:'DM',autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]">
                            <thead>
                                <tr>
                                    <th data-options="field:'ck',checkbox:true ">
                                        选择
                                    </th>
                                    <th style="display: none" data-options="hidden:true,field:'DM',width:0">
                                        角色代码
                                    </th>
                                    <th data-options="field:'NAME',width:150">
                                        角色名称
                                    </th>
                                    <th data-options="field:'JSMS',width:180">
                                        角色描述
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="WinMsg" class="easyui-window" title="历史消息查询" data-options="minimizable:false"
                closed="true" style="width: 800px; height: 450px; padding: 20px; text-align: center;">
                <div style="text-align: left" class="easyui-layout" fit="true" style="width: 100%;
                    height: 100%;">
                    <div style="height: 30px; background: #ADD8E6;" data-options="region:'north'">
                        起始时间：<input id="QSSJ" style="width: 100px" class="easyui-datebox" data-options="onSelect:onQSSJ" />&nbsp;
                        &nbsp; 结束时间：<input id="JZSJ" style="width: 100px" class="easyui-datebox" data-options="onSelect:onJZSJ" />
                        &nbsp; &nbsp;
                        <input type="button" value="退出" onclick="WinMsg(false)" class="button5" style="width: 60px" />&nbsp;&nbsp;
                    </div>
                    <div id="MsgHistory" data-options="region:'center'" style="width: 100%; height: 100%;">
                        <%--   <%=GetDataGridList(3)%>--%>
                        <table id="TabMsgHistory" style="width: 100%; height: 100%;" class="easyui-datagrid"
                            title="" data-options="nowrap:false,singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]">
                            <thead>
                                <tr>
                                    <th data-options="field:'SJ',width:100">
                                        发送时间
                                    </th>
                                    <th data-options="field:'NAME',width:120">
                                        接收角色
                                    </th>
                                    <th data-options="field:'INFO',width:250">
                                        消息内容
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
