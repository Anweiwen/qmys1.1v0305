﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using System.Text;
using System.Net;
using RMYH.DBUtility;

public partial class SYS_kjcd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_kjcd));        
    }
    /// <summary>
    /// 获取当前用户设置的快捷菜单
    /// </summary>
    /// <returns></returns>
    public DataSet GetBBKJMenu()
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder StrSql = new StringBuilder();
        string USERDM = HttpContext.Current.Session["USER_ID"].ToString();
        StrSql.Append(" SELECT KJ.BBBM,MLMXMC,SYMB FROM TB_BBKJMENU KJ,BB_MLGLMX MX");
        StrSql.Append(" WHERE KJ.BBBM=MX.ID AND USERDM='"+USERDM+"'");
        return BLL.Query(StrSql.ToString());
    }
    [AjaxPro.AjaxMethod]
    public string getlist()
    {
        int t = 0;
        string URL ="",XMDM="",SYMB="";
        string YY=HttpContext.Current.Session["YEAR"].ToString();
        string HSZXDM=HttpContext.Current.Session["CC_NO"].ToString();
        string USERDM=HttpContext.Current.Session["USER_ID"].ToString();
        DataSet DS = GetBBKJMenu();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder sb = new StringBuilder();
        sb.Append("<table align=\"center\"; style=\"margin-top:30px;\">");
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            SYMB = DS.Tables[0].Rows[i]["SYMB"].ToString();
            SYMB = Uri.EscapeUriString(SYMB);
            SYMB = Uri.EscapeUriString(SYMB);
            XMDM=cssz(DS.Tables[0].Rows[i]["BBBM"].ToString());
            URL = "http://" + GetLocalIPAddress() + ":" + GetTomcatPort() + "/WebReport/ReportServer?reportlet=" + SYMB + "" + XMDM + "&" + USERDM + "&" + HSZXDM + "&" + YY;
            if (t == 0)
            {
                sb.Append("<tr>");
            }
            sb.Append("<td style=\" text-align:center; \">");
            sb.Append("<a href=\"#\" style=\" width:150px; height:26px; cursor:pointer; text-align:center\" onclick=\"OpenBB('" + DS.Tables[0].Rows[i]["BBBM"].ToString() + "','" + DS.Tables[0].Rows[i]["MLMXMC"].ToString() + "','" + URL + "')\">" + DS.Tables[0].Rows[i]["MLMXMC"].ToString() + "</a> ");
            sb.Append("</td>");
            t++;
            if (t == 6)
            {
                sb.Append("</tr>");
                t = 0;
            }
        }
        sb.Append("</table>");
        return sb.ToString();   
    }
    /// <summary>
    /// 获取本机的IP地址
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetLocalIPAddress()
    {
        System.Net.IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
        string strNativeIP = "";
        string strServerIP = "";
        if (addressList.Length > 1)
        {
            strNativeIP = addressList[0].ToString();
            strServerIP = addressList[1].ToString();
        }
        else if (addressList.Length == 1)
        {
            strServerIP = addressList[0].ToString();
        }
        return strServerIP;
    }
    /// <summary>
    /// 获取当前报表所要用的Tomcat的端口号
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetTomcatPort()
    {
        string Port = PubConstant.TomcatPort;
        return Port;
    }
    /// <summary>
    /// 参数设置
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string cssz(string id)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "select ID,MLDM,CSMC,CSVALUE,TYPE from BBGL_MLMX_CSSZ WHERE MLDM='"+id+"' ";
        DataSet set = BLL.Query(sql);
        string cs = "";
        if (set.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                cs += set.Tables[0].Rows[i]["CSMC"].ToString() + "=" + set.Tables[0].Rows[i]["CSVALUE"].ToString();
                if (i < set.Tables[0].Rows.Count - 1)
                {
                    cs += "&";
                }
            }
        }
        if (cs != "")
        {
            cs = "&" + cs;
        }
        return cs;
    }
    [AjaxPro.AjaxMethod]
    public void bb_czrz(string mc)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string year = DateTime.Now.Year.ToString();
        string sql = "select * from sysobjects where name='BB_CZRZ_" + year + "'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            string time = DateTime.Now.ToString();
            string czr = Session["USER_ID"].ToString();
            sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('" + mc + "','" + time + "','" + czr + "')";
            bll.ExecuteSql(sql);
        }
        else
        {
            sql = "create table BB_CZRZ_" + year + "( ";
            sql += "DM NUMERIC(10,0) IDENTITY,";
            sql += "RZMC NCHAR(100), ";
            sql += "MX NCHAR(100), ";
            sql += "DATE NCHAR(100), ";
            sql += "CZR NCHAR(100) ";
            sql += ")";
            int count = bll.ExecuteSql(sql);
            if (count == -1)
            {
                string time = DateTime.Now.ToString();
                string czr = Session["USER_ID"].ToString();
                sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('" + mc + "','" + time + "','" + czr + "')";
                bll.ExecuteSql(sql);
            }
        }

    }
}
