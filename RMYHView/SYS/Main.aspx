﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="SYS_Main" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="WinJS" class="easyui-window" title="角色选取" closed="true" style="width:900px;height:450px;padding:20px;text-align: center; ">
        <div style=" text-align:left">
           <div style="height:30px">
                角色名称：<input type="text" id="TxtJS" style="width:120px" />&nbsp;&nbsp;
                <input type="button" value="查询" onclick="GetJSList()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
                <br />        <br />
           </div>
           <div id="JSXQ" style="overflow:auto"></div>
       </div>
 </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<script type="text/javascript">
    function GetJSList() {
        var rtnstr = SYS_Main.GetJSList($("#TxtJS").val()).value;
        document.getElementById("JSXQ").innerHTML = rtnstr;
        $("#JSXQ INPUT[type=button]").attr("style", "display:none")
    }
    function WinJS(Flag, BZ) {
        if (Flag == true) {
            $("#WinJS").window('open');
            GetJSList();
        }
        else {
            if (BZ == -1) {
                $("#HidJSBZ").val("1");
            } else {
                $("#HidJSBZ").val("");
            }
            $("#WinJS").window('close');
        }
    }
</script>
</asp:Content>

