﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using RMYH.BLL;
using System.Data;

public partial class SYS_JHDXSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_JHDXSZ));
    }
    #region Ajax方法
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144557");
    }
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <param name="id">主键ID</param>
    /// <param name="trid">Tr的ID</param>
    /// <param name="intimgcount">Tr行数</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return  GetDataList.GetDataListstring("10144557", "", new string[] { }, new string[] { }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 获取汉字首字母（可包含多个汉字）
    /// </summary>
    /// <param name="strText">汉子和字母的字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetChineseSpell(string strText)
    {
        int len = strText.Length;
        string myStr = string.Empty;
        foreach (char C in strText) 
        {
            if ((int)C >= 33 && (int)C <= 126)
            {
                //字母和符号原样保留 
                myStr += C.ToString();
            }
            else
            {
                //累加拼音声母 
                myStr += GetPYChar(C.ToString());
            } 
        }
        return myStr;
    }

    /// <summary>
    /// 获取汉字的首字母，只能输入汉字
    /// </summary>
    /// <param name="c">需要解析的汉字</param>
    /// <returns></returns>
    public string GetPYChar(string c)
    {
        byte[] array = new byte[2];
        array = System.Text.Encoding.Default.GetBytes(c);
        int i = (short)(array[0] - '\0') * 256 + ((short)(array[1] - '\0'));
        if (i < 0xB0A1) return "*";
        if (i < 0xB0C5) return "A";
        if (i < 0xB2C1) return "B";
        if (i < 0xB4EE) return "C";
        if (i < 0xB6EA) return "D";
        if (i < 0xB7A2) return "E";
        if (i < 0xB8C1) return "F";
        if (i < 0xB9FE) return "G";
        if (i < 0xBBF7) return "H";
        if (i < 0xBFA6) return "J";
        if (i < 0xC0AC) return "K";
        if (i < 0xC2E8) return "L";
        if (i < 0xC4C3) return "M";
        if (i < 0xC5B6) return "N";
        if (i < 0xC5BE) return "O";
        if (i < 0xC6DA) return "P";
        if (i < 0xC8BB) return "Q";
        if (i < 0xC8F6) return "R";
        if (i < 0xCBFA) return "S";
        if (i < 0xCDDA) return "T";
        if (i < 0xCEF4) return "W";
        if (i < 0xD1B9) return "X";
        if (i < 0xD4D1) return "Y";
        if (i < 0xD7FA) return "Z";
        return "*";
    }
    /// <summary>
    /// 根据代码获取计划对象的相关信息
    /// </summary>
    /// <param name="DM">计划对象代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHDXStr(string DM)
    {
        string Str = string.Empty;
        DataSet DS = new DataSet(); 
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS=BLL.Query("SELECT XMMC,ZFBM,SJLX FROM TB_JSDX WHERE DM='"+DM+"'");
        if (DS.Tables[0].Rows.Count > 0) 
        {
            Str = DS.Tables[0].Rows[0]["XMMC"].ToString().Trim() + "|" + DS.Tables[0].Rows[0]["ZFBM"].ToString().Trim() + "|" + DS.Tables[0].Rows[0]["SJLX"].ToString().Trim();
        }
        return Str;
    }
    /// <summary>
    /// 获取计算对象表中最大的顺序号
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMAXSX()
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        return　BLL.Query("SELECT ISNULL(MAX(SX),0) SX FROM TB_JSDX").Tables[0].Rows[0]["SX"].ToString().Trim();
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_JSDX", id, "DM");
        return ret;
    }
    /// <summary>
    /// 更新数据
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="fileds"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string mldm)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.Update("10144557", "TB_JSDX", fileds, values, "DM", ID);
        }
        catch
        {

        }
        return ret;
    }
    /// <summary>
    /// 计算对象信息导出
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DC() {
        StringBuilder strSql=new StringBuilder();
        TB_ZDSXBBLL TB = new TB_ZDSXBBLL();
        TB_TABLESXBLL BLL=new TB_TABLESXBLL();
        strSql.Append("SELECT JS.XMMC 计算对象名称,ZFBM 字母别名,CS.XMMC 数据类型,DWMC 默认计量单位,SX 顺序 FROM TB_JSDX JS");
        strSql.Append(" LEFT JOIN XT_CSSZ CS ON JS.SJLX=CONVERT(VARCHAR,CS.XMDH_A) AND CS.XMFL='YSJSDX'");
        strSql.Append(" LEFT JOIN TB_JLDW JL ON JL.DWDM=JS.JLDW");
        return BLL.DataToExcelByNPOI(TB.Query(strSql.ToString()).Tables[0], "计算对象导出.xls", "计算对象信息导出");
    }
    #endregion
}