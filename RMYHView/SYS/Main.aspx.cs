﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SYS_Main : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_Main));
    }
    #region
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSList(string JSName)
    {
        return GetDataList.GetDataListstring("10144511", "", new string[] { ":NAME" }, new string[] { JSName }, false, "", "", "");
    }
    #endregion
}