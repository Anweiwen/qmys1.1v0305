﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Text;
using System.Data;
using System.Collections;

public partial class DYSZ_GDDY : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(DYSZ_GDDY));
    }
    #region Ajax方法
    [AjaxPro.AjaxMethod]
    public string LoadList()
    
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder sb = new StringBuilder();
        DataSet ds = bll.GetList("1=2");
        DataRow dr;
        DataRow dr1;
        DataRow dr3;
        string sql1 = "SELECT DYDM,DYMC FROM TB_ZYFPFS where SYBZ='1' AND DYBZ='2'";
        DataSet ds1 = bll.Query(sql1);

        string sql2 = "SELECT ZYDM,ZYMC FROM TB_JHZYML WHERE YJDBZ='1' AND SYBZ='1'";
        DataSet ds2 = bll.Query(sql2);

        StringBuilder strb = new StringBuilder();
        strb.Append("SELECT B.DYDM,B.DYMC, ISNULL(DYBL,0) DYBL,C.ZYDM FROM TB_GDDYBL A left join TB_ZYFPFS B   on A.DYDM=B.DYDM  left join TB_JHZYML C on A.ZYDM=C.ZYDM AND   C.YJDBZ='1' AND C.SYBZ='1'");
       
        String sql3 = strb.ToString();
        DataSet ds3 = bll.Query(sql3);

        //设置宽度
        int withs = 0;
        withs = 300 + ds2.Tables[0].Rows.Count * 120;

        sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"table-layout:fixed\"");
        sb.Append(" width=\"" + withs + "px\" >");
        sb.Append("<tr id=\"trDYDM\" align=\"center\" class=\"summary-title\" >");
        sb.Append("<td width='65px'>&nbsp;</td>");
        sb.Append("<td width='50px' >编号</td>");
        sb.Append("<td  width='120px' >动因名称</td>");

        dr = ds.Tables[0].NewRow();
        dr["SFXS"] = "1";
        dr["SRFS"] = "0";
        dr["FIELDNAME"] = "DYMC";
        //dr["XSCD"] = "0";
        dr["SRKJ"] = "0";
        dr["LEN"] = "50";
        ds.Tables[0].Rows.Add(dr);

        dr3 = ds.Tables[0].NewRow();
        dr3["SFXS"] = "0";
        dr3["SRFS"] = "2";
        dr3["FIELDNAME"] = "DYDM";
        //dr3["XSCD"] = "0";
        dr3["SRKJ"] = "0";
        dr3["LEN"] = "50";
        ds.Tables[0].Rows.Add(dr3);



        for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
        {
            sb.Append("<td id=\"tdbl_" + ds2.Tables[0].Rows[i]["ZYDM"].ToString() + "\" width='120px' >");
            sb.Append("按" + ds2.Tables[0].Rows[i]["ZYMC"].ToString() + "分配");
            sb.Append("</td>");

            ds1.Tables[0].Columns.Add("DYBL" + ds2.Tables[0].Rows[i]["ZYDM"].ToString());
            dr1 = ds.Tables[0].NewRow();
            dr1["SFXS"] = "1";
            dr1["SRFS"] = "1";
            dr1["FIELDNAME"] = "DYBL" + ds2.Tables[0].Rows[i]["ZYDM"].ToString();
            //dr1["XSCD"] = "0";
            dr1["SRKJ"] = "0";
            dr1["LEN"] = "50";
            ds.Tables[0].Rows.Add(dr1);
        }

        sb.Append("</tr>");
        for (int j = 0; j < ds3.Tables[0].Rows.Count; j++)
        {
            for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
            {
                if (ds3.Tables[0].Rows[j]["DYDM"].ToString() == ds1.Tables[0].Rows[i]["DYDM"].ToString())
                {
                    ds1.Tables[0].Rows[i]["DYBL" + ds3.Tables[0].Rows[j]["ZYDM"].ToString()] = ds3.Tables[0].Rows[j]["DYBL"].ToString();
                }
            }
        }
        GetDataList.getdata(ds1.Tables[0], ds, "", "0", "tr", ref sb, "", null, false, false,"");
        sb.Append("</table>");
        return sb.ToString();

    }
    // <summary>
    //添加数据，更新数据
    // </summary>
    // <returns></returns>
    [AjaxPro.AjaxMethod]
    public string update(string DYDM, string FPFSDM, string DATAVALUE)
    {
        string[] arrFPFSDM = FPFSDM.Split(',');
        string[] arrDATAVALUE = DATAVALUE.Split('|');

        //删除数据,删除对应作业代码的所有数据
        string delsql = "delete from TB_GDDYBL where DYDM='" + DYDM + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(delsql);
        string insertsql1 = "", insertsql2="";
        ArrayList arr = new ArrayList();

        insertsql1 = "insert into TB_GDDYBL(DYDM,ZYDM,DYBL) Values({0},{1},{2})";
        insertsql2 = "insert into TB_GDDYBL(DYDM,ZYDM) Values({0},{1})";
        for (int i = 0; i < arrDATAVALUE.Length; i++)
        {
            if (arrDATAVALUE[i] != "")
            {
                if (arrDATAVALUE[i] != "" && arrDATAVALUE[i]!=null)
                {
                    arr.Add(string.Format(insertsql1, "'"+DYDM+"'","'"+ arrFPFSDM[i]+"'", decimal.Parse(arrDATAVALUE[i])));
                }
                else
                {
                    arr.Add(string.Format(insertsql2, "'" + DYDM + "'", "'"+arrFPFSDM[i]+"'"));
                }
                
            }
        }
        int rows = bll.ExecuteSql((string[])arr.ToArray(typeof(string)), null);

        return "";
    }
    #endregion
}