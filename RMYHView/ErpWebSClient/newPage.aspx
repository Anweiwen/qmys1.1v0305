﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="newPage.aspx.cs" Inherits="ErpWebSClient_newPage" %>

<!DOCTYPE>
<meta http-equiv=”X-UA-Compatible” content=”IE=edge,chrome=1″/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>无标题页</title>
    <link href="../Spread/jquery-easyui-1.5.2/themes/default/easyui.css" rel="stylesheet"
        type="text/css" />
    <link href="../Spread/jquery-easyui-1.5.2/themes/icon.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/style1.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/style.css"/>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.min.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.easyui.mobile.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/Main.js" type="text/javascript"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    

    <script type="text/javascript">

        (function (window) {

            var gsacbs = undefined, ischeck = undefined;

            window.getSoap = function () {
                $.ajax({
                    type: 'post',
                    //url: 'WebService.ashx?action=WebServie',
                    url: 'Parse.ashx?action=WebServie',
                    data: {
                        wsdl: $("#wsdl").textbox('getText'),
                        user: $("#user").textbox('getValue'),
                        pass: $("#pass").textbox('getValue')
                    },
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        // res = JSON.parse(result);
                        $('#tg').treegrid('loadData', result);
                    },
                    error: function () {
                        alert("错误!");
                    }
                })
            }

            window.initGrid = function init() {
                $('#tg').treegrid({
                    idField: 'id',
                    fit: true,
                    fitColumns: true,
                    lines: true,
                    treeField: 'name',
                    loadMsg: "please wait …",
                    singleSelect: true,
                    selectOnCheck: true,
                    checkbox: true,
                    cascadeCheck: true,
                    columns: [[
                    { field: 'id', title: 'ID', width: fixWidth(0), align: 'left', hidden: true },
                    { field: 'name', title: '服务名', width: fixWidth(0.1), align: 'left', styler: setColor },
                    { field: 'type', title: '参数类型', width: fixWidth(0.1), align: 'left', hidden: true },
                    { field: 'protocol', title: '参数类型', width: fixWidth(0.1), align: 'left', hidden: true },
                    { field: 'value', title: '参数值', width: fixWidth(0.15), align: 'left',
                        editor: {
                            type: 'textbox'
                        }
                    },
                    { field: 'progress', title: '采集进度', width: fixWidth(0.2), align: 'left', hidden: true }
                ]],
                    onLoadSuccess: function (row, data) {
                        $("a[name='opera']").linkbutton({
                            text: '采集',
                            width: 110,
                            plain: true,
                            iconCls: 'icon-add',
                            toggle: true
                        });
                        $("a[name='probar']").progressbar({ width: 110 });
                    },
                    onBeforeEdit: function (row) {
                        ischeck = $("[node-id=" + row._parentId + "] .tree-checkbox").attr("class");
                        timeWidget(row);
                    },
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function (rowIndex, rowData, changes) {
                        //var id = rowIndex._parentId
                        $("[node-id=" + rowIndex._parentId + "] .tree-checkbox").attr("class", ischeck);
                        $("[node-id=" + rowIndex.id + "] .tree-checkbox").attr("class", ischeck);
                        ischeck = undefined;
                    },
                    // 双击选择行
                    onDblClickCell: function (index, rowData, value) {
                        //$('#tg').treegrid('getChildren', rowData.id).length <= 0  判断叶子节点
                        if (rowData.type != "WebService" && rowData.type != "Soap" && rowData.type != "interface") {
                            if (gsacbs != undefined) {
                                $('#tg').treegrid('endEdit', gsacbs);
                                gsacbs = rowData.id;
                                $('#tg').treegrid('select', gsacbs);
                                $('#tg').treegrid('beginEdit', gsacbs);
                            }
                            if (gsacbs == undefined) {
                                $('#tg').treegrid('beginEdit', rowData.id);
                                gsacbs = rowData.id;
                            }
                        }
                    }

                });
            }

            //设置easyuI 对应字段的时间控件
            window.timeWidget = function (row) {
                var funname = $("#tg").treegrid("getParent", row.id).name;

                switch (row.name) {
                    case "IM_XGSJ":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datebox'}}]);
                        break;
                    case "strTimestamp":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datetimebox'}}]);
                        break;
                    case "strStartReportDate":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datebox'}}]);
                        break;
                    case "strEndReportDate":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datebox'}}]);
                        break;
                    case "strStartTimestamp":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datetimebox'}}]);
                        break;
                    case "strEndTimestamp":
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'datetimebox'}}]);
                        break;
                    default:
                        $("#tg").datagrid('addEditor', [{ field: 'value', editor: { type: 'textbox'}}]);
                }
            }

            window.initTree = function () {
                $('#serTree').tree({
                    onClick: function (node) {
                        getFunction(node.id, node.text, node.attributes);  // alert node text property when clicked
                    }
                });
            }

            window.getFunction = function (id, text, attr) {
                $("#tg").treegrid("unselectAll");

                //请求查询wsdl信息
                $.ajax({
                    type: 'post',
                    url: 'Parse.ashx?action=GetWsdlData',
                    data: {
                        id: id,
                        text: text,
                        attr: attr
                    },
                    async: true,
                    success: function (result) {
                        var res = JSON.parse(result);
                        //$("#wsdldm").val(res.WSDLDM);
                        $("#wsdl").textbox("setValue", res.WSDL); $("#wsdl").textbox("setText", res.WSDL);
                        $("#user").textbox("setValue", res.UNAME.trim()); $("#user").textbox("setText", res.UNAME.trim());
                        $("#pass").textbox("setValue", res.PASS.trim()); $("#pass").textbox("setText", res.PASS.trim());
                    },
                    error: function () {
                        alert("错误!");
                    }
                })
                //解析wsdl
                $.ajax({
                    type: 'post',
                    url: 'Parse.ashx?action=GetXmlData',
                    data: {
                        id: id
                    },
                    async: true,
                    success: function (result) {
                        res = JSON.parse(result);
                        $('#tg').treegrid('loadData', res);
                        //$("#tg").treegrid("CheckAll", $("#tg").treegrid("getRoot").id); //默认选中全部
                    },
                    error: function () {
                        alert("错误!");
                    }
                })


            }

            window.setColor = function (value, rowData, rowIndex) {
                if (rowData.type == "interface") {
                    return 'background-color:#ffee00;color:red;';
                }
            }

            window.progressFormatter = function (value) {
                $("#ft").attr("style", "width:" + value + "%; height: 20px; line-height: 20px;background:#53CA22;color:#FF0000;text-align:center;") // 修改进度条进度
                $("#ft").text("" + value + "%") //修改进度值
                // $("#ft").css("background-color", "#53CA22"); //修改进度条颜色（绿色）

                //                var htmlstr = ""; value = 0;
                //                if (rowData.type == "interface") //判断是接口方法
                //                {
                //                    var s = '<div style="width:100%;border:1px solid #ccc">' +
                //		    			    '<div id="bar-' + rowData.id + '" style="width:' + value + '%;background:#53CA22;color:#FF0000;text-align:center">' + value + '%' + '</div>'
                //                    '</div>';
                //                    return s;
                //                }
                //                return htmlstr;
            }

            window.fixWidth = function (percent) {
                return document.body.clientWidth * percent; //这里你可以自己做调整  
            }


            //保存soap
            window.Save = function () {
                $('#tg').treegrid('endEdit', gsacbs);
                gsacbs = undefined;
                var arr = [];
                var root = $("#tg").treegrid("getRoot");
                var children = $("#tg").treegrid("getChildren", $("#tg").treegrid("getRoot").id);
                arr.push(root);
                var dataArr = getSoapData(root, children);
                $.ajax({
                    type: 'post',
                    url: 'Parse.ashx?action=SaveSoap',
                    data: {
                        wsdldm: $("#wsdldm").val(),
                        wsdl: $("#wsdl").textbox("getText"),
                        soapname: $("#tg").treegrid("getRoot").name,
                        name: $("#user").textbox("getValue"),
                        pass: $("#pass").textbox("getValue"),
                        soap: JSON.stringify(dataArr)
                    },
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        getWsdl();
                        if (result[0].res == "1") {
                            prompt("操作成功！！");
                        }
                       //$("#wsdl").textbox("setValue", result[0].id);
                        $("#wsdl").textbox("setText", result[0].wsdl);
                    },
                    error: function (error) {
                        alert(error);
                    }
                })
            }

            window.getSoapData = function (root, rows) {
                var arr = []; var item = [];
                item.push(root.id);
                item.push(root._parentId);
                item.push(root.name);
                item.push(root.value);
                item.push(root.type);
                item.push(root.checkState); 
                arr.push(item);
                for (var i = 0; i < rows.length; i++) {
                    var items = [];
                    items.push(rows[i].id);
                    items.push(rows[i]._parentId);
                    items.push(rows[i].name);
                    items.push(rows[i].value);
                    items.push(rows[i].type);
                    items.push(rows[i].checkState);
                    arr.push(items);
                }
                return arr;
            }

            window.getAllData = function (id) {
                var root = $("#tg").treegrid("getRoot");
                if ($("#tg").treegrid("getRoot").children != undefined) {
                    var count = $("#tg").treegrid("getRoot").children.length;
                    for (var i = 0; i < count; i++) {

                    }
                }

            }

            //执行采集接口的方法
            window.getData = function () {
                var select = $('#tg').datagrid('getSelected');
                if (select == null) { prompt("请选择采集函数！"); return; }
                var row = $("#tg").treegrid("getParent", $("#tg").treegrid("getSelected").id)//找到参数节点的父节点，就是方法节点
                if (row != null) {
                    if (row.type == "interface") { $("#tg").treegrid("select", row.id); } //选中方法节点
                }

                $('#tg').treegrid('endEdit', gsacbs);
                gsacbs = undefined;

                var getSelectRow = $("#tg").treegrid("getSelected");
                if (getSelectRow.type != "interface") {
                    prompt("请选择采集接口！！");
                    return;
                } else {
                    var funName = $("#tg").treegrid("getSelected").name;  //获取方法名称
                    var protocol = $("#tg").treegrid("getSelected").protocol;
                    var Parameter = getParameter(); //获取方法名和参数值
                    $.ajax({
                        type: 'post',
                        //                        dataType: "json",
                        url: 'WebService.ashx?action=WebServie',
                        data: {
                            wsdl: $("#wsdl").textbox('getValue'),
                            user: $("#user").textbox('getValue'),
                            pass: $("#pass").textbox('getValue'),
                            funname: funName,
                            protocol: protocol,
                            parameter: JSON.stringify(Parameter)
                        },
                        async: false,
                        success: function (data) {
                            //                            $("#data").datagrid("loadData", { rows: [] });   //清楚数据
                            //                            $('#data').datagrid({ columns: [[]], rownumbers: false, pagination: false });    //先清楚标题和数据
                            //                            $('#data').empty();
                            //                            $("#data").datagrid({
                            //                                columns: [data.titleds]    //动态取标题
                            //                            });
                            //                            $("#data").datagrid("loadData", { "total": data.total, rows: data.rows });   //动态取数据
                            $("#grid").empty();
                            $("#grid").append(data);
                            $.parser.parse($("#grid"));
                        },
                        error: function (result) {
                            alert(result);
                        }
                    })
                }
            }

            window.CJ = function () {
                $('#tg').treegrid('endEdit', gsacbs);
                gsacbs = undefined;
                $('#result').datagrid('loadData', []); //清空操作提示
                var res = getSelect();
                if (res.length < 2) {
                    prompt("请选择采集节点！！！！！！！");
                    return;
                }
                var SerName = res[0];
                var funs = res[1];
                var count = funs.length;
                var pargram = Math.round(100 / count);
                for (var i = 0; i < count; i++) {
                    $.ajax({
                        type: 'post',
                        url: 'WebService.ashx?action=WebServie',
                        data: {
                            wsdl: $("#wsdl").textbox('getText'),
                            user: $("#user").textbox('getValue'),
                            pass: $("#pass").textbox('getValue'),
                            SerName: SerName,
                            // parameter: JSON.stringify({ "getStationAndTimeByTrainCode": [{ "TrainCode": "1" }, { "UserID": "22"}] })
                            parameter: JSON.stringify(funs[i])
                        },
                        async: false,
                        success: function (data) {
                            progressFormatter(pargram); //设置进度条
                            var res = JSON.parse(data);
                            $('#result').datagrid('appendRow', res);
                        },
                        error: function (result) {
                            alert(result);
                            MaskUtil.unmask();
                        }
                    })
                }

            }


            window.getParameter = function (rtn) {
                var selectRow = $("#tg").treegrid("getSelected")
                var count = $("#tg").treegrid("getChildren", selectRow.id).length;
                var params = [];
                for (var i = 0; i < count; i++) {
                    var map = [];
                    var key = $("#tg").treegrid("getChildren", selectRow.id)[i].name;
                    var value = $("#tg").treegrid("getChildren", selectRow.id)[i].value;
                    map.push(key);
                    map.push(value);
                    params.push(map);
                }
                return params;
            }

            window.getWsdl = function () {
                $.ajax({
                    type: 'post',
                    url: 'Parse.ashx?action=GetWSDL',
                    data: {

                    },
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        $("#serTree").tree('loadData', result);
                    },
                    error: function () {
                        alert("错误!");
                    }
                })
            }

            window.getSelect = function () {
                //[{"getA": [{ "a":"a1" },{ "b": "b1"}]},{"getB":[{ "a":"a1" },{ "b":"b1"}]}]
                var rows = $("#tg").treegrid("getAllChecked", true); //easyui扩展方法，获取treegrid 复选框选中的节点
                var len = rows.length;
                var arrlist = [], arr = [];
                for (var i = 0; i < len; i++) {
                    if (rows[i].type == 'WebService') {//记录当前WsbService 的名称
                        arrlist.push(rows[i].name);
                    }
                    if (rows[i].type == 'interface') {
                        var obj_id = rows[i].id;
                        var obj = {};
                        if (rows[i].children == undefined) {//表明方法节点没有子节点，只返回方法名和空的参数数组
                            obj[rows[i].name] = [];
                            arr.push(obj);
                        } else {
                            var ar = []; var key = rows[i].name;
                            for (var j = i + 1; j < len; j++) {
                                var obj2 = {};
                                if (rows[j]._parentId == obj_id) {//判断当前节点的父节点，表明是当前方法的参数节点
                                    var value = rows[j].value == undefined ? "" : rows[j].value;
                                    obj2[rows[j].name] = value;
                                    ar.push(obj2);
                                } else {//不是当前节点的参数节点，跳出循环
                                    i = j - 1; //将最外层循环变量置为当前循环变量-1 ，减少最外层循环次数
                                    break;
                                }
                            }
                            obj[key] = ar;
                            arr.push(obj);
                        }
                    }
                }
                arrlist.push(arr);
                return arrlist;
            }

            window.Main = function () {
                $.ajax({
                    type: 'post',
                    url: 'CJ.ashx?action=Scheduler',
                    data: {

                    },
                    async: false,
                    success: function (result) {
                        $("#serTree").tree('loadData', result);
                    },
                    error: function () {
                        alert("错误!");
                    }
                })
            }

            //扩展datagrid:动态添加删除editor
            $.extend($.fn.datagrid.methods, {
                addEditor: function (jq, param) {
                    if (param instanceof Array) {
                        $.each(param, function (index, item) {
                            var e = $(jq).datagrid('getColumnOption', item.field);
                            e.editor = item.editor;
                        });
                    } else {
                        var e = $(jq).datagrid('getColumnOption', param.field);
                        e.editor = param.editor;
                    }
                },
                removeEditor: function (jq, param) {
                    if (param instanceof Array) {
                        $.each(param, function (index, item) {
                            var e = $(jq).datagrid('getColumnOption', item);
                            e.editor = {};
                        });
                    } else {
                        var e = $(jq).datagrid('getColumnOption', param);
                        e.editor = {};
                    }
                }
            });


            $.extend($.fn.treegrid.methods, {
                //iscontains是否包含父节点（即子节点被选中时是否也取父节点）
                getAllChecked: function (jq, iscontains) {
                    var keyValues = [];
                    /*
                    tree-checkbox2 有子节点被选中的css
                    tree-checkbox1 节点被选中的css
                    tree-checkbox0 节点未选中的css
                    */
                    if (iscontains) {
                        var childCheckNodes = jq.treegrid("getPanel").find(".tree-checkbox2");
                        var len = childCheckNodes.length;
                        for (var i = 0; i < len; i++) {
                            var keyValue2 = $($(childCheckNodes[i]).closest('tr')[0]).attr("node-id");
                            var nowrow = $('#tg').treegrid("find", keyValue2);
                            keyValues.push(nowrow);
                        }
                    }

                    var checkNodes = jq.treegrid("getPanel").find(".tree-checkbox1");
                    var len = checkNodes.length;
                    for (var i = 0; i < len; i++) {
                        var keyValue1 = $($(checkNodes[i]).closest('tr')[0]).attr("node-id");
                        var nowrow = $('#tg').treegrid("find", keyValue1);
                        keyValues.push(nowrow);
                    }

                    return keyValues;
                },
                //全选
                CheckAll: function (jq, rootId) {
                    $("[node-id=" + rootId + "] .tree-checkbox").click();
                    $(".tree-checkbox").attr("class", "tree-checkbox tree-checkbox1");
                },
                //取消全选
                UnCheckAll: function (jq, rootId) {
                    var res = $("[node-id=" + rootId + "] .tree-checkbox").attr("class");
                    if (res == "tree-checkbox tree-checkbox1") { $("[node-id=" + rootId + "] .tree-checkbox").click() }
                    $(".tree-checkbox").attr("class", "tree-checkbox tree-checkbox0");
                },
                CheckById: function (jq, id) {
                    $("[node-id=" + id + "] .tree-checkbox").click();
                    $("[node-id=" + id + "] .tree-checkbox").attr("class", "tree-checkbox tree-checkbox1");
                },
                UnCheckById: function (jq, id) {
                    $("[node-id=" + id + "] .tree-checkbox").attr("class", "tree-checkbox tree-checkbox0");
                }
            });




            window.MaskUtil = (function () {

                var $mask, $maskMsg;

                var defMsg = '正在处理，请稍待。。。';

                function init() {
                    if (!$mask) {
                        $mask = $("<div class=\"datagrid-mask mymask\"></div>").appendTo("body");
                    }
                    if (!$maskMsg) {
                        $maskMsg = $("<div class=\"datagrid-mask-msg mymask\">" + defMsg + "</div>")
                .appendTo("body").css({ 'font-size': '12px' });
                    }

                    $mask.css({ width: "100%", height: $(document).height() });

                    var scrollTop = $(document.body).scrollTop();

                    $maskMsg.css({
                        left: ($(document.body).outerWidth(true) - 190) / 2
            , top: (($(window).height() - 45) / 2) + scrollTop
                    });

                }

                return {
                    mask: function (msg) {
                        init();
                        $mask.show();
                        $maskMsg.html(msg || defMsg).show();
                    }
        , unmask: function () {
            $mask.hide();
            $maskMsg.hide();
        }
                }

            } ());



            window.prompt = function (rtn) {
                $.messager.show({
                    title: '提示',
                    msg: rtn,
                    timeout: 1000,
                    showType: 'slide'
                });
            }
        })(window)




    $(function () {
        initGrid();
        initTree();
        getWsdl();
    })
    </script>
</head>
<body>
<form id="form1" runat="server" style="width:100%; height:100%;">
  <div id="cc" class="easyui-layout" style="width:1000px;height:400px;"fit="true">
    <div data-options="region:'west',title:'WebService服务',split:true" style="width:15%;">
        <ul id="serTree" class="easyui-tree"></ul>
    </div>
    <div data-options="region:'center',title:''" style="padding:0px;background:#eee;">
        <div id="cc1" class="easyui-layout" style="width:80%;height:400px;" fit="true">
             <div data-options="region:'center',title:''" style="padding:5px;background:#eee;">
                <table id="tg" title="采集接口调用：" class="easyui-treegrid" data-options="toolbar:'#menu'" style="" fit=true>
                </table>
             </div>
             <div data-options="region:'south',title:'',split:true" style="height:100px;" id="grid">
                <table id="result" title="" class="easyui-datagrid" data-options="footer:'#ft',fitColumns:true" style="" fit=true>
                    <thead>
		                <tr>
			                <th data-options="field:'code',width:50">code</th>
			                <th data-options="field:'message',width:200">message</th>
		                </tr>
                    </thead>
                </table>
             </div>
        </div>
    </div>
</div>

        <div id="menu" style="padding:5px;height:auto">
<%--		    <div style="margin-bottom:5px">
			    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"></a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true"></a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true"></a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true"></a>
		    </div>--%>
		    <div>
                      <input id="wsdldm"  type="hidden"/>
			    Wsdl: <input id="wsdl" class="easyui-textbox" style="width:40%">
			    账号: <input id="user" class="easyui-textbox" style="width:80px">
			    口令: <input id="pass" class="easyui-textbox" type="password" style="width:80px">
			    <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="javascript:getSoap()">获取</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="javascript:Save()">保存</a>
                <%--<a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:getData()">采集</a>--%>
                <a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:CJ()">采集</a>
                <%--<a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:Main()">执行</a>--%>
		    </div>
	    </div>

        <div id="ft" style="">
            <div style="width:100%;border:1px solid #ccc">
               <div id="buttom" style="width:0%;background:#53CA22;color:#FF0000;text-align:center">0%</div>
            </div>
	    </div>

</form>
</body>
</html>

