﻿<%@ WebHandler Language="C#" Class="BDXMDataHandler" %>

using System.Data;
using System.Text;
using System.Web;
using RMYH.BLL;
using System.Web.SessionState;

public class BDXMDataHandler : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        switch (action)
        {
            case "getCWHSFS":
                string index = context.Request.QueryString["index"];
                string year = context.Session["YY"].ToString();
                res = GetCWHSFS(index, year);
                break;
            case "getFLBH":
                index = context.Request.QueryString["index"];
                string flbh = context.Request.QueryString["flbh"];
                year = context.Session["YY"].ToString();
                res = GetFLBH(index, flbh, year);
                break;
            case "getKMBH":
                index = context.Request.QueryString["index"];
                year = context.Session["YY"].ToString();
                res = GetKMBH(index, year);
                break;
        }
        context.Response.Write(res);
    }

    private string GetCWHSFS(string index, string year)
    {
        string sql = "SELECT F_FLBH,F_FLMC FROM  TB_CWLSHSFL  WHERE '1'='" + index + "' AND   YY='" + year +
                     "' UNION SELECT '-1' F_FLBH,'产品核算分类' F_FLMC FROM XT_CSSZ  WHERE '2'='" + index +
                     "'  AND XMFL='A001' AND XMDH_A=1 UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSYSLB WHERE '3'='" + index +
                     "' AND YY='" + year + "'  UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSZDFZLB WHERE '4'='" + index +
                     "'  ORDER BY F_FLBH";
        return ExecuteSql(sql);
    }

    private string GetFLBH(string index, string flbh, string year)
    {
        string sql =
            "SELECT F_HSBH, F_HSMC  F_HSMC FROM  TB_CWLSHSZD WHERE '1'='" + index + "' AND   YY='" + year +
            "' AND F_FLBH='" + flbh + "' UNION SELECT F_CPBH,F_CPMC F_HSMC FROM  TB_CWLSCPZD WHERE '2'='" + index +
            "' AND YY='" + year + "' UNION SELECT F_YSBH,F_YSMC F_HSMC FROM TB_CWLSYSZD WHERE '3'='" + index +
            "' AND YY='" + year + "' AND F_LBBH='" + flbh + "' UNION SELECT F_ZFBH,F_ZFMC F_HSMC FROM  TB_CWLSZFXM WHERE '4'='" + index +
            "' AND F_LBBH='" + flbh + "' ORDER BY 1";
        return ExecuteSql(sql);
    }

    private string GetKMBH(string index, string year)
    {
        string sql =
            "SELECT F_KMBH,LTRIM(F_KMMC) CBMC,SUBSTRING(F_KMBH,1,SZCS2-SZCS) FBBM,F_MX YJDBZ  FROM TB_CWLSKMXX A,XT_CSSZ B Where YY='" + year + "' AND A.F_KMJS=B.XMDH_A  AND B.XMFL='S004' AND CONVERT(CHAR,B.XMDH_B)='" + year + "' AND ISNULL(SUBSTRING(F_KMBH,1,SZCS2-SZCS),'0')='" + index + "' ORDER BY F_KMBH";
        return ExecuteSql(sql);
    }

    private string ExecuteSql(string sql)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(sql);
        StringBuilder rtn = new StringBuilder();
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            rtn.Append("<option value='" + ds.Tables[0].Rows[i][0] + "'>");
            rtn.Append(ds.Tables[0].Rows[i][1]);
            rtn.Append("</option>");
        }
        return rtn.ToString();
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}