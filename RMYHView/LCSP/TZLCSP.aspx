﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TZLCSP.aspx.cs" Inherits="LCSP_TZLCSP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <title>审批流程设置</title>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css"/>
     <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css"/>
     <link rel="stylesheet" type="text/css" href="../CSS/style1.css"/>
     <link rel="stylesheet" type="text/css" href="../CSS/style.css"/>
     <script src="../JS/jquery.min.js" type="text/javascript"></script>
     <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
     <script src="../JS/easyui-lang-zh_CN.js" type="text/javascript"></script>
     <script src="../JS/mainScript.js" type="text/javascript"></script>
     <script src="../JS/Main.js" type="text/javascript"></script>
     <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
     <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript">
        var param = "";
        $(function () {
            $('#LCSPGrid').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                singleSelect: false,
                striped: true,
                pagination: true,
                pageSize: 30,
                
                toolbar: ['-', {
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        Edit("add");
                    }
                }, '-', {
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        Edit("edit");
                    }
                },
                 '-', {
                     text: '删除',
                     iconCls: 'icon-remove',
                     handler: function () {
                         Remove();
                     }
                 }],
                columns: [[
                    { field: 'CK', checkbox:true },
                    { field: 'LCDM', title: '流程代码' },
                    { field: 'LCMC', title: '流程名称', width: 120 },
                    { field: 'LCLXDM', title: '流程类型代码', hidden: true },
					{ field: 'LCLX', title: '流程类型', width: 200 }
				]],
                onHeaderContextMenu: function (e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                },
                onDblClickRow: function (rowIndex, rowData) {
                
                }

            });
            initDYDialog('dialogLC', '调整审批流程维护', 600, 200, $('#DYXX'));
            //需要加载的数据
            LoadLCLX(); //加载流程类型
            InitDY(); //加载添加的流程数据
        });

        function LoadLCLX() {//加载流程类型
            $.ajax({
                type: 'get',
                url: 'ZTLXSP.ashx?action=LoadLCLX',
                async: false,
                cache: false,
                success: function (result) {
                    $("#LCLX").combobox("clear");
                    var data = JSON.parse(result)
                    $("#LCLX").combobox("loadData", data);
                    if (result != "") {
                        $("#LCLX").combobox("setValue", data[0].id);
                    }
                }
            });
        }


        function initDYDialog(name, title, wid, hg, con) {
            $('#' + name).dialog({
                title: title,
                width: wid,
                height: hg,
                modal: true,
                closed: true,
                cache: false,
                buttons: [{
                    text: '保存',
                    iconCls: 'icon-ok',
                    handler: function () {

                        var LCMC = $("#LCMC").val();//流程名称
                        var LCLX = $("#LCLX").combobox("getValue");//流程类型
    
                        switch (param) {
                            case "add":
                                if (LCMC != "" && LCLX != "") {
                                    AddData(LCMC, LCLX);
                                }
                                else {
                                    alert("请填写完整信息！");
                                }
                                break;
                            case "edit":
                                var rows = $("#LCSPGrid").datagrid("getSelected");
                                var LCDM = rows.LCDM; //获取流程代码，修改相关流程
                                var LCMC = $("#LCMC").val();
                                var LCLX = rows.LCLXDM;

                                if (LCMC != "" && LCLX != "") {
                                    EditData(LCDM, LCMC, LCLX);
                                }
                                else {
                                    alert("请填写完整信息！");
                                }
                                break;
                            default:

                        }
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function (param) {
                        $('#' + name).dialog('close');
                    }
                }],
                content: con
            });
        }

        function Edit(flag) {
            param = flag;
            switch (flag) {
                case "add":
                    break;
                case "edit":
                    var rows = $("#LCSPGrid").datagrid("getSelected")
                    if (rows == null) {
                        alert("请先选择一条记录！");
                        return;
                    }
                    else {
                        $("#LCMC").val(rows.LCMC);
                        $("#HidLCLX").val(rows.LCLXDM);
                        
                        if (rows.LCLX != "") {
                            $("#HidLCLX").val(rows.LCLXDM)
                            $("#LCLX").combobox("setText", rows.LCLX);
                        }
                    }
                    break;
            }
            $("#dialogLC").dialog('open');
        }

        function Remove() {
            var rows = $("#LCSPGrid").datagrid("getSelections")
            if (rows == null) {
                alert("请先选择一条记录！");
                return;
            }
            else {
                $.messager.confirm('警告', '确定要删除选中的记录吗？', function (r) {

                    if (r) {
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                     
                        $.ajax({
                            type: 'get',
                            url: 'ZTLXSP.ashx?action=Remove&LCDM=' + row.LCDM,
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result != "") {
                                    var arr = new Array();
                                    arr = result.split(',');
                                    if (arr[0] == "1") {
                                        alert("删除成功！");
                                        InitDY();
                                    }
                                    else if (arr[0] == "0") {
                                        alert(arr[1]);
                                        return;
                                    }
                                }
                                else {
                                    tishi("保存失败！");
                                    return;
                                }
                            }
                        });
                    }
                    }
                });
            }
        }

        function AddData(LCMC, LCLX) {
            $.ajax({
                type: 'get',
                url: 'ZTLXSP.ashx?action=AddData&LCMC=' + LCMC + '&LCLX=' + LCLX,
                async: false,
                cache: false,
                success: function (result) {
                    if (result != "") {
                        var arr = new Array();
                        arr = result.split(',');
                        if (arr[0] == "1") {
                            alert("添加成功！");
                            InitDY();
                            $('#dialogLC').dialog('close');
                        }
                        else if (arr[0] == "0") {
                            alert(arr[1]);
                            return;
                        }
                    }
                    else {
                        tishi("保存失败！");
                        return;
                    }
                }
            });
        }

        function EditData(LCDM, LCMC, LCLX) {
            $.ajax({
                type: 'get',
                url: 'ZTLXSP.ashx?action=EditData&LCDM=' + LCDM + '&LCMC=' + LCMC + '&LCLX=' + LCLX,
                async: false,
                cache: false,
                success: function (result) {
                    if (result != "") {
                        var arr = new Array();
                        arr = result.split(',');
                        if (arr[0] == "1") {
                            alert("修改成功！");
                            InitDY();
                            $('#dialogLC').dialog('close');
                        }
                        else if (arr[0] == "0") {
                            alert(arr[1]);
                            return;
                        }
                    }
                    else {
                        alert("修改失败！");
                        return;
                    }
                }
            });
        }

        function InitDY() {
            $.ajax({
                type: 'get',
                url: 'ZTLXSP.ashx?action=InitDY',
                async: false,
                cache: false,
                success: function (result) {
                    $("#LCSPGrid").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', eval(result));
                }
            });
        }

        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }

                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }

    </script>
</head>
<body>
    <div class="easyui-tabs" id="tt" style="width: 100%; height: 100%;">
        <form id="form1" runat="server">
        <div id="Div1" runat="server" class="easyui-layout" style="width: 100%; height: 100%;">

                <div style="float: left">
                
                    &nbsp;</div>
                <div id="JHDiv" style="float: left">
                    
                </div>

            <div id="DYGXDiv" region="center" title="流程审批名称">
                <div id="DivDYGX" style="width: 100%; height: 100%; display: block;">
                    <table id="LCSPGrid" style="width: 100%; height: 100%;">
                    </table>
                </div>
            </div>
        </div>
   </form>
    </div>
    <div id="dialogLC">
    </div>
    <div id="DYXX">
        <table>

            <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                           流程名称:
                        </th>
                        <th>
                        <input class="easyui-input" id="LCMC" name="CWKM" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>

                   <td>
                    <div style="padding-top: 50px;">
                        <th>
                            流程类型:
                        </th>
                        <th>
                        <input class="easyui-combobox" id="LCLX" name="SJLY" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>
            </tr>
            <input type="hidden" id="HidLCMC" />
            <input type="hidden" id="HidLCLX" />
        </table>
    </div>
</body>
</html>
