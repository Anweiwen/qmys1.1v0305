﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SPXMLC.aspx.cs" Inherits="LCSP_SPXMLC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>审批项目流程</title>
        <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css"/>
     <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css"/>
     <link rel="stylesheet" type="text/css" href="../CSS/style1.css"/>
     <link rel="stylesheet" type="text/css" href="../CSS/style.css"/>
     <script src="../JS/jquery.min.js" type="text/javascript"></script>
     <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
     <script src="../JS/easyui-lang-zh_CN.js" type="text/javascript"></script>
     <script src="../JS/mainScript.js" type="text/javascript"></script>
     <script src="../JS/Main.js" type="text/javascript"></script>
     <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
     <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
         <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript">
    
    </script>
</head>
<body>
    <div class="easyui-tabs" id="tt" style="width: 100%; height: 100%;">
        <form id="form1" runat="server">
        <div id="Div1" runat="server" class="easyui-layout" style="width: 100%; height: 100%;">

                <div style="float: left">
                
                    &nbsp;</div>
                   <div id="JHDiv" style="float: left">
                    流程名称：<input class="easyui-combobox" id="LCMC" name="LCMC" style="width: 150px" data-options="valueField:'id',textField:'text'" />&nbsp;
                </div>

            <div id="DYGXDiv" region="center" title="审批项目流程">
                <div id="DivDYGX" style="width: 100%; height: 100%; display: block;">
                    <table id="LCSPGrid" style="width: 100%; height: 100%;">
                    </table>
                </div>
            </div>
        </div>
   </form>
    </div>
    <div id="dialogLC">
    </div>
    <div id="DYXX">
        <table>

            <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                           审批流程名称:
                        </th>
                        <th>
                        <input class="easyui-input" id="SPLCMC" name="SPLCMC" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>

                   <td>
                    <div style="padding-top: 50px;">
                        <th>
                            审批顺序:
                        </th>
                        <th>
                        <input class="easyui-combobox" id="SPSX" name="SPSX" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>
            </tr>

             <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                           审批人:
                        </th>
                        <th>
                        <input class="easyui-input" id="SPR" name="SPR" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>

                   <td>
                   <%-- <div style="padding-top: 50px;">
                        <th>
                            菜单:
                        </th>
                        <th>
                        <input class="easyui-combobox" id="CD" name="CD" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>--%>
                </td>
            </tr>

                       <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                           模板:
                        </th>
                        <th>
                        <input class="easyui-input" id="MBDM" name="MBDM" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>

                   <td>
                    <div style="padding-top: 50px;">
                        <th>
                            所有用户提交执行:
                        </th>
                        <th>
                          <input id="TJZX" type="checkbox" />
                        </th>
                    </div>
                </td>
            </tr>
            <input type="hidden" id="HidLCMC" />
            <input type="hidden" id="HidLCLX" />
        </table>
    </div>
</body>
</html>
