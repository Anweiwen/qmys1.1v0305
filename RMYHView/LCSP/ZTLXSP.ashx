﻿<%@ WebHandler Language="C#" Class="ZTLXSP" %>

using System;
using System.Web;
using System.Data;
using System.Text;
using RMYH.BLL;
using System.Web.SessionState;

public class ZTLXSP : System.Web.UI.Page, IRequiresSessionState, IHttpHandler
{

    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "", LCDM = "", LCMC = "", LCLX = "";
        switch (action)
        {
            case "LoadLCLX":
                res = LoadXMFL();
                break;
            case "InitDY":

                res = InitDY();
                break;
            case "AddData":
                LCMC = context.Request.QueryString["LCMC"];
                LCLX = context.Request.QueryString["LCLX"];
                LCDM = context.Request.QueryString["LCDM"]; //流程代码，唯一标识
                res = AddData(LCDM, LCMC, LCLX);
                break;
            case "EditData":
                LCMC = context.Request.QueryString["LCMC"];
                LCLX = context.Request.QueryString["LCLX"];
                LCDM = context.Request.QueryString["LCDM"]; //流程代码，唯一标识
                res = EditData(LCDM, LCMC, LCLX);
                break;
            case "Remove":

                LCDM = context.Request.QueryString["LCDM"]; //流程代码，唯一标识   
                res = Remove(LCDM);
                break;
        }
        context.Response.Write(res);
    }

    /// <summary>
    /// 初始化查询
    /// </summary>
    public string InitDY()
    {
        string res = "";
        string sql = " SELECT DISTINCT A.LCDM ,A.LCMC,B.XMMC LCLX,B.XMDH_A LCLXDM FROM dbo.TB_TZSPLCMC A LEFT JOIN XT_CSSZ B ON A.LCLX = convert(char,B.XMDH_A )   WHERE B.XMFL = 'LCLX' ORDER BY A.LCDM";

            DataTable dt = bll.Query(sql).Tables[0];
            res = FillData("easyui-datagrid", sql);
            return res;
    }

    /// <summary>
    /// 添加数据
    /// </summary>
    /// <param name="LCDM">流程代码</param>
    /// <param name="LCMC">流程名称</param>
    /// <param name="LCLX">流程类型</param>
    /// <returns></returns>
    public string AddData(string LCDM, string LCMC, string LCLX)
    {
        string res = "";
        string sql = "SELECT * FROM dbo.TB_TZSPLCMC WHERE LCDM='" + LCDM + "' ";
        DataTable dt = bll.Query(sql).Tables[0];
        if (dt.Rows.Count > 0)
        {
            res = "0,已存在此记录";
        }
        else
        {
            string countSql = "SELECT MAX(LCDM)MAXVAL FROM TB_TZSPLCMC ";
            
            var sq = bll.Query(countSql).Tables[0];
            int count = 0;
            if (string.IsNullOrEmpty(sq.Rows[0]["MAXVAL"].ToString()))
            {
                count = 10000;
            }
            else
            {
                count = Convert.ToInt32(sq.Rows[0]["MAXVAL"]);
                count += 1;
            }

            string insertsql = "insert into TB_TZSPLCMC(LCDM,LCMC,LCLX) VALUES('" + count  + "','" + LCMC + "','" + LCLX + "')";
            string flag = bll.ExecuteSql(insertsql).ToString();
            if (flag == "1")
            {
                res = "1,添加成功！";
            }
            else
            {
                res = "0,添加失败！";
            }
        }
        return res;
    }


    /// <summary>
    /// 修改流程数据
    /// </summary>
    /// <param name="LCDM">流程代码</param>
    /// <param name="LCMC">流程名称</param>
    /// <param name="LCXL">流程类型</param>
    /// <returns></returns>
    public string EditData(string LCDM, string LCMC, string LCXL)
    {
        string res = "";
        string existsql = "SELECT * FROM dbo.TB_TZSPLCMC WHERE LCDM='" + LCDM + "' ";
        DataTable existdt = bll.Query(existsql).Tables[0];
        if (existdt.Rows.Count > 0)
        {
            string uptsql = "UPDATE TB_TZSPLCMC SET LCMC='" + LCMC + "' , LCLX='" + LCXL + "'  " + "where LCDM='" + LCDM + "'";
            try
            {
                string flag = bll.ExecuteSql(uptsql).ToString();
                if (flag == "1")
                {
                    res = "1,修改成功！";
                }
                else
                {
                    res = "0,修改失败！";
                }
            }
            catch
            { }
     
        }
        else
        {
            string insertsql = "insert into TB_TZSPLCMC(LCDM,LCMC,LCLX) VALUES('" + Guid.NewGuid().ToString("D").ToUpper() + "','" + LCMC + "','" + LCXL + "')";
            string flag = bll.ExecuteSql(insertsql).ToString();
            if (flag == "1")
            {
                res = "1,修改成功！";
            }
            else
            {
                res = "0,修改失败！";
            }
        }
        return res;
    }


    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="LCDM">唯一标识</param>
    /// <returns></returns>
    public string Remove(string LCDM)
    {
        string res = "";
        string sql = "delete from TB_TZSPLCMC where LCDM='" + LCDM + "'";
        string flag = bll.ExecuteSql(sql).ToString();
        if (flag == "1")
        {
            res = "1,删除成功！";
        }
        else
        {
            res = "0,删除失败！";
        }
        return res;
    }


    public string LoadXMFL()
    {
        string sql = " SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='LCLX'";
        DataSet set = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        if (set.Tables[0].Rows.Count > 0)
        {
            Str.Append("[");
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                Str.Append("{\"id\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMDH_A"].ToString() + "\"" + ",");
                Str.Append("\"text\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMMC"].ToString() + "\"" + "}");
                if (i < set.Tables[0].Rows.Count - 1)
                {
                    Str.Append(",");
                }
            }
            Str.Append("]");
        }
        return Str.ToString();
    }

    private string FillData(string _type, string _sql)
    {
        if (!string.IsNullOrEmpty(_type) && !string.IsNullOrEmpty(_sql))
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = new DataSet();
            ds = bll.Query(_sql);
            string res = "[]";
            if (ds.Tables[0].Rows.Count > 0)
            {
                switch (_type)
                {
                    case "easyui-tree":
                        res = gettreenode("", ds.Tables[0]);
                        break;
                    case "easyui-datagrid":
                        res = getdatagrid(ds.Tables[0]);
                        break;
                }
            }
            return res;
        }
        else
        {
            return "";
        }
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private string getdatagrid(DataTable _table)
    {
        string res = "";
        string sr = "";
        for (int i = 0; i < _table.Rows.Count; i++)
        {
            DataRow row = _table.Rows[i];
            sr = "";
            if (row != null)
            {
                if (res == "")
                {
                    res = "{";
                }
                else
                {
                    res += ",{";
                }
                for (int j = 0; j < _table.Columns.Count; j++)
                {
                    if (sr == "")
                    {
                        sr = _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                    else
                    {
                        sr += "," + _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                }
                res += sr + "}";
            }
        }
        return "[" + res + "]";
    }
    private string gettreenode(string _fbdm, DataTable _table)
    {
        string res = "";
        DataRow[] rows = _table.Select(_table.Columns[2].ColumnName + "='" + _fbdm + "'");
        for (int i = 0; i < rows.Length; i++)
        {
            DataRow row = rows[i];
            if (row != null)
            {
                if (row[3].ToString() == "1")
                {
                    if (res == "")
                    {
                        res = "{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\"}";
                    }
                    else
                    {
                        res += ",{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\"}";
                    }
                }
                else if (row[3].ToString() == "0")
                {
                    if (res == "")
                    {
                        res = "{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                    else
                    {
                        res += ",{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                }
            }
        }
        return "[" + res + "]";
    }
    
}