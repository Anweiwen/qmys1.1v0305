﻿<%@ WebHandler Language="C#" Class="SPXMLC" %>

using System;
using System.Web;
using System.Data;
using System.Text;
using RMYH.BLL;
using System.Web.SessionState;

public class SPXMLC : System.Web.UI.Page, IRequiresSessionState, IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}