﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QCCB.aspx.cs" Inherits="JHCPCSCBLR_QCCB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CSS/demo.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/icon.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style1.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/jquery-1.3.1.min.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
 <div id="tt" class="easyui-tabs" fit="true">
         <div id="divCbfy" title="原料库存成本" style="padding:5px">
            <iframe  id="ylkc" name="ifr" style="width:100%; height: 100%;" frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	    </div>
         <div id="div2" title="辅材和燃料动力价格" style="padding:5px">
            <iframe  id="fcrl" name="ifr" style="width:100%; height: 100%;" frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	    </div>
	    <div id="divJg" title="半产品期初成本" style="padding:5px;">
            <iframe  id="cpqc" name="ifr" style="width:100%; height: 100%;" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>
        <div id="div1" title="最终产品期初成本" style="padding:5px;">
            <iframe  id="zzcpqc" name="ifr" style="width:100%; height: 100%;" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>  
        <div id="div3" title="油价转换" style="padding:5px;">
            <iframe  id="yjzh" name="ifr" style="width:100%; height: 100%;" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>     
	</div>
<input type="hidden" runat="server" id="hidloginout" />
    <script type="text/javascript">
        if ($("#<%=hidloginout.ClientID %>").val() == "Out") {
            window.parent.parent.location = "<%=Request.ApplicationPath%>/Login.aspx?rnd=" + Math.random();
        }
        function GetExcelList() {
            $("#ylkc").attr("src", "YLKCCB.aspx");
            $("#fcrl").attr("src", "FCRLDLJG.aspx");
            $("#cpqc").attr("src", "BCPQCCB.aspx");
            $("#zzcpqc").attr("src", "ZZCPQCCB.aspx");
            $("#yjzh").attr("src", "YJZH.aspx");
        }
        $(document).ready(function () {
            GetExcelList();
            var hg = document.documentElement.clientHeight;
            $("#tt")[0].style.height = hg - 5;
        });
    </script>
</body>
</html>
