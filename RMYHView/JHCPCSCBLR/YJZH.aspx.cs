﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class JHCPCSCBLR_YJZH : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JHCPCSCBLR_YJZH));
        }
    }
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 月
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonths()
    {
        string str = "";
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else if (i <= 9)
            {
                str += "<option value='" + i + "'>0" + i + "</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 季度
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetQuarter()
    {
        string str = "";
        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            for (int i = 1; i <= 4; i++)
            {
                if (i == 1)
                {
                    Str += "<option value='" + i + "'>月</option>";
                }
                else if (i == 2)
                {
                    Str += "<option value='" + i + "'>季</option>";
                }
                else if (i == 3)
                {
                    Str += "<option value='" + i + "'>年</option>";
                }
                else
                {
                    Str += "<option value='" + i + "'>其它</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFANAME(string fabs, string yy, string nn, string jd)
    {
        string Str = "";
        try
        {
            //月份小于10的在前面加0
            if (int.Parse(nn) <= 9)
            {
                nn = 0 + nn;
            }
            //查询所有
            if (int.Parse(nn) == 0 && int.Parse(jd) == 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的月
            else if (int.Parse(nn) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.NN='" + nn + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的季度
            else if (int.Parse(jd) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.JD='" + jd + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取 元/吨
    /// </summary>
    /// <param name="faName">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string YD(string jhFadm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet YD = bll.Query("select * from TB_YJGZH where XMDH_A='3' AND JHFADM='" + jhFadm + "'");
        string rtn = YD.Tables[0].Rows[0][3].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 桶/吨
    /// </summary>
    /// <param name="faName">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string TD(string jhFadm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet TD = bll.Query("select * from TB_YJGZH WHERE XMDH_A='1' and JHFADM='" + jhFadm + "'");
        string rtn = TD.Tables[0].Rows[0][3].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 美元/元
    /// </summary>
    /// <param name="faName">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string MYY(string jhFadm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet MYY = bll.Query("select * from TB_YJGZH WHERE XMDH_A='2' and JHFADM='" + jhFadm + "'");
        string rtn = MYY.Tables[0].Rows[0][3].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 美元/桶
    /// </summary>
    /// <param name="faName">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string MYT(string jhFadm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet MYT = bll.Query("select * from TB_YJGZH WHERE XMDH_A='4' and JHFADM='" + jhFadm + "'");
        string rtn = MYT.Tables[0].Rows[0][3].ToString();
        return rtn;
    }
    /// <summary>
    /// 油价转换
    /// </summary>
    /// <param name="YD">元/吨</param>
    /// <param name="TD">桶/吨</param>
    /// <param name="MYY">美元/元</param>
    /// <param name="faName">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Conversion(string YD, string TD, string MYY, string jhFadm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string ydxmmc = " 元/吨";
        string tdxmmc = "桶/吨";
        string myyxmmc = "美元/元";
        string mytxmmc = "美元/桶";
        string rtn = (Math.Round(double.Parse(YD) / double.Parse(TD) / double.Parse(MYY), 3)).ToString();
        string[] arr = new string[1];
        //查询数据库中有没有美元/桶的数据
        DataSet selMYT = bll.Query("select * from TB_YJGZH where XMDH_A='4'and JHFADM='" + jhFadm + "'");
        if (selMYT.Tables[0].Rows.Count > 0)
        {
            arr[0] = "update TB_YJGZH set SZCS=" + rtn + " where XMDH_A='4' AND JHFADM='" + jhFadm + "'";
            bll.ExecuteSql(arr, null);
        }
        else
        {
            arr[0] = "insert into TB_YJGZH(JHFADM,XMDH_A,XMMC,SZCS) values('" + jhFadm + "','" + 4 + "','" + mytxmmc + "'," + rtn + ")";
            bll.ExecuteSql(arr, null);
        }
        //查询数据库中有没有元/吨的数据
        DataSet selYD = bll.Query("select * from TB_YJGZH where XMDH_A='3' AND JHFADM='" + jhFadm + "'");
        if (selYD.Tables[0].Rows.Count > 0)
        {
            arr[0] = "update TB_YJGZH set SZCS=" + YD + " where XMDH_A='3' AND JHFADM='" + jhFadm + "'";
            bll.ExecuteSql(arr, null);
        }
        else
        {
            arr[0] = "insert into TB_YJGZH(JHFADM,XMDH_A,XMMC,SZCS) values('" + jhFadm + "','" + 3 + "','" + ydxmmc+ "',"+YD+")";
            bll.ExecuteSql(arr, null);
        }
        //查询数据库中有没有桶/吨的数据
        DataSet selTD = bll.Query("select * from TB_YJGZH where XMDH_A='1' AND JHFADM='" + jhFadm + "'");
        if (selTD.Tables[0].Rows.Count > 0)
        {
            arr[0] = "update TB_YJGZH set SZCS=" + TD + " where XMDH_A='1' AND JHFADM='" + jhFadm + "'";
            bll.ExecuteSql(arr, null);
        }
        else
        {
            arr[0] = "insert into TB_YJGZH(JHFADM,XMDH_A,XMMC,SZCS) values('" + jhFadm + "','" +1 + "','" + tdxmmc + "'," + TD + ")";
            bll.ExecuteSql(arr, null);
        }
        //查询数据库中有没有美元/元的数据
        DataSet selMYY = bll.Query("select * from TB_YJGZH where XMDH_A='2' AND JHFADM='" + jhFadm + "'");
        if (selMYY.Tables[0].Rows.Count > 0)
        {
            arr[0] = "update TB_YJGZH set SZCS=" + MYY + " where XMDH_A='2' AND JHFADM='" + jhFadm + "'";
            bll.ExecuteSql(arr, null);
        }
        else
        {
            arr[0] = "insert into TB_YJGZH(JHFADM,XMDH_A,XMMC,SZCS) values('" + jhFadm + "','" + 2 + "','" + myyxmmc + "'," + MYY + ")";
            bll.ExecuteSql(arr, null);
        }
        return rtn;
    }
}