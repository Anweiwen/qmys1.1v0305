﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using RMYH.DBUtility;

public partial class JHCPCSCBLR_FCRLDLJG : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JHCPCSCBLR_FCRLDLJG));
        }
    }
    /// <summary>
    /// 刷新方法
    /// </summary>
    /// <param name="trid"></param>
    /// <param name="id"></param>
    /// <param name="intimgcount"></param>
    /// <param name="mbzq">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string mbdm,string yy)
    {
        return tabGetDataList.GetDataListstring("10146034", "", new string[] { ":JHFADM", ":HSZXDM", ":YY" }, new string[] { mbdm, Session["HSZXDM"].ToString(), yy }, false, trid, id, intimgcount);
    }
     /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string jhfadm)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string[] arr = new string[1];
            for (int i = 0; i < values.Length; i++)
            {
                string hszxdm = Session["HSZXDM"].ToString();
                string trdj = values[i];
                DataSet sql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ID[i] + "'");
                if (sql.Tables[0].Rows.Count > 0)
                {
                    arr[0] = "update TB_JHYYCB set TRDJ=" + trdj + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ID[i] + "'";
                    DbHelperOra.ExecuteSql(arr, null);
                }
                else
                {
                    arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,HSZXDM,TRDJ,QCYYL,QCCB,TRYYL,YKCL) values('" + jhfadm + "','" + ID[i] + "','" + hszxdm + "'," + trdj + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ")";
                    DbHelperOra.ExecuteSql(arr, null);
                }
            }
            return "保存成功！";
        }
        catch
        {
            return "保存失败！";
        }

    }
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 月
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonths()
    {
        string str = "";
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else if (i <= 9)
            {
                str += "<option value='" + i + "'>0" + i + "</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 季度
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetQuarter()
    {
        string str = "";
        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            for (int i = 1; i <= 4; i++)
            {
                if (i == 1)
                {
                    Str += "<option value='" + i + "'>月</option>";
                }
                else if (i == 2)
                {
                    Str += "<option value='" + i + "'>季</option>";
                }
                else if (i == 3)
                {
                    Str += "<option value='" + i + "'>年</option>";
                }
                else
                {
                    Str += "<option value='" + i + "'>其它</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFANAME(string fabs, string yy, string nn, string jd)
    {
        string Str = "";
        try
        {
            //月份小于10的在前面加0
            if (int.Parse(nn) <= 9)
            {
                nn = 0 + nn;
            }
            //查询所有
            if (int.Parse(nn) == 0 && int.Parse(jd) == 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的月
            else if (int.Parse(nn) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.NN='" + nn + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的季度
            else if (int.Parse(jd) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.JD='" + jd + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 导出excel
    /// </summary>
    /// <param name="jhfadm">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string ExportExcelByDataTable(string jhfadm,string yy)
    {
        try
        {
            string hszxdm = Session["HSZXDM"].ToString();
            string sql = "";
            sql += "SELECT E.XMMC '项目分类',B.CPMC '产品名称',C.DWMC '计量单位',A.TRDJ '单价' ";
            sql += " FROM TB_JHYYCB A,TB_JHCPBM B,TB_JLDW C,TB_FYYS D,XT_CSSZ E";
            sql += " WHERE JHFADM='" + jhfadm + "' AND E.XMFL='FYYSSXLB' AND D.FYYSSXFLDM=CONVERT(CHAR,E.XMDH_A) AND A.HSZXDM='" + hszxdm + "'";
            sql += " AND B.JLDW*=C.DWDM AND  A.YLBM=B.CPDM AND B.FYYSDM=D.FYYSDM AND FYYSSXFLDM<>'0' AND D.YY='" + yy + "' AND (B.CPBS=6 AND B.YJDBZ='1')";
            sql += " UNION";
            sql += " SELECT F.XMMC XMFL,C.CPMC,D.DWMC,NULL TRDJ";
            sql += " FROM TB_JHCPBM C ,TB_JLDW D, TB_FYYS  E,XT_CSSZ F";
            sql += " WHERE CPBS=6 AND C.YJDBZ='1' AND C.HSZXDM='" + hszxdm + "' AND C.SYBZ='1' AND C.JLDW*=D.DWDM AND C.FYYSDM=E.FYYSDM AND E.FYYSSXFLDM='0'";
            sql += " AND E.YY='" + yy + "' AND F.XMFL='FYYSSXLB' AND E.FYYSSXFLDM=CONVERT(CHAR,F.XMDH_A)";
            sql += " AND C.CPDM NOT IN(SELECT YLBM FROM TB_JHYYCB D WHERE JHFADM='" + jhfadm + "' AND D.HSZXDM='"+hszxdm+"') ";
            DataTable dt = DbHelperOra.Query(sql).Tables[0];

            TB_TABLESXBLL bll = new TB_TABLESXBLL();
            string filename = bll.DataToExcelByNPOI(dt, "辅材和燃料动力价格.xls", "辅材和燃料动力价格");


            return filename;
        }
        catch
        {
            return "导出失败！";
        }
    }
}