﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelMB.aspx.cs" Inherits="SIS_SelMB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>模板选择</title>
    <base target="_self" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
    <script src="../JS/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script type="text/javascript">

        function SelMB(id, name, type) {
            // debugger;
            var zvalue = id[0].value + "," + name[0].value + "," + type[0].value;
            window.returnValue = zvalue;
            window.close();
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server" style="text-align: left;">
    <asp:TreeView runat="server" ID="TreeView1">
    </asp:TreeView>
    <input type="hidden" id="hidyy" runat="server" />
    <input type="hidden" id="hidName" runat="server" />
    <input type="hidden" id="hidType" runat="server" />
    <input type="hidden" id="hidID" runat="server" />
    </form>
</body>
</html>
