﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using Excel;
using RMYH.BLL;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Reflection;
using System.Collections;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
public partial class Excels_ExcelMb_Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(Excels_ExcelMb_Default3));
        //QYJS("", "", "", "", "","dsd","ds",1);
        //JHZYTRGS(\"10070010\",\"10010694\",\"10113513\",\"FAZCLL\",\"DQFA\")";
        //qz(gsss, "2015", "11", "10050689", "10050727", "1");
        JXGS("10162214", "2015", "11", "10050689", "10050727", "1");
    }
    public string  JXGS(string MBDM,string YY, string NN, string FADM1, string FADM2, string FABS)
    {
        string yy2 = "2016";
        string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID FROM dbo.TB_MBJYGS T where MBDM='"+MBDM+"'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet set=BLL.Query(sql);
        sql = "SELECT A.MBDM,A.MBLX,A.MBMC,A.ZYDM,A.MBWJ,A.MBZQ,A.MBBM FROM TB_YSBBMB A WHERE MBDM='"+MBDM+"'";
        DataSet mbmcset = BLL.Query(sql);
        string mbmc = "";
        string yzjg = "";
        if (mbmcset.Tables[0].Rows.Count > 0)
        {
            mbmc = mbmcset.Tables[0].Rows[0]["MBWJ"].ToString();
        }
        if(set.Tables[0].Rows.Count>0)
        {
            string gsjx = "";
            string gs = "";
            string INFOMATION = "";
            string sheet = "";
            ArrayList listYzjg = new ArrayList();

            Excel.Application app = new Excel.Application();
            object Unknown = System.Type.Missing;
            string filename = HttpContext.Current.Server.MapPath("../XlS/" + mbmc);
            Excel.Workbook rwb = app.Workbooks.Open(filename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
            Excel.Worksheet rs = rwb.Worksheets[1] as Excel.Worksheet;
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                gsjx = set.Tables[0].Rows[i]["GSJX"].ToString();
                gs = set.Tables[0].Rows[i]["GS"].ToString();
                INFOMATION = set.Tables[0].Rows[i]["INFOMATION"].ToString();
                sheet = set.Tables[0].Rows[i]["SHEETNAME"].ToString();
                gs = GetDataList.qz(gs, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
                ((Excel.Range)rs.Cells[1, 1]).Formula = "=" + gs;
                object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
                string jg = dell[1, 1].ToString();
                if (jg == "False")
                {
                    listYzjg.Add(gsjx + INFOMATION + "\n");
                }
                ((Excel.Range)rs.Cells[1, 1]).Formula = null;
            }
            yzjg = string.Concat(listYzjg.ToArray());
            app.Quit();
        }
        return yzjg;

    }
    public bool jx(string gs, string gsjx, string info, string sheet, string mbmc, string YY, string NN, string FADM1, string FADM2, string FABS)
    {
        try
        {
            string yy2 = "2016";
            Excel.Application app = new Excel.Application();
            object Unknown = System.Type.Missing;
            string filename = HttpContext.Current.Server.MapPath("../XlS/" + mbmc);
            Excel.Workbook rwb = app.Workbooks.Open(filename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
            gs = GetDataList.qz(gs, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
            Excel.Worksheet rs = rwb.Worksheets[1] as Excel.Worksheet;
            ((Excel.Range)rs.Cells[1, 1]).Formula = "=" + gs;
            //string DSD = ((Excel.Range)rs.Cells[1, 1]).Formula.ToString();
            object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
            string jg = dell[1, 1].ToString();
            app.Quit();
            if (jg == "False")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (Exception ee)
        {
            throw new Exception(ee.Message);
        }
        finally {
            
        }
        
    }
  
    public void getPath()
    {
        string filename = HttpContext.Current.Server.MapPath("../XLS/" + "M10160036.XLS");
        //读取用户上传的Excle文件
        string conn = "Provider = Microsoft.Jet.OLEDB.4.0 ;Data Source ='" + filename + "';Extended Properties=Excel 8.0";
        OleDbConnection olecon = new OleDbConnection(conn);
        olecon.Open();
        //注意表名,打开Excel文件后,最底部分页的Excle名字,
        //默认是$Sheet1,$Sheet2,$Sheet3
        string sql = "select * from [$Sheet1]";
        OleDbDataAdapter oleda = new OleDbDataAdapter(sql, conn);
        DataSet ds = new DataSet();
        oleda.Fill(ds);
        olecon.Close();
        string file_name = "20091126002.xml";
        string xml_path = @Server.MapPath("../Reports/xml/") + file_name;
        ds.WriteXml(xml_path);
        ds.Dispose();
    }
    public int getNum(char ch)
    {
        if (ch >= 'a' && ch <= 'z')
            return ch - 'a' + 1;
        if (ch >= 'A' && ch <= 'Z')
            return ch - 'A' + 1;
        else
            return -1;
    }
    public int f(String str)
    {
        char[] ch = str.ToCharArray();
        int ret = 0;
        for (int i = 0; i < ch.Length; i++)
        {
            ret *= 26;
            ret += getNum(ch[i]);
        }
        return ret;
    }
    /// <summary>
    /// 区域计算
    /// </summary>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="address">区域地址</param>
    /// <param name="wjm">文件名</param>
    /// <param name="index">sheet页</param>
    /// <returns></returns>
    public string QYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string address, string wjm, int index)
    {
        string yy2 = "2016";
        string resjson = "";
        if (address != "" && address != null)
        {
            //"$C$3:$E$10"
             address = "$A$1:$A$4";
             index=1;
            Excel.Application app = new Excel.Application();
            object Unknown = System.Type.Missing;
            string filename = HttpContext.Current.Server.MapPath("../XLS/" + "M10160036.XLS");
            Excel.Workbook rwb = app.Workbooks.Open(filename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
            Excel.Worksheet rs = rwb.Worksheets[index] as Excel.Worksheet;
            
            object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
            string[] QSH = address.Split(':');
            string gs = "";
            int count = 0;
            resjson = "{\"grids\":[";
            if (QSH.Length > 1)
            {
                int qsL = f(QSH[0].Substring(1).Split('$')[0]);
                int qsH = Convert.ToInt32(QSH[0].Substring(1).Split('$')[1]);
                int jsL = f(QSH[1].Substring(1).Split('$')[0]);
                int jsH = Convert.ToInt32(QSH[1].Substring(1).Split('$')[1]);
                for (int i = qsH; i < jsH + 1; i++)
                {
                    for (int j = qsL; j < jsL + 1; j++)
                    {
                        gs = ((Excel.Range)rs.Cells[i, j]).Formula.ToString();
                        gs = GetDataList.qz(gs, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
                        count++;
                        resjson += "{\"idx\":\"" + index + "\",\"row\":\"" + jsH + "\",\"col\":\"" + jsL + "\",\"gs\":\"" + gs + "\"},";
                    }
                }
                //resjson = resjson.Substring(0, resjson.Length - 1);
            }
            else
            {
                int L = f(QSH[0].Substring(1).Split('$')[0]);
                int H = Convert.ToInt32(QSH[0].Substring(1).Split('$')[1]);
                gs = ((Excel.Range)rs.Cells[H,L]).Formula.ToString();
                gs = GetDataList.qz(gs, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
                resjson += "{\"idx\":\"" + index + "\",\"row\":\"" + H + "\",\"col\":\"" + L + "\",\"gs\":\"" + gs + "\"},";
                count = 1;
            }
            resjson = resjson.Substring(0, resjson.Length - 1);
            resjson += "],";
            resjson += "\"count\":\"" + count + "\"";
            resjson += "}";
            app.Quit();
            // this.setgs = function (_json) {
            //this.poctrl.EnableExcelCalling();

            //if (this.poctrl != null && this.poctrl.Document != null) {
            //    var icount = _json["count"];
            //    for (i = 0; i < icount; i++) {
            //        var idx = _json["grids"][i]["idx"];
            //        var row = _json["grids"][i]["row"];
            //        var col = _json["grids"][i]["col"];
            //        var gs = _json["grids"][i]["gs"];
            //        var sheet = this.poctrl.Document.Sheets(parseInt(idx));
            //        if (sheet) {
            //            sheet.Cells(parseFloat(row), parseInt(col)).Value = gs;
            //        }
            //    }
            //}

        }
        else
        {
            resjson = "";
        }
        return resjson;
    }
        private string fileName = null; //文件名
        private IWorkbook workbook = null;
        private FileStream fs = null;
        private bool disposed;
        public int qyjs(string sheetName, bool isColumnWritten)
        {
            int i = 0;
            int j = 0;
            int count = 0;
            ISheet sheet = null;
            fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            if (fileName.IndexOf(".xlsx") > 0) // 2007版本
                workbook = new XSSFWorkbook();
            else if (fileName.IndexOf(".xls") > 0) // 2003版本
                workbook = new HSSFWorkbook();
            try
            {
                if (workbook != null)
                {
                    sheet = workbook.CreateSheet(sheetName);
                }
                else
                {
                    return -1;
                }

                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }

    public void strings()
    {
        String str = @"测试数据";

        // 1.启动Excel进程
        //Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
        //excelApp.Visible = false;

        //// 2.获取Workbooks对象

        //// 3.添加工作薄Workbook对象
        //Workbook wb = wbs.Add();
        //// 4.获取第一个工作表Workbook
        //Worksheet ws = (Worksheet)wb.Worksheets.Item[1];
        //// 5.获取单元格并设置文本
     
      
        //// 6.7 保存Excel文档并关闭
        //wb.Close();
        //// 8.退出Excel程序
        //excelApp.Quit();
        //// 9.释放资源
        //System.Runtime.InteropServices.Marshal.ReleaseComObject(ws);
        //System.Runtime.InteropServices.Marshal.ReleaseComObject(wb);
        //System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
        //// 10.调用GC的垃圾收集方法
        //GC.Collect();
        //GC.WaitForPendingFinalizers();




    }

}