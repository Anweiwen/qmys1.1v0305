﻿//*********************************************************//
//** 名称：公式设置
//** 功能: 设置公式
//** 开发日期: 2012-7-3  开发人员：余妮
//** 表:REPORT_CSSZ_SJY         报表数据源
//**    REPORT_CSSZ_SJYDYXTJ    报表数据源对应项条件
//**    REPORT_CSSZ_SJYDYX      报表数据源对应项
//**    XT_CSSZ                 系统参数设置字典
//**    TB_WLTBZDSZ             物料填报字段设置表
//**    TB_ZYML                 作业划分目录表
//**    TB_YHWLQX               用户物料权限
//*********************************************************//
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using System.Text;
public partial class bbgsjs : System.Web.UI.Page
{
    protected string MBDM="";
    protected string SHEETNAME = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(bbgsjs));
        MBDM = Request.QueryString["MBDM"].ToString();
        SHEETNAME = Request.QueryString["SHEETNAME"].ToString();
        string id = Request.QueryString["ID"].ToString();
        HidHszxdm.Value = Request.QueryString["HSZXDM"].ToString();
        HidUserdm.Value = Request.QueryString["USERDM"].ToString();
        HidYy.Value = Request.QueryString["YY"].ToString();
        hidlx.Value = Request.QueryString["lx"].ToString().Trim();
        string lx = Request.QueryString["lx"].ToString().Trim();
        if (lx == "1")
        {
            HidGS.Value = Request.QueryString["GS"].ToString().Trim();
        }
        else
        {
            HidGS.Value =HttpUtility.UrlDecode(Request.QueryString["GS1"].ToString().Trim());
        }
        
       
        Session["gsjsid"] = id;
    }
    /// <summary>
    /// 去除数组重复相
    /// </summary>
    /// <param name="ARR"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public ArrayList getTjLit(ArrayList ARR)
    {
        ArrayList ad = new ArrayList();
        for (int i = 0; i < ARR.Count; i++)
        {
            if (!ad.Contains(ARR[i]))
            {
                ad.Add(ARR[i]);
            }
        }
        return ad;
    }

    #region 后台方法

    public string fhgs(string gs)
    {

        string gss = gs;
        int index = gss.IndexOf("(");
        string _gs = string.Empty;
        string zgs = string.Empty;
        if (index > -1)
        {
            _gs = gss.Substring(0, index);
            string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY  where Upper(DYGS)='" + _gs.ToUpper() + "'";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet set = bll.Query(sql);
            //报表名称
            string bBMC = set.Tables[0].Rows[0]["SJYMC"].ToString();
            //报表id
            string bBID = set.Tables[0].Rows[0]["SJYID"].ToString();
            //延展值
            _gs = gss.Substring(index + 1);
            int j = _gs.LastIndexOf(",");
            string zqmc = "";
            //周期值
            string zq = _gs.Substring(j + 1);
            zq = zq.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
            string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + bBID + "'";
            DataSet dtz = bll.Query(sqlz);
            DataSet zqSet;
            if (bBID != "2")
            {
                if (dtz.Tables[0].Rows.Count > 0)
                {
                    zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
                }
                else
                {
                    zqSet = null;
                }
                if (zqSet.Tables[0].Rows.Count > 0)
                {
                    DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                    zqmc = DR[0]["XMMC"].ToString();
                }
                else
                {
                    zqmc = "";
                }
            }
            else {
                if (dtz.Tables[0].Rows.Count > 0)
                {
                    zqSet = bll.Query("SELECT * FROM XT_CSSZ WHERE XMFL='YSSJDX' ");
                }
                else
                {
                    zqSet = null;
                }
                if (zqSet.Tables[0].Rows.Count > 0)
                {
                    DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                    zqmc = DR[0]["XMMC"].ToString();
                }
                else
                {
                    zqmc = "";
                }
            }

            //报表值
            _gs = _gs.Substring(0, j);
            _gs = _gs.Replace('"', ' ').Trim();
            j = _gs.LastIndexOf(",");
            string zifu = _gs.Substring(j + 1);
            zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
            string zfmc = string.Empty;
            if (bBID != "2" && bBID!="15")
            {
                string zfsql = "select SJYID,BID,XBM,XMC from REPORT_CSSZ_SJYDYX where SJYID='" + bBID + "' and Upper(XBM)='" + zifu + "'";
                DataSet zfset = bll.Query(zfsql);
               
                if (zfset.Tables[0].Rows.Count > 0)
                {
                    zfmc = '"' + zfset.Tables[0].Rows[0]["XMC"].ToString() + '"';
                }
                else
                {
                    zfmc = "''";
                }
            }
            else
            {
                string zfsql = "SELECT * FROM TB_JSDX where ZFBM='" + zifu + "'";
                DataSet zfset = bll.Query(zfsql);
                if (zfset.Tables[0].Rows.Count > 0)
                {
                    zfmc = '"' + zfset.Tables[0].Rows[0]["XMMC"].ToString() + '"';
                }
                else
                {
                    zfmc = "''";
                }
            }
            // 条件值

            j = _gs.LastIndexOf(",");
            _gs = _gs.Substring(0, j);
            string QSFS = string.Empty;
            //如果当前解析的是预算变动行项目取数函数的话，则倒数第3个是变动行取数函数取数方式（YZ：延展；LJ：累计）两种
            if (bBID.Equals("15"))
            {
                j = _gs.LastIndexOf(",");
                string FS = _gs.Substring(j + 1);
                if (FS.ToUpper().Trim().Equals("YZ"))
                {
                    QSFS = "延展";
                }
                else if (FS.ToUpper().Trim().Equals("LJ")) {
                    QSFS = "累计";
                }
                j = _gs.LastIndexOf(",");
                _gs = _gs.Substring(0, j);
            }
            string[] tjzhi = _gs.Split(',');
            string SJYID = set.Tables[0].Rows[0]["SJYID"].ToString();
            string tjmc = "";
            string sqltj = "select TABLEMC,TABLEZDMC,BM,TJDM from REPORT_CSSZ_SJYDYXTJ where SJYID='" + SJYID + "' order by  ID";
            DataSet tjset = bll.Query(sqltj);
            for (int h = 0; h < tjzhi.Length; h++)
            {
                string[] td = tjzhi[h].Split('|');
                //string td = tjzhi.ToString().Split('|');
                for (int k = 0; k < td.Length; k++)
                {
                    string sql1 = string.Empty;
                    sql1 = "select " + tjset.Tables[0].Rows[h]["TABLEZDMC"].ToString() + " from " + tjset.Tables[0].Rows[h]["TABLEMC"].ToString() + " where   " + tjset.Tables[0].Rows[h]["BM"].ToString() + "='" + td[k].Trim() + "'  ";
                    DataSet set1 = bll.Query(sql1.ToUpper());
                    if (set1.Tables[0].Rows.Count > 0)
                    {
                        tjmc += set1.Tables[0].Rows[0][0].ToString();
                    }
                    if (k < td.Length - 1)
                    {
                        tjmc += "|";
                    }
                }
                if (h < tjzhi.Length - 1)
                {
                    tjmc += ",";
                }

            }
            string tjmcs = string.Empty;
            for (int m = 0; m < tjmc.Split(',').Length; m++)
            {
                tjmcs = tjmcs + '"' + tjmc.Split(',')[m] + '"';
                if (m < tjmcs.Length - 1)
                {
                    tjmcs += ",";
                }
            }
            if (bBID.Trim().Equals("15"))
            {
                zgs = bBMC.ToUpper() + "(" + tjmcs + "\"" + QSFS.Trim() + "\"," + zfmc.Trim() + ",\"" + zqmc.Trim() + "\")";
            }
            else
            {
                zgs = bBMC.ToUpper() + "(" + tjmcs +zfmc.Trim() + ",\"" + zqmc.Trim() + "\")";
            }
        }
        return zgs;
    }


    //填报列数据录入
    public string tblsjlugsjx(string gs)
    {
        string gss = gs;
        int index = gss.IndexOf("(");
        string _gs = string.Empty;
        string zgs = string.Empty;
        if (index > -1)
        {
            _gs = gss.Substring(0, index);
            string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY  where DYGS='" + _gs + "'";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet set = bll.Query(sql);
            //报表名称
            string bBMC = set.Tables[0].Rows[0]["SJYMC"].ToString();
            //报表id
            string bBID = set.Tables[0].Rows[0]["SJYID"].ToString();
            //报表周期
            _gs = gss.Substring(index + 1);
            int j = _gs.LastIndexOf(",");
            string zq = _gs.Substring(j);
            zq = zq.Replace(")", "").Replace('"', ' ').Replace(",", "").Trim();
            string sqlzq = "select XMFL,XMDH_A,XMMC from XT_CSSZ where XMFL='H006' AND  XMDH_A=" + zq + " ";
            DataSet zqset = bll.Query(sqlzq);
            string zqmc = "";
            if (zqset.Tables[0].Rows.Count > 0)
            {
                zqmc = zqset.Tables[0].Rows[0]["XMMC"].ToString().Trim();
            }
            else
            {
                zqmc = "";
            }
            //报表值
            _gs = _gs.Substring(0, j);
            _gs = _gs.Replace('"', ' ').Trim();
            j = _gs.LastIndexOf(",");
            string zifu = _gs.Substring(j + 1);
            zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
            string zfsql = "select SJYID,BID,XBM,XMC from REPORT_CSSZ_SJYDYX where SJYID='" + bBID + "' and upper(XBM)='" + zifu + "'";
            DataSet zfset = bll.Query(zfsql);
            string zfmc = string.Empty;
            if (zfset.Tables[0].Rows.Count > 0)
            {
                zfmc = '"' + zfset.Tables[0].Rows[0]["XMC"].ToString() + '"';
            }
            else
            {
                zfmc = "''";
            }
            // 条件值

            j = _gs.LastIndexOf(",");
            _gs = _gs.Substring(0, j);
            string[] tjzhi = _gs.Split(',');
            string SJYID = set.Tables[0].Rows[0]["SJYID"].ToString();
            string tjmc = "";
            string sqltj = "select TABLEMC,TABLEZDMC,BM,TJDM from REPORT_CSSZ_SJYDYXTJ where SJYID='" + SJYID + "' order by  ID";
            DataSet tjset = bll.Query(sqltj);
            for (int h = 0; h < tjzhi.Length; h++)
            {
                string[] td = tjzhi[h].Split('|');

                for (int k = 0; k < td.Length; k++)
                {
                    string sql1 = "select " + tjset.Tables[0].Rows[h]["TABLEZDMC"].ToString() + " from " + tjset.Tables[0].Rows[h]["TABLEMC"].ToString() + " where   " + tjset.Tables[0].Rows[h]["TJDM"].ToString() + "='" + td[k].Trim() + "'  ";
                    DataSet set1 = bll.Query(sql1);
                    if (set1.Tables[0].Rows.Count > 0)
                    {
                        tjmc += set1.Tables[0].Rows[0][0].ToString();
                    }
                    if (k < td.Length - 1)
                    {
                        tjmc += "|";
                    }
                }

                if (h < tjzhi.Length - 1)
                {
                    tjmc += ",";
                }

            }
            string tjmcs = string.Empty;
            for (int m = 0; m < tjmc.Split(',').Length; m++)
            {
                tjmcs = tjmcs + '"' + tjmc.Split(',')[m] + '"';
                if (m < tjmcs.Length - 1)
                {
                    tjmcs += ",";
                }
            }
            zgs = bBMC.ToUpper() + "(" + tjmcs + zfmc + "," + zqmc + ")";

        }
        return zgs;
    }

    #endregion;

    #region Ajax方法

    //根据公式值获取当前公式的SJYID
    [AjaxPro.AjaxMethod]
    public string csgs(string gs)
    {
        int i = gs.IndexOf("(");
        string zhi = string.Empty;
        if (i > -1)
        {
            string _gs = gs.Substring(0, i);
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string sql = "select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY where DYGS='" + _gs + "'";
            DataSet set = bll.Query(sql);
            zhi = set.Tables[0].Rows[0]["SJYID"].ToString();
        }
        return zhi;
    }
    //获取当前字段值
    [AjaxPro.AjaxMethod]
    public string xgetzd(string id)
    {
        string sql = "select count(*) from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id + "' ";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        int count = int.Parse(set.Tables[0].Rows[0][0].ToString());
        string gs = Session["gsjsid"].ToString();
        int i = gs.LastIndexOf(",");
        string[] ziduan = gs.Split(',');

        string zhi = string.Empty;
        zhi = ziduan[count].ToString();
        if (i > -1)
        {
            zhi = zhi.Replace('"', ' ');
            zhi = zhi.Replace(")", "");
            zhi = zhi.Trim();
        }
        return zhi;
    }

    //获取当前周期值
    [AjaxPro.AjaxMethod]
    public string zhouqizhi(string id)
    {
        string gs = Session["gsjsid"].ToString();
        int i = gs.LastIndexOf(",");
        string zhi = string.Empty;
        if (i > -1)
        {
            zhi = gs.Substring(i + 1);
            zhi = zhi.Replace('"', ' ');
            zhi = zhi.Replace(")", "");
            zhi = zhi.Trim();
        }
        return zhi;
    }

    [AjaxPro.AjaxMethod]
    public string dm()
    {
        string gs = Session["gsjsid"].ToString();
        return gs;
    }
    //取得当前条件值
    [AjaxPro.AjaxMethod]
    public string xgettj(string id, string gs)
    {
        string sql = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id + "' order by ID";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        int i = gs.IndexOf("(");
        string zhi = string.Empty;
        DataSet set = bll.Query(sql);

        string _gs = string.Empty;
        if (i > -1)
        {
            _gs = gs.Substring(i + 1);
            int j = _gs.LastIndexOf(",");
            string zifu = _gs.Substring(j);
            _gs = _gs.Replace(zifu, "");
            j = _gs.LastIndexOf(",");
            zifu = _gs.Substring(j);
            _gs = _gs.Replace(zifu, "");
            _gs = _gs.Replace('"', ' ').Trim();
            string[] tjzhi = _gs.Split(',');
            if (tjzhi.Length > 0)
            {
                for (int s = 0; s < tjzhi.Length; s++)
                {
                    DataSet ds = new DataSet();

                    if (set.Tables[0].Rows[s]["SQL"].ToString() != "")
                    {
                        ds = bll.Query(set.Tables[0].Rows[s]["SQL"].ToString().ToUpper());
                        if (tjzhi[s].ToString() != "")
                        {
                            string xtjz = tjzhi[s].ToString();
                            for (int n = 0; n < xtjz.Split('|').Length; n++)
                            {
                                string qz = xtjz.Split('|')[n].ToString().Trim();
                                if (qz != "-")
                                {
                                    DataRow[] row = null;
                                    row = ds.Tables[0].Select("BM='" + qz + "'");
                                    if (row.Length > 0)
                                    {
                                        zhi += row[0]["BM"].ToString() + "." + row[0]["MC"].ToString();
                                    }

                                    if (n < xtjz.Split('|').Length - 1)
                                    {
                                        zhi += "|";
                                    }
                                }
                                else
                                {
                                    zhi += "-";
                                    if (n < xtjz.Split('|').Length - 1)
                                    {
                                        zhi += "|";
                                    }


                                }
                            }
                        }
                    }
                    if (s < tjzhi.Length - 1)
                    {
                        zhi += "$";
                    }

                }
            }
        }
        return zhi;
    }

    //获取公式值
    [AjaxPro.AjaxMethod]
    public string getgs()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        DataSet set = bll.Query(sql);
        string ss = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            ss += set.Tables[0].Rows[i]["SJYID"].ToString() + "," + set.Tables[0].Rows[i]["SJYMC"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                ss += "|";
            }
        }
        return ss;
    }
    /// <summary>
    ///刷新公式列表
    /// </summary>
    /// <param name="id">公式代码</param>
    /// <param name="ArrCs">参数</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string liebiao(string id, ArrayList ArrCs)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //条件
        string sql = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM,XH from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id.Trim() + "' order by SJYID,ID";
        //字段
        string sql1 = "select DYXID,SJYID,BID,BID,XBM,XMC,XZ from REPORT_CSSZ_SJYDYX where SJYID='" + id.Trim() + "'";
        DataSet set = bll.Query(sql);
        DataSet set1 = bll.Query(sql1);
        //预算项目取数函数取值字段
       
        string ss = string.Empty;

        StringBuilder str = new StringBuilder();
        if (id.Trim() != "2" && id.Trim()!="15")
        {

            str.Append("<table>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("字段");
            str.Append("</td>");
            str.Append("<td>");
            str.Append("<select style =\" width:170px\" id=\"zhiduan\">");

            for (int i = 0; i < set1.Tables[0].Rows.Count; i++)
            {
                str.Append("<option value='" + set1.Tables[0].Rows[i]["XBM"].ToString() + "'>");
                str.Append(set1.Tables[0].Rows[i]["XMC"].ToString());
                str.Append("</option>");
            }


            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("周期");
            str.Append("</td>");
            str.Append("<td>");
          
            string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + id.Trim() + "'";
            DataSet dtz = bll.Query(sqlz);
            DataSet zqSet;
            if (dtz.Tables[0].Rows.Count > 0)
            {
                zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
            }
            else
            {
                zqSet = null;
            }
            str.Append("<select style =\" width:170px\" id=\"zhouqi\">");
            if (zqSet != null)
            {
                for (int i = 0; i < zqSet.Tables[0].Rows.Count; i++)
                {
                    str.Append("<option value='" + zqSet.Tables[0].Rows[i]["ZFCS"].ToString() + "'>");
                    str.Append(zqSet.Tables[0].Rows[i]["XMMC"].ToString());
                    str.Append("</option>");
                }
            }
            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");

            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                str.Append("<tr>");
                str.Append("<td>");
                str.Append(set.Tables[0].Rows[i]["MC"].ToString());
                str.Append("</td>");
                str.Append("<td>");
                str.Append("<input id=" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + " type=\"text\" onclick=\"getvalue('" + set.Tables[0].Rows[i]["ID"].ToString() + "','" + set.Tables[0].Rows[i]["SJYID"].ToString() + "','" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + "','" + set.Tables[0].Rows[i]["XH"].ToString().ToLower() + "')\" style=\"width: 200px; \"  />");
                str.Append("</td>");
                str.Append("</tr>");
            }
            str.Append("</table>");
        }
        else
        {
            str.Append("<table>");
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                str.Append("<tr>");
                str.Append("<td>");
                str.Append(set.Tables[0].Rows[i]["MC"].ToString());
                str.Append("</td>");
                str.Append("<td>");
                str.Append("<input id=" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + " type=\"text\" onclick=\"getvalues('" + set.Tables[0].Rows[i]["ID"].ToString() + "','" + set.Tables[0].Rows[i]["SJYID"].ToString() + "','" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + "','" + set.Tables[0].Rows[i]["XH"].ToString().ToLower() + "')\" style=\"width: 200px; \"  />");
                str.Append("</td>");
                str.Append("</tr>");
            }
            //如果是预算变动项目取数公式的话，则须添加取数方式
            if (id == "15")
            {
                str.Append("<tr>");
                str.Append("<td>");
                str.Append("方式");
                str.Append("</td>");
                str.Append("<td>");
                str.Append("<select style =\" width:170px\" id=\"BDFS\">");
                str.Append("<option value=\"YZ\">延展</option>");
                str.Append("<option value=\"LJ\">累计</option>");
                str.Append("</select>");
                str.Append("</td>");
                str.Append("</tr>");
            }

            str.Append("<tr>");
            str.Append("<td>");
            str.Append("字段");
            str.Append("</td>");
            str.Append("<td>");
            str.Append("<select style =\" width:170px\" id=\"zhiduan\">");

            //for (int i = 0; i < setysxm.Tables[0].Rows.Count; i++)
            //{
            //    str.Append("<option value='" + set1.Tables[0].Rows[i]["ZFBM"].ToString() + "'>");
            //    str.Append(set1.Tables[0].Rows[i]["XMMC"].ToString());
            //    str.Append("</option>");
            //}
            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("周期");
            str.Append("</td>");
            str.Append("<td>");
            //string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + id + "'";
            //DataSet dtz = bll.Query(sqlz);
            //DataSet zqSet;
            //if (dtz.Tables[0].Rows.Count > 0)
            //{
            //    zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
            //}
            //else
            //{
            //    zqSet = null;
            //}
            str.Append("<select style =\" width:170px\" id=\"zhouqi\">");
            //if (zqSet != null)
            //{
            //    for (int i = 0; i < zqSet.Tables[0].Rows.Count; i++)
            //    {
            //        str.Append("<option value='" + zqSet.Tables[0].Rows[i]["ZFCS"].ToString() + "'>");
            //        str.Append(zqSet.Tables[0].Rows[i]["XMMC"].ToString());
            //        str.Append("</option>");
            //    }
            //}
            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("</table>");
        }
        ss = str.ToString();
        return ss;
    }
    //查询条件值
    [AjaxPro.AjaxMethod]
    public string tjz(string id)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql1 = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id.Trim() + "' order by ID ";
        DataSet set1 = bll.Query(sql1);
        string dm = string.Empty;


        for (int i = 0; i < set1.Tables[0].Rows.Count; i++)
        {
            dm += set1.Tables[0].Rows[i]["TJDM"].ToString().ToLower();
            if (i < set1.Tables[0].Rows.Count - 1)
            {
                dm += ",";
            }
        }
        return dm;
    }
    //添加公式
    [AjaxPro.AjaxMethod]
    public string addgs(string id, string zhi,string QSFS,string zd, string zq)
    {
        string sql = "select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY WHERE SJYID='" + id.Trim() + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string ss = string.Empty;
        ss = set.Tables[0].Rows[0]["DYGS"].ToString().ToUpper();
        ss += "(";
        for (int i = 0; i < zhi.Split('+').Length; i++)
        {
            ss = ss + '"';
            string dmzhi = string.Empty;
            for (int j = 0; j < zhi.Split('+')[i].Split('|').Length; j++)
            {
                dmzhi += zhi.Split('+')[i].Split('|')[j].Split('.')[0].ToString();
                if (j < zhi.Split('+')[i].Split('|').Length - 1)
                {
                    dmzhi += "|";
                }
            }
            ss += dmzhi;
            //ss +=zhi.Split('+')[i].ToString();
            ss = ss + '"';
            if (i < zhi.Split('+').Length - 1)
            {
                ss += ",";
            }
        }
        //如果是预算变动行取数函数的话，则须添加取数方式（YZ:延展；LJ:累计）
        if (id.Trim().Equals("15"))
        {
            ss += ",";
            ss = ss + '"';
            ss += QSFS.ToUpper();
            ss = ss + '"';
        }
        ss += ",";
        ss = ss + '"';
        ss += zd.ToUpper();
        ss = ss + '"';
        ss += ",";
        ss = ss + '"';
        ss += zq;
        ss = ss + '"';
        ss += ")";
        //string sql1="select a.id,a.mc,a.TJDM,a.type,a.SJYID,sql,bm from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id + "'";
        //DataSet set1 = bll.Query(sql1);
        //string dm= string.Empty;
        //for (int i = 0; i < set1.Tables[0].Rows.Count;i++)
        //{

        //}
        return ss;
    }

    [AjaxPro.AjaxMethod]
    public string retjx(string gs, string fh)
    {
        string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gs.Contains(gygss))
            {
                do
                {
                    int index_i = gs.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString().ToUpper());
                    int end2 = gs.IndexOf(")", index_i);
                    string gss = gs.Substring(index_i, end2 - index_i + 1);
                    fhz = fhgs(gss);
                    gs = gs.Replace(gss, fhz);
                }
                while (gs.Contains(gygss));
            }
        }
        return gs.Trim();

    }

    //获取一个公式
    [AjaxPro.AjaxMethod]
    public string getfirstgs(string gs)
    {
        gs = Session["gsjsid"].ToString();
        string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim();
            if (gs.Contains(gygss))
            {

                int index_i = gs.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString());
                int end2 = gs.IndexOf(")", index_i);
                fhz = gs.Substring(index_i, end2 - index_i + 1);
                break;
            }
        }
        return fhz;

    }
    /// <summary>
    /// 根据模板获取字段值
    /// </summary>
    /// <param name="parid"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string zhiduanzhi(string mb)
    {
        //string sql = "SELECT * FROM TB_JSDX WHERE DM IN(SELECT distinct JSDX FROM TB_MBCOLSX WHERE MBDM IN('" + mb.Replace(",","','") + "'))";
        string sql = "SELECT * FROM TB_JSDX";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string ss = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            ss += set.Tables[0].Rows[i]["ZFBM"].ToString() + "," + set.Tables[0].Rows[i]["XMMC"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                ss += "|";
            }
        }
        return ss;
    }
    /// <summary>
    /// 根据模板获取周期
    /// </summary>
    /// <param name="parid"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string zhouqizhi1(string mb,string SJYID)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + SJYID + "'";
            DataSet dtz = bll.Query(sqlz);
            DataSet zqSet;
            if (dtz.Tables[0].Rows.Count > 0)
            {
                zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
            }
            else
            {
                zqSet = null;
            }

            //DataSet set = bll.Query(zqSet.Tables[0].Rows[0]["SQL"].ToString().Replace(":MBDM", mb.Replace(",", "','")));
            string ss = string.Empty;
            for (int i = 0; i < zqSet.Tables[0].Rows.Count; i++)
            {
                ss += zqSet.Tables[0].Rows[i]["ZFCS"].ToString() + "," + zqSet.Tables[0].Rows[i]["XMMC"].ToString();
                if (i < zqSet.Tables[0].Rows.Count - 1)
                {
                    ss += "|";
                }
            }
            return ss;
        }
        catch
        {
            return "";
        }
    }

    /// <summary>
    /// 根据模板获取周期
    /// </summary>
    /// <param name="parid"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int IsCanSetBDGS(string MBDM, string SHEETNAME)
    {
        int Flag = -1;
        bool F=GetDataList.IsCanSetBDGS(MBDM,SHEETNAME);
        if (F)
        {
            Flag = 1;
        }
        return Flag;
    }

    #endregion;
 
}
