<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExcelBZ.aspx.cs" Inherits="Excels_ExcelMb_ExcelMb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="PageOffice, Version=3.0.0.1, Culture=neutral, PublicKeyToken=1d75ee5788809228"
    Namespace="PageOffice" TagPrefix="po" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>模板设置</title>
    <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
    <link href="../../Content/themes/icon.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" type="text/css" href="../../CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style.css" />
    <script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <script src="../../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../../JS/jquery.zclip.js" type="text/javascript"></script>
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript">
        function cxlb() {
            var wjm = $("#<%=HidMbwj.ClientID %>").val();
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "CopyFile1",
                    filename: wjm
                },
                async: true,
                cache: false,
                success: function (result) {
                    document.getElementById("PageOfficeCtrl1").WebOpen("../XLSBZ/" + wjm + "", "xlsNormalEdit", "hghfhfh");
                    //document.getElementById("PageOfficeCtrl1").close();
                    //document.getElementById("PageOfficeCtrl1").JsFunction_OnExcelCellClick = "OnCellClick()";
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
        //本地打开：
        function OpenAsFile() {
            po.poctrl.ShowDialog(1);

        }

        function Save() {
            $.messager.confirm('提示：', '是否保存?', function (r) {
                if (r) {
                    po.setvisible(false);
                    po.poctrl.EnableExcelCalling();
                    document.getElementById("PageOfficeCtrl1").WebSave();
                    alert("备注已保存");

                }
                po.setvisible(true);
            });
        }
        $(document).ready(function () {  cxlb(); po.poctrl = document.getElementById("PageOfficeCtrl1"); });    

    </script>
</head>
<body>
   <form id="form1" runat="server" class="easyui-layout" style="width:100%; height:100%;">
    <div style="width:100%;height:100%">
      <po:PageOfficeCtrl ID="PageOfficeCtrl1" runat="server" Menubar="false" OfficeToolbars="false">
       </po:PageOfficeCtrl>
    </div>
      <input type="hidden" id="HidMbwj" runat="server" />
    </form>
</body>
</html>
