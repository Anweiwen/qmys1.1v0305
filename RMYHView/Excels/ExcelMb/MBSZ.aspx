﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Spread/WebExcel.master" AutoEventWireup="true"
CodeFile="MBSZ.aspx.cs" Inherits="Excels_test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head2" runat="Server">
    <script type="text/javascript">
        (function(window) {
            var hszxdm = "",
                yy = "",
                userdm = "",
                msg = "",
                strs = "",
                symbol = "",
                resval = "",
                node,
                flag = "",
                rowacbs = undefined,
                colacbs = undefined,
                gsacbs = undefined,
                hideacbs = undefined,
                mark = 1,
                Bm = 0,
                sellx = "",
                sellxjson = "",
                obj = {},
                batch = {},
                getJsdx = undefined,
                getXmfl = undefined;
            var zydm = "",
                cpdm = "",
                jsdx = "",
                xmfl = "",
                sjdx = "",
                jldw = "",
                addData = "",
                upData = "",
                editRows = "";
            //避免污染全局变量

            $(function() {
                //扩展easyui editor列属性，编辑列为只读状态。
                $.extend($.fn.datagrid.defaults.editors,
                    {
                        textReadonly: {
                            init: function(container, options) {
                                var input = $('<input type="text" readonly="readonly" class="datagrid-editable-input">')
                                    .appendTo(container);
                                return input;
                            },
                            getValue: function(target) {
                                return $(target).val();
                            },
                            setValue: function(target, value) {
                                $(target).val(value);
                            },
                            resize: function(target, width) {
                                var input = $(target);
                                if ($.boxModel == true) {
                                    input.width(width - (input.outerWidth() - input.width()));
                                } else {
                                    input.width(width);
                                }
                            }
                        }


                    });

                $('#fy').datagrid({
                    detailFormatter: function(rowIndex, rowData) {
                        return '<table><tr>' +
                            '<td style="border:0">' +
                            '<p>公式翻译:</p>' +
                            '<p>' +
                            rowData.GSFY +
                            '</p>' +
                            '</td>' +
                            '</tr></table>';
                    }
                });

                $('#rowdg').datagrid({
                    width: function() {
                        return document.body.clientWidth
                    },
                    height: 450,
                    nowrap: true,
                    rownumbers: true,
                    fit: true,
                    animate: false,
                    singleSelect: true,
                    frozenColumns: [
                        [
                            {
                                field: 'ck',
                                checkbox: true
                            }
                        ]
                    ],
                    columns: [
                        [
                            {
                                field: 'HH',
                                title: '行号',
                                width: '5%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'HMC',
                                title: '行名称',
                                width: '10%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'ZYDM',
                                title: '成本中心',
                                width: '20%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        editable: true,
                                        onShowPanel: function() {
                                            $(this).combotree('loadData', zydm);
                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }
                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'CPDM',
                                title: '对应项目',
                                width: '25%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        onShowPanel: function() {
                                            $(this).combotree('loadData', cpdm);

                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }

                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'JSDX',
                                title: '计算对象',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'text',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', jsdx);
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'XMFL',
                                title: '项目分类',
                                width: '25%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'text',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', xmfl);
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            }
                        ]
                    ],
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function(rowIndex, rowData, changes) {

                        var inserted = $('#rowdg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                        var updated = $('#rowdg').datagrid('getChanges', 'updated'); //获取要执行的更新行
                        if (inserted.length > 0) { //执行增加方法
                            addData = inserted;
                        }
                        if (updated.length > 0) { //执行修改方法
                            upData = updated;
                        }

                        $('#rowdg').datagrid('unselectAll');
                    },
                    //双击选择行
                    onDblClickRow: function(rowIndex, rowData) {
                        if (rowacbs != undefined) {
                            $('#rowdg').datagrid('endEdit', rowacbs);
                            rowacbs = undefined;
                        }
                        if (rowacbs == undefined) {
                            $('#rowdg').datagrid('beginEdit', rowIndex);
                            rowacbs = rowIndex;
                        }
                    }
                });

                $('#coldg').datagrid({
                    width: function() {
                        return document.body.clientWidth
                    },
                    height: 450,
                    nowrap: true,
                    rownumbers: true,
                    fit: true,
                    animate: false,
                    singleSelect: true,
                    frozenColumns: [
                        [
                            {
                                field: 'ck',
                                checkbox: true
                            }
                        ]
                    ],
                    columns: [
                        [
                            {
                                field: 'LN',
                                title: '列',
                                width: '5%',
                                hidden: true,
                                align: 'left'
                            },
                            {
                                field: 'LH',
                                title: '列号',
                                width: '5%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'LMC',
                                title: '列名称',
                                width: '10%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'ZYDM',
                                title: '作业代码',
                                width: '20%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        editable: true,
                                        onShowPanel: function() {
                                            // $(this).combotree('tree').tree('options').data = res;
                                            // $(this).combotree({ panelWidth: $(this).combotree('options').width * 1.5 });
                                            $(this).combotree('loadData', zydm);
                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }

                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'XMDM',
                                title: '项目代码',
                                width: '20%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        onShowPanel: function() {
                                            $(this).combotree('loadData', cpdm);

                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }

                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'SJDX',
                                title: '时间对象',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'text',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', sjdx);
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'JSDX',
                                title: '计算对象',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'text',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', jsdx);
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'JLDW',
                                title: '计量单位',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'text',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', jldw);
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            }
                        ]
                    ],
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function(rowIndex, rowData, changes) {

                        var inserted = $('#coldg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                        var updated = $('#coldg').datagrid('getChanges', 'updated'); //获取要执行的更新行
                        if (inserted.length > 0) { //执行增加方法
                            addData = inserted;
                        }
                        if (updated.length > 0) { //执行修改方法
                            upData = updated;
                        }
                        $('#coldg').datagrid('unselectAll');
                    },
                    //双击选择行
                    onDblClickRow: function(rowIndex, rowData) {
                        if (colacbs != undefined) {
                            $('#coldg').datagrid('endEdit', colacbs);
                            colacbs = undefined;
                        }
                        if (colacbs == undefined) {
                            $('#coldg').datagrid('beginEdit', rowIndex);
                            colacbs = rowIndex;
                        }
                    }
                });

                $('#xygsdg').datagrid({
                    width: function() {
                        return document.body.clientWidth
                    },
                    height: 450,
                    nowrap: true,
                    rownumbers: true,
                    fit: true,
                    animate: false,
                    fitColumns: true,
                    collapsible: true,
                    lines: true,
                    maximizable: true,
                    maximized: true,
                    singleSelect: false,
                    frozenColumns: [
                        [
                            {
                                field: 'ck',
                                checkbox: true
                            }
                        ]
                    ],
                    pageSize: 10, // 默认选择的分页是每页5行数据
                    pageList: [10, 20, 30, 40], // 可以选择的分页集合
                    pagination: true, // 分页
                    rownumbers: true, // 行数
                    columns: [
                        [
                            {
                                field: 'ID',
                                title: 'ID',
                                hidden: true,
                                align: 'left'
                            },
                            {
                                field: 'MBDM',
                                title: '模板代码',
                                hidden: true,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'SHEETNAME',
                                title: 'SHEET名',
                                hidden: true,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'GSBH',
                                title: '公式编号',
                                width: 40,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'GS',
                                title: '公式',
                                width: 120,
                                align: 'left',
                                editor: {
                                    type: 'textReadonly'
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'INFOMATION',
                                title: '校验错误提示',
                                width: 110,
                                align: 'left',
                                editor: {
                                    type: 'validatebox'
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'GSJX',
                                title: '公式中文翻译',
                                width: 120,
                                align: 'left',
                                editor: {
                                    type: 'textReadonly'
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'LX',
                                title: '类型',
                                width: 120,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        data: [
                                            {
                                                "id": 0,
                                                "text": "错误"
                                            }, {
                                                "id": 1,
                                                "text": "警告"
                                            }
                                        ],
                                        valueField: 'id',
                                        textField: 'text'
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            }
                        ]
                    ],
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function(rowIndex, rowData, changes) {
                        var inserted = $('#xygsdg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                        var updated = $('#xygsdg').datagrid('getChanges', 'updated'); //获取要执行的更新行
                        if (inserted.length > 0) { //执行增加方法
                            addData = inserted;
                        }
                        if (updated.length > 0) { //执行修改方法
                            upData = updated;
                        }
                        $('#xygsdg').datagrid('unselectAll');
                        gsacbs = undefined;
                    },
                    //双击选择行
                    onDblClickRow: function(rowIndex, rowData) {
                        if (gsacbs != undefined) {
                            $('#xygsdg').datagrid('endEdit', gsacbs);
                        }
                        if (gsacbs == undefined) {
                            $('#xygsdg').datagrid('beginEdit', rowIndex);
                            gsacbs = rowIndex;
                        }
                    }
                });

                $('#showHideCol').datagrid({
                    width: function() {
                        return document.body.clientWidth;
                    },
                    height: 450,
                    nowrap: true,
                    rownumbers: true,
                    fit: true,
                    animate: false,
                    singleSelect: true,
                    frozenColumns: [
                        [
                            {
                                field: 'ck',
                                checkbox: true
                            }
                        ]
                    ],
                    columns: [
                        [
                            {
                                field: 'MBDM',
                                title: '模板代码',
                                width: '8%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'SHEETNAME',
                                title: 'sheet名称',
                                width: '8%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'COLS',
                                title: '列号',
                                width: '8%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        editable: true,
                                        onShowPanel: function() {
                                            $(this).combotree('loadData', zydm);
                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }
                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'GS',
                                title: '公式',
                                width: '30%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combotree',
                                    options: {
                                        onShowPanel: function() {
                                            $(this).combotree('loadData', cpdm);

                                        },
                                        onChange: function(n, o) {
                                            var text = "";
                                            if ($(this).combotree('tree').tree('getSelected') != null) {
                                                text = $(this).combotree('tree').tree('getSelected').text;
                                                $(this).combotree('setValue', text);
                                                $(this).combotree('setText', text);
                                            }

                                        },
                                        onBeforeExpand: function(node) {

                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'GSJX',
                                title: '公式解析',
                                width: '35%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        valueField: 'id',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            $(this).combobox('loadData', jsdx);
                                        },
                                        onSelect: function(param) {
                                            $(this).combobox('setValue', param.id + '.' + param.text);
                                        }
                                    }
                                },
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'ISYC',
                                title: '隐藏类型',
                                width: '11%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            }
                        ]
                    ],
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function(rowIndex, rowData, changes) {

                    },
                    //单击选择行
                    onClickRow: function(rowIndex, rowData) {
                        getLx();
                        getGs();
                        //给下拉框赋值
                        $('#cols').combobox('setValues', rowData.COLS);
                        $('#ISYC').combobox('setValues', rowData.ISYC);
                        showSetRow(rowData.MBDM, rowData.SHEETNAME, rowData.COLS);
                    }
                });

                $('#setHideCol').datagrid({
                    width: function() {
                        return document.body.clientWidth;
                    },
                    height: 450,
                    nowrap: true,
                    rownumbers: true,
                    fit: true,
                    animate: false,
                    singleSelect: true,
                    frozenColumns: [
                        [
                            {
                                field: 'ck',
                                checkbox: true
                            }
                        ]
                    ],
                    columns: [
                        [
                            {
                                field: 'gsid',
                                title: '列代码',
                                width: '0%',
                                hidden: true,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'mbdm',
                                title: '模板代码',
                                width: '10%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'sheet',
                                title: 'sheet名称',
                                width: '10%',
                                hidden: false,
                                align: 'left',
                                formatter: function(value) {
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'lh',
                                title: '列号',
                                width: '0%',
                                hidden: true,
                                align: 'left',
                                formatter: function(value, row) {
                                    value = value == "" ? $("#cols").combobox('getText') : value;
                                    row.lh = value; //为列号赋值
                                    return "<span title='" + value + "'>" + value + "</span>";
                                }
                            },
                            {
                                field: 'gslx',
                                title: '公式类型',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        panelHeight: 'auto',
                                        valueField: 'id',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            getLx();
                                            getGs();
                                            $(this).combobox('loadData', sellx["GSLX"]);
                                        },
                                        onChange: function(n, o) {
                                            debugger;
                                            var keyArray = new Array(); //修改处
                                            for (var key in sellx["GSLX"]) {
                                                if (sellx["GSLX"][key].id.indexOf(n) != -1 ? true : false) {
                                                    $(this).combobox('setText', sellx["GSLX"][key].text);
                                                    break;
                                                }
                                            }
                                            var row = $('#setHideCol').datagrid('getSelected');
                                            var rindex = $('#setHideCol').datagrid('getRowIndex', row);
                                            var ed = $('#setHideCol').datagrid('getEditor',
                                                { index: rindex, field: 'gs' });
                                            for (var key in sellxjson) { //修改处
                                                keyArray.push(key);
                                            }
                                            $(ed.target).combobox('clear');
                                            $(ed.target).combobox('loadData', sellxjson[keyArray[n - 1]]); //修改处
                                        },
                                        onSelect: function(param) {

                                        }
                                    }
                                },
                                formatter: function(value, row) {
                                    if (value == "") {
                                        return "<span title='" + value + "'>" + value + "</span>";
                                    } else {
                                        for (var key in sellx["GSLX"]) {
                                            if (sellx["GSLX"][key].id.indexOf(value) != -1 ? true : false) {
                                                row.lxtext = sellx["GSLX"][key].text;
                                                return "<span title='" +
                                                    value +
                                                    "'>" +
                                                    sellx["GSLX"][key].text +
                                                    "</span>";
                                                break;
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                field: 'lxtext',
                                title: '类型显示',
                                width: '0%',
                                hidden: true,
                                align: 'left'
                            },
                            {
                                field: 'gs',
                                title: '公式',
                                width: '45%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        multiple: true,
                                        panelHeight: '200px',
                                        valueField: 'id',
                                        textField: 'text',
                                        onShowPanel: function() {

                                        },
                                        onChange: function(n, o) {
                                            var row = $('#setHideCol').datagrid('getSelected');
                                            var rindex = $('#setHideCol').datagrid('getRowIndex', row);
                                            var ed = $('#setHideCol').datagrid('getEditor',
                                                {
                                                    index: rindex,
                                                    field: 'gs'
                                                });

                                        },
                                        onSelect: function(param) {
                                            //$(this).combobox('setValues', param.text);
                                        }
                                    }
                                },
                                formatter: function(value, row) {
                                    var arr = value.split(','), val = "", sel = "";
                                    if (value != "") {
                                        var keyArray = new Array(); //修改处
                                        for (var key in sellxjson) { //修改处
                                            keyArray.push(key);
                                        }
                                        sel = sellxjson[keyArray[row.gslx - 1]]; //修改处
                                        for (var i = 0, m = arr.length; i < m; i++) {
                                            for (var j = 0, max = sel.length; j < max; j++) {
                                                if (arr[i] == sel[j]["id"]) {
                                                    val += sel[j]["text"];
                                                }
                                            }
                                            if (i < m - 1) {
                                                val += ',';
                                            }
                                        }
                                    }
                                    row.gsjx = value == "" ? value : val; //为公式解析列赋值
                                    return "<span title='" + value + "'>" + value == "" ? value : val + "</span>";
                                }
                            },
                            {
                                field: 'gsjx',
                                title: '公式解析',
                                width: '0%',
                                hidden: true,
                                align: 'left'
                            },
                            {
                                field: 'condition',
                                title: '关联条件',
                                width: '15%',
                                hidden: false,
                                align: 'left',
                                editor: {
                                    type: 'combobox',
                                    options: {
                                        panelHeight: 'auto',
                                        valueField: 'id',
                                        textField: 'text',
                                        onShowPanel: function() {
                                            getLx();
                                            $(this).combobox('loadData', sellx["TJANDOR"]);
                                        },
                                        onChange: function(n, o) {
                                            if (n == " ") {
                                                $(this).combobox('setText', "");
                                            } else {
                                                $(this).combobox('setText', sellx["TJANDOR"][n - 1].text);
                                            }

                                        },
                                        onSelect: function(param) {
                                        }
                                    }
                                },
                                formatter: function(value) {
                                    if (value == "" || value == " ") {
                                        return "<span title='" + value + "'>" + value + "</span>";
                                    } else {
                                        return "<span title='" +
                                            value +
                                            "'>" +
                                            sellx["TJANDOR"][parseInt(value) - 1].text +
                                            "</span>";
                                    }
                                }
                            }
                        ]
                    ],
                    onBeforeEdit: function(rowIndex, rowData) {
                        $('#setHideCol').datagrid('selectRow', rowIndex);
                    },
                    //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                    onAfterEdit: function(rowIndex, rowData, changes) {

                        //            var inserted = $('#setHideCol').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                        //            var updated = $('#setHideCol').datagrid('getChanges', 'updated');   //获取要执行的更新行
                        //            var deleted = $('#setHideCol').datagrid('getChanges', 'deleted');   //获取要删除的行
                        //            if (inserted.length > 0) {//执行增加方法
                        //                batch["add"]=inserted;
                        //            }
                        //            if (updated.length > 0) {//执行修改方法
                        //                 batch["upd"] = updated;
                        //            }
                        //            if (deleted.length > 0) {//执行删除方法
                        //                 batch["del"] = deleted;
                        //            }
                    },
                    //双击选择行
                    onDblClickRow: function(rowIndex, rowData) {
                        if (hideacbs != undefined) {
                            $('#setHideCol').datagrid('endEdit', hideacbs);
                            hideacbs = undefined;
                        }
                        if (hideacbs == undefined) {
                            $('#setHideCol').datagrid('beginEdit', rowIndex);
                            hideacbs = rowIndex;
                        }
                    },
                    onClickRow: function(rowIndex, rowData) {
                        if (hideacbs != undefined) {
                            $('#setHideCol').datagrid('endEdit', hideacbs);
                            hideacbs = undefined;
                        }
                    },
                    rowStyler: function(index, row) {
                        if (row.mbdm == "dels") {
                            return 'background-color:#6293BB;color:#fff;'; // return inline style
                        }
                    }
                });

            })

            window.clear = function() {
                var id = $(this)[0].id;
                var pop = document.getElementById("" + id + "").getAttribute('children');
                //获取父辈属性
                if (pop != null && pop != "") {
                    for (var i = 0, max = pop.split(',').length; i < max; i++) {
                        $('#' + pop.split(',')[i].toLowerCase() + '').combotree('tree').tree('loadData', []);
                        //清除树
                        $('#' + pop.split(',')[i].toLowerCase() + '').combotree('clear');
                        //清除树
                    }
                }
            }
            window.LoadGSData = function(node, checked) {

                if (checked) {
                    var SJYID = document.getElementById(node.attributes.comboxid).getAttribute('msg').split(',')[1];
                    $("#zhiduan").combobox({
                        url: '../ExcelMb/ExcelData.ashx?action=JS',
                        editable: false,
                        valueField: 'id',
                        textField: 'text',
                        onLoadSuccess: function() {
                            var data = $(this).combobox('getData');
                            if (data.length > 0) {
                                $(this).combobox('setValue', data[0].id);
                            }
                        }
                    });


                    $("#zhouqi").combobox({
                        url: '../ExcelMb/ExcelData.ashx?action=ZQ&&SJYID=' + SJYID,
                        editable: false,
                        valueField: 'id',
                        textField: 'text',
                        onLoadSuccess: function() {
                            var ZQ = $(this).combobox('getData');
                            if (ZQ.length > 0) {
                                $(this).combobox('setValue', ZQ[0].id);
                            }
                        }
                    });
                }

            }
            window.GetTree = function(node) {
                var text = "", str = "";
                debugger;
                if (document.getElementById(node.attributes.comboxid).getAttribute('parents') != "") {
                    var arr = document.getElementById(node.attributes.comboxid).getAttribute('parents').split(',');
                    for (var i = 0, max = arr.length; i < max; i++) {
                        if ($("#" + arr[i].toLowerCase() + "").combobox('getValue') == "") {
                            if (text != "") {
                                text += ','
                            }
                            text += document.getElementById("" + arr[i].toLowerCase() + "").parentNode.parentNode
                                .children[0].innerHTML;
                        } else {
                            if (str != "") {
                                str += ","
                            }
                            str += arr[i] + "|" + "'" + $("#" + arr[i].toLowerCase() + "").combobox('getValue') + "'";
                        }
                    }
                }
                var childrens = $("#" + node.attributes.comboxid + "").combotree('tree')
                    .tree('getChildren', node.target);
                var url = geturlpath() +
                    'ExcelData.ashx?action=GetList&parid=' +
                    node.id +
                    '&msg=' +
                    document.getElementById(node.attributes.comboxid).getAttribute('msg') +
                    '&yy=' +
                    yy +
                    '&hszxdm=' +
                    hszxdm +
                    '&userdm=' +
                    userdm +
                    '&str=' +
                    str +
                    '';

                if (childrens == false) {
                    $.ajax({
                        type: 'get',
                        url: url,
                        async: false,
                        success: function(result) {
                            $("#" + node.attributes.comboxid + "").combotree('tree').tree('append',
                                {
                                    parent: node.target,
                                    data: JSON.parse(result)
                                });
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }
                //        }
            }
            /**
            * 设置公式时，ajax请求服务器，返回下拉树数据
            **/
            window.getTreeData = function() {
                debugger;
                var id = $(this)[0].id;
                if ($("#" + id + "").combotree('tree').tree('getRoot') != null) {
                    return;
                }
                hszxdm = parent.parent.document.getElementById('HidHSZXDM').value;
                yy = parent.parent.document.getElementById('HidYY').value;
                userdm = parent.parent.document.getElementById('HidUSER').value;
                msg = document.getElementById(id).getAttribute('msg');
                var res = document.getElementById(id).getAttribute('parents');
                var suburl = geturlpath() +
                    'ExcelData.ashx?action=GetList&parid=0&msg=' +
                    msg +
                    '&yy=' +
                    yy +
                    '&hszxdm=' +
                    hszxdm +
                    '&userdm=' +
                    userdm +
                    '';
                var text = "", str = "";
                if (res !== "") {
                    //如果关联选项没值的话，就提示设置并返回
                    var arr = res.split(',');
                    for (var i = 0, max = arr.length; i < max; i++) {
                        if ($("#" + arr[i].toLowerCase() + "").combobox('getValue') == "") {
                            if (text != "") {
                                text += '@'
                            }
                            text += document.getElementById("" + arr[i].toLowerCase() + "").parentNode.parentNode
                                .children[0].innerHTML;
                        } else {
                            if (str != "") {
                                str += "@"
                            }
                            var valueText = $("#" + arr[i].toLowerCase() + "").combobox('getText');
                            if (valueText.indexOf(',') > -1) {
                                var ArrValue = valueText.split(',');
                                valueText = "";
                                for (var j = 0; j < ArrValue.length; j++) {
                                    valueText = valueText == ""
                                        ? "'" + ArrValue[j].split('.')[0].toString() + "'"
                                        : valueText + ",'" + ArrValue[j].split('.')[0].toString() + "'";
                                }
                                str += arr[i] + "|" + valueText;
                            } else {
                                str += arr[i] +
                                    "|" +
                                    "'" +
                                    $("#" + arr[i].toLowerCase() + "").combobox('getValue') +
                                    "'";
                            }
                        }
                    }
                    if (text != "") {
                        prompt("【" + text + "】" + "不能为空！！");
                        return;
                    }
                    strs = str;
                }

                $.ajax({
                    type: 'get',
                    url: suburl + '&str=' + str + '',
                    async: false,
                    success: function(result) {
                        //console.log("id:" + id);
                        $('#' + id + '').combotree('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            }

            window.getCorrelation = function() {
                var res = "";
                $.ajax({
                    type: 'get',
                    url: geturlpath() +
                        'ExcelData.ashx?action=getCorrelation&ID=' +
                        $('#selformula').combobox('getValue'),
                    async: false,
                    success: function(result) {
                        res = result
                    },
                    error: function() {
                        alert("错误!");
                    }
                })
                return res;
            }

            window.addgs = function() {
                var correlation = getCorrelation();
                var id = $('#selformula').combobox('getValue');
                var zd = $('#zhiduan').combobox('getValue');
                var zq = $("#zhouqi").combotree('getValue');
                var p = "", count = 0;

                if (correlation == "" || correlation == null) {
                    return;
                }
                var values = "";
                for (var i = 0; i < correlation.split(',').length; i++) {
                    values += $("#" + correlation.split(',')[i] + "").combotree('getText');
                    if (i < correlation.split(',').length - 1) {
                        values += "+";
                    }
                }
                if (id == 15) {
                    values += "+";
                    values += $('#FS').combobox('getValue');
                }

                if (zq == null) {
                    prompt("字段不能为空");
                } else {
                    $.ajax({
                        type: 'post',
                        url: geturlpath() + 'ExcelData.ashx?action=AddGs',
                        data: {
                            id: id,
                            values: encodeURIComponent(values),
                            zd: zd,
                            zq: zq
                        },
                        //url: geturlpath() + 'ExcelData.ashx?action=AddGs&id=' + id + '&values=' + encodeURIComponent(values) + '&zd=' + zd + '&zq=' + zq + '',
                        async: false,
                        success: function(result) {
                            resval = result
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }
                var gsval = $("#gs").val();
                var sign = gsval.replace(p, "");
                sign = sign.replace(resval, "");
                symbol += sign;
                if (count > 0) {
                    symbol = symbol + ",";
                }

                resval = gsval + resval;
                $("#gs").val(resval);
                var ss = $("#gs").val();
                p = ss;
                count = count + 1;
            }

            window.gsjx = function() {
                var gsval = $("#gs").val();
                $.ajax({
                    type: 'post',
                    data: {
                        gsval: gsval,
                        resval: resval
                    },
                    url: geturlpath() + 'ExcelData.ashx?action=GsJx',
                    async: false,
                    success: function(result) {
                        $("#con").val(result);
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            }

            window.ok = function() {
                //设置公式的时候点击执行
                if (mark == 1) {
                    var gsval = "N_" + $("#gs").val();
                    var sheet = spread.getActiveSheet();
                    if (sheet.getSelections()[0].colCount > 1 || sheet.getSelections()[0].rowCount > 1) {
                        prompt("请选择一个单元格");
                    } else {
                        sheet.suspendPaint();
                        sheet.getCell(sheet.getSelections()[0].row, sheet.getSelections()[0].col).formula(gsval);
                        sheet.resumePaint();
                    }
                } else { //设置公式效验的时候执行
                    AddGS($('#gs').val(), $('#con').val());
                }
            }

            window.cancel = function() {
                QK();
                $("#dlg").dialog("close");
            }

            //清空之前所有操作过的html元素
            window.QK = function() {
                $("#selformula").combobox('setValue', '');
                $("#ggsz").html("");
                $("#gs").val("");
                $("#con").val("");
            }

            //填充行数据
            window.TCHSJ = function() {
                //模板名称
                var colIndex = "", strartRow = "";
                //选择所在列
                var mcl = $('#mcl').textbox("getValue");
                if (mcl == "") {
                    prompt("请输入列名称！");
                    return;
                }
                //$("#HidH").val(lmc);
                //当前sheet页
                var sheetname = spread.getActiveSheet().name();
                //$("#hidSHEET").val(sheet);
                //当前excel有数据的有效行数,列数
                var rows = spread.getActiveSheet().getRowCount();
                var cols = spread.getActiveSheet().getColumnCount();
                //当前excel有数据的首行地址
                //行号
                var hh = 0;

                for (var i = 0; i < cols; i++) {
                    if (spread.getActiveSheet().getText(0, i, spreadNS.SheetArea.colHeader) == mcl.toUpperCase()) {
                        colIndex = i;
                        break;
                    }
                }

                //获取名称列不为空的第一行行号
                for (var i = 0; i < cols; i++) {
                    var values = spread.getActiveSheet().getCell(i, colIndex).value();
                    if (values != null) {
                        hh = i;
                        break;
                    }
                }
                //        var ss = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows[0].Address;
                //        ss = ss.replace("$", "").replace("$", "");
                //首行行号
                //        var shhh = ss.replace(lmc,"");
                var shhh = hh;
                //

                //起始行
                //var strartRow = Excels_ExcelMb_ExcelMb.getStartRows(mbdm).value;
                $.ajax({
                    type: 'post',
                    url: geturlpath() + 'ExcelData.ashx?action=getStartRowsOrCols',
                    data: {
                        flag: "STARTROW",
                        mbdm: row
                    },
                    async: false,
                    success: function(result) {
                        strartRow = result - 1; //spreadjs 行列索引都是从0开始
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

                var values = [];
                var arr = "";
                //如果起始行大于当前有数据的行号
                //    if (strartRow < shhh) {
                //        for (var i = shhh; i < parseInt(rows) + parseInt(shhh); i++) {

                //            var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                //            var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
                //            if (Hvalue != undefined) {
                //                //jsAddDataH(Hvalue, i);
                //                values.push(i + "|" + Hvalue + "|" + hmbvalue);
                //            }
                //        }
                //    }
                //    else {
                //        //起始行和当前有数据的首行差值
                //        var cz = strartRow - shhh;
                //        rows = rows - cz;
                //        parseInt
                //        for (var i = strartRow; i < parseInt(rows) + parseInt(strartRow); i++) {
                //            var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                //            var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
                //            if (Hvalue != undefined) {
                //                //jsAddDataH(Hvalue, i);
                //                values.push(i + "|" + Hvalue + "|" + hmbvalue);
                //            }
                //        }
                //    }
                var num = 0;
                for (var i = strartRow; i < rows; i++) {
                    var item = new Array();
                    var Hvalue = spread.getActiveSheet().getCell(i, colIndex).value(); //名称列
                    var hmbvalue = spread.getActiveSheet().getCell(i, 0).value(); //行属性
                    if (Hvalue != null) {
                        item.push(i);
                        item.push(Hvalue);
                        item.push(hmbvalue);
                        values.push(item);
                    }

                }

                //加载table数据
                $.ajax({
                    type: 'post',
                    data: {
                        flag: "H",
                        arr: JSON.stringify(values)
                    },
                    url: geturlpath() + 'ExcelData.ashx?action=AddDataHL',
                    async: true,
                    success: function(result) {
                        $('#rowdg').datagrid('loadData', JSON.parse(result)); //填充数据
                        //$('#rowdg').datagrid('loadData', result); //填充数据
                        zydm = zydm == "" ? loaddata('ZYDM', 'H') : zydm;
                        cpdm = cpdm == "" ? loaddata('CPDM', 'H') : cpdm;
                        jsdx = jsdx == "" ? loaddata('JSDX', 'H') : jsdx;
                        xmfl = xmfl == "" ? loaddata('XMFL', 'H') : xmfl;
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

            }

            window.row_ok = function() {
                if ($('#rowdg').datagrid('getData').total == 0) {
                    prompt("请先设置行属性！");
                    return;
                }
                $('#rowdg').datagrid('endEdit', rowacbs);
                rowacbs = undefined;
                var rows = $('#rowdg').datagrid('getData').rows;
                var sel = $('#selRowProp').combo('getValue');
                var sheet = spread.getActiveSheet();
                var sheetname = spread.getActiveSheet().name();
                var mcl = $('#mcl').textbox("getValue");
                sheet.suspendPaint(); //暂停绘制
                switch (sel) {
                case '1': //显示作业
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].ZYDM != "") {
                            val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                        }
                        if (rows[i].XMFL != "") {
                            val += val == ""
                                ? 'XMFL:' + rows[i].XMFL.split('.')[0]
                                : ',XMFL:' + rows[i].XMFL.split('.')[0]
                        }

                        sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                    }
                    break;
                case '2': //显示项目
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].CPDM != "") {
                            val += val == ""
                                ? 'CPDM:' + rows[i].CPDM.split('.')[0]
                                : ',CPDM:' + rows[i].CPDM.split('.')[0]
                        }
                        if (rows[i].XMFL != "") {
                            val += val == ""
                                ? 'XMFL:' + rows[i].XMFL.split('.')[0]
                                : ',XMFL:' + rows[i].XMFL.split('.')[0]
                        }
                        sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                    }
                    break;
                case '3': //显示作业+项目
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].ZYDM != "") {
                            val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                        }
                        if (rows[i].CPDM != "") {
                            val += val == ""
                                ? 'CPDM:' + rows[i].CPDM.split('.')[0]
                                : ',CPDM:' + rows[i].CPDM.split('.')[0]
                        }
                        if (rows[i].XMFL != "") {
                            val += val == ""
                                ? 'XMFL:' + rows[i].XMFL.split('.')[0]
                                : ',XMFL:' + rows[i].XMFL.split('.')[0]
                        }
                        sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                    }
                    break;
                case '4': //显示作业+计算对象
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].ZYDM != "") {
                            val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                        }
                        if (rows[i].JSDX != "") {
                            val += val == ""
                                ? 'JSDX:' + rows[i].JSDX.split('.')[0]
                                : ',JSDX:' + rows[i].JSDX.split('.')[0]
                        }
                        if (rows[i].XMFL != "") {
                            val += val == ""
                                ? 'XMFL:' + rows[i].XMFL.split('.')[0]
                                : ',XMFL:' + rows[i].XMFL.split('.')[0]
                        }
                        sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                    }
                    break;
                case '5': //显示作业+项目+计算对象
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].ZYDM != "") {
                            val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                        }
                        if (rows[i].CPDM != "") {
                            val += val == ""
                                ? 'CPDM:' + rows[i].CPDM.split('.')[0]
                                : ',CPDM:' + rows[i].CPDM.split('.')[0]
                        }
                        if (rows[i].JSDX != "") {
                            val += val == ""
                                ? 'JSDX:' + rows[i].JSDX.split('.')[0]
                                : ',JSDX:' + rows[i].JSDX.split('.')[0]
                        }
                        if (rows[i].XMFL != "") {
                            val += val == ""
                                ? 'XMFL:' + rows[i].XMFL.split('.')[0]
                                : ',XMFL:' + rows[i].XMFL.split('.')[0]
                        }

                        sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                    }
                    break;
                default:

                }
                sheet.resumePaint(); //重新绘制

                var mbdm = row.substring(row.indexOf("MBDM") + 5, row.indexOf("MBWJ") - 1);
                //保存横向类型和列名称
                $.ajax({
                    type: 'get',
                    url: geturlpath() +
                        'ExcelData.ashx?action=SaveHxsx&mbdm=' +
                        mbdm +
                        '&sheet=' +
                        sheetname +
                        '&hxlx=' +
                        sel +
                        '&lmc=' +
                        mcl +
                        '',
                    async: false,
                    success: function(result) {
                        if (result == "1") {
                            prompt("保存成功！");
                        }
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

            }

            //填充列数据
            window.TCLSJ = function() {
                //当前模板代码，名称
                //        var mbmc = row[3];
                //        var mbdm = row[1];
                var sheet = spread.getActiveSheet();
                var sheetname = spread.getActiveSheet().name();
                var strartCol = "", maxCol = "";
                var values = new Array();
                if ($('#hmc').textbox("getValue") == "") {
                    prompt("请输入列名称！！！");
                    return;
                }
                //初始化datagrid列
                $('#coldg').datagrid('showColumn', 'ZYDM');
                $('#coldg').datagrid('showColumn', 'XMDM');
                $('#coldg').datagrid('showColumn', 'JSDX');
                //获取起始最大列
                $.ajax({
                    type: 'post',
                    url: geturlpath() + 'ExcelData.ashx?action=getStartRowsOrCols',
                    data: {
                        flag: "STARTCOL",
                        mbdm: row
                    },
                    async: false,
                    success: function(result) {
                        strartCol = result.split('|')[0]; //spreadjs 行列索引都是从0开始
                        maxCol = result.split('|')[1];
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

                //列数
                var colsnum = parseInt(maxCol) - parseInt(strartCol);

                for (var i = strartCol, max = parseInt(maxCol) + 1; i < max; i++) {
                    //获取当前的索引的列头字母       
                    var Hvalue = sheet.getCell(parseInt($('#hmc').textbox("getValue")) - 1, parseInt(i - 1)).value();
                    var hmvalue = sheet.getCell(0, parseInt(i - 1)).value();
                    if (Hvalue != null && typeof Hvalue != 'object') {
                        var item = new Array();
                        item.push(i +
                            "|" +
                            spread.getActiveSheet().getText(0, parseInt(i - 1), spreadNS.SheetArea.colHeader));
                        item.push(Hvalue);
                        item.push(delPartChar(hmvalue));
                        values.push(item);
                    }
                }

                //加载table数据
                $.ajax({
                    type: 'post',
                    data: {
                        flag: "L",
                        arr: JSON.stringify(values)
                    },
                    url: geturlpath() + 'ExcelData.ashx?action=AddDataHL',
                    async: true,
                    success: function(result) {
                        $('#coldg').datagrid({ data: JSON.parse(result) }); //填充数据
                        zydm = zydm == "" ? loaddata('ZYDM', 'L') : zydm;
                        cpdm = cpdm == "" ? loaddata('XMDM', 'L') : cpdm;
                        sjdx = sjdx == "" ? loaddata('SJDX', 'L') : sjdx;
                        jsdx = jsdx == "" ? loaddata('JSDX', 'L') : jsdx;
                        jldw = jldw == "" ? loaddata('JLDW', 'L') : jldw;
                    },
                    error: function() {
                        alert("错误!");
                    }
                });


                //查询横向类型
                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=HXLX&' + row + '&sheet=' + sheetname + '',
                    async: false,
                    success: function(result) {
                        //显示项目
                        flag = result;
                        if (result == "1") {
                            $('#coldg').datagrid('hideColumn', 'ZYDM');
                        } else if (result == "2") {
                            $('#coldg').datagrid('hideColumn', 'XMDM');
                        } else if (result == "3") {
                            $('#coldg').datagrid('hideColumn', 'ZYDM');
                            $('#coldg').datagrid('hideColumn', 'XMDM');
                        } else if (result == "4") {
                            $('#coldg').datagrid('hideColumn', 'ZYDM');
                            $('#coldg').datagrid('hideColumn', 'JSDX');
                        } else if (result == "5") {
                            $('#coldg').datagrid('hideColumn', 'ZYDM');
                            $('#coldg').datagrid('hideColumn', 'JSDX');
                            $('#coldg').datagrid('hideColumn', 'XMDM');
                        }
                    },
                    error: function() {
                        alert("错误!");
                    }
                });


            }

            window.delPartChar = function(param) {
                var a = param == null ? param : param.replace("|", "");
                return a;
            }

            window.col_ok = function() {
                $('#coldg').datagrid('endEdit', colacbs);
                colacbs = undefined;
                var rows = $('#coldg').datagrid('getData').rows;
                var sheet = spread.getActiveSheet();
                var sheetname = spread.getActiveSheet().name();
                sheet.suspendPaint(); //暂停绘制
                switch (flag) {
                case '1': //显示作业
                    for (var i = 0; i < rows.length; i++) {
                        var val = "", lx = getJsdxLx(rows[i].JSDX.split('.')[0]);
                        if (rows[i].XMDM != "") {
                            val += 'XMDM:' + rows[i].XMDM.split('.')[0];
                        }
                        if (rows[i].SJDX != "") {
                            val += val == ""
                                ? 'SJDX:' + rows[i].SJDX.split('.')[0]
                                : ',SJDX:' + rows[i].SJDX.split('.')[0];;
                        }
                        if (rows[i].JSDX != "") {
                            if (lx == 6) {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                            } else {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0]
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0]
                            }
                        }
                        if (rows[i].JLDW != "") {
                            val += val == ""
                                ? 'JLDW:' + rows[i].JLDW.split('.')[0]
                                : ',JLDW:' + rows[i].JLDW.split('.')[0]
                        }
                        sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                    }
                    break;
                case '2': //显示项目
                    for (var i = 0; i < rows.length; i++) {
                        var val = "", lx = getJsdxLx(rows[i].JSDX.split('.')[0]);
                        if (rows[i].ZYDM != "") {
                            val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                        }
                        if (rows[i].SJDX != "") {
                            val += val == ""
                                ? 'SJDX:' + rows[i].SJDX.split('.')[0]
                                : ',SJDX:' + rows[i].SJDX.split('.')[0]
                        }
                        if (rows[i].JSDX != "") {
                            if (lx == 6) {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                            } else {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0]
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0]
                            }
                        }
                        if (rows[i].JLDW != "") {
                            val += val == ""
                                ? 'JLDW:' + rows[i].JLDW.split('.')[0]
                                : ',JLDW:' + rows[i].JLDW.split('.')[0]
                        }
                        sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                    }
                    break;
                case '3': //显示作业+项目
                    for (var i = 0; i < rows.length; i++) {
                        var val = "", lx = getJsdxLx(rows[i].JSDX.split('.')[0]);
                        if (rows[i].SJDX != "") {
                            val += 'SJDX:' + rows[i].SJDX.split('.')[0]
                        }
                        if (rows[i].JSDX != "") {
                            if (lx == 6) {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0] + '|'
                            } else {
                                val += val == ""
                                    ? 'JSDX:' + rows[i].JSDX.split('.')[0]
                                    : ',JSDX:' + rows[i].JSDX.split('.')[0]
                            }
                        }
                        if (rows[i].JLDW != "") {
                            val += val == ""
                                ? 'JLDW:' + rows[i].JLDW.split('.')[0]
                                : ',JLDW:' + rows[i].JLDW.split('.')[0]
                        }
                        sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                    }
                    break;
                case '4': //显示作业+计算对象
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].XMDM != "") {
                            val += 'XMDM:' + rows[i].XMDM.split('.')[0]
                        }
                        if (rows[i].SJDX != "") {
                            val += val == ""
                                ? 'SJDX:' + rows[i].SJDX.split('.')[0]
                                : ',SJDX:' + rows[i].SJDX.split('.')[0]
                        }
                        if (rows[i].JLDW != "") {
                            val += val == ""
                                ? 'JLDW:' + rows[i].JLDW.split('.')[0]
                                : ',JLDW:' + rows[i].JLDW.split('.')[0]
                        }
                        sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                    }
                    break;
                case '5': //显示作业+项目+计算对象
                    for (var i = 0; i < rows.length; i++) {
                        var val = "";
                        if (rows[i].SJDX != "") {
                            val += 'SJDX:' + rows[i].SJDX.split('.')[0]
                        }
                        if (rows[i].JLDW != "") {
                            val += val == ""
                                ? 'JLDW:' + rows[i].JLDW.split('.')[0]
                                : ',JLDW:' + rows[i].JLDW.split('.')[0]
                        }

                        sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                    }
                    break;
                default:
                    prompt("请先设置列属性！！！！");
                }
                sheet.resumePaint(); //重新绘制
            }


            //名称列选项发生变化时，触发的方法
            window.changeCol = function() {
                var val = $('#selRowProp').combo('getValue');

                switch (val) {
                case '1':
                    $('#rowdg').datagrid('hideColumn', 'CPDM');
                    $('#rowdg').datagrid('hideColumn', 'JSDX');
                    $('#rowdg').datagrid('showColumn', 'ZYDM');
                    break;
                case '2':
                    $('#rowdg').datagrid('hideColumn', 'ZYDM');
                    $('#rowdg').datagrid('hideColumn', 'JSDX');
                    $('#rowdg').datagrid('showColumn', 'CPDM');
                    break;
                case '3':
                    $('#rowdg').datagrid('hideColumn', 'JSDX');
                    $('#rowdg').datagrid('showColumn', 'CPDM');
                    $('#rowdg').datagrid('showColumn', 'ZYDM');
                    break;
                case '4':
                    $('#rowdg').datagrid('hideColumn', 'CPDM');
                    $('#rowdg').datagrid('showColumn', 'ZYDM');
                    $('#rowdg').datagrid('showColumn', 'JSDX');
                    break;
                case '5':
                    $('#rowdg').datagrid('showColumn', 'CPDM');
                    $('#rowdg').datagrid('showColumn', 'ZYDM');
                    $('#rowdg').datagrid('showColumn', 'JSDX');
                    break;
                default:

                }

            }

            window.getJsdxLx = function(zybm) {
                var res = "";
                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=GETJSDXLX&ZYBM=' + zybm + '',
                    async: false,
                    success: function(result) {
                        res = JSON.parse(result)
                    },
                    error: function() {
                        alert("错误!");
                    }
                })
                return res;

            }


            window.loaddata = function(field, flag) {
                var res = "";
                $.ajax({
                    type: 'get',
                    url: geturlpath() +
                        'ExcelData.ashx?action=getTreeData&field=' +
                        field +
                        '&flag=' +
                        flag +
                        '&parid=0',
                    async: false,
                    success: function(result) {
                        res = JSON.parse(result)
                    },
                    error: function() {
                        alert("错误!");
                    }
                })
                return res;
            }

            //添加校验公式
            window.Add = function() {
                var rows = $('#xygsdg').datagrid('getSelections');
                if (rows.length > 0) {
                    $('#xygsdg').datagrid('unselectAll'); //添加操作之前，先清空选中行
                }

                //打开公式设置界面
                $('#dlg').dialog('open');
                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=GetFormulaName',
                    async: true,
                    success: function(result) {
                        $("#selformula").empty();
                        $("#selformula").combobox("loadData", JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

                mark = 2;

                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=MaxBh',
                    async: true,
                    success: function(result) {
                        Bm = result; //获取最大编码
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            }

            //添加校验公式
            window.AddGS = function(GSvalue, GSJX) {
                if (GSvalue == "" || GSJX == "") {
                    alert("公式或公式解析不能为空！");
                    return;
                }
                var rows = $('#xygsdg').datagrid('getSelected');
                var len = rows == null ? 0 : rows.length;
                var idd = $('#xygsdg').datagrid('getRowIndex', rows);
                if (len == 0) {
                    Bm = parseInt(Bm) + 1;
                    $('#xygsdg').datagrid('insertRow',
                        {
                            row: {
                                MBDM: iframeid,
                                SHEETNAME: spread.getActiveSheet().name(),
                                GSBH: Bm,
                                GS: GSvalue,
                                INFOMATION: "",
                                GSJX: GSJX,
                                LX: "0"
                            }
                        });
                    if (addData == "") {
                        addData = [
                            {
                                "MBDM": iframeid,
                                "SHEETNAME": spread.getActiveSheet().name(),
                                "GSBH": Bm,
                                "GS": GSvalue,
                                "INFOMATION": "",
                                "GSJX": GSJX,
                                "LX": "0"
                            }
                        ];
                    } else {
                        addData.push({
                            "MBDM": iframeid,
                            "SHEETNAME": spread.getActiveSheet().name(),
                            "GSBH": Bm,
                            "GS": GSvalue,
                            "INFOMATION": "",
                            "GSJX": GSJX,
                            "LX": "0"
                        });
                    }

                }
                if (rows != null) {
                    $('#xygsdg').datagrid('beginEdit', idd);
                    var edGS = $('#xygsdg').datagrid('getEditor', { index: idd, field: 'GS' });
                    var edGSJX = $('#xygsdg').datagrid('getEditor', { index: idd, field: 'GSJX' });
                    //修改内容
                    edGS.target.val(GSvalue);
                    edGSJX.target.val(GSJX);
                    $('#xygsdg').datagrid('endEdit', idd);

                }

            }

            //保存校验公式
            window.EasyuiSave = function() {
                debugger;
                $('#xygsdg').datagrid('endEdit', gsacbs);
                gsacbs = undefined;
                var newAddData = "";
                var newUpData = "";
                for (i = 0; i < addData.length; i++) {
                    newAddData += addData[i].MBDM + "&";
                    newAddData += addData[i].SHEETNAME + "&";
                    newAddData += addData[i].GSBH + "&";
                    newAddData += addData[i].GS + "&";
                    newAddData += addData[i].GSJX + "&";
                    newAddData += addData[i].INFOMATION + "&";
                    newAddData += addData[i].LX;
                    if (i < addData.length - 1) {
                        newAddData += "^";
                    }
                }

                for (i = 0; i < upData.length; i++) {
                    newUpData += upData[i].ID + "&";
                    newUpData += upData[i].MBDM + "&";
                    newUpData += upData[i].SHEETNAME + "&";
                    newUpData += upData[i].GSBH + "&";
                    newUpData += upData[i].GS + "&";
                    newUpData += upData[i].GSJX + "&";
                    newUpData += upData[i].INFOMATION + "&";
                    if (upData[i].LX == "错误" || upData[i].LX == "0") {
                        newUpData += "0";
                    } else {
                        newUpData += "1";
                    }
                    if (i < upData.length - 1) {
                        newUpData += "^";
                    }
                }
                //var res = Excels_ExcelMb_ExcelMb.EasyuiSave(newAddData, newUpData).value;

                $.ajax({
                    type: 'post',
                    data: {
                        newAddData: newAddData,
                        newUpData: newUpData
                    },
                    url: geturlpath() + 'ExcelData.ashx?action=EasyuiSave',
                    async: false,
                    success: function(result) {
                        if (result == "1") {
                            prompt("保存成功！");
                        } else {
                            prompt("保存失败！");
                        }
                    },
                    error: function() {
                        alert("错误!");
                    }
                })

                addData = "";
                upData = "";
                editRows = "";
                //Bm = Excels_ExcelMb_ExcelMb.MaxBh().value;
                $.ajax({
                    type: 'get',
                    url: geturlpath() +
                        'ExcelData.ashx?action=getXygsList&' +
                        row +
                        '&sheet=' +
                        spread.getActiveSheet().name() +
                        '',
                    async: true,
                    success: function(result) {
                        result = result.replace(/\r/g, "").replace(/\n/g, "");
                        $('#xygsdg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                })

                mark = 1;
            }


            //修改校验公式
            window.Edit = function() {
                mark = 2;
                var editRows = $('#xygsdg').datagrid('getSelections');
                if (editRows.length != 1) {
                    $.messager.alert('修改提示', '请选择一行记录进行修改!!!!!!!!');
                    return;
                }
                var gs = editRows[0].GS;
                // $("#hidjygs").val(gs);

                //打开公式设置界面
                $('#dlg').dialog('open');
                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=GetFormulaName',
                    async: true,
                    success: function(result) {
                        $("#selformula").empty();
                        $("#selformula").combobox("loadData", JSON.parse(result));
                        $("#gs").val(gs); //修改处
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            }


            //确定结束编辑状态
            window.Ok = function() {
                $('#xygsdg').datagrid('endEdit', gsacbs);
            }
            //删除提示
            window.isDel = function() {
                $.messager.confirm('删除提示',
                    '确定要删除所选的数据?',
                    function(r) {
                        if (r) {
                            Del();
                        }
                    });
            }

            //删除校验公式
            window.Del = function() {
                var rows = $('#xygsdg').datagrid('getSelections');
                var newDelData = "";
                for (i = 0; i < rows.length; i++) {
                    newDelData += rows[i].MBDM + "|";
                    newDelData += rows[i].SHEETNAME + "|";
                    newDelData += rows[i].GSBH + "|";
                    newDelData += rows[i].GS + "|";
                    newDelData += rows[i].GSJX + "|";
                    newDelData += rows[i].INFOMATION;
                    if (i < rows.length - 1) {
                        newDelData += "^";
                    }
                }

                $.ajax({
                    type: 'post',
                    data: {
                        newDelData: newDelData
                    },
                    url: geturlpath() + 'ExcelData.ashx?action=DelData',
                    async: false,
                    success: function(result) {

                    },
                    error: function() {
                        alert("错误!");
                    }
                })

                $.ajax({
                    type: 'get',
                    url: geturlpath() +
                        'ExcelData.ashx?action=getXygsList&' +
                        row +
                        '&sheet=' +
                        spread.getActiveSheet().name() +
                        '',
                    async: true,
                    success: function(result) {
                        result = result.replace(/\r/g, "").replace(/\n/g, "");
                        $('#xygsdg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                })
            }


            window.Canel = function() {
                //删除刚添加的数据行（不是删除后台表里的数据）
                var rows = $('#xygsdg').datagrid('getSelections');
                if (rows.length == 1) {
                    var index = $('#xygsdg').datagrid('getRowIndex', rows[0]);
                    $('#xygsdg').datagrid('deleteRow', index);
                }
            }

            //加载列名称到combox
            window.loadCols = function() {
                var json = "[", colname = "";
                for (var i = 0, max = spread.getActiveSheet().getColumnCount(); i < max; i++) {
                    colname = spread.getActiveSheet().getText(0, i, spreadNS.SheetArea.colHeader);
                    json += "{" + "\"id\":" + "\"" + colname + "\"" + "," + "\"text\":" + "\"" + colname + "\"" + "}"
                    json += ","
                }
                json = json.substring(0, json.lastIndexOf(","));
                json += "]";
                $("#cols").combobox('loadData', JSON.parse(json));

            }

            //获取隐藏列设置的公式类型列的下拉选项
            window.getLx = function() {
                if (sellx == "") {
                    $.ajax({
                        type: 'get',
                        url: geturlpath() +
                            'ExcelData.ashx?action=getLx&' +
                            row +
                            '&sheet=' +
                            spread.getActiveSheet().name(),
                        async: false,
                        success: function(result) {
                            sellx = JSON.parse(result);
                            console.log(sellx);
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }
            }

            //获取隐藏列设置的公式列的下拉选项
            window.getGs = function() {
                if (sellxjson == "") {
                    $.ajax({
                        type: 'get',
                        url: geturlpath() + 'ExcelData.ashx?action=GetLxJson',
                        async: false,
                        success: function(result) {
                            sellxjson = JSON.parse(result);
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }
            }


            //开始设置新的隐藏列
            window.adNewRow = function() {
                //$('#showHideCol').datagrid('unselectAll');//清楚选中状态   
                //$('#setHideCol').datagrid('loadData', []);//清楚条件设置
                if ($('#setHideCol').datagrid('getData').rows.length != 0) {
                    okRow();
                } else {
                    prompt("请先添加隐藏条件");
                }
            }

            //删除隐藏列
            window.deRow = function() {
                var row = $('#showHideCol').datagrid('getSelected')
                if (row) {
                    $.ajax({
                        type: 'post',
                        url: geturlpath() + 'ExcelData.ashx?action=DelHideColSet',
                        data: {
                            row: JSON.stringify(row)
                        },
                        async: true,
                        success: function(result) {
                            if (result == "1") {
                                $('#setHideCol').datagrid('loadData', []);
                                prompt("删除成功！！！！！");
                                searchRow();
                            } else {
                                alert(result);
                            }
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                } else {
                    prompt("请选择一行！");
                }
            }

            //添加一行隐藏列条件
            window.addRow = function() {
                if ($("#cols").combobox('getValues').length != 0) {
                    $('#setHideCol').datagrid('appendRow',
                        {
                            gsid: '',
                            mbdm: iframeid,
                            sheet: spread.getActiveSheet().name(),
                            lh: '',
                            gslx: '',
                            lxtext: '',
                            gs: '',
                            gsjx: '',
                            condition: ''
                        });

                } else {
                    prompt("请先设置选择列！！！！！");
                }
            }

            //删除一行隐藏列条件
            window.delRow = function() {
                if ($('#setHideCol').datagrid('getSelected') != null) {
                    var row = $('#setHideCol').datagrid('getSelected');
                    var index = $('#setHideCol').datagrid('getRowIndex', $('#setHideCol').datagrid('getSelected'));
                    if (row.gsid != "") {
                        $.messager.confirm('提示：',
                            '确定要删除此项?',
                            function(r) {
                                if (r) {
                                    $.ajax({
                                        type: 'post',
                                        data: {
                                            row: JSON.stringify(row)
                                        },
                                        url: geturlpath() + 'ExcelData.ashx?action=DelYclTj',
                                        async: true,
                                        success: function(result) {
                                            if (result == "1") {
                                                $('#setHideCol').datagrid('deleteRow', index);
                                                prompt("删除成功！！！！！");
                                            } else {
                                                alert(result);
                                            }
                                        },
                                        error: function() {
                                            alert("错误!");
                                        }
                                    });
                                }
                            });
                    } else {
                        $('#setHideCol').datagrid('deleteRow', index);
                    }
                } else {
                    prompt("请选择一行");
                }
            }

            //添加的时候判断后台是否有重复隐藏列设置
            window.isRepeat = function() {
                var boo = "";
                $.ajax({
                    url: geturlpath() + 'ExcelData.ashx?action=isRepeat',
                    type: 'post',
                    data: {
                        cols: $("#cols").combobox('getText'),
                        mbdm: iframeid,
                        sheet: spread.getActiveSheet().name()
                    },
                    async: false,
                    success: function(result) {
                        boo = result
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

                return boo;
            }

            ////保存隐藏列条件设置
            window.okRow = function() {
                //         if(isRepeat()=="true"){
                //           prompt("有重复的列设置，请重新设置！");
                //           return;
                //         }
                var Rows = $('#showHideCol').datagrid('getSelected') == null
                    ? ""
                    : $('#showHideCol').datagrid('getSelected');
                var rows = $('#setHideCol').datagrid('getData').rows, gscon = "", gsjxcon = "";
                var arr = new Array();
                $('#setHideCol').datagrid('endEdit', hideacbs);
                hideacbs = undefined;
                for (var i = 0, max = rows.length; i < max; i++) {
                    if (rows[i].gslx == "" || rows[i].gs == "") {
                        prompt("【公式类型】或【公式】不能为空！！！");
                        return;
                    }
                }

                //arr=getRowText(rows);
                //obj={mbdm: iframeid,sheet: spread.getActiveSheet().name(),LH:$("#cols").combobox('getText'),gs: arr[0],  gsjx: arr[1]};

                //        $('#showHideCol').datagrid('insertRow',{
                //	        index: $('#showHideCol').datagrid('getSelected')==null?$('#showHideCol').datagrid('getData').rows.length:$('#showHideCol').datagrid('getRowIndex',$('#showHideCol').datagrid('getSelected')),// index start with 0
                //	        row: obj
                //            });

                //        var arr=new Array();
                //        for(var i=0,max=$('#setHideCol').datagrid('getData').rows.length;i<max;i++){
                //           arr.push($('#setHideCol').datagrid('getData').rows[i]);
                //        }


                //        $.ajax({
                //            url: geturlpath()+'ExcelData.ashx?action=saveRows',
                //            type: 'post',
                //            data:{
                //               cols:$("#cols").combobox('getText'),
                //               gstxt:JSON.stringify(arr[2]),
                //               row:JSON.stringify(obj),
                //               rows:JSON.stringify($('#setHideCol').datagrid('getData').rows)
                //            },
                //            async: true,
                //            success: function (result) {
                //                result=="1"?$('#setHideCol').datagrid('loadData',[]):console.log(result);
                //                searchRow();
                //            },
                //            error: function () {
                //                alert("错误!");
                //            }
                //        });

                $.ajax({
                    url: geturlpath() + 'ExcelData.ashx?action=saveRows',
                    type: 'post',
                    data: {
                        cols: $("#cols").combobox('getText'),
                        rows: JSON.stringify(rows), //设置隐藏列的条件
                        pro: Rows == "" ? Rows : JSON.stringify(Rows),
                        mbdm: iframeid,
                        sheet: spread.getActiveSheet().name(),
                        isyc: $("#ISYC").val()
                    },
                    async: true,
                    success: function(result) {
                        if (result == "1") {
                            $('#setHideCol').datagrid('loadData', []);
                            searchRow();
                        } else if (result == "true") {
                            prompt("有重复的列，请重新选择列！")
                        } else {
                            alert(result);
                        }
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            }

            //查询已设置好的隐藏列
            window.searchRow = function() {
                $.ajax({
                    url: geturlpath() + 'ExcelData.ashx?action=searchRow&mbdm=' + iframeid,
                    type: 'get',
                    async: true,
                    success: function(result) {
                        $('#showHideCol').datagrid('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

            }

            //双击行时，查询已设置的隐藏列的详细设置
            window.showSetRow = function(mbdm, sheetname, cols) {
                $.ajax({
                    url: geturlpath() + 'ExcelData.ashx?action=showSetRow',
                    type: 'post',
                    data: {
                        mbdm: mbdm,
                        sheetname: sheetname,
                        cols: cols
                    },
                    async: true,
                    success: function(result) {
                        console.log("searchRow已执行！！！！");
                        $('#setHideCol').datagrid('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

            }


        })(window);


        function attachToolbarItemEvents() {
            $("#addpicture, #doImport").click(function() {
                $("#fileSelector").data("action", this.id);
                $("#fileSelector").click();
            });

            $("#toggleInspector").click(toggleInspector);

            $("#doClear").click(function() {
                var $dropdown = $("#clearActionList"),
                    $this = $(this),
                    offset = $this.offset();

                $dropdown.css({ left: offset.left, top: offset.top + $this.outerHeight() });
                $dropdown.show();
                processEventListenerHandleClosePopup(true);
            });

            $("#doExport").click(function() {
                var $dropdown = $("#exportActionList"),
                    $this = $(this),
                    offset = $this.offset();

                $dropdown.css({ left: offset.left, top: offset.top + $this.outerHeight() });
                $dropdown.show();
                processEventListenerHandleClosePopup(true);
            });

            $("#doServerSave").click(function() {
                if (isRowOrColRepeat()) {
                    alert("同一个报表同一sheet页内行列属性均不允许重复！");
                    return;
                }
                if (isSheetRowRepeat()) {
                    alert("同一个报表不同sheet页之间行属性不允许重复！");
                    return;
                }
                if (!checkRowAndCol()) {
                    alert("行列属性不能同时包含计算对象！");
                    return;
                }
                saveJsonToServer("file",
                    row,
                    geturlpath() + 'ExcelData.ashx?action=SaveExcelorJsonOnServer',
                    "yes"); //保存json
            });

            $("#addslicer").click(processAddSlicer);

            /* *
            * 点击公式设置按钮
            * add by LGS
            * */
            $("#doSetUp").click(function() {
                debugger;
                showMask();
                var selcells = spread.getActiveSheet().getSelections();
                var formula = spread.getActiveSheet().getCell(selcells[0].row, selcells[0].col).formula();
                if (formula != null && formula.indexOf('N_') > -1) {
                    formula = formula.substring(2);
                }
                $('#dlg').dialog('open');
                $('#dlg').window('center'); //使Dialog居中显示
                $('#gs').val(formula);
                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=GetFormulaName',
                    async: true,
                    success: function(result) {
                        $("#selformula").empty();
                        $("#selformula").combobox("loadData", JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
            });

            $("#transFormula").click(function() {
                var selcells = spread.getActiveSheet().getSelections();
                var arr0 = new Array();
                var arr1 = new Array();
                var obj = {};
                if (!selcells || selcells.length === 0) {
                    alert("请先选择单元格！！！！");
                    return;
                }

                for (var i = 0, max = selcells.length; i < max; i++) {
                    var sel = selcells[i],
                        rowIndex = sel.row,
                        colIndex = sel.col,
                        rowCount = sel.rowCount,
                        colCount = sel.colCount,
                        maxRow = rowIndex + rowCount,
                        maxColumn = colIndex + colCount,
                        r,
                        c;

                    for (r = rowIndex; r < maxRow; r++) {
                        for (c = colIndex; c < maxColumn; c++) {
                            var formula = spread.getActiveSheet().getCell(r, c).formula();
                            var con = spread.getActiveSheet().getCell(r, c).text();
                            if (formula != null) {
                                arr1.push(formula);
                            } else if (formula == null && con != "") {
                                arr1.push(con);
                            }
                        }
                    }

                }
                if (arr1.length != 0) {
                    $.ajax({
                        type: 'get', //修改处
                        url: geturlpath() + 'ExcelData.ashx?action=Gsfy',
                        data: {
                            GS: JSON.stringify(arr1)
                        },
                        async: true,
                        success: function(result) {
                            $("#GSFY").window("open");
                            $("#fy").datagrid("loadData", []);
                            $("#fy").datagrid("loadData", JSON.parse(result));
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }

            });


            /* *
            * 点击行属性设置按钮
            * add by LGS
            * */
            $("#rowProperty").click(function() {
                showMask();
                var sheet = spread.getActiveSheet().name();
                $('#rowProp').dialog('open');
                $('#rowProp').window('center'); //使Dialog居中显示
                if ($('#selRowProp').combobox('getData') == "") { //判断下拉选项是否有值，获取横向类型下拉选项
                    $.ajax({
                        type: 'get',
                        url: geturlpath() + 'ExcelData.ashx?action=getHxlx',
                        async: false,
                        success: function(result) {
                            $('#selRowProp').combobox('loadData', JSON.parse(result));
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }

                $.ajax({ //查询横向类型
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=HXLX&' + row + '&sheet=' + sheet + '',
                    async: false,
                    success: function(result) {
                        $('#selRowProp').combobox("select", result);
                    },
                    error: function() {
                        alert("错误!");
                    }
                });

                if ($('#mcl').textbox("getValue") == "") {
                    $.ajax({ //查询名称列
                        type: 'get',
                        url: geturlpath() + 'ExcelData.ashx?action=getHLH&flag=LH&' + row + '&sheet=' + sheet + '',
                        async: true,
                        success: function(result) {
                            $('#mcl').textbox("setValue", result);
                        },
                        error: function() {
                            alert("错误!");
                        }
                    });
                }
                $('#rowdg').datagrid('loadData', []);
            });

            /* *
            * 点击列属性设置按钮
            * add by LGS
            * */
            $("#colProperty").click(function() {
                showMask();
                var sheet = spread.getActiveSheet().name();
                $('#colProp').dialog('open');
                $('#colProp').window('center'); //使Dialog居中显示
                $.ajax({ //查询行名称
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=getHLH&flag=HH&' + row + '&sheet=' + sheet + '',
                    async: true,
                    success: function(result) {
                        $('#hmc').textbox("setValue", result);
                    },
                    error: function() {
                        alert("错误!");
                    }
                });
                $('#coldg').datagrid('loadData', []);
            });

            /* *
            * 点击效验公式设置按钮
            * add by LGS
            * */
            $("#checkFormula").click(function() {
                showMask();
                $("#XYGS").window("open");
                var SHEET = spread.getActiveSheet().name();

                $.ajax({
                    type: 'get',
                    url: geturlpath() + 'ExcelData.ashx?action=getXygsList&' + row + '&sheet=' + SHEET + '',
                    async: true,
                    success: function(result) {
                        result = result.replace(/\r/g, "").replace(/\n/g, "");
                        $('#xygsdg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', JSON.parse(result));
                    },
                    error: function() {
                        alert("错误!");
                    }
                })
            })


            /* *
            * 打开列隐藏设置
            *
            * */
            $("#colHide").click(function() {
                showMask();
                $("#YCL").window("open");

                //加载当前已打开模板的设置好的隐藏列
                searchRow();
            });
            //编辑区域
            $("#editArea").click(function() {
                var isLocked = false;
                var sheet = spread.getActiveSheet();
                var selections = sheet.getSelections();
                var selectionsLength = selections.length;
                var cell = selections.cells;
                //spread.setselectcolor();
                //spread.getActiveSheet().Application.selection.Cells.Locked = "false";
                //spread.addcustpro(spread.activesheet(), "locked", "true");
            });
            //设置备注
            $("#setNotes").click(function() {
                $("#DivBZ").window('open');
            });
            //自动生成公式
            $("#autoGenerateFormula").click(function() {
                var sheet = spread.getActiveSheet();
                var selections = sheet.getSelections();
                var startRowIndex = sheet.getActiveRowIndex(); //起始行数
                var startColumnIndex = sheet.getActiveColumnIndex(); //起始列数
                var endRowIndex = selections[0].rowCount + startRowIndex - 1; //终止行数
                var endColIndex = selections[0].colCount + startColumnIndex - 1; //终止列数
                var resjson = "";
                var addr = "";
                //获取首列的行列属性
                for (var i = startColumnIndex; i <= endColIndex; i++) {
                    for (var j = startRowIndex; j <= endRowIndex; j++) {
                        var rowcolval = "";
                        //var H = po.activesheet().Range("A" + j + "").value;
                        var H = sheet.getCell(j, 0).value();
                        //var L = po.activesheet().Range(num2col(i) + "1").value;
                        var L = sheet.getCell(0, i).value();
                        if (H != undefined & L != undefined) {
                            if (H)
                                rowcolval = H + ",";
                            if (L)
                                rowcolval += L + ",";
                            //var A = "addr:" + num2col(i) + j; "addr":"T6"
                            var A = "addr:" + "" + j + "&" + i + "";
                            rowcolval += A;
                            //var rowcolval = "" + po.activesheet().Range("A" + j + "").value + "," + po.activesheet().Range(num2col(i) + "1").value + "," + "addr:" + num2col(i) + j;
                            rowcolval = rowcolval.replace(/:/g, "\":\"");
                            rowcolval = rowcolval.replace(/,/g, "\",\"");
                            rowcolval = rowcolval + "\"";
                            resjson += "{\"" + rowcolval + "\}";
                            resjson += ",";
                        }
                    }
                }
                resjson = resjson.substring(0, resjson.length - 1);
                resjson = resjson.replace(/\r/g, "").replace(/\n/g, "");
                var value = eval("([" + resjson + "])");
                var ZYDM = "";
                var CPDM = "";
                var XMFL = "";
                var SJDX = "";
                var JSDX = "";
                var JLDW = "";
                var addr = "";
                var mbdm = parent.$("#HidMBDM").val();
                for (var i = 0; i < value.length; i++) {
                    ZYDM = value[i]["ZYDM"];
                    if (ZYDM == undefined)
                        ZYDM = "";
                    CPDM = value[i]["CPDM"];
                    if (CPDM == undefined)
                        CPDM = "";
                    XMFL = value[i]["XMFL"];
                    if (XMFL == undefined)
                        XMFL = "";
                    SJDX = value[i]["SJDX"];
                    if (SJDX == undefined)
                        SJDX = "";
                    JSDX = value[i]["JSDX"];
                    if (JSDX == undefined)
                        JSDX = "";
                    JLDW = value[i]["JLDW"];
                    if (JLDW == undefined)
                        JLDW = "";
                    addr = value[i]["addr"];

                    var gs = "=n_YSXMGS(" +
                        "\"" +
                        mbdm +
                        "\"" +
                        "," +
                        "\"" +
                        XMFL +
                        "\"" +
                        "," +
                        "\"" +
                        CPDM +
                        "\"" +
                        "," +
                        "\"" +
                        ZYDM +
                        "\"" +
                        "," +
                        "\"" +
                        JSDX +
                        "\"" +
                        "," +
                        "\"" +
                        SJDX +
                        "\"" +
                        ")";
                    gs = gs.replace("|", "");
                    sheet.getCell(parseInt(addr.split('&')[0]), parseInt(addr.split('&')[1])).formula(gs);

                }

            });
            //全区域计算
            $("#fullAreaCalculateColumn").click(function() {
                showMask();
                $("#WinQQYJSL").window('open');
                var mbdm = parent.$("#HidMBDM").val();
                var sheet = spread.getActiveSheet().name();
                var path = geturlpath();
                $.ajax({
                    type: "POST",
                    url: path + 'ExcelData.ashx?action=getQQYJSL',
                    data: {
                        mbdm: mbdm,
                        sheet: sheet
                    },
                    success: function(result) {
                        var rtn = result;
                        if (rtn != "" && rtn != null) {
                            var list = rtn.split('|');
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].split('&')[0] == "3") {
                                    $("#txtBZ").val(list[i].split('&')[1]);
                                } else if (list[i].split('&')[0] == "4") {
                                    $("#txtPF").val(list[i].split('&')[1]);
                                } else if (list[i].split('&')[0] == "5") {
                                    $("#txtFJ").val(list[i].split('&')[1]);
                                }
                            }
                        } else {
                            $("#txtBZ").val("");
                            $("#txtPF").val("");
                            $("#txtFJ").val("");
                        }
                    },
                    error: function(e) {
                        alert(e);
                    }
                });
            })
        }

        //全区域计算列设置表
        function saveQQYJSL() {
            var bz = $("#txtBZ").val();
            var pf = $("#txtPF").val();
            var fj = $("#txtFJ").val();
            var mbdm = parent.$("#HidMBDM").val();
            var sheet = spread.getActiveSheet().name();
            var arrLx = new Array();
            var arrCol = new Array();
            var path = geturlpath();
            if (bz.replace(/(\s*$)/g, "") !== "") {
                arrLx.push("3");
                arrCol.push(bz);
            }
            if (pf.replace(/(\s*$)/g, "") !== "") {
                arrLx.push("4");
                arrCol.push(pf);
            }
            if (fj.replace(/(\s*$)/g, "") !== "") {
                arrLx.push("5");
                arrCol.push(fj);
            }
            $.ajax({
                type: "POST",
                url: path + 'ExcelData.ashx?action=saveQQYJSL',
                data: {
                    mbdm: mbdm,
                    sheet: sheet,
                    arrLx: JSON.stringify(arrLx),
                    arrCol: JSON.stringify(arrCol)
                },
                traditional: true,
                success: function(result) {
                    var rtn = result;
                    if (rtn > 0) {
                        alert("保存成功");
                        tcQQYJSL();
                    } else {
                        alert("保存失败");
                    }
                },
                error: function(e) {
                    alert(e);
                }
            });

        }

        //退出条件选择
        function tcQQYJSL() {
            $("#WinQQYJSL").window('close');
            $("#txtBZ").val("");
            $("#txtPF").val("");
            $("#txtFJ").val("");
        }

        //选择设置隐藏的行列
        var LHOBJ;

        function XZL(OBJ) {
            LHOBJ = OBJ;
            $("#WinYCLLHXZ").window('open');
            jsonTree();
        }

        //修改隐藏列前的列名
        var YSLH = "", YSYC = "";

        function jsonTree() {
            $('#tt').tree({
                lines: true,
                checkbox: true,
                cascadeCheck: false
            });
            var columnS = spread.getActiveSheet().getColumnCount();
            var rnt = "[";
            for (var i = 1; i < columnS + 1; i++) {
                rnt += "{\"id\":\"" + num2col(i) + "\",\"text\":\"" + num2col(i) + "\"}";
                if (i < columnS) {
                    rnt += ",";
                }
            }
            rnt += "]";
            var Data = eval("(" + rnt + ")");
            $("#tt").tree("loadData", Data);
            var mbdm = parent.$("#HidMBDM").val();
            var sheet = spread.getActiveSheet().name();
            var rtns = "";
            var path = geturlpath();
            switch (LHOBJ.id) {
            case "txtYcl":
                var YC = YSYC;
                var COL = YSLH;
                if (YC == "") {
                    if ($("#divYCSZ td[name^='tdISYC']").length > 1) {
                        YC = $("#divYCSZ td[name^='tdISYC']")[1].innerHTML;
                    } else {
                        YC = $("#ISYC").val();
                    }
                }
                if (COL == "") {
                    if ($("#divYCSZ td[name^='tdCOLS']").length > 1) {
                        COL = $("#divYCSZ td[name^='tdCOLS']")[1].innerHTML;
                    } else {
                        COL = $("#txtYcl").val();
                    }
                }
                $.ajax({
                    type: "POST",
                    url: path + 'ExcelData.ashx?action=getlh',
                    data: {
                        mbdm: mbdm,
                        sheet: sheet,
                        col: COL,
                        yc: YC
                    },
                    success: function(result) {
                        rtns = result;
                        if (rtns != "") {
                            var CD = rtns.split(',');
                            for (var i = 0; i < CD.length; i++) {
                                var node = $('#tt').tree("find", CD[i].toString());
                                if (node != null) {
                                    $("#tt").tree("check", node.target);
                                }
                            }
                        }
                    },
                    error: function(e) {
                        alert(e);
                    }
                });
                break;
            case "txtBZ":
                $.ajax({
                    type: "POST",
                    url: path + 'ExcelData.ashx?action=getlhqqy',
                    data: {
                        mbdm: mbdm,
                        sheet: sheet,
                        mblx: '3'
                    },
                    success: function(result) {
                        rtns = result;
                        if (rtns != "") {
                            var CD = rtns.split(',');
                            for (var i = 0; i < CD.length; i++) {
                                var node = $('#tt').tree("find", CD[i].toString());
                                if (node != null) {
                                    $("#tt").tree("check", node.target);
                                }
                            }
                        }
                    },
                    error: function(e) {
                        alert(e);
                    }
                });
                break;
            case "txtPF":
                $.ajax({
                    type: "POST",
                    url: path + 'ExcelData.ashx?action=getlhqqy',
                    data: {
                        mbdm: mbdm,
                        sheet: sheet,
                        mblx: '4'
                    },
                    success: function(result) {
                        rtns = result;
                        if (rtns != "") {
                            var CD = rtns.split(',');
                            for (var i = 0; i < CD.length; i++) {
                                var node = $('#tt').tree("find", CD[i].toString());
                                if (node != null) {
                                    $("#tt").tree("check", node.target);
                                }
                            }
                        }
                    },
                    error: function(e) {
                        alert(e);
                    }
                });
                break;
            case "txtFJ":
                $.ajax({
                    type: "POST",
                    url: path + 'ExcelData.ashx?action=getlhqqy',
                    data: {
                        mbdm: mbdm,
                        sheet: sheet,
                        mblx: '5'
                    },
                    success: function(result) {
                        rtns = result;
                        if (rtns != "") {
                            var CD = rtns.split(',');
                            for (var i = 0; i < CD.length; i++) {
                                var node = $('#tt').tree("find", CD[i].toString());
                                if (node != null) {
                                    $("#tt").tree("check", node.target);
                                }
                            }
                        }
                    },
                    error: function(e) {
                        alert(e);
                    }
                });
                break;
            }
        }

        function getChecked() {
            var nodes = $('#tt').tree('getChecked');
            var s = "";
            for (var i = 0; i < nodes.length; i++) {
                s += nodes[i].id.replace(/(\s*$)/g, "");
                if (i < nodes.length - 1) {
                    s += ",";
                }
            }
            var id = LHOBJ.id;
            $("#" + id + "").val(s);
            $("#WinYCLLHXZ").window('close');
        }

        function tc() {
            $("#WinYCLLHXZ").window('close');
            $("#txtYcl").val("");
        }

        /**
        * 将数字索引转换为英文字母
        * @param {Number} colIndex
        */

        function num2col(colIndex) {
            //    colIndex += 1;
            if (colIndex < 1) {
                return "";
            }
            var str = "";
            var result = "";
            var A = 'A';
            while (colIndex != 0) {
                var num = colIndex % 26; // 取余
                var c = A.charCodeAt(0) + num - 1;
                colIndex = Math.floor(colIndex / 26); //返回值小于等于其数值参数的最大整数值。
                // 对于26的特殊处理
                if (num == 0) {
                    //c = A.charCodeAt(0) + 26;
                    str = 'Z';
                    colIndex -= 1; //退位
                } else {
                    str = String.fromCharCode(c);
                }
                // 3.插入
                result += str;
            }
            if (result.length > 1) {
                result = result.split('').reverse().join("");
            }
            return result;
        };


        //关闭设置备注页面
        function qx() {
            $("#DivBZ").window('close');
        }

        //打开备注
        function getBZ() {
            //_sheet.Hyperlinks.Add(_sheet.Cells(_row, _col), _url, '', _text, _text);
            var xsz = $("#txt_xsz").val();
            //    var mc = Excels_ExcelMb_ExcelMb.getExcelBzMc().value;
            //    Excels_ExcelMb_ExcelMb.addExcelBzMc(mc);
            //    var mbwj = mc + ".xls";
            //    var url = "pageoffice: //| " + geturlpath() + "ExcelBZ.aspx?mbwj=" + encodeURIComponent(mbwj) + "";
            //            window.location.href = url + "|||";
            //    po.application().ActiveSheet.Hyperlinks.Add(po.application().ActiveCell, url, '', xsz, xsz);
            var sheet = spread.getActiveSheet();
            var h1 = new spreadNS.CellTypes.HyperLink();
            h1.text(xsz);
            sheet.setCellType(1, 2, h1, spreadNS.SheetArea.viewport);
            sheet.getCell(1, 2, spreadNS.SheetArea.viewport).value("http://www.grapecity.com/en/spreadjs/")
                .hAlign(spreadNS.HorizontalAlign.left);
            sheet.setValue(1, 1, "basic hyperlink:");
            $("#DivBZ").window('close');
        }


        /**
        * Created by LI on 2017-05-17 .
        * 请求服务器端json数据，在页面展示
        */
        function openJson(selected, IsExist) {
            var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
            var excelIo = new GC.Spread.Excel.IO();
            var path = window.document.location.pathname.substring(1);
            var Len = path.indexOf("/");
            var root = "/" + path.substring(0, Len + 1);
            root = root + "Excels/ExcelMb/";

            var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected + "&rand=" + Math.random();
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'text';

            xhr.onload = function(e) {
                if (this.status == 200) {
                    //设置自定义公式
                    MySelfMethod();
                    // get binary data as a response
                    importJson(JSON.parse(this.response));
                    $("#toggleInspector").click();
                }
            };

            xhr.send();

        }


        /**
        * Created by LI on 2017-05-17 .
        * 请求服务器端excel在页面展示
        */
        function openexcel(selected, IsExist) {
            var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
            var excelIo = new GC.Spread.Excel.IO();
            // Download Excel file
            var path = window.document.location.pathname.substring(1);
            var Len = path.indexOf("/");
            var root = "/" + path.substring(0, Len + 1);
            root = root + "Excels/ExcelMb/";
            var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
                if (this.status == 200) {
                    //MySelfMethod();
                    // get binary data as a response
                    var blob = this.response;
                    // convert Excel to JSON
                    excelIo.open(blob,
                        function(json) {
                            json.tabStripVisible = true; //加上这句，spreadjs 的excel会显示sheet页。
                            //设置自定义公式
                            MySelfMethod();

                            importJson(json);
                            $("#toggleInspector").click();
                            //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
                            //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
                            //savetoS();
                            saveJsonToServer("file", row, root + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');

                        },
                        function(e) {
                            // process error
                            alert(e.errorMessage);
                        },
                        {});
                }
            };

            xhr.send();
        }

        // save json to server      add by LGS
        function saveJsonToServer(name, mbdm, url, property) {
            var json = spread.toJSON({ includeBindingSource: true });
            text = JSON.stringify(json);
            var fd = new FormData(document.forms.namedItem("exform"));
            fd.append("flag", "json");
            fd.append(name, new Blob([text], { type: "text/plain;charset=utf-8" }));
            //模板设置参数
            fd.append("mbdm", mbdm);
            //模板数据参数
            fd.append("FileName", mbdm);
            //是否保存行列属性
            fd.append("Property", property);

            $.ajax({
                url: url,
                type: "POST",
                contentType: false,
                processData: false,
                data: fd,
                async: true,
                cache: false,
                success: function(data) {
                    prompt(data);
                },
                error: function(ex) {
                    alert(ex);
                }
            });

        }

        function openJsonOrExcel(row) {
            var path = geturlpath();
            $.ajax({
                type: "POST",
                url: encodeURI(path + 'ExcelData.ashx?action=GetUrlPath&' + row),
                success: function(result) {
                    debugger;
                    if (result == '1') {
                        openJson(row);
                    } else {
                        openexcel(row);
                    }
                },
                error: function(e) {
                    alert(e);
                }
            });
        }

        function checkRowAndCol() {
            var sheet = spread.getActiveSheet();
            var rowCount = sheet.getRowCount();
            var columnCount = sheet.getColumnCount();
            for (var i = 0; i < rowCount; i++) {
                var rowCell = sheet.getCell(i, 0).value();
                if (rowCell != null && rowCell.indexOf("JSDX") > -1) {
                    for (var j = 0; j < columnCount; j++) {
                        var colCell = sheet.getCell(0, j).value();
                        if (colCell != null && colCell.indexOf("JSDX") > -1) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        function isRowOrColRepeat() {
            var sheet = spread.getActiveSheet();
            var rowCount = sheet.getRowCount();
            var columnCount = sheet.getColumnCount();
            var array = new Array();
            for (let i = 0; i < rowCount; i++) {
                var rowCell = sheet.getText(i, 0);
                if (rowCell.length > 0) {
                    array.push(rowCell);
                }
            }
            for (let i = 0; i < columnCount; i++) {
                var colCell = sheet.getText(0, i);
                if (colCell.length > 0) {
                    array.push(colCell);
                }
            }
            var newArray = array.sort();
            for (let i = 0; i < newArray.length; i++) {
                if (newArray[i] == newArray[i + 1]) {
                    return true;
                }
            }
            return false;
        }

        function isSheetRowRepeat() {
            var array = new Array();
            for (var i = 0; i < spread.getSheetCount(); i++) {
                var sheet = spread.getSheet(i);
                var rowCount = sheet.getRowCount();
                for (var j = 0; j < rowCount; j++) {
                    var rowCell = sheet.getText(j, 0);
                    if (rowCell.length > 0) {
                        array.push(rowCell);
                    }
                }
            }
            var newArray = array.sort();
            for (var i = 0; i < newArray.length; i++) {
                if (newArray[i] == newArray[i + 1]) {
                    return true;
                }
            }
            return false;
        }


        $(function() {
            openJsonOrExcel(row);
            $("#BtnDeleteSheet").click(function(){
                var SheetName=$("#CheckedSheetName").html();
                $.messager.confirm('提示：', '您确定要删除当前选中的'+SheetName+'页吗？', function (r) {
                    if (r) 
                    {
                        if(SheetName!="" && SheetName!=null && SheetName!=undefined)
                        {
                            if(spread.getSheetCount()<=1)
                            {
                                $.messager.alert("提示框","Excel文件中至少含有一个sheet页！");
                                return;
                            }
                            else
                            {
                                spread.suspendPaint();
                                var sheetIndex=spread.getSheetIndex(SheetName);
                                spread.removeSheet(sheetIndex);
                                spread.resumePaint();
                                $("#CheckedSheetName").html("");
                                SetSheetVisable();
                            }
                        }
                        else
                        {
                            $.messager.alert("提示框","请选择要删除的sheet页！");
                            return;
                        }
                    }
                });
            });
        });
        
    </script>
    <style>
        #hidebg {
            position: absolute;
            left: 0px;
            top: 0px;
            background-color: #000;
            width: 100%; /*宽度设置为100%，这样才能使隐藏背景层覆盖原页面*/
            filter: alpha(opacity=10); /*设置透明度为10%*/
            opacity: 0.1; /*非IE浏览器下设置透明度为10%*/
            display: none;
            z-index: 2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- 模板设置菜单显示 begin-->
<div id="EXCELMBSZ">
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doImport"
            title="@toolBar.importFile@">
        <span class="fa fa-folder-open fa-lg"></span></br> <span>本地打开</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doExport"
            title="@toolBar.export.title@">
        <span class="fa icon-DC fa-lg"></span></br> <span>本地保存</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doClear"
            title="@toolBar.clear.title@">
        <span class="fa fa-eraser fa-lg"></span></br> <span>清除</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doServerSave"
            title="@toolBar.serverSave@">
        <span class="fa fa-cloud-upload-o fa-lg"></span></br> <span>服务器端保存</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doSetUp"
            title="@toolBar.formulaSetup@">
        <span class="fa icon-GSSZ fa-lg"></span></br> <span>公式设置</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="transFormula"
            title="@toolBar.transFormula@">
        <span class="fa icon-GSFY fa-lg"></span></br> <span>公式翻译</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="rowProperty"
            title="@toolBar.rowProperty@">
        <span class="fa fa-align-justify fa-lg"></span></br> <span>行属性设置</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="colProperty"
            title="@toolBar.colProperty@">
        <span class="fa fa-columns fa-lg"></span></br> <span>列属性设置</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="checkFormula"
            title="@toolBar.checkFormula@">
        <span class="fa icon-JYGS fa-lg"></span></br> <span>校验公式</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="colHide"
            title="@toolBar.colHide@">
        <span class="fa icon-YC fa-lg"></span></br> <span>隐藏列设置</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="autoGenerateFormula"
            title="自动生成公式">
        <!--   <span class="fa icon-YC fa-2x"></span>-->
        <span class="fa fa-code fa-lg"></span></br> <span>自动生成公式</span>
    </button>
    <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="fullAreaCalculateColumn"
            title="全区域计算列设置表">
        <span class="fa icon-QQYJS fa-lg"></span></br> <span>全区域计算列设置表 </span>
    </button>
    <div id="DivBZ" data-options="left:'200px',top:'0px'" class="easyui-window" title="设置备注"
         closed="true" style="width: 350px; height: 220px; padding: 20px; text-align: center; padding-top: 30px">
        <div style="overflow: auto; width: 350px" id="div2">
            <table>
                <tr>
                    <td>
                        显示值：
                    </td>
                    <td>
                        <input id="txt_xsz" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="Button1" type="button" value="确定" class="button5" onclick="getBZ()"/>
                    </td>
                    <td>
                        <input id="Button3" type="button" value="取消" class="button5" onclick="qx()"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="WinQQYJSL" data-options="left:'200px',top:'0px',onClose:hideMask" class="easyui-window"
         title="全区域计算列设置表" collapsible="false" minimizable="false" closable="true" closed="true"
         style="width: 500px; height: 200px; margin-top: 1px; padding: 1px;">
        <div style="text-align: center">
            <a href="#" class="easyui-linkbutton" iconcls="icon-save" onclick="saveQQYJSL()">保存</a>
            <a href="#" class="easyui-linkbutton" iconcls="icon-cancel" onclick="tcQQYJSL()">退出</a>
        </div>
        <div id="div3">
            <table style="margin: auto">
                <tr>
                    <td>
                        编制计算列
                    </td>
                    <td>
                        <input type="text" id="txtBZ" ondblclick="XZL(this)" readonly="readonly" style="width: 200px;"/>
                    </td>
                    <td>
                        （请双击文本框选择列）
                    </td>
                </tr>
                <tr>
                    <td>
                        批复计算列
                    </td>
                    <td>
                        <input type="text" id="txtPF" ondblclick="XZL(this)" readonly="readonly" style="width: 200px;"/>
                    </td>
                    <td>
                        （请双击文本框选择列）
                    </td>
                </tr>
                <tr>
                    <td>
                        分解计算列
                    </td>
                    <td>
                        <input type="text" id="txtFJ" ondblclick="XZL(this)" readonly="readonly" style="width: 200px;"/>
                    </td>
                    <td>
                        （请双击文本框选择列）
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--全面计算列设置树加载-->
    <div id="WinYCLLHXZ" data-options="left:'200px',top:'0px'" class="easyui-window"
         title="列设置" collapsible="false" minimizable="false" closable="false" closed="true"
         style="overflow: hidden; width: 200px; height: 500px; text-align: center; padding-top: 1px; margin-top: 1px">
        <div style="text-align: left; height: 430px; overflow: auto">
            <ul checkbox:true id="tt" class="easyui-tree"/>
        </div>
        <div style="height: 40px; width: 100%">
            <table>
                <tr>
                    <td style="text-align: center">
                        <input id="Button8" type="button" value="确定" class="button5" onclick="getChecked()"/>
                    </td>
                    <td style="text-align: center">
                        <input id="Button9" type="button" value="取消" onclick="tc()" class="button5"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<!-- 模板设置菜单显示 end -->
<!--easyui 设置界面 begin-->
<div id="dlg" class="easyui-dialog" title="公式设置" data-options="iconCls:'icon-save',closed:true,modal:false,onBeforeClose:function(){QK()},onClose:hideMask"
     style="width: 450px; height: 100%">
    选择公式：
    <input id="selformula" class="easyui-combobox" name="dept" data-options="valueField:'id',textField:'text'"/>
    <div class="group-item-divider">
    </div>
    <div id="gsTitle">
        <fieldset style="width: 99%" class="customFieldset">
            <legend class="customLegend">公式设置：</legend>
            <div id="ggsz">
            </div>
        </fieldset>
    </div>
    <div class="group-item-divider">
    </div>
    <div>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width: 80px" onclick="addgs()">
            添加公式
        </a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width: 80px"
           onclick="gsjx()">
            公式解析
        </a>
    </div>
    <div class="group-item-divider">
    </div>
    <fieldset style="width: 99%; height: 25%" class="customFieldset">
        <legend class="customLegend">公式：</legend>
        <textarea id="gs" style="width: 100%; height: 90%; resize: none; border: none"></textarea>
    </fieldset>
    <div class="group-item-divider">
    </div>
    <fieldset style="width: 99%; height: 25%" class="customFieldset">
        <legend class="customLegend">公式解析：</legend>
        <textarea id="con" style="width: 100%; height: 90%; resize: none; border: none"></textarea>
    </fieldset>
    <div class="group-item-divider">
    </div>
    <div style="">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width: 80px" onclick="ok()">
            确定
        </a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width: 80px"
           onclick="cancel()">
            取消
        </a>
    </div>
</div>
<div id="rowProp" class="easyui-dialog" title="行属性设置" data-options="closed:true,modal:false,collapsible:true,maximizable:true,onClose:hideMask"
     style="width: 800px; height: 400px">
    <div id="rowlayout" class="easyui-layout" style="" data-options="fit:true">
        <div data-options="region:'north',split:true" style="height: 36px">
            <div>
                <div style="float: left">
                    <label>
                        名称列：
                    </label><input class="easyui-textbox" id="mcl"/>
                    <input id="selRowProp" class="easyui-combobox" data-options="panelHeight:'auto',valueField:'id',textField:'text',onChange:function (n,o){changeCol()}"/>
                </div>
                <div style="float: left">
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
                       style="width: 80px" onclick="TCHSJ()">
                        确定
                    </a>
                    <a href="javascript:void(0)" class="easyui-linkbutton"
                       data-options="iconCls:'icon-save'" style="width: 80px" onclick="row_ok()">
                        保存
                    </a>
                </div>
            </div>
        </div>
        <div data-options="region:'center',split:true" style="height: 100%">
            <table id="rowdg" class="easyui-datagrid">
            </table>
        </div>
    </div>
</div>
<div id="colProp" class="easyui-dialog" title="列属性设置" data-options="closed:true,modal:false,collapsible:true,maximizable:true,onClose:hideMask"
     style="width: 800px; height: 400px">
    <div id="collayout" class="easyui-layout" style="" data-options="fit:true,split:true">
        <div data-options="region:'north',title:'',split:true" style="height: 36px;">
            <div>
                <div style="float: left">
                    <label>
                        行号：
                    </label><input id="hmc" class="easyui-textbox"/>
                </div>
                <div style="float: left">
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
                       style="width: 80px" onclick="TCLSJ()">
                        确定
                    </a>
                    <a href="javascript:void(0)" class="easyui-linkbutton"
                       data-options="iconCls:'icon-save'" style="width: 80px" onclick="col_ok()">
                        保存
                    </a>
                </div>
            </div>
        </div>
        <div data-options="region:'center'" style="height: 100%">
            <table id="coldg" class="easyui-datagrid" data-options="fitColumns:true,singleSelect:true,fit:true">
            </table>
        </div>
    </div>
</div>
<div id="XYGS" class="easyui-dialog" title="校验公式" closed="true" style="width: 800px; height: 400px; text-align: center;" data-options="iconCls: 'icon-save',resizable:true,minimizable:true,maximizable:true,buttons: [{text:'增加',iconCls:'icon-ok',handler: function(){ Add()}},{text:'修改公式',iconCls:'icon-ok',handler: function(){ Edit()}},{text:'取消',iconCls:'icon-cancel',handler: function(){ Canel()}},{text:'删除',iconCls:'icon-remove',handler: function(){ isDel()}},{text:'保存',iconCls:'icon-save',handler: function(){ EasyuiSave()}}],onClose:hideMask">
    <table id="xygsdg" class="easyui-datagrid" style="width: 700px; height: 250px" data-options="fitColumns:true,singleSelect:true,fit:true,pagination:true,pageSize :5,pageList : [ 5, 10, 15, 20 ]">
    </table>
</div>
<div id="YCL" class="easyui-dialog" title="隐藏列设置" style="width: 800px; height: 450px;"
     data-options="iconCls:'icon-save',resizable:true,modal:false,closed:true,collapsible:true,maximizable:true,onClose:hideMask">
    <div class="easyui-layout" fit="true">
        <div data-options="region:'south',split:false" style="height: 200px;">
            <table class="easyui-datagrid" id="setHideCol" data-options="method:'get',toolbar:'#settb',border:false,singleSelect:true,fit:true,fitColumns:true">
            </table>
        </div>
        <div data-options="region:'center',title:''">
            <table class="easyui-datagrid" id="showHideCol" data-options="method:'get',toolbar:'#tb',border:false,singleSelect:true,fit:true,fitColumns:true">
            </table>
        </div>
    </div>
    <div id="tb" style="padding: 1px; height: auto">
        <div style="margin-bottom: 1px">
            <a href="#" class="easyui-linkbutton" iconcls="icon-add" onclick="javascript:adNewRow()">
                添加隐藏列
            </a>
            <a href="#" class="easyui-linkbutton" iconcls="icon-remove" onclick="javascript:deRow()">
                删除隐藏列
            </a>
        </div>
    </div>
    <div id="settb" style="padding: 1px; height: auto">
        <div style="margin-bottom: 1px">
            <a href="#" class="easyui-linkbutton" iconcls="icon-add" onclick="javascript:addRow()">
                添加隐藏条件
            </a>
            <a href="#" class="easyui-linkbutton" iconcls="icon-remove" onclick="javascript:delRow()">
                删除隐藏条件
            </a>
            <a href="#" class="easyui-linkbutton" iconcls="icon-save" onclick="javascript:okRow()">
                隐藏条件保存
            </a> 选择隐藏列:
            <select class="easyui-combobox" style="width: 150px" id="cols" data-options="valueField:'id',textField:'text',multiple:true,onShowPanel:function(){ loadCols(); }">
            </select>
            隐藏类型：
            <select class="easyui-combobox" id="ISYC" style="width: 120px">
                <option value="0">隐藏</option>
                <option value="1">只读</option>
            </select>
        </div>
    </div>
    <div id="divYCSZ">
    </div>
</div>
<div id="GSFY" class="easyui-dialog" title="公式翻译" style="width: 800px; height: 400px;"
     data-options="iconCls:'icon-save',resizable:true,modal:false,closed:true,collapsible:true,maximizable:true">
    <table class="easyui-datagrid" id="fy" data-options="view:scrollview,rownumbers:true,singleSelect:true,autoRowHeight:false,fit:true,fitColumns:true,pageSize:50">
        <thead>
        <tr>
            <th data-options="field:'GS',width:400">
                公式
            </th>
            <th data-options="field:'GSFY',width:400">
                公式翻译
            </th>
        </tr>
        </thead>
    </table>
</div>
<div id="hidebg">
</div>
<!--easyui 设置界面 end-->
</asp:Content>