﻿//*********************************************************//
//** 名称：模板选择
//** 功能: 模板选择
//** 开发日期: 2012-8-1  开发人员：余妮
//** 表:TB_EXCEL             EXCEL模板
//*********************************************************//
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;

public partial class SIS_SelMB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SIS_SelMB));
        hidyy.Value = Session["YY"].ToString();
        setDataBind();
        //setDataBind2();
    }

    #region 后台方法

    /// <summary>
    /// 邦定树结构数据(监控分析模板)
    /// </summary>
    /// <returns></returns>
    public string setDataBind()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query("select * from TB_YSBBMB");
        CreateTreeViewRecursive(TreeView1.Nodes, ds.Tables[0], "0");
        return "";
    }

    /// <summary>
    /// 邦定树结构数据(报表模板)
    /// </summary>
    /// <returns></returns>
    //public string setDataBind2()
    //{
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    DataSet ds = bll.Query("select * from TB_EXCEL WHERE BZ='1'");
    //    CreateTreeViewRecursive2(TreeView1.Nodes, ds.Tables[0], "0");
    //    return "";
    //}

    /// <summary>
    /// 递归查询(监控分析模板)
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    private void CreateTreeViewRecursive(TreeNodeCollection nodes, System.Data.DataTable dataSource, string ParCode)
    {
        DataRow[] drarr = dataSource.Select();
        TreeNode node;
        TreeNode Cnode;

        node = new TreeNode();
        node.Text = "EXCEL模板";
        node.Value = "";
        node.Expanded = false;
        node.ImageUrl = "~/images/minus.gif";
        node.NavigateUrl="javascript:";
        nodes.Add(node);
        foreach (DataRow dr in drarr)
        {
            Cnode = new TreeNode();
            Cnode.Text = dr["MBMC"].ToString();
            Cnode.Value = dr["MBDM"].ToString();
            Cnode.Expanded = false;
            Cnode.ImageUrl = "~/images/noexpand.gif";
            Cnode.NavigateUrl = "javascript:SelMB($(\"#" + hidID.ClientID + "\").val(" + dr["MBDM"].ToString() + "),$(\"#" + hidName.ClientID + "\").val('" + dr["MBMC"].ToString() + "'),$(\"#" + hidType.ClientID + "\").val(0));";
            node.ChildNodes.Add(Cnode);
        }

    }

    /// <summary>
    /// 递归查询(报表模板)
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    //private void CreateTreeViewRecursive2(TreeNodeCollection nodes, System.Data.DataTable dataSource, string ParCode)
    //{
    //    DataRow[] drarr = dataSource.Select();
    //    TreeNode node;
    //    TreeNode Cnode;

    //    node = new TreeNode();
    //    node.Text = "报表模板";
    //    node.Value = "";
    //    node.Expanded = false;
    //    node.ImageUrl = "~/images/minus.gif";
    //    node.NavigateUrl = "javascript:";
    //    nodes.Add(node);
    //    foreach (DataRow dr in drarr)
    //    {
    //        Cnode = new TreeNode();
    //        Cnode.Text = dr["NAME"].ToString();
    //        Cnode.Value = dr["ID"].ToString();
    //        Cnode.Expanded = false;
    //        Cnode.ImageUrl = "~/images/noexpand.gif";
    //        Cnode.NavigateUrl = "javascript:SelMB($(\"#" + hidID.ClientID + "\").val(" + dr["ID"].ToString() + "),$(\"#" + hidName.ClientID + "\").val('" + dr["NAME"].ToString() + "'),$(\"#" + hidType.ClientID + "\").val(1));";
    //        node.ChildNodes.Add(Cnode);
    //    }
    //}

    #endregion;
}
