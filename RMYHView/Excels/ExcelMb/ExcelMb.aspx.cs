﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text;
using System.Data.OleDb;
using Excel;
using System.Drawing;
using RMYH.BLL;
using RMYH.DAL;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using RMYH.Model;
using System.Data;
using System.Collections;
public partial class Excels_ExcelMb_ExcelMb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(Excels_ExcelMb_ExcelMb));
        //if (!IsPostBack)

       

        string mbdm = Request.QueryString["mbdm"].ToString();
        string mbmc = Request.QueryString["mbmc"].ToString();
        string mbwj = Request.QueryString["mbwj"].ToString();
        HidHszxdm.Value = Request.QueryString["HSZXDM"].ToString();
        HidUserdm.Value = Request.QueryString["USERDM"].ToString();
        HidYy.Value = Request.QueryString["YY"].ToString();
        HidMbwj.Value = mbwj;
        HidMbdm.Value = mbdm;
        HidMbmc.Value = mbmc;
        PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/Excels/pageoffice/server.aspx";
        //PageOfficeCtrl1.AddCustomToolButton("保存", "Save()", 1);
        string address = hidAddress.Value;
        //定义Workbook对象
        //PageOffice.ExcelWriter.Workbook workBook = new PageOffice.ExcelWriter.Workbook();
        ////定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        //PageOffice.ExcelWriter.Sheet sheet = workBook.OpenSheet("Sheet1");
        //PageOffice.ExcelWriter.Table table1 = sheet.OpenTable("A1:B50");
        ////设置table对象的提交名称，以便保存页面获取提交的数据
        //table1.SubmitName = "Info";
        //PageOfficeCtrl1.SetWriter(workBook);
        //查询能修改模板的权限
        string sql = "  SELECT MBXGQX ,A.* FROM TB_MBJSQX A,TB_JSYH B WHERE A.JSDM=B.JSDM ";
        sql += "AND B.USERID='" + Request.QueryString["USERDM"].ToString() + "' AND A.MBDM='" + mbdm + "' AND MBXGQX='1'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set=bll.Query(sql);
        bool MBQX=false;
        if (set.Tables[0].Rows.Count > 0)
        {
            MBQX = true;
        }
        PageOfficeCtrl1.AddCustomToolButton("公式翻译", "gsfy()", 0);
        PageOfficeCtrl1.AddCustomToolButton("设置公式", "ByVal('1')", 7);
        if (MBQX == true)
        {
            PageOfficeCtrl1.AddCustomToolButton("保 存", "Save()", 0);
        }
        PageOfficeCtrl1.AddCustomToolButton("行属性设置", "HSZ()", 1);
        PageOfficeCtrl1.AddCustomToolButton("列属性设置", "LSZ()", 18);
        if (MBQX == true)
        {
            PageOfficeCtrl1.AddCustomToolButton("编辑区域", "DJQY()", 8);
            PageOfficeCtrl1.AddCustomToolButton("取消编辑区域", "QXDJQY()", 9);
            PageOfficeCtrl1.AddCustomToolButton("本地打开", "OpenAsFile()", 11);
            PageOfficeCtrl1.AddCustomToolButton("设置备注", "Szbz()", 14);
            PageOfficeCtrl1.AddCustomToolButton("校验公式", "XYGS()", 20);
            PageOfficeCtrl1.AddCustomToolButton("自动生成公式", "ZDSCGS()", 20);
            PageOfficeCtrl1.AddCustomToolButton("隐藏列设置", "YCLSZ()", 16);
            PageOfficeCtrl1.AddCustomToolButton("全区域计算列设置表", "QQYLBSZ()", 16);
        }
        ////PageOfficeCtrl1.AddCustomToolButton("测试", "cs()", 20);
        //PageOfficeCtrl1.AddCustomToolButton("添加印章", "TJYZ()", 18);
        PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
        PageOfficeCtrl1.TimeSlice = 100;
        //PageOfficeCtrl1.SaveFilePage = "SaveFile1.aspx";
        //PageOfficeCtrl1.WebOpen("doc/test1.xls", PageOffice.OpenModeType.xlsSubmitForm, "操作人姓名");

    }
    /// <summary>
    /// 获取隐藏列
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getlh(string mbdm, string sheet,string cols,string ISYC)
    {
        string sql = "";
        sql = "select *  from TB_MBCOLYCTJ where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' and COLS='"+cols+"' AND ISYC='"+ISYC+"'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["COLS"].ToString();
        }
        else
        {
            return "";
        }
    }
    /// <summary>
    /// 获取全区域计算选择列
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getlhqqy(string mbdm, string sheet,string MBLX)
    {
        string sql = "";
        sql = "select MBLX,COLS from TB_QQYJSCOL where MBDM='" + mbdm + "' and SHEETNAME='" + sheet + "' and MBLX='"+MBLX+"'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["COLS"].ToString();
        }
        else
        {
            return "";
        }
    }
    /// <summary>
    /// 模板定义里增加模板计算列设置
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet</param>
    /// <param name="mblx">模板类型</param>
    /// <param name="col">列</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int saveQQYJSL(string mbdm,string sheet,string[] mblx,string[] col)
    {
        ArrayList arr = new ArrayList();
        string SQL = "delete FROM TB_QQYJSCOL WHERE MBDM='"+mbdm+"' and SHEETNAME='"+sheet+"'";
        arr.Add(SQL);
        for (int i = 0; i < mblx.Length; i++)
        {
            arr.Add("insert into TB_QQYJSCOL(MBDM,SHEETNAME,MBLX,COLS) values('"+mbdm+"','"+sheet+"','"+mblx[i]+"','"+col[i]+"')");
        }
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        int count = BLL.ExecuteSql((string[])arr.ToArray(typeof(string)), null);
        return count;
    }
    /// <summary>
    /// 获取设置好的计算列
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getQQYJSL(string mbdm, string sheet)
    {
        string SQL = "select MBLX,COLS from TB_QQYJSCOL where MBDM='"+mbdm+"' and SHEETNAME='"+sheet+"'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet SET = BLL.Query(SQL);
        string list = "";
        if (SET.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < SET.Tables[0].Rows.Count; i++)
            {
                list += SET.Tables[0].Rows[i]["MBLX"].ToString() + "&" + SET.Tables[0].Rows[i]["COLS"].ToString();
                if (i < SET.Tables[0].Rows.Count - 1)
                {
                    list += "|";
                }
            }
        }
        return list;
    }
    /// <summary>
    /// 设置隐藏列时查询当前库中是否存在已经相同的列
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <param name="cols">列明</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string YZLSFCF(string mbdm, string sheet,string YSLH, string cols,string ISYC)
    {
        string sql = "";
        if (YSLH != "")
        {
            sql = "select *  from TB_MBCOLYC where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' and COLS!='" + YSLH + "' AND ISYC='"+ISYC+"'";
        }
        else
        {
            sql = "select *  from TB_MBCOLYC where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' AND ISYC='" + ISYC + "'";
        }
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            var LH = "";
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                LH += set.Tables[0].Rows[i]["COLS"].ToString()+",";

            }
            LH=LH.Substring(0,LH.Length-1);
            string[] colss = cols.Split(',');
            string[] LHs = LH.Split(',');
            //相同字符个数
            int count = 0;
            for (int i = 0; i < colss.Length; i++)
            {
                for (int j = 0; j < LHs.Length; j++)
                {
                    //同位置字符是否相同
                    if (colss[i] == LHs[j])
                    {
                        count++;
                    }
                }
            }
            return count.ToString();
        }
        else
        {
            return "0";
        }


    }
    /// <summary>
    /// 获取隐藏列设置中条件设置，1年份，2角色，3模板类型
    /// </summary>
    /// <param name="gslx">公式类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string gslx)
    {
        string Str="";
        TB_ZDSXBBLL bll=new TB_ZDSXBBLL();
        //拼接1-12月份josn
        if (gslx == "1" || gslx=="4")
        {
            Str = "[";
            for (int i = 1; i <= 12; i++)
            {
                Str += "{\"id\":\"" + i + "\",\"text\":\"" + i+"月" + "\"}";
                if (i < 12)
                {
                    Str += ",";
                }
            }
            //如果是当前分解月份的话，则添加一项已发布月份
            if (gslx == "4")
            {
                Str += ",{\"id\":\"@YFBYF\",\"text\":\"已发布月份\"}";
            }
            Str += "]";
        }
        //拼接角色josn
        else if (gslx == "2")
        {
            string sql = " SELECT JSDM,NAME FROM TB_JIAOSE";
            DataSet set = bll.Query(sql);
            Str = "[";
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                Str += "{\"id\":\"" + set.Tables[0].Rows[i]["JSDM"].ToString() + "\",\"text\":\"" + set.Tables[0].Rows[i]["NAME"].ToString() + "\"}";
                if (i < set.Tables[0].Rows.Count-1)
                {
                    Str += ",";
                }
            }
            Str += "]";
        }
        //模板类别
        else if (gslx == "3")
        {
            //string sql = " SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSMBLX'";
            //DataSet set = bll.Query(sql);
            Str = "[";
            //for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            //{
            Str += "{\"id\":\"0\",\"text\":\"填报\"},";
            Str += "{\"id\":\"1\",\"text\":\"分解\"},";
            Str += "{\"id\":\"2\",\"text\":\"批复\"}";
            //    if (i < set.Tables[0].Rows.Count + 1)
            //    {
            //        Str += ",";
            //    }
            //}
            Str += "]";
        }
        return Str;
    }
  
/// <summary>
/// 添加隐藏列数据设置明细
/// </summary>
/// <param name="mbdm">模板代码</param>
/// <param name="sheet">sheet页</param>
/// <param name="HH">行号</param>
/// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string mbdm,string sheet,int HH)
    {
        return getNewLine("10147028",false,"",mbdm,sheet,HH);
    }
    [AjaxPro.AjaxMethod]
    public string UpdateLH(string mbdm, string sheet, string cols,string LH,string YC,string ISYC)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string[] ARRSQL = new string[1];
        string sqlyc = "UPDATE  TB_MBCOLYCTJ SET COLS='" + cols + "',ISYC='"+ISYC+"' where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' AND COLS='" + LH.Trim() + "' AND ISYC='"+YC+"'";
        ARRSQL[0] = sqlyc;
        int count = bll.ExecuteSql(ARRSQL, null);
        return count.ToString();
    }
    [AjaxPro.AjaxMethod]
    public string DeleteDataYCL(string mbdm, string sheet, string cols,string ISYC)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string[] ARRSQL=new  string[2];
        string sqlyc = "delete from TB_MBCOLYC where  MBDM='"+mbdm+"' AND SHEETNAME='"+sheet+"' AND COLS='"+cols+"' AND ISYC='"+ISYC+"'";
        string sqlycl = "delete from TB_MBCOLYCTJ where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' AND COLS='" + cols + "' AND ISYC='"+ISYC+"'";
        ARRSQL[0] = sqlyc;
        ARRSQL[1] = sqlycl;
        int count = bll.ExecuteSql(ARRSQL, null);
        return count.ToString();
    }
    /// <summary>
    /// 删除隐藏列明细
    /// </summary>
    /// <param name="id">主键字段</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteDataYCLSZ(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_MBCOLYCTJ", id, "GSID");
        return ret;
    }
    /// <summary>
    /// 添加隐藏列公式
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <param name="HH">列号</param>
    /// <param name="GS">公式</param>
    /// <param name="GSJX">公式解析</param>
    /// <param name="GSJX">修改前的列号</param>
    /// <param name="Flag">判断是否有行设置</param>
    /// <param name="ISYC">新的隐藏类型</param>
    /// <param name="YC">原隐藏类型</param>
    /// <param name="YSLH">原设置列号</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int AddDataGS(string mbdm, string sheet, string HH, string GS, string GSJX, string YSLH, string ISYC, string YC,bool Flag)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string[] ARRSQL;
        string sqlyc = "delete from TB_MBCOLYC where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "' AND COLS='" + YSLH + "' AND ISYC='"+YC+"'";
        if (Flag == true)
        {
            ARRSQL = new string[2];
            ARRSQL[0] = sqlyc;
            string sql = " insert into TB_MBCOLYC(MBDM,SHEETNAME,COLS,GS,GSJX,ISYC) VALUES ('" + mbdm + "','" + sheet + "','" + HH + "','" + GS + "','" + GSJX + "','" + ISYC + "')";
            sql = sql.Replace("\\", "'");
            ARRSQL[1] = sql;
        }
        else
        {
            ARRSQL = new string[1];
            ARRSQL[0] = sqlyc;
        }
        int count = bll.ExecuteSql(ARRSQL,null);
        return count;
    }
/// <summary>
/// 修改隐藏列数据
/// </summary>
/// <param name="ID"></param>
/// <param name="fileds"></param>
/// <param name="values"></param>
/// <param name="LH">列号</param>
/// <param name="MBDM">模板代码</param>
/// <param name="SHEET">sheet页</param>
/// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateDataYCL(string[] ID, string fileds, string[] values,string LH,string MBDM,string SHEET,string ISYC)
    {
       
        string ret = "";
        try
        {
            ArrayList idadd = new ArrayList();
            ////需要修改的id数组
            //ArrayList idupdate = new ArrayList();
            for (int i = 0; i < ID.Length; i++)
            {
                idadd.Add(ID[i].Trim());
            }
            //for (int i = 0; i < idadd.Count; i++)
            //{
            //    string sql = "";
            //}
            fileds = fileds + ",MBDM,SHEETNAME,COLS,GS,ISYC";
            for (int i = 0; i < ID.Length; i++)
            {
                // 1|7.7月,9.9月|1
                string gs = "";
                for (int j = 0; j < values[i].Split('|')[1].Split(',').Length; j++)
                {
                    gs += values[i].Split('|')[1].Split(',')[j].Split('.')[0].ToString();
                    if (j< values[i].Split('|')[1].Split(',').Length - 1)
                    {
                        gs += ",";
                    }
                }
                values[i] += "|" + MBDM + "|" + SHEET + "|" + LH + "|" + gs+"|"+ISYC;
            }
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.Update("10147028", "TB_MBCOLYCTJ", fileds, values, "GSID", (string[])idadd.ToArray(typeof(string)));
          
        }
        catch 
        {

        }
        return ret;
    }
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled,string mbdm,string sheet,int HH)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "MBDM")
                        {
                            sb.Append(mbdm);
                        }
                        else if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "SHEETNAME")
                        {
                            sb.Append(sheet);
                        }
                        else
                        {
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        }

                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvaluesYCL(this)' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取excel备注名称
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getExcelBzMc()
    {
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        string SQL = "SELECT ISNULL(MAX(BM),0) BM FROM TB_EXCLEBZ";
        DataSet DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            int maxDM = int.Parse(DS.Tables[0].Rows[0][0].ToString());
            int excelMC = maxDM + 1;
            return excelMC.ToString();
        }
        else
        {
            return "1";
        }
    }
    /// <summary>
    /// 添加excel备注名称
    /// </summary>
    /// <param name="mc"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string addExcelBzMc(string mc)
    {
        if (mc == "")
            mc = "0";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "insert into TB_EXCLEBZ values("+mc+")";
        int count = BLL.ExecuteSql(SQL);
        return count.ToString();
    }
    /// <summary>
    /// 查询隐藏类型
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet页</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string yzlx(string mbdm,string sheet)
    {
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        string sql = "select * from TB_MBSHEETSX where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = BLL.Query(sql);
        int count = SET.Tables[0].Rows.Count;
        if (count == 1)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }
    /// <summary>
    /// 查询横向类型
    /// </summary>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string hlx(string mbdm, string sheet)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "select * from TB_MBSHEETSX where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = BLL.Query(sql);
        if (SET.Tables[0].Rows.Count > 0)
        {
            return SET.Tables[0].Rows[0]["HXLX"].ToString();
        }
        else
        {
            return "-1";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ZYBM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GETJSDXLX(string ZYBM)
    {
        string sql = " select SJLX from TB_JSDX where ZFBM='" + ZYBM + "' ";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet SET = BLL.Query(sql);
        if (SET.Tables[0].Rows.Count > 0)
        {
            return SET.Tables[0].Rows[0]["SJLX"].ToString();
        }
        else
        {
            return "-1";
        }
        
    }
    /// <summary>
    /// 保存横向属性
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="sheet">sheet</param>
    /// <param name="hxlx">横向类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int SaveHxsx(string mbdm,string sheet,string hxlx)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "delete from TB_MBSHEETSX where MBDM='"+mbdm+"' AND SHEETNAME='"+sheet+"'";
        bll.ExecuteSql(sql);
        sql = "insert into TB_MBSHEETSX(MBDM,SHEETNAME,HXLX) values('" + mbdm + "','" + sheet + "','" + hxlx + "')";
        int count = bll.ExecuteSql(sql);
        return count;
    }
    [AjaxPro.AjaxMethod]
    public int updataLH(string mbdm, string sheet, string lh)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "UPDATE  TB_MBSHEETSX SET LH='"+lh+"' where MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        int count=bll.ExecuteSql(sql);
        return count;
    }
    [AjaxPro.AjaxMethod]
    public int updataHH(string mbdm, string sheet, string Hh)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "UPDATE  TB_MBSHEETSX SET HH='" + Hh + "' where MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        int count = bll.ExecuteSql(sql);
        return count;
    }
    [AjaxPro.AjaxMethod]
    public string getLH(string mbdm, string sheet)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select LH FROM TB_MBSHEETSX WHERE  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = bll.Query(sql);
        string LH = "";
        if (SET.Tables[0].Rows.Count > 0)
        {
            LH = SET.Tables[0].Rows[0]["LH"].ToString();
        }
        return LH;
    }
    [AjaxPro.AjaxMethod]
    public string getHH(string mbdm, string sheet)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT HH FROM  TB_MBSHEETSX WHERE MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = bll.Query(sql);
        string HH = "";
        if (SET.Tables[0].Rows.Count > 0)
        {
            HH = SET.Tables[0].Rows[0]["HH"].ToString();
        }
        return HH;
    }
    [AjaxPro.AjaxMethod]
    public string gethxlx()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select XMDH_A,XMMC from XT_CSSZ where XMFL='HXLX'";
        DataSet ds = bll.Query(sql);
        string retstr = "";
        for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
        {
            retstr += "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
        }
        return retstr;
    }
   
    
    /// <summary>
    /// 验证行列属性
    /// </summary>
    /// <returns></returns>
    public string HLyz(string[] HSX,string[] LSX)
    {
        return "";
    }
    public int ch2int(char ch)  
    {
        if (ch >= 'a' && ch <= 'z')
            return ch - 'a' + 1;
        if (ch >= 'A' && ch <= 'Z')
            return ch - 'A' + 1;
        else
            return -1;
    }

    /// <summary>
    /// 根据字母获取数字
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public int f(String str)
    {
        char[] ch = str.ToCharArray();
        int ret = 0;
        for (int i = 0; i < ch.Length; i++)
        {
            ret *= 26;
            ret += ch2int(ch[i]);
        }
        return ret;
    }
    /// <summary>
    /// 根据数字获取字母
    /// </summary>
    /// <param name="SZ">数值</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getZM(int SZ)
    {
        return (convert(SZ));
    }
    public string convert(int num)
    {
        if (num <= 26)
        {
            return num == 0 ? "z" : Convert.ToString((char)('a' + num - 1));
        }
        else
        {
            return convert(num % 26 == 0 ? num / 26 - 1 : num / 26) + convert(num % 26);
        }
    }
    //判断当前列表是设置隐藏列的列表还是设置行列属性的列表
    static string  GSJY = "";
    /// <summary>
    /// 刷新隐藏行列数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadListYCL(string trid, string id, string intimgcount, string mb, string sheet)
    {
        GSJY = "true";
        return GetDataListstring("10147020", "", new string[] { ":MBDM", ":SHEETNAME" }, new string[] { mb, sheet }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 刷新隐藏行列设置数据
    /// </summary>
    /// <param name="trid"></param>
    /// <param name="id"></param>
    /// <param name="intimgcount"></param>
    /// <param name="mb"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadListYCLSZ(string trid, string id, string intimgcount, string mb, string sheet,string COLS,string ISYC)
    {
        GSJY = "true";
        return GetDataListstring("10147028", "", new string[] { ":MBDM", ":SHEET", ":COLS",":ISYC" }, new string[] { mb, sheet, COLS,ISYC }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 刷新数据行10144712
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadListH(string trid, string id, string intimgcount, string mb, string sheet)
    {
        GSJY = "";
        return GetDataListstring("10144647", "", new string[] { ":MBDM", ":SHEET" }, new string[] { mb, sheet }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 刷新数据行10144712
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadListL(string trid, string id, string intimgcount, string mb, string sheet)
    {
        GSJY = "";
        return GetDataListstring("10144735", "", new string[] { ":MBDM", ":SHEET" }, new string[] { mb, sheet }, false, trid, id, intimgcount);
    }

    /// <summary>
    /// 添加行数据
    /// </summary>
    /// <param name="value">列名称</param>
    /// <param name="Hh">行号</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddDataH(string[] value)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string ZYML="SELECT ZYDM BM,ZYMC MC,CCJB,YJDBZ,FBDM PAR ";
        ZYML+=" FROM TB_JHZYML ";
        ZYML+=" WHERE SYBZ='1' ORDER BY ZYNBBM ";
        DataSet SetZYML = BLL.Query(ZYML);
        string CPDM=" SELECT 0 CPBS ,XMBM,XMDM BM,XMMC MC,CCJB,YJDBZ,CASE  PAR WHEN '' THEN '0' ELSE PAR END PAR,'' HSZXDM ";
        CPDM+=" FROM TB_XMXX  ";
        CPDM+=" UNION ";
        CPDM+=" SELECT 1 CPBS ,'1' XMDM ,'1' XMBM,'装置产品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM,'' HSZXDM ";
        CPDM+=" UNION ";
        CPDM+="SELECT  1 CPBS ,ZYBM,ZYDM,ZYMC, 2 CCJB,'0' YJDBZ, '1' FBDM ,  HSZXDM ";
        CPDM+="  FROM TB_JHZYML WHERE SYBZ='1' ";
        CPDM+="UNION ";
        CPDM+="SELECT  1 CPBS ,CPBM,CPDM,CPMC, 3 CCJB,'1' YJDBZ, ZYDM  FBDM ,  HSZXDM ";
        CPDM+=" FROM TB_JHCPBM WHERE  CPBS IN(1,4)  ";
        CPDM+="UNION ";
        CPDM+="SELECT 3 CPBS ,'3' XMDM ,'3' XMBM,'自制半成品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM, ''  HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT 3 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ, ";
        CPDM+="CASE FBDM WHEN NULL THEN '3' WHEN '0' THEN '3'  ELSE FBDM END  FBDM ,  HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=3 AND SYBZ='1' ";
        CPDM+=" UNION ";
        CPDM+=" SELECT 5 CPBS ,'5' XMDM ,'5' XMBM,'最终产品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM, ''  HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT  5 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ,  ";
        CPDM+="CASE FBDM WHEN NULL THEN '5' WHEN '0' THEN '5'  ELSE FBDM END  FBDM , HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=5 AND SYBZ='1' ";
        CPDM+="  UNION ";
        CPDM+=" SELECT 6 CPBS , '6' XMDM ,'6' XMBM,'外购物料' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM ,''HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT 6 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ,  ";
        CPDM+="CASE FBDM WHEN NULL THEN '6' WHEN '0' THEN '6'  ELSE FBDM END  FBDM  , HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=5 AND SYBZ='1' ";
        CPDM += " ORDER BY CPBS ";
        DataSet SetCPBM = BLL.Query(CPDM);

        string ss = "";
        for (int i = 0; i < value.Length; i++)
        {
            ss += getNewLine("10144647", false, "", value[i].Split('|')[1].ToString(), value[i].Split('|')[0].ToString(), value[i].Split('|')[2].ToString(), i,SetCPBM,SetZYML);
        }
        return ss;
    }
    /// <summary>
    /// 添加列数据
    /// </summary>
    /// <param name="value">列名称</param>
    /// <param name="Hh">行号</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddDataL(string[] value)
    {
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL ();
        string ZYML="SELECT ZYDM BM,ZYMC MC,CCJB,YJDBZ,FBDM PAR ";
        ZYML+=" FROM TB_JHZYML ";
        ZYML+=" WHERE SYBZ='1' ORDER BY ZYNBBM ";
        DataSet SetZYML = BLL.Query(ZYML);
        string CPDM=" SELECT 0 CPBS ,XMBM,XMDM BM,XMMC MC,CCJB,YJDBZ,CASE  PAR WHEN '' THEN '0' ELSE PAR END PAR,'' HSZXDM ";
        CPDM+=" FROM TB_XMXX  ";
        CPDM+=" UNION ";
        CPDM+=" SELECT 1 CPBS ,'1' XMDM ,'1' XMBM,'装置产品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM,'' HSZXDM ";
        CPDM+=" UNION ";
        CPDM+="SELECT  1 CPBS ,ZYBM,ZYDM,ZYMC, 2 CCJB,'0' YJDBZ, '1' FBDM ,  HSZXDM ";
        CPDM+="  FROM TB_JHZYML WHERE  SYBZ='1' ";
        CPDM+="UNION ";
        CPDM+="SELECT  1 CPBS ,CPBM,CPDM,CPMC, 3 CCJB,'1' YJDBZ, ZYDM  FBDM ,  HSZXDM ";
        CPDM+=" FROM TB_JHCPBM WHERE  CPBS IN(1,4)  ";
        CPDM+="UNION ";
        CPDM+="SELECT 3 CPBS ,'3' XMDM ,'3' XMBM,'自制半成品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM, ''  HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT 3 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ, ";
        CPDM+="CASE FBDM WHEN NULL THEN '3' WHEN '0' THEN '3'  ELSE FBDM END  FBDM ,  HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=3 AND SYBZ='1' ";
        CPDM+=" UNION ";
        CPDM+=" SELECT 5 CPBS ,'5' XMDM ,'5' XMBM,'最终产品' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM, ''  HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT  5 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ,  ";
        CPDM+="CASE FBDM WHEN NULL THEN '5' WHEN '0' THEN '5'  ELSE FBDM END  FBDM , HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=5 AND SYBZ='1' ";
        CPDM+="  UNION ";
        CPDM+=" SELECT 6 CPBS , '6' XMDM ,'6' XMBM,'外购物料' XMMC,1 CCJB,'0'  YJDBZ,'0' FBDM ,''HSZXDM ";
        CPDM+="UNION ";
        CPDM+="SELECT 6 CPBS , CPBM,CPDM,CPMC,1+CCJB  CCJB, YJDBZ,  ";
        CPDM+="CASE FBDM WHEN NULL THEN '6' WHEN '0' THEN '6'  ELSE FBDM END  FBDM  , HSZXDM ";
        CPDM+="  FROM TB_JHCPBM WHERE   CPBS=5 AND SYBZ='1' ";
        CPDM += " ORDER BY CPBS ";
        DataSet SetCPBM = BLL.Query(CPDM);
        string ss = "";
        for (int i = 0; i < value.Length; i++)
        {
            ss += getNewLine("10144735", false, "", value[i].Split('|')[1].ToString(), value[i].Split('|')[0].ToString(), value[i].Split('|')[2].ToString(), i, SetCPBM,SetZYML);
        }
        return ss;
    }
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, string value, string Hh,string HZ, int HID,DataSet SETCPBM,DataSet SETZYML)
    {
        try
        {
            if (HZ == "undefined")
            {
                HZ = ":";
            }
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            //字段说明
            ArrayList ZdSM = new ArrayList();
            int count=   HZ.Split(',').Length;
            ArrayList ZdVALUE = new ArrayList();
               // HZ.Split(',');
            for (int i = 0; i < count;i++ )
            {
                 ZdSM.Add(HZ.Split(',')[i].Split(':')[0]);
                 ZdVALUE.Add(HZ.Split(',')[i].Split(':')[1]);
            }
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr" + HID + ">");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择  class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "" + HID + "</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td style=\"table-layout:fixed\" name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox

                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "ROWH" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "COLH")
                                        sb.Append(" value=" + Hh + " ");
                                    else if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "ROWNAME" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "COLNAME")
                                        sb.Append(" value=" + value + " ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");


                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ZdSM.IndexOf(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()) > -1)
                                    {
                                        int index = ZdSM.IndexOf(ds.Tables[0].Rows[j]["FIELDNAME"].ToString());
                                        string values = ZdVALUE[index].ToString();
                                        sb.Append("    name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList1(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString(),values) + "</select>");
                                    }
                                    else
                                    {
                                        sb.Append("    name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    }
                                     break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString())-60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    string dm = ds.Tables[0].Rows[j]["FIELDNAME"].ToString();
                                    if (dm == "XMDM")
                                        dm = "CPDM";
                                    if (ZdSM.IndexOf(dm) > -1)
                                    {
                                        int index = ZdSM.IndexOf(dm);
                                        string values = ZdVALUE[index].ToString();
                                        if (dm == "CPDM")
                                        {
                                            DataRow[] CpRow = SETCPBM.Tables[0].Select("BM='"+values+"'");
                                            if (CpRow.Length > 0)
                                            {
                                                values = values + "." + CpRow[0]["MC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DataRow[] ZyRow = SETCPBM.Tables[0].Select("BM='" + values + "'");
                                            if (ZyRow.Length > 0)
                                            {
                                                values = values + "." + ZyRow[0]["MC"].ToString();
                                            }
                                        }
                                        sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + values + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50x\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    }
                                    else
                                    {
                                        sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50x\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 横向类型
    /// </summary>
    /// <param name="mbdm"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getHXSX(string mbdm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB WHERE MBDM='" + mbdm + "'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["HXLX"].ToString();
        }
        else
        {
            return "";
        }
    }

    /// <summary>
    /// 获取起始列，最大列
    /// </summary>
    /// <param name="mbdm"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getQslZdl(string mbdm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB WHERE MBDM='" + mbdm + "'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["STARTCOL"].ToString() + "|" + set.Tables[0].Rows[0]["MAXCOLS"].ToString();
        }
        else
        {
            return "";
        }
    }
    // <summary>
    //添加数据，更新数据行属性
    // </summary>
    // <returns></returns>
    [AjaxPro.AjaxMethod]
    public string updateHSX(string FPFSDM, string[] DATAVALUE, string mbdm, string sheet, string nameCOL)
    {
        //string[] arrFPFSDM = FPFSDM[0].Split(',');
        int ss = 1;
        //删除数据,删除对应作业代码的所有数据
        string delsql = "delete from TB_MBROWSX where MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(delsql);
        string insertsql1 = "";
        ArrayList arr = new ArrayList();
        TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
        insertsql1 = "insert into TB_MBROWSX(ID,MBDM,SHEETNAME,ROWH,ROWNAME,XMDM,ZYDM,NAMECOL) Values({0},{1},{2},{3},{4},{5},{6},{7})";
        for (int i = 0; i < DATAVALUE.Length; i++)
        {
            var id = DAL.GetSeed("TB_MBROWSX", "ID");
            arr.Add(string.Format(insertsql1, "'" + id + "'", "'" + mbdm + "'", "'" + sheet + "'", "'" + DATAVALUE[i].Split('|')[0].Trim() + "'", "'" + DATAVALUE[i].Split('|')[1].Trim() + "'", "'" + DATAVALUE[i].Split('|')[2].Trim() + "'", "'" + DATAVALUE[i].Split('|')[3].Trim() + "'", "'" + nameCOL + "'"));
        }
        int rows = bll.ExecuteSql((string[])arr.ToArray(typeof(string)), null);
        return rows.ToString();

    }
    // <summary>
    //添加数据，更新数据列属性
    // </summary>
    // <returns></returns>
    [AjaxPro.AjaxMethod]
    public string updateLSX(string FPFSDM, string[] DATAVALUE, string mbdm, string sheet, string nameCOL)
    {
        //string[] arrFPFSDM = FPFSDM[0].Split(',');

        int ss = 1;
        //删除数据,删除对应作业代码的所有数据
        string delsql = "delete from TB_MBCOLSX where MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(delsql);
        string insertsql1 = "";
        ArrayList arr = new ArrayList();
        TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
        insertsql1 = "insert into TB_MBCOLSX(ID,MBDM,SHEETNAME,NAMEROW,COLH,COLNAME,ZYDM,XMDM,SJDX,JSDX,JLDW) Values({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})";
        for (int i = 0; i < DATAVALUE.Length; i++)
        {
            var id = DAL.GetSeed("TB_MBCOLSX", "ID");
            arr.Add(string.Format(insertsql1, "'" + id + "'", "'" + mbdm + "'", "'" + sheet + "'", "'" + nameCOL + "'", "'" + DATAVALUE[i].Trim().Split('|')[0].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[1].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[2].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[3].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[4].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[5].Trim() + "'", "'" + DATAVALUE[i].Trim().Split('|')[6].Trim() + "'"));
        }
        int rows = bll.ExecuteSql((string[])arr.ToArray(typeof(string)), null);
        return rows.ToString();
    }
    //刷新成本中心 项目
    #region
    [AjaxPro.AjaxMethod]
    public string LoadListXsCb(string trid, string id, string intimgcount)
    {
        return GetDataList.GetDataListstring("10144712", "BM", new string[] { }, new string[] { }, false, trid, id, intimgcount);

    }
    [AjaxPro.AjaxMethod]
    public string LoadListXsXM(string trid, string id, string intimgcount, string ZYDM)
    {
        return GetDataList.GetDataListstring("10144723", "BM", new string[] { ":ZYDM" }, new string[] { ZYDM }, false, trid, id, intimgcount);
    }
    #endregion

    /// <summary>
    /// 获取起始行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getStartRows(String dm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB where MBDM='" + dm + "' ";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["STARTROW"].ToString();
        }
        else
        {
            return "";
        }
    }
    /// <summary>
    /// 获取起始行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getZYDM(String dm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB where MBDM='" + dm + "' ";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["ZYDM"].ToString();
        }
        else
        {
            return "";
        }
    }
    /// <summary>
    /// 获取起始列
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getStartCol(String dm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB where MBDM='" + dm + "' ";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            return set.Tables[0].Rows[0]["STARCOL"].ToString();
        }
        else
        {
            return "";
        }
    }
    [AjaxPro.AjaxMethod]
    public void SaveXml(string dm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB where MBDM='" + dm + "' ";
        DataSet set = bll.Query(sql);
        string xlsValue;
        if (set.Tables[0].Rows.Count > 0)
        {
            xlsValue = set.Tables[0].Rows[0]["MBWJ"].ToString();
        }
        else
        {
            xlsValue = "test1.xls";
        }
        string filenames = Server.MapPath("../Excels/XLS/" + xlsValue);
        string file1 = Server.MapPath("../Excels/XLS/" + xlsValue.Replace("xls", "xml"));
        Excel.Application app = new Excel.Application();
        Excel.Workbooks workbooks = app.Workbooks;
        workbooks.Add(filenames);
        Excel.Workbook workbook = workbooks.get_Item(1);
        workbook.SaveAs(file1, Excel.XlFileFormat.xlXMLSpreadsheet, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        NAR(workbook);
        NAR(workbooks);
        app.Quit();
        NAR(app);
        System.Threading.Thread.Sleep(500);
        XmlDocument xml = new XmlDocument();
        xml.Load(file1);
    }
    private void NAR(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
        }
        catch { }
        finally
        {
            obj = null;
        }
    }

    //protected void Button111_Click(object sender, EventArgs e)
    //{
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    var mbid = hidID.Value;
    //    string sql = "select * from TB_YSBBMB where MBDM='" + mbid + "' ";
    //    DataSet set = bll.Query(sql);
    //    string xlsValue;
    //    if (set.Tables[0].Rows.Count > 0)
    //    {
    //        xlsValue = set.Tables[0].Rows[0]["MBWJ "].ToString();
    //    }
    //    else
    //    {
    //        xlsValue = "test1.xls";
    //    }
    //    string filenames = Server.MapPath("../XLS/" + xlsValue);
    //    File.Delete(filenames);
    //    File1.PostedFile.SaveAs(filenames);
    //    PageOfficeCtrl1.WebOpen("XLS/" + xlsValue + "", PageOffice.OpenModeType.xlsNormalEdit, "操作人姓名");
    //}
    /// <summary>
    /// 获取选中的文件名
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getWJM(string name)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_YSBBMB where MBMC='" + name + "' ";
        DataSet set = bll.Query(sql);
        string xlsValue;
        if (set.Tables[0].Rows.Count > 0)
        {
            xlsValue = set.Tables[0].Rows[0]["MBWJ"].ToString();
        }
        else
        {
            xlsValue = "test1.xls";
        }
        return xlsValue;
    }
    #region
    //protected void Button3_Click(object sender, EventArgs e)
    //{
    //    PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/pageoffice/server.aspx";
    //    PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
    //    //创建文件
    //    //PageOfficeCtrl1.WebCreateNew("somebody", PageOffice.DocumentVersion.Excel2003);//可创建不同版本的word文件
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    var mbid = hidID.Value;
    //    string sql = "select * from TB_YSBBMB where MBDM='" + mbid + "' ";
    //    DataSet set = bll.Query(sql);
    //    string xlsValue;
    //    if (set.Tables[0].Rows.Count > 0)
    //    {
    //        xlsValue = set.Tables[0].Rows[0]["MBWJ"].ToString();
    //    }
    //    else
    //    {
    //        xlsValue = "test1.xls";
    //    }
    //    string XmlName = xlsValue;
    //    hidMBVALUE.Value = xlsValue;
    //    string file = Server.MapPath("../ExcelMb/XLS/" + XmlName);
    //    string file1 = Server.MapPath("../ExcelMb/XLS/" + XmlName.Replace("xls","xml"));
    //    if (!File.Exists(file))
    //    {
    //        File.WriteAllText(file, "", Encoding.Unicode);
    //    }
    //    if (!File.Exists(file1))
    //    {
    //        File.WriteAllText(file1, "", Encoding.Unicode);
    //    }
    //    PageOfficeCtrl1.WebOpen("XLS/" + xlsValue + "", PageOffice.OpenModeType.xlsNormalEdit, "操作人姓名");
    //    txtMB.Value = hidMBVALUE.Value;
    //}
    #endregion

    #region
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 250) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='50px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' name=td" + ds.Tables[0].Rows[i]["FIELDNAME"].ToString() + "");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue;
            if (GSJY == "")
            {
                dsValue = bll.Query(model.SQL.ToString() + " and 1!=1");
            }
            else
            {
                dsValue = bll.Query(model.SQL.ToString() );
            }
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(System.Data.DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, System.Collections.Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        if (ds.Tables[0].Rows[j]["LEN"].ToString() == "" || ds.Tables[0].Rows[j]["LEN"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["LEN"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        else
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["LEN"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {

                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT   STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (GSJY == "")
                                    {
                                        sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    }
                                    else
                                    {
                                        sb.Append(" disabled=\"disabled\"  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" VALUE=选取 ONCLICK='selectvaluesYCL(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    private static string GetSelectList1(string filedvalue, object objds, string isnull,string values)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (values == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion

    //接收bbgsjs页面传过来的值
    [AjaxPro.AjaxMethod]
    public string MaxBh()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string gs = "select GSBH from TB_MBJYGS where ID=(select MAX(ID) from TB_MBJYGS)";
        DataSet da = bll.Query(gs);
        string maxbh = "";
        if (da.Tables[0].Rows.Count == 0)
        { //表里没有数据时
            maxbh = "0";
        }
        else
        {
            if (da.Tables[0].Rows[0][0].ToString() == "")
            {
                maxbh = "1";
            }
            else
            {
                maxbh = (int.Parse(da.Tables[0].Rows[0][0].ToString())).ToString();
            }
        }

        return maxbh;
    }

    //查询TB_MBJYGS表数据
    [AjaxPro.AjaxMethod]
    public string getList(string MBDM,string sheet)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select ID,MBDM,SHEETNAME,GSBH,GS,INFOMATION,GSJX,LX from TB_MBJYGS WHERE MBDM='" + MBDM + "' and SHEETNAME='" + sheet + "' order by ID,MBDM ";
        DataSet da = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        Str.Append("{\"total\":");
        Str.Append(da.Tables[0].Rows.Count);
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            Str.Append("\"ID\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ID"].ToString() + "\"" + ",");
            Str.Append("\"MBDM\":");
            Str.Append(da.Tables[0].Rows[i]["MBDM"].ToString() + ",");
            Str.Append("\"SHEETNAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["SHEETNAME"].ToString() + "\"" + ",");
            Str.Append("\"GSBH\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GSBH"].ToString() + "\"" + ",");
            Str.Append("\"GS\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GS"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"INFOMATION\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["INFOMATION"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"GSJX\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GSJX"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"LX\":");
            if (da.Tables[0].Rows[i]["LX"].ToString().Replace("\"", "\\\"") == "0")
            {
                Str.Append("\"错误\"");
            }
            else
            {
                Str.Append("\"警告\"");
            }
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }

    //保存easyuidatagrid 新增数据
    [AjaxPro.AjaxMethod]
    public int EasyuiSave(string addData, string upData)
    {

        int Flag = 0;
        string ID = "", MBDM = "", SHEETNAME = "", GSBH = "", GS = "", GSJX = "", INFOMATION = "",LX="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();

        if (addData != "" || upData != "")
        {
            if (addData.Length != 0)
            {
                string[] arr = addData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    MBDM = ZD[0].ToString();
                    SHEETNAME = ZD[1].ToString();
                    GSBH = ZD[2].ToString();
                    GS = ZD[3].ToString();
                    GSJX = ZD[4].ToString();
                    INFOMATION = ZD[5].ToString();
                    LX = ZD[6].ToString();
                    string NewSql = "INSERT INTO TB_MBJYGS (MBDM,SHEETNAME,GSBH,GS,GSJX,INFOMATION,LX)VALUES(@MBDM,@SHEETNAME,@GSBH,@GS,@GSJX,@INFOMATION,@LX)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@MBDM", AseDbType.VarChar,40),
                    new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                    new AseParameter("@GSBH", AseDbType.VarChar,40),
                    new AseParameter("@GS", AseDbType.VarChar,600),
                    new AseParameter("@GSJX", AseDbType.VarChar,800), 
                    new AseParameter("@INFOMATION", AseDbType.VarChar,500),
                    new AseParameter("@LX", AseDbType.VarChar,500),
                };
                    New[0].Value = MBDM;
                    New[1].Value = SHEETNAME;
                    New[2].Value = GSBH;
                    New[3].Value = GS;
                    New[4].Value = GSJX;
                    New[5].Value = INFOMATION;
                    New[6].Value = LX;
                    List.Add(New);
                }
            }

            if (upData.Length != 0)
            {
                string[] arr = upData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    ID = ZD[0].ToString();
                    MBDM = ZD[1].ToString();
                    SHEETNAME = ZD[2].ToString();
                    GSBH = ZD[3].ToString();
                    GS = ZD[4].ToString();
                    GSJX = ZD[5].ToString();
                    INFOMATION = ZD[6].ToString();
                    LX = ZD[7].ToString();
                    string UpSql = "UPDATE TB_MBJYGS set INFOMATION=@INFOMATION,GS=@GS,GSJX=@GSJX,LX=@LX where ID=@ID";
                    ArrList.Add(UpSql);
                    AseParameter[] Up = {
                    new AseParameter("@ID", AseDbType.Integer),
                    new AseParameter("@GS", AseDbType.VarChar,600),
                    new AseParameter("@GSJX", AseDbType.VarChar,800), 
                    new AseParameter("@INFOMATION", AseDbType.VarChar,500),
                    new AseParameter("@LX", AseDbType.VarChar,500),
                };
                    Up[0].Value = ID;
                    Up[1].Value = GS;
                    Up[2].Value = GSJX;
                    Up[3].Value = INFOMATION;
                    Up[4].Value = LX;
                    List.Add(Up);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        return Flag;
    }

    //删除easyuidatagrid 数据
    [AjaxPro.AjaxMethod]
    public int Del(string DelData)
    {
        int Flag = 0;
        string MBDM = "", SHEETNAME = "", GSBH = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();
        if (DelData != "")
        {
            if (DelData.Length != 0)
            {
                string[] arr = DelData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('|');
                    MBDM = ZD[0].ToString();
                    SHEETNAME = ZD[1].ToString();
                    GSBH = ZD[2].ToString();
                    string DelSql = "DELETE FROM TB_MBJYGS WHERE MBDM=@MBDM and SHEETNAME=@SHEETNAME and GSBH=@GSBH";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = {
                    new AseParameter("@MBDM", AseDbType.VarChar,40),
                    new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                    new AseParameter("@GSBH", AseDbType.VarChar,40),
                };
                    Del[0].Value = MBDM;
                    Del[1].Value = SHEETNAME;
                    Del[2].Value = GSBH;
                    List.Add(Del);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        return Flag;
    }

    /// <summary>
    /// 判断当前的设置之前有没有设置过
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SHEETNAME">SHEETNAME名称</param>
    /// <param name="Cols">列名</param>
    /// <param name="ISYC">隐藏或者只读</param>
    /// <returns></returns>
     [AjaxPro.AjaxMethod]
    public string IsCanSetYC(string MBDM,string SHEETNAME,string Cols,string ISYC)
    {
        string Flag = "";
        string SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string[] Arr = Cols.Split(',');
        for (int i = 0; i < Arr.Length; i++)
        {
            SQL = "SELECT * FROM TB_MBCOLYC WHERE MBDM='"+MBDM+"' AND SHEETNAME='"+SHEETNAME+"' AND ISYC='"+ISYC+"' AND (COLS LIKE '%,"+Arr[i]+"%' OR COLS LIKE '%"+Arr[i]+",%') ";
            DS=BLL.Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Flag = Flag==""? Arr[i]:Flag+","+Arr[i];
            }
        }
        return Flag;
    }
}

