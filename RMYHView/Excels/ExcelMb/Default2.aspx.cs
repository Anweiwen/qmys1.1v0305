﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using System.Text;
using System.Collections.Generic;
public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(Default2));
        //JHZYTRGS("10070010","10010688|10010690","10113699|10113765","TRSL","JHBZ")
        //string gsss = "  JHZYTRGS(\"10070010\",\"10010694\",\"10113513\",\"FAZCLL\",\"DQFA\")";
        string gsss = "YSXMGS(\"10160036\",\"10161179\",\"10160858\",\"10010100\",\"CWXGJE\",\"YSSQ\")";
        //YSXMGS("10161653","10160713","10110191","10010002","CWXGJE","FA2")
        //CWQSGS("102178000001","51011700","7800105016","","","JFJE","BNLJ1")
        qz(gsss, "2016", "08", "10050689", "10050727", "1");
        //ss("1231","dsd");
    }
    DataSet Jygs = null;
    public string   ss(string hszxdm,string jhfadm)
    {
        string sql = "";
        sql += " select   b.CPDM, '('+a.ZYMC+')'+b.CPMC CPMC,SUM(c.CPCLL) CL,'合计' LXMC,NULL ZFL, a.ZYBM,b.CPBM, 0 CE,1 BS ";
        sql += "       from TB_JHZYML a,TB_JHCPBM b,TB_JHZYCC c ";
        sql += " where c.JHFADM='10050157' AND c.HSZXDM='10070003'  ";
        sql += "           and a.ZYDM=b.ZYDM   and b.CPDM=c.CCCPDM  ";
        sql += "          and b.CPBS=1  ";
        sql += "           GROUP BY b.CPDM,a.ZYMC,b.CPBS, a.ZYBM,b.CPBM ";
        sql += "          union    ";
        sql += "          select  b.CPDM, '('+a.ZYMC+')'+b.CPMC ,0.00 CL  ,'合计' LXMC,NULL ZFL,a.ZYBM, b.CPBM ,  0 CE,1 BS ";
        sql += "           from TB_JHZYML a,TB_JHCPBM b    ";
        sql += "           where a.ZYDM=b.ZYDM   and b.CPBS=1 AND b.HSZXDM='10070003' and  ";
        sql += "           b.CPDM in(select distinct  TRCPDM from TB_JHZYTR where  JHFADM='10050157' AND  ";
        sql += "HSZXDM='10070003'and isnull(FAZCLL,0)<>0)  ";
        sql += "   and b.CPDM not in(select distinct CCCPDM from TB_JHZYCC where HSZXDM='10070003'AND    ";
        sql += "JHFADM='10050157' )  ";
        sql += "        GROUP BY b.CPDM,a.ZYMC,b.CPBS, a.ZYBM,b.CPBM ";
        sql += " UNION ";
        sql += " select   b.CPDM,b.CPMC  CPMC,ISNULL(c.NGCL,0)+ISNULL(ZCCL,0) CL,'合计' LXMC,NULL ZFL,  '3' ZYBM,b.CPBM, 0 CE,1 BS ";
        sql += "   from  V_JHTHPDM  b,TB_JHBCPQCQM  c ";
        sql += " where c.JHFADM='10050157' AND c.HSZXDM='10070003' ";
        sql += "               and b.CPDM=c.ZZBCPDM  ";
        sql += "          and b.CPBS=3  ";
        sql += " UNION ";
        sql += " SELECT A.TRCPDM,'' CPMC,NULL CL,C.ZYMC LXMC,SUM(FAZCLL) ZFL,D.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZYTR A,TB_JHCPBM B,TB_JHZYML C,TB_JHZYML D ";
        sql += " WHERE A.JHFADM='10050157' AND A.ZYDM=C.ZYDM AND B.ZYDM=D.ZYDM  ";
        sql += " AND  A.TRCPDM=B.CPDM  AND A.HSZXDM='10070003' ";
        sql += "  GROUP BY A.TRCPDM,C.ZYMC,D.ZYBM,B.CPBM ";
        sql += " UNION ";
        sql += " SELECT A.TRCPDM,'' CPMC,NULL CL,C.ZYMC LXMC,SUM(FAZCLL) ZFL,'3' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZYTR A,TB_JHCPBM B,TB_JHZYML C ";
        sql += " WHERE A.JHFADM='10050157' AND A.ZYDM=C.ZYDM AND B.CPBS=3 ";
        sql += " AND A.TRCPDM=B.CPDM AND A.HSZXDM='10070003' ";
        sql += " GROUP BY A.TRCPDM,C.ZYMC,B.CPBM ";
        sql += "UNION ";
        sql += "SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(半成品)' LXMC,ZFL ZFL,C.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZBCPTHGC A,TB_JHCPBM B,TB_JHZYML C,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM AND B.ZYDM=C.ZYDM ";
        sql += " AND A.JHFADM='10050157' ";
        sql += " UNION ";
        sql += "  SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(半成品)' LXMC,ZFL ZFL,'3' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZBCPTHGC A,TB_JHCPBM B,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM    AND B.CPBS=3 ";
        sql += " AND A.JHFADM='10050157'  ";
        sql += " UNION ";
        sql += " SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(产成品)' LXMC,ZFL ZFL,C.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZCPTHGC A,TB_JHCPBM B,TB_JHZYML C,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM AND B.ZYDM=C.ZYDM  ";
        sql += " AND A.JHFADM='10050157'  ";
        sql += " UNION ";
        sql += " SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(产成品)' LXMC,ZFL ZFL,'5' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZCPTHGC A,TB_JHCPBM B,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM    AND B.CPBS=5 ";
        sql += " AND A.JHFADM='10050157'  ";
        sql += " ORDER BY  ZYBM,CPBM ,CPMC DESC ";
        TB_ZDSXBBLL bll=new TB_ZDSXBBLL();
        DataSet dst = bll.Query(sql);
        DataRow[] row = dst.Tables[0].Select("BS=1");
        DataTable dt=new DataTable();
        dt.Columns.Add("CPDM", Type.GetType("System.String"));
        dt.Columns.Add("CPMC", Type.GetType("System.String"));
        dt.Columns.Add("CL", Type.GetType("System.String"));
        dt.Columns.Add("LXMC", Type.GetType("System.String"));
        dt.Columns.Add("ZFL", Type.GetType("System.String"));
        dt.Columns.Add("BS", Type.GetType("System.String"));
        dt.Columns.Add("CE", Type.GetType("System.String"));
        string jygssm = "";
        for (int i = 0; i < row.Length; i++)
        {
            DataRow R1 = dt.NewRow();
            R1["CPDM"] = row[i]["CPDM"].ToString();
            R1["CPMC"] = row[i]["CPMC"].ToString();
            R1["CL"] = row[i]["CL"].ToString();
            R1["LXMC"] = row[i]["LXMC"].ToString();
            R1["BS"] = row[i]["BS"].ToString();
            DataRow[]  rows = dst.Tables[0].Select("BS=" + 0 + " AND CPDM='" + R1["CPDM"] + "'");
            double SumZfl = 0;
            for (int j = 0; j < rows.Length; j++)
            {
                if (rows[j]["ZFL"].ToString() == "" || rows[j]["ZFL"] == null)
                    rows[j]["ZFL"] = "0";
                 SumZfl= SumZfl + Convert.ToDouble(rows[j]["ZFL"].ToString());
            }
            R1["ZFL"] = SumZfl;
            if (R1["CL"].ToString() == "" || R1["CL"] == null)
                R1["CL"] = "0";
            double ce = Convert.ToDouble(R1["CL"]) - SumZfl;
            if (ce != 0)
            {
                jygssm += R1["LXMC"]+"\n";
            }
            R1["CE"] = ce;
            dt.Rows.Add(R1);
            for (int j = 0; j < rows.Length;j++ )
            {
                DataRow R = dt.NewRow();
                R["CPDM"] = rows[j]["CPDM"].ToString();
                R["CPMC"] = rows[j]["CPMC"].ToString();
                R["CL"] = rows[j]["CL"].ToString();
                R["LXMC"] = rows[j]["LXMC"].ToString();
                R["ZFL"] = rows[j]["ZFL"].ToString();
                R["CE"] = rows[j]["CE"].ToString();
                R["BS"] = rows[j]["BS"].ToString();
                dt.Rows.Add(R);
            }
        }
        Jygs = new DataSet();
        Jygs.Tables.Add(dt);
        return jygssm;
    }
   
  
    /// <summary>
    /// 公式取值
    /// </summary>
    /// <param name="gsss">公式</param>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <returns></returns>
    public static object qz(string gsss, string YY, string NN, string FADM1, string FADM2, string FABS)
    {
        gsss = gsss.Replace("S_NN", Convert.ToInt32(NN).ToString());
        object value;
        string sql = " select * from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = "0";
        string gs = gsss;
        gsss = gsss.Trim();
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gsss.Contains(gygss))
            {
                do
                {
                    int index_i = gsss.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString());
                    int end2 = gsss.IndexOf(")", index_i);
                    string gss = gsss.Substring(index_i, end2 - index_i + 1);
                    fhz = NewMethod(gss, YY, NN, FADM1, FADM2, FABS);
                    if (fhz.IndexOf("错误公式") > -1)
                    {
                        gs = gs.Replace(":", "\\:").Replace(",", "\\,").Replace("\"", "\\\"");
                        gsss = fhz + gs;
                        break;
                    }
                    gsss = gsss.Replace(gss, fhz.ToString());

                }
                while (gsss.Contains(gygss));
            }
            if (gsss.IndexOf("错误公式") > -1)
            {
                break;
            }

        }
        value = gsss;
        return value;
    }


    /// <summary>
    /// 求值
    /// </summary>
    /// <param name="gs">公式</param>
    /// <param name="hszqdm">核算周期</param>
    /// <param name="time">时间</param>
    /// <param name="type">Yy~2015~Ysyf~5~Jhfadm~10050577~Mb~test.</param>
    /// <param name="thzd">要和界面的条件替换的值，值之间以|分开</param>
    /// <returns></returns>
    public static string NewMethod(string gs, string YY, string NN, string FADM1, string FADM2, string FABS)
    {
        string fhzhi = "0";
        try
        {
            string login_YY = "2016";
            //string login_YY = HttpContext.Current.Request.Cookies["RMYH_LOGIN"].Values.Get("YY").ToString();

            int i = gs.IndexOf("(");
            string zhi = string.Empty;
            string _gs = string.Empty;
            string mbdm = "";
            string sqljhfa = "";
            string sqlzq = "";
            string sfzf = "";
            DataSet setzq2 = null;
            if (i > -1)
            {

                _gs = gs.Substring(0, i);
                string sql = " select * from REPORT_CSSZ_SJY  where Upper(DYGS)='" + _gs + "'";
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet set = bll.Query(sql);
                string bid = set.Tables[0].Rows[0]["BID"].ToString();
                _gs = gs.Substring(i + 1);
                int j = _gs.LastIndexOf(",");
                string zq = _gs.Substring(j);
                //周期
                zq = zq.Replace(")", "").Replace('"', ' ').Replace(",", "").Trim();
                _gs = _gs.Substring(0, j);
                //_gs = _gs.Replace('"', ' ').Trim();
                j = _gs.LastIndexOf(",");
                string zifu = _gs.Substring(j + 1);
                //查询字段
                zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
                j = _gs.LastIndexOf(",");
                _gs = _gs.Substring(0, j).Replace('"', ' ');
                string[] tjzhi = _gs.Split(',');
                string sjyid = set.Tables[0].Rows[0]["sjyid"].ToString();
                string sql3 = string.Empty;
                //求条件值
                string sql2 = "select * from REPORT_CSSZ_SJYDYXTJ a where SJYID='" + sjyid + "' order by XH";
                DataSet set2 = bll.Query(sql2);
                if (set2.Tables[0].Rows.Count == 0)
                    return "错误公式-";
                if (sjyid == "2")
                {
                    //如果是单耗，单价，单位成本不能SUM
                    if (zifu == "DJ" || zifu == "DH" || zifu == "DWCB" || zifu == "CWXGDJ" || zifu == "CWXGDH")
                    {
                        //sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  and STATE='1' AND JSDX='" + zifu + "' ";
                        sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                    }
                    else
                    {
                        string SQLSJLX = "SELECT SJLX FROM TB_JSDX WHERE ZFBM='" + zifu + "' ";
                        DataSet SET = bll.Query(SQLSJLX);
                        if (SET.Tables[0].Rows.Count > 0)
                        {
                            string jsdx = SET.Tables[0].Rows[0][0].ToString();
                            if (jsdx == "6")
                            {
                                sql3 = "select ZFVALUE from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                                sfzf = "@";
                            }
                            else
                            {
                                sql3 = "select ISNULL(sum(VALUE),0) from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                            }
                        }

                    }
                }
                else
                {
                    sql3 = "select ISNULL(sum(" + zifu + "),0) from " + bid + " where 1=1   ";
                }
                for (i = 0; i < tjzhi.Length; i++)
                {
                    sql3 += " ";
                    if (tjzhi[i].ToString().Trim() == "" && set2.Tables[0].Rows[i]["tjdm"].ToString() != "TRLDM")
                    {
                        sql3 += " ";
                    }
                    else
                    {
                        if (sjyid == "2")
                        {

                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "XMDM")
                            {
                                string sqlfl = " SELECT XMLX FROM V_XMZD WHERE XMDM in(" + zhi + ") ";
                                DataSet dsd = bll.Query(sqlfl);
                                if (dsd.Tables[0].Rows.Count == 0)
                                {
                                    return "错误公式-";
                                }
                                else
                                {
                                    if (dsd.Tables[0].Rows[0]["XMLX"].ToString() == "13")
                                    {
                                        sql3 += " AND XMDM IN(SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM IN(" + zhi + "))";
                                    }
                                    else
                                    {
                                        sql3 += " AND XMDM IN (" + zhi + ") ";
                                    }
                                }
                            }
                            else if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "MBDM")
                            {
                                int z = 0;
                                string pdz = "";
                                if (zhi.IndexOf(',') - 2 < 0 || zhi.IndexOf(',') - 2 == 0)
                                {
                                    z = 0;
                                    pdz = zhi.Replace("'", "");
                                }
                                else
                                {
                                    z = zhi.IndexOf(',') - 2;
                                    pdz = zhi.Replace("'", "").Substring(0, z);
                                }
                                if (Convert.ToInt32(pdz) < 10)
                                {
                                    sql3 += " AND MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";

                                }
                                else
                                {
                                    sql3 += " AND MBDM IN (" + zhi + ") ";
                                }
                                mbdm = zhi;
                            }
                            else
                            {
                                zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                                sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";

                            }
                        }
                        else if (sjyid == "1")
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "%'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "like" + "" + zhi + "";
                        }
                        else
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";

                        }
                    }

                }
                string fa = FADM1;
                string sj = zq;
                if (sjyid != "1" && sjyid != "2")
                {
                    string SQL = "";
                    //计划标准成本
                    if (zq == "JHBZ")
                    {
                        SQL = "SELECT JHFADM  FROM TB_JHFA WHERE YSBS='0' AND YY='" + YY + "' and NN='" + NN + "' AND FABS='" + FABS + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "SJBZ")
                    {
                        SQL = "SELECT JHFADM FROM TB_JHFA WHERE YSBS='1' AND FABS='" + FABS + "' AND YY='" + YY + "' AND NN='" + NN + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "DQFA")
                    {
                        sql3 += " AND JHFADM='" + FADM1 + "'";
                    }
                    else if (zq == "DQFA2")
                    {
                        sql3 += " AND JHFADM='" + FADM2 + "'";
                    }

                    //DataSet SetZ = bll.Query(sql3);
                    //if (SetZ.Tables[0].Rows.Count > 0)
                    //{
                    //    fhzhi = double.Parse(SetZ.Tables[0].Rows[0][0].ToString());
                    //}
                    //else
                    //{
                    //    fhzhi = 0;
                    //}
                }
                else if (sjyid == "1")
                {
                    //本年累计
                    if (zq == "BNLJ")
                    {
                        sql3 += " and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //本期发生
                    else if (zq == "BYLJ")
                    {
                        sql3 += " And YYNN=" + YY + NN + " ";
                    }
                    //上期发生
                    else if (zq == "SYLJ")
                    {
                        //And YYNN=当前年+主界面传的预算实际月份
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(YY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(YY);
                            int ys = int.Parse(NN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }

                        sql3 += "and YYNN=" + yys + months + "";
                    }
                    //上年同期
                    else if (zq == "SNTQ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN='" + SNYY.ToString() + NN + "'";
                    }
                    //上年累计
                    else if (zq == "SNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY.ToString() + "01 and YYNN<=" + SNYY.ToString() + NN + " ";
                    }
                    //本期之前累计
                    else if (zq == "BQZQLJ")
                    {
                        sql3 += "and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //上年全年累计
                    else if (zq == "SNQNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY + "01 and YYNN<=" + SNYY + "12 ";
                    }
                    else if (zq == "BNQC")
                    {
                        sql3 += "and YYNN=" + YY + "01 ";
                    }
                    else if (zq == "01")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "02")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "03")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "04")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "05")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "06")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "07")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "08")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "09")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "10")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "11")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "12")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }

                }
                else if (sjyid == "2")
                {
                    //本年分解预算
                    if (zq == "BNFJYS")
                    {
                        string sqlBNFJYS = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='5' AND DELBZ='1' AND YY='" + login_YY + "' AND FABS='3'";
                        DataSet SETBNFJYS = bll.Query(sqlBNFJYS);
                        if (SETBNFJYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNFJYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "XBNYJ")// –本年批复预算
                    {
                        string sqlBNPFYS = " SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' AND YY='" + login_YY + "' AND FABS='3'";
                        DataSet SETBNPFYS = bll.Query(sqlBNPFYS);
                        if (SETBNPFYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNPFYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    //预算申请
                    else if (zq == "YSSQ")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='3' AND DELBZ='1' and FABS='3' AND YY='" + YY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }

                    }
                    //批复预算
                    else if (zq == "YSPF")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' and FABS='3' AND YY='" + YY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "FA2")
                    {
                        fa = FADM2;
                        sj = "FA1";
                    }
                    //else
                    //{
                    //int z = 0;
                    //string pdz = "";
                    //if (zhi.IndexOf(',') - 2 < 0 || zhi.IndexOf(',') - 2 == 0)
                    //{
                    //    z = 0;
                    //    pdz = zhi.Replace("'", "");
                    //}
                    //else
                    //{
                    //    z = zhi.IndexOf(',') - 2;
                    //    pdz = zhi.Replace("'", "").Substring(0, z);
                    //}
                    //if (Convert.ToInt32(pdz) < 10)
                    //{
                    //    sqlzq = "SELECT distinct SJDX FROM TB_MBCOLSX WHERE MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + mbdm + ")) AND SJDX='" + zq + "'";
                    //}
                    //else
                    //{
                    //    sqlzq = "SELECT distinct SJDX FROM TB_MBCOLSX WHERE MBDM IN(" + mbdm + ") AND SJDX='" + zq + "'";
                    //}
                    //setzq2 = bll.Query(sqlzq);
                    //if (setzq2.Tables[0].Rows.Count > 0)
                    //{
                    //    sj = setzq2.Tables[0].Rows[0]["SJDX"].ToString();
                    //}
                    //else
                    //{
                    //    sj = "FA1";
                    //}
                    //}
                    sql3 += "and JHFADM='" + fa + "' and SJDX='" + sj + "' ";
                }
                DataSet SetZ = bll.Query(sql3);
                if (SetZ.Tables[0].Rows.Count > 0)
                {
                    fhzhi = SetZ.Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    fhzhi = "0";
                }
                if (sfzf != "")
                {
                    fhzhi = fhzhi + "@";
                }

            }
        }
        catch (Exception EE)
        {
            throw EE;
        }

        return fhzhi;
    }
}
       



