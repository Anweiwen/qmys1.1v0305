﻿//*********************************************************//
//** 名称：编辑EXCEL模板
//** 功能: 编辑EXCEL模板
//** 开发日期: 2011-11-10  开发人员：余妮
//** 表:TB_EXCEL           EXCEL模板
//*********************************************************//
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class SIS_EditExcel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SIS_EditExcel));
        hidType.Value = Request.QueryString["type"];
        hidID.Value = Request.QueryString["id"];
        if (!IsPostBack)
        {
            if (hidType.Value == "update")
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("select name from TB_EXCEL where id=" + hidID.Value + "");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xname.Value = ds.Tables[0].Rows[0][0].ToString();
                }
            }
        }
    }

    #region Ajax方法;
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string Name)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        bool f = false;
        int ID = int.Parse(bll.GetSeed("TB_EXCEL", "ID").ToString());
        string result = bll.Query("insert into TB_EXCEL (ID,NAME) values (" + ID + ",'" + Name + "')").ToString();
        if (result != "")
        {
            f = true;
        }
        else
        {
            f = false;
        }
        return f + "," + ID.ToString();
    }

    /// <summary>
    /// 更新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool UpdateData(int ID, string Name)
    {
       TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        bool f = false;
        string result = bll.Query("update TB_EXCEL set name='" + Name + "' where id=" + ID + "").ToString();
        if (result != "")
        {
            f = true; 
        }
        else
        {
            f = false;
        }
        return f;
    }
    #endregion;
}