﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tj2.aspx.cs" Inherits="Excels_ExcelMb_tj2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <base target="_self" />
   <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
    <link href="../../Content/themes/icon.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" type="text/css" href="../../CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style.css" />
    <script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <script src="../../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../../JS/jquery.zclip.js" type="text/javascript"></script>
    <title></title>
    <script>
        function jsonTree() {
            var rtn = Excels_ExcelMb_tj2.GetChildJson("0", $("#<%=hidSQL.ClientID %>").val());
            var dj = null;
            $('#tt').tree({
                lines: true,
                checkbox: true,
                cascadeCheck: false,
                onBeforeExpand: function (node) {
                    var childrens = $('#tt').tree('getChildren', node.target);

                    if (childrens == false) {

                        var res = Excels_ExcelMb_tj2.GetChildJson(node.id, $("#<%=hidSQL.ClientID %>").val());
                        var Data = eval("(" + res.value + ")");
                        //var selected = $('#tt').tree('getSelected');
                        $('#tt').tree('append', {
                            parent: node.target,
                            data: Data
                        });
                    }
                },
                onExpand: function (node) {

                },
                onCollapse: function (node) {

                },
                onClick: function (node) {

                },
                onCheck: function (node, checked) {
                    //只有当节点被选中时，判断当前结点的父辈节点、子节点没有被选中
                    if (checked == true) {
                        //判断其子节点没有被选中的，如果有的话，则父节点和子节点不能同时被选中。
                        var c = RecursionChild(node);
                        if (c == false) {
                            alert("父节点和子节点不能同时被选中!");
                            $("#tt").tree("uncheck", node.target);
                        }

                        //如果是支节点，则判断其父节点没有被选中的，如果有，则该支节点和父节点不能同时选中
                        var Flag = RecursionPar(node);
                        if (Flag == false) {
                            alert("父节点和子节点不能同时被选中!");
                            $("#tt").tree("uncheck", node.target);
                            return;
                        }

                    }
                }

            });
            if (rtn.value != "") {
                var Data = eval("(" + rtn.value + ")");
                $("#tt").tree("loadData", Data);
            }
        }
        
        //递归当前结点的父辈结点有没有被选中的
        //返回true表示没有被选中的；false表示有被选中的父辈节点
        function RecursionPar(node) {
            if (node.attributes.par != "0") {
                var n = $('#tt').tree("find",parseInt(node.attributes.par));
                if (n.checked == true) {
                    return false;
                }
                else {
                    var p = $('#tt').tree("find",parseInt(node.attributes.par));
                    var Flag = RecursionPar(p);
                    if (Flag == false) {
                        return false;
                    }
                }
                return true;
            }
            else {
                if (node.checked == true) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        //递归当前结点的子节点有没有被选中的
        //返回true表示没有被选中的；false表示有被选中的父辈节点
        function RecursionChild(node) {
            var c = node.children;
            if (c != undefined) {
                for (var i = 0; i < c.length; i++) {
                    if (c[i]._checked == true) {
                        return false;
                    }
                    else {

                        var Flag = RecursionChild(c[i]);
                        if (Flag == false) {
                            return false;
                        }

                    }
                }
                return true;
            }
            else {
                return true;
            }
        }

        function tc() {
            window.close();
        }
        function getChecked() {
            var sjyid = $("#<%=hidSJYID.ClientID %>").val();
            var nodes = $('#tt').tree('getChecked');
            if (sjyid == "1") {
                if (nodes.length > 1) {
                    alert("财务模板条件值只能一个，请重新选择");
                }
                else {
                    var s = "";
                    for (var i = 0; i < nodes.length; i++) {
                        s += nodes[i].id.replace(/(\s*$)/g, "") + "." + nodes[i].text.replace(/(\s*$)/g, "") + "|";
                    }
                    if (s != "") {
                        s = s.substring(0, s.length - 1);
                    }
                    window.returnValue = s;
                    window.close();
                }
            }
            else {
                var s = "";
                for (var i = 0; i < nodes.length; i++) {
                    s += nodes[i].id.replace(/(\s*$)/g, "") + "." + nodes[i].text.replace(/(\s*$)/g, "") + "|";
                }
                if (s != "") {
                    s = s.substring(0, s.length - 1);
                }
                window.returnValue = s;
                window.close();
            }
        }
            
        $(document).ready(function () {
            jsonTree();

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style=" height:400px; overflow:auto">
		<ul checkbox:true id="tt" class="easyui-tree"/> 
	</div>
    <div style="height:40px;width:100%">
        <table  >
        <tr>
        <td  style =" text-align :center "> 
            <input id="Button1" type="button" value="确定" class ="button5" onclick="getChecked()"/>   </td>
        <td  style =" text-align :center "> 
            <input id="Button2" type="button" value="取消" 　 onclick ="tc()" class ="button5"  />
           </td>
        <td>   <input id="hid" type="hidden"  runat ="server" /> </td>
        </tr>
        </table>
   </div>
    <input id="hidSQL" type="hidden"  runat="server"/>
    <input id="hidSJYID" type="hidden"  runat="server"/>
    </form>
</body>
</html>
