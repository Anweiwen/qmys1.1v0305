﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text;
using RMYH.BLL;
using System.Data.OleDb;
using Excel;
using RMYH.DBUtility;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Drawing;


public partial class ExcelMb_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(ExcelMb_Default));
        PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/Excels/pageoffice/server.aspx";
        PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
        string gsss = "JHZZCPCBGS(\"10070003\",\"10110169\",\"TRCB\",\"DQFA\")";
        JXGS("10160036", "2015", "11", "10050689", "10050727", "1","2016");
    }
  


    public static string JXGS(string MBDM, string YY, string NN, string FADM1, string FADM2, string FABS,string yy2)
    {

        string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID FROM dbo.TB_MBJYGS T where MBDM='" + MBDM + "'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet set = BLL.Query(sql);
        sql = "SELECT A.MBDM,A.MBLX,A.MBMC,A.ZYDM,A.MBWJ,A.MBZQ,A.MBBM,A.MBLX FROM TB_YSBBMB A WHERE MBDM='" + MBDM + "'";
        DataSet mbmcset = BLL.Query(sql);
        sql = " select YSBS from  TB_JHFA WHERE JHFADM='" + FADM1 + "' ";
        DataSet setBs = BLL.Query(sql);
        sql = "SELECT A.XMDM,B.XMMC,MBDM,GS,VALUE,WCFW FROM TB_ZBXD A,V_XMZD B WHERE A.XMDM=B.XMDM  AND A.YY='" + YY + "' AND MBDM='" + MBDM + "'";
        DataSet setJY = BLL.Query(sql);
        string YSBS = "";
        if (setBs.Tables[0].Rows.Count > 0)
        {
            YSBS = setBs.Tables[0].Rows[0]["YSBS"].ToString();
        }
        string mbmc = "";
        string yzjg = "";
        string mblx = "";
        if (mbmcset.Tables[0].Rows.Count > 0)
        {
            mbmc = mbmcset.Tables[0].Rows[0]["MBWJ"].ToString();
            mblx = mbmcset.Tables[0].Rows[0]["MBLX"].ToString();
        }
        if (set.Tables[0].Rows.Count > 0)
        {
            string gsjx = "";
            string gs = "";
            string INFOMATION = "";
            string sheet = "";
            ArrayList listYzjg = new ArrayList();
            Excel.Application app = new Excel.Application();
            object Unknown = System.Type.Missing;
            string filename = HttpContext.Current.Server.MapPath("../XlS/" + mbmc);
            Excel.Workbook rwb = app.Workbooks.Open(filename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
            Excel.Worksheet rs = rwb.Worksheets[1] as Excel.Worksheet;
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                gsjx = set.Tables[0].Rows[i]["GSJX"].ToString();
                gs = set.Tables[0].Rows[i]["GS"].ToString();
                INFOMATION = set.Tables[0].Rows[i]["INFOMATION"].ToString();
                sheet = set.Tables[0].Rows[i]["SHEETNAME"].ToString();
                gs = GetDataList.qz(gs, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
                ((Excel.Range)rs.Cells[1, 1]).Formula = "=" + gs;
                object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
                string jg = dell[1, 1].ToString();
                if (jg == "False")
                {
                    listYzjg.Add(gsjx + INFOMATION + "\n");
                }
                ((Excel.Range)rs.Cells[1, 1]).Formula = null;
            }

            if (mblx == "4" && YSBS == "4" && setJY.Tables[0].Rows.Count > 0)
            {
                string value = "";
                string GS = "";
                string WCFW = "";
                for (int i = 0; i < setJY.Tables[0].Rows.Count; i++)
                {
                    value = setJY.Tables[0].Rows[i]["VALUE"].ToString();
                    if (value == "")
                        value = "0";
                    GS = setJY.Tables[0].Rows[i]["GS"].ToString();
                    if (GS == "")
                        GS = "0";
                    WCFW = setJY.Tables[0].Rows[i]["WCFW"].ToString();
                    if (WCFW == "")
                        WCFW = "0";
                    GS = GetDataList.qz(GS, YY, NN, FADM1, FADM2, FABS,yy2).ToString();
                    if (System.Math.Abs(decimal.Parse(GS) - decimal.Parse(value)) > decimal.Parse(WCFW))
                    {
                        listYzjg.Add("总部下达指标" + setJY.Tables[0].Rows[i]["XXMC"].ToString() + "超过指标值,请修改");
                    }
                }
            }
            yzjg = string.Concat(listYzjg.ToArray());

            int generation = 0;
            app.DisplayAlerts = false;
            app.Quit();
            generation = System.GC.GetGeneration(app);
            app = null;
            System.GC.Collect(generation);

        }
        return yzjg;

    }
}