﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class word : System.Web.UI.Page
{
    protected string content = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //定义table对象，设置table对象的设置范围

        if (PageOfficeCtrl1.ServerPage == "")
        {
            //设置PageOfficeCtrl控件的服务页面
            PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/pageoffice/server.aspx";
            // 设置文件打开后执行的js function
            PageOfficeCtrl1.JsFunction_AfterDocumentOpened = "AfterDocumentOpened()";
            PageOfficeCtrl1.AddCustomToolButton("保存", "Save()", 1);

            //定义Workbook对象
            PageOffice.ExcelWriter.Workbook workBook = new PageOffice.ExcelWriter.Workbook();
            //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
            PageOffice.ExcelWriter.Sheet sheet = workBook.OpenSheet("Sheet1");
            PageOffice.ExcelWriter.Table table1 = sheet.OpenTable("B1:B4");
            //设置table对象的提交名称，以便保存页面获取提交的数据
            table1.SubmitName = "Info";
            PageOfficeCtrl1.SetWriter(workBook);
            PageOfficeCtrl1.SaveDataPage = "Excel.aspx";
            PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
            //PageOfficeCtrl1.SaveFilePage = "SaveFile1.aspx";
            PageOfficeCtrl1.WebOpen("doc/test.xls", PageOffice.OpenModeType.xlsSubmitForm, "操作人姓名");
        }
        else
        {
            //fs.SaveToFile(Server.MapPath("doc/") + fs.FileName);
            //fs.Close();

            PageOffice.ExcelReader.Workbook workBook = new PageOffice.ExcelReader.Workbook();
            PageOffice.ExcelReader.Sheet sheet = workBook.OpenSheet("Sheet1");
            PageOffice.ExcelReader.Table table = sheet.OpenTable("Info");
            int count = table.RowCount;

            int result = 0;
            while (!table.EOF)
            {
                //DataFields.Count标识的是提交过来的table的列数
                if (!table.DataFields.IsEmpty)
                {
                    content += "<br/>月份名称：" + table.DataFields[0].Text;
                    content += "<br/>计划完成量：" + table.DataFields[1].Text;
                    content += "<br/>实际完成量：" + table.DataFields[2].Text;
                    content += "<br/>累计完成量：" + table.DataFields[3].Text;
                    if (string.IsNullOrEmpty(table.DataFields[2].Text) || !int.TryParse(table.DataFields[2].Text, out result) ||
                        !int.TryParse(table.DataFields[1].Text, out result))
                    {
                        content += "<br/>完成率：0";
                    }
                    else
                    {
                        float f = int.Parse(table.DataFields[2].Text);
                        f = f / int.Parse(table.DataFields[1].Text);
                        content += "<br/>完成率：" + string.Format("{0:P}", f);
                    }
                    content += "<br/>*********************************************";
                }
                //循环进入下一行
                table.NextRow();
            }
            table.Close();

            workBook.ShowPage(500, 400);
            workBook.Close();
        
        }
    }
    
}
