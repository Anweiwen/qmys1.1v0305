﻿<%@ WebHandler Language="C#" Class="InitMBHandle" %>

using System;
using System.Web;
using RMYH.BLL;
using System.Data;
using System.Text;
using System.Web.SessionState;
using System.Collections.Generic;


public class InitMBHandle : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string action = context.Request.Params["action"];
        string FADM = context.Request.Params["FADM"];
        string MBDM = context.Request.Params["MBDM"];
        string MBLB = context.Request.Params["MBLB"];
        string MBLX = context.Request.Params["MBLX"];
        string ISEXE = context.Request.Params["ISEXE"];
        string SFZLC = context.Request.Params["SFZLC"];
        string USERDM = context.Request.Params["USERDM"];
        string HSZXDM = context.Request.Params["HSZXDM"];

        if (action == "getgsyymm")
        {
            string GSYY = "", GSMM = "";

            string FALB = context.Request.Params["FALB"];
            string YSYF = context.Request.Params["YSYF"];
            string YY = context.Request.Params["YY"];
            //根据方案和年月判断方案传入的年月：
            GetGsYearMonth(FADM, YY, FALB, YSYF, ref GSYY, ref GSMM);
            context.Response.Write(GSYY + "," + GSMM);
        }
        else if (action == "GetMBQX")
        {
            int IMBLB = 1;
            string rtn = "";

            if (MBLB == "basesb" || MBLB == "resosb" || MBLB == "respys")
            {
                IMBLB = 1;
            }
            else
            {
                IMBLB = 2;
            }

            //如果是模版流程查询则不考虑权限问题：
            if (MBLB.Equals("queryflow"))
            {
                rtn += "\"HQZXMB\":1";
                rtn += ",";
                rtn += "\"SCSJ\":1";
                rtn += ",";
                rtn += "\"ZJSJ\":1";
                rtn += ",";
                rtn += "\"JSBB\":1";
                rtn += ",";
                rtn += "\"QQYJS\":1";
                rtn += ",";
                rtn += "\"HQHLSX\":0";
                rtn += ",";
                rtn += "\"BC\":0";
                rtn += ",";
                rtn += "\"BDDK\":1";
                rtn += ",";
                rtn += "\"BDLC\":1";
                rtn += ",";
                rtn += "\"SB\":0";
                rtn += ",";
                rtn += "\"SP\":0";
                rtn += ",";
                rtn += "\"DH\":0";
                rtn += ",";
                rtn += "\"DY\":0";
            }
            else
            {
                TB_YSLCBLL LC = new TB_YSLCBLL();
                string lcdm = context.Request.Params["LCDM"];
                //获取当前操作人的按钮权限
                DataTable DT = LC.GetMBCZQX(MBDM, IMBLB, ISEXE, FADM, MBLB, SFZLC, USERDM, HSZXDM);
                if (DT.Rows.Count > 0)
                {
                    if (DT.Rows[0]["XGQX"].ToString() != "0")
                    {
                        if (MBLX != "1")
                        {
                            rtn += "\"HQZXMB\":1";
                            rtn += ",";
                            rtn += "\"SCSJ\":0";
                            rtn += ",";
                            rtn += "\"ZJSJ\":0";
                            rtn += ",";
                        }
                        else
                        {
                            rtn += "\"HQZXMB\":0";
                            rtn += ",";
                            rtn += "\"SCSJ\":1";
                            rtn += ",";
                            rtn += "\"ZJSJ\":1";
                            rtn += ",";
                        }
                    }
                    rtn += "\"JSBB\":1";
                    rtn += ",";
                    rtn += "\"QQYJS\":1";
                    rtn += ",";
                    rtn += "\"HQHLSX\":1";
                    rtn += ",";
                    rtn += "\"BDDK\":1";
                    rtn += ",";
                    rtn += "\"BDLC\":1";
                    //如果有效期：
                    if (DT.Rows[0]["XZTBBZ"].ToString() != "0")
                    {
                        if (DT.Rows[0]["XGQX"].ToString() != "0")
                        {
                            rtn += ",";
                            rtn += "\"BC\":1";
                        }
                        else
                        {
                            rtn += ",";
                            rtn += "\"BC\":0";
                        }

                        // 此处筛选是否有相应权限：
                        if ((DT.Rows[0]["SBQX"].ToString() != "0") && !(MBLB.Equals("respfj") && SFZLC.Equals("0")))
                        {
                            rtn += ",";
                            rtn += "\"SB\":1";
                        }
                        else
                        {
                            rtn += ",";
                            rtn += "\"SB\":0";
                        }
                        if (DT.Rows[0]["SPQX"].ToString() != "0")
                        {
                            //填报模块下不存在审批和打回功能，即使有相应权限也要去审批模块下完成
                            if (MBLB != "basesb" && MBLB != "resosb" && MBLB != "respys" && MBLB != "respfj")
                            {
                                rtn += ",";
                                rtn += "\"SP\":1";
                            }
                            else
                            {
                                rtn += ",";
                                rtn += "\"SP\":0";
                            }
                        }
                        else
                        {
                            rtn += ",";
                            rtn += "\"SP\":0";
                        }

                        if (DT.Rows[0]["RETURNQX"].ToString() != "0")
                        {
                            //填报模块下不存在审批和打回功能，即使有相应权限也要去审批模块下完成
                            if (MBLB != "basesb" && MBLB != "resosb" && MBLB != "respys" && MBLB != "respfj")
                            {
                                rtn += ",";
                                rtn += "\"DH\":1";
                            }
                            else
                            {
                                rtn += ",";
                                rtn += "\"DH\":0";
                            }
                        }
                        else
                        {
                            rtn += ",";
                            rtn += "\"DH\":0";
                        }
                        //过期标识（1代表为过期；0代表过期）
                        rtn += ",";
                        rtn += "\"GQ\":1";

                    }
                    //过期标记：
                    else
                    {
                        rtn += ",";
                        rtn += "\"BC\":0";
                        rtn += ",";
                        rtn += "\"SB\":0";
                        rtn += ",";
                        rtn += "\"SP\":0";
                        rtn += ",";
                        rtn += "\"DH\":0";
                        rtn += ",";
                        rtn += "\"GQ\":0";
                    }
                    if (DT.Rows[0]["PRINTQX"].ToString() != "0")
                    {
                        rtn += ",";
                        rtn += "\"DY\":1";
                    }
                    else
                    {
                        rtn += ",";
                        rtn += "\"DY\":0";
                    }
                }
                else
                {
                    rtn += "\"HQZXMB\":1";
                    rtn += ",";
                    rtn += "\"SCSJ\":1";
                    rtn += ",";
                    rtn += "\"ZJSJ\":1";
                    rtn += ",";
                    rtn += "\"JSBB\":1";
                    rtn += ",";
                    rtn += "\"QQYJS\":1";
                    rtn += ",";
                    rtn += "\"HQHLSX\":1";
                    rtn += ",";
                    rtn += "\"BC\":1";
                    rtn += ",";
                    rtn += "\"BDDK\":1";
                    rtn += ",";
                    rtn += "\"BDLC\":1";
                    rtn += ",";
                    rtn += "\"SB\":1";
                    rtn += ",";
                    rtn += "\"SP\":1";
                    rtn += ",";
                    rtn += "\"DH\":1";
                    rtn += ",";
                    rtn += "\"DY\":1";
                }
            }
            rtn += ",";
            rtn += "\"GBMB\":1";
            rtn += ",";
            rtn += "\"INSERTBDH\":1";
            rtn += ",";
            rtn += "\"DELBDH\":1";
            context.Response.Write(rtn);
        }
        else if (action == "GetList")
        {
            string jhfadm = context.Request.Params["JHFADM"];
            BLL = new TB_ZDSXBBLL();
            string flag = BLL.GetDHMB(MBDM, jhfadm, MBLB);
            context.Response.Write(flag);
        }
        else if (action == "GetCJBMByJHFADM")
        {
            string jhfadm = context.Request.Params["JHFADM"];
            BLL = new TB_ZDSXBBLL();
            string sql = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
            DataSet ds = BLL.Query(sql);
            string cj = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0]["CJBM"].ToString() : BLL.GetParCJBM(jhfadm, MBDM, MBLB);
            context.Response.Write(cj);
        }
        else if (action == "GetMBANDJS")
        {
            string cjbm = context.Request.Params["CJBM"];
            BLL = new TB_ZDSXBBLL();
            var str = BLL.GetMBANDJS(cjbm);
            context.Response.Write(str);
        }
        else if (action == "GetCJBM")
        {
            string jsdm = context.Request.Params["JSDM"];
            string cjbm = context.Request.Params["CJBM"];
            string jhfadm = context.Request.Params["JHFADM"];
            BLL = new TB_ZDSXBBLL();
            var cjStr = BLL.GetCJBM(jsdm, MBDM, cjbm, jhfadm, "");
            context.Response.Write(cjStr);
        }
        else if (action == "GetChird")
        {
            string cjbm = context.Request.Params["CJBM"];
            string jhfadm = context.Request.Params["JHFADM"];
            BLL = new TB_ZDSXBBLL();
            string cjStr = BLL.GetChird(cjbm, jhfadm, "");
            context.Response.Write(cjStr);
        }
        else if (action == "GetPar")
        {
            string cjbm = context.Request.Params["CJBM"];
            string jhfadm = context.Request.Params["JHFADM"];
            BLL = new TB_ZDSXBBLL();
            string cjStr = BLL.GetPar(cjbm, jhfadm, "");
            context.Response.Write(cjStr);
        }
        else if (action == "GetYDHMB")
        {
            string cj = context.Request.Params["CJ"];
            string jhfadm = context.Request.Params["JHFADM"];
            bool flag = false;
            string cjbm, mbzq = "";
            MBLX = "";
            BLL = new TB_ZDSXBBLL();
            DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + jhfadm + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                mbzq = DS.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
            }
            cjbm = BLL.GetDCLCJBM(jhfadm, "", mbzq, MBLX);
            if (cjbm != "")
            {
                cjbm = "'" + cjbm.Replace(",", "','") + "'";
                cj = "'" + cj + "'";
                if (cjbm.IndexOf(cj) > -1)
                {
                    flag = true;
                }
            }
            context.Response.Write(flag);
        }
        else if (action == "GetDHTree")
        {
            List<string> list = new List<string>();
            List<string> list2 = new List<string>();
            List<string> mblist = new List<string>();
            string LCDM = context.Request.Params["LCDM"];
            string JSDM = context.Request.Params["JSDM"];
            string JHFADM = context.Request.Params["JHFADM"];
            string DQMBDM = context.Request.Params["MBDM"];
            jsdms(ref list, ref mblist, JSDM, LCDM, HSZXDM, JHFADM, DQMBDM);
            string s = string.Join(",", list.ToArray());

            string rtns = s.Replace(",", "','");
            StringBuilder mbStr = new StringBuilder();
            StringBuilder faStr = new StringBuilder();
            string js = "select JSDM BM,NAME MC,'0' PAR FROM TB_JIAOSE WHERE JSDM IN ('" + rtns + "')";
            DataSet dsValue = BLL.Query(js);
            DataRow[] DR;
            string PAR = "0";
            //查询父节点为PAR的子节点
            DR = dsValue.Tables[0].Select("PAR='" + PAR + "'");
            mbStr.Append("{\"total\":");
            mbStr.Append(dsValue.Tables[0].Rows[0][0].ToString());
            mbStr.Append(",");
            mbStr.Append("\"rows\":[");
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                mbStr.Append("{");
                mbStr.Append("\"id\":");
                mbStr.Append("\"" + dr["BM"].ToString() + "\"");
                mbStr.Append(",");
                mbStr.Append("\"text\":");
                mbStr.Append("\"" + dr["MC"].ToString() + "\"");
                mbStr.Append(",");
                mbStr.Append("\"attributes\":");
                mbStr.Append("\"" + 0 + "\"");
                mbStr.Append(",");
                mbStr.Append("\"state\":");
                mbStr.Append("\"closed\"");
                mbStr.Append("}");
                if (i < DR.Length - 1)
                {
                    mbStr.Append(",");
                }
            }
            mbStr.Append("]}");
            list2.Add(faStr.ToString());
            list2.Add(mbStr.ToString());
            context.Response.Write(string.Join("", list2.ToArray()));
        }
        else if (action == "GetMB")
        {
            List<string> list = new List<string>();
            List<string> listjs = new List<string>();
            List<string> listmb = new List<string>();
            List<string> mbdmlist = new List<string>();
            StringBuilder mbStr = new StringBuilder();
            string JSDM = context.Request.Params["JSDM"];
            string LCDM = context.Request.Params["LCDM"];
            string JHFADM = context.Request.Params["JHFADM"];
            string THISJSDM = context.Request.Params["THISJSDM"];
            string DQMBDM = context.Request.Params["MBDM"];
            jsdms(ref listjs, ref mbdmlist, THISJSDM, LCDM, HSZXDM, JHFADM, DQMBDM);
            string dqmbmList = string.Join(",", mbdmlist.ToArray());
            dqmbmList = dqmbmList.Replace(",", "','");
            string s = "";
            //排出打回的模板
            string DhMb = " select DQMBDM FROM TB_YSLCSB where LCDM=" + int.Parse(LCDM) + " AND JSDM='" + JSDM + "' AND STATE=-1 AND JHFADM='" + JHFADM + "'";
            DataSet DsDh = BLL.Query(DhMb);
            if (DsDh.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DsDh.Tables[0].Rows.Count; i++)
                {
                    listmb.Add(DsDh.Tables[0].Rows[i][0].ToString());
                }
            }
            if (listmb.Count > 0)
            {
                s = string.Join(",", listmb.ToArray());
            }
            string mbdms = s.Replace(",", "','");
            string mbdm = "";
            mbdm = "select distinct A.MBDM BM,B.MBMC MC,'0' PAR FROM TB_YSJSXGMB A,TB_YSBBMB B,TB_YSLCSB LC";
            mbdm += " where A.MBDM=B.MBDM and A.JSDM=LC.JSDM AND A.LCDM=" + int.Parse(LCDM) + " AND A.JSDM='" + JSDM + "' ";
            mbdm += " AND LC.STATE=1 and LC.DQMBDM=A.MBDM AND A.LCDM=LC.LCDM AND JHFADM='" + JHFADM + "' and DQMBDM IN('" + dqmbmList + "') ";
            mbdm += " and DQMBDM NOT IN('" + mbdms + "')";

            DataSet dsValue = BLL.Query(mbdm);
            DataRow[] DR;
            string PAR = "0";
            //查询父节点为PAR的子节点
            DR = dsValue.Tables[0].Select("PAR='" + PAR + "'");
            if (DR.Length > 0)
            {
                mbStr.Append("{\"total\":");
                mbStr.Append(dsValue.Tables[0].Rows[0][0].ToString());
                mbStr.Append(",");
                mbStr.Append("\"rows\":[");
                for (int i = 0; i < DR.Length; i++)
                {
                    DataRow dr = DR[i];
                    mbStr.Append("{");
                    mbStr.Append("\"id\":");
                    mbStr.Append("\"" + dr["BM"].ToString() + "\"");
                    mbStr.Append(",");
                    mbStr.Append("\"text\":");
                    mbStr.Append("\"" + dr["MC"].ToString() + "\"");
                    mbStr.Append(",");
                    mbStr.Append("\"attributes\":");
                    mbStr.Append("\"" + 1 + "\"");
                    mbStr.Append("}");
                    if (i < DR.Length - 1)
                    {
                        mbStr.Append(",");
                    }
                }
            }
            mbStr.Append("]}");
            list.Add(mbStr.ToString());
            context.Response.Write(string.Join("", list.ToArray()));
        }
        //else if (action == "Childnode")
        //{
        //    List<string> list = new List<string>();
        //    string mbdm = context.Request.Params["MBDM"];
        //    string lcdm = context.Request.Params["LCDM"];
        //    string jhfadm = context.Request.Params["JHFADM"];
        //    string hszxdm = context.Request.Params["HSZXDM"];
        //    string sql = "SELECT SB.JSDM,DQMBDM FROM TB_YSLCSB SB,TB_YSBBMB MB,TB_YSJSXGMB YS WHERE MB.MBDM='" + mbdm + "'";
        //    sql += " and SB.STATE=1 AND SB.DQMBDM=MB.MBDM AND SB.LCDM=" + lcdm + " AND JHFADM='" + jhfadm + "' AND SB.HSZXDM='" + hszxdm + "'";
        //    sql += " AND SB.DQMBDM=YS.MBDM AND SB.LCDM=YS.LCDM AND YS.CZLX='0' AND SB.HSZXDM=YS.HSZXDM AND SB.JSDM=YS.JSDM";
        //    DataSet ds = BLL.Query(sql);
        //    list.Add(ds.Tables[0].Rows[0][0].ToString());
        //    list.Add(ds.Tables[0].Rows[0][1].ToString());
        //    context.Response.Write(string.Join(",", list.ToArray()));
        //}
        else if (action == "SP")
        {
            int Flag = 0;
            TB_YSLCBLL bll = new TB_YSLCBLL();
            string MB = context.Request.Params["MBDM"];
            string JSDM = context.Request.Params["JSDM"];
            string LCDM = context.Request.Params["LCDM"];
            string State = context.Request.Params["State"];
            string ACTIONRIGHT = context.Request.Params["ACTIONRIGHT"];
            string SPYJ = context.Request.Params["SPYJ"];
            Flag = bll.HQMB(JSDM, MB, State, int.Parse(LCDM), FADM, HSZXDM, ACTIONRIGHT, SPYJ);
            context.Response.Write(Flag);
        }
        else if (action == "AddSH")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string dhmb = context.Request.Params["DHMB"];
            string state = context.Request.Params["State"];
            string dhyy = context.Request.Params["DHYY"];
            string spyj = context.Request.Params["SPYJ"];
            int flag = bll.ZTTJ(int.Parse(state), MBDM, FADM, dhmb, MBLB, dhyy, spyj, SFZLC);
            context.Response.Write(flag);
        }
        else if (action == "GetCheckMb")
        {
            List<string> list = new List<string>();
            string mbdm = context.Request.Params["MBDM"];
            string lcdm = context.Request.Params["LCDM"];
            string jhfadm = context.Request.Params["JHFADM"];
            string FatherDm = context.Request.Params["FatherDm"];
            string sql = "SELECT  A.JSDM FROM TB_YSJSXGMB A,TB_YSLCSB B WHERE A.LCDM=" + lcdm + " AND A.MBDM='" + mbdm + "' AND B.JHFADM='" + jhfadm + "'";
            sql += " AND A.LCDM=B.LCDM AND A.MBDM=B.DQMBDM and B.STATE=1 AND A.JSDM=B.JSDM AND A.JSDM<>'" + FatherDm + "'";
            DataSet ds = BLL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    list.Add(ds.Tables[0].Rows[i][0].ToString());
                }
            }
            context.Response.Write(string.Join(",", list.ToArray()));
        }
        else if (action == "FailedMb")
        {
            List<string> list = new List<string>();
            string MB = context.Request.Params["MBDM"];
            string JSDM = context.Request.Params["JSDM"];
            string LCDM = context.Request.Params["LCDM"];
            int t = 1;
            string MBMC = "";
            //查询是否有未完成的下级关联模板
            string xjglmb = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + MB + "' AND LCDM=" + LCDM + " and JSDM='" + JSDM + "' AND HSZXDM='" + HSZXDM + "'";
            DataSet Dsxjglmb = BLL.Query(xjglmb);
            if (Dsxjglmb.Tables[0].Rows.Count > 0)
            {
                //查找关联下级模板的流程
                for (int j = 0; j < Dsxjglmb.Tables[0].Rows.Count; j++)
                {
                    string dqmbdm = Dsxjglmb.Tables[0].Rows[j][0].ToString();
                    //查询下级关联模板是否有为完成的
                    string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + LCDM + " AND HSZXDM='" + HSZXDM + "' and JHFADM='" + FADM + "' and STATE=-1";
                    DataSet DsNoFinish = BLL.Query(NoFinish);
                    if (DsNoFinish.Tables[0].Rows.Count <= 0)
                    {
                        HeightMbSp(ref list, dqmbdm, LCDM.ToString(), FADM, HSZXDM);
                    }
                    else
                    {
                        string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                        list.Add(mbjs);
                    }
                    //查询该节点的最高节点是否完成
                    //HeightMbSp(ref list, Dsxjglmb.Tables[0].Rows[j][0].ToString(), LCDM, FADM, HSZXDM);
                }
            }
            //ExistMbMC(ref list,MB,JSDM,LCDM,HSZXDM,FADM);
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string mbmc = "select MBMC FROM TB_YSBBMB WHERE MBDM='" + list[i].Split(',')[0] + "'";
                    DataSet dsMbmc = BLL.Query(mbmc);
                    MBMC = MBMC == "" ? t.ToString() + "、" + dsMbmc.Tables[0].Rows[0][0].ToString() + "----" + GetJSNameByJSDM(list[i].Split(',')[1]) : MBMC + "<br>" + t.ToString() + "、" + dsMbmc.Tables[0].Rows[0][0].ToString() + "----" + GetJSNameByJSDM(list[i].Split(',')[1]);
                    t++;
                }
            }
            context.Response.Write(MBMC);
        }
    }

    public void ExistMbMC(ref List<string> list, string MB, string JSDM, string LCDM, string HSZXDM, string FADM)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //查询是否有未完成的下级关联模板
        string xjglmb = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + MB + "' AND LCDM=" + LCDM + " and JSDM='" + JSDM + "' AND HSZXDM='" + HSZXDM + "'";
        DataSet Dsxjglmb = BLL.Query(xjglmb);
        if (Dsxjglmb.Tables[0].Rows.Count > 0)
        {
            for (int j = 0; j < Dsxjglmb.Tables[0].Rows.Count; j++)
            {
                //查询是否还有下级关联模板
                string ExistMb = "select * from TB_YSJSMBZMB where MBDM='" + Dsxjglmb.Tables[0].Rows[j][0] + "' AND LCDM=" + LCDM + "";
                DataSet DsExistMb = BLL.Query(ExistMb);
                //存在
                if (DsExistMb.Tables[0].Rows.Count > 0)
                {
                    for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
                    {
                        string mbdm = DsExistMb.Tables[0].Rows[k][4].ToString();
                        string jsdm = DsExistMb.Tables[0].Rows[k][2].ToString();
                        //递归查询
                        ExistMbMC(ref list, mbdm, jsdm, LCDM, HSZXDM, FADM);
                    }
                }
                else
                {
                    //查询审批该代码为审判的
                    string selSp = "select MBDM,JSDM from TB_YSJSXGMB where MBDM='" + Dsxjglmb.Tables[0].Rows[j][0] + "' AND LCDM=" + LCDM + " AND CZLX='1'";
                    DataSet DsselSp = BLL.Query(selSp);
                    if (DsselSp.Tables[0].Rows.Count > 1)
                    {
                        for (int i = 0; i < DsselSp.Tables[0].Rows.Count; i++)
                        {
                            //查询最终审批代码
                            string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsselSp.Tables[0].Rows[i][1] + "' AND A.LCDM=" + LCDM + "";
                            FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsselSp.Tables[0].Rows[i][0] + "' AND A.JSDM=B.JSDM";
                            DataSet DsFinalDm = BLL.Query(FinalDm);
                            if (DsFinalDm.Tables[0].Rows.Count > 0)
                            {
                                //查询是否审批完成
                                string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + LCDM + " AND STATE=1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + FADM + "' AND HSZXDM='" + HSZXDM + "'";
                                DataSet DsFinishSp = BLL.Query(FinishSp);
                                if (DsFinishSp.Tables[0].Rows.Count <= 0)
                                {
                                    string mbjs = DsFinalDm.Tables[0].Rows[0][0].ToString() + "," + DsFinalDm.Tables[0].Rows[0][1].ToString();
                                    list.Add(mbjs);
                                }
                            }
                        }
                    }
                    else
                    {
                        //查询是否审批完成
                        string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsselSp.Tables[0].Rows[0][0] + "' AND LCDM=" + LCDM + " AND STATE=1 AND JSDM='" + DsselSp.Tables[0].Rows[0][1] + "' AND JHFADM='" + FADM + "' and HSZXDM='" + HSZXDM + "'";
                        DataSet DsFinishSp = BLL.Query(FinishSp);
                        if (DsFinishSp.Tables[0].Rows.Count <= 0)
                        {
                            string mbjs = DsselSp.Tables[0].Rows[0][0].ToString() + "," + DsselSp.Tables[0].Rows[0][1].ToString();
                            list.Add(mbjs);
                        }
                    }
                }
            }
        }
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string JSName = "";
        DataSet DS = new DataSet();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN('" + JSDM + "')";
            DS = bll.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
    /// <summary>
    /// 递归获取角色
    /// </summary>
    /// <param name="list"></param>
    /// <param name="jsdm"></param>
    public void jsdms(ref List<string> list, ref List<string> mblist, string jsdm, string LCDM, string HSZXDM, string JHFADM, string MBDM)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> listSp = new List<string>();
        //查询当前角色模板的父辈角色是否有改模板
        string sql = "Select distinct A.JSDM,DQMBDM from TB_YSLCNode A,TB_YSLCSB LC WHERE A.HSZXDM='" + HSZXDM + "' AND A.LCDM=" + LCDM + " AND A.FBJSDM='" + jsdm + "'";
        sql += " AND A.JSDM=LC.JSDM AND STATE=1 AND A.LCDM=LC.LCDM AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "'";
        DataSet DS = bll.Query(sql);
        //如果父辈角色有该模板
        if (DS.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                list.Add(DS.Tables[0].Rows[i][0].ToString());
                mblist.Add(DS.Tables[0].Rows[i][1].ToString());
                //查询是否有关联下级模板
                string xjgl = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + DS.Tables[0].Rows[i][1].ToString() + "' AND LCDM=" + LCDM + " and JSDM='" + DS.Tables[0].Rows[i][0].ToString() + "' AND HSZXDM='" + HSZXDM + "'";
                DataSet DsXj = bll.Query(xjgl);
                if (DsXj.Tables[0].Rows.Count > 0)
                {
                    //查找关联下级模板的流程
                    for (int j = 0; j < DsXj.Tables[0].Rows.Count; j++)
                    {
                        //查询下级关联模板是否有为完成的
                        string NoFinish = "select * FROM TB_YSLCSB WHERE DQMBDM='" + DsXj.Tables[0].Rows[j][0] + "' AND LCDM=" + LCDM + " AND HSZXDM='" + HSZXDM + "' and JHFADM='" + JHFADM + "' and STATE=-1";
                        DataSet DsNoFinish = bll.Query(NoFinish);
                        if (DsNoFinish.Tables[0].Rows.Count <= 0)
                        {
                            listSp.Clear();
                            //查询该节点的最高节点是否完成
                            HeightMbSp(ref listSp, DsXj.Tables[0].Rows[j][0].ToString(), LCDM, JHFADM, HSZXDM);
                            if (listSp.Count <= 0)
                            {
                                //查询下级关联模板的审批
                                string xjsp = "select DISTINCT SB.JSDM,DQMBDM from TB_YSLCSB SB,TB_YSJSXGMB A WHERE DQMBDM='" + DsXj.Tables[0].Rows[j][0] + "' AND SB.LCDM=" + LCDM + "";
                                xjsp += " AND SB.HSZXDM='" + HSZXDM + "' AND JHFADM='" + JHFADM + "' AND STATE=1 AND SB.JSDM=A.JSDM AND SB.LCDM=A.LCDM AND SB.HSZXDM=A.HSZXDM AND A.CZLX='1' AND SB.DQMBDM=A.MBDM";
                                DataSet DsXjSp = bll.Query(xjsp);
                                if (DsXjSp.Tables[0].Rows.Count > 1)
                                {
                                    for (int k = 0; k < DsXjSp.Tables[0].Rows.Count; k++)
                                    {
                                        string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsXjSp.Tables[0].Rows[k][0] + "' AND A.LCDM=" + LCDM + "";
                                        FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsXjSp.Tables[0].Rows[k][1] + "' AND A.JSDM=B.JSDM";
                                        DataSet DsFinalDm = bll.Query(FinalDm);
                                        if (DsFinalDm.Tables[0].Rows.Count > 0)
                                        {
                                            list.Add(DsXjSp.Tables[0].Rows[k][0].ToString());
                                            mblist.Add(DsXjSp.Tables[0].Rows[k][1].ToString());
                                            jsdms(ref list, ref mblist, DsXjSp.Tables[0].Rows[k][0].ToString(), LCDM, HSZXDM, JHFADM, DsXjSp.Tables[0].Rows[k][1].ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    list.Add(DsXjSp.Tables[0].Rows[0][0].ToString());
                                    mblist.Add(DsXjSp.Tables[0].Rows[0][1].ToString());
                                    jsdms(ref list, ref mblist, DsXjSp.Tables[0].Rows[0][0].ToString(), LCDM, HSZXDM, JHFADM, DsXjSp.Tables[0].Rows[0][1].ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    jsdms(ref list, ref mblist, DS.Tables[0].Rows[i][0].ToString(), LCDM, HSZXDM, JHFADM, MBDM);
                }
            }
        }
        else//查询是否有关联下级模板
        {
            //查询是否有关联下级模板
            string xjgl = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + MBDM + "' AND LCDM=" + LCDM + " and JSDM='" + jsdm + "' AND HSZXDM='" + HSZXDM + "'";
            DataSet DsXj = bll.Query(xjgl);
            if (DsXj.Tables[0].Rows.Count > 0)
            {
                //查找关联下级模板的流程
                for (int j = 0; j < DsXj.Tables[0].Rows.Count; j++)
                {
                    //查询下级关联模板是否有为完成的
                    string NoFinish = "select * FROM TB_YSLCSB WHERE DQMBDM='" + DsXj.Tables[0].Rows[j][0] + "' AND LCDM=" + LCDM + " AND HSZXDM='" + HSZXDM + "' and JHFADM='" + JHFADM + "' and STATE=-1";
                    DataSet DsNoFinish = bll.Query(NoFinish);
                    if (DsNoFinish.Tables[0].Rows.Count <= 0)
                    {
                        listSp.Clear();
                        //查询该节点的最高节点是否完成
                        HeightMbSp(ref listSp, DsXj.Tables[0].Rows[j][0].ToString(), LCDM, JHFADM, HSZXDM);
                        if (listSp.Count <= 0)
                        {
                            //查询下级关联模板的审批
                            string xjsp = "select DISTINCT SB.JSDM,DQMBDM from TB_YSLCSB SB,TB_YSJSXGMB A WHERE DQMBDM='" + DsXj.Tables[0].Rows[j][0] + "' AND SB.LCDM=" + LCDM + "";
                            xjsp += " AND SB.HSZXDM='" + HSZXDM + "' AND JHFADM='" + JHFADM + "' AND STATE=1 AND SB.JSDM=A.JSDM AND SB.LCDM=A.LCDM AND SB.HSZXDM=A.HSZXDM AND A.CZLX='1' AND SB.DQMBDM=A.MBDM";
                            DataSet DsXjSp = bll.Query(xjsp);
                            if (DsXjSp.Tables[0].Rows.Count > 1)
                            {
                                for (int k = 0; k < DsXjSp.Tables[0].Rows.Count; k++)
                                {
                                    string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsXjSp.Tables[0].Rows[k][0] + "' AND A.LCDM=" + LCDM + "";
                                    FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsXjSp.Tables[0].Rows[k][1] + "' AND A.JSDM=B.JSDM";
                                    DataSet DsFinalDm = bll.Query(FinalDm);
                                    if (DsFinalDm.Tables[0].Rows.Count > 0)
                                    {
                                        list.Add(DsXjSp.Tables[0].Rows[k][0].ToString());
                                        mblist.Add(DsXjSp.Tables[0].Rows[k][1].ToString());
                                        jsdms(ref list, ref mblist, DsXjSp.Tables[0].Rows[k][0].ToString(), LCDM, HSZXDM, JHFADM, DsXjSp.Tables[0].Rows[k][1].ToString());
                                    }
                                }
                            }
                            else
                            {
                                list.Add(DsXjSp.Tables[0].Rows[0][0].ToString());
                                mblist.Add(DsXjSp.Tables[0].Rows[0][1].ToString());
                                jsdms(ref list, ref mblist, DsXjSp.Tables[0].Rows[0][0].ToString(), LCDM, HSZXDM, JHFADM, DsXjSp.Tables[0].Rows[0][1].ToString());
                            }
                        }
                    }
                }
            }
        }
    }
    /// <summary>
    /// 查询最高级是否审批完成
    /// </summary>
    /// <param name="listSp"></param>
    /// <param name="mbdm"></param>
    /// <param name="lcdm"></param>
    /// <param name="jhfadm"></param>
    /// <param name="hszxdm"></param>
    public void HeightMbSp(ref List<string> listSp, string mbdm, string lcdm, string jhfadm, string hszxdm)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //查询审批该代码为审判的
        string selSpdm = "select MBDM,JSDM from TB_YSJSXGMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " AND CZLX='1'";
        DataSet DsselSpdm = BLL.Query(selSpdm);
        if (DsselSpdm.Tables[0].Rows.Count > 1)
        {
            for (int i = 0; i < DsselSpdm.Tables[0].Rows.Count; i++)
            {
                //查询最终审批代码
                string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsselSpdm.Tables[0].Rows[i][1] + "' AND A.LCDM=" + lcdm + "";
                FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsselSpdm.Tables[0].Rows[i][0] + "' AND A.JSDM=B.JSDM";
                DataSet DsFinalDm = BLL.Query(FinalDm);
                if (DsFinalDm.Tables[0].Rows.Count > 0)
                {
                    //查询是否审批完成
                    string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DsFinishSp = BLL.Query(FinishSp);
                    if (DsFinishSp.Tables[0].Rows.Count <= 0)
                    {
                        string mbjs = DsFinalDm.Tables[0].Rows[0][0].ToString() + "," + DsFinalDm.Tables[0].Rows[0][1].ToString();
                        listSp.Add(mbjs);
                    }
                    else
                    {
                        //查询是否还有下级关联模板
                        string ExistMb = "select JSDM,CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + "";
                        DataSet DsExistMb = BLL.Query(ExistMb);
                        {
                            if (DsExistMb.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
                                {
                                    string DQMBDM = DsExistMb.Tables[0].Rows[k][1].ToString();
                                    HeightMbSp(ref listSp, DQMBDM, lcdm, jhfadm, hszxdm);
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //查询是否审批完成
            string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsselSpdm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsselSpdm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "'";
            DataSet DsFinishSp = BLL.Query(FinishSp);
            if (DsFinishSp.Tables[0].Rows.Count <= 0)
            {
                string mbjs = DsselSpdm.Tables[0].Rows[0][0].ToString() + "," + DsselSpdm.Tables[0].Rows[0][1].ToString();
                listSp.Add(mbjs);
            }
            else
            {
                //查询是否还有下级关联模板
                string ExistMb = "select JSDM,CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + "";
                DataSet DsExistMb = BLL.Query(ExistMb);
                {
                    if (DsExistMb.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
                        {
                            string DQMBDM = DsExistMb.Tables[0].Rows[k][1].ToString();
                            HeightMbSp(ref listSp, DQMBDM, lcdm, jhfadm, hszxdm);
                        }
                    }
                }
            }
        }
    }
    //获取计划方案月份：对季度中月份为空做了处理：
    private string GetFaMonth(string _jhfadm)
    {
        string FaMonth = "";
        string Fabs = "";
        string JD = "";
        int iMonth = 1;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NN,FABS,JD FROM TB_JHFA WHERE JHFADM ='" + _jhfadm + "'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            FaMonth = DS.Tables[0].Rows[0]["NN"].ToString();
            Fabs = DS.Tables[0].Rows[0]["FABS"].ToString();
            JD = DS.Tables[0].Rows[0]["JD"].ToString();
            //如果是季度并且有异常数据，则按如下处理：
            if (Fabs.Equals("2") && (FaMonth == null || FaMonth.Trim().Equals("")))
            {
                //每季度第一月：
                iMonth = Convert.ToInt32(JD) * 3 - 2;
                FaMonth = iMonth.ToString();
            }
            else
            {
                FaMonth = Convert.ToInt32(FaMonth).ToString();
            }
        }
        return FaMonth;
    }


    //根据传入的信息获取取数公式或校验公式所需年月信息：
    //    如果是月季，其他方案 FABS IN(1,2,4)的时候
    //BEGIN
    //IF  预算月份>=方案的年月 THEN
    //当前月份=方案月份-1
    //ELSE
    //当前月份=预算月份 年=方案年
    //END
    //ELSE  如果FABS=3 是年方案
    //如果 
    //那么当前月份=预算月份 年=方案年-1
    private void GetGsYearMonth(string _jhfadm, string _year, string _fabs, string _ysyf, ref string _gsyear, ref string _gsmonth)
    {
        string famonth = GetFaMonth(_jhfadm);
        if (_fabs.Equals("1") || _fabs.Equals("2") || _fabs.Equals("4"))
        {
            _gsyear = _year;
            if (Convert.ToInt32(_ysyf) >= Convert.ToInt32(famonth))
            {
                _gsmonth = (Convert.ToInt32(famonth) - 1).ToString();
                if (_gsmonth.Equals("0"))
                {
                    _gsmonth = "1";
                }
            }
            else
            {
                _gsmonth = _ysyf;
            }
        }
        else if (_fabs.Equals("3"))
        {
            _gsyear = (Convert.ToInt32(_year) - 1).ToString();
            _gsmonth = _ysyf;
        }
        if (_gsmonth.Length == 1)
        {
            _gsmonth = '0' + _gsmonth;
        }
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}