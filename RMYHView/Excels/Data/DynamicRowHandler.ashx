﻿<%@ WebHandler Language="C#" Class="DynamicRowHandler" %>

using System;
using System.Web;
using System.Linq;
using System.Web.SessionState;
using System.Collections.Generic;
using Newtonsoft.Json;
using RMYH.Model;
using RMYH.Common;
using RMYH.BLL;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Data;

public class DynamicRowHandler : IHttpHandler,IRequiresSessionState
{
    public void ProcessRequest (HttpContext context) {
        string rtn = "";
        string action = context.Request.Params["action"];
        if (action.Equals("Insert"))
        {
            try
            {
                //将数据页中插入的变动行插入模板页中
                string MBstr = context.Request.Params["MB"];
                string MBDM = context.Request.Params["MBDM"];
                string JHFADM = context.Request.Params["JHFADM"];
                string SessionID = context.Request.Params["SessionID"];
                string CacheKey = SessionID + "." + JHFADM + "." + MBDM;
                if (MBstr != "" && MBstr != null)
                {
                    //变动行反序列化MBGSVALUE对象
                    List<ExcelSheet> ESS = JsonConvert.DeserializeObject<List<ExcelSheet>>(MBstr);
                   
                    //根据SessionID、计划方案代码以及模板代码获取模板缓存
                    List<ExcelSheet> MBCache = (List<ExcelSheet>)DataCache.GetCache(CacheKey);
                    //定义修改追加变动行所在的Sheet页的单元格的行索引正则表达式
                    Regex Reg = new Regex(@"\$?[a-zA-Z]+\$?[0-9]+");
                    if (MBCache != null)
                    {
                        foreach (var E in ESS)
                        {
                            //先查找需要插入变动行的Sheet页
                            ExcelSheet ES = MBCache.Find(G => G.SHEETNAME == E.SHEETNAME);
                            if (ES != null)
                            {
                                //定义修改其它跨Sheet页的单元格的行索引的正则表达式
                                Regex Reg2 = new Regex(E.SHEETNAME + @"!\$?[a-zA-Z]+\$?[0-9]+");
                                foreach (var M in E.Rows)
                                {
                                    //插入变动行
                                    dealExcel.InsertDynamicRow(MBCache, ES, M, Reg, Reg2, E.SHEETNAME);
                                }
                            }
                        }
                        //最终将原数据以及变动行的数据，缓存到内存中
                        DataCache.SetCache(CacheKey, MBCache);
                        rtn = "OK";
                    }
                }
                else
                {
                    //删除缓存
                    DataCache.DelCache(CacheKey);
                    //缓存模板数据到内存中，目的是插入变动行时，以此数据为依据
                    CacheMBData.CacheDataToMemory(JHFADM, MBDM, SessionID);
                    rtn = "OK";
                }
            }
            catch (Exception E)
            {
                rtn = E.Message; 
            }
            context.Response.Write(rtn);
        }
        else if (action == "BDYZ") 
        {
            using (var reader = new System.IO.StreamReader(context.Request.InputStream))
            {
                int Len = 0,HIndex=0,C=0,RI=0;
                List<RowAndCol> RC=new List<RowAndCol>();
                string RowPropertyIndex = "-1",LastGS="",LastSheetName="",MC="";
                DataSet DB = new DataSet();
                TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
                //获取计划方案数据集合
                DataSet TT = BLL.Query("select * from REPORT_CSSZ_SJYDYXTJ");
                JavaScriptSerializer js = new JavaScriptSerializer();
                string jsonData = reader.ReadToEnd();
                GetGsRoot resdata = js.Deserialize<GetGsRoot>(jsonData);
                string icount = resdata.count;
                string fadm = resdata.fadm;
                string mbdm = resdata.mbdm;
                string fadm2 = resdata.fadm2;
                //当前服务器时间年月：
                string bnyy = resdata.yy;
                string bnnn = resdata.nn;
                string fabs = resdata.fabs;
                string loginyy = resdata.loginyy;
                string SessionID=resdata.sessionid;
                DataSet DS = BLL.Query("select * from TB_JHFA WHERE JHFADM='"+fadm+"'");
                string CacheKey = SessionID + "." + fadm + "." + mbdm;
                //删除缓存
                DataCache.DelCache(CacheKey);
                //缓存模板数据到内存中，目的是插入变动行时，以此数据为依据
                CacheMBData.CacheDataToMemory(fadm, mbdm, SessionID);

                string resjson = "{\"grids\":[";
                for (int i = 0; i < int.Parse(icount); i++)
                {
                    string idx = resdata.grids[i].idx;
                    string row = resdata.grids[i].row;
                    string col = resdata.grids[i].col;
                    string gs = resdata.grids[i].gs;
                    try
                    {
                        DB.Tables.Clear();
                        //获取变动取数项目数据集
                        if (LastGS == "")
                        {
                            LastGS = gs; 
                        }

                        if (LastSheetName == "")
                        {
                            LastSheetName = idx;
                        }
                        else
                        {
                            if (LastSheetName != idx) 
                            {
                                C = 0;
                            }
                        }
                            
                        DB = GetDataList.BDYZ(DS, TT, gs, bnyy, bnnn, fadm, fadm2, fabs, loginyy, mbdm, idx,ref RowPropertyIndex);
                        if (DB.Tables[0].Rows.Count > 0)
                        {
                            //获取变动行拼接的Json字符串
                            resjson = dealExcel.GetDynamicRowsString(DB, resjson, idx, fadm, mbdm, ref Len, ref HIndex, SessionID, gs, int.Parse(row), int.Parse(col), RowPropertyIndex, "-1", ref LastGS, ref C,ref RC);
                            LastGS = gs;
                        }
                        else
                        {
                            //如果取数延展公式没有取到数的话，则返回0，目的是为了SUM公式取合计时，不会因为取数延展公式，而取不到值
                            if (gs.IndexOf(",\"XMMC\"") == -1)
                            {
                                MC = "0.00";
                            }
                            else
                            {
                                MC = "";
                            }
                            
                            RI = int.Parse(row);
                            RowAndCol RR = RC.FindLast(R => R.RowIndex <= int.Parse(row));
                            if (RR != null)
                            {
                                RI = RI + RR.Count;
                            }
                            dealExcel.GetBDJsonString(ref resjson, "-1", idx, RI, int.Parse(col), "-1",MC, 2, 1, RowPropertyIndex);
                            Len++;
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(idx + "标签页，" + row.ToString() + "行、" + col.ToString() + "列，公式：" + gs
                            + "出错！\n" + ex.Message);
                    }
                }
                resjson = resjson+ "],\"count\":"+Len+"}";
                context.Response.Write(resjson);
                context.Response.End();
            }
        }
        else if (action == "UpdateRows") {
            string resJson = "";
            string GSYY = context.Request.Params["GSYY"];
            string GSNN = context.Request.Params["GSNN"];
            string FADM2 = context.Request.Params["FADM2"];
            string FABS = context.Request.Params["FABS"];
            string LYY = context.Request.Params["YY"];
            string MBDM = context.Request.Params["MBDM"];
            string JHFADM = context.Request.Params["FADM"];
            string SessionID = context.Request.Params["SessionID"];
            resJson=dealExcel.InsertDynamicRowsToMemory(JHFADM, MBDM, SessionID, GSYY, GSNN, FADM2, FABS, LYY);
            context.Response.Write(resJson);
        }
        else if (action == "GetRowProp") {
            //获取行组成部分
            DataSet DS = new DataSet();            
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            string MBDM = context.Request.Params["MBDM"];
            string SHEETNAME = context.Request.Params["SHEETNAME"];
            string SQL = "SELECT HXLX FROM TB_MBSHEETSX WHERE  MBDM='" + MBDM + "'  AND SHEETNAME='" + SHEETNAME + "'";
            DS=BLL.Query(SQL);
            if (DS.Tables[0].Rows.Count>0)
            {
                rtn = DS.Tables[0].Rows[0]["HXLX"].ToString();
            }
            context.Response.Write(rtn); 
        }
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}