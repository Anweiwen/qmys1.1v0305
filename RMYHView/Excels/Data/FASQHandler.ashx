﻿<%@ WebHandler Language="C#" Class="FASQHandler" %>

using System;
using System.Web;
using RMYH.BLL;

public class FASQHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string res = "";
        context.Response.ContentType = "text/plain";
        string JHFADM = context.Request.QueryString["JHFADM"].ToString();
        string USERDM = context.Request.QueryString["USERDM"].ToString();
        string HSZXDM = context.Request.QueryString["HSZXDM"].ToString();
        context.Response.Clear();
        //获取计划方案的截止时间
        res = GetFAJZSJStr(JHFADM,USERDM, HSZXDM);
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取计划方案的截止时间和剩余时间
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="USERDM">用户代码</param>
    /// <param name="HSZXDM">核算中心代码</param>
    /// <returns></returns>
    public string GetFAJZSJStr(string JHFADM, string USERDM, string HSZXDM)
    {
        int t = 0;
        string SYTS = "", JZSJ = "", Data = "", Week = "", SZCS = "",DataHour="";
        TimeSpan TS = TimeSpan.Zero;
        TimeSpan TSHour = TimeSpan.Zero;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JHFADM != "")
        {
            //判断当前计划方案是否走流程
            if (BLL.IsLCSP(JHFADM))
            {
                //获取方案的截止时间
                JZSJ = BLL.GetJZSJByJHFADM(JHFADM);
                SZCS = BLL.GetSZCS();
                //获取当前用户是否是受限用户
                bool Flag = BLL.IsSQUSER(USERDM, HSZXDM,JHFADM);
                if (Flag)
                {
                    
                    if (JZSJ != "")
                    {
                        Data = DateTime.Now.ToString("yyyy-MM-dd");
                        DataHour = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        TS = DateTime.Parse(JZSJ.Substring(0, 10)) - DateTime.Parse(Data + " 00:00:00");
                        TSHour = DateTime.Parse(JZSJ.Substring(0, 10)) - DateTime.Parse(DataHour);
                        if (TS.Days > 0)
                        {
                            for (int n = 0; n < TS.Days; n++)
                            {
                                //获取当前天是星期几
                                Week = DateTime.Parse(Data).AddDays(n).DayOfWeek.ToString();
                                //只算工作日，不算周六、日
                                if (Week == "Saturday" || Week == "Sunday")
                                {
                                    t++;
                                }
                            }
                            if (SZCS == "1")
                            {
                                SYTS = "剩余" + (TSHour.Hours - t * 24).ToString() + "个小时";
                            }
                            else
                            {
                                SYTS = "截止时间：" + JZSJ + ", 剩余" + (TS.Days - t).ToString() + "个工作日";
                            }
                        }
                        else if (TS.Days == 0)
                        {
                            if (SZCS == "1")
                            {
                                SYTS = "剩余" + (TSHour.Hours - t * 24).ToString() + "个小时";
                            }
                            else
                            {
                                SYTS = "截止时间：" + JZSJ + ",剩余" + (TS.Days - t).ToString() + "个工作日";
                            }
                        }
                        else
                        {
                            SYTS = "截止时间：" + JZSJ + ", 方案已完成";
                            //SYTS = "截止时间：" + JZSJ + ", 延时" + (0 - TS.Days - t).ToString() + "个工作日";
                        }
                    }
                }
                else
                {
                      SYTS = "截止时间：" + JZSJ;
                }
            }
        }
        return SYTS;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}