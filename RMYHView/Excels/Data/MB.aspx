﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MB.aspx.cs" Inherits="MB"
    ValidateRequest="false" %>

<%@ Register Assembly="PageOffice, Version=3.0.0.1, Culture=neutral, PublicKeyToken=1d75ee5788809228"
    Namespace="PageOffice" TagPrefix="po" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <title>基础模板填报</title>
    <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
    <link href="../../Content/themes/icon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
        .messager-body
        {
            font-size: 14px;
        }
        
        .textbox .textbox-text
        {
            font-size: 14px;
        }
    </style>
    <script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <script src="../../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../../JS/json2.js" type="text/javascript"></script>
    <script type="text/javascript">
        var statueID = 0;
        //辨别是上报、还是审批
        var state = 0;
        var MBWJ = "",IsExistFile;
        var hasopenfile = null;
        //当前服务器年，月：
        var bnyy = "", bnnn = "", smqr = ""; SmqrWidth = "610"; SmqrHeight = "480";
        var localopen = false;
        //本地打开文件名：
        var localfilename = '';

        /// <summary>对datagrid控件添加遮罩</summary>
        /// <param name="_datagrid" type="datagird">要进行遮罩的datagrid控件</param>
        function datagridloading(_datagrid) {
            var opts = _datagrid.datagrid("options");
            var wrap = $.data(_datagrid[0], "datagrid").panel;
            if (opts.loadMsg) {
                $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: wrap.width(), height: wrap.height() }).appendTo(wrap);
                $("<div class=\"datagrid-mask-msg\"></div>").html(opts.loadMsg).appendTo(wrap).css({ display: "block", left: (wrap.width() - $("div.datagrid-mask-msg", wrap).outerWidth()) / 2, top: (wrap.height() - $("div.datagrid-mask-msg", wrap).outerHeight()) / 2 });
            }
        }

        function datagridloaded(_datagrid) {
            _datagrid.datagrid("getPager").pagination("loaded");
            var wrap = $.data(_datagrid[0], "datagrid").panel;
            wrap.find("div.datagrid-mask-msg").remove();
            wrap.find("div.datagrid-mask").remove();
        }

        function IsCheckFormula(_fn) {
            if (_fn == undefined || _fn == null || _fn == "") {
                return false;
            }
            else 
            {
                return (_fn.toString().indexOf('checkformula') >= 0 || _fn.toString().indexOf('CheckFamb') >= 0) ? true : false;
            }
        }

        /// <summary>获取服务器年月：</summary>
        function GetYyNn() {
            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=getyynn',
                cache: false,
                success: function (result) {
                    if (result != null && result != '') {
                        bnyy = result.toString().substr(0, 4);
                        bnnn = result.toString().substr(4, 2);
                    }
                },
                error: function (req) {
                    dialogclose();
                    BmShowWinError('获取服务器年月错误!', req.responseText);
                }
            });
        }

        //有修改是否保存：
        function IsSave(_fn) {
            if (po.poctrl.Document) {
                po.poctrl.EnableExcelCalling();
                //如果是已经修改了：
                if ((!po.poctrl.Document.Saved) && "<%=CANSAVE%>" == "1") {
                    SaveUpClick(_fn);
                } else {
                    if (IsCheckFormula(_fn)) {
                        eval(_fn);
                    }
                }
            }
        }

        //N_JYGS($B$1:$D$8,$E$1:$F$8)
        //获取校验公式以N_JYGS开头的数据，此数据意思是对比指定列数据是否对应，比如数据列有数据，名称列必须不能为空
        function JyTsgs(_tjflag) {
            var mbdm = '<%=MBDM%>';
            var message = "";
            $.ajax({
                type: 'get',
                url: 'MB.ashx',
                data: {
                    action: 'getJygs',
                    MBDM: mbdm
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != '') {
                        //获取各校验公式：
                        for (var i = 0; i < result.split('+').length; i++) {
                            var sheet = result.split('+')[i].split('|')[0];
                            var gs = result.split('+')[i].split('|')[1];
                            //解析校验公式：
                            message += jy(sheet, gs);
                        }
                    }
                    //如果公式校验有问题，则提示：
                    if (message != '') {
                        alert('指定列校验错误：\n\r' + message);
                        po.UpdateAndVis(true);
                        //如果公式校验正确则message没有返回值，进入提交申请或者审批
                    } else {
                        SaveFile();
                        var FileNamePath = SaveExcelFileName("<%=FADM %>", "<%=MBWJ%>");
                        $.ajax({
                            type: 'get',
                            url: 'MB.ashx',
                            data: {
                                action: "DeleteExcelToNewData",
                                FileNamePath: FileNamePath,
                            },
                            async: true,
                            cache: false,
                            success: function (result) {
                                if (result=1) {
                                    //修改延展行项目分类属性
                                    po.UpdateXMFLProperties();
                                    if (po.poctrl.FullScreen)
                                        po.poctrl.FullScreen = false;
                                    po.UpdateAndVis(false);
                                    var _fn;
                                    if ('<%=MBLB %>' == 'YSTZ') {
                                        MBSave(_fn);
                                    }
                                    else {
                                        $.messager.confirm('提示：', '是否要保存数据和文件?', function (r) {
                                            if (r) {
                                                MBSave(_fn);
                                            }
                                            else {
                                              dialogclose();
                                                po.UpdateAndVis(true);
                                            }
                                        }, function () {
                                            po.UpdateAndVis(true);
                                        });
                                    }
                                 }
                             },
                            error: function () {
                       
                            }
                        });
                        $("#HidTJ").val(_tjflag);
                    }
                },
                error: function (req) {
                    dialogclose();
                    po.setvisible(true);
                    BmShowWinError('请求校验数据错误!', req.responseText);
                }
            });
        }

        /// <summary>解析校验公式看是否有不符合条件的单元格</summary>
        /// <returns type="String" />
        /// <param name="_sheet" type="String">Sheet页名</param>
        /// <param name="gs" type="String">要解析的公式</param>
        function jy(_sheet, gs) {
            var message = '';
            if (_sheet) {
                var index = gs.indexOf("(");
                var _gs;
                _gs = gs.toString().trim().replace(/\r/g, '').replace(/\n/g, '');
                if (index > -1) {
                    _gs = _gs.substring(index + 1, gs.length);
                }
                else {
                    message = '校验公式有错误!';
                    return message;
                }
                if (_gs) {
                    var addr = _gs.split(',')[0];
                    if (addr == "") {
                        message = '校验公式有错误!';
                        return message;
                    }
                    var JyqyAddr = _gs.split(',')[1].toString();
                    if (JyqyAddr == "") {
                        message = '校验公式有错误!';
                        return message;
                    }
                    JyqyAddr = JyqyAddr.substring(0, JyqyAddr.length - 1);
                    var JyTs = new Array();
                    var qswzL = "";
                    var qswzH = "";
                    var zzwzL = "";
                    var zzwzH = "";
                    var JyqyQswzL = "";
                    var JyqyQswzH = "";
                    var JyqyZzwzL = "";
                    var JyqyZzwzH = "";
                    if (addr.split(':').length == 1) {
                        //起始位置列
                        qswzL = addr.split('$')[1].replace("$", "");
                        //起始位置行
                        qswzH = addr.split('$')[2].replace("$", "");
                        //终止位置列
                        zzwzL = addr.split('$')[1].replace("$", "");
                        //终止位置行
                        zzwzH = addr.split('$')[2].replace("$", "");
                    }
                    else {
                        //起始位置列
                        qswzL = addr.split(':')[0].split('$')[1].replace("$", "");
                        //起始位置行
                        qswzH = addr.split(':')[0].split('$')[2].replace("$", "");
                        //终止位置列
                        zzwzL = addr.split(':')[1].split('$')[1].replace("$", "");
                        //终止位置行
                        zzwzH = addr.split(':')[1].split('$')[2].replace("$", "");
                    }
                    if (JyqyAddr.split(':').length == 1) {
                        //起始位置列
                        JyqyQswzL = JyqyAddr.split('$')[1].replace("$", "");
                        //起始位置行
                        JyqyQswzH = JyqyAddr.split('$')[2].replace("$", "");
                        //终止位置列
                        JyqyZzwzL = JyqyAddr.split('$')[1].replace("$", "");
                        //终止位置行
                        JyqyZzwzH = JyqyAddr.split('$')[2].replace("$", "");
                    }
                    else {
                        //起始位置列
                        JyqyQswzL = JyqyAddr.split(':')[0].split('$')[1].replace("$", "");
                        //起始位置行
                        JyqyQswzH = JyqyAddr.split(':')[0].split('$')[2].replace("$", "");
                        //终止位置列
                        JyqyZzwzL = JyqyAddr.split(':')[1].split('$')[1].replace("$", "");
                        //终止位置行
                        JyqyZzwzH = JyqyAddr.split(':')[1].split('$')[2].replace("$", "");
                    }
                    if (qswzH != JyqyQswzH || zzwzH != JyqyZzwzH) {
                        message = '起始位置行或终止位置行不对应，公式有误，请修改校验公式!';
                        return message;
                    }
                    //当前sheet页
                    var sheet = po.getsheetbyname(_sheet);
                    if (sheet == null || sheet == undefined) {
                        message = '[' + _sheet + ']标签页已不存在，请在模板设置中重新设置校验公式!';
                        return message;
                    }
                    //获取首列的行列属性
                    //数值列数组
                    var arrSz = new Array();
                    //名称对比列数组
                    var arrSzJy = new Array();
                    //获取所有数值列数据添加到数组中
                    for (var j = parseInt(qswzH); j <= parseInt(zzwzH); j++) {
                        //数值行数据
                        var szhSj = "";
                        for (var i = col2num(qswzL); i <= col2num(zzwzL); i++) {
                            var rowcolval = "";
                            var value = sheet.Range("" + num2col(i) + "" + j + "").value;
                            if (value != undefined && value != "" && value.toString().trim() != "") {
                                szhSj += "T";
                            }
                        }
                        arrSz.push(szhSj);
                    }
                    //获取所有数值对比列数据添加到数组中
                    for (var j = parseInt(JyqyQswzH); j <= parseInt(JyqyZzwzH); j++) {
                        //数值行数据
                        var szhSjJY = "";
                        for (var i = col2num(JyqyQswzL); i <= col2num(JyqyZzwzL); i++) {
                            var rowcolval = "";
                            var value = sheet.Range("" + num2col(i) + "" + j + "").value;
                            if (value != undefined && value != "" && value.toString().trim() != "") {
                                szhSjJY += "T";
                            }
                        }
                        arrSzJy.push(szhSjJY);
                    }
                    //获取列数
                    var Ls = col2num(JyqyZzwzL) - col2num(JyqyQswzL) + 1;
                    for (var i = 0; i < arrSzJy.length; i++) {
                        if (arrSz[i].toString() != "" && arrSzJy[i].toString().length != Ls) {
                            var HH = parseInt(i) + parseInt(JyqyQswzH);
                            message += sheet.Name + "页" + HH + "行名称或名称说明为空\n";
                            //                            return message;
                        }
                    }
                }
            }
            return message;
        }

        //校验公式，上报和审批操作前执行：
        function checkformula(_tjflag) {
            //校验公式：
            var surl = 'MB.ashx?action=checkformula&mbdm=<%=MBDM %>&gsyy=<%=GSYEAR %>&gsnn=<%=GSMONTH %>&fadm=<%=FADM %>&fadm2=<%=FADM2 %>&fabs=<%=FALB %>&yy=<%=YY%>';
            po.UpdateAndVis(false);
            po.poctrl.EnableExcelCalling();
            dialogshowwin();
            var shint = '正在校验数据，请稍候......';
            dialogshow(shint, true);
            //请求校验公式：
            $.ajax({
                type: 'get',
                url: surl,
                cache: false,
                success: function (result) {
                    dialogclose();
                    //代表校验正常：
                    if (result == '') {
                        //校验行列是否为空公式：
                        JyTsgs(_tjflag);
                        //如果有警告或者错误提示：
                    } else {
                        var aryres = result.split('||');
                        var message = '';
                        for (var i = 0; i < aryres.length - 1; i++) {
                            if (message == '') {
                                message = aryres[i];
                            } else {
                                message = message + '||' + aryres[i];
                            }
                        }
                        //如果为'1'则警告
                        if (aryres[aryres.length - 1] == '1') {
                            $.messager.confirm('出现以下警告，是否继续：', message, function (r) {
                                if (r) {
                                    JyTsgs(_tjflag);
                                } else {
                                    po.UpdateAndVis(true);
                                }
                            }, function () {
                                po.UpdateAndVis(true);
                            });
                            //如果为'0'则为错误：
                        } else if (aryres[aryres.length - 1] == '0') {
                            po.UpdateAndVis(true);
                            alert('公式校验错误：\n\r' + message);
                            statueID = 1;
                        } else {
                            JyTsgs(_tjflag);
                        }
                    }
                },
                error: function (req) {
                    dialogclose();
                    po.setvisible(true);
                    BmShowWinError('请求校验数据错误!', req.responseText);
                }
            });
        }

        //生产方案模板校验：
        function CheckFamb(_tjflag) {
            //模板校验：
            var surl = 'MB.ashx?action=fambcheck&hszxdm=<%=HSZXDM %>&fadm=<%=FADM %>';
            po.UpdateAndVis(false);
            po.poctrl.EnableExcelCalling();
            //请求：
            $.ajax({
                type: 'get',
                url: surl,
                cache: false,
                success: function (result) {
                    dialogclose();
                    //代表校验正常：
                    if (result == '') {
                        //提交申请或者审批
                        $("#HidTJ").val(_tjflag);
                        OpenSmqr(_tjflag);
                        po.UpdateAndVis(true);
                    } else {
                        var GrdImbl = $("#GrdImba");
                        datagridloading(GrdImbl);
                        var WinImba = $("#WinImba");
                        WinImba.window({
                            onClose: function () {
                                po.UpdateAndVis(true);
                            }
                        });
                        WinImba.window('open');
                        result = result.replace(/\r/g, "").replace(/\n/g, "");
                        try {
                            var jsonres = eval(result);
                        } catch (e) {
                            BmShowWinError('公式解析错误!', result);
                            po.UpdateAndVis(false);
                        }
                        GrdImbl.datagrid('loadData', jsonres);
                        datagridloaded(GrdImbl);
                    }
                },
                error: function (req) {
                    dialogclose();
                    po.UpdateAndVis(true);
                    BmShowWinError('请求校验数据错误!', req.responseText);
                }
            });
        }

        //获取模板行列属性：
        function GetRowCol() {
            UpdateExcelRows(3);
//            var rtnJson = po.getDynamicRowsJson("<%=MBDM %>");
//            $.ajax({
//                type: 'post',
//                url: 'DynamicRowHandler.ashx',
//                data: {
//                    MB: rtnJson,
//                    action: "Insert",
//                    MBDM: "<%=MBDM %>",
//                    JHFADM: "<%=FADM %>",
//                    SessionID: "<%=SessionID%>"
//                },
//                cache: false,
//                success: function (result) {
//                    
//                },
//                error: function (result) {
//                    alert(result.responseText);
//                    return;
//                }
//            });
        }


        function GetRowAndColProperty(){
            po.UpdateAndVis(false);
            //请求获取模板行列属性：
            $.messager.confirm('提示：', '是否重新获取模板行列属性?', function (r) {
                if (r) {
                    var jhfadm = "<%=FADM %>";
                    var mbdm = "<%=MBDM %>";
                    var sessionID = "<%=SessionID %>";
                    var bnyy = "<%=GSYEAR %>", bnnn = "<%=GSMONTH %>", fabs = "<%=FALB %>", loginyy = "<%=YY %>", fadm = "<%=FADM %>", fadm2 = "<%=FADM2 %>";
                    var surl = 'MB.ashx?action=getrowcol&JHFADM=' + jhfadm + '&MBDM=' + mbdm + '&SessionID=' + sessionID + '&bnyy=' + bnyy + '&bnnn=' + bnnn + '&fabs=' + fabs + '&loginyy=' + loginyy + '&fadm=' + fadm + '&fadm2=' + fadm2 + '';
                    po.poctrl.EnableExcelCalling();
                    var shint = '正在获取后台行列属性，请稍候......';
                    dialogshowwin();
                    dialogshow(shint, true);
                    //请求获取行列属性数据：
                    $.ajax({
                        type: 'get',
                        url: surl,
                        cache: false,
                        success: function (result) {
                            shint = '当前数据文件回写行列属性......';
                            dialogshow(shint);
                            result = result.replace(/\r/g, "").replace(/\n/g, "");
                            try {
                                var sucjson = eval("(" + result + ")");
                            } catch (e) {
                                BmShowWinError('公式解析错误!', result);
                                po.UpdateAndVis(false);
                            }
                            if (sucjson) {
                                if (parseInt(sucjson["count"]) > 0) {
                                    if (!po.writerowcol(sucjson)) {
                                        dialogclose();
                                        alert("行列属性回写错误！");
                                    } else {
                                        dialogclose();
                                        alert('行列属性回写完毕！');
                                    }
                                }
                            }
                            dialogclose();
                            po.UpdateAndVis(true);
                        },
                        error: function (req) {
                            dialogclose();
                            BmShowWinError('行列属性获取错误!', req.responseText);
                        }
                    });
                }
                else {
                    po.UpdateAndVis(true);
                }
            }, function () {
                po.UpdateAndVis(true);
            });
        }

        //全区域计算：
        function AllZoneCal() {
            if ('<%=MBLB %>' == 'YSTZ') {
                QQYJS();
            }
            else {
                //请求当前选中区域数据解析结果：
                $.messager.confirm('提示：', '是否重新计算所有填报页数据?', function (r) {
                    if (r) 
                    {
                        QQYJS();
                    }
                    else 
                    {
                        po.UpdateAndVis(true);
                    }
                }, function () {
                    po.UpdateAndVis(true);
                });
            }
        }
        function UpdateExcelRows(IsQYJS, _addr, _async, _isconfirm, _fn) {
            var C,Arr3=[];
            var CI = 0;
            $.ajax({
                type: 'get',
                url: 'DynamicRowHandler.ashx?action=UpdateRows&FADM=<%=FADM %>&FADM2=<%=FADM2 %>&FABS=<%=FALB %>&MBDM=<%=MBDM %>&GSYY=<%=GSYEAR %>&GSNN=<%=GSMONTH %>&YY=<%=YY %>&SessionID=<%=SessionID %>',
                cache: false,
                success: function (result) {
                    if (result != "") {
                        _json = JSON.parse(result);
                        po.unprotectallsheet();
                        var icount = _json["count"];
                        var idx = -1;
                        var DataFormat = "";
                        var Arr = new Array();
                        var RowIndex = -1;
                        var SHEETNAME = "", LastSheetName = "", DataFormat = "", LastSheet;
                        for (var i = 0; i < icount; i++) {
                            var jsonrow = _json["grids"][i];
                            SHEETNAME = jsonrow["name"];
                            var sheet = po.getsheetbyname(SHEETNAME);
                            if (sheet) {
                                if (LastSheetName == "") {
                                    Arr = po.getDynamicRows(sheet);
                                }
                                else {
                                    if (SHEETNAME != LastSheetName) {
                                        if (Arr.length > 0) {
                                            //删除不存在的变动行
                                            LastSheet = po.getsheetbyname(LastSheetName);
                                            po.DeleteNotExistDynamicRows(LastSheet, Arr);
                                        }
                                        Arr = po.getDynamicRows(sheet);
                                    }
                                }
                                LastSheetName = SHEETNAME;
                                if (Arr.length > 0) {
                                    var t = 0;
                                    var F = false;
                                    var Arr2 = JSON.parse("{\"" + jsonrow["DM"].toString().replace(/,/g, '\",\"').replace(/:/g, '\":\"') + "\"}");
                                    RowIndex = po.IsSameRowProperty(jsonrow["RPI"], Arr, Arr2);
                                    if (RowIndex != -1) {

                                        if (po.IsDoRowLock(Arr3, SHEETNAME, RowIndex) == false) {
                                            CI = po.usedrcolscnt(sheet);
                                            C = num2col(CI);
                                            sheet.Range("B" + RowIndex.toString() + ":" + C + RowIndex.toString()).Interior.Color = parseInt("C1FFC1", 16);
                                            sheet.Range("B" + RowIndex.toString() + ":" + C + RowIndex.toString()).Locked = "False";

                                            Arr3.push([SHEETNAME, RowIndex]);
                                        }
                                        Cell = sheet.Cells(RowIndex, parseInt(jsonrow["col"]));
                                        Cell.Value = jsonrow["MC"];
                                        DataFormat = jsonrow["nfl"];
                                        if (DataFormat != null && DataFormat.toString() != "") {
                                            Cell.NumberFormatLocal = DataFormat.substring(0, DataFormat.length - 1);
                                        }
                                        Cell.Locked = "True";
                                    }
                                    else {
                                        //说明之前的存在的变动行不存在此项记录
                                        Arr = po.setDynamicCellValue(sheet, jsonrow, Arr);
                                    }
                                }
                                else {
                                    //说明之前的存在的变动行不存在此项记录
                                    Arr = po.setDynamicCellValue(sheet, jsonrow, Arr);
                                }
                            }
                        }
                        //删除最后一个Sheet页中的废弃的变动行
                        if (Arr.length > 0) {
                            //删除不存在的变动行
                            LastSheet = po.getsheetbyname(LastSheetName);
                            po.DeleteNotExistDynamicRows(LastSheet, Arr);
                        }
                        //获取Excel中添加的变动行拼接的Json字符串
                        var rtnJson = po.getDynamicRowsJson("<%=MBDM %>");
                        //插入变动行到缓存中
                        InsertDynamicRows(rtnJson, IsQYJS, _addr, _async, _isconfirm, _fn);
                    }
                    else {
                        //获取Excel中添加的变动行拼接的Json字符串
                        var rtnJson = po.getDynamicRowsJson("<%=MBDM %>");
                        //插入变动行到缓存中
                        InsertDynamicRows(rtnJson, IsQYJS, _addr, _async, _isconfirm, _fn);
                    }
                },
                error: function (req) {
                    dialogclose();
                    BmShowWinError('变动行区域计算异常!', req.responseText);
                }
            });
        }
        //全区域计算
        function AllZoneJS() {
            var surl = 'MB.ashx?action=allzonecal&gsyy=<%=GSYEAR %>&yy=<%=YY %>&gsnn=<%=GSMONTH %>&fadm=<%=FADM %>&fadm2=<%=FADM2 %>&fabs=<%=FALB %>&wjm=<%=MBWJ%>&LB=<%=MBLB %>&MBDM=<%=MBDM %>&SessionID=<%=SessionID %>';
            po.UpdateAndVis(false);
            po.poctrl.EnableExcelCalling();
            var shint = '正在计算区域数据，请稍候......';
            dialogshowwin();
            dialogshow(shint, true);
            //请求区域计算数据：
            $.ajax({
                type: 'get',
                url: surl,
                cache: false,
                success: function (result) {
                    shint = '回写区域计算数据......';
                    dialogshow(shint);
                    result = result.replace(/\r/g, "").replace(/\n/g, "");
                    try {
                        var sucjson = JSON.parse(result);
                    } catch (e) {
                        BmShowWinError('公式解析错误!', result);
                        po.UpdateAndVis(false);
                    }
                    if (sucjson) {
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!po.writezone(sucjson, "<%=YY %>", "<%=NN %>", "<%=JD %>", "<%=FALB %>", bnyy.toString(), bnnn.toString(), "<%=YSYF %>")) {
                                alert("区域计算回写错误！");
                            }
                            else {
                                //预算调整模板打开页面后，进行全区域计算，最后保存
                                if ('<%=MBLB %>' == 'YSTZ') {
                                    //保存
                                    SaveUpClick();
                                }
                                else {
                                    alert('区域计算数据生成完毕！');
                                }
                            }
                        }
                        else {
                            //预算调整模板打开页面后，进行全区域计算，最后保存
                            if ('<%=MBLB %>' == 'YSTZ') {
                                //保存
                                SaveUpClick();
                            }
                            else {
                                alert('区域计算数据生成完毕！');
                            }
                        }
                    }
                    dialogclose();
                    po.UpdateAndVis(true);
                },
                error: function (req) {
                    dialogclose();
                    BmShowWinError('请求区域计算数据错误!', req.responseText);
                }
            });
            po.UpdateAndVis(true);
        }
        function QQYJS() {
            UpdateExcelRows(2);
        }

        //区域计算：
        function ZoneCal(_async, _isconfirm, _addr) {
            po.setvisible(false);
            //使用区域与选中区域叠加区域：
            try {
                var app = po.application();
                if (_addr != undefined) {
                    var rag = app.Intersect(_addr, po.UsedRange());
                } else {
                    var rag = app.Intersect(app.selection, po.UsedRange());
                }
                var addr = rag.Address;
                alert(addr);
            }
            catch (err) {
                alert('Excel获取选中区域出错！');
            }

            //请求当前选中区域数据解析结果：
            if (_isconfirm == false) {
                dealZoneCal(addr, _async, _isconfirm);
            } else {
                $.messager.confirm('提示：', '是否重新计算当前选定数据?', function (r) {
                    if (r) {
                        dialogshowwin();
                        dealZoneCal(addr, true, true, function () {
                            po.UpdateAndVis(true);
                            dialogclose();
                        });
                    } else {
                        po.UpdateAndVis(true);
                    }
                }, function () {
                    po.UpdateAndVis(true);
                });
            }
        }
        //插入变动行
        //rtnJson: 要插入的变动行Json
        //IsQYJS:true代表是区域计算；false代表的是全区域计算
        function InsertDynamicRows(rtnJson, IsQYJS, _addr, _async, _isconfirm, _fn) {
            $.ajax({
                type: 'post',
                url: "DynamicRowHandler.ashx",
                data: {
                    MB: rtnJson,
                    action: "Insert",
                    MBDM: "<%=MBDM %>",
                    JHFADM: "<%=FADM %>",
                    SessionID: "<%=SessionID%>"
                },
                cache: false,
                success: function (result) 
                {
                    if (result == "OK") 
                    {
                        if (IsQYJS == 1) 
                        {
                            QYJS(_addr, _async, _isconfirm, _fn);
                        }
                        else if (IsQYJS == 2) 
                        {
                            AllZoneJS();
                        }
                        else if (IsQYJS == 3) 
                        {
                            //获取行列属性
                            GetRowAndColProperty();
                        }
                    }
                    else 
                    {
                        alert("初始化Excel数据异常!");
                    }
                },
                error: function (req) {
                    dialogclose();
                    BmShowWinError('初始化Excel数据异常!', req.responseText);
                }
            });
        }

        function dealZoneCal(_addr, _async, _isconfirm, _fn) {
            UpdateExcelRows(1, _addr, _async, _isconfirm, _fn);
        }

        //区域计算
        function QYJS(_addr, _async, _isconfirm, _fn) 
        {
            var url = 'MB.ashx?action=zonecal&gsyy=<%=GSYEAR %>&yy=<%=YY %>&gsnn=<%=GSMONTH %>&fadm=<%=FADM %>&fadm2=<%=FADM2 %>&fabs=<%=FALB %>&adr=' +
                             _addr + '&wjm=<%=MBWJ%>&MBDM=<%=MBDM %>&SessionID=<%=SessionID %>';
            po.poctrl.EnableExcelCalling();
            var shint = '正在计算区域数据，请稍候......';
            dialogshow(shint, true);
            if (_async != false) {
                _async = true;
            }
            //sheet页名字字符串：
            var sheetname = "{\"sheetname\":\"" + po.activesheet().Name + "\"}";
            ajaxpost(url, _async, sheetname, function () {
                //                debugger;
                if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                    // 本地提示：加载中/处理中 
                }
                else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    if (xmlHttp.responseText != '') {
                        try {
                            shint = '回写区域计算数据......';
                            dialogshow(shint);
                            var sucjson = eval('(' + xmlHttp.responseText + ')');
                        } catch (e) {
                            BmShowWinError('回写区域计算数据错误!', xmlHttp.responseText);
                            po.UpdateAndVis(false)
                        }
                        if (sucjson) {
                            if (parseInt(sucjson["count"]) > 0) {
                                if (!po.writezone(sucjson, "<%=YY %>", "<%=NN %>", "<%=JD %>", "<%=FALB %>", bnyy.toString(), bnnn.toString(), "<%=YSYF %>")) {
                                    alert("区域计算回写错误！");
                                }
                            }
                            if (_isconfirm == true) {
                                alert('区域计算数据生成完毕！');
                            };
                        }

                    }
                    if (_fn != undefined) {
                        _fn();
                    }
                } else {
                    dialogclose();
                    BmShowWinError('请求区域计算数据错误!', xmlHttp.responseText);

                }
            });
        }

        //上报、审批、打回、保存权限获取：
        function WinJS(Flag, BZ) {
            var HidIsOnlyOne = $("#HidIsOnlyOne");
            if (Flag == true) {
                getlist();
                if (HidIsOnlyOne.val() == "1") {
                    var CJ = MB.GetCJBMByJHFADM('<%=FADM %>', '<%=MBDM %>', '<%=MBLB %>').value;
                    if (CJ != "") {
                        var nodes = $("#tt").tree("getChecked", "unchecked");
                        for (var j = 0; j < nodes.length; j++) {
                            if (nodes[j].attributes.CJBM != CJ) {
                                $("#tt").tree("check", nodes[j].target);
                                break;
                            }
                        }
                        WinJS(false, 1);
                    }
                    HidIsOnlyOne.val("");
                }
                else {
                    if (HidIsOnlyOne.val() == "0") {
                        HidIsOnlyOne.val("");
                        alert("您没有要打回的模板！");
                        return;
                    }
                    else {
                        $("#WinJS").window('open');
                    }
                }
            }
            else {
                var nodes = $("#tt").tree("getChecked");
                if (BZ == -1) {
                    $("#WinJS").window('close');
                }
                else {
                    if (nodes.length == 0) {
                        alert("请选择要打回的模板！");
                        return;
                    }
                    else {
                        var DH = "", MB1 = "", JS = "", CJ = "";
                        for (var i = 0; i < nodes.length; i++) {
                            MB1 = nodes[i].id;
                            CJ = nodes[i].attributes.CJBM;
                            JS = nodes[i].attributes.JSDM;
                            DH = DH == "" ? MB1 + "&" + JS + "&" + CJ : DH + "|" + MB1 + "&" + JS + "&" + CJ;
                        }
                        //提交打回审批
                        if (DH != "") {
                            $("#HidDH").val(DH);
                        }
                        //                        TJ('3');
                        $("#WinJS").window('close');
                        $("#HidTJ").val("3");
                        OpenSmqr("0");
                    }
                }
            }
        }

        function matchcs(re, s) {
            re = eval("/" + re + "/g")
            return s.match(re).length;
        }
        var C2, C3, P1, C1;
        //获取打回列表：
        function getlist() {
            var rtnstr = MB.GetDHMBStr('<%=FADM %>', '<%=MBDM %>', '<%=MBLB %>').value;
            if (rtnstr != "" && rtnstr != null) {
                var Len = matchcs("\"id\"", rtnstr);
                if (Len == 1) {
                    $("#HidIsOnlyOne").val("0");
                }
                else if (Len == 2) {
                    $("#HidIsOnlyOne").val("1");
                }
                var Data = eval("(" + rtnstr + ")");
                $("#tt").tree("loadData", Data);
            }
            $("#tt").tree({
                lines: true,
                cascadeCheck: false,
                onCheck: function (node, checked) {
                    if (checked == true) {
                        var CJ = MB.GetCJBMByJHFADM('<%=FADM %>', '<%=MBDM %>', '<%=MBLB %>').value;
                        if (CJ != "") {
                            if (node.attributes.CJBM == CJ) {
                                alert("模板不能打回到自己！");
                                $("#tt").tree("uncheck", node.target);
                                return;
                            }
                        }
                        var nodes = $("#tt").tree("getChecked");
                        if (nodes.length > 0) {
                            C3 = MB.GetMBANDJS(node.attributes.CJBM.substring(0, 4)).value;
                            C2 = MB.GetCJBM(node.attributes.JSDM, node.id, node.attributes.CJBM, "<%=FADM%>").value;
                            C1 = MB.GetChird(node.attributes.CJBM, "<%=FADM%>").value;
                            P1 = MB.GetPar(node.attributes.CJBM.substring(0, 4), "<%=FADM%>").value;
                        }
                        for (var i = 0; i < nodes.length; i++) {
                            if (node != nodes[i]) {
                                if (node.attributes.CJBM.indexOf(nodes[i].attributes.CJBM) > -1 || nodes[i].attributes.CJBM.indexOf(node.attributes.CJBM) > -1) {
                                    alert("同一个根节点下不能选取多个模板！");
                                    $("#tt").tree("uncheck", node.target);
                                    return;
                                }
                                else if (node.attributes.JSDM == nodes[i].attributes.JSDM && node.id == nodes[i].id) {
                                    alert("不能选取相同模板！");
                                    $("#tt").tree("uncheck", node.target);
                                    return;
                                }
                                else {
                                    if (C2 != "") {
                                        var BM = C2.split(',');
                                        for (var j = 0; j < BM.length; j++) {
                                            if (BM[j].toString().indexOf(nodes[i].attributes.CJBM) > -1 || nodes[i].attributes.CJBM.indexOf(BM[j].toString()) > -1) {
                                                alert("同一模板以及子节点不能选取多个！");
                                                $("#tt").tree("uncheck", node.target);
                                                return;
                                            }
                                        }
                                    }
                                    else {
                                        if (node.attributes.CJBM.length != 4) {
                                            if (C3 != "") {
                                                var DM = C3.split('@');
                                                if (DM[0].toString() == nodes[i].attributes.JSDM && DM[1].toString() == nodes[i].id) {
                                                    alert("同一模板以及子节点不能选取多个！");
                                                    $("#tt").tree("uncheck", node.target);
                                                    return;
                                                }
                                            }

                                        }
                                        if (C1 != "") {
                                            var BM = C1.split(',');
                                            for (var j = 0; j < BM.length; j++) {
                                                if (BM[j].toString().indexOf(nodes[i].attributes.CJBM) > -1 || nodes[i].attributes.CJBM.indexOf(BM[j].toString()) > -1) {
                                                    alert("同一模板以及子节点不能选取多个！");
                                                    $("#tt").tree("uncheck", node.target);
                                                    return;
                                                }
                                            }
                                        }
                                        if (P1 != "") {
                                            var BM = P1.split(',');
                                            for (var j = 0; j < BM.length; j++) {
                                                if (BM[j].toString().indexOf(nodes[i].attributes.CJBM) > -1) {
                                                    alert("同一模板以及子节点不能选取多个！");
                                                    $("#tt").tree("uncheck", node.target);
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var F = MB.GetYDHMB('<%=FADM %>', node.attributes.CJBM).value;
                        if (F == true) {
                            alert("此模板的子节点模板已经被打回，父模板不能被打回！");
                            $("#tt").tree("uncheck", node.target);
                            return;
                        }
                    }
                }
            });
        }

        /// <summary>处理批复状态</summary>
        function DealRespondStatus() {
            //更新批复状态：
            $.ajax({
                type: 'post',
                url: 'MB.ashx?action=dealrespondstatus&jhfadm=<%=FADM %>&mbdm=<%=MBDM %>&userid=<%=USER_ID%>',
                cache: false,
                error: function (req) {
                    BmShowWinError('更新批复状态错误！', req.responseText);
                }
            });
        }

        //保存前先校验
        function CheckAndSave() {
//            var CANDATAUP = '<%=CANDATAUP %>';
//            var CANDATAAPP = '<%=CANDATAAPP %>';
            if ('<%=CANDATAUP %>' == '1') {
                //如果是"批复预算基础数据维护"
                if ('<%=MBLB %>' == 'respfj') {
                    DealRespondStatus();
                }
                //如果是生产方案模板：
                if ('<%=MBLX %>' == '1') {
                    CheckFamb('1');
                }
                else {
                    checkformula('1');
                }
            }
            if ('<%=CANDATAAPP %>' == '1') {
                DataApp();
            }
        }

        //上报：
        function DataUp() {
             state = 1;
            //如果是"批复预算基础数据维护"
            if ('<%=MBLB %>' == 'respfj') {
                DealRespondStatus();
            }
            //如果是生产方案模板：
            if ('<%=MBLX %>' == '1') {
                IsSave("CheckFamb('1')");
            }
            else {
                IsSave("checkformula('1')");
            }
        }

        //审批：
        function DataApp() {
             state = 2;
            //            debugger;
            if ('<%=MBLX %>' == '1') {
                IsSave("CheckFamb('2')");
            }
            else {
                IsSave("checkformula('2')");
            }
        }

        //打回：
        function DataBack() {
            WinJS(true, 0);
        }

        //上报流程函数：
        function TJ(_flag) {
            var hint = '是否确认执行';
            po.setvisible(false);
            if (_flag == '1') {
                hint += '上报';
            } else if (_flag == '2') {
                hint += '审批';
            } else if (_flag == '3') {
                hint += '打回';
            }
            hint += '?';
            $.messager.confirm('提示：', hint, function (r) {
                if (r) {
                    var rtn = MB.AddSH("<%=FADM%>", "<%=MBDM%>", _flag, $("#HidDH").val(), "<%=MBLB%>", $('#txta').val(), smqr, "<%=SFZLC%>").value;
                    $("#HidDH").val("");
                    if (rtn >= 1) {
                        alert("操作成功！");
                        $("#HidTJ").val("");
                    } else if (rtn == 0) {
                        alert("操作失败！");
                    } else if (rtn == -1) {
                        alert("您没有权限！");
                    } else if (rtn == -2) {
                        alert("预算流程没有发起！");
                    } else if (rtn == -3) {
                        alert("当前数据已经存在，不能重复提交！");
                    } else if (rtn == -4) {
                        alert("操作模板作为其他模板的子节点时，角色设置不一致！");
                    }
                    window.location.reload();
                    po.UpdateAndVis(true);
                } else {
                    po.UpdateAndVis(true);
                }
            }, function () {
                po.UpdateAndVis(true);
            });
        }

        /// <summary>根据模板设置计算列</summary>
        function calcolsbytemplet() {
            po.UpdateAndVis(false)
            //3：编制,4:批复,5：分解
            var mblx = '0';
            if ('<%=MBLB %>' == 'basesb' || '<%=MBLB %>' == 'basesp' || '<%=MBLB %>' == 'baseys') {
                mblx = '3';
                //批复:
            } else if ('<%=MBLB %>' == 'respfj' || '<%=MBLB %>' == 'respsp' || '<%=MBLB %>' == 'respys') {
                mblx = '4';
                //分解:
            } else if ('<%=MBLB %>' == 'resosb' || '<%=MBLB %>' == 'resosp' || '<%=MBLB %>' == 'resoys') {
                mblx = '5';
            }
            if (mblx != '0') {
                $.ajax({
                    type: 'get',
                    url: 'MB.ashx?action=calcolsbytemplet&mbdm=<%=MBDM %>&mblx=' + mblx,
                    cache: false,
                    async: false,
                    success: function (result) {
                        //计算指定列：
                        if (result != '') {
                            try {
                                var calcols = eval('(' + result + ')');
                            } catch (e) {
                                BmShowWinError('公式解析错误!', result);
                                po.UpdateAndVis(false)
                            }
                            po.poctrl.EnableExcelCalling();
                            for (var i = 0; i < calcols['sheets'].length; i++) {
                                //                                debugger;
                                var sheetname = calcols['sheets'][i]["name"];
                                var cols = calcols['sheets'][i]["cols"];
                                var sheet = po.getsheetbyname(sheetname);
                                if (sheet != null) {
                                    if (cols != '') {
                                        var arycol = cols.split(',');
                                        var strcols = '';
                                        for (var j = 0; j < arycol.length; j++) {
                                            if (strcols == '') {
                                                strcols = arycol[j] + ':' + arycol[j];
                                            }
                                            else {
                                                strcols += ',' + arycol[j] + ':' + arycol[j];
                                            }
                                        }
                                        //此处区域计算不提示计算完成：
                                        sheet.Activate();
                                        // var Add = sheet.Range(strcols);
                                        ZoneCal(false, false, sheet.Range(strcols));
                                    }
                                } else {
                                    alert('标签页：【' + sheetname + '】不存在！');
                                }
                            }
                            //根据公式隐藏列：
                            dialogshow('按公式隐藏列......');
                            hidecolbyformula();
                        } else {
                            po.UpdateAndVis(true);
                        }

                    },
                    error: function (req) {
                        BmShowWinError('获取按公式隐藏列错误！', req.responseText);
                    }
                });
            }
        }

        /// <summary>根据公式隐藏列</summary>
        function hidecolbyformula() {
            //0：填报，1：分解,2:批复
            var mblx = '0';
            //分解:
            if ('<%=MBLB %>' == 'resosb' || '<%=MBLB %>' == 'resosp' || '<%=MBLB %>' == 'resoys') {
                mblx = '1';
                //批复:
            } else if ('<%=MBLB %>' == 'respfj' || '<%=MBLB %>' == 'respsp' || '<%=MBLB %>' == 'respys') {
                mblx = '2';
            }
            //sheet页名字字符串：
            var sheetnames = "{\"MBDM\":\"<%=MBDM %>\",\"USERID\":\"<%=USER_ID%>\",\"MBLX\":\"" + mblx
            + "\",\"NN\":\"<%=NN %>\",\"SHEETNAMES\":\"" + po.getsheetnames() + "\",\"HSZXDM\":\"<%=HSZXDM %>\",\"YY\":\"<%=YY %>\"}";
            var url = 'MB.ashx?action=hidecolbyformula';
            ajaxpost(url, false, sheetnames, function () {
                //                debugger;
                if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                    // 本地提示：加载中/处理中 
                }
                else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    //隐藏指定列：
                    if (xmlHttp.responseText != '') {
                        try {
                            var hideformula = eval('(' + xmlHttp.responseText + ')');
                            //                            po.setvisible(false);
                            po.hidecolbyformula(hideformula);
                            po.UpdateAndVis(true);
                        } catch (e) {
                            BmShowWinError('公式解析错误!', xmlHttp.responseText);
                            po.UpdateAndVis(false)
                        }
                    } else {
                        po.UpdateAndVis(true);
                    }
                } else {
                    BmShowWinError('获取按公式隐藏列错误！', xmlHttp.responseText);
                    po.UpdateAndVis(false)
                }
            });
            //            $.ajax({
            //                type: 'post',
            //                url: 'MB.ashx?action=hidecolbyformula&mbdm=<%=MBDM %>&userid=<%=USER_ID%>&mblx='
            //                 + mblx + '&nn=<%=NN %>',
            //                data: {
            //                    sheetnames: po.getsheetnames()
            //                },
            //                cache: false,
            //                async: false,
            //                success: function (result) {
            //                    //隐藏指定列：
            //                    if (result != '') {
            //                        try {
            //                            var hideformula = eval('(' + result + ')');
            //                            po.hidecolbyformula(hideformula);
            //                        } catch (e) {
            //                            BmShowWinError('公式解析错误!', result);
            //                            po.setvisible(false);
            //                            po.ScreenUpdating(false);
            //                        }
            //                    }
            //                },
            //                error: function (req) {
            //                    BmShowWinError('获取按公式隐藏列错误！', req.responseText);
            //                    po.setvisible(false);
            //                    po.ScreenUpdating(false);
            //                }
            //            });
        }

        //按默认颜色解锁同时隐藏各标签页中第一行第一列：
        function AfterOpen(_issaved) {
            //如果不是本地打开：
            if (!localopen) 
            {
                //判断当前excel版本：
                if (po.version() < 11) 
                {
                    alert('当前Excel版本为：' + po.excelversion() + '，版本过低请安装Excel 2003及以上版本！');
                }
                else {
                    dialogshowwin();
                    dialogshow('保护填报页......');
                    //如果不是生产方案类型模版：
                    if ("<%=MBLX %>" != "1") {
                        dialogshow('隐藏首行首列......');
                        po.hideallrowcol();
                        dialogshow('隐藏无效列......');
                        //根据月份隐藏列：
                        po.hiddenmonthcol("<%=NN%>", "<%=FALB%>");
                        //如果当前是不能保存的则锁定所有标签页：
                        if ('<%=CANSAVE%>' != '1') {
                            po.lockworkbook();
                        }
                        else {
                            po.lockallsheet();
                        }
                        //根据模板计算列，回调按公式隐藏列：
                        calcolsbytemplet();
                        dialogshow('锁定相关单元格......');
                        po.protectworkbook();
                        
                    }
                    else {
                        dialogshow('隐藏首列......');
                        po.hidefirstcol(po.firstsheet());
                        dialogshow('锁定相关单元格......');
                        po.lockfirstsheet();
                    }
                    dialogclose();
                    po.UpdateAndVis(true);
                    //如果是打开上报文件，当前状态先设置为已保存；如果是打开模板文件，当前状态设置为未保存：
                    if (_issaved == true) 
                    {
                        po.poctrl.Document.Saved = true;
                    }
                    else 
                    {
                        po.poctrl.Document.Saved = false;
                    }
                    po.application().DisplayFormulaBar = true;
                    //预算调整模板打开页面后，进行全区域计算，最后保存
                    if ('<%=MBLB %>' == 'YSTZ') {
                        //判断文件是否存在。存在的话，则进行全区域计算；否则进行保存操作。
                        if (IsExistFile) {
                            //全区域计算
                            AllZoneCal(); 
                        }
                        else {
                            //保存
                            SaveUpClick();
                        }
                    }
                }
            }
            else 
            {
                po.lockallsheet();
            }
        }

        //在现有模板基础上追加投入产出数据：
        function InsertTrCc() {
            var firstsheet = po.firstsheet();
            po.setvisible(false);
            $.messager.confirm('提示：', '是否根据数据库内容变更【' + firstsheet.Name + '】模板数据?', function (r) {
                if (r) {
                    po.UpdateAndVis(false);
                    po.poctrl.EnableExcelCalling();
                    dialogshowwin();
                    dialogshow('正在处理数据，请稍候......', true);
                    dialogshow('获取投入产出数据......');
                    //请求投入产出模板数据：
                    $.ajax({
                        type: 'get',
                        url: 'MB.ashx?action=inittrcc&hszxdm=<%=HSZXDM %>',
                        cache: false,
                        success: function (result) {
                            dialogshow('回写投入产出数据......');
                            result = result.replace(/\r/g, "").replace(/\n/g, "");
                            try {
                                var sucjson = eval("(" + result + ")");
                            } catch (e) {
                                BmShowWinError('公式解析错误!', result);
                                po.ScreenUpdating(false);
                            }
                            if (sucjson) {
                                if (parseInt(sucjson["count"]) > 0) {
                                    if (!po.inserttrcc(sucjson, firstsheet)) {
                                        alert("投入产出回写错误！");
                                    }
                                    dialogshow('设置投入产出合计公式......');
                                    if (!po.settrccsum(firstsheet)) {
                                        alert("设置投入产出公式错误！");
                                    }
                                    dialogshow('投入产出数据生成完毕！');
                                    po.hidefirstcol(firstsheet);
                                    firstsheet.Activate();
                                }
                            }
                            dialogclose();
                            po.UpdateAndVis(true);
                        },
                        error: function (req) {
                            firstsheet.Activate();
                            dialogclose();
                            BmShowWinError('请求投入产出数据错误', req.responseText);
                        }
                    });

                }
                else {
                    po.setvisible(true);
                }
            }, function () {
                po.UpdateAndVis(true);
                //                po.ScreenUpdating(true);
                //                po.setvisible(true);
            });
        }

        //生成投入产出数据：
        function InitTrCc() {
            var firstsheet = po.firstsheet();
            po.setvisible(false);
            $.messager.confirm('提示：', '清空【' + firstsheet.Name + '】模板数据?', function (r) {
                if (r) {
                    po.UpdateAndVis(false);
                    po.poctrl.EnableExcelCalling();
                    dialogshowwin();
                    dialogshow('正在处理数据，请稍候......', true);
                    dialogshow('清空【' + firstsheet.Name + '】模板页......');
                    po.clearsheet(firstsheet);
                    dialogshow('获取投入产出数据.....');
                    //请求投入产出模板数据：
                    $.ajax({
                        type: 'get',
                        url: 'MB.ashx?action=inittrcc&hszxdm=<%=HSZXDM %>',
                        cache: false,
                        success: function (result) {
                            dialogshow('回写投入产出数据......');
                            result = result.replace(/\r/g, "").replace(/\n/g, "");
                            try {
                                var sucjson = eval("(" + result + ")");
                            } catch (e) {
                                BmShowWinError('公式解析错误!', result);
                                po.UpdateAndVis(false);
                            }
                            if (sucjson) {
                                if (parseInt(sucjson["count"]) > 0) {
                                    if (!po.inittrcc(sucjson, firstsheet)) {
                                        alert("投入产出回写错误！");
                                    }
                                    dialogshow('设置投入产出合计公式......');
                                    if (!po.settrccsum(firstsheet)) {
                                        alert("设置投入产出公式错误！");
                                    }
                                    dialogshow('投入产出数据生成完毕！');
                                    po.hidefirstcol(firstsheet);
                                    firstsheet.Activate();
                                }
                            }
                            dialogclose();
                            po.setvisible(true);
                        },
                        error: function (req) {
                            firstsheet.Activate();
                            dialogclose();
                            po.setvisible(true);
                            BmShowWinError('请求投入产出数据错误', req.responseText);
                        }
                    });
                    po.ScreenUpdating(true);
                }
                else {
                    po.setvisible(true);
                }
            }, function () {
                po.UpdateAndVis(true);
            });
        }

        //保存投入产出数据：
        function SaveTrCc(_fn) {
            //获取到数据json：
            dialogshowwin();
            dialogshow('正在获取投入产出数据......', true);
            var trccjson = po.gettrccjson(po.firstsheet());
            if (trccjson) {
                dialogshow('正在保存投入产出数据......');
                ajaxpost('MB.ashx?action=savetrcc&hszxdm=<%=HSZXDM %>&fadm=<%=FADM%>', true, JSON.stringify(trccjson), function () {
                    if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                        // 本地提示：加载中/处理中 
                    }
                    else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        //如果对于生产方案模板的填报中后几个标签页也存在填报页，则放开此注释同时注释掉该function中SaveData(true);以下代码：
                        var filename = getsavefilename("<%=FADM %>","<%=MBWJ%>");
                        $("#HidSaveFileName").val(filename);
                        po.poctrl.WebSave();
                        dialogclose();
                        po.protectsheet(po.firstsheet());
                        alert(xmlHttp.responseText);
                        po.UpdateAndVis(true);
                        if (IsCheckFormula(_fn)) {
                            eval(_fn);
                        }
                        po.poctrl.Document.Saved = true;
                    } else {
                        dialogclose();
                        po.UpdateAndVis(true);
                        BmShowWinError('投入产出数据保存错误!', xmlHttp.responseText);

                    }
                });
                //                $.ajax({
                //                    type: 'post',
                //                    url: 'MB.ashx?action=savetrcc&hszxdm=<%=HSZXDM %>&fadm=<%=FADM%>',
                //                    data: trccjson,
                //                    dataType: 'html',
                //                    success: function (result) {
                //                        //如果对于生产方案模板的填报中后几个标签页也存在填报页，则放开此注释同时注释掉该function中SaveData(true);以下代码：
                //                        var filename = getsavefilename("<%=FADM %>", "<%=MBWJ%>");
                //                        $("#HidSaveFileName").val(filename);
                //                        po.poctrl.WebSave();
                //                        po.ScreenUpdating(true);
                //                        dialogclose();
                //                        po.protectsheet(po.firstsheet());
                //                        alert(result);
                //                        po.setvisible(true);
                //                        if (IsCheckFormula(_fn)) {
                //                            eval(_fn);
                //                        }
                //                        po.poctrl.Document.Saved = true;
                //                    },
                //                    error: function (req) {
                //                        po.ScreenUpdating(true);
                //                        dialogclose();
                //                        po.setvisible(true);
                //                        BmShowWinError('投入产出数据保存错误!', req.responseText);
                //                    }
                //                });
            } else {
                dialogclose();
                po.UpdateAndVis(true);
                //                po.ScreenUpdating(true);
                //                po.setvisible(true);
            }
        }

        //文件保存完毕后：
        function AfterSave() {
            po.setvisible(true);
            //hasopenfile = null;
            var FileName = getsavefilename("<%=FADM %>", "<%=MBWJ%>");
            $.ajax({
                type: 'get',
                url: 'MB.ashx',
                data: {
                    action: "SaveExcelToDataBase",
                    MBDM: "<%=MBDM%>",
                    MBWJ: FileName,
                    FADM: "<%=FADM %>"
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != "") {
                    }
                    else {
                        alert("数据保存异常，请重新保存！");
                    }
                },
                error: function () {
                    alert("校验公式有错误!");
                }
            });
            //预算调整后，自动关闭！调用下一个模板打开事件
            if ('<%=MBLB %>' == 'YSTZ') {
                var YSTab = window.parent.frames["IfrImage"];
                YSTab.YSIndex = YSTab.YSIndex + 1;
                if (YSTab.YSIndex <= YSTab.YSArr.length - 1) {
                    //调用下一个模板
                    YSTab.AutoYSTZ();
                }
                else {
                    //关闭PageOffice
                    document.getElementById("PageOfficeCtrl1").close();
                    //关闭预算自动调整Tab页
                    window.parent.CloseYSTab();
                    window.external.close();
                }
            }
        }
//        function AddDynamicRow() {
//            var SheetName = po.activesheet().Name;
//            $.ajax({
//                type: 'POST',
//                url: 'DynamicRowHandler.ashx?action=Insert',
//                data: {
//                    JHFADM: "10050433",
//                    MBDM: "10160876",
//                    SHEETNAME: SheetName,
//                    SessionID: "<%=SessionID%>",
//                    MB: "[{\"RowIndex\":18,\"Cols\":[{\"ColumnIndex\":1,\"VALUE\":\"QWER\"}]},{\"RowIndex\":19,\"Cols\":[{\"ColumnIndex\":1,\"VALUE\":\"QWER\"}]},{\"RowIndex\":31,\"Cols\":[{\"ColumnIndex\":1,\"VALUE\":\"LJF\"},{\"ColumnIndex\":4,\"GS\":\"N6+N8\"}]}]"
//                },
//                async: true,
//                cache: false
//            });
//        }

        //关闭模板：
        function CloseMb() {
            po.poctrl.EnableExcelCalling();
            po.UpdateAndVis(false);
            //            po.ScreenUpdating(false);
            //            po.setvisible(false);
            if (po.poctrl.FullScreen) {
                po.poctrl.FullScreen = false;
            }
            $.messager.confirm('提示：', '当前文件尚未保存，是否要关闭?', function (r) {
                if (r) {
                    //                    po.setvisible(true);
                    //                    po.ScreenUpdating(true);
                    document.getElementById("PageOfficeCtrl1").close();
                    window.external.close();
                } else {
                    po.UpdateAndVis(true);
                    //                    po.ScreenUpdating(true);
                    //                    po.setvisible(true);
                }
            },
            function () {
                po.UpdateAndVis(true);
                //                po.ScreenUpdating(true);
                //                po.setvisible(true);
            });
        }

        //全屏切换：
        function SwitchFullScreen() {
            po.poctrl.FullScreen = !po.poctrl.FullScreen;
        }

        //只保存文件：
        function SaveFile() {
            var filename = getsavefilename("<%=FADM %>", "<%=MBWJ%>");
            $("#HidSaveFileName").val(filename);
            dialogshow('保存相关数据文件......');
            if (po.version() >= 12) {
                po.poctrl.Document.CheckCompatibility = "False";
            }

            po.poctrl.WebSave();
            dialogshow('保护数据页......');
            if ('<%=MBLB %>' != 'YSTZ') {
                po.protectallsheet();
            }
            dialogclose();
            if ('<%=MBLB %>' != 'YSTZ') {
                po.poctrl.Document.Saved = true;
            }
            IsExistFile = true;
        }

         //保存文件：
        var FileExcelNamePath = "";
        function SaveExcelFile() {
            FileExcelNamePath = SaveExcelFileName("<%=FADM %>", "<%=MBWJ%>");
            $("#HidSaveFileName").val(FileExcelNamePath);
            dialogshow('保存相关数据文件......');
            if (po.version() >= 12) {
                po.poctrl.Document.CheckCompatibility = "False";
            }

            po.poctrl.WebSave();
            dialogshow('保护数据页......');
            if ('<%=MBLB %>' != 'YSTZ') {
                po.protectallsheet();
            }
            dialogclose();
            if ('<%=MBLB %>' != 'YSTZ') {
                po.poctrl.Document.Saved = true;
            }
            IsExistFile = true;
        }

        //保存上传按钮:
        function SaveUpClick(_fn) {
            SaveExcelFile();
            if (FileExcelNamePath != "") {
                if (state == 1 ) {
                   checkformula('1');
                }
                else if (state == 2) {
                   checkformula('2');
                }
                else{
                   checkformula('1');
                }
            }
        }



//        //保存上传按钮:
//        function SaveUpClick(_fn) {
//            //保存之前先执行校验
//            CheckAndSave();
//            if (statueID != 0) {
//                po.poctrl.EnableExcelCalling();
//                if (localfilename != '') {
//                    alert('您打开的本地文档：' + localfilename + '与模板不符！\n\n请重新打开文件名为：<%=MBMC %>.XLS的文件！');
//                }
//                else {
//                    //修改延展行项目分类属性
//                    po.UpdateXMFLProperties();
//                    if (po.poctrl.FullScreen)
//                        po.poctrl.FullScreen = false;
//                    po.UpdateAndVis(false);
//                    if ('<%=MBLB %>' == 'YSTZ') {
//                        MBSave(_fn);
//                    }
//                    else {
//                        $.messager.confirm('提示：', '是否要保存数据和文件?', function (r) {
//                            if (r) {
//                                MBSave(_fn);
//                            }
//                            else {
//                                if (IsCheckFormula(_fn)) {
//                                    eval(_fn);
//                                }
//                                po.UpdateAndVis(true);
//                            }
//                        }, function () {
//                            po.UpdateAndVis(true);
//                        });
//                    }
//                }
//            }
//        }
        //模板文件保存
        function MBSave(_fn) 
        {
            if ('<%=MBLX%>' == '1') 
            {
                SaveTrCc(_fn);
            }
            else 
            {
                SaveData(false, _fn);
            }

            //如果是"批复预算基础数据维护"
            if (('<%=MBLB %>' == 'respfj') && ('<%=SFZLC %>' == '0')) {
                DealRespondStatus();
            }
        }

        //保存指定标签页数据，即按行列属性保存数据
        function SaveData(_istrcc, _fn) {
        debugger;
            var jsondata = null;
            dialogshowwin();
            dialogshow('正在保存数据......', true);
            dialogshow('遍历单元格获取要存储的数据......');
            if (_istrcc) {
                jsondata = po.savedata("<%=MBDM%>", "<%=FADM%>","<%=USER_ID%>", _istrcc);
            } else {
                jsondata = po.savedata("<%=MBDM%>", "<%=FADM%>","<%=USER_ID%>");
            }
            dialogshow('存储填报数据......');
            //预算调整时，不提示保存完成
            if ('<%=MBLB %>' != 'YSTZ') {
                alert('保存完成！');
            }
            if (jsondata && isJson(jsondata)) {
                dialogshow('总计：【' + jsondata['count'] + '】条填报数据，请稍候......');
                ajaxpost('MB.ashx?action=savedata', true, JSON.stringify(jsondata), function () {
                    if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                        // 本地提示：加载中/处理中 
                    }
                    else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        if (xmlHttp.responseText == '保存完成！') {
                        
                            po.UpdateAndVis(true);
                            //                            po.poctrl.Document.Saved = true;
                            //提示是否上报或审批：
                            if('<%=CANDATAUP%>' == '1' || '<%=CANDATAAPP%>' == '1') {
                            
                            dialogclose();
                                if ('<%=MBLB %>' != 'YSTZ') {
                                    if ('<%=CANDATAUP%>' == '1') {
                                        msgbox('提示', '保存完成，是否将数据继续上报?', 'OpenSmqr()', 1, 1, 'Warning', '是(上报)', '否(继续编辑)');
                                    }
                                    else if ('<%=CANDATAAPP%>' == '1') {
                                        msgbox('提示', '保存完成，是否将数据继续审批?', 'OpenSmqr()', 1, 1, 'Warning', '是(上报)', '否(继续审批)');
                                    }
                                }
                            }
                            else {
                            dialogclose();
                                return false;
                            }
                        }
                        po.UpdateAndVis(true);
                    } else {
                        dialogclose();
                        po.UpdateAndVis(true);
                        BmShowWinError('数据保存错误!', xmlHttp.responseText);
                    }
                });
            }
            else {
                dialogshow('保护数据页......');
                po.protectallsheet();
                dialogclose();
                po.UpdateAndVis(true);
            }
        }

        //切换office工具栏
        function VisToolBar() {
            document.getElementById("PageOfficeCtrl1").OfficeToolbars = !document.getElementById("PageOfficeCtrl1").OfficeToolbars;
            po.application().DisplayFormulaBar = document.getElementById("PageOfficeCtrl1").OfficeToolbars;
        }

        //打印：
        function Print() {
            po.poctrl.PrintPreview();
        }

        //本地另存:
        function SaveAsFile() {
            sflename = '<%=MBMC %>.XLS';
            try {
                var fileSave = new ActiveXObject("MSComDlg.CommonDialog");
                fileSave.CancelError = true;
                fileSave.Filter = "Excel文件|*.xls";
                fileSave.FilterIndex = 2;
                fileSave.FileName = sflename;
                // 必须设置MaxFileSize. 否则出错                 
                fileSave.MaxFileSize = 128;
                fileSave.ShowSave();
                var fileName = fileSave.FileName;
                var filePath = fileName.substring(0, fileName.lastIndexOf('\\') + 1);
                var sflename1 = fileName.substring(fileName.lastIndexOf('\\') + 1, fileName.length);
                //如果文件名相同则保存
                if (sflename != sflename1.trim()) {
                    alert('只能保存文件名为：【' + sflename + '】');
                    SaveAsFile();
                } else {
                    po.poctrl.SaveAs(fileName, true);
                    alert('文件已保存,路径如下：\n' + fileName);
                }
            } catch (e) {
                //此处是点击了取消按钮
                if (e.message != 'Cancel was selected.') {
                    po.poctrl.ShowDialog(3);
                }
            }
        }

        //本地打开：
        function OpenAsFile() {
            try {
                var sflename = '<%=MBMC%>.XLS';
                localopen = true;
                var oldfilename = po.poctrl.DocumentFileName.toUpperCase();
                po.poctrl.ShowDialog(1);
                var NewFile = po.poctrl.DocumentFileName.toUpperCase();
                if (NewFile != "" && NewFile != oldfilename && sflename.toUpperCase() != NewFile) {
                    localfilename = po.poctrl.DocumentFileName;
                    alert('您本地打开的文件名与模板名称不符，\n\n请重新打开否则保存无效!');
                } else {
                    po.UpdateAndVis(true);
                }
            } catch (e) {
            }
            finally {
                localopen = false;
            }
        }

        /* 创建 XMLHttpRequest 对象 */
        var xmlHttp;

        function GetXmlHttpObject() {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari 
                xmlhttp = new XMLHttpRequest();
            } else {// code for IE6, IE5 
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } return xmlhttp;
        }

        // -----------ajax方法-----------//
        function AfterOpenMbPost(_jsongetgs, _aryrag) {
            xmlHttp = GetXmlHttpObject();
            if (xmlHttp == null) {
                alert('您的浏览器不支持AJAX！');
                return;
            }
            var url = 'MB.ashx?action=getgs';
            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
            xmlhttp.send(_jsongetgs);
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                    // 本地提示：加载中/处理中 
                }
                else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    var sucjson = eval('(' + xmlHttp.responseText + ')');
                    if (sucjson) {
                        dialogshow('总计【' + sucjson["count"].toString() + '】个公式......');
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!po.setgs(sucjson, _aryrag)) {
                                alert("解析公式回写错误！");
                            }
                            else {
                                var BDJson = po.getBDYZJson("<%=FADM %>", "<%=FADM2 %>", "<%=FALB %>", "<%=GSYEAR %>", "<%=GSMONTH %>", "<%=YY%>", "<%=SessionID%>", "<%=MBDM %>");
                                if (BDJson && parseInt(BDJson["count"]) > 0) {
                                    //解析变动延展公式
                                    JXDynamicRowGS(JSON.stringify(BDJson));
                                }
                                else {
                                    AfterOpen(false);
                                }
                            }
                        }
                    }
                    else {
                        AfterOpen(false);
                    }
                }
                else {
                    BmShowWinError('公式解析错误!', xmlHttp.responseText);
                    AfterOpen(false);
                }
            }
        }

        //解析变动项目延展公式
        function JXDynamicRowGS(BDJson)
        {
            xmlHttp = GetXmlHttpObject();
            if (xmlHttp == null) 
            {
                alert('您的浏览器不支持AJAX！');
                return;
            }
            
            var url = 'DynamicRowHandler.ashx?action=BDYZ';
            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
            xmlhttp.send(BDJson);
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                    // 本地提示：加载中/处理中 
                }
                else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                   
                    var sucjson = eval('(' + xmlHttp.responseText + ')');
                    if (sucjson) {
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!po.writeDynamicRows(sucjson)) {
                                alert("解析变动公式回写错误！");
                            }
                        }
                    }
                    AfterOpen(false);
                }
                else {
                    BmShowWinError('公式解析错误!', xmlHttp.responseText);
                    AfterOpen(false);
                }
            }
        }

        function ajaxpost(_url, _async, _strjson, _fn) {
            xmlHttp = GetXmlHttpObject();
            if (xmlHttp == null) {
                alert('您的浏览器不支持AJAX！');
                return;
            }
            _url += '&rnd=' + Math.random();
            xmlhttp.open("POST", _url, _async);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
            xmlhttp.send(_strjson);
            if (_async == true) {
                xmlHttp.onreadystatechange = _fn;
            } else {
                _fn();
            }

        }

        //模板打开以后执行的事件：
        function AfterOpenMb() {
            if (!localopen) 
            {
                po.UpdateAndVis(false);
                dialogshowwin();
                dialogshow('获取模板公式......');
                var aryrag = new Array();
                var jsongetgs = po.getgs("<%=YY %>", "<%=NN %>", "<%=JD %>", "<%=FADM %>", "<%=FADM2 %>", "<%=FALB %>", "<%=YY %>", "<%=NN %>", "<%=YSYF %>", aryrag, "<%=YY%>");
                if (jsongetgs && parseInt(jsongetgs["count"]) > 0) 
                {
                    dialogshow('解析模板公式......');
                    AfterOpenMbPost(JSON.stringify(jsongetgs), aryrag);

                }
                else 
                {
                    //AfterOpen(false);
                    var BDJson = po.getBDYZJson("<%=FADM %>", "<%=FADM2 %>", "<%=FALB %>", "<%=YY %>", "<%=NN %>", "<%=YY%>", "<%=SessionID%>", "<%=MBDM %>");
                    if (BDJson && parseInt(BDJson["count"]) > 0) {
                        //解析变动延展公式
                        JXDynamicRowGS(JSON.stringify(BDJson));
                    }
                    else {
                        AfterOpen(false);
                    }
                }
            }
            else 
            {
                po.lockallsheet();
            }
        }

        //打开模板并执行相关取数函数：
        function OpenMbFile() {
            localfilename = '';
            if (hasopenfile) {
                po.poctrl.close();
                hasopenfile = null;
            }
            po.poctrl.JsFunction_AfterDocumentOpened = "AfterOpenMb()";
            po.poctrl.WebOpen("../Xls/<%=MBWJ%>", "xlsNormalEdit", "<%=USERNAME%>");
            po.setvisible(false);
            hasopenfile = '../Xls/<%=MBWJ%>';
        }

        $(function () {
            //获取年月信息：
            GetYyNn();
            //load_func();
        });

        //ajax 同步发送请求，确认是否有需要打开承诺书，需要打开返回true,否则返回false
        function isOpenSmcn() {
            var bool = '0';
            $.ajax({
                url: 'MB.ashx?action=isDisPlaySmcn&mbdm=' + '<%=MBDM%>&userid=' + '<%=USER_ID%>',
                type: 'GET',
                async: false, //表示同步
                cache: false,
                success: function (data) {
                    bool = data;
                }

            });
            return bool;
        }

        function OpenSmqr(_tjflag) {
            var boo = isOpenSmcn(); //显示承诺书返回1，不显示返回0
            if (boo == "0") {
                po.UpdateAndVis(true);
                //执行操作：
                TJ($("#HidTJ").val());
                return;
            }
            var res = selectValue(_tjflag);
            var mblx = "<%=MBLB %>";
            var context = res.split(','); //返回的部门内容，文字说明内容，用户信息
            var selects = context[0].split('|'); //返回的文字说明内容
            var department = context[1]; //返回的部门内容
            var user = context[2].split('|'); //返回的用户信息
            var username = user[0];
            var userphone = user[1];
            var option = "";
            for (var i = 0; i < selects.length; i++) {
                if (_tjflag == '0' && selects[i] == '不同意') {
                    option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
                } else if (_tjflag == '1' && selects[i] == '提交上报') {
                    option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
                } else if (_tjflag == '2' && selects[i] == '同意') {
                    option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
                } else {
                    option += "<option value ='" + selects[i] + "'>" + selects[i] + "</option>";
                }
            }

            $.ajax({
                url: 'SMCN.html', //这里是静态页的地址
                type: 'GET', //静态页用get方法，否则服务器会抛出405错误
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if (mblx == "basesb" || mblx == "resosb" || mblx == "baseys" || mblx == "resoys") {//当前菜单属于预算编制填报或与预算分解分解填报，预算编制-生成上报预算和预算分解-预算分解报表生成
                        data = upData(data, "id=\"SH\"", "id=\"SH\" style=\"display:none;\"");
                        data = upData(data, "id=\"TBR\"", "id=\"TBR\" value=\"" + username + "\"");
                        data = upData(data, "id=\"TBRPHONE\"", "id=\"TBRPHONE\" value=\"" + userphone + "\"");
                    }
                    if (mblx == "basesp" || mblx == "resosp") {//当前菜单属于预算编制报表审批或预算分解报表审批
                        data = upData(data, "id=\"TB\"", "id=\"TB\" style=\"display:none;\"");
                        data = upData(data, "id=\"SHR\"", "id=\"SHR\" value=\"" + username + "\"");
                        data = upData(data, "id=\"SHRPHONE\"", "id=\"SHRPHONE\" value=\"" + userphone + "\"");
                    }
                    if (_tjflag == '0') { //退回
                        SmqrHeight = '450';
                        $('#SMQR').panel('resize', { width: 610, height: SmqrHeight });
                        data = upData(data, "</textarea>", "不同意" + "</textarea>");
                    }
                    if (_tjflag == '1') { //上报
                        data = upData(data, "</textarea>", "上报" + "</textarea>");
                    }
                    if (_tjflag == '2') {//审批
                        data = upData(data, "</textarea>", "同意" + "</textarea>");
                    }
                    data = upData(data, "id=\"Department\"", "id=\"Department\" value=\"" + department + "\" readonly=\"readonly\"");
                    data = upData(data, "</select>", option + "</select>");
                    //                    po.ScreenUpdating(false);
                    //                    po.setvisible(false);
                    po.UpdateAndVis(false);
                    $('#SMQR').dialog({
                        content: data,
                        top: (_bodyHeight - SmqrHeight) / 2,
                        left: (_bodyWidth - SmqrWidth) / 2,
                        onClose: function () {
                            po.UpdateAndVis(true);
                            //                            po.ScreenUpdating(true);
                            //                            po.setvisible(true);
                        }
                    })
                    //退回的时候重新布局设置，设置相应的css样式
                    if (_tjflag == '0') {//退回
                        $('#con').css("display", "none");
                        $('#th').css("display", "inline");
                        $('h2').css("padding", "5px,0px,20px,0px");
                        $('#dep').css("border-collapse", "separate"); $('#dep').css("border-spacing", "10px"); //设置table的行间距
                        $('#txta').css("height", "50%");
                    }

                    $('#SMQR').window('open');
                    time();
                }
            })

        }


        function time() {
            var date = new Date();
            var year = date.getFullYear();
            var month = (date.getMonth() + 1) < 10 ? ("0" + "" + (date.getMonth() + 1)) : (date.getMonth() + 1);
            var day = date.getDate() < 10 ? ("0" + "" + date.getDate()) : date.getDate();
            var hour = date.getHours() < 10 ? ("0" + "" + date.getHours()) : date.getHours();
            var minute = date.getMinutes() < 10 ? ("0" + "" + date.getMinutes()) : date.getMinutes();
            var second = date.getSeconds() < 10 ? ("0" + "" + date.getSeconds()) : date.getSeconds();
            document.getElementById("TBSJ").value = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
            document.getElementById("SHSJ").value = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        }
        //        setInterval("time()", 60000); // 循环定时调用

        function selectValue(_tjflag) {
            var MBDM = "<%=MBDM %>";
            var USER_ID = "<%=USER_ID %>";
            var res = "";
            $.ajax({
                url: 'MB.ashx?action=getValue&MBDM=' + MBDM + '&USER_ID=' + USER_ID + '&FLAG=' + _tjflag + '',
                type: 'GET',
                async: false, //同步
                success: function (data) {
                    res = data;
                }
            });
            return res;
        }

        function upData(_data, element, text) {
            _data = _data.replace(element, text);
            return _data;
        }

        function getContent() {
            //smqr = $('#SMQR').dialog('options').content; //获取dialog里的content属性的值
            smqr = $('#Department').val() + "|" + $('#txta').val() + "|" + $('#TBR').val() + "|" + $('#TBSJ').val() + "|" + $('#TBRPHONE').val() + "|" + $('#SHR').val() + "|" + $('#SHSJ').val() + "|" + $('#SHRPHONE').val();
            $('#SMQR').window('close');
            //            po.ScreenUpdating(true);
            //            po.setvisible(true);
            po.UpdateAndVis(true);
            TJ($("#HidTJ").val());
            //            alert(smqr);
        }

        function close() {
            $('#SMQR').window('close');
            $("#HidTJ").val("");
            po.UpdateAndVis(true);
            //            po.ScreenUpdating(true);
            //            po.setvisible(true);
        }

        //添加不平衡模板：
        function InitImbal() {
            $('#GrdImba').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                //                fitColumns: true,
                singleSelect: true,
                striped: true,
                //                pagination: true,
                //                pageSize: 10,
                loadMsg: '正在加载数据.....',
                columns: [[
                    { field: 'CPDM', title: '产品代码', hidden: true },
					{ field: 'CPMC', title: '产品名称', width: 120 },
                    { field: 'CL', title: '产量', width: 80 },
                    { field: 'LXMC', title: '流向名称', width: 120 },
                    { field: 'ZFL', title: '流量', width: 80 },
                    { field: 'ZYBM', title: '作业编码', hidden: true },
                    { field: 'CPBM', title: '产品编码', hidden: true },
                    { field: 'CE', title: '差量', width: 80 },
                    { field: 'BS', title: '标识', hidden: true },
				]],
                onHeaderContextMenu: function (e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                },
                onDblClickRow: function (rowIndex, rowData) {
                }
            });

            //            var pager = $('#GrdImba ').datagrid('getPager');

            //            pager.pagination({
            //                loading: true,
            //                showRefresh: false
            //            });
        }

        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }

                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    loading: true,
                    showRefresh: false,
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }


        var cmenu;

        function createColumnMenu() {
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function (item) {
                    if (item.iconCls == 'icon-ok') {
                        $('#GrdImba').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#GrdImba').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
            });
            var fields = $('#GrdImba').datagrid('getColumnFields');
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var col = $('#GrdImba').datagrid('getColumnOption', field);
                if (col.hidden) {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-empty'
                    });
                } else {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-ok'
                    });
                }
            }
        }

        var selxmfl = "";
        /// <summary>获取变动行的项目分类</summary>
        function XMFLXZ() {
            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=XMFLXZ',
                async: false,
                cache: false,
                success: function (result) {
                    var Data = eval("(" + result + ")");
                    selxmfl = Data;
                    $("#cslx").combobox("loadData", Data);
                }
            });
        }
        var param = "";
        /// <summary>添加变动项目</summary>
        function AddXM() {
            $("#BDXMMC").val("");
            param = "add";
            $('#dialogBDXM').window('open');
        }
        /// <summary>修改变动项目</summary>
        function UpdateBdxm() {
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length > 0) {
                if (rows.length > 1) {
                    tishi("修改变动行只能选择一项");
                }
                else {
                    $("#BDXMMC").val(rows[0].BDXMMC);
                    param = "upt";
                    $('#dialogBDXM').window('open');
                }
            }
            else {
                tishi("请选择要修改的项目名称");
            }
        }
        function initDialog(name, title, wid, hg, con) {
            $('#' + name).dialog({
                title: title,
                width: wid,
                height: hg,
                closed: true,
                cache: false,
                buttons: [{
                    text: '保存',
                    iconCls: 'icon-ok',
                    handler: function () {
                        saveBDXM(param);
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function () {
                        $('#' + name).dialog('close');
                    }
                }],
                content: con

            });
        }
        /// <summary>提示</summary>
        function tishi(rtn) {
            $.messager.alert({
                title: '温馨提示',
                msg: rtn
            });
        }
        /// <summary>保存变动项目名称</summary>
        function saveBDXM(param) {
            var mbdm = "<%=MBDM %>";
            var hszxdm = "<%=HSZXDM %>";
            var bdxm = $("#BDXMMC").val();
            var xmfl = $("#cslx").combobox("getValue");
            if (bdxm.trim().trim() == "") {
                tishi("项目名称为空")
            }
            else {
                switch (param) {
                    case 'add':
                        $.ajax({
                            type: 'post',
                            url: 'MB.ashx?action=AddBDXM&hszxdm=' + hszxdm + '&bdxm=' + escape(bdxm) + '&mbdm=' + mbdm + '&xmfl=' + xmfl + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result > 0) {
                                    tishi("保存成功！");
                                    $('#dialogBDXM').window('close');
                                }
                                else if (result == -1) {
                                    tishi("已有相同的变动项目名称");
                                }
                                else {
                                    tishi("保存失败！");
                                }
                            }
                        });
                        GetDataGrid(hszxdm, mbdm);
                        break;
                    case 'upt':
                        var rows = $('#dg').datagrid('getSelections');
                        var dm = rows[0].BDXMDM;
                        $.ajax({
                            type: 'post',
                            url: 'MB.ashx?action=UptBDXM&hszxdm=' + hszxdm + '&bdxm=' + escape(bdxm) + '&mbdm=' + mbdm + '&xmfl=' + xmfl + '&bdxmdm=' + dm + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result > 0) {
                                    tishi("修改成功！");
                                    $('#dialogBDXM').window('close');
                                }
                                else if (result == -1) {
                                    tishi("已有相同的变动项目名称");
                                }
                                else {
                                    tishi("修改失败！");
                                }
                            }
                        });
                        GetDataGrid(hszxdm, mbdm);
                        break;
                }
            }
        }
        /// <summary>查找框按键事件</summary>
        function ChangeText() {
            var mbdm = "<%=MBDM %>";
            var hszxdm = "<%=HSZXDM %>";
            var ChangeText = $("#TxtselXm").val();
            $.ajax({
                type: 'post',
                url: 'MB.ashx?action=GetChangeTextGrid&hszxdm=' + hszxdm + '&mbdm=' + mbdm + '&text=' + escape(ChangeText) + '',
                async: false,
                cache: false,
                success: function (result) {
                    var Data = eval("(" + result + ")");
                    $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                }
            });
        }
        /// <summary>打开插入变动行的选项框</summary>
        function InsertRow() {
            var mbdm = "<%=MBDM %>";
            var hszxdm = "<%=HSZXDM %>";
            var sheet = po.activesheet();
            var a = po.application().Selection;
            var sheetName = sheet.Name;
            var FatherRow = a.Row;
            var sel = sheet.Cells(parseInt(FatherRow), parseInt(1)).Value;
            //第一列的属性
            var FirstColSX = "";
            //查找首列的行类型
            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName + '',
                async: false,
                cache: false,
                success: function (result) {
                    FirstColSX = result
                }
            });
            if (sel == 0 || sel == undefined || FirstColSX == "1" || FirstColSX == "4") {
                tishi("行属性无项目，无法插入变动行");
            }
            else {
                var Cell = sheet.Cells(FatherRow, 1);
                if (Cell.Value != undefined && Cell.Value != null && Cell.Value != "" && Cell.Value.indexOf(",BZ:YZ") == -1) {
                    var xmfl = "";
                    var arr = new Array();
                    arr = sel.split(',');
                    var cpdm = "";
                    var bdxmdm = "";
                    if (FirstColSX == "1") {
                        if (sel.indexOf("|") > 0) {
                            cpdm = arr[0].split('|')[0].split(':')[1];
                            bdxmdm = arr[0].split('|')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            cpdm = arr[0].split(':')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                    }
                    if (FirstColSX == "2") {
                        if (sel.indexOf("|") > 0) {
                            cpdm = arr[0].split('|')[0].split(':')[1];
                            bdxmdm = arr[0].split('|')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            cpdm = arr[0].split(':')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                    }
                    if (FirstColSX == "3") {
                        if (sel.indexOf("|") > 0) {
                            cpdm = arr[1].split('|')[0].split(':')[1];
                            bdxmdm = arr[1].split('|')[1];
                            xmfl = arr[2].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            cpdm = arr[1].split(':')[1];
                            xmfl = arr[2].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                    }
                    if (FirstColSX == "4") {

                    }
                    if (FirstColSX == "5") {

                    }
                    $.ajax({
                        type: 'post',
                        url: 'MB.ashx?action=InsertBDXM&hszxdm=' + hszxdm + '&xmfl=' + xmfl + '&mbdm=' + mbdm + '',
                        async: false,
                        cache: false,
                        success: function (result) {
                        }
                    });
                    GetDataGrid(hszxdm, mbdm);
                    XMFLXZ();
                    $("#cslx").combobox({ panelHeight: 200 })
                    $("#SZBDH").window('open');
                    $('#dg').datagrid('clearSelections');
                    var rows = $('#dg').datagrid('getRows');
                    var index = -1;
                    for (var i = 0; i < rows.length; i++) {
                        if (rows[i].BDXMDM == bdxmdm) {
                            index = $("#dg").datagrid('getRowIndex', rows[i]);
                        }
                    }
                    if (index != -1) {
                        $('#dg').datagrid('selectRow', index);
                        var row = $("#dg").datagrid('getSelections');
                        $("#TxtselXm").val(row[0].BDXMMC);
                    }
                    //                                var row = $("#dg").datagrid('getSelections');
                    //for循环判断combox的方法getValue获取的值为json的text的时候，将其值赋值为相应的id
                    for (var i = 0; i < selxmfl.length; i++) {
                        if ($("#cslx").combobox('getText').trim() == selxmfl[i].text) {
                            $("#cslx").combobox('setValue', selxmfl[i].id);
                        }
                    }
                    var cslx = $("#cslx").combobox('setValues', xmfl);
                    initDialog('dialogBDXM', '设置变动行项', 300, 120, $('#BDXM'));
                    if (sel.indexOf("|") > 0) {
                        if (index != -1) {
                            var row = $("#dg").datagrid('getSelections');
                            $("#TxtXMMC").val(row[0].BDXMMC);
                        }
                    }
                    else {
                        $.ajax({
                            type: 'get',
                            url: 'MB.ashx?action=Selxmmc&xmdm=' + cpdm + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                $("#TxtXMMC").val(result);
                            }
                        });
                    }
                }
                else 
                {
                    alert("延展的行上不能添加变动行,请选择非延展行！");
                    return;
                }
                //$("#TxtselXm").val(row[0].BDXMMC);
            }
        }

        //项目名称列
        var XMMCCol = 0;
        //获取项目分类列
        var XMFLXZCol = 0;
        //获取下一个要添加的行
        var nextRows = 0;
        //查找父亲行
        var nextFatherRow = 0;
        //是否有空行
        var isKh = 0;
        //空行的个数；
        var isKhCount = 0;
        //是否有相同的变动项目
        var IsHavebdxm = 0;
        //确定插入变动行
        function SelQuery() {
            var sheet = po.activesheet();
            //获取sheetName
            var sheetName = sheet.Name;
            var a = po.application().Selection;
            var FatherRow = a.Row;
            //获取选中行的第一列的值
            var sel = sheet.Cells(parseInt(FatherRow), parseInt(1)).Value;
            //获取选中行下一行的值
            var nextvalue = sheet.Cells(parseInt(FatherRow) + 1, parseInt(1)).Value;
            if (sel.indexOf("|") > -1) {
                if (nextvalue != undefined) {
                    if (nextvalue.indexOf("BZ:YZ") > -1) {
                        if (sel.indexOf("BZ:YZ") > -1) {
                            tishi("延展行之间不能插入变动行");
                        }
                        else {
                            //获取选中行的上一行的值
                            var lastValue = sheet.Cells(parseInt(FatherRow) - 1, parseInt(1)).Value;
                            if (lastValue.indexOf("BZ:YZ") > -1) {
                                tishi("延展行之间不能插入变动行");
                            }
                            else {
                                InsertValue();
                            }
                        }
                    }
                    else {
                        InsertValue();
                    }
                }
                else {
                    InsertValue();
                }
            }
            else {
                InsertValue();
            }
        }
        //插入变动行数据
        function InsertValue() {
            var mbdm = "<%=MBDM %>";
            var fadm = "<%=FADM %>";
            var hszxdm = "<%=HSZXDM %>";
            var sheet = po.activesheet();
            //获取sheetName
            var sheetName = sheet.Name;
            //项目分类
            var xmfl = $("#cslx").combobox("getValue");
            //获取变动datagrid选中的行
            var row = $("#dg").datagrid('getSelections');
            var a = po.application().Selection;
            var FR = a.Row;
            var FatherRow = a.Row;
            //获取选中行的第一列的值
            var sel = sheet.Cells(parseInt(FatherRow), parseInt(1)).Value;
            //获取选中行下一行的值
            var nextvalue = sheet.Cells(parseInt(FatherRow) + 1, parseInt(1)).Value;
            //第一列的属性
            var FirstColSX = "";
            //查找首列的行类型
            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName + '',
                async: false,
                cache: false,
                success: function (result) {
                    FirstColSX = result
                }
            });
            if (nextvalue != undefined) {
                SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                FatherRow = nextRows;
            }
            var stringMB = "";
            var maxbdRow = 0;
            //如果有选中变动行的项目
            if (row.length > 0) {
                var arr = new Array();
                arr = sel.split(',');
                var cpdmCol = "";
                var zydmCol = "";
                //显示作业+分类
                if (FirstColSX == "1") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a.row);
                        var len = FatherValue.indexOf(",");
                        zydmCol = FatherValue.substring(0, len);
                    }
                    else {
                        zydmCol = arr[0];
                    }
                }
                //产品代码+分类
                else if (FirstColSX == "2") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a.row);
                        var len = FatherValue.indexOf(",");
                        cpdmCol = FatherValue.substring(0, len);
                    }
                    else {
                        cpdmCol = arr[0];
                    }
                }
                //作业代码+产品代码+分类
                else if (FirstColSX == "3") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a.row);
                        var len = FatherValue.lastIndexOf(",");
                        cpdmCol = FatherValue.substring(0, len);
                    }
                    else {
                        var len = sel.lastIndexOf(",");
                        cpdmCol = sel.substring(0, len);
                    }
                }
                //作业代码+计算对象+分类
                else if (FirstColSX == "4") {

                }
                //作业代码+产品代码+计算对象+分类
                else if (FirstColSX == "5") {

                }
                //查找项目名称所在的列
                //SelXmmcCol(mbdm, sheetName);
                XMMCCol = po.FindXMMCColumn(sheet);
                if (XMMCCol == 0) {
                    alert("您没有设置项目名称列！");
                    return;
                }
                for (var i = 0; i < row.length; i++) {
                    var ychsx = "";
                    var xmmc = "";
                    var nextRow = parseInt(FatherRow) + (i + 1);
                    if (FirstColSX == "1") {
                        ychsx = zydmCol + '|' + row[i].BDXMDM + ',' + arr[1].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC
                    }
                    else if (FirstColSX == "2") {
                        ychsx = cpdmCol + '|' + row[i].BDXMDM + ',' + arr[1].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC;
                    }
                    else if (FirstColSX == "3") {
                        ychsx = cpdmCol + '|' + row[i].BDXMDM + ',' + arr[2].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC;
                    }
                    else if (FirstColSX == "4") {
                    }
                    else if (FirstColSX == "5") {
                    }
                    var FF;
                    //如果是变动行增加的时候
                    if (sel.indexOf("|") > -1) 
                    {
                        FF = true;
                    }
                    else 
                    {
                        FF = false;
                    }
                    var rtn = po.IsExistSameDynamicRow(sheet, ychsx, FirstColSX, FR, FF);
                    if (rtn == 1) 
                    {
                        tishi("已有相同的变动项目！");
                        IsHavebdxm = 0;
                        break;
                    }
                    else 
                    {
                        //查找下行是否有空行
                        if (isKh == 0) {
                            //下一个要插入变动行的值
                            var NextInsertRow = sheet.Cells(nextRow, 1).Value;
                            //查找最大列
                            var maxCol = po.usedrcolscnt(sheet);
                            //统计下一行值的个数
                            var CountValue = 0;
                            if (sel.indexOf("|") > -1) {
                                SelFatherRow(sel, a.Row);
                                var fatherValue = sheet.Cells(parseInt(nextFatherRow), parseInt(1)).Value;
                                //查找父亲行下是否有空行
                                selIsNullKh(mbdm, sheetName, fatherValue, XMMCCol);
                                if (isKhCount > 0) {
                                    var thisKhRow = nextRow - nextFatherRow;
                                    if (thisKhRow >= isKhCount) {
                                        isKh = 1
                                    }
                                    if (NextInsertRow == undefined) {
                                        isKh = 0;
                                    }
                                }
                                if (NextInsertRow == undefined) {
                                    isKh = 0;
                                }
                                //表示模板定义没有保存
                                if (isKh == "-1") {
                                    //查找下行是否有值
                                    for (var n = 1; n < maxCol; n++) {
                                        var nextValue = sheet.cells(a.Row + 1, n).Value;
                                        if (nextValue != "" || nextValue != undefined) {
                                            CountValue++;
                                        }
                                    }
                                    if (CountValue > 0) {
                                        isKh = 1
                                    }
                                    else {
                                        isKh = 0
                                    }
                                }
                            }
                            else {
                                selIsNullKh(mbdm, sheetName, sel, XMMCCol);
                                if (isKhCount > 0) {
                                    var thisKhRow = nextRow - a.Row;
                                    if (thisKhRow >= isKhCount) {
                                        isKh = 1
                                    }
                                }
                                if (NextInsertRow == undefined) {
                                    isKh = 0;
                                }
                            }
                            //表示模板定义没有保存
                            if (isKh == "-1") {
                                //查找下行是否有值
                                for (var n = 1; n < maxCol; n++) {
                                    var nextValue = sheet.cells(a.Row + 1, n).Value;
                                    if (nextValue != "" || nextValue != undefined) {
                                        CountValue++;
                                    }
                                }
                                if (CountValue > 0) {
                                    isKh = 1
                                }
                                else {
                                    isKh = 0
                                }
                            }
                        }
                        po.unprotectsheet(sheet);
                        if (isKh == "1") {
                            ychsx = ychsx + ",BZ:Ins";
                            sheet.Rows(nextRow).Insert();
                            isKh = 0;
                        }
                        if (FirstColSX == "1") {
                            var insertRow = sheet.Cells(parseInt(nextRow), parseInt(1));
                            insertRow.Value = ychsx;
                            if (XMMCCol != 0) {
                                var insertXmmc = sheet.Cells(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.Value = xmmc;
                            }
                        }
                        if (FirstColSX == "2") {
                            var insertRow = sheet.Cells(parseInt(nextRow), parseInt(1));
                            insertRow.Value = ychsx;
                            if (XMMCCol != 0) {
                                var insertXmmc = sheet.Cells(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.Value = xmmc;
                            }
                        }
                        if (FirstColSX == "3") {
                            var insertRow = sheet.Cells(parseInt(nextRow), parseInt(1));
                            insertRow.Value = ychsx;
                            if (XMMCCol != 0) {
                                var insertXmmc = sheet.Cells(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.Value = xmmc;
                            }
                        }
                        if (FirstColSX == "4") {
                        }
                        if (FirstColSX == "5") {
                        }
                        var xcolor = "C2F6A2";
                        var _color = parseInt(xcolor, 16);
                        var maxCol = po.usedrcolscnt(sheet);
                        var C = num2col(maxCol);
                        sheet.Range("B" + nextRow + ":" + C + nextRow).Interior.Color = _color;
                        sheet.Range("B" + nextRow + ":" + C + nextRow).Borders.Color = parseInt("000000", 16);
                        sheet.Range("B" + nextRow + ":" + C + nextRow).Locked = "False";
                        //有项目分类的添加下拉框
                        po.AddExcelDropDownList(sheet, ychsx, nextRow);
                        po.protectallsheet(sheet);
                    }
                }
            }
            else {
                tishi("无法插入变动项目");
            }
        }
        //查找父亲行的第一列
        function QueryFatherValue(selectValue, row) {
            if (selectValue == undefined) {
                var sheet = po.activesheet();
                var lastRow = row - 1;
                var lastValue = sheet.Cells(parseInt(lastRow), parseInt(1)).Value;
                QueryFatherValue(lastValue, lastRow);
            }
            else if (selectValue.indexOf("|") > 0) {
                var sheet = po.activesheet();
                var lastRow = row - 1;
                var lastValue = sheet.Cells(parseInt(lastRow), parseInt(1)).Value;
                QueryFatherValue(lastValue, lastRow);
            }
            else {
                FatherValue = selectValue;
            }
        }
        //查找下行是否有空行
        function selIsNullKh(mbdm, sheetName, sel, XMMCCol) {
            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=IsNullKh&MBDM=' + mbdm + '&sheetName=' + escape(sheetName) + '&Value=' + sel + '&ColumnIndex=' + XMMCCol + '',
                async: false,
                cache: false,
                success: function (result) {
                    if (result > 1) {
                        isKhCount = result;
                    }
                    else {
                        isKh = result;
                    }
                }
            });
        }
        //查找变动项目的父亲行
        function SelFatherRow(sel, row) {
            if (sel == undefined) {
                var sheet = po.activesheet();
                var lastValue = sheet.Cells(parseInt(row) - 1, parseInt(1)).Value;
                SelFatherRow(lastValue, parseInt(row) - 1);
            }
            else if (sel.indexOf("|") > 0) {
                var arr = new Array();
                arr = sel.split(',');
                var sheet = po.activesheet();
                var lastValue = sheet.Cells(parseInt(row) - 1, parseInt(1)).Value;
                SelFatherRow(lastValue, parseInt(row) - 1);
            }
            else {
                nextFatherRow = row;
            }
        }
        //判断是否有相同的隐藏属性
        function SelYCHSX(nextvalue, ychsx, row, flag) {
            if (flag == "up") {
                if (nextvalue == undefined) {
                    var sheet = po.activesheet();
                    var nextvalue = sheet.Cells(parseInt(row) - 1, parseInt(1)).Value;
                    SelYCHSX(nextvalue, ychsx, parseInt(row) - 1, "up");
                }
                else if (nextvalue.indexOf("|") > 0) {
                    var arr = new Array();
                    arr = nextvalue.split(',');
                    if (nextvalue != ychsx) {
                        var sheet = po.activesheet();
                        var sheetName = sheet.Name;
                        var nextvalue = sheet.Cells(parseInt(row) - 1, parseInt(1)).Value;
                        SelYCHSX(nextvalue, ychsx, parseInt(row) - 1, "up");
                    }
                    else {
                        IsHavebdxm = 1;
                    }
                }
            }
            else {
                if (nextvalue != undefined) {
                    if (nextvalue != ychsx) {
                        var sheet = po.activesheet();
                        var sheetName = sheet.Name;
                        var nextvalue = sheet.Cells(parseInt(row) + 1, parseInt(1)).Value;
                        SelYCHSX(nextvalue, ychsx, parseInt(row) + 1, "down");
                    }
                    else {
                        IsHavebdxm = 1;
                    }
                }
            }
        }
        //获取下个要插入的行
        function SelNextRow(sel, nextvalue, FatherRow, FirstColSX) {
            if (nextvalue != undefined) {
                var cpdm = "";
                var zydm = "";
                var nextcpdm = "";
                var nextzydm = "";
                var xmfl = "";
                var nextxmfl = "";
                var arr = new Array();
                arr = sel.split(',');
                var nextsel = new Array();
                nextsel = nextvalue.split(',');
                //显示作业+分类
                if (FirstColSX == "1") {
                    //拆分变动行的ZYDM
                    if (sel.indexOf("|") > -1) {
                        zydm = arr[0].split('|')[0];
                        xmfl = arr[1];
                    }
                    else {
                        zydm = arr[0];
                        xmfl = arr[1];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextzydm = nextsel[0].split('|')[0];
                        nextxmfl = nextsel[1];
                    }
                    else {
                        nextzydm = nextsel[0];
                        nextxmfl = nextsel[1];
                    }
                    //当前行的首列
                    var thiszydm = zydm + ',' + xmfl;
                    //下一行的首列
                    var nextFirstCol = nextzydm + ',' + nextxmfl;
                    if (thiszydm == nextFirstCol) {
                        var sheet = po.activesheet();
                        var sheetName = sheet.Name;
                        var nextvalue = sheet.Cells(parseInt(FatherRow) + 1, parseInt(1)).Value;
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //产品代码+分类
                else if (FirstColSX == "2") {
                    if (sel.indexOf("|") > -1) {
                        cpdm = arr[0].split('|')[0];
                    }
                    else {
                        cpdm = arr[0];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextcpdm = nextsel[0].split('|')[0];
                    }
                    else {
                        nextcpdm = nextsel[0];
                    }
                    if (nextcpdm == cpdm) {
                        var sheet = po.activesheet();
                        var sheetName = sheet.Name;
                        var nextvalue = sheet.Cells(parseInt(FatherRow) + 1, parseInt(1)).Value;
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //作业代码+产品代码+分类
                else if (FirstColSX == "3") {
                    if (sel.indexOf("|") > -1) {
                        cpdm = arr[1].split('|')[0];
                    }
                    else {
                        cpdm = arr[1];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextcpdm = nextsel[1].split('|')[0];
                    }
                    else {
                        nextcpdm = nextsel[1];
                    }
                    if (nextcpdm == cpdm) {
                        var sheet = po.activesheet();
                        var sheetName = sheet.Name;
                        var nextvalue = sheet.Cells(parseInt(FatherRow) + 1, parseInt(1)).Value;
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //作业代码+计算对象+分类
                else if (FirstColSX == "4") {

                }
                //作业代码+产品代码+计算对象+分类
                else if (FirstColSX == "5") {

                }
            }
            else {
                nextRows = parseInt(FatherRow) - 1;
            }
        }
        /// <summary>获取变动项目的列表</summary>
        function GetDataGrid(hszxdm, mbdm) {
            $.ajax({
                type: 'post',
                url: 'MB.ashx?action=GetDataGrid&hszxdm=' + hszxdm + '&mbdm=' + mbdm + '',
                async: false,
                cache: false,
                success: function (result) {
                    var Data = eval("(" + result + ")");
                    $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                }
            });
        }
        function UpdateBd() {
            var mbdm = "<%=MBDM %>";
            var row = $("#dg").datagrid('getSelections');
            if (row.length > 0) 
            {
                if (row.length > 1) {
                    tishi("修改变动行只能选择一项");
                }
                else 
                {
                    var sheet = po.activesheet();
                    var a = po.application().Selection;
                    var FatherRow = a.Row;
                    var sheetName = sheet.Name;
                    //SelXmmcCol(mbdm, sheetName);
                    XMMCCol = po.FindXMMCColumn(sheet);
                    if (XMMCCol == 0) {
                        alert("您没有设置项目名称列！");
                        return;
                    }
                    var sel = sheet.Cells(parseInt(FatherRow), parseInt(1)).Value;
                    var CellJson = JSON.parse("{\"" + sel.replace(/:/g, "\":\"").replace(/,/g, "\",\"") + "\"}");
                    //查找选中的项目名称列
                    var selxmmc = sheet.Cells(parseInt(FatherRow), parseInt(XMMCCol)).Value;
                    //当前要修改的名称
                    var ThisUpXmmc = row[0].BDXMMC;
                    var FirstColSX = "";
                    //查找首列的行类型
                    $.ajax({
                        type: 'get',
                        url: 'MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName + '',
                        async: false,
                        cache: false,
                        success: function (result) {
                            FirstColSX = result
                        }
                    });
                    if (sel.indexOf("|") > -1) 
                    {
                        if (FirstColSX == "2" || FirstColSX == "3" || FirstColSX == "5") 
                        {
                            var xmfl = $("#cslx").combobox("getValue");
                            if (FirstColSX == "2") 
                            {
                                ychsx = "CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl;
                            }
                            else if (FirstColSX == "3") {
                                ychsx = "ZYDM:" + CellJson["ZYDM"] + ",CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl;
                            }
                            else 
                            {
                                ychsx = "ZYDM:" + CellJson["ZYDM"] + ",CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl + ",JSDX:" + CellJson["JSDX"];
                            }

                            if (CellJson["BZ"] != undefined) 
                            {
                                ychsx = ychsx + ",BZ:" + CellJson["BZ"];
                            }

                            if (sel != ychsx) 
                            {
                                //判断是否
                                var rtn = po.IsExistSameDynamicRow(sheet, ychsx, FirstColSX, FatherRow, true);
                                if (rtn == 1) 
                                {
                                    tishi("已有相同的变动项目！");
                                    IsHavebdxm = 0;
                                }
                                else 
                                {
                                    po.unprotectsheet(sheet);
                                    sheet.Cells(parseInt(FatherRow), parseInt(1)).Value = ychsx;
                                    sheet.Cells(parseInt(FatherRow), parseInt(XMMCCol)).Value = ThisUpXmmc;
                                    tishi("修改成功!");
                                    po.protectsheet(sheet);
                                }
                            }
                            else 
                            {
                                if (selxmmc != ThisUpXmmc) 
                                {
                                    po.unprotectsheet(sheet);
                                    sheet.Cells(parseInt(FatherRow), parseInt(XMMCCol)).Value = ThisUpXmmc;
                                    tishi("修改成功!");
                                    po.protectsheet(sheet);
                                }
                                else 
                                {
                                    tishi("与原数据一致，并未修改!");
                                }
                            }
                        }
                        else {
                            alert("此文件不能添加变动行!");
                            return;
                        }
                    }
                    else {
                        tishi("非变动项目不可修改!");
                    }
                }
            }
            else {
                tishi("请选择要修改的变动项目!")
            }
        }
        /// <summary>查找xmmc所在的列</summary>
        function SelXmmcCol(mbdm, sheetName) {

            $.ajax({
                type: 'get',
                url: 'MB.ashx?action=XmmcCol&MBDM=' + mbdm + '&sheetName=' + escape(sheetName) + '',
                async: false,
                cache: false,
                success: function (result) {
                    XMMCCol = result;
                }
            });
        }
        /// <summary>删除变动行名称</summary>
        function DeleteBdxm() {
            var mbdm = "<%=MBDM %>";
            var hszxdm = "<%=HSZXDM %>";
            var rows = $('#dg').datagrid('getSelections');
            var delRow = [];
            if (rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    delRow.push(rows[i].BDXMDM);
                }
                $.messager.confirm('警告', '确定要删除该变动项目名称吗？', function (r) {
                    if (r) {
                        $.ajax({
                            type: 'post',
                            url: 'MB.ashx?action=DelBdXMMC&bdxmdm=' + delRow.join(",") + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result > 0) {
                                    tishi("删除成功!");
                                    GetDataGrid(hszxdm, mbdm);
                                }
                                else if (result == -1) {
                                    tishi("变动项目已被使用无法删除！");
                                }
                                else {
                                    tishi("删除失败!");
                                }
                            }
                        });
                    }
                });
            }
            else {
                tishi("没有可删除的变动行名称");
            }
        }
        //删除变动行
        function DelBdRow() {
            var sheet = po.activesheet();
            var a = po.application().Selection.Rows;
            for (var j = a.cells.count; j >= 1; j--) {
                var FatherRow = a(j).row;
                //获取选中行的第一列的值
                var sel = sheet.Cells(parseInt(FatherRow), parseInt(1)).Value;
                if (sel != undefined) {
                    if (sel.indexOf("|") > -1) {
                        if (sel.indexOf("BZ:YZ") == -1) {
                            po.unprotectallsheet(sheet)
                            if (sel.indexOf("BZ:Ins") == -1) 
                            {
                                sheet.Rows(FatherRow).ClearContents();
                                var ColIndex = po.GetXMFLId(sheet);
                                if (ColIndex != -1) {
                                    var val = sheet.Cells(FatherRow, ColIndex).Validation;
                                    val.Delete();
                                }
                            }
                            else 
                            {
                                sheet.Rows(FatherRow).Delete();
                            }
                            po.protectallsheet(sheet);
                        }
                        else 
                        {
                            tishi("延展项目无法删除！");
                            break;
                        }
                    }
                    else {
                        tishi("不是变动项目无法删除！");
                        break;
                    }
                }
            }
        }

        //打开Excel文件
        function OpenExcelMB() {
            //文档保存后事件：
            po.poctrl.JsFunction_AfterDocumentSaved = "AfterSave()";
            po.setvisible(false);
            var filename = getsavefilename("<%=FADM %>", "<%=MBWJ%>");
            fileexist('../XlsData/' + filename,
            //如果存在填报文件：
            function () {
                IsExistFile = true;
                dialogshowwin();
                dialogshow('存在已填报文件,正在加载......', true);
                if (hasopenfile) {
                    po.poctrl.close();
                    hasopenfile = null;
                }
                po.poctrl.JsFunction_AfterDocumentOpened = "AfterOpen()";

                po.poctrl.WebOpen('../XlsData/' + filename, "xlsNormalEdit", "<%=USERNAME%>");

                dialogshow('已填报文件加载完成!');
                hasopenfile = '../XlsData/' + filename;
            },
            //如果不存在填报文件：
            function () {
                fileexist('../Xls/<%=MBWJ%>',
                //如果存在模板文件：
                function () {
                    IsExistFile = false;
                    dialogshowwin();
                    dialogshow('不存在已填报文件,正在加载模板......', true);
                    OpenMbFile();
                    dialogshow('模板文件加载完成!');
                },
                function () {
                    alert('模板文件和数据文件均不存在，请检测文件是否存在！\n \n 有可能服务器文件损坏导致......');
                    window.external.close();
                }
                )
            });
        }

        $(window).resize(function () {//jquery 的resize事件，当浏览器大小改变时触发！           
            _bodyWidth = document.documentElement.clientWidth;
            _bodyHeight = document.documentElement.clientHeight;
            $('#SMQR').window('move', { top: (_bodyHeight - 420) / 2, left: (_bodyWidth - 610) / 2 }); //easyui的dialog重新定位
        });

        //打开模板执行的过程：
        $(document).ready(function () {
            if ('<%=MBLB %>' == 'YSTZ') {
                //预算模板自动调整上级模板
                $('#dlgmsg').dialog({ modal: true });
                po.poctrl = document.getElementById("PageOfficeCtrl1");
                po.poctrl.Caption = "计划方案：【<%=FAMC%>】 模板名称：【<%=MBMC %>】";
                OpenExcelMB();
            }
            else {
                _bodyWidth = document.documentElement.clientWidth;
                _bodyHeight = document.documentElement.clientHeight;
                if ('<%=MBLX%>' == '1') {
                    InitImbal();
                }
                $('#dlgmsg').dialog({ modal: true });
                po.poctrl = document.getElementById("PageOfficeCtrl1");
                if ('<%=VALIDITY%>' == '0') {
                    po.poctrl.Caption = "当前填报时间已经截止，无法保存报表，请联系预算管理办公室！";
                    //po.poctrl.Caption = "当前流程已过期,无法执行【保存、上报、审批、退回】操作,计划方案：【<%=FAMC%>】 模板名称：【<%=MBMC%>】";
                } else {
                    po.poctrl.Caption = "计划方案：【<%=FAMC%>】 模板名称：【<%=MBMC%>】";
                }
                OpenExcelMB();
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" style="width: 98%; height: 95%;">
    <input type="hidden" id="HidSaveFileName" name="HidSaveFileName" runat="server" />
    <po:PageOfficeCtrl ID="PageOfficeCtrl1" runat="server" Menubar="false" OfficeToolbars="false">
    </po:PageOfficeCtrl>
    <%--<input type="button" onclick="window.external.showMessage(0,'提示1',2)" value="test" />--%>
    <input type="hidden" id="hidindexid" value="CJBM" />
    <input type="hidden" id="hidcheckid" />
    <input type="hidden" id="HidJSBZ" />
    <input type="hidden" id="HidMB" />
    <input type="hidden" id="HidDH" />
    <input type="hidden" id="HidIsOnlyOne" />
    <input type="hidden" id="HidTJ" />
    <div id="WinJS" class="easyui-window" title="模板打回选取" closed="true" collapsible="false"
        minimizable="false" style="width: 600px; height: 300px; padding: 20px; text-align: center;">
        <div style="text-align: left" class="easyui-layout" fit="true" style="width: 100%;
            height: 100%;">
            <div style="height: 30px; background: #ADD8E6;" data-options="region:'north'">
                <%-- 打回原因:
                <input id="TxtWhy" type="text" style="width: 200px" />&nbsp;&nbsp;--%>
                <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
            </div>
            <div data-options="region:'center'" style="width: 100%; height: 100%;">
                <ul id="tt" class="easyui-tree" data-options="checkbox:true">
                </ul>
            </div>
        </div>
    </div>
    <!-- #include file="BmDialog.htm" -->
    <div id="SMQR" class="easyui-dialog" title="书面确认" style="width: 610px; height: 480px;
        padding: 10px; text-align: right;" data-options="maximizable:false,resizable:true,closed:true,modal:true,buttons:[{text:'确定',iconCls:'icon-ok',handler: function(){ getContent()}},{text:'取消',iconCls:'icon-cancel',handler: function(){ close()}}]">
    </div>
    <div id="WinImba" class="easyui-window" title="存在不平衡数据：" closed="true" style="width: 600px; height: 400px" data-options="iconCls:'icon-no',modal:true">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'center'">
                <table id="GrdImba" style="width: 100%; height: 100%;">
                </table>
            </div>
        </div>
    </div>
        <div id="SZBDH" class="easyui-window" title="设置变动行项" closed="true" style="width: 600px;
        height: 420px; padding: 20px; text-align: center; " minimizable="false" collapsible="false" resizable="false" maximizable="false">
        <table style=" width:100%">
            <tr>
                <td style=" width:224px">
                    项目分类：<input id="cslx" type="text" style=" width:120px" class="easyui-combobox" 
                    data-options="valueField:'id',textField:'text',panelHeight:'auto'" editable="false"/>
                </td>
                <td colspan="3" align=left>
                    项目名称：<input id="TxtXMMC" style="width: 150px" type="text" class="easyui-validatebox" />
                </td>
            </tr>
            <tr>
                <td>查找：<input id="TxtselXm"  style="width: 120px" type="text" onkeypress="ChangeText()" onkeyup="ChangeText()"/></td>
            </tr>
             <tr>
                <td style="font-size: 12px"><input type="button" class="button5" value="增加项目" onclick="AddXM()" style="width: 80px"/>&nbsp &nbsp &nbsp &nbsp
                <input type="button" class="button5" value="修改项目名称" onclick="UpdateBdxm()" style="width: 110px"/>
                </td>
                <td style=" padding-left:20px;"><input id="Button1" class="button5" style=" width:80px" type="button" value="删除项目" onclick="DeleteBdxm()" /></td>
                <td style=" padding-left:20px;font-size: 12px"><input type="button" class="button5" value="修改项目" onclick="UpdateBd()" style="width: 80px"  id="xgbdh"/></td>
                <td style=" padding-left:20px" align=left><input type="button" class="button5" value="选择确定" onclick="SelQuery()" style="width: 80px" id="xzqr"/>
                </td>
                
            </tr>
            <tr>
                <table id="dg" class="easyui-datagrid" style="height:80%;width:100%"  
                data-options="autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: false,idField:'BDXMDM'">
                     <thead>
                         <tr>
                             <th data-options="field:'BDXMDM',width:20">
                                 变动项目编码
                             </th>
                             <th data-options="field:'BDXMMC',width:60">
                                 变动项目名称
                             </th>
                         </tr>
                     </thead>
                 </table>
            </tr>
        </table>
          <div id="dialogBDXM"></div>
          <div id="BDXM">
              <table>
                  <tr style=" padding-top:100px">
                    <th>变动项目名称:</th>
                    <th><input id="BDXMMC" type="text" class="easyui-validatebox" data-options="required:true"/></th>
                  </tr>
              </table>
          </div>
        </div>
    </form>
    <%--    <iframe src="" name="show"  style="width:0;height:0"></iframe>--%>
    <%--    <input id="fileSelect" type="file" value="dsfsdff.xls" accept="application/msexcel"/>  
 <input value="Get File" type="button" onclick="getSelectFile();"/> --%>
</body>
</html>
