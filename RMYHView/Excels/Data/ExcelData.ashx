﻿<%@ WebHandler Language="C#" Class="ExcelData" %>

using System;
using System.Globalization;
using System.Web;
using RMYH.BLL;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;
using ICSharpCode.SharpZipLib.Zip;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


public class ExcelData : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        string yy = "";
        string userid = "";
        string hszxdm = "";
        string jhfadm = "";
        string mblb = "";
        string isexe = "";
        string mbdm = "";
        string mbmc = "";
        string zydm = "0";
        string mbwj = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "InitHszx":
                    yy = context.Request.QueryString["yy"];
                    userid = context.Request.QueryString["userid"];
                    res = InitHszx(yy, userid);
                    break;
                case "InitFalb":
                    res = InitFalb();
                    break;
                case "InitZyml":
                    hszxdm = context.Request.QueryString["hszxdm"];
                    res = InitZyml(hszxdm);
                    break;
                case "GetFa":
                    hszxdm = context.Request.QueryString["hszxdm"];
                    yy = context.Request.QueryString["yy"];
                    string nn = context.Request.QueryString["nn"];
                    string jd = context.Request.QueryString["jd"];
                    string fabs = context.Request.QueryString["fabs"];
                    mblb = context.Request.QueryString["mblb"];
                    res = GetFa(hszxdm, yy, nn, jd, fabs, mblb);
                    break;
                case "InitYsyf":
                    res = InitYsyf();
                    break;
                case "InitMb":
                    jhfadm = context.Request.QueryString["jhfadm"];
                    isexe = context.Request.QueryString["isexe"];
                    mblb = context.Request.QueryString["mblb"];
                    mbmc = context.Request.QueryString["mbmc"];
                    zydm = context.Request.QueryString["zydm"];
                    res = InitMb(jhfadm, isexe, mblb, mbmc, zydm);
                    break;
                case "CopyFile":
                    string filename = context.Request.QueryString["filename"];
                    res = CopyFile(filename);
                    break;
                case "UndoMb":
                    jhfadm = context.Request.QueryString["jhfadm"];
                    mbdm = context.Request.QueryString["mbdm"];
                    isexe = context.Request.QueryString["isexe"];
                    mblb = context.Request.QueryString["mblb"];
                    hszxdm = context.Request.QueryString["hszxdm"];
                    res = bll.GetDCLQZMBStr(jhfadm, mbdm, mblb, isexe, hszxdm);
                    break;
                case "InitBm":
                    userid = context.Request.QueryString["userid"];
                    res = InitBm(userid);
                    break;
                case "LoadFa":
                    mblb = context.Request.QueryString["Mblb"];
                    hszxdm = context.Request.QueryString["HSZXDM"];
                    string falb = context.Request.QueryString["SelFalb"];
                    jhfadm = context.Request.QueryString["jhfadm"];
                    yy = context.Request.QueryString["YY"];
                    nn = context.Request.QueryString["NN"];
                    jd = context.Request.QueryString["JD"];
                    zydm = context.Request.QueryString["ZYDM"];
                    res = LoadFa(mblb, hszxdm, falb, jhfadm, yy, nn, jd, zydm);
                    break;
                case "CopyMb":
                    mblb = context.Request.QueryString["Mblb"];
                    falb = context.Request.QueryString["SelFalb"];
                    jhfadm = context.Request.QueryString["jhfadm"];
                    yy = context.Request.QueryString["YY"];
                    nn = context.Request.QueryString["NN"];
                    jd = context.Request.QueryString["JD"];
                    zydm = context.Request.QueryString["ZYDM"];
                    res = loadMb(mblb, falb, jhfadm, yy, nn, jd, zydm);
                    break;
                case "SaveCopyMb":
                    jhfadm = context.Request["jhfadm"];
                    mbdm = context.Request["mbdm"];
                    string fzfadm = context.Request["fzfadm"];
                    filename = context.Request["filename"];
                    string newfilename = context.Request["newfilename"];
                    res = SaveCopyMb(jhfadm, mbdm, fzfadm, filename, newfilename);
                    break;
                case "GetMBCLYJ":
                    jhfadm = context.Request.QueryString["jhfadm"];
                    mbdm = context.Request.QueryString["mbdm"];
                    res = GetMBCLYJ(jhfadm, mbdm);
                    break;
                case "GetContent":
                    jhfadm = context.Request.QueryString["jhfadm"];
                    string cjbm = context.Request.QueryString["cjbm"];
                    string state = context.Request.QueryString["state"];
                    res = GetContent(jhfadm, cjbm, state);
                    break;
                case "GetDownLoadsList":
                    jhfadm = context.Request.QueryString["jhfadm"];
                    isexe = context.Request.QueryString["isexe"];
                    mblb = context.Request.QueryString["mblb"];
                    mbmc = context.Request.QueryString["mbmc"];
                    zydm = context.Request.QueryString["zydm"];
                    res = DownLoadsList(jhfadm, isexe, mblb, mbmc, zydm);
                    break;
                case "GetDownLoads":
                    jhfadm = context.Request["jhfadm"];
                    string excelFiles = context.Request["excelFiles"];
                    string zipFileName = context.Request["zipFileName"];
                    GetDownLoad(jhfadm, excelFiles, zipFileName);
                    break;
                case "AutoCal":
                    mbdm = context.Request["mbdm"];
                    mbwj = context.Request["mbwj"];
                    userid = context.Request["userid"];
                    res = AutoCal(mbdm, mbwj, userid);
                    break;
                case "CalMbAry":
                    jhfadm = context.Request["jhfadm"];
                    mbdm = context.Request["mbdm"];
                    mbwj = context.Request["mbwj"];
                    userid = context.Request["userid"];
                    res = CalMbAry(jhfadm, mbdm, mbwj, userid);
                    break;
                case "GetExcelorJson":
                    string filesName = context.Request["FileName"];
                    GetExcelorJson(filesName);
                    break;
                case "SaveExcelorJsonOnServer":
                    string flag = context.Request.Form["flag"];
                    HttpPostedFile file = context.Request.Files["file"];//获取前端FormData传过来的文件
                    string fileName = context.Request.Form["FileName"];         //获取FormData传过来的mbdms
                    SaveExcelorJsonOnServer(flag, file, fileName);
                    break;
                case "GetUrlPath":
                    filename = context.Request["FileName"];
                    res = GetUrlPath(filename);
                    break;
                case "IsExcelOrJsonExisted":
                    filename = context.Request["filename"];
                    string jsonName = context.Request["jsonname"];
                    res = IsExcelOrJsonExisted(filename, jsonName);
                    break;
                case "CheckMB":
                    filename = context.Request["mbwj"];
                    res = CheckMB(filename);
                    break;
                case "CheckMBExcel":
                    filename = context.Request["mbwj"];
                    res = CheckMBExcel(filename);
                    break;
                case "getSessionId":
                    string sessionId = HttpContext.Current.Session.SessionID;
                    res = sessionId;
                    break;
                case "checkName":
                    string bdxname = context.Request["bdxname"];
                    res = CheckName(bdxname);
                    break;
                case "addBDX":
                    bdxname = context.Request.QueryString["bdxname"];
                    string xmdm = context.Request.QueryString["xmdm"];
                    string ischeckpar = context.Request.QueryString["ischeckpar"];
                    res = AddBDX(bdxname, xmdm, ischeckpar);
                    break;
            }
            context.Response.Write(res);
        }
    }


    /// <summary>
    /// 从服务器端指定路径读取excel，生成字节码返回到前端
    /// </summary>
    /// <param name="excelFiles"></param>
    /// <param name="fileName"></param>
    public void GetExcelorJson(string fileName)
    {
        string newFileName = "";
        // 上报文件保存后文件名待定
        string newFileJsonName = fileName.Substring(0, fileName.LastIndexOf(".")) + ".json";
        //string newFileJsonName = "Jhfadm~10050157~Mb~M10161896.json";
        string res = "";
        byte[] buffer = null;
        try
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + newFileJsonName))
            {
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../XlsData/") + newFileJsonName, FileMode.Open);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
            }
            else
            {
                newFileName = fileName.Substring(0, fileName.LastIndexOf(".")) + ".xlsx";
                //newFileName = "Jhfadm~10050157~Mb~M10161896.xlsx";
                if (!File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + newFileName))
                {
                    string wirteName = fileName.Substring(0, fileName.LastIndexOf("."));
                    //string wirteName = "Jhfadm~10050157~Mb~M10161896";
                    try
                    {
                        ConvertExcel(fileName, wirteName);
                        GetExcelorJson(fileName);
                    }
                    catch (Exception e)
                    {
                        res = e.Message;
                    }

                }
                else
                {
                    FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../XlsData/") + newFileName, FileMode.Open);
                    buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    fs.Close();

                }

            }

        }
        catch (Exception e)
        {
            res = e.Message;

        }
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        if (newFileName == "")
        {
            HttpContext.Current.Response.ContentType = "text/plain;charset=utf-8";  //json
        }
        else
        {
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";//xlsx
        }
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }

    //excel xls转xlsx
    public void ConvertExcel(string readExcelPath, string writeExcelPath)
    {
        var app = new Microsoft.Office.Interop.Excel.Application { Visible = false };
        var book = app.Workbooks.Open(HttpContext.Current.Server.MapPath("../XlsData/") + readExcelPath);
        book.SaveAs(HttpContext.Current.Server.MapPath("../XlsData/") + writeExcelPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
        book.Close();
        app.Quit();
        KillExcelProcess();
    }

    //转化完成关掉进程
    public void KillExcelProcess()
    {
        var myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
        foreach (var myProcess in myProcesses)
        {
            myProcess.Kill();
        }
    }

    //public void GetExcelorJson(string FileName)
    //{
    //    string res = "",rtn="";
    //    byte[] buffer = null;
    //    FileStream fs;
    //    try
    //    {





    //        string FilePath = HttpContext.Current.Server.MapPath("../XlsData/") + FileName;
    //        //如果数据文件不存在的话，就打开模板文件
    //        if (File.Exists(FilePath))
    //        {
    //            fs = new FileStream(FilePath, FileMode.Open);
    //            buffer = new byte[fs.Length];
    //            fs.Read(buffer, 0, buffer.Length);
    //            fs.Close();
    //        }
    //        else
    //        {
    //            using (fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + FileName, FileMode.Open))
    //            {
    //                buffer = new byte[fs.Length];
    //                fs.Read(buffer, 0, buffer.Length);
    //                fs.Close();
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        res = e.Message;

    //    }
    //    HttpContext.Current.Response.Clear();
    //    HttpContext.Current.Response.Buffer = true;
    //    HttpContext.Current.Response.ContentType = "text/plain;charset=utf-8";  //json
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(""));
    //    HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
    //    HttpContext.Current.Response.Flush();
    //    HttpContext.Current.Response.End();

    //}



    /// <summary>
    /// 从服务器端指定路径读取excel，生成字节码返回到前端
    /// </summary>
    /// <param name="excelFiles"></param>
    //public void GetExcelorJson(string FileName)
    //{
    //    string res = "";
    //    byte[] buffer = null;
    //    FileStream fs;
    //    try
    //    {
    //        string FilePath = HttpContext.Current.Server.MapPath("../XlsData/") + FileName;
    //        //如果数据文件不存在的话，就打开模板文件
    //        if (File.Exists(FilePath))
    //        {
    //            fs = new FileStream(FilePath, FileMode.Open);
    //            buffer = new byte[fs.Length];
    //            fs.Read(buffer, 0, buffer.Length);
    //            fs.Close();
    //        }
    //        else
    //        {
    //            fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + FileName, FileMode.Open);
    //            buffer = new byte[fs.Length];
    //            fs.Read(buffer, 0, buffer.Length);
    //            fs.Close();  
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        res = e.Message;

    //    }
    //    HttpContext.Current.Response.Clear();
    //    HttpContext.Current.Response.Buffer = true;
    //    HttpContext.Current.Response.ContentType = "text/plain;charset=utf-8";  //json
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(""));
    //    HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
    //    HttpContext.Current.Response.Flush();
    //    HttpContext.Current.Response.End();

    //}

    /// <summary>
    /// 接收http上传的excel 或json文件，存储到服务器指定的文件夹下
    /// </summary>
    /// <param name="flag">上传类型</param>
    /// <param name="file">文件内容</param>
    /// <param name="mbdm">模板代码</param>
    public void SaveExcelorJsonOnServer(string flag, HttpPostedFile file, string FileName)
    {
        string filename = "";
        filename = FileName.Substring(0, FileName.LastIndexOf('.'));
        if (file != null && file.FileName != "")
        {
            if (flag.Equals("excel"))
            {
                filename = filename + ".xlsx";
            }
            if (flag.Equals("json"))
            {
                filename = filename + ".json";
            }
            file.SaveAs(HttpContext.Current.Server.MapPath("../XlsData/") + filename);
            HttpContext.Current.Response.Write("保存成功！\n文件名：" + filename + "\n" + DateTime.Now);
        }
        else
        {
            HttpContext.Current.Response.Write("请选择文件！");
        }
    }

    /// <summary>
    /// 计算单个模版即：将模版在服务器端打开后获取公式保存
    /// </summary>
    /// <param name="_jhfadm">计划方案代码</param>
    /// <param name="_mbdm">模版代码</param>
    /// <param name="_mbdm">模版文件</param>
    /// <param name="_userid">用户代码</param>
    /// <returns></returns>
    private string CalMbAry(string _jhfadm, string _mbdm, string _mbwj, string _userid)
    {
        try
        {
            //查找模版是否存在：
            string filename = HttpContext.Current.Server.MapPath("../XlsData/") + "Jhfadm" + "~" + _jhfadm + "~" + "Mb" + "~" + _mbwj;
            //是否存在填报文件
            Boolean isfile = File.Exists(filename);

            //如果不存在则加载模版文件：
            if (!isfile)
            {
                filename = HttpContext.Current.Server.MapPath("../Xls/") + _mbwj;
                if (!File.Exists(filename))
                {
                    dealExcel.WritePF("m", _jhfadm, _mbdm, _userid, "2", "填报文件与模版文件均不存在！");
                    return _mbdm;
                }
            }
            //计算单个模版：
            dealExcel.CalSingleMb(filename, isfile);
        }
        catch (Exception e)
        {
        }
        return _mbdm;
    }



    /// <summary>
    /// 自动计算功能
    /// </summary>
    /// <param name="_mbdm">模版代码集合“10230019，11237860”</param>
    /// <param name="_userid">用户代码</param>
    /// <returns>按优先级排序的上级模版集合“10230019，11237860”</returns>
    private string AutoCal(string _mbdm, string _mbwj, string _userid)
    {
        string res = "{\"mbs\":[";
        //首先保存计算当前模版：

        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        //按递归顺序返回上级模版
        string mbs = bll.GetAllParMB(_mbdm);
        if (!mbs.Equals(""))
        {
            mbs = "'" + mbs.Replace(",", "','") + "'";
            ds = bll.Query("SELECT MBDM,MBMC FROM TB_YSBBMB WHERE MBDM IN (" + mbs + ")");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        res += "{\"mbdm\":\"" + ds.Tables[0].Rows[i][0].ToString() + "\",\"mbmc\":\""
                            + ds.Tables[0].Rows[i][1].ToString() + "\"}";
                    }
                    else
                    {
                        res += ",{\"mbdm\":\"" + ds.Tables[0].Rows[i][0].ToString() + "\",\"mbmc\":\""
                        + ds.Tables[0].Rows[i][1].ToString() + "\"}";
                    }
                }
            }
        }
        res += "]}";
        return res;
    }

    /// <summary>
    /// 根据用户代码装入模版
    /// </summary>
    /// <param name="_userid">用户代码</param>
    /// <returns>selbm下的下拉框</returns>
    private string InitBm(string _userid)
    {
        string res = "<option value='0' selected='selected'>全部</option>";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        ds = bll.Query("SELECT DISTINCT D.ZYDM,D.ZYMC FROM TB_JSYH A,TB_MBJSQX B,TB_YSBBMB C,TB_JHZYML D"
              + " WHERE A.USERID='" + _userid + "' AND B.CXQX='1'  AND A.JSDM=B.JSDM AND B.MBDM=C.MBDM"
              + "  AND C.ZYDM=D.ZYDM");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";

            }
        }
        return res;
    }

    private string CopyFile(string _filename)
    {
        string res = "失败";
        res = HttpContext.Current.Server.MapPath("./") + "Xls\\";//test.xls";
        File.Copy(res + "test.xls", res + _filename, true);
        return res;
    }

    private string InitYsyf()
    {
        int month = DateTime.Now.Month;
        string res = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        //ds = bll.Query("SELECT ZFCS FROM XT_CSSZ WHERE XMFL='YSSJMON' AND XMDH_A=1 AND XMDH_B IS NULL");
        //取财务实际销售利润表中有数的离当前月最近的月份：
        ds = bll.Query("SELECT CASE SUBSTRING(CONVERT(CHAR(6),MAX(CONVERT(INT,YY+NN))),5,1) WHEN '0' THEN SUBSTRING(CONVERT(CHAR(6),MAX(CONVERT(INT,YY+NN))),6,1) ELSE SUBSTRING(CONVERT(CHAR(6),MAX(CONVERT(INT,YY+NN))),5,2) END "
            + "FROM  TB_SJXSLR WHERE CONVERT(INT,YY+NN)<=CONVERT(INT,SUBSTRING(convert(char,getdate(),112),1,6))");

        if (ds.Tables[0].Rows.Count > 0)
        {
            month = int.Parse(ds.Tables[0].Rows[0][0].ToString());
        }
        for (int i = 1; i <= 12; i++)
        {
            if (month == i)
            {
                res += "<option value='" + i.ToString() + "' selected='selected'>"
                                     + i.ToString() + "</option>";
            }
            else
            {
                res += "<option value='" + i.ToString() + "' >"
                                     + i.ToString() + "</option>";
            }
        }
        return res;
    }

    private string GetFa(string _hszxdm, string _yy, string _nn, string _jd, string _fabs, string _mblb)
    {
        string sql = "SELECT A.JHFADM,A.JHFANAME FROM TB_JHFA A ";
        //如果是填报或者分解的话则按方案查询加限制条件，即只有方案发起的方案才显示：
        if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "baseys" || _mblb == "resosb" || _mblb == "resoys" || _mblb == "resosp")
        {
            sql += ",TB_YSLCFS B ";
        }
        switch (_fabs)
        {
            //月：    
            case "1":
                sql += " WHERE  A.HSZXDM='" + _hszxdm + "' AND A.YY='" + _yy + "' AND A.NN='"
                    + int.Parse(_nn).ToString("00") + "' AND A.FABS='" + _fabs + "'";
                break;
            //季：    
            case "2":
                sql += " WHERE A.HSZXDM='" + _hszxdm + "' AND A.YY='" + _yy + "' AND A.JD='"
                    + _jd + "' AND A.FABS='" + _fabs + "'";
                break;
            //年：    
            case "3":
                sql += " WHERE A.HSZXDM='" + _hszxdm + "' AND A.YY='" + _yy + "' AND A.FABS='"
                    + _fabs + "'";
                break;
            //其他：    
            case "4":
                sql += " WHERE A.HSZXDM='" + _hszxdm + "' AND A.YY='" + _yy + "' AND A.NN='"
                    + int.Parse(_nn).ToString("00") + "' AND A.FABS='" + _fabs + "'";
                break;
        }
        //如果是填报或者分解的话则按方案查询加限制条件，即只有方案发起的方案才显示：
        if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "baseys" || _mblb == "resosb" || _mblb == "resoys" || _mblb == "resosp")
        {
            sql += " AND A.JHFADM=B.JHFADM AND B.ISFQ=1 ";
        }
        if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "baseys")
        {
            sql += " AND  A.YSBS='3'";
        }
        else if (_mblb == "respfj" || _mblb == "respys" || _mblb == "respsp")
        {
            sql += " AND  A.YSBS='4'";
        }
        else if (_mblb == "resosb" || _mblb == "resoys" || _mblb == "resosp")
        {
            sql += " AND  A.YSBS='5'";
        }
        sql += " ORDER BY A.XTSJ DESC";
        return FillData("select", sql);
    }

    /// <summary>
    /// 根据及计划方案代码获取是否走流程标记
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns>true代表走流程；false代表不走流程</returns>
    private bool IsLCSP(string JHFADM)
    {
        bool Flag = false;
        if (JHFADM != "")
        {
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            string SQL = "SELECT SCBS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
            if (BLL.Query(SQL).Tables[0].Rows[0]["SCBS"].ToString() == "1")
            {
                Flag = true;
            }
            else
            {
                Flag = false;
            }
        }
        return Flag;
    }


    private string FillData(string _type, string _sql)
    {
        if (!string.IsNullOrEmpty(_type) && !string.IsNullOrEmpty(_sql))
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = new DataSet();
            ds = bll.Query(_sql);
            string res = "[]";
            if (ds.Tables[0].Rows.Count > 0)
            {
                switch (_type)
                {
                    case "select":
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                res = "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                            }
                            else
                            {
                                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                            }

                        }
                        break;
                    case "easyui-tree":
                        res = gettreenode("", ds.Tables[0]);
                        break;
                    case "easyui-datagrid":
                        res = getdatagrid(ds.Tables[0]);
                        break;
                }
            }
            return res;
        }
        else
        {
            return "[]";
        }
    }

    private string InitMb(string _jhfadm, string _isdone, string _mblb, string _mbmc, string _zydm)
    {
        TB_YSLCBLL BLL = new TB_YSLCBLL();
        //TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = BLL.GetMBListSQL(_jhfadm, _isdone, _mblb, _zydm);
        if (!_mbmc.Trim().Equals(""))
        {
            if (sql.IndexOf("UNION") >= 0)
            {
                sql = sql.Replace("UNION", "AND A.MBMC LIKE '%" + _mbmc + "%' UNION ");
            }
            sql += " AND A.MBMC LIKE '%" + _mbmc + "%'";
        }
        string res = "";
        sql += " ORDER BY 6";
        res = FillData("easyui-datagrid", sql);
        //res = FillData("easyui-datagrid", "SELECT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,A.MBZQ,D.XMMC MBZQMC "
        //    + "FROM TB_YSBBMB A,"
        //+ "XT_CSSZ B,XT_CSSZ D WHERE A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) "
        //+ "AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A) "
        //+ "AND D.XMFL='FAZQ'");
        return res;
    }

    private string getdatagrid(DataTable _table)
    {
        string res = "";
        string sr = "";
        if (_table.Rows.Count > 0)
        {
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                DataRow row = _table.Rows[i];
                sr = "";
                if (row != null)
                {
                    if (res == "")
                    {
                        res = "{";
                    }
                    else
                    {
                        res += ",{";
                    }
                    for (int j = 0; j < _table.Columns.Count; j++)
                    {
                        if (sr == "")
                        {
                            sr = "\"" + _table.Columns[j].ColumnName + "\":\"" + row[j].ToString() + "\"";
                        }
                        else
                        {
                            sr += ",\"" + _table.Columns[j].ColumnName + "\":\"" + row[j].ToString() + "\"";
                        }
                    }
                    res += sr + "}";
                }
            }

            return "[" + res + "]";
        }
        else
        {
            return "";
        }
    }

    private string gettreenode(string _fbdm, DataTable _table)
    {
        string res = "";
        DataRow[] rows = _table.Select(_table.Columns[2].ColumnName + "='" + _fbdm + "'");
        for (int i = 0; i < rows.Length; i++)
        {
            DataRow row = rows[i];
            if (row != null)
            {
                if (row[3].ToString() == "1")
                {
                    if (res == "")
                    {
                        res = "{\"text\":\"" + row[1].ToString() + "\"}";
                    }
                    else
                    {
                        res += ",{\"text\":\"" + row[1].ToString() + "\"}";
                    }
                }
                else if (row[3].ToString() == "0")
                {
                    if (res == "")
                    {
                        res = "{\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                    else
                    {
                        res += ",{\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                }
            }
        }
        return "[" + res + "]";
    }

    private string InitZyml(string _hszxdm)
    {
        return FillData("easyui-tree", "SELECT ZYDM,ZYMC,FBDM,YJDBZ FROM TB_JHZYML WHERE (HSZXDM='" + _hszxdm
            + "' OR ZYDM='0') ORDER BY ZYNBBM ");
    }

    private string InitHszx(string _yy, string _userid)
    {
        //return FillData("select", "SELECT HSZXDM,HSZXMC FROM TB_HSZXZD WHERE YY='" + _yy + "' AND YJDBZ='1'");
        string sql = "SELECT HSZXDM,HSZXMC FROM TB_HSZXZD WHERE NBBM LIKE "
        + "(SELECT B.NBBM FROM TB_USER A,TB_HSZXZD B WHERE A.USER_ID='" + _userid
        + "' AND A.CC_NO=B.HSZXDM AND YY='" + _yy + "')+'%' AND YY='" + _yy + "'";
        return FillData("select", sql);
    }

    private string InitFalb()
    {
        return FillData("select", "SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ'");
    }

    /// <summary>
    /// 生成模板复制的要复制的方案
    /// </summary>
    /// <param name="mblb">模板类别</param>
    /// <param name="falb">方案类别</param>
    /// <param name="jhfadm">计划方案</param>
    /// <param name="yy">年</param>
    /// <param name="nn">月</param>
    /// <param name="jd">季</param>
    /// <param name="zydm">作业代码</param>
    /// <param name="selfa">要复制的方案代码</param>
    /// <returns></returns>
    public string LoadFa(string mblb, string hszxdm, string falb, string jhfadm, string yy, string nn, string jd, string zydm)
    {
        mblb = mblb.Replace("\"", "");
        hszxdm = hszxdm.Replace("\"", "");
        falb = falb.Replace("\"", "");
        jhfadm = jhfadm.Replace("\"", "");
        zydm = zydm.Replace("\"", "");
        yy = yy.Replace("\"", "").PadLeft(2, '0');
        nn = nn.Replace("\"", "").PadLeft(2, '0');
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<string> list = new List<string>();
        string initFaSql = "";
        string faRes = "";
        string initFa_1 = "SELECT A.JHFADM,A.JHFANAME FROM TB_JHFA A ,TB_YSLCFS B  WHERE A.HSZXDM='" + hszxdm + "' AND A.YY='" + yy + "' AND A.FABS='" + falb + "' AND A.JHFADM=B.JHFADM AND B.ISFQ=1  AND  A.YSBS='3'";
        if (jhfadm != "" && yy != "" && nn != "" && jd != "")//根据年月变化先加载要选择复制的方案
        {
            switch (falb)
            {
                case "3": //年
                    if (mblb != "resosb" && mblb != "resoys" && mblb != "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where  YY='" + yy + "' and FABS='" + falb + "' and YSBS in ('3','4') and JHFADM!='" + jhfadm + "'";
                    }
                    else if (mblb == "resosb" || mblb == "resoys" || mblb == "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where YY='" + yy + "'and FABS='" + falb + "' and YSBS ='5' and JHFADM !='" + jhfadm + "' ";
                        //
                        initFa_1 += " ORDER BY A.XTSJ DESC";
                    }
                    break;
                case "2": //季 
                    if (mblb != "resosb" && mblb != "resoys" && mblb != "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where  YY='" + yy + "' and FABS='" + falb + "' and YSBS in ('3','4') and JD='" + jd + "' and JHFADM!='" + jhfadm + "'";
                    }
                    else if (mblb == "resosb" || mblb == "resoys" || mblb == "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where YY='" + yy + "'and FABS='" + falb + "' and YSBS ='5' and JD='" + jd + "' and JHFADM !='" + jhfadm + "' ";
                        initFa_1 += " and A.JD='" + jd + "' ORDER BY A.XTSJ DESC";
                    }
                    break;
                case "1": //月  
                    if (mblb != "resosb" && mblb != "resoys" && mblb != "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where  YY='" + yy + "' and FABS='" + falb + "'and NN='" + nn + "' and YSBS in ('3','4') and JHFADM!='" + jhfadm + "'";
                    }
                    else if (mblb == "resosb" || mblb == "resoys" || mblb == "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where YY='" + yy + "'and FABS='" + falb + "'and NN='" + nn + "' and YSBS ='5' and JHFADM !='" + jhfadm + "' ";
                        initFa_1 += " and A.NN='" + nn + "' ORDER BY A.XTSJ DESC";
                    }
                    break;
                case "4": //其他
                    if (mblb != "resosb" && mblb != "resoys" && mblb != "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where  YY='" + yy + "' and FABS='" + falb + "'and NN='" + nn + "' and YSBS in ('3','4') and JHFADM!='" + jhfadm + "'";
                    }
                    else if (mblb == "resosb" || mblb == "resoys" || mblb == "resosp")
                    {
                        initFaSql = "select JHFADM,JHFANAME from TB_JHFA where YY='" + yy + "'and FABS='" + falb + "'and NN='" + nn + "' and YSBS ='5' and JHFADM !='" + jhfadm + "' ";
                        initFa_1 += " and A.NN='" + nn + "' ORDER BY A.XTSJ DESC";
                    }
                    break;
                default:
                    break;
            }

            DataSet initfa = BLL.Query(initFaSql);

            if (initfa.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < initfa.Tables[0].Rows.Count; i++)
                {
                    faRes += "<option value='" + initfa.Tables[0].Rows[i][0].ToString() + "'>"
                        + initfa.Tables[0].Rows[i][1].ToString() + "</option>";
                }
            }

            //如果是预算分解，就刷出编制的方案
            if (mblb == "resosb" || mblb == "resoys" || mblb == "resosp")
            {
                initfa.Clear();
                initfa = BLL.Query(initFa_1);
                if (initfa.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < initfa.Tables[0].Rows.Count; i++)
                    {
                        faRes += "<option value='" + initfa.Tables[0].Rows[i][0].ToString() + "'>"
                            + initfa.Tables[0].Rows[i][1].ToString() + "</option>";
                    }
                }

            }
            //没有选择时间所对应的计划方案
            if (faRes == "") { faRes = "0"; }

        }
        return faRes;
    }

    /// <summary>
    /// 加载模板列表，加载计划方案和模板列表   by:liguosheng
    /// </summary>
    /// <param name="mblb">模板类型</param>
    /// <param name="falb">方案类别</param>
    /// <param name="jhfadm">计划方案代码</param>
    /// <param name="yy">年</param>
    /// <param name="nn">月</param>
    /// <param name="jd">季</param>
    /// <returns></returns>
    public string loadMb(string mblb, string falb, string jhfadm, string yy, string nn, string jd, string zydm)
    {
        mblb = mblb.Replace("\"", "");
        falb = falb.Replace("\"", "");
        jhfadm = jhfadm.Replace("\"", "");
        zydm = zydm.Replace("\"", "");
        yy = yy.Replace("\"", "").PadLeft(2, '0');
        nn = nn.Replace("\"", "").PadLeft(2, '0');
        List<string> list = new List<string>();
        string sql = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder mbStr = new StringBuilder();

        if (zydm == "0")
        {
            sql = "SELECT DISTINCT A.MBDM MBDM,B.MBMC MBMC,B.MBWJ MBWJ FROM TB_MBJSQX A,TB_YSBBMB B WHERE MBXGQX='1' AND A.MBDM=B.MBDM AND B.MBZQ='" + falb + "' AND A.JSDM in (select JSDM from TB_JSYH where USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "')";
        }
        else
        {
            sql = "SELECT DISTINCT A.MBDM MBDM,B.MBMC MBMC,B.MBWJ MBWJ FROM TB_MBJSQX A,TB_YSBBMB B WHERE MBXGQX='1' AND A.MBDM=B.MBDM AND B.MBZQ='" + falb + "' AND A.JSDM in (select JSDM from TB_JSYH where USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "') and B.ZYDM='" + zydm + "'";
        }

        if (mblb == "basesb" || mblb == "basesp" || mblb == "respfj") //预算申请填报、预算报表审批修改、 批复预算基础数据分解维护
        {
            sql += "AND B.MBLX IN('1','2','3')";
        }

        if (mblb == "baseys" || mblb == "respsp" || mblb == "respys")//生成上报预算报表、批复预算审批、生成批复预算
        {
            sql += "AND B.MBLX='4'";
        }

        if (mblb == "resosb")// 预算分解填报
        {
            sql += "AND B.MBLX IN('7','1','3','2')";
        }

        if (mblb == "resosp")// 预算分解报表审批
        {
            sql += "AND B.MBLX IN('7','2','3','5')";
        }

        if (mblb == "resoys")// 预算分解报表生成
        {
            sql += "AND B.MBLX='5'";
        }

        if (mblb == "otheryd")//月度分析报表 
        {
            sql += "AND B.MBLX='6'";
        }

        if (mblb == "otherdy")//自定义报表
        {
            sql += "AND B.MBLX='8'";
        }

        DataSet da = BLL.Query(sql);
        string path = HttpContext.Current.Server.MapPath("../XlsData/");
        bool boo = false;

        mbStr.Append("{\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            mbStr.Append("{");
            mbStr.Append("\"MBDM\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["MBDM"].ToString() + "\"" + ",");
            mbStr.Append("\"MBMC\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["MBMC"].ToString() + "\"" + ",");
            mbStr.Append("\"MBWJ\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["MBWJ"].ToString() + "\"" + ",");
            boo = File.Exists(path + "Jhfadm~" + jhfadm + "~Mb~" + da.Tables[0].Rows[i]["MBWJ"].ToString());
            mbStr.Append("\"SFCZ\":");
            if (boo)
            {
                mbStr.Append("\"" + "模板存在" + "\"");
            }
            else
            {
                mbStr.Append("\"" + "模板不存在" + "\"");
            }

            mbStr.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                mbStr.Append(",");
            }


        }
        mbStr.Append("]}");

        return mbStr.ToString();
    }


    /// <summary>
    /// 保存复制模板 by:liguosheng
    /// </summary>
    /// <param name="jhfadm"></param>
    /// <param name="mbdm"></param>
    /// <param name="fzfadm"></param>
    /// <param name="fileName"></param>
    /// <param name="newFileName"></param>
    /// <returns></returns>
    public string SaveCopyMb(string jhfadm, string mbdm, string fzfadm, string fileName, string newFileName)
    {
        jhfadm = jhfadm.Replace("\"", "");
        mbdm = mbdm.Replace("\"", "");
        fzfadm = fzfadm.Replace("\"", "");
        fileName = fileName.Replace("\"", "");
        newFileName = newFileName.Replace("\"", "");
        bool boo;
        List<string> existlist = new List<string>();
        List<string> nolist = new List<string>();
        List<string> dmlist = new List<string>();
        string[] dm = mbdm.Split(',');
        string[] fname = fileName.Split(',');
        string[] nfname = newFileName.Split(',');
        string[] splitname = new string[2];
        string res = HttpContext.Current.Server.MapPath("../XlsData/");
        string msg = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();

        StringBuilder delSql = new StringBuilder();
        StringBuilder insql = new StringBuilder();

        for (int i = 0; i < fname.Length; i++)
        {
            boo = File.Exists(res + fname[i].Substring(0, fname[i].LastIndexOf("~")));
            if (boo)
            {
                existlist.Add(fname[i].Substring(0, fname[i].LastIndexOf("~")) + "," + nfname[i]);//将存在的excel 文件名和相对应的新文件名按逗号存在existlist
                dmlist.Add(dm[i]);//将存在文件的文件代码存到dmlist，拼接sql使用
            }
            else
            {
                nolist.Add(fname[i].Substring(fname[i].LastIndexOf("~") + 1)); //将不存在的文件名保存到nolist,返回前台提示
            }
        }

        if (existlist.Count == 0)
        {
            msg += "|";
            msg += string.Join(",", nolist.ToArray());//将list按，转成string并返回。
            return msg;//将list按，转成string并返回。
        }

        for (int i = 0; i < existlist.Count; i++)
        {
            splitname = existlist[i].Split(',');
            File.Copy(res + splitname[0], res + splitname[1], true);     //将existlist存在的文件复制按相应的名称复制
        }

        //执行sql
        delSql.Append("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + jhfadm + "' AND MBDM in(");
        insql.Append("INSERT INTO TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) SELECT '" + jhfadm + "'JHFADM,MBDM ,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE FROM TB_BBFYSJ WHERE JHFADM='" + fzfadm + "' AND MBDM  in(");
        for (int i = 0; i < dmlist.Count; i++)
        {
            delSql.Append("'" + dmlist[i] + "'");
            insql.Append("'" + dmlist[i] + "'");

            if (i < dmlist.Count - 1)
            {
                delSql.Append(",");
                insql.Append(",");
            }
        }
        delSql.Append(")");
        insql.Append(")");
        BLL.Query(delSql.ToString());
        BLL.Query(insql.ToString());

        if (existlist.Count != 0 && nolist.Count != 0)
        {//既存在复制成功的模板，又有不存在的模板
            msg += "1";
            msg += "|";
            msg += string.Join(",", nolist.ToArray());
        }
        else if (nolist.Count == 0)
        {
            msg += "1";
            msg += "|";
        }

        return msg;
    }

    /// <summary>
    /// 查看审批，打回意见
    /// </summary>
    /// <param name="jhfadm">计划方案代码</param>
    /// <param name="mbdm">模板代码</param>
    /// <returns></returns>
    public string GetMBCLYJ(string jhfadm, string mbdm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string MBZQ = bll.GetMBZQByJHFADM(jhfadm);//获取当前计划方案代码的模板周期
        string sql = "SELECT MB.MBDM,MB.MBMC,U.USER_NAME,SB.CLSJ,SB.SPYJ FROM TB_MBLCSB SB,TB_YSBBMB MB,TB_USER U WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND SB.DQMBDM=MB.MBDM AND MB.MBZQ='" + MBZQ + "' AND U.USER_ID=SB.CLR";
        DataSet da = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        Str.Append("{\"total\":");
        Str.Append(da.Tables[0].Rows.Count);
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            Str.Append("\"MBDM\":");
            Str.Append(da.Tables[0].Rows[i]["MBDM"].ToString() + ",");
            Str.Append("\"MBMC\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MBMC"].ToString() + "\"" + ",");
            Str.Append("\"USER_NAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_NAME"].ToString() + "\"" + ",");
            Str.Append("\"CLSJ\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["CLSJ"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"SPYJ\":");
            if (da.Tables[0].Rows[i]["SPYJ"].ToString() == "")
            {
                Str.Append("\"" + "" + "\"");
            }
            else
            {
                Str.Append("\"" + Encoding.Default.GetString((byte[])da.Tables[0].Rows[i]["SPYJ"]).ToString().Replace("\"", "\\\"") + "\"");
            }
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }


    /// <summary>
    /// 查询承诺书内容
    /// </summary>
    /// <param name="jhfadm">计划方案代码</param>
    /// <param name="cjbm">层级编码</param>
    /// <param name="state">状态</param>
    /// <returns>书面承诺界面内容</returns>
    public string GetContent(string jhfadm, string cjbm, string state)
    {
        string res = "";
        if (state == "0")
        {
            state = "-1";
        }
        else if (state == "1")
        {
            state = "0,1";
        }
        string sql = "SELECT S1.SPYJ FROM TB_MBLCSB S1 WHERE JHFADM='" + jhfadm + "' AND CJBM='" + cjbm + "' AND STATE IN(" + state + ") AND ISNULL(CLSJ,CREATETIME)=(SELECT MAX(ISNULL(CLSJ,CREATETIME)) FROM  TB_MBLCSB S2 WHERE S1.JHFADM=S2.JHFADM AND S1.CJBM=S2.CJBM AND STATE IN(" + state + "))";
        //string sql01 = "SELECT S1.SPYJ FROM TB_MBLCSB S1 WHERE JHFADM='10161862' AND CJBM='004400010001' AND STATE IN(-1) AND ISNULL(CLSJ,CREATETIME)=(SELECT MAX(ISNULL(CLSJ,CREATETIME)) FROM  TB_MBLCSB S2 WHERE S1.JHFADM=S2.JHFADM AND S1.CJBM=S2.CJBM AND STATE IN(-1))";
        //string sql3 = "select SPYJ from TB_MBLCSB where DQMBDM='10161959' and CJBM='0139'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet da = bll.Query(sql);
        if (da.Tables[0].Rows.Count == 0)
        {
            return res;
        }
        else if (da.Tables[0].Rows.Count != 0 && da.Tables[0].Rows[0]["SPYJ"].ToString() != "")
        {
            byte[] by = (byte[])da.Tables[0].Rows[0]["SPYJ"];//image转化为byte数组
            res = Encoding.Default.GetString(by);//byte数组转化为string


        }
        else
        {
            return res;
        }

        return res;
    }

    /// <summary>
    /// 获取下载模板列表，并提示哪些模板不存在
    /// </summary>
    /// <param name="jhfadm"></param>
    /// <param name="isexe"></param>
    /// <param name="mblb"></param>
    /// <param name="mbmc"></param>
    /// <param name="zydm"></param>
    public string DownLoadsList(string jhfadm, string isexe, string mblb, string mbmc, string zydm)
    {
        bool boo;
        StringBuilder Str = new StringBuilder();
        TB_ZDSXBBLL zdsx = new TB_ZDSXBBLL();
        string sql = zdsx.GetMBListSQL(jhfadm, isexe, mblb, zydm);
        if (!mbmc.Trim().Equals(""))
        {
            if (sql.IndexOf("UNION") >= 0)
            {
                sql = sql.Replace("UNION", "AND A.MBMC LIKE '%" + mbmc + "%' UNION ");
            }
            sql += " AND A.MBMC LIKE '%" + mbmc + "%'";
        }

        DataSet da = zdsx.Query(sql);
        string path = HttpContext.Current.Server.MapPath("../XlsData/");

        Str.Append("{\"total\":");
        Str.Append(da.Tables[0].Rows.Count);
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            string fileName = da.Tables[0].Rows[i]["MBWJ"].ToString();
            Str.Append("{");
            Str.Append("\"MBDM\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MBDM"].ToString() + "\"" + ",");
            Str.Append("\"XMMC\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["XMMC"].ToString() + "\"" + ",");
            Str.Append("\"MBMC\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MBMC"].ToString() + "\"" + ",");
            Str.Append("\"MBWJ\":");
            fileName = fileName.Substring(0, fileName.LastIndexOf('.')) + ".xlsx";
            Str.Append("\"" + fileName + "\"" + ",");
            Str.Append("\"SFCZ\":");
            //判断服务器上模板文件是否存在

            //fileName = fileName.Substring(0, fileName.LastIndexOf('.')) + ".json";
            boo = File.Exists(path + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + fileName);
            if (boo)//存在
            {
                Str.Append("\"" + "模板存在" + "\"");
            }
            else//不存在
            {
                Str.Append("\"" + "模板不存在" + "\"");
            }
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");
        return Str.ToString();

    }

    /// <summary>
    /// 批量进行多个文件压缩到一个文件  
    /// </summary>
    /// <param name="files">文件列表(绝对路径)</param>
    /// <param name="zipFileName">生成的zip文件名称</param>
    /// <returns></returns>
    public void GetDownLoad(string jhfadm, string excelFiles, string zipFileName)
    {
        string excelfiles = "";
        string filesName = "";
        string[] files = excelFiles.Split('|');
        MemoryStream ms = new MemoryStream();
        byte[] buffer = null;
        using (ZipFile file = ZipFile.Create(ms))
        {
            file.BeginUpdate();

            file.NameTransform = new MyNameTransfom();
            foreach (string item in files)
            {
                excelfiles = item.Split(',')[0];
                filesName = item.Split(',')[1];
                if (File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + excelfiles))
                {
                    file.Add(HttpContext.Current.Server.MapPath("../XlsData/") + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + excelfiles, filesName + ".xlsx");//添加文件并重命名
                }
            }
            file.CommitUpdate();
            buffer = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(buffer, 0, buffer.Length);   //读取文件内容(1次读ms.Length/1024M)  
            ms.Flush();
            ms.Close();
        }
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/x-zip-compressed";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(zipFileName));
        HttpContext.Current.Response.BinaryWrite(buffer);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }

    /// <summary>
    /// 处理生成的文件夹内容，否则会把生成的excel目录也压缩进去
    /// </summary>
    public class MyNameTransfom : ICSharpCode.SharpZipLib.Core.INameTransform
    {

        #region INameTransform 成员

        public string TransformDirectory(string name)
        {
            return null;
        }

        public string TransformFile(string name)
        {
            return Path.GetFileName(name);
        }

        #endregion
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string GetUrlPath(string filename)
    {
        //filename = "M" + filename + ".json";
        //filename = "Jhfadm~10050157~Mb~M10161896.json";
        filename = filename.Substring(0, filename.LastIndexOf('.')) + ".json";
        return File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + filename) ? "1" : "0";
    }
    public string IsExcelOrJsonExisted(string filename, string jsonName)
    {
        var excelName = filename.Substring(0, filename.LastIndexOf('.')) + ".xlsx";
        if (File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + filename) || File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + jsonName) || File.Exists(HttpContext.Current.Server.MapPath("../XlsData/") + excelName))
        {
            return "1";
        }

        return "0";
    }
    public string CheckMB(string filename)
    {
        string fileJsonName = filename.Substring(0, filename.LastIndexOf(".")) + ".json";
        string excelName = filename.Substring(0, filename.LastIndexOf(".")) + ".xlsx";
        if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + fileJsonName))
            return "0";
        if (!File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + filename) && !File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + excelName))
            return "0";
        return "1";
    }
    private string CheckMBExcel(string filename)
    {
        string fileJsonName = filename.Substring(0, filename.LastIndexOf(".")) + ".json";
        string excelName = filename.Substring(0, filename.LastIndexOf(".")) + ".xlsx";
        if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + filename) || File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + excelName) || File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + fileJsonName))
        {
            return "1";
        }
        return "0";
    }

    private string CheckName(string bdxname)
    {
        string sql = "SELECT 1 FROM TB_XMXX WHERE XMMC LIKE '%" + bdxname + "%'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(sql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            return "false";
        }
        sql = "SELECT 1 from TB_BDXMJCBM WHERE BDXMMC LIKE '%" + bdxname + "%'";
        ds = bll.Query(sql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            return "false";
        }
        return "true";
    }

    private string AddBDX(string bdxname, string xmdm, string ischeckpar)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        const string querySpdhSql = "SELECT SPDH FROM TB_BDXMSQD ORDER BY SPDH DESC";
        DataSet ds = bll.Query(querySpdhSql);
        string spdh;
        if (ds.Tables[0].Rows.Count > 0)
        {
            int num = int.Parse(ds.Tables[0].Rows[0][0].ToString().Substring(4));
            spdh = "BDXM" + (num + 1);
        }
        else
        {
            spdh = "BDXM10001";
        }
        if (ischeckpar == "true")
        {
            string queryParXmdm = "SELECT PAR FROM TB_XMXX WHERE XMDM='" + xmdm + "'";
            DataSet dsSet = bll.Query(queryParXmdm);
            xmdm = dsSet.Tables[0].Rows[0][0].ToString();
        }
        string sql = "INSERT INTO TB_BDXMSQD(SPDH,XMDM,BDXMMC) VALUES('" + spdh + "','" + xmdm + "','" + bdxname + "')";
        int flag = bll.ExecuteSql(sql);
        string queryBDXMBM =
            "SET ROWCOUNT 1 SELECT BDXMBM FROM TB_BDXMJCBM WHERE XMDM='" + xmdm + "' ORDER BY BDXMBM DESC SET ROWCOUNT 0";
        ds.Clear();
        ds = bll.Query(queryBDXMBM);
        string bdxmbm;
        if (ds.Tables[0].Rows.Count > 0)
        {
            string str = ds.Tables[0].Rows[0][0].ToString();
            string strPar = str.Substring(0, str.Length - 4);
            string strChild = str.Substring(str.Length - 4);
            string n = (int.Parse(strChild) + 1).ToString().PadLeft(4, '0');
            bdxmbm = strPar + n;
        }
        else
        {
            queryBDXMBM = "SELECT XMBM FROM TB_XMXX WHERE XMDM='" + xmdm + "'";
            ds = bll.Query(queryBDXMBM);
            string strPar = ds.Tables[0].Rows[0][0].ToString();
            bdxmbm = strPar + "0001";
        }
        string insertSql =
            "INSERT INTO TB_BDXMJCBM(XMDM,BDXMBM,BDXMMC,GKBM,XMSFLRSL,SJLYFS,CWHSFS,FLBH,CWHSBH) VALUES('" + xmdm + "','" + bdxmbm + "','" + bdxname + "','0',0,'0',null,null,null)";
        flag = bll.ExecuteSql(insertSql);
        return flag.ToString();
    }

}