﻿<%@ WebHandler Language="C#" Class="MBDataHandler" %>

using System;
using System.IO;
using System.Web;
using RMYH.BLL;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Text;

public class MBDataHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string action = context.Request.Params["action"];
        string FADM = context.Request.Params["FADM"];

        if (action == "QYJS")
        {
            //区域计算
            string rtn = "";
            string YY = context.Request.Params["GSYY"];
            string NN = context.Request.Params["GSNN"];
            string FABS = context.Request.Params["FABS"];
            string FADM2 = context.Request.Params["FADM2"];
            string Address = context.Request.Params["Address"];
            string FileName = context.Request.Params["FileName"];
            string serveryy = context.Request.Params["serveryy"];
            rtn = QYJS(FileName, YY, NN, FADM, FADM2, FABS, serveryy, Address).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);
        }
        else if (action == "QQYJS")
        {
            //全区域计算
            string rtn = "";
            string GSYY = context.Request.Params["GSYY"];
            string GSMM = context.Request.Params["GSMM"];
            string FABS = context.Request.Params["FABS"];
            string FADM2 = context.Request.Params["FADM2"];
            string FileName = context.Request.Params["FileName"];
            string YY = context.Request.Params["YY"];
            rtn = QQYJS(FileName, GSYY, GSMM, FADM, FADM2, FABS, YY).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);

        }
        else if (action == "GetCellStyle")
        {
            //返回模板页单元格样式
            string FileName = context.Request.Params["FileName"];
            string sheetName = context.Request.Params["sheetName"];
            string rowIndex = context.Request.Params["rowIndex"];
            string colIndex = context.Request.Params["colIndex"];
            string rtn = GetCellStyle(FileName, sheetName, rowIndex, colIndex).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);
        }
        else if (action == "GetRowAndCol")
        {
            //获取行列属性
            string FileName = context.Request.Params["FileName"];
            string sheetName = context.Request.Params["sheetName"];
            string rtn = GetRowAndColProperty(FileName, sheetName).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);
        }
        else if (action == "CheckFormula")
        {
            string rtn = "";
            string MBDM = context.Request.QueryString["MBDM"];
            string GSYY = context.Request.QueryString["GSYY"];
            string GSNN = context.Request.QueryString["GSNN"];
            string FADM2 = context.Request.QueryString["FADM2"];
            string FABS = context.Request.QueryString["FABS"];
            string YY = context.Request.QueryString["YY"];
            string RowCount = context.Request.QueryString["RowCount"];
            string mblb = context.Request.QueryString["MBLB"];
            rtn = JYGS(MBDM, GSYY, GSNN, FADM, FADM2, FABS, YY);
            context.Response.Write(rtn);
        }
        else if (action == "getgs")
        {
            //获取公式
            string rtn = "";
            string GSYY = context.Request.Params["GSYY"];
            string GSMM = context.Request.Params["GSMM"];
            string FABS = context.Request.Params["FABS"];
            string FADM2 = context.Request.Params["FADM2"];
            string FileName = context.Request.Params["FileName"];
            //FileName = "M10161712.XLS";
            string YY = context.Request.Params["YY"];
            rtn = GetGS(FileName, GSYY, GSMM, FADM, FADM2, FABS, YY).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);
        }
        else if (action == "SetCopyFormula")
        {
            //获取公式
            string rtn = "";
            string FileName = context.Request.Params["FileName"];
            rtn = SetCopyFormula(FileName).Replace("\r", "").Replace("\n", "");
            rtn = (rtn == "") ? "{}" : rtn;
            context.Response.Write(rtn);
        }
        else if (action == "GetSelfFormula")
        {
            int count = 0;
            StringBuilder Str = new StringBuilder();
            string sql = "SELECT SJYMC,DYGS FROM REPORT_CSSZ_SJY";
            DataSet DS = BLL.Query(sql);
            Str.Append("{\"grids\":[");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Str.Append("{\"MC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["SJYMC"].ToString() + "\"" + ",");
                Str.Append("\"GS\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["DYGS"].ToString() + "\"" + "},");
                count++;
            }
            if (Str.ToString() != "{\"grids\":[")
            {
                string rs = Str.ToString().Substring(0, Str.ToString().Length - 1);
                Str = new StringBuilder();
                Str.Append(rs);
            }
            Str.Append("],");
            Str.Append("\"count\":\"" + count + "\"");
            Str.Append("}");
            context.Response.Write(Str.ToString());
        }
    }

    #region
    /// <summary>
    /// 获取Excel的所有自定义公式
    /// </summary>
    /// <param name="FileName">文件名称</param>
    /// <param name="GSYY">公式年份</param>
    /// <param name="GSMM">公式月份</param>
    /// <param name="FADM">方案代码</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="YY">年份</param>
    /// <returns></returns>
    public string SetCopyFormula(string FileName)
    {
        int count = 0;
        StringBuilder resJson = new StringBuilder();
        resJson.Append("{\"grids\":[");
        //存取Excel的所有样式
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell;
        string rtn = "", sheetName = "", gsval = "", formula = "", rowIndex = "", colIndex = "";
        //打开模板.Json文件，获取相关信息
        rtn = GetExcelJson("../Xls/", FileName);
        //将Json文件存入字典
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            if (objs != null)
            {
                //获取sheet页数据
                if (objs.ContainsKey("sheets"))
                {
                    //获取Excel文件中的所有Sheet页
                    sheets = (Dictionary<string, object>)objs["sheets"];
                    foreach (var item in sheets)
                    {
                        sheetName = item.Key;
                        sheet = (Dictionary<string, object>)sheets[item.Key];
                        if (sheet.ContainsKey("data"))
                        {
                            //获取sheet页所有数据
                            Data = (Dictionary<string, object>)sheet["data"];
                            if (Data.ContainsKey("dataTable"))
                            {
                                //获取sheet页所有单元格数据
                                dataTable = (Dictionary<string, object>)Data["dataTable"];
                                foreach (var DR in dataTable)
                                {
                                    Row = (Dictionary<string, object>)DR.Value;
                                    rowIndex = DR.Key;
                                    foreach (var Col in Row.Keys)
                                    {
                                        Cell = (Dictionary<string, object>)Row[Col];
                                        colIndex = Col;
                                        //获取单元格的公式值
                                        if (Cell.ContainsKey("formula"))
                                        {
                                            formula = Cell["formula"].ToString();
                                            if (formula != null && formula != "")
                                            {
                                                if (formula.ToString().IndexOf("N_") != -1)
                                                    gsval = formula.ToString().Replace("\"", "\\\"");
                                            }
                                        }

                                        //单元格的公式或者小数点精度不为空时，才返回到客户端
                                        if (gsval != "")
                                        {
                                            resJson.Append("{\"name\":\"" + sheetName + "\",\"row\":\"" + rowIndex + "\",\"col\":\"" + colIndex + "\",\"gs\":\"" + gsval + "\"},");
                                            count++;
                                            gsval = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //如果判断区域计算不为空，则去掉最后的那个多余的逗号
                if (count > 0)
                {
                    string rs = resJson.ToString().Substring(0, resJson.ToString().Length - 1);
                    //resJson.Clear();
                    resJson = new StringBuilder();
                    resJson.Append(rs);
                }
                resJson.Append("],");
                resJson.Append("\"count\":\"" + count + "\"");
                resJson.Append("}");
            }
            else
            {
                resJson.Append("],");
                resJson.Append("\"count\":\"" + count + "\"");
                resJson.Append("}");
            }
        }
        else
        {
            resJson.Append("{}");
        }
        return resJson.ToString();
    }

    #endregion


    #region
    /// <summary>
    /// 获取Excel的所有公式以及YY
    /// </summary>
    /// <param name="FileName">文件名称</param>
    /// <param name="GSYY">公式年份</param>
    /// <param name="GSMM">公式月份</param>
    /// <param name="FADM">方案代码</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="YY">年份</param>
    /// <returns></returns>
    public string GetGS(string FileName, string GSYY, string GSMM, string FADM, string FADM2, string FABS, string YY)
    {
        int count = 0, Len = 0;
        StringBuilder resJson = new StringBuilder();
        resJson.Append("{\"grids\":[");
        //存取Excel的所有样式
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell;
        string rtn = "", sheetName = "", gs = "", gsval = "", formula = "", value = "", rowIndex = "", colIndex = "";
        //打开模板.Json文件，获取相关信息
        rtn = GetExcelJson("../Xls/", FileName);
        //将Json文件存入字典
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            if (objs != null)
            {
                //获取sheet页数据
                if (objs.ContainsKey("sheets"))
                {
                    //获取Excel文件中的所有Sheet页
                    sheets = (Dictionary<string, object>)objs["sheets"];
                    foreach (var item in sheets)
                    {
                        sheetName = item.Key;
                        sheet = (Dictionary<string, object>)sheets[item.Key];
                        if (sheet.ContainsKey("data"))
                        {
                            //获取sheet页所有数据
                            Data = (Dictionary<string, object>)sheet["data"];
                            if (Data.ContainsKey("dataTable"))
                            {
                                //获取sheet页所有单元格数据
                                dataTable = (Dictionary<string, object>)Data["dataTable"];
                                foreach (var DR in dataTable)
                                {
                                    Row = (Dictionary<string, object>)DR.Value;
                                    rowIndex = DR.Key;
                                    foreach (var Col in Row.Keys)
                                    {
                                        Cell = (Dictionary<string, object>)Row[Col];
                                        colIndex = Col;
                                        //获取单元格的公式值
                                        if (Cell.ContainsKey("formula"))
                                        {
                                            formula = Cell["formula"].ToString();
                                            if (formula != null && formula != "")
                                            {
                                                if (formula.ToString().IndexOf("N_") != -1)
                                                {
                                                    gs = formula.ToString().Replace("N_", "");
                                                    if (gs.IndexOf("=") == 0)
                                                    {
                                                        gs = gs.Substring(1, gs.Length - 1);
                                                    }
                                                    gsval = GetFunVal(gs, GSYY, GSMM, FADM, FADM2, FABS, YY).Replace("\"", "\\\"");
                                                }
                                                else
                                                {
                                                    gs = formula.ToString();
                                                    gsval = formula.ToString().Replace("\"", "\\\"");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Cell.ContainsKey("value"))
                                            {

                                                value = Cell["value"].ToString().Trim();
                                                if (value != "")
                                                {
                                                    if (value.Contains("~~"))
                                                    {
                                                        gs = value.ToString();
                                                        gsval = value.ToString();
                                                        Len++;
                                                    }
                                                }
                                            }
                                        }

                                        //单元格的公式或者小数点精度不为空时，才返回到客户端
                                        if (gs != "")
                                        {
                                            resJson.Append("{\"name\":\"" + sheetName + "\",\"row\":\"" + rowIndex + "\",\"col\":\"" + colIndex + "\",\"gs\":\"" + gsval + "\"},");
                                            count++;
                                            gs = "";
                                            gsval = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //如果判断区域计算不为空，则去掉最后的那个多余的逗号
                if (count > 0)
                {
                    string rs = resJson.ToString().Substring(0, resJson.ToString().Length - 1);
                    //resJson.Clear();
                    resJson = new StringBuilder();
                    resJson.Append(rs);
                }
                resJson.Append("],");
                resJson.Append("\"count\":\"" + count + "\",");
                resJson.Append("\"Len\":\"" + Len + "\"");
                resJson.Append("}");
            }
            else
            {
                resJson.Append("],");
                resJson.Append("\"count\":\"" + count + "\",");
                resJson.Append("\"Len\":\"" + Len + "\"");
                resJson.Append("}");
            }
        }
        else
        {
            resJson.Append("{}");
        }
        return resJson.ToString();
    }

    #endregion

    #region 获取行列属性
    /// <summary>
    /// 获取行列属性
    /// </summary>
    /// <param name="FileName">文件名</param>
    /// <param name="sheetName">sheet名称</param>
    /// <returns></returns>
    public string GetRowAndColProperty(string FileName, string sheetName)
    {
        int count = 0;
        //存取Excel的所有样式
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell;
        string rtn = "", value = "", rowIndex = "", colIndex = "", resJson = "{\"grids\":[";
        //打开模板.Json文件，获取相关信息
        rtn = GetExcelJson("../Xls/", FileName);
        //将Json文件存入字典
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            if (objs != null)
            {
                //获取sheet页数据
                if (objs.ContainsKey("sheets"))
                {
                    //获取Excel文件中的所有Sheet页
                    sheets = (Dictionary<string, object>)objs["sheets"];
                    if (sheets.ContainsKey(sheetName))
                    {
                        sheet = (Dictionary<string, object>)sheets[sheetName];
                        if (sheet.ContainsKey("data"))
                        {
                            //获取sheet页所有数据
                            Data = (Dictionary<string, object>)sheet["data"];
                            if (Data.ContainsKey("dataTable"))
                            {
                                //获取sheet页所有单元格数据
                                dataTable = (Dictionary<string, object>)Data["dataTable"];
                                foreach (var DR in dataTable)
                                {
                                    Row = (Dictionary<string, object>)DR.Value;
                                    rowIndex = DR.Key;

                                    foreach (var Col in Row.Keys)
                                    {
                                        Cell = (Dictionary<string, object>)Row[Col];
                                        colIndex = Col;
                                        if (Cell.ContainsKey("value"))
                                        {
                                            //取第一行的所有列上的属性值 即：列属性
                                            if (DR.Key == "0")
                                            {
                                                value = Cell["value"].ToString().Trim();
                                                //.json文件，必须排除值是 "" 这种的单元格，否则校验数据时，提示不正确
                                                if (value != "")
                                                {
                                                    resJson += "{\"row\":\"" + rowIndex + "\",\"col\":\"" + colIndex + "\",\"gs\":\"" + value.Replace(":", "\\:").Replace(",", "\\,") + "\"},";
                                                    count++;
                                                }
                                            }
                                            else
                                            {
                                                //取每一行第一列的单元格值 即：行属性
                                                if (Col == "0")
                                                {
                                                    value = Cell["value"].ToString().Trim();
                                                    //.json文件，必须排除值是 "" 这种的单元格，否则校验数据时，提示不正确
                                                    if (value != "")
                                                    {
                                                        resJson += "{\"row\":\"" + rowIndex + "\",\"col\":\"" + colIndex + "\",\"gs\":\"" + value.Replace(":", "\\:").Replace(",", "\\,") + "\"},";
                                                        count++;
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //如果判断区域计算不为空，则去掉最后的那个多余的逗号
                if (count > 0)
                {
                    resJson = resJson.Substring(0, resJson.Length - 1);
                }
                resJson += "],";
                resJson += "\"count\":\"" + count + "\"";
                resJson += "}";
            }
            else
            {
                resJson += "],";
                resJson += "\"count\":\"" + count + "\"";
                resJson += "}";
            }
        }
        else
        {
            resJson = "{}";
        }
        return resJson;
    }
    #endregion

    #region
    /// <summary>
    /// 全区域计算
    /// </summary>
    /// <param name="FileName">文件名称</param>
    /// <param name="GSYY">公式年份</param>
    /// <param name="GSMM">公式月份</param>
    /// <param name="FADM">方案代码</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="YY">年份</param>
    /// <returns></returns>
    public string QQYJS(string FileName, string GSYY, string GSMM, string FADM, string FADM2, string FABS, string YY)
    {
        int count = 0;
        //存取Excel的所有样式
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell, CellStyle;
        string rtn = "", sheetName = "", gs = "", gsval = "", formatter = "", formula = "", style = "", value = "", rowIndex = "", colIndex = "", resJson = "{\"grids\":[";
        //打开模板.Json文件，获取相关信息
        rtn = GetExcelJson("../Xls/", FileName);
        //将Json文件存入字典
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            if (objs != null)
            {
                //获取Excel所有的样式
                GetExcelAllStyle(objs, ref ExcelStyles);
                //获取sheet页数据
                if (objs.ContainsKey("sheets"))
                {
                    //获取Excel文件中的所有Sheet页

                    sheets = (Dictionary<string, object>)objs["sheets"];
                    foreach (var item in sheets)
                    {
                        sheetName = item.Key;
                        sheet = (Dictionary<string, object>)sheets[item.Key];
                        if (sheet.ContainsKey("data"))
                        {
                            //获取sheet页所有数据
                            Data = (Dictionary<string, object>)sheet["data"];
                            if (Data.ContainsKey("dataTable"))
                            {
                                //获取sheet页所有单元格数据
                                dataTable = (Dictionary<string, object>)Data["dataTable"];
                                foreach (var DR in dataTable)
                                {
                                    Row = (Dictionary<string, object>)DR.Value;
                                    rowIndex = DR.Key;
                                    foreach (var Col in Row.Keys)
                                    {
                                        Cell = (Dictionary<string, object>)Row[Col];
                                        colIndex = Col;
                                        //获取单元格的公式值
                                        if (Cell.ContainsKey("formula"))
                                        {
                                            formula = Cell["formula"].ToString();
                                            if (formula != null && formula != "")
                                            {
                                                if (formula.ToString().IndexOf("N_") != -1)
                                                {
                                                    gs = formula.ToString().Replace("N_", "");
                                                    if (gs.IndexOf("=") == 0)
                                                    {
                                                        gs = gs.Substring(1, gs.Length - 1);
                                                    }
                                                    gsval = GetFunVal(gs, GSYY, GSMM, FADM, FADM2, FABS, YY);
                                                }
                                                else
                                                {
                                                    gs = formula.ToString();
                                                    gsval = formula.ToString();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Cell.ContainsKey("value"))
                                            {

                                                value = Cell["value"].ToString();
                                                if (value.Contains("~~"))
                                                {
                                                    gs = value.ToString();
                                                    gsval = value.ToString();
                                                }
                                            }
                                        }
                                        //获取单元格精度
                                        if (Cell.ContainsKey("style"))
                                        {
                                            if (Cell["style"].GetType().ToString() == "System.String")
                                            {
                                                //获取单元格样式对象
                                                style = Cell["style"].ToString();
                                                //判断样式集合中是否包含该样式
                                                if (ExcelStyles.ContainsKey(style))
                                                {
                                                    CellStyle = (Dictionary<string, object>)ExcelStyles[style];
                                                    if (CellStyle.ContainsKey("formatter"))
                                                    {
                                                        formatter = CellStyle["formatter"].ToString();
                                                    }
                                                }
                                            }
                                        }

                                        //单元格的公式或者小数点精度不为空时，才返回到客户端
                                        if (formatter != "" || gs != "")
                                        {
                                            resJson += "{\"name\":\"" + sheetName + "\",\"row\":\"" + rowIndex + "\",\"col\":\"" + colIndex + "\",\"gs\":\"" + gsval + "\",\"formatter\":\"" + formatter + "\"},";
                                            count++;
                                            gs = "";
                                            gsval = "";
                                            formatter = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //如果判断区域计算不为空，则去掉最后的那个多余的逗号
                if (count > 0)
                {
                    resJson = resJson.Substring(0, resJson.Length - 1);
                }
                resJson += "],";
                resJson += "\"count\":\"" + count + "\"";
                resJson += "}";
            }
            else
            {
                resJson += "],";
                resJson += "\"count\":\"" + count + "\"";
                resJson += "}";
            }
        }
        else
        {
            resJson = "{}";
        }
        return resJson;
    }
    #endregion

    #region 区域计算相关方法
    /// <summary>
    /// 区域计算
    /// </summary>
    /// <param name="FileName">文件名称</param>
    /// <param name="YY">年份</param>
    /// <param name="NN">月份</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="serveryy"></param>
    /// <param name="Add">Excel所有选中的区域地址</param>
    /// <returns></returns>
    public string QYJS(string FileName, string YY, string NN, string FADM1, string FADM2, string FABS, string serveryy, string Add)
    {
        string[] CellArr;
        int row = 0, col = 0, count = 0;
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell, CellStyle;
        string rtn = "", sheetName = "", gs = "", gsval = "", formatter = "", formula = "", style = "", value = "", resJson = "{\"grids\":[";
        //存取Excel的所有样式
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        //打开模板.Json文件，获取相关信息
        //rtn = GetExcelJson("../Xls/", "M10160037.json");
        rtn = GetExcelJson("../Xls/", FileName);
        //将Json文件存入字典
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            //获取Excel所有的样式
            GetExcelAllStyle(objs, ref ExcelStyles);
            //获取sheet页数据
            if (objs.ContainsKey("sheets"))
            {
                //获取Excel文件中的所有Sheet页
                sheets = (Dictionary<string, object>)objs["sheets"];
                string[] Arr = Add.Split('|');
                for (var k = 0; k < Arr.Length; k++)
                {
                    CellArr = Arr[k].Split(',');
                    //获取需要解析的sheet名称
                    sheetName = CellArr[0];
                    if (sheets.ContainsKey(sheetName))
                    {
                        //根据名称获取sheet页
                        sheet = (Dictionary<string, object>)sheets[sheetName];
                        if (sheet.ContainsKey("data"))
                        {
                            //获取sheet页所有数据
                            Data = (Dictionary<string, object>)sheet["data"];
                            if (Data.ContainsKey("dataTable"))
                            {
                                //获取sheet页所有单元格数据
                                dataTable = (Dictionary<string, object>)Data["dataTable"];
                                //区域计算的行数
                                row = int.Parse(CellArr[1]) + int.Parse(CellArr[2]);
                                //区域计算的列数
                                col = int.Parse(CellArr[3]) + int.Parse(CellArr[4]);
                                for (var i = int.Parse(CellArr[1]); i < row; i++)
                                {
                                    if (dataTable.ContainsKey(i.ToString()))
                                    {
                                        //获取sheet页的行数据
                                        Row = (Dictionary<string, object>)dataTable[i.ToString()];
                                        for (var j = int.Parse(CellArr[3]); j < col; j++)
                                        {
                                            if (Row.ContainsKey(j.ToString()))
                                            {
                                                Cell = (Dictionary<string, object>)Row[j.ToString()];
                                                //获取单元格的公式值
                                                if (Cell.ContainsKey("formula"))
                                                {
                                                    formula = Cell["formula"].ToString();
                                                    if (formula != null && formula != "")
                                                    {
                                                        if (formula.ToString().IndexOf("N_") != -1)
                                                        {
                                                            gs = formula.ToString().Replace("N_", "");
                                                            if (gs.IndexOf("=") == 0)
                                                            {
                                                                gs = gs.Substring(1, gs.Length - 1);
                                                            }
                                                            gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                                        }
                                                        else
                                                        {
                                                            gs = formula.ToString();
                                                            gsval = formula.ToString();
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (Cell.ContainsKey("value"))
                                                    {
                                                        value = Cell["value"].ToString().Trim();
                                                        if (value != "")
                                                        {
                                                            if (value.Contains("~~"))
                                                            {
                                                                gs = value.ToString();
                                                                gsval = value.ToString();
                                                            }
                                                        }
                                                    }
                                                }
                                                //获取单元格精度
                                                if (Cell.ContainsKey("style"))
                                                {
                                                    if (Cell["style"].GetType().ToString() == "System.String")
                                                    {
                                                        //获取单元格样式对象
                                                        style = Cell["style"].ToString();
                                                        //判断样式集合中是否包含该样式
                                                        if (ExcelStyles.ContainsKey(style))
                                                        {
                                                            CellStyle = (Dictionary<string, object>)ExcelStyles[style];
                                                            if (CellStyle.ContainsKey("formatter"))
                                                            {
                                                                formatter = CellStyle["formatter"].ToString();
                                                            }
                                                        }
                                                    }
                                                }

                                                //单元格的公式或者小数点精度不为空时，才返回到客户端
                                                if (formatter != "" || gs != "")
                                                {
                                                    resJson += "{\"name\":\"" + sheetName + "\",\"row\":\"" + i.ToString() + "\",\"col\":\"" + j.ToString() + "\",\"gs\":\"" + gsval + "\",\"formatter\":\"" + formatter + "\"},";
                                                    count++;
                                                    gs = "";
                                                    gsval = "";
                                                    formatter = "";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //如果判断区域计算不为空，则去掉最后的那个多余的逗号
            if (count > 0)
            {
                resJson = resJson.Substring(0, resJson.Length - 1);
            }
            resJson += "],";
            resJson += "\"count\":\"" + count + "\"";
            resJson += "}";
        }
        else
        {
            //表示读取模板数据失败
            resJson = "{}";
        }
        return resJson;
    }

    /// <summary>
    ///  获取公式值
    /// </summary>
    /// <param name="_gs">公式</param>
    /// <param name="_gsyy">公式年份</param>
    /// <param name="_gsnn">公式月份</param>
    /// <param name="_fadm">方案代码</param>
    /// <param name="_fadm2">方案代码2</param>
    /// <param name="_fabs">方案标识</param>
    /// <param name="_yy">年份</param>
    /// <returns></returns>
    public string GetFunVal(string _gs, string _gsyy, string _gsnn, string _fadm, string _fadm2, string _fabs, string _yy)
    {
        string res = "";
        res = GetDataList.qz(_gs, _gsyy, _gsnn, _fadm, _fadm2, _fabs, _yy).ToString();
        if (res.IndexOf("@") >= 0)
        {
            res = res.Replace("@", "");
        }
        else
        {
            res = "=" + res;
        }
        return res;
    }

    /// <summary>
    /// 获取Excel中所有的样式
    /// </summary>
    /// <param name="objs">Excel相关的字典对象</param>
    /// <param name="ExcelStyles">存储Excel样式的集合</param>
    public void GetExcelAllStyle(Dictionary<string, object> objs, ref Dictionary<string, object> ExcelStyles)
    {
        if (objs.ContainsKey("namedStyles"))
        {
            // var namedStyles = objs["namedStyles"];
            var namedStyles = new ArrayList();
            namedStyles = (ArrayList)objs["namedStyles"];
            Dictionary<string, object> obj = new Dictionary<string, object>();
            for (int i = 0; i < namedStyles.Count; i++)
            {
                obj = (Dictionary<string, object>)namedStyles[i];
                ExcelStyles.Add(obj["name"].ToString(), namedStyles[i]);
            }
        }
    }

    /// <summary>
    /// 获取模板页的Json字符串
    /// </summary>
    /// <param name="FileName">模板文件名</param>
    /// <returns></returns>
    public string GetExcelJson(string path, string FileName)
    {
        string rtn = "";
        //打开模板.Json文件，获取相关信息
        //Add by anweiwen 目前服务器excel没有json格式文件
        FileName = FileName.Substring(0, FileName.LastIndexOf(".")) + ".json";
        if (!File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + FileName))
        {
            FileName = FileName.Substring(0, FileName.LastIndexOf(".")) + ".xlsx";
        }
        using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(path) + FileName, FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(fs))
            {
                string strLine = sr.ReadLine();
                while (strLine != null)
                {
                    rtn += strLine;
                    strLine = sr.ReadLine();

                }
                sr.Close();
            }
        }
        return rtn;
    }

    /// <summary>
    /// 获取模板单元格样式
    /// </summary>
    /// <param name="FileName">文件名</param>
    /// <param name="sheetName">sheet页名称</param>
    /// <param name="row">行索引</param>
    /// <param name="col">列索引</param>
    /// <returns>返回单元格样式</returns>
    public string GetCellStyle(string FileName, string sheetName, string row, string col)
    {
        string style = "", StyleJson = "";
        string rtn = GetExcelJson("../Xls/", FileName);
        Dictionary<string, object> ExcelStyles = new Dictionary<string, object>();
        Dictionary<string, object> sheets, sheet, Data, dataTable, Row, Cell, CellStyle;
        if (rtn != "")
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Serializer.MaxJsonLength = rtn.Length;
            var objs = Serializer.Deserialize<Dictionary<string, object>>(rtn);
            //获取Excel所有的样式
            GetExcelAllStyle(objs, ref ExcelStyles);
            sheets = (Dictionary<string, object>)objs["sheets"];
            if (sheets.ContainsKey(sheetName))
            {
                //根据名称获取sheet页
                sheet = (Dictionary<string, object>)sheets[sheetName];
                if (sheet.ContainsKey("data"))
                {
                    //获取sheet页所有数据
                    Data = (Dictionary<string, object>)sheet["data"];
                    if (Data.ContainsKey("dataTable"))
                    {
                        //获取sheet页所有单元格数据
                        dataTable = (Dictionary<string, object>)Data["dataTable"];
                        if (dataTable.ContainsKey(row))
                        {
                            //获取sheet页的行数据
                            Row = (Dictionary<string, object>)dataTable[row];
                            if (Row.ContainsKey(col))
                            {
                                Cell = (Dictionary<string, object>)Row[col];
                                if (Cell.ContainsKey("style"))
                                {
                                    //获取单元格样式对象
                                    style = Cell["style"].ToString();
                                    //判断样式集合中是否包含该样式
                                    if (ExcelStyles.ContainsKey(style))
                                    {
                                        CellStyle = (Dictionary<string, object>)ExcelStyles[style];
                                        StyleJson = Serializer.Serialize(CellStyle);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return StyleJson;
    }
    #endregion

    #region 公式校验相关代码

    /// <summary>
    /// 公式校验 MBDM=4，YSBS=4
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="YY">公式年份</param>
    /// <param name="NN">公式月份</param>
    /// <param name="FADM1">方案代码</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="yy2">系统年份</param>
    /// <returns></returns>
    public string JYGS(string MBDM, string YY, string NN, string FADM1, string FADM2, string FABS, string yy2)
    {
        int Count = 0;
        string YSBS = "", mbmc = "", mblx = "";
        string resJson = "", JYGS = "\"JYGS\":[", JYMBLX = "\"JYMBLX\":[", JYYSBS = "\"JYYSBS\":[";
        try
        {
            string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID,T.LX  FROM dbo.TB_MBJYGS T where MBDM='" + MBDM + "' AND patindex('%N_JYGS%',GS) = 0";
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            DataSet set = BLL.Query(sql);
            sql = "SELECT A.MBDM,A.MBLX,A.MBMC,A.ZYDM,A.MBWJ,A.MBZQ,A.MBBM,A.MBLX FROM TB_YSBBMB A WHERE MBDM='" + MBDM + "'";
            DataSet mbmcset = BLL.Query(sql);
            sql = " select YSBS from  TB_JHFA WHERE JHFADM='" + FADM1 + "' ";
            DataSet setBs = BLL.Query(sql);
            sql = "SELECT A.XMDM,B.XMMC,MBDM,GS,VALUE,WCFW FROM TB_ZBXD A,V_XMZD B WHERE A.XMDM=B.XMDM  AND A.YY='" + yy2 + "' AND MBDM='" + MBDM + "'";
            DataSet setJY = BLL.Query(sql);
            if (setBs.Tables[0].Rows.Count > 0)
            {
                YSBS = setBs.Tables[0].Rows[0]["YSBS"].ToString();
            }

            if (mbmcset.Tables[0].Rows.Count > 0)
            {
                mbmc = mbmcset.Tables[0].Rows[0]["MBWJ"].ToString();
                mblx = mbmcset.Tables[0].Rows[0]["MBLX"].ToString();
            }
            if (set.Tables[0].Rows.Count > 0)
            {
                string gsjx = "", rtn = "";
                string gs = "";
                string INFOMATION = "";
                string sheet = "";
                string LX = "";
                for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                {
                    gsjx = set.Tables[0].Rows[i]["GSJX"].ToString();
                    gs = set.Tables[0].Rows[i]["GS"].ToString();
                    INFOMATION = set.Tables[0].Rows[i]["INFOMATION"].ToString();
                    sheet = set.Tables[0].Rows[i]["SHEETNAME"].ToString();
                    LX = set.Tables[0].Rows[i]["LX"].ToString();
                    gs = qz(gs, YY, NN, FADM1, FADM2, FABS, yy2).ToString();

                    if (i == set.Tables[0].Rows.Count - 1)
                    {
                        rtn = "{\"sheetName\":\"" + sheet + "\",\"GS\":\"" + gs + "\",\"GSJX\":\"" + gsjx.Replace("\"", "\\\"") + "\",\"LX\":\"" + LX + "\",\"INFOMATION\":\"" + INFOMATION + "\"}";
                    }
                    else
                    {
                        rtn = "{\"sheetName\":\"" + sheet + "\",\"GS\":\"" + gs + "\",\"GSJX\":\"" + gsjx.Replace("\"", "\\\"") + "\",\"LX\":\"" + LX + "\",\"INFOMATION\":\"" + INFOMATION + "\"},";
                    }
                    Count++;
                    JYGS = JYGS + rtn;

                }

                JYGS += "]";

                JYGS += ",\"JYGSCount\":\"" + Count + "\"";

                Count = 0;

                if (mblx == "4" && YSBS == "4" && setJY.Tables[0].Rows.Count > 0)
                {
                    string value = "";
                    string GS = "";
                    string WCFW = "";
                    for (int i = 0; i < setJY.Tables[0].Rows.Count; i++)
                    {
                        value = setJY.Tables[0].Rows[i]["VALUE"].ToString();
                        if (value == "")
                            value = "0";
                        GS = setJY.Tables[0].Rows[i]["GS"].ToString();
                        if (GS == "")
                            GS = "0";
                        WCFW = setJY.Tables[0].Rows[i]["WCFW"].ToString();
                        if (WCFW == "")
                            WCFW = "0";
                        GS = qz(GS, YY, NN, FADM1, FADM2, FABS, yy2).ToString();

                        if (i == setJY.Tables[0].Rows.Count - 1)
                        {
                            rtn = "{\"VALUE\":\"" + value + "\",\"GS\":\"" + GS + "\",\"WCFW\":\"" + WCFW + "\",\"LX\":\"" + LX + "\",\"XMMC\":\"" + setJY.Tables[0].Rows[i]["XMMC"].ToString() + "\"}";
                        }
                        else
                        {
                            rtn = "{\"VALUE\":\"" + value + "\",\"GS\":\"" + GS + "\",\"WCFW\":\"" + WCFW + "\",\"LX\":\"" + LX + "\",\"XMMC\":\"" + setJY.Tables[0].Rows[i]["XMMC"].ToString() + "\"},";
                        }

                        Count++;
                        JYMBLX = JYMBLX + rtn;
                    }
                }
                JYMBLX += "]";

                JYMBLX += ",\"JYMBLXCount\":\"" + Count + "\"";

                Count = 0;

                if (YSBS == "4")
                {
                    string sqlysbs4 = "SELECT  A.XMDM,B.XMMC,JSDX,VALUE,isnull(WCFW,0) WCFW FROM TB_CWZBPF A,V_XMZD B WHERE JHFADM='" + FADM1 + "'  AND A.XMDM=B.XMDM   AND MBDM='" + MBDM + "' AND  ISNULL(VALUE,0)<>0";
                    DataSet ds4 = BLL.Query(sqlysbs4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds4.Tables[0].Rows.Count; i++)
                        {
                            string SQL2 = "SELECT SUM(VALUE) value";
                            SQL2 += " FROM TB_BBFYSJ A, TB_MBYJDZZY B ";
                            SQL2 += " WHERE A.JHFADM='" + FADM1 + "' ";
                            SQL2 += " AND A.MBDM='" + MBDM + "' ";
                            SQL2 += " And A.XMDM='" + ds4.Tables[0].Rows[i]["XMDM"].ToString() + "' ";
                            SQL2 += " AND A.JSDX='" + ds4.Tables[0].Rows[i]["JSDX"].ToString() + "' ";
                            SQL2 += " AND A.SJDX='FA1' ";
                            SQL2 += " AND A.ZYDM=B.ZYDM AND A.JHFADM=B.JHFADM ";
                            SQL2 += " AND A.XMDM=B.XMDM AND A.MBDM=B.MBDM ";
                            DataSet DS24 = BLL.Query(SQL2);
                            if (DS24.Tables[0].Rows.Count > 0)
                            {
                                double sql2value = Convert.ToDouble(DS24.Tables[0].Rows[0]["value"].ToString());
                                double sql1value = Convert.ToDouble(ds4.Tables[0].Rows[i]["value"].ToString());
                                double wcfw = Convert.ToDouble(ds4.Tables[0].Rows[i]["WCFW"].ToString());
                                if (i == DS24.Tables[0].Rows.Count - 1)
                                {
                                    rtn = "{\"sql2value\":\"" + sql2value + "\",\"sql1value\":\"" + sql1value + "\",\"WCFW\":\"" + wcfw + "\",\"LX\":\"" + LX + "\"}";
                                }
                                else
                                {
                                    rtn = "{\"sql2value\":\"" + sql2value + "\",\"sql1value\":\"" + sql1value + "\",\"WCFW\":\"" + wcfw + "\",\"LX\":\"" + LX + "\"},";
                                }

                                Count++;
                                JYYSBS = JYYSBS + rtn;

                            }
                        }
                    }
                }
                JYYSBS += "]";

                JYYSBS += ",\"JYYSBSCount\":\"" + Count + "\"";
            }
        }
        catch (Exception ee)
        {
            throw ee;
        }

        if (!(JYGS == "\"JYGS\":[" && JYMBLX == "\"JYMBLX\":[" && JYYSBS == "\"JYYSBS\":["))
        {
            resJson = "{" + JYGS + "," + JYMBLX + "," + JYYSBS + ",\"YSBS\":\"" + YSBS + "\",\"MBLX\":\"" + mblx + "\"}";
        }
        return resJson;

    }

    /// <summary>
    /// 公式取值
    /// </summary>
    /// <param name="gsss">公式</param>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <returns></returns>
    public object qz(string gsss, string YY, string NN, string FADM1, string FADM2, string FABS, string yy2)
    {
        object value;
        gsss = gsss.Replace("S_NN", Convert.ToInt32(NN).ToString());
        string sql = " select * from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = "0";
        string gs = gsss;
        gsss = gsss.Trim();
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gsss.Contains(gygss))
            {
                do
                {
                    int index_i = gsss.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString());
                    int end2 = gsss.IndexOf(")", index_i);
                    string gss = gsss.Substring(index_i, end2 - index_i + 1);
                    fhz = NewMethod(gss, YY, NN, FADM1, FADM2, FABS, yy2);
                    if (fhz.IndexOf("错误公式") > -1)
                    {
                        fhz = "0";
                    }
                    gsss = gsss.Replace(gss, fhz.ToString());
                }
                while (gsss.Contains(gygss));
            }
        }
        value = gsss;
        return value;
    }

    /// <summary>
    /// 求值
    /// </summary>
    /// <param name="gs">公式</param>
    /// <param name="hszqdm">核算周期</param>
    /// <param name="time">时间</param>
    /// <param name="type">Yy~2015~Ysyf~5~Jhfadm~10050577~Mb~test.</param>
    /// <param name="thzd">要和界面的条件替换的值，值之间以|分开</param>
    /// <returns></returns>
    public static string NewMethod(string gs, string YY, string NN, string FADM1, string FADM2, string FABS, string YY2)
    {
        string fhzhi = "0";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string sqlNN = "select * from TB_JHFA WHERE JHFADM='" + FADM1 + "'";
            DataSet SETNN = bll.Query(sqlNN);
            string FANN = "";
            string FAYY = "";
            if (SETNN.Tables[0].Rows.Count > 0)
            {
                FANN = SETNN.Tables[0].Rows[0]["NN"].ToString();
                FAYY = SETNN.Tables[0].Rows[0]["YY"].ToString();
            }
            string login_YY = YY2;
            //string login_YY = HttpContext.Current.Request.Cookies["RMYH_LOGIN"].Values.Get("YY").ToString();

            int i = gs.IndexOf("(");
            string zhi = string.Empty;
            string _gs = string.Empty;
            string mbdm = "";
            string sqljhfa = "";
            string sqlzq = "";
            string sfzf = "";
            DataSet setzq2 = null;
            if (i > -1)
            {

                _gs = gs.Substring(0, i);
                string sql = " select * from REPORT_CSSZ_SJY  where Upper(DYGS)='" + _gs + "'";

                DataSet set = bll.Query(sql);
                string bid = set.Tables[0].Rows[0]["BID"].ToString();
                _gs = gs.Substring(i + 1);
                int j = _gs.LastIndexOf(",");
                string zq = _gs.Substring(j);
                //周期
                zq = zq.Replace(")", "").Replace('"', ' ').Replace(",", "").Trim();
                _gs = _gs.Substring(0, j);
                //_gs = _gs.Replace('"', ' ').Trim();
                j = _gs.LastIndexOf(",");
                string zifu = _gs.Substring(j + 1);
                //查询字段
                zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
                j = _gs.LastIndexOf(",");
                _gs = _gs.Substring(0, j).Replace('"', ' ');
                string[] tjzhi = _gs.Split(',');
                string sjyid = set.Tables[0].Rows[0]["sjyid"].ToString();
                string sql3 = string.Empty;
                //拼接上期方案差不错数据时的sql
                string sqlJHFADM = string.Empty;
                //求条件值
                string sql2 = "select * from REPORT_CSSZ_SJYDYXTJ a where SJYID='" + sjyid + "' order by XH";
                DataSet set2 = bll.Query(sql2);
                if (set2.Tables[0].Rows.Count == 0)
                    return "错误公式-";
                if (sjyid == "2")
                {
                    //如果是单耗，单价，单位成本不能SUM
                    if (zifu == "DJ" || zifu == "DH" || zifu == "DWCB" || zifu == "CWXGDJ" || zifu == "CWXGDH")
                    {
                        //sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  and STATE='1' AND JSDX='" + zifu + "' ";
                        sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                        sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                    }
                    else
                    {
                        string SQLSJLX = "SELECT SJLX FROM TB_JSDX WHERE ZFBM='" + zifu + "' ";
                        DataSet SET = bll.Query(SQLSJLX);
                        if (SET.Tables[0].Rows.Count > 0)
                        {
                            string jsdx = SET.Tables[0].Rows[0][0].ToString();
                            if (jsdx == "6")
                            {
                                sql3 = "select ZFVALUE from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                                sfzf = "@";
                                sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                            }
                            else
                            {
                                sql3 = "select ISNULL(sum(VALUE),0) from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                                sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                            }
                        }

                    }
                }
                else
                {
                    sql3 = "select ISNULL(sum(" + zifu + "),0) from " + bid + " where 1=1   ";
                    sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                }
                for (i = 0; i < tjzhi.Length; i++)
                {
                    sql3 += " ";
                    if (tjzhi[i].ToString().Trim() == "" && set2.Tables[0].Rows[i]["tjdm"].ToString() != "TRLDM")
                    {
                        sql3 += " ";
                    }
                    else
                    {
                        if (sjyid == "2")
                        {

                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "XMDM")
                            {
                                string sqlfl = " SELECT XMLX FROM V_XMZD WHERE XMDM in(" + zhi + ") ";
                                DataSet dsd = bll.Query(sqlfl);
                                if (dsd.Tables[0].Rows.Count == 0)
                                {
                                    return "错误公式-";
                                }
                                else
                                {
                                    if (dsd.Tables[0].Rows[0]["XMLX"].ToString() == "13")
                                    {
                                        sql3 += " AND XMDM IN(SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM IN(" + zhi + "))";
                                        sqlJHFADM += " AND XMDM IN(SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM IN(" + zhi + "))";
                                    }
                                    else
                                    {
                                        sql3 += " AND XMDM IN (" + zhi + ") ";
                                        sqlJHFADM += " AND XMDM IN (" + zhi + ") ";
                                    }
                                }
                            }
                            else if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "MBDM")
                            {
                                int z = 0;
                                string pdz = "";
                                if (zhi.IndexOf(',') - 2 < 0 || zhi.IndexOf(',') - 2 == 0)
                                {
                                    z = 0;
                                    pdz = zhi.Replace("'", "");
                                }
                                else
                                {
                                    z = zhi.IndexOf(',') - 2;
                                    pdz = zhi.Replace("'", "").Substring(0, z);
                                }
                                if (Convert.ToInt32(pdz) < 10)
                                {
                                    sql3 += " AND MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                                    sqlJHFADM += " AND MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                                }
                                else
                                {
                                    sql3 += " AND MBDM IN (" + zhi + ") ";
                                    sqlJHFADM += " AND MBDM IN (" + zhi + ") ";
                                }
                                mbdm = zhi;
                            }
                            else
                            {
                                zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                                sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                                sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                            }
                        }
                        else if (sjyid == "1")
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "%'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "like" + "" + zhi + "";
                            sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "like" + "" + zhi + "";
                        }
                        else
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                            sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                        }
                    }

                }
                string fa = FADM1;
                string sj = zq;
                if (sjyid != "1" && sjyid != "2")
                {
                    string SQL = "";
                    //计划标准成本
                    if (zq == "JHBZ")
                    {
                        SQL = "SELECT JHFADM  FROM TB_JHFA WHERE YSBS='0' AND YY='" + FAYY + "' and NN='" + FANN + "' AND FABS='" + FABS + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "SJBZ")
                    {
                        SQL = "SELECT JHFADM FROM TB_JHFA WHERE YSBS='1' AND FABS='" + FABS + "' AND YY='" + FAYY + "' AND NN='" + FANN + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "PFYS")// 判断为批复预算时  modify by liguosheng
                    {
                        SQL = "SELECT CASE when JHFADM!=null then JHFADM else '' end JHFADM FROM TB_JHFA WHERE YSBS='4' AND FABS='3' AND YY='" + FAYY + "' AND DELBZ='1'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "DQFA")
                    {
                        sql3 += " AND JHFADM='" + FADM1 + "'";
                    }
                    else if (zq == "DQFA2")
                    {
                        sql3 += " AND JHFADM='" + FADM2 + "'";
                    }

                    //DataSet SetZ = bll.Query(sql3);
                    //if (SetZ.Tables[0].Rows.Count > 0)
                    //{
                    //    fhzhi = double.Parse(SetZ.Tables[0].Rows[0][0].ToString());
                    //}
                    //else
                    //{
                    //    fhzhi = 0;
                    //}
                }
                else if (sjyid == "1")
                {
                    //本年累计
                    if (zq == "BNLJ")
                    {
                        sql3 += " and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //本期发生
                    else if (zq == "BYLJ")
                    {
                        sql3 += " And YYNN=" + YY + NN + " ";
                    }
                    //上期发生
                    else if (zq == "SYLJ")
                    {
                        //And YYNN=当前年+主界面传的预算实际月份
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(YY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(YY);
                            int ys = int.Parse(NN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }

                        sql3 += "and YYNN=" + yys + months + "";
                    }
                    //上年同期
                    else if (zq == "SNTQ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN='" + SNYY.ToString() + NN + "'";
                    }
                    //上年累计
                    else if (zq == "SNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY.ToString() + "01 and YYNN<=" + SNYY.ToString() + NN + " ";
                    }
                    //本期之前累计
                    else if (zq == "BQZQLJ")
                    {
                        sql3 += "and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //上年全年累计
                    else if (zq == "SNQNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY + "01 and YYNN<=" + SNYY + "12 ";
                    }
                    else if (zq == "BNQC")
                    {
                        sql3 += "and YYNN=" + YY + "01 ";
                    }
                    else if (zq == "01")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "02")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "03")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "04")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "05")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "06")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "07")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "08")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "09")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "10")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "11")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "12")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }

                }
                else if (sjyid == "2")
                {
                    //本年分解预算
                    if (zq == "BNFJYS")
                    {
                        string sqlBNFJYS = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='5' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                        DataSet SETBNFJYS = bll.Query(sqlBNFJYS);
                        if (SETBNFJYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNFJYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "XBNYJ")// –本年批复预算
                    {
                        string sqlBNPFYS = " SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                        DataSet SETBNPFYS = bll.Query(sqlBNPFYS);
                        if (SETBNPFYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNPFYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    //预算申请
                    else if (zq == "YSSQ")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='3' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }

                    }
                    //批复预算
                    else if (zq == "YSPF")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "FA2")
                    {
                        fa = FADM2;
                        sj = "FA1";
                    }
                    else if (zq == "SQFA")
                    {
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(FAYY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(FAYY);
                            int ys = int.Parse(FANN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }
                        string sqlSQ = "SELECT DELBZ,JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' and FABS='1' AND YSBS='3' and DELBZ='1'";
                        DataSet dsSQ = bll.Query(sqlSQ);
                        if (dsSQ.Tables[0].Rows.Count > 0)
                        {
                            fa = dsSQ.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }
                    }

                    if (zq == "SQFA")
                    {
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(FAYY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(FAYY);
                            int ys = int.Parse(FANN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }
                        DataSet Se = bll.Query(sql3 + "and JHFADM='" + fa + "' and SJDX='" + sj + "' ");
                        if (Se.Tables[0].Rows.Count > 0)
                        {
                            fhzhi = Se.Tables[0].Rows[0][0].ToString();
                            if (fhzhi == "" || fhzhi == "0")
                            {
                                sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                                Se = bll.Query(sqlJHFADM);
                                if (Se.Tables[0].Rows.Count > 0)
                                {
                                    fa = Se.Tables[0].Rows[0][0].ToString();
                                    sj = "FA1";
                                }
                            }

                        }
                        else
                        {
                            sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                            Se = bll.Query(sqlJHFADM);
                            if (Se.Tables[0].Rows.Count > 0)
                            {
                                fa = Se.Tables[0].Rows[0][0].ToString();
                                sj = "FA1";
                            }
                        }
                    }

                    sql3 += "and JHFADM='" + fa + "' and SJDX='" + sj + "' ";
                }
                DataSet SetZ = bll.Query(sql3);
                if (SetZ.Tables[0].Rows.Count > 0)
                {
                    fhzhi = SetZ.Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    fhzhi = "0";
                }
                if (sfzf != "")
                {
                    fhzhi = fhzhi + "@";
                }

            }
        }
        catch (Exception EE)
        {
            throw EE;
        }

        return fhzhi;
    }
    #endregion

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}