﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Spread/WebExcel.master" AutoEventWireup="true"
    CodeFile="EXCELMB.aspx.cs" Inherits="Excels_Data_EXCELMB" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="Server">
    <script type="text/javascript" src="<%=Request.ApplicationPath%>/JS/MBDataLoader.js"></script>
    <script type="text/javascript">

        //获取按钮权限
        function GetButtonQX() {
            if (ParamDic["MBSZ"] == "ExcelData" || ParamDic["MBLB"] == "queryflow" || ParamDic["MBLB"] == "respys") {
                $.ajax({
                    type: 'get',
                    url: 'InitMBHandle.ashx',
                    data: {
                        action: 'GetMBQX',
                        FADM: ParamDic["FADM"],
                        MBDM: ParamDic["MBDM"],
                        MBLB: ParamDic["MBLB"],
                        MBLX: ParamDic["MBLX"],
                        ISEXE: ParamDic["ISEXE"],
                        SFZLC: ParamDic["SFZLC"],
                        USERDM: ParamDic["USERDM"],
                        HSZXDM: ParamDic["HSZXDM"],
                        LCDM: ParamDic["LCDM"]
                    },
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result != "" && result != null) {
                            Btn = result;
                            QX = eval('({' + result + '})');
                        }
                    }
                });
            }
            //设置按钮权限
            SetButtonQX();
        }

        //设置按钮权限
        function SetButtonQX() {
            //设置页面按钮对应的div的显示和隐藏
            $("#ExcelData").attr("style", "display:inline");
            //启用模板设置页面按钮可用
            if (Btn != "" && Btn != undefined && Btn != null) {
                var BtnID, ID;
                var display = "";
                debugger;
                var Arr = Btn.split(',');
                for (var i = 0; i < Arr.length; i++) {
                    BtnID = Arr[i].split(':');
                    display = BtnID[1] == "1" ? false : true;
                    ID = BtnID[0].substring(1, BtnID[0].length - 1);
                    if (ID == "BDDK") {
                        $("#doImport").attr("disabled", display);
                    }
                    if (ID == "BDLC") {
                        $("#doExport").attr("disabled", display);
                    }
                    if (ID == "GQ" && BtnID[1] == "0") {
                        $("#validity").val("0");
                    }
                    $("#" + ID).attr("disabled", display);
                }
            }
        }

        //        /**
        //        * Created by LI on 2017-05-17 .
        //        * 请求服务器端json数据，在页面展示
        //        */
        //        function openJson(selected, IsExist) {
        //            var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
        //            var excelIo = new GC.Spread.Excel.IO();
        //            var path = window.document.location.pathname.substring(1);
        //            var Len = path.lastIndexOf("/");
        //            var root = "/" + path.substring(0, Len + 1);
        ////            if (ParamDic["MBSZ"] == "ExcelData" && IsExist) {
        ////                root = root + "Excels/Data/";
        ////            }
        ////            else {
        ////                root = root + "Excels/ExcelMb/";
        ////            }
        //            var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected + "&rand=" + Math.random();
        //            var xhr = new XMLHttpRequest();
        //            xhr.open('GET', url, true);
        //            xhr.responseType = 'text';

        //            xhr.onload = function (e) {
        //                if (this.status == 200) {
        //                    //设置自定义公式
        //                    MySelfMethod();
        //                    // get binary data as a response
        //                    importJson(JSON.parse(this.response));

        //                    //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
        //                    //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
        //                    if (ParamDic["MBSZ"] == "ExcelData") {
        //                        //如果存在数据页的话，则执行下面操作
        //                        if (IsExist) {
        //                            //打开Excel之后的操作
        //                            AfterOpen();
        //                            dialogshow('已填报文件加载完成!');
        //                        }
        //                        else {
        //                            //不存在执行下面操作
        //                            AfterOpenMb();
        //                            dialogshow('模板文件加载完成!');
        //                        }
        //                    }
        //                    delSpacseValue(); //将模板单元格值为"" 设置为null
        //                }
        //            };

        //            xhr.send();
        //        }

        $(function () {
            debugger;
            easyuiload();
            GetButtonQX();
            $("#bardiv").attr("style", "width:140%;display:inline-table");
            //显示计划方案名称
            if ($("#validity").val() == '0') {
                alert("当前填报时间已经截止，无法保存报表，请联系预算管理办公室！");
            }
            $("#DivTitle").html("  计划方案:【" + ParamDic["FAMC"] + "】");

            //加载Excel数据页
            InitData();
            var activeSheet = spread.getActiveSheet();
            activeSheet.setColumnVisible(0, false);
            activeSheet.setRowVisible(0, false);
            easyuidisLoad();
        });
        var param = "";
        // <summary>添加变动项目</summary>
        function AddXM() {
            $("#BDXMMC").val("");
            param = "add";
            //$('#dialogBDXM').window('open');
            $('#applyBDX').window('open');
        }
        // <summary>修改变动项目</summary>
        function UpdateBdxm() {
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length > 0) {
                if (rows.length > 1) {
                    tishi("修改变动行只能选择一项");
                }
                else {
                    $("#BDXMMC").val(rows[0].BDXMMC);
                    param = "upt";
                    $('#dialogBDXM').window('open');
                }
            }
            else {
                tishi("请选择要修改的项目名称");
            }
        }
        // <summary>删除变动行名称</summary>
        function DeleteBdxm() {
            var mbdm = ParamDic["MBDM"];
            var hszxdm = ParamDic["HSZXDM"];
            var rows = $('#dg').datagrid('getSelections');
            var delRow = [];
            if (rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    delRow.push(rows[i].BDXMDM);
                }
                $.messager.confirm('警告', '确定要删除该变动项目名称吗？', function (r) {
                    if (r) {
                        $.ajax({
                            type: 'post',
                            url: '../Data/MB.ashx?action=DelBdXMMC&bdxmdm=' + delRow.join(",") + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result > 0) {
                                    tishi("删除成功!");
                                    GetDataGrid(hszxdm, mbdm);
                                }
                                else if (result == -1) {
                                    tishi("变动项目已被使用无法删除！");
                                }
                                else {
                                    tishi("删除失败!");
                                }
                            }
                        });
                    }
                });
            }
            else {
                tishi("没有可删除的变动行名称");
            }
        }
        function UpdateBd() {
            var mbdm = ParamDic["MBDM"];
            var row = $("#dg").datagrid('getSelections');
            if (row.length > 0) {
                if (row.length > 1) {
                    tishi("修改变动行只能选择一项");
                }
                else {
                    var sheet = spread.getActiveSheet();
                    var a = sheet.getSelections();
                    var FatherRow = a[0].row;
                    var sheetName = sheet.name();
                    //SelXmmcCol(mbdm, sheetName);
                    XMMCCol = findXMMCColumn(sheet);
                    if (XMMCCol == 0) {
                        alert("您没有设置项目名称列！");
                        return;
                    }
                    var sel = sheet.getCell(parseInt(FatherRow), 0).value();
                    var CellJson = JSON.parse("{\"" + sel.replace(/:/g, "\":\"").replace(/,/g, "\",\"") + "\"}");
                    //查找选中的项目名称列
                    var selxmmc = sheet.getCell(parseInt(FatherRow), parseInt(XMMCCol)).value();
                    //当前要修改的名称
                    var ThisUpXmmc = row[0].BDXMMC;
                    var FirstColSX = "";
                    //查找首列的行类型
                    $.ajax({
                        type: 'get',
                        url: '../Data/MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName + '',
                        async: false,
                        cache: false,
                        success: function (result) {
                            FirstColSX = result;
                        }
                    });
                    if (sel.indexOf("|") > -1) {
                        if (FirstColSX == "2" || FirstColSX == "3" || FirstColSX == "5") {
                            var xmfl = $("#cslx").combobox("getValue");
                            if (FirstColSX == "2") {
                                ychsx = "CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl;
                            }
                            else if (FirstColSX == "3") {
                                ychsx = "ZYDM:" + CellJson["ZYDM"] + ",CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl;
                            }
                            else {
                                ychsx = "ZYDM:" + CellJson["ZYDM"] + ",CPDM:" + CellJson["CPDM"].split('|')[0] + "|" + row[0].BDXMDM + ",XMFL:" + xmfl + ",JSDX:" + CellJson["JSDX"];
                            }

                            if (CellJson["BZ"] != undefined) {
                                ychsx = ychsx + ",BZ:" + CellJson["BZ"];
                            }

                            if (sel != ychsx) {
                                //判断是否
                                var rtn = IsExistSameDynamicRow(sheet, ychsx, FirstColSX, FatherRow, true);
                                if (rtn == 1) {
                                    tishi("已有相同的变动项目！");
                                    IsHavebdxm = 0;
                                }
                                else {
                                    unprotectsheet(sheet);
                                    sheet.getCell(parseInt(FatherRow), 0).value(ychsx);
                                    sheet.getCell(parseInt(FatherRow), parseInt(XMMCCol)).value(ThisUpXmmc);
                                    tishi("修改成功!");
                                    protectsheet(sheet);
                                }
                            }
                            else {
                                if (selxmmc != ThisUpXmmc) {
                                    unprotectsheet(sheet);
                                    sheet.getCell(parseInt(FatherRow), parseInt(XMMCCol)).value(ThisUpXmmc);
                                    tishi("修改成功!");
                                    protectsheet(sheet);
                                }
                                else {
                                    tishi("与原数据一致，并未修改!");
                                }
                            }
                        }
                        else {
                            alert("此文件不能添加变动行!");
                            return;
                        }
                    }
                    else {
                        tishi("非变动项目不可修改!");
                    }
                }
            }
            else {
                tishi("请选择要修改的变动项目!")
            }
        }
        //查找项目名称列所在的列标
        function findXMMCColumn(sheet) {
            var cell;
            var colIndex = 0;
            var colCount = usedrcolscnt(sheet);
            for (var k = 0; k < colCount; k++) {
                cell = sheet.getCell(0, k);
                if (cell != undefined && cell.value() != undefined) {
                    if (cell.value().indexOf("JSDX:XMMC|") > -1) {
                        colIndex = k;
                        break;
                    }
                }
            }
            return colIndex;
        }
        //添加变动行时，是否存在同样的变动行
        function IsExistSameDynamicRow(sheet, rowProperty, rowType, rowIndex, flag) {
            var f = -1;
            //获取可用行的行数
            var rowCount = usedrowscnt(sheet);
            rowProperty = "{\"" + rowProperty.replace(/:/g, "\":\"").replace(/,/g, "\",\"") + "\"}";
            var rowPropJson = JSON.parse(rowProperty);
            //如果当前选中的行属性包含 | 的话，则表明选择的是变动项，即加变动项的兄弟节点；
            //选择兄弟节点的话，需要向上、向下遍历，直到遇到不带| 的行属性，表明向上找到的它的父节点，向下找到了父节点的兄弟节点
            //否则添加的是选中节点的子节点
            //选择父节点的话，只须向下遍历，直到循环到父节点的兄弟节点为止
            if (flag) {
                //向上循环遍历
                for (var i = rowIndex; i >= 1; i--) {
                    f = findSameRowProperty(sheet, rowPropJson, rowType, i);
                    if (f == 1 || f == 0)
                        break;
                }

                if (f == -1) {
                    //向下遍历
                    for (var j = rowIndex; j <= rowCount; j++) {
                        f = findSameRowProperty(sheet, rowPropJson, rowType, j);
                        if (f == 1 || f == 0)
                            break;
                    }
                }
            }
            else {
                //向下遍历
                for (var i = rowIndex + 1; i <= rowCount; i++) {
                    f = findSameRowProperty(sheet, rowPropJson, rowType, i);
                    if (f == 1 || f == 0)
                        break;
                }
            }
            return f;
        }
        //项目名称列
        var XMMCCol = 0;
        //获取项目分类列
        var XMFLXZCol = 0;
        //获取下一个要添加的行
        var nextRows = 0;
        //查找父亲行
        var nextFatherRow = 0;
        //是否有空行
        var isKh = 0;
        //空行的个数；
        var isKhCount = 0;
        //是否有相同的变动项目
        var IsHavebdxm = 0;
        //确定插入变动行
        function SelQuery() {
            var sheet = spread.getActiveSheet();
            var a = sheet.getSelections();
            var fatherRow = a[0].row;
            //获取选中行的第一列的值
            var sel = sheet.getCell(parseInt(fatherRow), 0).value();
            //获取选中行下一行的值
            var nextvalue = sheet.getCell(parseInt(fatherRow) + 1, 0).value();
            if (sel.indexOf("|") > -1) {
                if (nextvalue != undefined) {
                    if (nextvalue.indexOf("BZ:YZ") > -1) {
                        if (sel.indexOf("BZ:YZ") > -1) {
                            tishi("延展行之间不能插入变动行");
                        }
                        else {
                            //获取选中行的上一行的值
                            var lastValue = sheet.getCell(parseInt(fatherRow) - 1, 0).value();
                            if (lastValue.indexOf("BZ:YZ") > -1) {
                                tishi("延展行之间不能插入变动行");
                            }
                            else {
                                InsertValue();
                            }
                        }
                    }
                    else {
                        InsertValue();
                    }
                }
                else {
                    InsertValue();
                }
            }
            else {
                InsertValue();
            }
        }
        //插入变动行数据
        function InsertValue() {
            var mbdm = ParamDic["MBDM"];
            var sheet = spread.getActiveSheet();
            //获取sheetName
            var sheetName = sheet.name();
            //项目分类
            var xmfl = $("#cslx").combobox("getValue");
            //获取变动datagrid选中的行
            var row = $("#dg").datagrid('getSelections');
            var a = sheet.getSelections();
            var fr = a[0].row;
            var fatherRow = a[0].row;
            //获取选中行的第一列的值
            var sel = sheet.getCell(parseInt(fatherRow), 0).value();
            //获取选中行下一行的值
            var nextvalue = sheet.getCell(parseInt(fatherRow) + 1, 0).value();
            //第一列的属性
            var firstColSx = "";
            //查找首列的行类型
            $.ajax({
                type: 'get',
                url: '../Data/MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName + '',
                async: false,
                cache: false,
                success: function (result) {
                    firstColSx = result;
                }
            });
            if (nextvalue != undefined) {
                SelNextRow(sel, nextvalue, parseInt(fatherRow) + 1, firstColSx);
                fatherRow = nextRows;
            }
            var len;
            //如果有选中变动行的项目
            if (row.length > 0) {
                var arr = new Array();
                arr = sel.split(',');
                var cpdmCol = "";
                var zydmCol = "";
                //显示作业+分类
                if (firstColSx == "1") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a[0].row);
                        len = FatherValue.indexOf(",");
                        zydmCol = FatherValue.substring(0, len);
                    }
                    else {
                        zydmCol = arr[0];
                    }
                }
                //产品代码+分类
                else if (firstColSx == "2") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a[0].row);
                        len = FatherValue.indexOf(",");
                        cpdmCol = FatherValue.substring(0, len);
                    }
                    else {
                        cpdmCol = arr[0];
                    }
                }
                //作业代码+产品代码+分类
                else if (firstColSx == "3") {
                    if (sel.indexOf("|") > -1) {
                        QueryFatherValue(sel, a[0].row);
                        len = FatherValue.lastIndexOf(",");
                        cpdmCol = FatherValue.substring(0, len);
                    }
                    else {
                        len = sel.lastIndexOf(",");
                        cpdmCol = sel.substring(0, len);
                    }
                }
                //作业代码+计算对象+分类
                else if (firstColSx == "4") {

                }
                //作业代码+产品代码+计算对象+分类
                else if (firstColSx == "5") {

                }
                //查找项目名称所在的列
                //SelXmmcCol(mbdm, sheetName);
                XMMCCol = findXMMCColumn(sheet);
                if (XMMCCol == 0) {
                    alert("您没有设置项目名称列！");
                    return;
                }
                for (var i = 0; i < row.length; i++) {
                    var ychsx = "";
                    var xmmc = "";
                    var nextRow = parseInt(fatherRow) + (i + 1);
                    if (firstColSx == "1") {
                        ychsx = zydmCol + '|' + row[i].BDXMBM + ',' + arr[1].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC
                    }
                    else if (firstColSx == "2") {
                        ychsx = cpdmCol + '|' + row[i].BDXMBM + ',' + arr[1].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC;
                    }
                    else if (firstColSx == "3") {
                        ychsx = cpdmCol + '|' + row[i].BDXMBM + ',' + arr[2].split(':')[0] + ':' + xmfl;
                        xmmc = row[i].BDXMMC;
                    }
                    else if (firstColSx == "4") {
                    }
                    else if (firstColSx == "5") {
                    }
                    var ff;
                    //如果是变动行增加的时候
                    if (sel.indexOf("|") > -1) {
                        ff = true;
                    }
                    else {
                        ff = false;
                    }
                    var rtn = IsExistSameDynamicRow(sheet, ychsx, firstColSx, fr, ff);
                    if (rtn == 1) {
                        tishi("已有相同的变动项目！");
                        IsHavebdxm = 0;
                        break;
                    }
                    else {
                        //查找下行是否有空行
                        if (isKh == 0) {
                            //下一个要插入变动行的值
                            var nextInsertRow = sheet.getCell(nextRow, 0).value();
                            //查找最大列
                            var maxCol = usedrcolscnt(sheet);
                            //统计下一行值的个数
                            var countValue = 0;
                            var thisKhRow;
                            if (sel.indexOf("|") > -1) {
                                SelFatherRow(sel, a[0].row);
                                var fatherValue = sheet.getCell(parseInt(nextFatherRow), 0).value();
                                //查找父亲行下是否有空行
                                selIsNullKh(mbdm, sheetName, fatherValue, XMMCCol);
                                if (isKhCount > 0) {
                                    thisKhRow = nextRow - nextFatherRow;
                                    if (thisKhRow >= isKhCount) {
                                        isKh = 1;
                                    }
                                    if (nextInsertRow == undefined) {
                                        isKh = 0;
                                    }
                                }
                                if (nextInsertRow == undefined) {
                                    isKh = 0;
                                }
                                //表示模板定义没有保存
                                if (isKh == "-1") {
                                    //查找下行是否有值
                                    for (var n = 0; n < maxCol; n++) {
                                        var nextValue = sheet.getCell(a[0].Row + 1, n).value();
                                        if (nextValue != "" || nextValue != undefined) {
                                            countValue++;
                                        }
                                    }
                                    if (countValue > 0) {
                                        isKh = 1;
                                    }
                                    else {
                                        isKh = 0;
                                    }
                                }
                            }
                            else {
                                selIsNullKh(mbdm, sheetName, sel, XMMCCol);
                                if (isKhCount > 0) {
                                    thisKhRow = nextRow - a[0].row;
                                    if (thisKhRow >= isKhCount) {
                                        isKh = 1;
                                    }
                                }
                                if (nextInsertRow == undefined) {
                                    isKh = 0;
                                }
                            }
                            //表示模板定义没有保存
                            if (isKh == "-1") {
                                //查找下行是否有值
                                for (var n = 0; n < maxCol; n++) {
                                    var nextValue = sheet.getCell(a[0].Row + 1, n).value();
                                    if (nextValue != "" || nextValue != undefined) {
                                        countValue++;
                                    }
                                }
                                if (countValue > 0) {
                                    isKh = 1;
                                }
                                else {
                                    isKh = 0;
                                }
                            }
                        }
                        unprotectallsheet(sheet);
                        if (isKh == "1") {
                            ychsx = ychsx + ",BZ:Ins";
                            //sheet.Rows(nextRow).Insert();
                            sheet.addRows(nextRow, 1);
                            isKh = 0;
                        }
                        var insertRow;
                        var insertXmmc;
                        if (firstColSx == "1") {
                            insertRow = sheet.getCell(parseInt(nextRow), 0);
                            insertRow.value(ychsx);
                            if (XMMCCol != 0) {
                                insertXmmc = sheet.getCell(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.value(xmmc);
                            }
                        }
                        if (firstColSx == "2") {
                            insertRow = sheet.getCell(parseInt(nextRow), 0);
                            insertRow.value(ychsx);
                            if (XMMCCol != 0) {
                                insertXmmc = sheet.getCell(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.value(xmmc);
                            }
                        }
                        if (firstColSx == "3") {
                            insertRow = sheet.getCell(parseInt(nextRow), 0);
                            insertRow.value(ychsx);
                            if (XMMCCol != 0) {
                                insertXmmc = sheet.getCell(parseInt(nextRow), parseInt(XMMCCol));
                                insertXmmc.value(xmmc);
                            }
                        }
                        if (firstColSx == "4") {
                        }
                        if (firstColSx == "5") {
                        }
                        var xcolor = "#A2F6C2";
                        maxCol = usedrcolscnt(sheet);
                        var style = new spreadNS.Style();
                        style.backColor = xcolor;
                        sheet.setStyle(nextRow, -1, style, GC.Spread.Sheets.SheetArea.viewport);
                        sheet.getRange(nextRow, 0, 1, sheet.getColumnCount(), GC.Spread.Sheets.SheetArea.viewport).setBorder(
                            new GC.Spread.Sheets.LineBorder("#000000", GC.Spread.Sheets.LineStyle.thin),
                            { all: true },
                            3);
                        //sheet.Range("B" + nextRow + ":" + C + nextRow).Borders.Color = parseInt("000000", 16);
                        //sheet.Range("B" + nextRow + ":" + C + nextRow).Locked = "False";
                        sheet.getRange(nextRow, 0, 1, sheet.getColumnCount()).locked(false);
                        //有项目分类的添加下拉框
                        //po.AddExcelDropDownList(sheet, ychsx, nextRow);
                        protectallsheet(sheet);
                    }
                }
            }
            else {
                tishi("无法插入变动项目");
            }
        }
        //获取下个要插入的行
        function SelNextRow(sel, nextvalue, FatherRow, FirstColSX) {
            if (nextvalue != undefined) {
                var cpdm = "";
                var zydm = "";
                var nextcpdm = "";
                var nextzydm = "";
                var xmfl = "";
                var nextxmfl = "";
                var arr = new Array();
                arr = sel.split(',');
                var nextsel = new Array();
                nextsel = nextvalue.split(',');
                //显示作业+分类
                if (FirstColSX == "1") {
                    //拆分变动行的ZYDM
                    if (sel.indexOf("|") > -1) {
                        zydm = arr[0].split('|')[0];
                        xmfl = arr[1];
                    }
                    else {
                        zydm = arr[0];
                        xmfl = arr[1];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextzydm = nextsel[0].split('|')[0];
                        nextxmfl = nextsel[1];
                    }
                    else {
                        nextzydm = nextsel[0];
                        nextxmfl = nextsel[1];
                    }
                    //当前行的首列
                    var thiszydm = zydm + ',' + xmfl;
                    //下一行的首列
                    var nextFirstCol = nextzydm + ',' + nextxmfl;
                    if (thiszydm == nextFirstCol) {
                        var sheet = spread.getActiveSheet();
                        var sheetName = sheet.name();
                        var nextvalue = sheet.getCell(parseInt(FatherRow) + 1, parseInt(1)).value();
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //产品代码+分类
                else if (FirstColSX == "2") {
                    if (sel.indexOf("|") > -1) {
                        cpdm = arr[0].split('|')[0];
                    }
                    else {
                        cpdm = arr[0];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextcpdm = nextsel[0].split('|')[0];
                    }
                    else {
                        nextcpdm = nextsel[0];
                    }
                    if (nextcpdm == cpdm) {
                        var sheet = spread.getActiveSheet();
                        var sheetName = sheet.name();
                        var nextvalue = sheet.getCell(parseInt(FatherRow) + 1, parseInt(1)).value();
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //作业代码+产品代码+分类
                else if (FirstColSX == "3") {
                    if (sel.indexOf("|") > -1) {
                        cpdm = arr[1].split('|')[0];
                    }
                    else {
                        cpdm = arr[1];
                    }
                    if (nextvalue.indexOf("|") > -1) {
                        nextcpdm = nextsel[1].split('|')[0];
                    }
                    else {
                        nextcpdm = nextsel[1];
                    }
                    if (nextcpdm == cpdm) {
                        var sheet = spread.getActiveSheet();
                        var sheetName = sheet.name();
                        var nextvalue = sheet.getCell(parseInt(FatherRow) + 1, parseInt(1)).value();
                        SelNextRow(sel, nextvalue, parseInt(FatherRow) + 1, FirstColSX);
                    }
                    else {
                        nextRows = parseInt(FatherRow) - 1;
                    }
                }
                //作业代码+计算对象+分类
                else if (FirstColSX == "4") {

                }
                //作业代码+产品代码+计算对象+分类
                else if (FirstColSX == "5") {

                }
            }
            else {
                nextRows = parseInt(FatherRow) - 1;
            }
        }
        //查找父亲行的第一列
        function QueryFatherValue(selectValue, row) {
            if (selectValue == undefined) {
                var sheet = spread.getActiveSheet();
                var lastRow = row - 1;
                var lastValue = sheet.getCell(parseInt(lastRow), 0).value();
                QueryFatherValue(lastValue, lastRow);
            }
            else if (selectValue.indexOf("|") > 0) {
                var sheet = spread.getActiveSheet();
                var lastRow = row - 1;
                var lastValue = sheet.getCell(parseInt(lastRow), 0).value();
                QueryFatherValue(lastValue, lastRow);
            }
            else {
                FatherValue = selectValue;
            }
        }
        //查找下行是否有空行
        function selIsNullKh(mbdm, sheetName, sel, XMMCCol) {
            $.ajax({
                type: 'get',
                url: '../Data/MB.ashx?action=IsNullKh&MBDM=' + mbdm + '&sheetName=' + escape(sheetName) + '&Value=' + sel + '&ColumnIndex=' + XMMCCol + '',
                async: false,
                cache: false,
                success: function (result) {
                    if (result > 1) {
                        isKhCount = result;
                    }
                    else {
                        isKh = result;
                    }
                }
            });
        }
        //查找变动项目的父亲行
        function SelFatherRow(sel, row) {
            if (sel == undefined) {
                var sheet = spread.getActiveSheet();
                var lastValue = sheet.getCell(parseInt(row) - 1, 0).value();
                SelFatherRow(lastValue, parseInt(row) - 1);
            }
            else if (sel.indexOf("|") > 0) {
                var arr = new Array();
                arr = sel.split(',');
                var sheet = spread.getActiveSheet();
                var lastValue = sheet.getCell(parseInt(row) - 1, 0).value();
                SelFatherRow(lastValue, parseInt(row) - 1);
            }
            else {
                nextFatherRow = row;
            }
        }
        function findSameRowProperty(sheet, RowPropJson, RowType, RowIndex) {
            var F = -1;
            var CellJson;
            var Cell = sheet.getCell(RowIndex, 0);
            if (Cell != undefined && Cell.value() != undefined) {
                if (Cell.value().indexOf('|') > -1) {
                    CellJson = JSON.parse("{\"" + Cell.value().replace(/:/g, "\":\"").replace(/,/g, "\",\"") + "\"}");
                    if (RowType == "2") {
                        if (RowPropJson["CPDM"] == CellJson["CPDM"] && RowPropJson["XMFL"] == CellJson["XMFL"]) {
                            F = 1;
                        }
                    }
                    else if (RowType == "3") {
                        if (RowPropJson["ZYDM"] == CellJson["ZYDM"] && RowPropJson["CPDM"] == CellJson["CPDM"] && RowPropJson["XMFL"] == CellJson["XMFL"]) {
                            F = 1;
                        }
                    }
                    else if (RowType == "5") {
                        if (RowPropJson["ZYDM"] == CellJson["ZYDM"] && RowPropJson["CPDM"] == CellJson["CPDM"] && RowPropJson["XMFL"] == CellJson["XMFL"] && RowPropJson["JSDX"] == CellJson["JSDX"]) {
                            F = 1;
                        }
                    }
                    else {
                        alert("此文件不能设置变动行！");
                        F = 0;
                    }
                }
            }
            return F;
        }
        function err(target, message) {
            var t = $(target);
            if (t.hasClass('textbox-text')) {
                t = t.parent();
            }
            var m = t.next('.error-message');
            if (!m.length) {
                m = $('<div class="error-message"></div>').insertAfter(t);
            }
            m.html(message);
        }
        function submitForm() {
            $('#ff').form('submit', {
                url: '../Data/ExcelData.ashx?action=addBDX&bdxname=' + $('#bdxname').val() + '&xmdm=' + $('#xmdm').val() + '&ischeckpar=' + $('#isCheckPar').val(),
                onSubmit: function () {
                    return $(this).form('enableValidation').form('validate');
                },
                success: function () {
                    prompt("保存成功");
                    $('#applyBDX').window('close');
                    GetNewDataGrid($('#xmdm').val(), $('#isCheckPar').val());
                }
            });
        }
        function clearForm() {
            $('#ff').form('clear');
        }
    </script>
    <style scoped="scoped">
        .tb
        {
            width: 100%;
            margin: 0;
            padding: 5px 4px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        
        .error-message
        {
            margin: 4px 0 0 0;
            padding: 0;
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="ExcelData">
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doImport"
            disabled="disabled" title="@toolBar.importFile@">
            <span class="fa fa-folder-open fa-lg"></span></br> <span>本地打开</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doExport"
            disabled="disabled" title="@toolBar.export.title@">
            <span class="fa icon-DC fa-lg"></span></br> <span>本地保存</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="HQZXMB"
            onclick="OpenMbFile()" disabled="disabled" title="@toolBar.HQZXMB@">
            <span class="fa icon-HQZXMB fa-lg"></span></br> <span>获取最新模板</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SCSJ"
            onclick="InitTrCc()" disabled="disabled" title="@toolBar.SCSJ@">
            <span class="fa icon-SCSJ fa-lg"></span></br> <span>生成数据</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="ZJSJ"
            onclick="InsertTrCc()" disabled="disabled" title="@toolBar.ZJSJ@">
            <span class="fa icon-ZJSJ fa-lg"></span></br> <span>追加数据</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="JSBB"
            onclick="ZoneCal()" disabled="disabled" title="@toolBar.JSBB@">
            <span class="fa icon-QYJS fa-lg"></span></br> <span>计算报表</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="QQYJS"
            onclick="AllZoneCal()" disabled="disabled" title="@toolBar.QQYJS@">
            <span class="fa icon-QQYJS fa-lg"></span></br> <span>全区域计算</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="HQHLSX"
            onclick="GetRowCol()" disabled="disabled" title="@toolBar.HQHLSX@">
            <span class="fa fa-table fa-lg"></span></br> <span>获取行列属性</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="INSERTBDH"
            onclick="InsertRow()" disabled="disabled" title="@toolBar.InsertRow@">
            <span class="fa fa-align-justify fa-lg"></span></br> <span>插入变动行</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DELBDH"
            onclick="DelBdRow()" disabled="disabled" title="@toolBar.DelBdRow@">
            <span class="fa fa-eraser fa-lg"></span></br> <span>删除变动行</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="BC"
            onclick="SaveUpClick()" disabled="disabled" title="@toolBar.BC@">
            <span class="fa fa-floppy-o fa-lg"></span></br> <span>保存</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SB"
            onclick="DataUp()" disabled="disabled" title="@toolBar.SB@">
            <span class="fa icon-SB fa-lg"></span></br> <span>上报</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SP"
            onclick="DataApp()" disabled="disabled" title="@toolBar.SP@">
            <span class="fa icon-SP fa-lg"></span></br> <span>审批</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DH"
            onclick="DataBack()" disabled="disabled" title="@toolBar.DH@">
            <span class="fa icon-DH fa-lg"></span></br> <span>退回</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DY"
            onclick="Print()" disabled="disabled" title="@toolBar.DY@">
            <span class="fa fa-print fa-lg"></span></br> <span>打印</span>
        </button>
    </div>
    <div id="WinJS" class="easyui-window" title="模板打回选取" closed="true" collapsible="false"
        minimizable="false" style="width: 600px; height: 300px; padding: 20px; text-align: center;">
        <div style="text-align: left" class="easyui-layout" fit="true" style="width: 100%;
            height: 100%;">
            <div style="height: 30px; background: #ADD8E6;" data-options="region:'north'">
                <input type="button" value="确定" onclick="WinJS(false, 1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinJS(false, -1)" class="button5" style="width: 60px" />&nbsp;&nbsp;
            </div>
            <div data-options="region:'center'" style="width: 100%; height: 100%;">
                <ul id="tt" class="easyui-tree" data-options="checkbox:true">
                </ul>
            </div>
        </div>
    </div>
    <div id="SZBDH" class="easyui-window" title="设置变动行项" closed="true" style="width: 600px;
        height: 420px; padding: 20px; text-align: center;" minimizable="false" collapsible="false"
        resizable="false" maximizable="false">
        <table style="width: 100%">
            <tr>
                <td style="width: 224px">
                    项目分类：
                    <input id="cslx" type="text" style="width: 120px" class="easyui-combobox" data-options="valueField:'id',textField:'text',panelHeight:'auto'"
                        editable="false" />
                </td>
                <td colspan="3" align="left">
                    项目名称：<input id="TxtXMMC" style="width: 150px" type="text" class="easyui-validatebox" />
                </td>
            </tr>
            <tr>
                <td>
                    查找：<input id="TxtselXm" style="width: 120px" type="text" onkeypress="ChangeText()"
                        onkeyup="ChangeText()" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 12px">
                    <input type="button" class="button5" value="增加项目" onclick="AddXM()" style="width: 80px" />&nbsp
                    &nbsp &nbsp &nbsp
                    <input type="button" class="button5" value="修改项目名称" onclick="UpdateBdxm()" style="width: 110px" />
                </td>
                <td style="padding-left: 20px;">
                    <input id="Button2" class="button5" style="width: 80px" type="button" value="删除项目"
                        onclick="DeleteBdxm()" />
                </td>
                <td style="padding-left: 20px; font-size: 12px">
                    <input type="button" class="button5" value="修改项目" onclick="UpdateBd()" style="width: 80px"
                        id="xgbdh" />
                </td>
                <td style="padding-left: 20px" align="left">
                    <input type="button" class="button5" value="选择确定" onclick="SelQuery()" style="width: 80px"
                        id="xzqr" />
                </td>
            </tr>
            <tr>
                <table id="dg" class="easyui-datagrid" style="height: 80%; width: 100%" data-options="autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: false,idField:'BDXMDM'">
                    <thead>
                        <tr>
                            <th data-options="field:'BDXMBM',width:20">
                                变动项目编码
                            </th>
                            <th data-options="field:'BDXMMC',width:60">
                                变动项目名称
                            </th>
                        </tr>
                    </thead>
                </table>
            </tr>
        </table>
        <div id="dialogBDXM">
        </div>
        <div id="BDXM">
            <table>
                <tr style="padding-top: 100px">
                    <th>
                        变动项目名称:
                    </th>
                    <th>
                        <input id="BDXMMC" type="text" class="easyui-validatebox" data-options="required:true" />
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <div id="applyBDX" class="easyui-window" title="新变动项目申请" style="width: 300px;" data-options="closed:true,collapsible:false,minimizable:false,maximizable:false">
        <form id="ff" class="easyui-form" method="post" data-options="novalidate:true">
        <div style="margin-bottom: 20px">
            <label for="bdxname" class="label-top">
                变动项目名称:</label>
            <input id="bdxname" class="easyui-validatebox tb" data-options="required:true,validType:{remote:['../Data/ExcelData.ashx?action=checkName','bdxname']},invalidMessage:'存在重复或类似的名称',err:err">
        </div>
        </form>
        <div style="text-align: center; padding: 5px 0">
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width: 80px">
                提交</a> <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()"
                    style="width: 80px">清除</a>
        </div>
    </div>
    <div id="SMQR" class="easyui-dialog" title="书面确认" style="width: 610px; height: 480px;
        padding: 10px; text-align: right;" data-options="maximizable:false,resizable:true,closed:true,modal:true,buttons:[{text:'确定',iconCls:'icon-ok',handler: function(){ getContent()}},{text:'取消',iconCls:'icon-cancel',handler: function(){ close()}}]">
    </div>
    <input type="hidden" id="HidDH" />
    <input type="hidden" id="HidJS" />
    <input type="hidden" id="HidTJ" />
    <input type="hidden" id="HidBC" />
    <input type="hidden" id="HidJYGSLX" />
    <input type="hidden" id="validity" />
    <input type="hidden" id="xmdm" />
    <input type="hidden" id="isCheckPar" />
</asp:Content>
