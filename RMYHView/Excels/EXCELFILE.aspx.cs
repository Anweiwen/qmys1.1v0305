﻿//*********************************************************//
//** 名称：EXCEL模板
//** 功能: 设置EXCEL模板
//** 开发日期: 2011-11-10  开发人员：余妮
//** 表:TB_EXCEL           EXCEL模板
//**    TB_XH              函数
//*********************************************************//
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;
using System.Data.OleDb;
using Excel;
using System.Drawing;
using RMYH.BLL;
using Sybase.Data.AseClient;
using RMYH.DBUtility;



public partial class SIS_EXCELFILE : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SIS_EXCELFILE));
        //if (!IsPostBack)
        {
            PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/pageoffice/server.aspx";
            // 设置文件打开后执行的js function
            PageOfficeCtrl1.JsFunction_AfterDocumentOpened = "AfterDocumentOpened()";
            PageOfficeCtrl1.AddCustomToolButton("保存", "Save()", 1);
            string address=hidAddress.Value;

            //定义Workbook对象
            PageOffice.ExcelWriter.Workbook workBook = new PageOffice.ExcelWriter.Workbook();
            //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
            PageOffice.ExcelWriter.Sheet sheet = workBook.OpenSheet("Sheet1");
            PageOffice.ExcelWriter.Table table1 = sheet.OpenTable("B1:B4");
            
            //设置table对象的提交名称，以便保存页面获取提交的数据
            table1.SubmitName = "Info";
            PageOfficeCtrl1.SetWriter(workBook);
            PageOfficeCtrl1.SaveDataPage = "Excel.aspx";
            PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
            //PageOfficeCtrl1.SaveFilePage = "SaveFile1.aspx";
            //PageOfficeCtrl1.WebOpen("doc/test1.xls", PageOffice.OpenModeType.xlsSubmitForm, "操作人姓名");
        }
    }
   
    protected void Button111_Click(object sender, EventArgs e)
    {

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        var mbid = hidID.Value;
        string sql = "select * from TB_EXCEL where ID='" + mbid + "' ";
        DataSet set = bll.Query(sql);
        string xlsValue;
        if (set.Tables[0].Rows.Count > 0)
        {
            xlsValue = set.Tables[0].Rows[0]["FILENAME"].ToString();
        }
        else
        {
            xlsValue = "test1.xls";
        }
       // PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/pageoffice/server.aspx";
        // 设置文件打开后执行的js function
        //PageOfficeCtrl1.JsFunction_AfterDocumentOpened = "AfterDocumentOpened()";
       // PageOfficeCtrl1.AddCustomToolButton("保存", "Save()", 1);

        //定义Workbook对象
        //PageOffice.ExcelWriter.Workbook workBook = new PageOffice.ExcelWriter.Workbook();
        //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        //PageOffice.ExcelWriter.Sheet sheet = workBook.OpenSheet("Sheet1");
        //PageOffice.ExcelWriter.Table table1 = sheet.OpenTable("B1:B4");
        //设置table对象的提交名称，以便保存页面获取提交的数据
        //table1.SubmitName = "Info";
        //PageOfficeCtrl1.SetWriter(workBook);
        //PageOfficeCtrl1.SaveDataPage = "Excel.aspx";
        //PageOfficeCtrl1.SaveFilePage = "SaveFile.aspx";
        //PageOfficeCtrl1.SaveFilePage = "SaveFile1.aspx";
        PageOfficeCtrl1.WebOpen("doc/"+xlsValue+"", PageOffice.OpenModeType.xlsSubmitForm, "操作人姓名");
    }
}

   