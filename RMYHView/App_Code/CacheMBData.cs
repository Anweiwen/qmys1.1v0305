﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMYH.Model;
using RMYH.BLL;
using RMYH.Common;
using System.Text;
using System.Data;

/// <summary>
///CacheMBData 的摘要说明
///缓存Excel数据到内存中
/// </summary>
public class CacheMBData
{
	public CacheMBData()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    #region 将模板页数据加载到Cache缓存

    /// <summary>
    /// 将模板页数据加载到Cache缓存
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    public static void CacheDataToMemory(string JHFADM, string MBDM, string SessionID)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //缓存的数据的键是由SessionID、计划方案代码以及模板代码组成
        string CacheKey = SessionID + "." + JHFADM + "." + MBDM;
        var OBJ = DataCache.GetCache(CacheKey);
        if (OBJ == null)
        {
            ExcelRow ER;
            ExcelCol EC;
            ExcelSheet ES;
            int RowIndex = 0;
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataTable DT2 = new DataTable();
            List<OwnExcel> MBGS = new List<OwnExcel>();
            DS = BLL.Query("SELECT * FROM TB_MBGSVALUE WHERE MBDM='" + MBDM + "' ORDER BY SHEETNAME,RowIndex,ColumnIndex");
            if (DS.Tables[0].Rows.Count > 0)
            {
                //获取模板下所有的Sheet页名称
                DT = DS.Tables[0].DefaultView.ToTable(true, new[] { "SHEETNAME" });
                List<ExcelSheet> LES = new List<ExcelSheet>();
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    ES = new ExcelSheet();
                    ES.SHEETNAME = DT.Rows[i]["SHEETNAME"].ToString();
                    List<ExcelRow> LER = new List<ExcelRow>();
                    var DV = DS.Tables[0].DefaultView;
                    DV.RowFilter = " SHEETNAME='" + DT.Rows[i]["SHEETNAME"] + "'";
                    DV.Sort = "RowIndex";
                    DataTable DTR = DV.ToTable(true, new[] { "SHEETNAME", "RowIndex" });

                    for (int j = 0; j < DTR.Rows.Count; j++)
                    {
                        ER = new ExcelRow();
                        RowIndex = int.Parse(DTR.Rows[j]["RowIndex"].ToString());
                        ER.RowIndex = RowIndex;
                        List<ExcelCol> LEC = new List<ExcelCol>();
                        //获取行下的列记录List集合
                        DataRow[] DR2 = DS.Tables[0].Select("SHEETNAME='" + DT.Rows[i]["SHEETNAME"] + "' AND RowIndex=" + RowIndex, "ColumnIndex");
                        foreach (var item in DR2)
                        {
                            EC = new ExcelCol();
                            EC.ColumnIndex = int.Parse(item["ColumnIndex"].ToString());
                            EC.GS = item["GS"].ToString();
                            EC.VALUE = item["VALUE"].ToString();
                            EC.DataFormat = item["DataFormat"].ToString();
                            LEC.Add(EC);
                        }
                        ER.Cols = LEC;
                        LER.Add(ER);
                    }
                    ES.Rows = LER;
                    LES.Add(ES);
                }
                if (LES.Count > 0)
                {
                    //将数据缓存到内存中
                    DataCache.SetCache(CacheKey, LES);
                }
            }
        }
    }
    #endregion
}