﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///RETURNREC 的摘要说明
/// </summary>
public class RETURNREC
{
	public RETURNREC()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    public class FIIF014REC
    {
        public string CHECK1 { get; set; }
        public string CHECK2 { get; set; }
        public string CHECK7 { get; set; }
        public string FMISKMBM { get; set; }
        public string FMISKMMS { get; set; }
        public string KMSFMX { get; set; }
        public string SAKNR { get; set; }
        public string TXT20 { get; set; }
        public string ZZJS { get; set; }
        public string TYBS { get; set; }
        public string XGSJ { get; set; }
    }
    public class FIIF009REC
    {
        public string F_ZBMC { get; set; }
        public string KOKRS { get; set; }
        public string KOSAR { get; set; }
        public string KOSTL { get; set; }
        public string KTEXT { get; set; }
        public string LTEXT { get; set; }
        public string PRCTR { get; set; }
        public string RCOMP { get; set; }
        public string ZZFMISBM { get; set; }
        public string ZZFMISCBZX { get; set; }
        public string CZBS { get; set; }
        public string XGSJ { get; set; }
    }

    public class FIIF027REC
    {
        public string BUKRS { get; set; }
        public DateTime DATAB { get; set; }
        public string GSBER { get; set; }
        public string JTORGF { get; set; }
        public string MAKTX { get; set; }
        public string MATKL { get; set; }
        public string MATNR { get; set; }
        public string MEINS { get; set; }
        public string NODEL { get; set; }
        public string USNAM_A { get; set; }
        public string USNAM_B { get; set; }
        public string WGBEZ { get; set; }
        public string ZZCPMC { get; set; }
        public string ZZCPZD { get; set; }
        public string XGSJ { get; set; }

    }

    public class MaterialREC
    {
        public string CHECK1 { get; set; }
        public string CHECK2 { get; set; }
        public string CHECK7 { get; set; }
        public string FACTORY { get; set; }
        public string FMISKMBM { get; set; }
        public string IM_BEGDA { get; set; }
    }

    public class BESGREC
    {
        public string BUKRS { get; set; }
        public string BELNR { get; set; }
        public string GJAHR { get; set; }
        public string BUZEI { get; set; }
        public string BUDAT { get; set; }
        public string MONAT { get; set; }
        public string KOART { get; set; }
        public string SHKZG { get; set; }
        public string DMBTR { get; set; }
        public string XNEGP { get; set; }
        public string SGTXT { get; set; }
        public string KOSTL { get; set; }
        public string AUFNR { get; set; }
        public string MATNR { get; set; }
        public string MENGE { get; set; }
        public string MEINS { get; set; }
        public string PRCTR { get; set; }
        public string ZZFMISKM { get; set; }
        public string ZZFMISBM { get; set; }
        public string ZZFMISCBZX { get; set; }
        public string ZZCPZD { get; set; }
        public string ZZPZLB { get; set; }
        public string ZZCRBS { get; set; }
        public string ZZZRZX { get; set; }
    }
}