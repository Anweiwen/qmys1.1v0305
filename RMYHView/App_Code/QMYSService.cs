﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using RMYH.BLL;
using System.Data;
using RMYH.DBUtility;
using Sybase.Data.AseClient;
using System.Reflection;
using System.Globalization;
using System.Threading;

    /// <summary>
    ///QMYSService 的摘要说明
    /// </summary>
[WebService(Namespace = "http://www.petrochina.com.cn/QMYS/ERP/service/FetchMaterialMasterDataV1.0/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class QMYSService : System.Web.Services.WebService
{

    public QMYSService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }

    public SOAP_ENV MonitorHeader = new SOAP_ENV();

    public string HelloWorld(ref string a,ref string b)
    {
        a = "失败";
        b = "数据库数据已存在";
        return "Hello World";
    }

    [System.Web.Services.Protocols.SoapHeader("MonitorHeader")]
    [WebMethod]
    public Boolean intiSO_fetchMaterialData(ref RETURNREC.MaterialREC[] T_MATREC, ref string FLAG, ref string INFO)
    {
        MonitorHeader.TransID = "CQS-QMY-2017080911253528770720";
        bool boo = false;
        
        try
        {
            string tableName = "TB_" + T_MATREC.GetType().GetElementType().Name.ToUpper();
            newTask task = new newTask(T_MATREC, "IM_BEGDA", "intiSO_fetchMaterialData", tableName, T_MATREC);
            Thread thread = new Thread(new ThreadStart(task.run));
            thread.Start();

            //List<object> list = T_MATREC.ToList<object>();
            //list.Sort(new MaterialTime("IM_BEGDA"));

            FLAG = "S";//操作成功！！
            boo = true;
        }
        catch (Exception e)
        {
            FLAG = "E";//操作失败！！
            INFO = e.Message;
        }
        
        return boo;
    }

    [WebMethod]
    public Boolean intiSO_postBSEGData(ref RETURNREC.BESGREC[] T_POSTBSEGREC, ref string FLAG, ref string INFO)
    {
        MonitorHeader.TransID = "CQS-QMY-2017080911253528770720";
        bool boo = false;

        try
        {
            string tableName = "TB_" + T_POSTBSEGREC.GetType().GetElementType().Name.ToUpper();
            newTask task = new newTask(T_POSTBSEGREC, "BUDAT", "intiSO_postBSEGData", tableName, T_POSTBSEGREC);
            Thread thread = new Thread(new ThreadStart(task.run));
            thread.Start();
            FLAG = "S";//操作成功！！
            boo = true;
        }
        catch (Exception e)
        {
            FLAG = "E";//操作失败！！
            INFO = e.Message;
        }

        return boo;
    }


    public class MaterialTime : IComparer<object>
    {
        public string colName;
        public MaterialTime(string name)
        {
            colName = name;
        }

        //按具体属性排序
        public int Compare(object x, object y)
        {
            return getValue(x).CompareTo(getValue(y));
        }

        public string getValue(object obj) {
            Type type = obj.GetType();  //获取类型
            PropertyInfo propertyInfo = type.GetProperty(colName); //获取指定名称的属性
            string colVals = propertyInfo.GetValue(obj, null).ToString(); //获取属性值
            return colVals;
        }
    }


 


    /// <summary>
    /// 多线程任务类
    /// </summary>
   public class newTask { 
        private object obj { get; set; }
        private string colName { get; set; }
        private string funname { get; set; }
        private string tablename { get; set; }
        private object[] o { get; set; }

        public newTask(object obj, string colName, string funname, string tablename, object[] o)
        {
            this.obj = obj;
            this.colName = colName;
            this.funname = funname;
            this.tablename = tablename;
            this.o = o;
        }

        public void run()
        {
            ThreadMethod(obj, colName, funname, tablename,o);
        }

        private void ThreadMethod(object obj, string colName, string funname, string tablename,object[] o)
        {
            DateTime dt = DateTime.Now;
            //1、按时间字段，将返回结果分组
            Dictionary<string, List<object>> result = REFLECT.getListByKey(obj, colName.ToString());

            //2、将查询结果集拼接成insert语句
            var insertSqls = REFLECT.createInsertSql(result, "", dt);

            //3、排序获取最小时间和最大时间
            List<object> list = o.ToList<object>();
            list.Sort(new MaterialTime(colName));
            string minTime = REFLECT.getValue(list[0], colName);
            string maxTime = REFLECT.getValue(list[list.Count - 1], colName);
            Dictionary<string, string> times = new Dictionary<string, string>();
            times.Add("minTime", minTime);
            times.Add("maxTime", maxTime);

            //4、装载配置文件的创建表和删除表数据的sql语句，先执行create，再执行delete,最后执行insert
            result = REFLECT.createTable(funname.ToString(), colName.ToString(), obj, insertSqls, times);
            foreach (string key in result.Keys)
            {
                REFLECT.ExecuteSqlTran(result[key]);
            }

            //5、执行汇总存储过程
            REFLECT.doProcedure(tablename, colName, minTime, maxTime);
        }
    }

}