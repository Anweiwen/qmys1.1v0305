﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Collections;
using RMYH.DAL;

/// <summary>
///jsonExcel ,对.json 文件的功能性封装
/// </summary>
public class jsonExcel
{
	public jsonExcel()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    /// <summary>
    /// 根据路径打开对应的.json 文件,生成动态类
    /// add by liguosheng
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static dynamic getJsonString(string path)
    {
        byte[] buffer = null;
        try {
            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
            }
            string data = Encoding.UTF8.GetString(buffer);
            return JsonConvert.DeserializeObject<dynamic>(data);
        }catch(Exception e){
            throw e;
        }
    }

    /// <summary>
    /// 写.json文件
    /// </summary>
    /// <param name="pathName"></param>
    /// <param name="Json"></param>
    public static void setJson(string pathName,string Json)
    {
        byte[] myByte = System.Text.Encoding.UTF8.GetBytes(Json);

        try {
            if (File.Exists(pathName))
            {
                //FileMode.Truncate 打开现有文件，清除其内容。流指向文件的开头，保留文件的初始创建日期
                using (FileStream fsWrite = new FileStream(pathName, FileMode.Truncate))
                {
                    fsWrite.Write(myByte, 0, myByte.Length);
                }
            }
        }catch(Exception e){
            throw e;
        }
    }

    /// <summary>
    /// 获取所有sheet页
    /// </summary>
    /// <param name="MBDM"></param>
    /// <param name="json"></param>
    /// <returns></returns>
    public static List<string> getSheetNames(string MBDM, dynamic json)
    {
        List<string> list = new List<string>();
        //遍历sheets页
        foreach (var itemSheet in json.sheets)
        {
            //获取sheet页名称
            string sheetName = itemSheet.Name;
            list.Add(sheetName);
        }
        return list;
    }

    ///// <summary>
    ///// 创建sheet页
    ///// </summary>
    ///// <param name="MBDM"></param>
    ///// <param name="json"></param>
    ///// <param name="SheetName">要创建的sheet页名称</param>
    ///// <returns></returns>
    //public static string CreateSheet(dynamic json,string[] SheetName)
    //{
    //    string sheets = "\"Sheet3\":{\"name\":\"Sheet3\",\"rowCount\":20,\"columnCount\":10,\"activeRow\":13,\"activeCol\":5,\"theme\":\"Office\",\"data\":{\"defaultDataNode\":{\"style\":{\"themeFont\":\"Body\"}},\"dataTable\":{}},\"rowHeaderData\":{\"defaultDataNode\":{\"style\":{\"themeFont\":\"Body\"}}},\"colHeaderData\":{\"defaultDataNode\":{\"style\":{\"themeFont\":\"Body\"}}},\"selections\":{\"0\":{\"row\":13,\"rowCount\":1,\"col\":5,\"colCount\":1},\"length\":1},\"index\":1}";
    //    return "";
    //}

    /// <summary>
    /// 设置sheet页保护|不保护,显示|隐藏
    /// </summary>
    /// <param name="json">json格式的动态类</param>
    /// <param name="Params">字典参数，key:sheet名称（例如：Sheet1），value:显示或保护参数（例如：isProtected:false,visible:true）</param>
    public static string SetSheetProtect(dynamic json,Dictionary<string,string> Params)
    {
        //遍历参数param
        foreach (var item in Params)
        {
            string key = item.Key;
            string Value=item.Value;
            string[] newParam = Value.Split(',');
            for (int i = 0; i < newParam.Length; i++)
            {
               //shetet页属性：isProtected 或 visible
               string Property=newParam[i].Split(':')[0];
               string value = newParam[i].Split(':')[1];

               foreach (var itemSheet in json.sheets)
               {
                   //获取sheet页名称
                   if (key.Equals(itemSheet.Name))
                   {
                       //获取sheet页内容
                       dynamic content = itemSheet.Value;

                       if (Property.Equals("isProtected")) content.isProtected = Convert.ToBoolean(value);
                       if (Property.Equals("visible")) content.visible = Convert.ToBoolean(value);
                       break;
                   }
               }
            }
        }
        return JsonConvert.SerializeObject(json);
    }

    /// <summary>
    /// 获取单元格值、公式、样式等
    /// JObject   用于操作JSON对象，简单来说就是生成”{}”
    /// JArray    用语操作JSON数组，也就是”[]”
    /// JValue    表示数组中的值
    /// JProperty 表示对象中的属性,以"key/value"形式。例如： A:a
    /// JToken    用于存放Linq to JSON查询后的结果
    /// 
    /// 目前已知SpreadJs 单元格属性有：value（值）,formula（公式）,style（样式）,tag（标签，spreadjs独有，excel无此属性）
    /// 
    /// </summary>
    /// <param name="json">dynamic 动态类</param>
    public static Dictionary<string, object> getCellContent(string MBDM, dynamic json)
    { 
        Dictionary<string, object> cells = new Dictionary<string, object>();

        Dictionary<string, dynamic> valueItem = new Dictionary<string, dynamic>();
        Dictionary<string, dynamic> formulaItem = new Dictionary<string, dynamic>();
        Dictionary<string, dynamic> styleItem = new Dictionary<string, dynamic>();
        Dictionary<string, dynamic> tagItem = new Dictionary<string, dynamic>();

        //namedStyles样式字典
        Dictionary<string, dynamic> namedStyles = new Dictionary<string, dynamic>();
        foreach (var itemSheet in json.namedStyles)
        {
            namedStyles.Add(itemSheet.name.Value, itemSheet);
        }
        //遍历sheets页
        foreach (var itemSheet in json.sheets)
        {
            //获取sheet页名称
            string sheetName = itemSheet.Name;
            //获取sheet页内容
            dynamic content = itemSheet.Value;
            //获取sheet页行列数
            int rowCount = content.rowCount != null ? content.rowCount : 0;
            int colCount = content.columnCount != null ? content.columnCount : 0;
            //获取sheet页数据
            dynamic dataTable = itemSheet.Value.data.dataTable;

            //遍历行数据
            foreach (var rowData in dataTable)
            {
                //行坐标
                string rIndex = rowData.Name;
                dynamic col = rowData.Value;

                //遍历列数据
                foreach (var colData in col)
                {
                    //列坐标
                    string cIndex = colData.Name;
                    dynamic cell = colData.Value;

                    //单元格key格式：MBDM_SHEETNAME_rIndex_cIndex
                    string key = MBDM + "_" + sheetName + "_" + rIndex + "_" + cIndex;

                    dynamic value = cell["value"];
                    dynamic formula = cell["formula"];
                    dynamic style = cell["style"];
                    dynamic tag = cell["tag"];

                    if (value != null)
                    {
                        if (value is Newtonsoft.Json.Linq.JObject)
                        {
                            //单元格有错误，显示这个值
                            value = value["_calcError"].Value;
                        }
                    }
        
                    if (style != null)
                    {
                        if (style is Newtonsoft.Json.Linq.JValue)
                        {
                            style = namedStyles[style.Value];
                        }
                    }

                    valueItem.Add(key, value);
                    formulaItem.Add(key, formula);
                    styleItem.Add(key, style);
                    tagItem.Add(key, tag);
                }
            }
        }
        cells.Add("value", valueItem);
        cells.Add("formula", formulaItem);
        cells.Add("style", styleItem);
        cells.Add("tag", tagItem);

        return cells;
    }


}