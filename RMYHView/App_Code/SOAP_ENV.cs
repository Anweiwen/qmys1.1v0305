﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///SOAP_ENV 的摘要说明
/// </summary>
public class SOAP_ENV : System.Web.Services.Protocols.SoapHeader
{
		public string TransID{ get; set; }
        public string TransTime{ get; set; }
        public string BizKey{ get; set; }
        public string Caller{ get; set; }

}