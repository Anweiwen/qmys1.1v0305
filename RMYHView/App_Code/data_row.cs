﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 长庆石化 MES1.0 数据类
/// MesData 的摘要说明
/// </summary>
[Serializable]
public class data_row
{
    public string rq { get; set; }
    public string dw { get; set; }
    public string dwmc { get; set; }
    public string bb { get; set; }
    public string bbmc { get; set; }
    public string zb { get; set; }
    public string zbmc { get; set; }
    public string jldw { get; set; }
    public string zt { get; set; }
    public string ztmc { get; set; }
    public string bdata { get; set; }
    public string zdata { get; set; }

    public data_row(string rq, string dw, string dwmc, string bb, string bbmc, string zb, string zbmc, string jldw, string zt, string ztmc, string bdata, string zdata)
    {
        this.rq = rq;
        this.dw = dw;
        this.dwmc = dwmc;
        this.bb = bb;
        this.bbmc = bbmc;
        this.zb = zb;
        this.zbmc = zbmc;
        this.jldw = jldw;
        this.zt = zt;
        this.ztmc = ztmc;
        this.bdata = bdata;
        this.zdata = bdata;
    }
}