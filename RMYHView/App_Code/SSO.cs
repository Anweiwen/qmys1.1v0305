﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Xml.Serialization;


/// <summary>
///AccountDTO 的摘要说明
///账户同步的数据JSON对应的Java POJO
///此处翻译成C#代码
/// </summary>
[Serializable]
public class AccountDTO
{
    // --------------- 必要信息 START------------------
    /**
     * 状态：创建 CREATE，删除账户 DELETE，更新 UPSERT
     */
    public string status { get; set; }
    /**
     * 账户名
     */
    public string accountName { get; set; }
    /**
      * 账户是否被禁用
      */
    public bool enabled { get; set; }
    /**
     * 账户映射属性
     */
    public Dictionary<string, string> mappingAttr { get; set; }

    /**
     * 账户扩展属性
     */
    public Dictionary<string, string> extAttr { get; set; }
    /**
     * 账户所属的组织机构
     */
    public string[] orgList { get; set; }
    // --------------- 必要信息 END------------------


    // 以下三个信息是对应身份认证中的属性，如果不集成关键操作保护，则不需要
    /**
     * 身份认证中的用户名
     */
    public string username { get; set; }
    /**
     * 身份认证中的用户ID
     */
    public string masterId { get; set; }
    /**
     * 身份认证中的应用代码
     */
    public string appCode { get; set; }
    /**
     * 用户的角色代码列表，只有在细粒度权限控制时需要
     */
    public List<string> roleCodes { get; set; }

}


/// <summary>
///OrgDTO 的摘要说明
///组织机构同步的数据JSON对应的Java POJO
///此处翻译成C#代码
/// </summary>
[Serializable]
public class OrgDTO
{
    /**
     * 同步状态：DELETE组织机构被删除，UPSERT组织机构更新或添加
     */
    public string status { get; set; }
    /**
     * 组织机构代码
     */
    public string code { get; set; }
    /**
     * 新组织机构代码，为空则表示组织机构代码未改变
     */
    public string newCode { get; set; }
    /**
     * 组织机构父代码
     */
    public string parentCode { get; set; }
    /**
     * 组织机构名称
     */
    public string name { get; set; }
    /**
     * 组织机构扩展属性
     */
    public Dictionary<string, string> extAttr { get; set; }
    /**
     * 所属应用
     */
    public string owner { get; set; }

}

[Serializable]
public class Org
{
    /**
    *  组织机构代码
    */
    public string code { get; set; }
    /**
     * 组织机构
     */
    public Dictionary<string, string> orgext { get; set; }

}


/// <summary>
///SSO 单点登录类，包含单点登录的公用方法
/// </summary>
public class SSO
{
    public SSO()
    {
        //
        //TODO: 在此处添加构造函数逻辑
        //
    }

    /// <summary>
    /// 身份认证的单点登录
    /// </summary>
    /// <param name="appid">身份认证系统提供的参数</param>
    /// <param name="appkey">身份认证系统提供的参数</param>
    /// <param name="code">身份认证系统返回的参数</param>
    /// <returns></returns>
    public static string Sso(string appid, string appkey, string code)
    {
        string token = "", userinfo = "";
        //1、从身份认证系统申请获得appid，appkey,这2个参数固定如下：
        //2、开发应用系统oauth认证URL，获得由身份认证系统传来的code参数。【从页面接收到传递过来的code参数，Request.QueryString已经接收】
        //3、向身份认证系统发送https请求获得token参数
        string tokenurl = "https://rfiam.cnpc/ngiam-rst/oauth2/token?appcode=" + appid + "&secret=" + appkey + "&code=" + code;

        string accesstoken = SSO.GetOrPostRequest("GET", tokenurl, new Dictionary<string, string>());

        //身份认证系统传过来的accesstoken 实例如下：
        //string accesstoken = "{\"accessToken\":\"f026b010180a3d9e7e12ed52e1da6293\",\"expire\":3600000}"; 
        if (accesstoken.IndexOf("accessToken") > -1)
        {
            token = accesstoken.Substring(accesstoken.IndexOf("\"accessToken\":\"") + 15, accesstoken.IndexOf("\",\"expire\":") - accesstoken.IndexOf("\"accessToken\":\"") - 15);
        }

        //4、向身份认证系统发送https请求获得userinfo参数
        string userinfourl = "https://rfiam.cnpc/ngiam-rst/oauth2/userinfo?appcode=" + appid + "&secret=" + appkey + "&token=" + token;

        string accountName = SSO.GetOrPostRequest("GET", userinfourl, new Dictionary<string, string>());

        //string accountName = "{\"accountAttrs\":{},\"accountName\":\"cqshlocal\",\"mappingAttrs\":{\"displayName\":\"superadmin\",\"mobile\":null,\"email\":\"superadmin@163.com\",\"username\":\"superadmin\"},\"userid\":\"599d083be4b049d2432e9710\"}";
        //5、并获得身份认证的用户信息，其中accountName为账号名
        if (accountName.IndexOf("\"accountName\":") > -1)
        {
            userinfo = accountName.Substring(accountName.IndexOf("\"accountName\":\"") + 15, accountName.IndexOf("\",\"mappingAttrs\":") - accountName.IndexOf("\"accountName\":\"") - 15);
        }

        return userinfo;
    }


    public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
        //直接确认，否则打不开
        return true;
    }

    /// <summary>
    /// get 或 post 请求
    /// </summary>
    /// <param name="req"></param>
    /// <param name="url"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static string GetOrPostRequest(string req, string url, Dictionary<string, string> parameters)
    {
        string res = "";
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
        request.ProtocolVersion = HttpVersion.Version11;
        request.Method = req;
        //System.Net.CredentialCache.DefaultCredentials 获取应用程序的系统凭据。
        request.Credentials = CredentialCache.DefaultCredentials;
        request.Timeout = 10000;
        //request.UserAgent = DefaultUserAgent;

        //如果需要POST数据
        if (!(parameters == null || parameters.Count == 0) && req.Equals("GET"))
        {
            StringBuilder buffer = new StringBuilder();
            int i = 0;
            foreach (string key in parameters.Keys)
            {
                if (i > 0)
                {
                    buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                }
                else
                {
                    buffer.AppendFormat("{0}={1}", key, parameters[key]);
                }
                i++;
            }
            Encoding encoding = Encoding.GetEncoding("utf-8");
            byte[] data = encoding.GetBytes(buffer.ToString());
            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
        }

        try
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string encoding = response.ContentEncoding;
            if (encoding == null || encoding.Length < 1)
            {
                encoding = "UTF-8"; //默认编码
            }
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(encoding));
            res = reader.ReadToEnd();
            response.Close();
        }
        catch (Exception e)
        {
            return e.ToString();
        }

        return res;
    }


}