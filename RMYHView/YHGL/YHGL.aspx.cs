﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using RMYH.BLL.CDQX;
using RMYH.DAL;
using System.Text;
using RMYH.Model;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class YHGL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YHGL));

    }

    #region Ajax方法

    /// <summary>
    /// 修改和删除数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string saveGroup(string edit,string id, string name)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (edit == "add") {
            string addSql = "insert into TB_GROUP (GROUP_ID,GROUP_NAME) values ('" + id + "','"+name+"') ";
            DataSet sa=BLL.Query(addSql);
        }

        if (edit == "update") {
            string upSql = "update  TB_GROUP set GROUP_NAME='"+name+"' where GROUP_ID='"+id+"'";
            BLL.Query(upSql);
        }

        if (edit == "del")
        {
            string delSql = "delete from TB_GROUP where GROUP_ID='" + id + "'";
            BLL.Query(delSql);
        }
        return null;

    }

    /// <summary>
    /// 同时更新TB_USER和TB_USERGROUP表
    /// </summary>
    /// <param name="edit"></param>
    /// <param name="id"></param>
    /// <param name="name"></param>
    /// <param name="phone"></param>
    /// <param name="pwd"></param>
    /// <param name="hszx"></param>
    /// <param name="login"></param>
    /// <param name="groupId"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string saveUser(string edit, string id,string name,string phone,string pwd,string hszx,string login,string zydm,string groupId)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        ArrayList List = new ArrayList();
        List<String> ArrList = new List<String>();
        string res = "";
        if (edit == "add")
        {
            string sqlgroup = "insert into TB_USERGROUP (USER_ID,GROUP_ID) values (@USER_ID,@GROUP_ID) ";
            string addsql = "insert into TB_USER (USER_ID,USER_NAME,PHONE,USER_PSW,CC_NO,USER_LOGIN,ZYDM) values (@USER_ID,@USER_NAME,@PHONE,@USER_PSW,@CC_NO,@USER_LOGIN,@ZYDM)";
            ArrList.Add(sqlgroup);
            ArrList.Add(addsql);
            AseParameter[] NewGroup = {
                    new AseParameter("@USER_ID", AseDbType.VarChar,40),
                    new AseParameter("@GROUP_ID", AseDbType.VarChar,4),
                };
            NewGroup[0].Value = id;
            NewGroup[1].Value = groupId;

            AseParameter[] NewUser = {
                    new AseParameter("@USER_ID", AseDbType.VarChar,40),
                    new AseParameter("@USER_NAME", AseDbType.VarChar,20),
                    new AseParameter("@PHONE", AseDbType.VarChar,40),
                    new AseParameter("@USER_PSW", AseDbType.VarChar,20),
                    new AseParameter("@CC_NO", AseDbType.VarChar,40),
                    new AseParameter("@USER_LOGIN", AseDbType.VarChar,40),
                    new AseParameter("@ZYDM", AseDbType.VarChar,40),
                };

            NewUser[0].Value = id;
            NewUser[1].Value = name;
            NewUser[2].Value = phone;
            NewUser[3].Value = pwd;
            NewUser[4].Value = hszx;
            NewUser[5].Value = id;
            NewUser[6].Value = zydm;

            List.Add(NewGroup);
            List.Add(NewUser);
            res = BLL.ExecuteSql(ArrList.ToArray(), List).ToString();
        }

        if (edit == "update")
        {
            string upsql = "update TB_USER set USER_NAME='" + name + "',PHONE='" + phone + "',USER_PSW='" + pwd + "',CC_NO='" + hszx + "',USER_LOGIN='" + login + "',ZYDM='" + zydm + "' where USER_ID='" + id + "'";
            BLL.Query(upsql);
            res = "1";
        }

        if (edit == "del")
        {
            string delUserSql = "delete from TB_USER where USER_ID=@USER_ID";
            string delGroupSql = "delete from TB_USERGROUP where USER_ID=@USER_ID";
            ArrList.Add(delUserSql);
            ArrList.Add(delGroupSql);
            AseParameter[] DelGroup = {
                    new AseParameter("@USER_ID", AseDbType.VarChar,40),
                };
            DelGroup[0].Value = id;

            AseParameter[] DelUser = {
                    new AseParameter("@USER_ID", AseDbType.VarChar,40),
                };

            DelUser[0].Value = id;

            List.Add(DelGroup);
            List.Add(DelUser);
            res = BLL.ExecuteSql(ArrList.ToArray(), List).ToString();
        }
        return res;

    }

    /// <summary>
    /// 更新用户所属的组
    /// </summary>
    /// <param name="userid"></param>
    /// <param name="groupid"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpGroup(string userid,string groupid)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "update TB_USERGROUP set GROUP_ID='" + groupid + "' where USER_ID='" + userid + "'";
        BLL.Query(sql);
        return null;
    }


    /// <summary>
    /// 查询用户组里ID与添加的ID是否有重复
    /// </summary>
    /// <param name="table"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getId(string table, string id)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "";
        if (table == "group") { sql = "select GROUP_ID from TB_GROUP where GROUP_ID='" + id + "'"; }
        if (table == "user") { sql = "select USER_ID from TB_USER where USER_ID='" + id + "'"; }
        DataSet da=BLL.Query(sql);
        if (da.Tables[0].Rows.Count > 0) {
            return "1"; //库里有已有记录返回1，否则返回0
        }
        return "0";
    }


    /// <summary>
    /// 查询用户里ID，或LoginName与新添加的是否有重复
    /// </summary>
    /// <param name="table"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getIdOrLogin(string table, string id, string Name,string LoginName)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "";
        if (table == "user") { sql = "select USER_ID from TB_USER where (USER_NAME='" + Name + "' or USER_LOGIN='" + LoginName + "') and USER_ID!='" + id + "'"; }
        DataSet da = BLL.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            return "1"; //库里有已有记录返回1，否则返回0
        }
        return "0";
    }



    /// <summary>
    /// 查询新加的用户组组名与库里的是否有重复
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getGroupName(string name)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "select GROUP_NAME from TB_GROUP where GROUP_NAME='" + name + "'"; 
        DataSet da = BLL.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            return "1"; //库里有已有记录返回1，否则返回0
        }
        return "0";
    }


    /// <summary>
    /// 查询用户
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetAllUser(string groupId)
    {
        StringBuilder Str = new StringBuilder();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //string sql = "SELECT U.USER_ID,U.USER_LOGIN,U.USER_NAME,U.PHONE,U.USER_PSW,D.HSZXMC FROM TB_USER U,TB_HSZXZD D where U.CC_NO =D.HSZXDM and D.YY='" + HttpContext.Current.Session["YY"].ToString() + "'";
        string sql = "SELECT U.USER_ID,U.USER_LOGIN,U.USER_NAME,U.PHONE,U.USER_PSW,D.HSZXMC,L.ZYDM,L.ZYMC FROM TB_USER U inner join TB_HSZXZD D on U.CC_NO =D.HSZXDM and D.YY='" + HttpContext.Current.Session["YY"].ToString() + "' left join TB_JHZYML L on L.ZYDM=U.ZYDM";
        string sqlByGroup = "select A.USER_ID,A.USER_LOGIN,A.USER_NAME,A.PHONE,A.USER_PSW,D.HSZXMC,L.ZYDM,L.ZYMC FROM TB_USER A inner join TB_USERGROUP B on A.USER_ID=B.USER_ID inner join TB_GROUP C on C.GROUP_ID=B.GROUP_ID and C.GROUP_ID='" + groupId + "' inner join TB_HSZXZD D on A.CC_NO =D.HSZXDM and D.YY='" + HttpContext.Current.Session["YY"].ToString() + "' left join TB_JHZYML L on A.ZYDM=L.ZYDM";
        string count = "select count(*) from TB_USER";
        DataSet num = BLL.Query(count);
        DataSet da =new DataSet();
        if (groupId!="") {
            da = BLL.Query(sqlByGroup);
        }
        else
        {
           da = BLL.Query(sql);
        }
        
        Str.Append("{\"total\":");
        Str.Append(num.Tables[0].Rows[0][0].ToString());
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            Str.Append("\"USER_ID\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_ID"].ToString() + "\"" + ",");
            Str.Append("\"USER_LOGIN\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_LOGIN"].ToString() + "\"" + ",");
            Str.Append("\"USER_NAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_NAME"].ToString() + "\"" + ",");
            Str.Append("\"PHONE\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["PHONE"].ToString() + "\"" + ",");
            Str.Append("\"USER_PSW\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_PSW"].ToString() + "\"" + ",");
            Str.Append("\"USER_PSW_RE\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["USER_PSW"].ToString()+ "\"" + ",");
            Str.Append("\"HSZXMC\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["HSZXMC"].ToString() + "\"" + ",");
            Str.Append("\"ZYDM\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ZYDM"].ToString() + "\"" + ",");
            Str.Append("\"ZYMC\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ZYMC"].ToString() + "\"");
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");

        return Str.ToString();

    }

    [AjaxPro.AjaxMethod]
    public string GetZyml() 
    {
        string sql = "select ZYDM,ZYMC,FBDM from TB_JHZYML";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da=BLL.Query(sql);
        StringBuilder result = new StringBuilder();
        string res = GetTree("0", da, ref result);
        return res; 
    }

    /// <summary>
    /// combotree下拉树控件的json字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    public string GetTree(string id, DataSet da, ref StringBuilder sb)
    {
        // StringBuilder sb=new StringBuilder();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataRow[] dr = da.Tables[0].Select("FBDM='" + id + "'");
        if (dr.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (sb.ToString() == "")
            {
                sb.Append("[");
            }
            else
            {
                sb.Append(",");
                sb.Append("\"children\": [");
            }
            for (int i = 0; i < dr.Length; i++)
            {
                DataRow dt = dr[i];
                sb.Append("{");
                sb.Append("\"id\":\"" + dt["ZYDM"].ToString() + "\"");
                sb.Append(",");
                sb.Append("\"text\":\"" + dt["ZYMC"].ToString().Trim() + "\"");
                sb.Append(",");

                DataRow[] dd = da.Tables[0].Select("FBDM='" + dt["ZYDM"].ToString() + "'");

                if (dd.Length > 0)
                {
                    sb.Append("\"state\":\"closed\"");

                }
                else
                {
                    sb.Append("\"state\":\"open\"");
                }

                //递归查询子节点
                GetTree(dt["ZYDM"].ToString(), da, ref sb);
                if (i < dr.Length - 1)
                {
                    sb.Append("},");
                }
                else
                {
                    sb.Append("}]");
                }
            }
        }
        return sb.ToString();
    }




    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson()
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(ref Str);
        return Str.ToString();
    }
    private void RecursionChild(ref StringBuilder Str)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //SELECT XMDM,XMBM,PAR,XMMC,CCJB,XMMCEX FROM TB_XMXX WHERE PAR='ParID'
        string sql = "SELECT GROUP_ID,GROUP_NAME FROM TB_GROUP";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);

        if (Str.ToString() == "")
        {
            Str.Append("[{");
            Str.Append("\"id\":\"000\"");
            Str.Append(",");
            Str.Append("\"text\":\"全部部门\"");
            Str.Append(",");
            Str.Append("\"children\": [{");
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count;i++ ) {
                    Str.Append("\"id\":\"" + DS.Tables[0].Rows[i][0].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"text\":\"" + DS.Tables[0].Rows[i][1].ToString() + "\"");
                    if (i < DS.Tables[0].Rows.Count - 1) { Str.Append("},{"); }   
                }
                Str.Append("}]");
            }
            else
            {
                Str.Append("}]");
            }
            Str.Append("}]");
        }

    }


    /// <summary>
    /// combox下拉控件的json字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetHszx()
    {
        StringBuilder Str = new StringBuilder();
       TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
       string sql = "select HSZXDM,HSZXBM,HSZXMC from TB_HSZXZD where YY='"+HttpContext.Current.Session["YY"].ToString()+"'";
       DataSet da = new DataSet();
       da = BLL.Query(sql);
       if (da.Tables[0].Rows.Count > 0) {
           Str.Append("[");
           Str.Append("{");
           Str.Append("\"id\":\"" + "0000" + "\"");
           Str.Append(",");
           Str.Append("\"text\":\"" + "无核算中心" + "\"");
           Str.Append("}");

           for (int i=0; i < da.Tables[0].Rows.Count; i++) {
               Str.Append(",");
               Str.Append("{");
               Str.Append("\"id\":\"" + da.Tables[0].Rows[i]["HSZXDM"].ToString() + "\"");
               Str.Append(",");
               Str.Append("\"text\":\"" + da.Tables[0].Rows[i]["HSZXMC"].ToString() + "\"");
               Str.Append("}");
               //if (i < da.Tables[0].Rows.Count-1)
               //{
               //    Str.Append(",");
               //}
           }
           Str.Append("]");
       }
       return Str.ToString();
    }


    ////查询用户表里最大的ID
    //[AjaxPro.AjaxMethod]
    //public string GetMaxId(string table) {
    //    string maxid = "";
    //    TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();//max(GROUP_ID)
    //    string sqlGroup = "select max(GROUP_ID) GROUP_ID from TB_GROUP ";
    //    string sqlUser = "select max(USER_ID) USER_ID from TB_USER where CC_NO in (select HSZXDM from TB_HSZXZD where YY='"+HttpContext.Current.Session["YY"].ToString()+"')";
    //    DataSet da = new DataSet();

    //    if (table == "GROUP") {
    //        da = BLL.Query(sqlGroup);
    //    } else {
    //        da = BLL.Query(sqlUser);
    //    }

    //    if (da.Tables[0].Rows.Count > 0)
    //    {
    //        if (table == "GROUP") { maxid = (int.Parse(da.Tables[0].Rows[0][0].ToString()) + 1).ToString().PadLeft(4, '0'); }
    //        if (table == "USER") { maxid = (int.Parse(da.Tables[0].Rows[0][0].ToString()) + 1).ToString().PadLeft(4, '0'); }
    //    }
    //    else {
    //        if (table == "GROUP") { maxid = "01"; }
    //        if (table == "USER") { maxid = "0001"; }
    //    }

    //    return maxid;
    //}

    ////查询用户表里最大的ID
    //[AjaxPro.AjaxMethod]
    //public string GetMaxUserId()
    //{
    //    string maxid = "";
    //    TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();//max(GROUP_ID)
    //    string sql = "select max(GROUP_ID) GROUP_ID from TB_USER ";
    //    DataSet da = BLL.Query(sql);
    //    if (da.Tables[0].Rows.Count > 0)
    //    {
    //        maxid = (int.Parse(da.Tables[0].Rows[0][0].ToString()) + 1).ToString().PadLeft(4, '0');
    //    }
    //    else
    //    {
    //        maxid = "0001";
    //    }
    //    return maxid;
    //}
  

    #endregion;
}