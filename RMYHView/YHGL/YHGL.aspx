﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="YHGL.aspx.cs" Inherits="YHGL" Title="YHGL"%>
<meta http-equiv=”X-UA-Compatible” content=”IE=edge,chrome=1″/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>无标题页</title>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/style1.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/style.css"/>
    <script src="../JS/jquery.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../JS/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/Main.js" type="text/javascript"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var paramGroup = "";
        var paramUser = "";
        var group_id = "";
        var group_name = "";
        var param_hszx;
        var maxid;
        var TreeGroupId = "";
        var ZYML = "";
        var row_zydm=""

        function updateGroup(info) {
            switch (info) {
                case "add":
                    if ($("#GROUP_ID").attr('disabled') != null) {
                        $("#GROUP_ID").removeAttr("disabled");
                    }
                    paramGroup = "add";
                    //maxid = YHGL.GetMaxId("GROUP").value;
                    $('#dialogGroup').window('open');
                    $('#GROUP_ID').val("");
                    $('#GROUP_NAME').val("");
                    break;
                case "update":
                    $("#GROUP_ID").attr('disabled', 'disabled');
                    paramGroup = "update";
                    var node = $('#tt').tree('getSelected');
                    if (node == null) {
                        tishi("请先选择要修改的组！！");
                        return;
                    }
                    $('#dialogGroup').window('open');
                    $('#GROUP_ID').val(group_id);
                    $('#GROUP_NAME').val(group_name);
                    //$('#GROUP_ID').attr('readonly', true);
                    break;
                case "del":
                    saveGroup("del");
                    break;
                default:

            }
        }

        //初始化dialog组建
        function initDialog(name, title, wid, hg, con) {
            $('#' + name).dialog({
                title: title,
                width: wid,
                height: hg,
                closed: true,
                cache: false,
                buttons: [{
                    text: '保存',
                    iconCls: 'icon-ok',
                    handler: function () {
                        if (name == "dialogGroup") {
                            saveGroup(paramGroup);
                        }
                        if (name == "dialogUser") {
                            saveUser(paramUser);
                        }
                        if (name == "GroupTree") {
                            SaveUpGroup();
                        }
                        //$('#' + name).dialog('close');
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function () {
                        $('#' + name).dialog('close');
                    }
                }],
                content: con

            });
        }

        //增删改用户组
        function saveGroup(edit) {
            switch (edit) {
                case "del":
                    if (group_id == "") {
                        tishi("请选择要删除的组！！！！");
                        return;
                    }
                    //删除用户组之前先判断用户组中是否有用户，有则提示：“先删除用户，再删除组”，没有用户，则删除该用户组
                    var res = YHGL.GetAllUser(group_id).value;
                    var len = eval("(" + res + ")").rows.length; //判断返回的json格式数据中是否有用户数据
                    if (len != 0) {
                        tishi("该用户组中存在用户，请先删除用户！");
                        return;
                    }
                    $.messager.confirm('警告', '确定要删除该组?', function (r) {
                        if (r) {
                            var node = $('#tt').tree('find', group_id);
                            $('#tt').tree('remove', node.target);
                            YHGL.saveGroup(edit, group_id, '');
                            group_id = ""; //在回调函数中清空全局变量值，不然会在回调函数执行前先改变全局变量值
                        }
                    });
                    break;
                case "update":
                    if (trim($('#GROUP_NAME').val()) == "") {
                        tishi("名称不能为空！！！！");
                        $('#GROUP_NAME').val("");
                        return;
                    }
                    var resName = YHGL.getGroupName($('#GROUP_NAME').val()).value; //判断输入的用户名在库里是否存在
                    if (resName == "1") {
                        tishi("用户组名称不能重复，请重新输入。");
                        return;
                    }
                    YHGL.saveGroup(edit, $('#GROUP_ID').val(), $('#GROUP_NAME').val());
                    jsonTree();
                    $('#dialogGroup').window('close');
                    break;
                case "add":
                    if (trim($('#GROUP_ID').val()) == "" || trim($('#GROUP_NAME').val()) == "") {
                        tishi("ID和名称不能为空！！！！");
                        $('#GROUP_ID').val("");
                        $('#GROUP_NAME').val("");
                        return;
                    }
                    //$('#userGroup').window('open');
                    var resId = YHGL.getId("group", $('#GROUP_ID').val()).value; //判断输入的用户组编号在库里是否存在
                    var resName = YHGL.getGroupName($('#GROUP_NAME').val()).value; //判断输入的用户名在库里是否存在
                    if (resId == "1") {
                        tishi("用户组编号已存在，请重新输入。");
                        return;
                    }
                    if (resName == "1") {
                        tishi("用户组名称不能重复，请重新输入。");
                        return;
                    }
                    YHGL.saveGroup(edit, $('#GROUP_ID').val(), $('#GROUP_NAME').val());
                    $('#dialogGroup').window('close');
                    group_id = "";
                    jsonTree();
                    break;
                default:

            }

        }

        //根据组查询用户
        function allUser(groupId) {
            if (groupId == "000") {
                groupId = "";
            }
            var res = YHGL.GetAllUser(groupId).value;
            var Data = eval("(" + res + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);

        }
        //左侧用户组树
        function jsonTree() {
            var rtn = YHGL.GetChildJson();
            $('#tt').tree({
                lines: true,
                onBeforeExpand: function (node) {
                },
                onExpand: function (node) {
                },
                onCollapse: function (node) {

                },
                onClick: function (node) {
                    group_id = node.id;
                    group_name = node.text;
                    node.id == '000' ? allUser("") : allUser(node.id);
                }

            });
            var Data = eval("(" + rtn.value + ")");
            $("#tt").tree("loadData", Data);
        }

        function tishi(rtn) {
            $.messager.show({
                title: '温馨提示',
                msg: rtn,
                timeout: 1000,
                showType: 'slide'
            });
        }

        function editUser(flag) {
            switch (flag) {
                case "add":
                    if (group_id == "" || group_id == "000") {
                        tishi("请先选择具体的用户组");
                        return;
                    }
                    $("#USER_LOGIN").val("");
                    $("#USER_NAME").val("");
                    $("#PHONE").val("");
                    $("#USER_PSW").val("");
                    $("#USER_PSW_RE").val("");
                    $('#dialogUser').window('open');
                    paramUser = flag;
                    break;
                case "update":
                    var row = $('#dg').datagrid('getSelected');
                    row_zydm = row.ZYDM;
                    if (row == null) {
                        tishi("请先选择用户！！");
                        return;
                    }
                    $("#USER_LOGIN").val(row.USER_LOGIN);
                    $("#USER_NAME").val(row.USER_NAME);
                    $("#PHONE").val(row.PHONE == " " ? "" : row.PHONE);
                    $("#USER_PSW").val(row.USER_PSW);
                    $("#USER_PSW_RE").val(row.USER_PSW_RE);
                    $("#CC_NO").combobox('setText', row.HSZXMC);
                    $("#ZYMC").combotree('setText', row.ZYMC);
                    $("#ZYMC").combotree('setValue', row.ZYDM);
                    $('#dialogUser').window('open');
                    paramUser = flag;
                    break;
                case "del":
                    saveUser("del");
                default:

            }
        }

        //改组
        function upGroup() {
            var row = $('#dg').datagrid('getSelected');
            if (row == null) {
                tishi("请先选择用户！！");
                return;
            }
            $('#GroupTree').window('open');
            var rtn = YHGL.GetChildJson();
            $('#tt1').tree({
                lines: true,
                onBeforeExpand: function (node) {
                },
                onExpand: function (node) {
                },
                onCollapse: function (node) {

                },
                onClick: function (node) {
                    TreeGroupId = node.id;
                }

            });
            var Data = eval("(" + rtn.value + ")");
            $("#tt1").tree("loadData", Data);
        }

        function SaveUpGroup() {
            if (TreeGroupId == "") {
                tishi("请选择要转移到的用户组！！");
                return;
            }
            var row = $('#dg').datagrid('getSelected');
            var userid = row.USER_ID;
            var res = YHGL.UpGroup(userid, TreeGroupId).value;
            $('#GroupTree').window('close');
            allUser(group_id);
            TreeGroupId = "";
        }

        function initGrid() {
            var IsCheckFlag = true; 
            $('#dg').datagrid({
                width: function () {
                    return document.body.clientWidth
                },
                height: 450,
                nowrap: true,
                rownumbers: true,
                fit: true,
                animate: false,
                fitColumns: true,
                collapsible: true,
                lines: true,
                maximizable: true,
                maximized: true,
                singleSelect: true,
//                checkOnSelect: false,
//                selectOnCheck: true,
                frozenColumns: [[{
                    field: 'ck',
                    checkbox: true
                }]],
                pageSize: 10, // 默认选择的分页是每页5行数据
                pageList: [10, 20, 30, 40], // 可以选择的分页集合
                pagination: true, // 分页
                rownumbers: true, // 行数
                toolbar: [{}, '-', {
                    text: '增加',
                    iconCls: 'icon-add',
                    handler: function () {
                        editUser("add");
                    }
                }, '-', {
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        editUser("update");
                    }
                }, '-', {
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        editUser("del");
                    }
                }, '-', {
                    text: '改组',
                    iconCls: 'icon-tip',
                    handler: function () {
                        upGroup();
                    }
                }],
                columns: [[
                    {
                        field: 'USER_ID',
                        title: '用户编号',
                        width: 40,
                        align: 'center',
                        hidden: true
                    },
                    {
                        field: 'USER_LOGIN',
                        title: '用户编号',
                        width: 40,
                        align: 'center',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'USER_NAME',
                        title: '用户名称',
                        width: 40,
                        align: 'center',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'PHONE',
                        title: '电话',
                        width: 40,
                        align: 'center',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'USER_PSW',
                        title: '密码',
                        width: 40,
                        align: 'center',
                        formatter: function (value, row, index) {
                            return "******";
                        }
                    },
                    {
                        field: 'USER_PSW_RE',
                        title: '确认密码',
                        width: 40,
                        align: 'center',
                        formatter: function (value, row, index) {
                            return "******";
                        }
                    },
                    {
                        field: 'HSZXMC',
                        title: '核算中心',
                        width: 60,
                        align: 'center',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                     {
                        field: 'ZYDM',
                        title: '作业代码',
                        width: 40,
                        align: 'center',
                        hidden: true
                    },
                    {
                        field: 'ZYMC',
                        title: '作业名称',
                        width: 60,
                        align: 'center',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    }
                ]],

                onAfterEdit: function (rowIndex, rowData, changes) {
                    var inserted = $('#dg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                    var updated = $('#dg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                    if (inserted.length > 0) {//执行增加方法
                        addData = inserted;
                    }
                    if (updated.length > 0) {//执行修改方法
                        upData = updated;
                    }
                    acbs = 1;
                    $('#dg').datagrid('unselectAll');
                },

                onDblClickRow: function (rowIndex, rowData) {
                    $('#dg').datagrid('beginEdit', rowIndex);
                },
                onClickCell: function (rowIndex, field, value) {
                    IsCheckFlag = false;
                },
                onSelect: function (rowIndex, rowData) {
                    if (!IsCheckFlag) {
                        IsCheckFlag = true;
                        $("#dg").datagrid("unselectRow", rowIndex);
                    }
                },
                onUnselect: function (rowIndex, rowData) {
                    if (!IsCheckFlag) {
                        IsCheckFlag = true;
                        $("#dg").datagrid("selectRow", rowIndex);
                    }
                }
            });

        }

        //下拉框的核算中心
        function hszx() {
            var res = YHGL.GetHszx().value;
            var Data = eval("(" + res + ")");
            param_hszx = Data;
            $("#CC_NO").combobox("loadData", Data);
        }

        //easyui前端分页
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }

        //增删改用户
        function saveUser(edit) {
            //判断validatebox是否验证通过
            var isValidUserLogin = $("#USER_LOGIN").validatebox("isValid");
            if (!isValidUserLogin) {
                $("#USER_LOGIN").val("");
            }
            var isValidName = $("#USER_NAME").validatebox("isValid");
            if (!isValidName) {
                $("#USER_NAME").val("");
            }
            var isValidPsw = $("#USER_PSW").validatebox("isValid");
            if (!isValidPsw) {
                $("#USER_PSW").val("");
            }
            var isValidPhswre = $("#USER_PSW_RE").validatebox("isValid");
            if (!isValidPhswre) {
                $("#USER_PSW_RE").val("");
            }
            var isValidPhone = $("#PHONE").validatebox("isValid");
            if (!isValidPhone) {
                $("#PHONE").val("");
            }

            //for循环判断combox的方法getValue获取的值为json的text的时候，将其值赋值为相应的id
            for (var i = 0; i < param_hszx.length; i++) { 
                if ($("#CC_NO").combobox('getText') == param_hszx[i].text) {
                    $("#CC_NO").combobox('setValue',param_hszx[i].id);
                }
            }
            var Hszx = $("#CC_NO").combobox('getValue');

            var Zydm = $("#ZYMC").combotree('getValue');

            if (edit != "del") {
                if (isValidUserLogin && isValidName && isValidPsw && isValidPhswre && isValidPhone && Hszx != "" && Zydm != "") {

                } else {
                    tishi("验证失败，无法保存，请完善用户信息！");
                    $('#user').dialog('open');
                    return;
                }
            }

            switch (edit) {
                case "del":
                    var row = $('#dg').datagrid('getSelected');
                    if (row == null) {
                        tishi("请选择要删除的用户！！！！");
                        return;
                    }
                    $.messager.confirm('警告', '确定要删除该用户?', function (r) {
                        if (r) {
                            var res = YHGL.saveUser(edit, row.USER_ID, '', '', '', '', '', '').value;
                            if (res == "1") {
                                tishi("操作成功!");
                                allUser(group_id);
                            } else {
                                tishi("操作失败!");
                                return;
                            }
                        }
                    });
                    break;
                case "add":
                    var res = YHGL.getId("user", $('#USER_ID').val()).value; //判断输入的用户ID在库里是否存在
                    if (res == "1") {
                        tishi("输入的【用户编号】已存在，请重新输入！！！！");
                        return;
                    }
                    if (trim($('#USER_LOGIN').val()) == "" || trim($('#USER_NAME').val()) == "" || Hszx == "") {
                        tishi("用户编号,用户名称和核算中心不能为空，请重新输入！！！！");
                        return;
                    }
                    var result = YHGL.saveUser(edit, $('#USER_LOGIN').val(), $('#USER_NAME').val(), $('#PHONE').val(), $('#USER_PSW').val(), Hszx, $('#USER_LOGIN').val(), $("#ZYMC").combotree('getValue'), group_id).value;
                    if (result == "1") {
                        tishi("操作成功!");
                    } else {
                        tishi("操作失败!");
                        return;
                    }
                    $('#dialogUser').window('close');
                    allUser(group_id);
                    break;
                case "update":
                    var row = $('#dg').datagrid('getSelected');
                    var res = YHGL.getIdOrLogin("user", row.USER_ID, $('#USER_NAME').val(), $('#USER_LOGIN').val()).value; //判断输入的用户ID在库里是否存在
                    if (res == "1") {
                        tishi("输入的【用户编号】或【用户名称】已存在，请重新输入！！！！");
                        return;
                    }
                    var result = YHGL.saveUser(edit, row.USER_ID, $('#USER_NAME').val(), $('#PHONE').val(), $('#USER_PSW').val(), Hszx, $('#USER_LOGIN').val(), Zydm, group_id).value;
                    if (result == "1") {
                        tishi("操作成功!");
                    } else {
                        tishi("操作失败!");
                        return;
                    }
                    $('#dialogUser').window('close');
                    allUser(group_id);
                    break;
                default:

            }
            paramUser = "";
        }

        //两次密码框输入是否一致
        $.extend($.fn.validatebox.defaults.rules, {
            equals: {
                validator: function (value, param) {
                    return value == $(param[0]).val();
                },
                message: '两次密码输入不一致！！！！！！！！！！.'
            }
        });

        //自定义验证电话号码
        $.extend($.fn.validatebox.defaults.rules, {
            phoneRex: {
                validator: function (value) {
                    var rex = /^1[3-8]+\d{9}$/;
                    //var rex=/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
                    //区号：前面一个0，后面跟2-3位数字 ： 0\d{2,3}
                    //电话号码：7-8位数字： \d{7,8
                    //分机号：一般都是3位数字： \d{3,}
                    //这样连接起来就是验证电话的正则表达式了：/^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/		 
                    var rex2 = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
                    if (rex.test(value) || rex2.test(value)) {
                        // alert('t'+value);
                        return true;
                    } else {
                        //alert('false '+value);
                        return false;
                    }

                },
                message: '请输入正确电话或手机格式'
            }
        });
        //删除左右两端的空格
        function trim(str) { 
            return str.replace(/(^\s*)|(\s*$)/g, "");
        }

        function zyml() {
            var res = YHGL.GetZyml().value;
            ZYML = eval("(" + res + ")");
            $("#ZYMC").combotree("loadData", ZYML);     
        }

        $(function () {
            jsonTree();
            hszx();
            zyml();
            initDialog('dialogGroup', '用户组设置', 400, 150, $('#userGroup'));
            initDialog('dialogUser', '用户设置', 500, 180, $('#user'));
            initDialog('GroupTree', '用户改组', 300, 300, $('#tt1'));
            initGrid();
            allUser("");
            //禁用验证，focus后启用验证
            $('input.easyui-validatebox').validatebox('disableValidation')
            .focus(function () { $(this).validatebox('enableValidation'); })
            .blur(function () { $(this).validatebox('validate') });
        });
    </script>
</head>
<body>
<form id="form1" runat="server" style="width:100%; height:100%;">
    <div class="easyui-layout" style="width:100%;height:100%;fit:true">
        <div id="p" data-options="region:'west',tools:'#west-tools'" title="用户组" style="width:15%;padding:10px">
            <ul id="tt" class="easyui-tree"></ul>
            <ul id="tt1" class="easyui-tree"></ul>
            <div id="west-tools">
                <a href="#" class="icon-add" onclick="javascript:updateGroup('add')"></a>
                <a href="#" class="icon-edit" onclick="javascript:updateGroup('update')"></a>
                <a href="#" class="icon-remove" onclick="javascript:updateGroup('del')"></a>
            </div>
        </div>
        <div data-options="region:'center'" title="用户管理">

            <input type="hidden" id="hidindexid" value="ZYDM"/>
            <input type="hidden" id="hidcheckid"/>
            <input type="hidden" id="hideeasyui"/>
            <input type="hidden" id="hidNewLine"/>
            <input type="hidden" id="HidTrID"/>

            <div id="dialogGroup"></div>
            <div id="dialogUser"></div>
            <div id="GroupTree"></div>

            <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
                   data-options="fitColumns:true,singleSelect:true,pagination:true,pageSize :5,pageList : [ 5, 10, 15, 20 ],idField: 'USER_ID'"></table>

            <div id="userGroup">
                <table>
                    <tr>
                        <td>用户组编号:</td>
                        <td><input id="GROUP_ID" type="text" class="easyui-validatebox" data-options="required:true"/>
                        </td>
                    </tr>
                    <tr>
                        <td>用户组名称:</td>
                        <td><input id="GROUP_NAME" type="text" class="easyui-validatebox" data-options="required:true"/>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="user">
                <table>
                    <tr>
                        <th>用户编号:</th>
                        <th><input id="USER_LOGIN" type="text" class="easyui-validatebox" data-options="required:true"/>
                        </th>
                        <th>用户名称:</th>
                        <th><input id="USER_NAME" type="text" class="easyui-validatebox" data-options="required:true"/>
                        </th>
                    </tr>
                    <tr>
                        <th>密码:</th>
                        <th><input id="USER_PSW" type="password" name="USER_PSW" class="easyui-validatebox"
                                   data-options="required:true"/></th>
                        <th>确认密码:</th>
                        <th><input id="USER_PSW_RE" type="password" name="USER_PSW_RE" class="easyui-validatebox"
                                   required="required" validType="equals['#USER_PSW']"/></th>
                    </tr>
                    <tr>
                        <th>电话号码:</th>
                        <th><input id="PHONE" type="text" class="easyui-validatebox"
                                   data-options="validType:'phoneRex'"/></th>
                        <th>核算中心:</th>
                        <th><input id="CC_NO" class="easyui-combobox"
                                   data-options="valueField:'id',textField:'text',panelHeight:'auto'" editable="false"/>
                        </th>
                    </tr>
                     <tr>
                        <th>作业名称:</th>
                        <th><input id="ZYMC" type="text" class="easyui-combotree"
                                   data-options="valueField:'id',textField:'text',required:true" editable="false"/></th>
                        <th></th>
                        <th></th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
</body>
</html>


<%--    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    </asp:Content>

    <asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">            
   
    </asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    </asp:Content>--%>