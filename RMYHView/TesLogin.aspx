﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TesLogin.aspx.cs" Inherits="TesLogin" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>全面预算分析系统</title>
    <link href="CSS/style1.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="JS/jquery-1.3.1.min.js"></script>
    <style type="text/css">

            .button11{border:1px #93b9dc solid;background:url(Images/button_bg.gif) repeat;padding:0px 6px; cursor:pointer; }
    </style>
</head>
<body  style="overflow:auto;height:auto">
    <form id="form1" runat="server">
        <img alt="" src="Images/login.jpg" id="divMain" style="position:absolute;z-index:2;left:0px;top:0px;background-image:url(images/login.jpg);height:640px;width:1067px;" />
        <div id="divS" style="text-align:left;width:400px;position:absolute;z-index:3;left:650px;top:160px">
            <div style="color:White; font-size: 16px; font-family: 宋体;">
                <asp:Label ID="Label1" runat="server" Text="用户名：" BorderStyle="None" 
                    Width="65px"></asp:Label>
                <input type="text" id="txtuid" style="width:160px" runat="server"/>
            </div>
            <div style="color:White; font-size: 16px;font-family: 宋体;">
                <asp:Label ID="Label2" runat="server" Text="密 码 ：" BorderStyle="None" 
                    Height="16px" Width="65px"></asp:Label>
                <input type="password" id="txtpwd" style="width:160px" runat="server" />
            </div>
            <div style="color:White; font-size: 16px;font-family: 宋体;">
                <asp:Label ID="Label3" runat="server" Text="年 份 ：" BorderStyle="None" 
                    Width="65px"></asp:Label>
                <select id="DDLYear" style="width: 160px"></select>
            </div>
            <div style="color:White; font-size: 16px; font-family: 宋体;">
            <asp:Label ID="Label4" runat="server" Text="数据库：" BorderStyle="None" 
                    Width="65px"></asp:Label>
               <input type="text" id="TxtDataBase" style="width:160px" runat="server"/></div>
            <div style="font-size: 16px;font-family: 宋体;text-align:center;width:230px">
            <asp:Button runat="server" ID="bytton1" Text="登&nbsp;&nbsp;&nbsp;&nbsp;录" CssClass="button11" Width="70px" OnClientClick="return checkisnull();" OnClick="Text1_ServerClick" />
            <input type="hidden" runat="server" id="hidyy" />
            </div>
        </div>  
    </form>
    <script type="text/javascript">
        function checkisnull() {
        if($("#txtuid").val()=="")
        {
           alert("用户名不能为空！")
            return false;
        }
//        if($("#txtpwd").val()=="")
//        {
//            alert("密码不能为空！")
//            return false;
//        }
        if ($("#TxtDataBase").val() == "") 
        {
            alert("数据库名称不能为空！");
            return false;
        }
        $("#hidyy").val($("#DDLYear").val());
        
        return true;
    }
    function setYYs() 
    {
        var s = document.getElementById("DDLYear");
        var ss = TesLogin.GetDDLYear().value;
        var value1 = ss.split('+');
        s.options.length = 0;
        for (var i = 0; i < value1.length; i++) {
            var value2 = value1[i].split(',');
            s[i] = new Option(value2[1], value2[0]);
            if (s[i].value == $("#<%=hidyy.ClientID %>").val())
                s[i].selected = true;
        }
    }
    function setSize() {
        var x = (document.body.clientWidth - 1067) / 2;
        var y = (document.body.clientHeight - 640) / 2;
        $("#divMain").css("top", (y > 0 ? y : 0) + "px");
        $("#divMain").css("left", (x > 0 ? x : 0) + "px");
        $("#divS").css("top", ((y > 0 ? y : 0) + 160) + "px");
        $("#divS").css("left", ((x > 0 ? x : 0) + 650) + "px");

    }
    window.onresize = function () { setSize(); };
    $(document).ready(function () {
        setYYs();
        setSize();
    })
    </script>
</body>
</html>