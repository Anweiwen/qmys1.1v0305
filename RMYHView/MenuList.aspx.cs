﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using RMYH.BLL;
using RMYH.BLL.CDQX;
using System.Text;
using RMYH.Model;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class MenuList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(MenuList));
    }
    #region Ajax方法
    /// <summary>
    /// 添加菜单节点
    /// </summary>
    /// <param name="ischild">是否为子节点</param>
    /// <param name="id">被选中节点的主键代码</param>
    /// <param name="xmmc"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int AddData(bool ischild, string id, string xmmc)
    {
        int Flag = 0,BM;
        string PARMENUID = "",sql="",MENUID = "",ChirldMENUID="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        BBFXXT_BBCDQXBLL BB = new BBFXXT_BBCDQXBLL();
        StringBuilder strSql = new StringBuilder();
        //获取当前预算分析最大的菜单代码
        int FUNCID = GetMENUID();

        //判断当前是添加同级还是添加子节点（true代表添加子节点；false代表添加同级）
        if (ischild)
        {
            //获取父节点的菜单编码
            MENUID = GetMENUIDByFUNCID(int.Parse(id));
            ChirldMENUID=GetMAXMENUIDByMENUID(MENUID);
            if (ChirldMENUID != "" && ChirldMENUID != "-1")
            {
                BM = int.Parse(ChirldMENUID.Substring(MENUID.Length)) + 1;
                ChirldMENUID = MENUID + BM.ToString().PadLeft(2, '0');
            }
            else
            {
                ChirldMENUID = MENUID + "01";
            }
        }
        else
        {
            if (id != "")
            {
                MENUID = GetMENUIDByFUNCID(int.Parse(id));             
                if (MENUID.Length == 2)
                {
                    //选择根节点下添加同级节点
                    ChirldMENUID = GetTJMENUID(MENUID);
                    BM = int.Parse(ChirldMENUID) + 1;
                    ChirldMENUID = BM.ToString().PadLeft(2, '0');
                }
                else
                {
                    //截取当前被选中节点的父节点
                    PARMENUID = MENUID.Substring(0, MENUID.Length - 2);
                    ChirldMENUID = GetMAXMENUIDByMENUID(PARMENUID);
                    BM = int.Parse(ChirldMENUID.Substring(MENUID.Length-2)) + 1;
                    ChirldMENUID = PARMENUID + BM.ToString().PadLeft(2, '0');
                }
            }
            else
            {   
                //添加同级时不选择节点，则默认添加到根节点下（01只是代表根节点是两位，没有任何意义）
                ChirldMENUID = GetTJMENUID("01");
                if (ChirldMENUID != "" && ChirldMENUID != "-1")
                {
                    BM = int.Parse(ChirldMENUID) + 1;
                    ChirldMENUID = BM.ToString().PadLeft(2, '0');
                }
                else
                {
                    ChirldMENUID = "01";
                }
            }
        }
        //插入项目分类明细相关信息
        sql = "INSERT INTO SYS_MENU ( FUNCID, MENUID, MENUCAPTION, CLASSNAME, IMAGENAME, CAPTIONDESCINFO, PARAMSTR, LOADTYPE, FORMSTAT, DLLNAME, ISVISIBLE, ACTIONRIGHT) VALUES ("+ (FUNCID + 1)+", '"+ChirldMENUID+"', '', '', '-1', '', '', '', '', '', 'Y', 'YSFX')";
        Flag = bll.ExecuteSql(sql) ;
        return Flag;
    }

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return GetDataListstring("10144458", "MENUID", new string[] { }, new string[] { }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace("@DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", "MENUID");
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", "MENUID");
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) style=\"width:50px\" class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=Images/Tree/white.gif /><img src=Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 100) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='180px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="DelStr">删除字符串</param>
    /// <param name="TableName">表名</param>
    /// <param name="MBID">模板ID</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UpdateORDel(string UpStr, string DelStr)
    {
        int Flag = 0;
        string ID = "", XMMC = "", DataValue = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        if (UpStr != "" || DelStr != "")
        {
            //更新记录
            if (UpStr != "")
            {
                string[] Col = UpStr.Split('|');
                for (int i = 0; i < Col.Length; i++)
                {

                    string[] ZD = Col[i].Split(',');
                    ID = ZD[0].ToString();
                    XMMC = ZD[1].ToString();
                    DataValue = ZD[2].ToString();
                    string UPSql = "UPDATE SYS_MENU SET MENUCAPTION=@MENUCAPTION,DLLNAME=@DLLNAME WHERE FUNCID=@FUNCID AND ACTIONRIGHT='YSFX'";
                    ArrList.Add(UPSql);
                    AseParameter[] Data = {
                      new AseParameter("@FUNCID", AseDbType.Integer, 8),
                      new AseParameter("@MENUCAPTION", AseDbType.VarChar, 50),
                      new AseParameter("@DLLNAME", AseDbType.VarChar,100)                 
                    };
                    Data[0].Value = int.Parse(ID);
                    Data[1].Value = XMMC;
                    Data[2].Value = DataValue;
                    List.Add(Data);
                }
            }
            //删除记录
            if (DelStr != "")
            {
                string [] CD;
                string KeyStr="";
                string[] Del = DelStr.Split('|');
                for (int i = 0; i < Del.Length; i++)
                {
                    CD=Del[i].Split(',');
                    KeyStr = KeyStr == "" ? CD[0].ToString() : KeyStr + "," + CD[0].ToString();
                    //递归获取其子节点
                    GetDelPrimaryKey(CD[1].ToString(),ref KeyStr);     
                }  
                string DelSql = "DELETE FROM SYS_MENU WHERE FUNCID IN(" + KeyStr + ") AND ACTIONRIGHT='YSFX' AND 1=@BJ";
                ArrList.Add(DelSql);
                AseParameter[] D = 
                {
                    new AseParameter("@BJ",AseDbType.Integer,4)
                };
                D[0].Value = 1;
                List.Add(D);
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        else
        {
            Flag = -1;
        }
        return Flag;
    }
    /// <summary>
    /// 判断被选中节点是否有子节点
    /// </summary>
    /// <param name="FUNCID">节点代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsHavingChird(string FUNCID)
    {
        bool Flag = false;
        //根据菜单代码获取菜单编码
        string MENUID=GetMENUIDByFUNCID(int.Parse(FUNCID));
        //获取最大的父节点的菜单编码
        string ChirldMENUID=GetMAXMENUIDByMENUID(MENUID);
        Flag = ChirldMENUID == "" ? true : (ChirldMENUID == "-1" ? false : true);
        return Flag;
    }
    /// <summary>
    /// 获取当前最大的菜单代码
    /// </summary>
    /// <returns></returns>
    public int GetMENUID()
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "SELECT ISNULL(MAX(FUNCID),-1) FUNCID FROM SYS_MENU WHERE ACTIONRIGHT='YSFX'";
        return int.Parse(BLL.Query(sql).Tables[0].Rows[0]["FUNCID"].ToString());
    }
    /// <summary>
    /// 根据菜单代码获取菜单编码
    /// </summary>
    /// <param name="FUNCID">菜单代码</param>
    /// <returns></returns>
    public string GetMENUIDByFUNCID(int FUNCID)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "SELECT MENUID FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND FUNCID=" + FUNCID;
        DS=BLL.Query(sql);
        return DS.Tables[0].Rows.Count > 0 ? DS.Tables[0].Rows[0]["MENUID"].ToString() : "";
    }
    /// <summary>
    /// 根据菜单编码获取最大的子节点菜单编码
    /// </summary>
    /// <param name="MENUID">父节点菜单编码</param>
    /// <returns></returns>
    public string GetMAXMENUIDByMENUID(string MENUID)
    {
        int LEN = MENUID.Length+2;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "SELECT ISNULL(MAX(MENUID),'-1') MENUID FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND MENUID LIKE '"+MENUID+"%' AND MENUID<>'"+MENUID+"' AND DATALENGTH(MENUID)="+LEN;
        DS = BLL.Query(sql);
        return DS.Tables[0].Rows.Count > 0 ? DS.Tables[0].Rows[0]["MENUID"].ToString() : "";
    }
    /// <summary>
    /// 根据菜单编码获取最大的子节点菜单编码
    /// </summary>
    /// <param name="MENUID">父节点菜单编码</param>
    /// <returns></returns>
    public string GetTJMENUID(string MENUID)
    {
        int LEN = MENUID.Length;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "SELECT ISNULL(MAX(MENUID),'-1') MENUID FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND DATALENGTH(MENUID)=" + LEN;
        DS = BLL.Query(sql);
        return DS.Tables[0].Rows.Count > 0 ? DS.Tables[0].Rows[0]["MENUID"].ToString() : "";
    }
    /// <summary>
    /// 获取Key下所有的子节点
    /// </summary>
    /// <param name="Key">菜单代码</param>
    /// <param name="KeyStr">存储删除代码字符串</param>
    [AjaxPro.AjaxMethod]
    public void GetDelPrimaryKey(string Key, ref string KeyStr)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT FUNCID,MENUID FROM SYS_MENU WHERE ACTIONRIGHT='YSFX' AND SUBSTRING(MENUID,1,DATALENGTH(MENUID)-2)='" + Key + "'";
        DS.Tables.Clear();
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                KeyStr = KeyStr == "" ? DS.Tables[0].Rows[i]["FUNCID"].ToString() : KeyStr + "," + DS.Tables[0].Rows[i]["FUNCID"].ToString();
                //遍历子节点
                GetDelPrimaryKey(DS.Tables[0].Rows[i]["MENUID"].ToString(), ref KeyStr);
            }
        }
    }
    #endregion;
}