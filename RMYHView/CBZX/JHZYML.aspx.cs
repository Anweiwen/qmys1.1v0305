﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using RMYH.BLL.CDQX;
using RMYH.DAL;
using System.Text;
using RMYH.Model;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class JHZYML : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(JHZYML));

    }

    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string[] LoadList(string trid, string id, string intimgcount, string HSZXDM,string YY)
    {
        return GetDataListstring("10145652", "ZYDM", new string[] { ":HSZXDM", ":YY",":ZYMC" }, new string[] { HttpContext.Current.Session["HSZXDM"].ToString(), HttpContext.Current.Session["YY"].ToString(),""}, false, trid, id, intimgcount);
    }


    /// <summary>
    /// 添加数据行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddNewData(int Len, string nbbm,string zydm, string zybm, string zymc, string fbdm, string ccjb, bool flag, string maxbm,string isTree)
    {
        string newrow = NewLine(Len.ToString(), nbbm, zydm, zybm, zymc, fbdm, ccjb, flag, maxbm, isTree);
        return newrow;
    }


    public string NewLine(string strarr, string nbbm, string zydm, string zybm, string zymc, string fbdm, string ccjb, bool flag, string maxbm, string isTree)
    {
        int intimgcount = int.Parse(strarr);
        strarr = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        for (int i = 0; i < intimgcount; i++)
        {
            strarr += ",0";
        }

        if (flag) //添加下级
        {

        }
        else//添加同级
        {
            if (zybm == null && isTree == "no") //zybm == null 表明是直接点击的添加同级按钮，则默认添加最顶层的同级
            {
                zybm = "";
                
            }
            else if (zybm != null && isTree == "yes")//isTree == "yes" 表明是点击树之后生成的行，参数由树的node属性传递过来
            {
                zybm = "";
                ccjb = (int.Parse(ccjb) + 1).ToString();
                fbdm = zydm;
            }
            else if (zybm != null && isTree == "no")
            {
                zybm = "";
            }

        }

        return getNewLine("10145652", false, "", strarr, nbbm, zydm, zybm, zymc, fbdm, ccjb, flag, maxbm, isTree);
    }

    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, string strarr, string nbbm, string zydm, string zybm, string zymc, string fbdm, string ccjb, bool flag, string maxbm, string isTree)
    {
        try
        {
            string fbbh = null; string bh = null;
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            string strfistimage = "";
            string[] arr = strarr.Split(',');
            if (flag)
            {  //添加下级
                fbbh = zybm;
                bh = GetStringPrimaryKey("2");
                //for (int i = 1; i < arr.Length; i++)
                //{
                //    strfistimage += "<img src=../Images/Tree/white.gif />";
                //}
                for (int i = 0; i < int.Parse(ccjb) + 1; i++)
                {
                    strfistimage += "<img src=../Images/Tree/white.gif />";
                }
            }
            else
            {//添加同级
                if (zybm != "")
                {
                    fbbh = zybm.Length == 4 ? zybm : zybm.Substring(0, zybm.Length - 4);
                }
                else
                {
                    fbbh = "";
                }
                bh = GetStringPrimaryKey("2");
                for (int i = 1; i < arr.Length - 1; i++)
                {
                    strfistimage += "<img src=../Images/Tree/white.gif />";
                }
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                        {

                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");

                        }
                        else
                        {
                            if (flag)
                            {//添加下级
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "");
                                switch (ds.Tables[0].Rows[j]["FIELDNAME"].ToString())//输入控件
                                {
                                    case "ZYDM"://作业代号
                                        sb.Append(bh);
                                        break;
                                    case "ZYNBBM"://作业内部编码
                                        sb.Append(GetZynbbm("2",1,nbbm,HttpContext.Current.Session["HSZXDM"].ToString(),flag,isTree));
                                        break;
                                    case "FBDM"://父辈代号
                                        sb.Append(zydm.ToString());
                                        break;
                                    case "CCJB"://层次级别
                                        sb.Append(int.Parse(ccjb) + 1);
                                        break;
                                    default:
                                        break;
                                }
                                sb.Append("</td>");

                            }
                            else
                            {//添加同级
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "");
                                switch (ds.Tables[0].Rows[j]["FIELDNAME"].ToString())//输入控件
                                {
                                    case "ZYDM"://作业代号
                                        sb.Append(bh);
                                        break;
                                    case "ZYNBBM"://作业内部编码
                                        sb.Append(GetZynbbm("2", 1, nbbm, HttpContext.Current.Session["HSZXDM"].ToString(), flag,isTree));
                                        break;
                                    case "FBDM"://父辈代号
                                        sb.Append(fbdm);
                                        break;
                                    case "CCJB"://层次级别
                                        sb.Append(ccjb);
                                        break;
                                    default:
                                        break;
                                }
                                sb.Append("</td>");

                            }

                        }
                    }
                    else
                    {
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " align='left' title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + ">");
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            sb.Append("</td>");

                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");

                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "ZYBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" +  (Convert.ToDecimal(bh) - 1).ToString()+ "\"");
                                    }
                                    else
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\"");
                                    }

                                    sb.Append(">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    //sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    //if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                    //    sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    //sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    sb.Append("<div name=" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "><input name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " class='easyui-combotree' data-options='onShowPanel:tt' style='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'/></div>");  

                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }


    /// <summary>
    /// 修改和删除数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string[] UpdateORDel(string NewStr, string UpStr, string DelStr)
    {
        int Flag = 0;
        string addData = ""; string editData = ""; string delData = "";
        string HSZXDM = "", ZYDM = "", ZYNBBM = "", ZYBM = "", ZYMC = "", ZYBZBM = "", SYBZ = "", ZYBS = "", CCJB = "", YJDBZ = "", WLPHBZ = "", FBDM = "", DWCBJSFS = "", C_ORDER = "", ISBZZY = "", CWCBZXDM = "", CWBMDM = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();

        StringBuilder newdata = new StringBuilder();
        StringBuilder updata = new StringBuilder();
        StringBuilder deledata = new StringBuilder();
        StringBuilder upsql = new StringBuilder();

        if (UpStr != "" || DelStr != "" || NewStr != "")
        {
            //新增记录
            if (NewStr.Length != 0)
            {
                string[] Col = NewStr.Split(',');
                newdata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
                    ZYDM = ZD[1].ToString();
                    ZYNBBM = ZD[2].ToString();
                    ZYBM = ZD[3].ToString();
                    ZYMC = ZD[4].ToString();
                    ZYBZBM = ZD[5].ToString();
                    SYBZ = ZD[6].ToString();
                    ZYBS = ZD[7].ToString();
                    CCJB = ZD[8].ToString();
                    YJDBZ = ZD[9].ToString();
                    WLPHBZ = ZD[10].ToString();
                    FBDM = ZD[11].ToString();
                    DWCBJSFS = ZD[12].ToString();
                    C_ORDER = ZD[13].ToString();
                    ISBZZY = ZD[14].ToString();
                    CWCBZXDM = ZD[15].ToString();
                    CWBMDM = ZD[16].ToString();
                    string NewSql = "INSERT INTO TB_JHZYML (HSZXDM,ZYDM,ZYNBBM,ZYBM,ZYMC,ZYBZBM,SYBZ,ZYBS,CCJB,YJDBZ,WLPHBZ,FBDM,DWCBJSFS,C_ORDER,ISBZZY,CWCBZXDM,CWBMDM)VALUES(@HSZXDM,@ZYDM,@ZYNBBM,@ZYBM,@ZYMC,@ZYBZBM,@SYBZ,@ZYBS,@CCJB,@YJDBZ,@WLPHBZ,@FBDM,@DWCBJSFS,@C_ORDER,@ISBZZY,@CWCBZXDM,@CWBMDM)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@HSZXDM", AseDbType.VarChar,40),
                    new AseParameter("@ZYDM", AseDbType.VarChar,40),
                    new AseParameter("@ZYNBBM", AseDbType.VarChar,100),
                    new AseParameter("@ZYBM", AseDbType.VarChar,3),
                    new AseParameter("@ZYMC", AseDbType.VarChar,60), 
                    new AseParameter("@ZYBZBM", AseDbType.VarChar,40),
                    new AseParameter("@SYBZ", AseDbType.VarChar,3),
                    new AseParameter("@ZYBS", AseDbType.Integer),
                    new AseParameter("@CCJB", AseDbType.Integer),
                    new AseParameter("@YJDBZ", AseDbType.VarChar,3),
                    new AseParameter("@WLPHBZ", AseDbType.VarChar,3),
                    new AseParameter("@FBDM", AseDbType.VarChar,40),
                    new AseParameter("@DWCBJSFS", AseDbType.VarChar,2),
                    new AseParameter("@C_ORDER", AseDbType.VarChar,2),
                    new AseParameter("@ISBZZY", AseDbType.Char,2),
                    new AseParameter("@CWCBZXDM", AseDbType.VarChar,40),
                    new AseParameter("@CWBMDM", AseDbType.VarChar,40),
                };
                    New[0].Value = HSZXDM;
                    New[1].Value = ZYDM;
                    New[2].Value = ZYNBBM;
                    New[3].Value = ZYBM;
                    New[4].Value = ZYMC;
                    New[5].Value = ZYBZBM;
                    New[6].Value = SYBZ;
                    New[7].Value = ZYBS;
                    New[8].Value = CCJB;
                    New[9].Value = YJDBZ;
                    New[10].Value = WLPHBZ;
                    New[11].Value = FBDM;
                    New[12].Value = DWCBJSFS;
                    New[13].Value = C_ORDER;
                    New[14].Value = ISBZZY;
                    New[15].Value = CWCBZXDM;
                    New[16].Value = CWBMDM;
                    List.Add(New);

                    //将要添加到数据库里的内容，以json的格式返回到前端的easyui tree树的相应节点下，以实现添加之后表格显示数据和左侧树结构的同步
                    newdata.Append("{");
                    newdata.Append("\"id\":" + "\"" + New[1].Value + "\"" + ",");
                    newdata.Append("\"text\":" + "\"" + New[4].Value + "\"" + ",");
                    newdata.Append("\"attributes\":" + "{");
                    newdata.Append("\"hszxdm\":" + "\"" + New[0].Value + "\"" + ",");
                    newdata.Append("\"zybm\":" + "\"" + New[3].Value + "\"" + ",");
                    newdata.Append("\"ccjb\":" + "\"" + New[8].Value + "\"" + ",");
                    newdata.Append("\"yjdbz\":" + "\"" + New[9].Value + "\"" + ",");
                    newdata.Append("\"fbdm\":" + "\"" + New[11].Value + "\"" + ",");
                    newdata.Append("\"zybs\":" + "\"" + New[7].Value + "\"" + ",");
                    newdata.Append("\"sybz\":" + "\"" + New[6].Value + "\"" + ",");
                    newdata.Append("\"wlphbz\":" + "\"" + New[10].Value + "\"" + ",");
                    newdata.Append("\"c_order\":" + "\"" + New[13].Value + "\"" + ",");
                    newdata.Append("\"yy\":" + "\"" + "" + "\"" + ",");
                    newdata.Append("\"zynbbm\":" + "\"" + New[2].Value + "\"" + ",");
                    newdata.Append("\"dwcbjsfs\":" + "\"" + New[12].Value + "\"" + ",");
                    newdata.Append("\"isbzzy\":" + "\"" + New[14].Value + "\"" + ",");
                    newdata.Append("\"cwcbzxdm\":" + "\"" + New[15].Value + "\"" + ",");
                    newdata.Append("\"cwbmdm\":" + "\"" + New[16].Value + "\"" + ",");
                    newdata.Append("\"isqdbzy\":" + "\"" + 0 + "\"" + ",");
                    newdata.Append("\"zybzbm\":" + "\"" + New[5].Value + "\"" + ",");
                    newdata.Append("},");
                    newdata.Append("\"state\":" + "\"open\"");
                    newdata.Append("}");
                    if (i < Col.Length - 1)
                    {
                        newdata.Append(",");
                    }
                }
                newdata.Append("]");
                addData = newdata.ToString();
            }

            //更新记录
            if (UpStr.Length != 0)
            {
                string[] Col = UpStr.Split(',');
                updata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
                    ZYDM = ZD[1].ToString();
                    ZYNBBM = ZD[2].ToString();
                    ZYBM = ZD[3].ToString();
                    ZYMC = ZD[4].ToString();
                    ZYBZBM = ZD[5].ToString();
                    SYBZ = ZD[6].ToString();
                    ZYBS = ZD[7].ToString();
                    CCJB = ZD[8].ToString();
                    YJDBZ = ZD[9].ToString();
                    WLPHBZ = ZD[10].ToString();
                    FBDM = ZD[11].ToString();
                    DWCBJSFS = ZD[12].ToString();
                    C_ORDER = ZD[13].ToString();
                    ISBZZY = ZD[14].ToString();
                    CWCBZXDM = ZD[15].ToString();
                    CWBMDM = ZD[16].ToString();
                    string UPSql = "UPDATE TB_JHZYML SET HSZXDM=@HSZXDM,ZYDM=@ZYDM,ZYNBBM=@ZYNBBM,ZYBM=@ZYBM,ZYMC=@ZYMC,ZYBZBM=@ZYBZBM,SYBZ=@SYBZ,ZYBS=@ZYBS,CCJB=@CCJB,YJDBZ=@YJDBZ,WLPHBZ=@WLPHBZ,FBDM=@FBDM,DWCBJSFS=@DWCBJSFS,C_ORDER=@C_ORDER,ISBZZY=@ISBZZY,CWCBZXDM=@CWCBZXDM,CWBMDM=@CWBMDM WHERE ZYDM=@ZYDM";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {   
                    new AseParameter("@HSZXDM", AseDbType.VarChar,40),
                    new AseParameter("@ZYDM", AseDbType.VarChar,40),
                    new AseParameter("@ZYNBBM", AseDbType.VarChar,100),
                    new AseParameter("@ZYBM", AseDbType.VarChar,3),
                    new AseParameter("@ZYMC", AseDbType.VarChar,60), 
                    new AseParameter("@ZYBZBM", AseDbType.VarChar,40),
                    new AseParameter("@SYBZ", AseDbType.VarChar,3),
                    new AseParameter("@ZYBS", AseDbType.Integer),
                    new AseParameter("@CCJB", AseDbType.Integer),
                    new AseParameter("@YJDBZ", AseDbType.VarChar,3),
                    new AseParameter("@WLPHBZ", AseDbType.VarChar,3),
                    new AseParameter("@FBDM", AseDbType.VarChar,40),
                    new AseParameter("@DWCBJSFS", AseDbType.VarChar,2),
                    new AseParameter("@C_ORDER", AseDbType.VarChar,2),
                    new AseParameter("@ISBZZY", AseDbType.Char,2),
                    new AseParameter("@CWCBZXDM", AseDbType.VarChar,40),
                    new AseParameter("@CWBMDM", AseDbType.VarChar,40),
                   
                };
                    UP[0].Value = HSZXDM;
                    UP[1].Value = ZYDM;
                    UP[2].Value = ZYNBBM;
                    UP[3].Value = ZYBM;
                    UP[4].Value = ZYMC;
                    UP[5].Value = ZYBZBM;
                    UP[6].Value = SYBZ;
                    UP[7].Value = ZYBS;
                    UP[8].Value = CCJB;
                    UP[9].Value = YJDBZ;
                    UP[10].Value = WLPHBZ;
                    UP[11].Value = FBDM;
                    UP[12].Value = DWCBJSFS;
                    UP[13].Value = C_ORDER;
                    UP[14].Value = ISBZZY;
                    UP[15].Value = CWCBZXDM;
                    UP[16].Value = CWBMDM;
                    List.Add(UP);


                    updata.Append("{");//跟新easyui tree相应的节点
                    updata.Append("\"id\":" + "\"" + ZYDM + "\"" + ",");
                    updata.Append("\"text\":" + "\"" + ZYMC + "\"" + ",");
                    updata.Append("\"attributes\":" + "{");
                    updata.Append("\"hszxdm\":" + "\"" + HSZXDM + "\"" + ",");
                    updata.Append("\"zybm\":" + "\"" + ZYBM + "\"" + ",");
                    updata.Append("\"ccjb\":" + "\"" + CCJB + "\"" + ",");
                    updata.Append("\"yjdbz\":" + "\"" + YJDBZ + "\"" + ",");
                    updata.Append("\"fbdm\":" + "\"" + FBDM + "\"" + ",");
                    updata.Append("\"zybs\":" + "\"" + ZYBS + "\"" + ",");
                    updata.Append("\"sybz\":" + "\"" + SYBZ + "\"" + ",");
                    updata.Append("\"wlphbz\":" + "\"" + WLPHBZ + "\"" + ",");
                    updata.Append("\"c_order\":" + "\"" + C_ORDER + "\"" + ",");
                    updata.Append("\"yy\":" + "\"" + "" + "\"" + ",");
                    updata.Append("\"zynbbm\":" + "\"" + ZYNBBM + "\"" + ",");
                    updata.Append("\"dwcbjsfs\":" + "\"" + DWCBJSFS + "\"" + ",");
                    updata.Append("\"isbzzy\":" + "\"" + ISBZZY + "\"" + ",");
                    updata.Append("\"cwcbzxdm\":" + "\"" + CWCBZXDM + "\"" + ",");
                    updata.Append("\"cwbmdm\":" + "\"" + CWBMDM + "\"" + ",");
                    updata.Append("\"isqdbzy\":" + "\"" + 0 + "\"" + ",");
                    updata.Append("\"zybzbm\":" + "\"" + ZYBZBM + "\"" + ",");
                    updata.Append("},");
                    updata.Append("\"state\":" + "\"open\"");
                    updata.Append("}");
                    if (i < Col.Length - 1)
                    {
                        updata.Append(",");
                    }
                }
                updata.Append("]");
                editData = updata.ToString();
            }

            //删除记录
            if (DelStr != "")
            {
                string[] arg = null ;
                string[] Col = DelStr.Split(',');
                StringBuilder strSql = new StringBuilder();
                deledata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    HSZXDM=ZD[0].ToString();
                    ZYDM = ZD[1].ToString();                    
                    //string res = DeleteData(ZYDM);
                    string set = SecDel(HSZXDM,ZYDM, ref strSql);
                    BLL.DeleteData("TB_JHZYML", ZYDM, "ZYDM");
                    if (set != null)
                    {
                        arg = set.Substring(0, set.Length - 1).Split(',');

                        for (int j = 0; j < arg.Length; j++)
                        {
                            BLL.DeleteData("TB_JHZYML", arg[j], "ZYDM");
                        }
                    }

                    deledata.Append("{");
                    deledata.Append("\"id\":");
                    deledata.Append("\"" + ZYDM + "\"");
                    deledata.Append("}");
                    if (i < Col.Length - 1)
                    {
                        deledata.Append(",");
                    }
                }
                deledata.Append("]");
                delData = deledata.ToString();
            }

            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);

        }
        else
        {
            Flag = -1;
        }
        DataSet num = BLL.Query("select count(ZYDM) from TB_JHZYML");
        if (num.Tables[0].Rows[0][0].ToString() != "0") { YjeBz(); }//更新叶节点标志

        string[] arr = new string[4];
        arr[0] = Flag.ToString();//返回增删改状态码
        arr[1] = addData;//返回增加的数据到easyui tree
        arr[2] = editData;//返回更新的数据到easyui tree
        arr[3] = delData;//返回删除的数据ID到easyui tree
        return arr;

    }

    //查询出需要删除的装置以及子节点的ID
    public string SecDel(string hszxdm,string id, ref StringBuilder ids)
    {
        //string arr=new Array[];
        string sql = "SELECT HSZXDM,ZYDM,ZYMC FROM TB_JHZYML WHERE FBDM='" + id + "'AND HSZXDM='"+hszxdm+"'";

        string res = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet da = bll.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            res = da.Tables[0].Rows[0][1].ToString();
            for (int i = 0; i < da.Tables[0].Rows.Count; i++)
            {

                ids.Append(da.Tables[0].Rows[i][1].ToString());
                ids.Append(",");
                SecDel(da.Tables[0].Rows[i][0].ToString(), da.Tables[0].Rows[i][1].ToString(), ref ids);

            }
        }
        else
        {
            return null;
        }

        return ids.ToString();
    }


    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string ParID)
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(ParID, ref Str);
        return Str.ToString();
    }
    private void RecursionChild(string ParID, ref StringBuilder Str)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //SELECT XMDM,XMBM,PAR,XMMC,CCJB,XMMCEX FROM TB_XMXX WHERE PAR='ParID'
        string sql = "SELECT * FROM TB_JHZYML WHERE FBDM='" + ParID + "' and HSZXDM='" + HttpContext.Current.Session["HSZXDM"] + "'";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        DR = DS.Tables[0].Select("FBDM='" + ParID + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["ZYDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["ZYMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\":{");
                Str.Append("\"hszxdm\":\"" + dr["HSZXDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"zybm\":\"" + dr["ZYBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"ccjb\":\"" + dr["CCJB"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"yjdbz\":\"" + dr["YJDBZ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"fbdm\":\"" + dr["FBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"zybs\":\"" + dr["ZYBS"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"sybz\":\"" + dr["SYBZ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"wlphbz\":\"" + dr["WLPHBZ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"c_order\":\"" + dr["C_ORDER"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"yy\":\"" + dr["YY"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"zynbbm\":\"" + dr["ZYNBBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"dwcbjsfs\":\"" + dr["DWCBJSFS"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"isbzzy\":\"" + dr["ISBZZY"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"cwcbzxdm\":\"" + dr["CWCBZXDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"cwbmdm\":\"" + dr["CWBMDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"isqdbzy\":\"" + dr["ISQDBZY"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"zybzbm\":\"" + dr["ZYBZBM"].ToString() + "\"");
                //Str.Append(",");
                Str.Append("}");
                Str.Append(",");
                string sqlnest = "select ZYDM from TB_JHZYML where FBDM='" + dr["ZYDM"].ToString() + "'";
                DataSet nest = BLL.Query(sqlnest);
                if (nest.Tables[0].Rows.Count > 0)
                {
                    Str.Append("\"state\":\"closed\"");

                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }

                //递归查询子节点
                 //RecursionChild(dr["XMDM"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }

    /// <summary>
    /// 获取easyui下拉树的json字符串
    /// </summary>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public static Hashtable GetTree()
    {
        string res = null;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet da = bll.GetList(string.Format(" MKDM='{0}'", 10145652));
        DataRow[] dt = da.Tables[0].Select("SRFS='2' and SRKJ='4'");
        string zdmc = null;
        Hashtable ht = new Hashtable();
        for (int i = 0; i < dt.Length; i++)
        {
            zdmc = dt[i]["FIELDNAME"].ToString();
            DataSet dm = bll.Query(dt[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", "'" + HttpContext.Current.Session["YY"].ToString() + "'").Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString()));
            StringBuilder result = new StringBuilder();
            res = GetTree("0", dm, zdmc, ref result);
            ht.Add(zdmc,res);
        }
        return ht;
    }


    /// <summary>
    /// combotree下拉树控件的json字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    public static string GetTree(string id, DataSet da, string zdmc, ref StringBuilder sb)
    {
      // StringBuilder sb=new StringBuilder();
       TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
       DataRow[] dr = da.Tables[0].Select("PAR='"+id+"'");
       if (dr.Length > 0)
       {
           //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
           if (sb.ToString() == "")
           {
               sb.Append("[");
           }
           else
           {
               sb.Append(",");
               sb.Append("\"children\": [");
           }
           for (int i = 0; i < dr.Length; i++)
           {
               DataRow dt = dr[i];
               sb.Append("{");
               if (zdmc == "CWCBZXDM")
               {
                   sb.Append("\"id\":\"" + dt["ZZBM"].ToString() + "\"");
                   sb.Append(",");
                   sb.Append("\"text\":\"" + dt["CWZYMC"].ToString().Trim() + "\"");
               }

               if (zdmc == "CWBMDM")
               {
                   sb.Append("\"id\":\"" + dt["ZZBM"].ToString() + "\"");
                   sb.Append(",");
                   sb.Append("\"text\":\"" + dt["CWBMM"].ToString().Trim() + "\"");
               }
               sb.Append(",");
           
               DataRow[] dd = da.Tables[0].Select("PAR='" + dt["ZZBM"].ToString() + "'");
              
               if (dd.Length > 0)
               {
                   sb.Append("\"state\":\"closed\"");

               }
               else
               {
                   sb.Append("\"state\":\"open\"");
               }

               //递归查询子节点
               GetTree(dt["ZZBM"].ToString(), da, zdmc, ref sb);
               if (i < dr.Length - 1)
               {
                   sb.Append("},");
               }
               else
               {
                   sb.Append("}]");
               }
           }
       }
       return sb.ToString();
    }

  

    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string[] GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }

    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>              
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string[] GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        string[] arr=new string[0];
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return arr;
        }
    }

    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string[] GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        List<string> ids = new List<string>();
        try
        {
       
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 100) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='180px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            DataRow[] dt = ds.Tables[0].Select("SRFS='2' and SRKJ='4'");
            Hashtable htdt = new Hashtable();
            for (int i = 0; i < dt.Length; i++)
            {
                htdt.Add(dt[i]["FIELDNAME"].ToString(), bll.Query(dt[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", "'"+HttpContext.Current.Session["YY"].ToString()+"'").Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));

            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                ids=getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, htdt, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {


        }
        sb.Append("</table>");

        string[] arr = new string[ids.Count + 1];
        for (int i = 0; i < ids.Count; i++)
        {
            arr[i] = ids[i];
        }
        arr[ids.Count] = sb.ToString();
        return arr;

    }

    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string[] GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        List<string> ids = new List<string>();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }

            DataRow[] dt = ds.Tables[0].Select("SRFS='2' and SRKJ='4'");
            Hashtable htdt = new Hashtable();
            for (int i = 0; i < dt.Length; i++)
            {
                htdt.Add(dt[i]["FIELDNAME"].ToString(), bll.Query(dt[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", "'"+HttpContext.Current.Session["YY"].ToString()+"'").Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));

               
            }

            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
               ids= getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, htdt, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }

        string[] arr = new string[ids.Count+1];
        for (int i = 0; i < ids.Count; i++) {
           arr[i] =ids[i];
        }
        arr[ids.Count] = sb.ToString();
        return arr;

    }


    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static List<string> getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, Hashtable htdt, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        Hashtable json = GetTree();
        List<string> ids = new List<string>();
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("FBDM='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("FBDM='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            ids.Add(trID + i.ToString()+'_');
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT class='easyui-validatebox' data-options='required:true' STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "ZYMC")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" validType='repeatMc'>");
                                    }
                                    else if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "ZYBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    }
                                    
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Trim() == "1") {
                                        sb.Append(" checked ");
                                    }
                                        
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0") {
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    }
                                       
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWCBZXDM" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWBMDM")
                                    {
                                        string res="";
                                        DataSet da = htdt[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()] as DataSet;
                                        DataRow[] row = da.Tables[0].Select("ZZBM='" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() + "'");              
                                        if (row.Length != 0) {
                                            res = ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWCBZXDM" ? row[0]["CWZYMC"].ToString().Trim() : row[0]["CWBMM"].ToString().Trim();
                                        }

                                        sb.Append("<div name='" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'><input id='" + Guid.NewGuid().ToString() + "' name='tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'class='easyui-combotree' data-options='onShowPanel:tt' style='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' value=\'" + res + "'/></div>"); 
                                    }else
                                    {
                                        sb.Append("<div name='" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'><input id='" + Guid.NewGuid().ToString() + "' name='tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "' style='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' value=\'" + dr[0][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "'/></div>"); 
                                    }
                                   
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
        return ids;
    }

    /// <summary>
    /// 生成添加行
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void GetData(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, Hashtable htdt, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        //if (string.IsNullOrEmpty(FiledName))
        //    dr = dtsource.Select("", "XMBM");
        //else
        //{
        //    dr = dtsource.Select("PAR='" + ParID + "'", "XMBM");
        //}
        dr=dtsource.Select();
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //DataSet dm = bll.Query("select ZYDM from TB_JHZYML where FBDM='" + dtsource.Rows[0][4] + "'");

        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            DataSet dm = bll.Query("select ZYDM from TB_JHZYML where FBDM='" + dr[i][4] + "'");
            if (!string.IsNullOrEmpty(FiledName))
                if (dm.Tables[0].Rows.Count > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + "1") + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + "1" + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (0 + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (0 + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[0][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" >");
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[0][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWCBZXDM" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWBMDM")
                                    {
                                        string res = "";
                                        DataSet da = htdt[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()] as DataSet;
                                        DataRow[] row = da.Tables[0].Select("ZZBM='" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() + "'");
                                        if (row.Length != 0)
                                        {
                                            res = ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CWCBZXDM" ? row[0]["CWZYMC"].ToString().Trim() : row[0]["CWBMM"].ToString().Trim();
                                        }

                                        sb.Append("<div name='" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'><input id='" + Guid.NewGuid().ToString() + "' name='tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'class='easyui-combotree' data-options='onShowPanel:tt' style='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' value=\'" + res + "'/></div>");
                                    }
                                    else
                                    {
                                        sb.Append("<div name='" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "'><input id='" + Guid.NewGuid().ToString() + "' name='tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "' style='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' value=\'" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "'/></div>");
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
        }

    }


    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }

    /// <summary>
    /// 返回自动生成的字符串主键值
    /// </summary>
    /// <returns></returns>
    public static string GetStringPrimaryKey(string bh)
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='"+bh+"'";
                        AseDataAdapter command = new AseDataAdapter(cmd);
                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+2
                            BM = (int.Parse(BM) + 2).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='" + bh + "'";
                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }



    /// <summary>
    /// 返回自动生成的NBBM
    /// </summary>
    /// <returns></returns>
    public static string GetZynbbm(string bmfl,int ccjb,string nbbm,string hszxdm,bool boo,string isTree)
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", FBBM="",BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        string bm = "";
                        if (isTree == "no") {
                             bm = boo ? nbbm : nbbm.Substring(0, nbbm.Length - 4);
                        }
                        if (isTree == "yes") {
                             bm = nbbm;
                        }
                        //当不选择添加添加同级的时候，先确定作业内部编码0002下一级长度8位的最大BM，找到加1并update，找不到做insert操作插入一条内部编码：0002XXXX
                        cmd.CommandText = nbbm == null ? "select FBBM,NEXTKYBM from TB_CURRENTBM where FBBM in (select max(FBBM) from TB_CURRENTBM where BMFL='" + bmfl + "'and len(FBBM)=8)" : "select FBBM,NEXTKYBM from TB_CURRENTBM where FBBM='" + bm + "'";
                        //查询当前最大的代码值
                        AseDataAdapter command = new AseDataAdapter(cmd);
                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            FBBM = DS.Tables[0].Rows[0]["FBBM"].ToString().Trim();
                            BM = DS.Tables[0].Rows[0]["NEXTKYBM"].ToString().Trim();

                            //获取当前编码的长度
                            int Len = BM.Length;
                            //获取当前最大的编码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前编码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTBM SET NEXTKYBM='" + BM + "' where FBBM='"+FBBM+"'";
                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["FBBM"].ToString().Trim() + BM;
                            }
                        }
                        else {
                            //查询开头为0002的代表作业编码的所有8位编码的最大值

                            string zynbbm = nbbm == null ? "00020001" : nbbm;
                            cmd.CommandText = nbbm == null ? "INSERT INTO TB_CURRENTBM (FBBM,BMFL,NEXTKYBM,BZ) values ('00020001','" + bmfl + "','0002',null)" : "INSERT INTO TB_CURRENTBM (FBBM,BMFL,NEXTKYBM,BZ) values ('"+nbbm+"','" + bmfl + "','0002',null)";
                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = zynbbm + "0001";
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }



    /// <summary>
    /// 点击查询
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public static string[] getResult(string FiledName, string TrID, string id, string YY, string HSZXDM,string ZYMC,string strarr, string readonlyfiled)
    {
        List<string> ids = new List<string>();
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='10145652' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 100) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", "10145652"));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='180px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");

            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();


            model = tbll.GetModel("10145652");
            string[] arrfiled = new string[] { ":YY" ,":HSZXDM",":ZYMC"};
            string[] arrvalue = new string[] { HttpContext.Current.Session["YY"].ToString(), HttpContext.Current.Session["HSZXDM"].ToString(), ZYMC };
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }

            DataRow[] dt = ds.Tables[0].Select("SRFS='2' and SRKJ='4'");
            Hashtable htdt = new Hashtable();
            for (int i = 0; i < dt.Length; i++)
            {
                htdt.Add(dt[i]["FIELDNAME"].ToString(), bll.Query(dt[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", "'" + HttpContext.Current.Session["YY"].ToString() + "'").Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));


            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                if (id != "")
                {
                    ids=getdata(dsValue.Tables[0], ds, FiledName, id, "tr", ref sb, "", htds, htdt, false, false, "");
                    //getdata(dsValue.Tables[0], ds, FiledName, id, "tr", ref sb, "", htds, false, false, "");
                }
                else
                {
                    GetData(dsValue.Tables[0], ds, FiledName, dsValue.Tables[0].Rows[0][4].ToString(), "tr", ref sb, "", htds, htdt, false, false, "");  
                   //GetData(dtsource,ds, FiledName,ParID,trID, ref StringBuilder sb,strarr,htds,ParIsLast,IsCheckBox,readonlyfiled)
                }

            }
        }
        catch
        {

        }

        string[] arr = new string[ids.Count + 1];
        for (int i = 0; i < ids.Count; i++)
        {
            arr[i] = ids[i];
        }
        arr[ids.Count] = sb.ToString();
        return arr;


    }


    //转移之前判断，要转移到的项目不能是该转移项目的子项目或兄弟节点
     [AjaxPro.AjaxMethod]
     public static string CheckChildren (bool boo,string oldZy,string newZy)
     {
         if (!boo)//添加到同级的时，判断已经是同级就没必要转移
         {
             TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
             string oldsql = "select FBDM from TB_JHZYML where ZYDM='" + oldZy + "'";
             string newsql = "select FBDM from TB_JHZYML where ZYDM='" + newZy + "'";
             DataSet da = bll.Query(oldsql);
             DataSet dt = bll.Query(newsql);
             if (da.Tables[0].Rows[0]["FBDM"].ToString() == dt.Tables[0].Rows[0]["FBDM"].ToString())
             {
                 return "要转移到的项目不能是被转移项目的兄弟节点！！！";
             }
         }
        
         ArrayList list=new ArrayList();
         ChildrenNode(oldZy, ref list);
         for (int i=0; i < list.Count; i++) {
             if (newZy == list[i].ToString())
             {
                 return "要转移到的项目不能是被转移项目子节点！！！";
              }
         }
             return null;
     }

    //递归查询子节点
     public static string ChildrenNode(string oldZy, ref ArrayList list)
     {
         DataSet da = new DataSet();
         TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
         string sql = "select ZYDM from TB_JHZYML where FBDM='" + oldZy + "'";
         da = bll.Query(sql);
         if (da.Tables[0].Rows.Count > 0)
         {
             for (int i = 0; i < da.Tables[0].Rows.Count;i++)
             {
                 list.Add(da.Tables[0].Rows[i]["ZYDM"].ToString());
                 ChildrenNode(da.Tables[0].Rows[i]["ZYDM"].ToString(), ref list);
             }
         }
         else {
             return null;
         }
         return null;
     }




    /// <summary>
    /// 转移数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public static string Migrate(bool boo, string id, string newid)
    {
        bool isGo = true;
        string sqlnewid = "select HSZXDM,ZYDM,FBDM,CCJB,ZYNBBM from TB_JHZYML where ZYDM='" + newid + "'";

        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(sqlnewid);
        string hszx = ds.Tables[0].Rows[0][0].ToString();
        string zydm = ds.Tables[0].Rows[0][1].ToString();
        string fbdm = ds.Tables[0].Rows[0][2].ToString();
        int ccjb = int.Parse(ds.Tables[0].Rows[0][3].ToString());
        string nbbm = ds.Tables[0].Rows[0][4].ToString();

        string sqlid = "select ZYDM,FBDM,CCJB,ZYNBBM from TB_JHZYML where ZYDM='" + id + "'";
        DataSet dt = bll.Query(sqlid);

        string parsql = "select ZYDM from TB_JHZYML where FBDM='" + id + "'";
        DataSet dp = bll.Query(parsql);

        if (boo)
        { //转移到下级   
            string bmnb = GetZynbbm("2", ccjb, nbbm, hszx, boo, "no");
            string newsql = "update TB_JHZYML set FBDM='" + zydm + "',ZYNBBM='" + bmnb + "',CCJB=" + (++ccjb) + " where ZYDM='" + id + "'";
            bll.Query(newsql);
            Zynbbm(id, isGo, boo);
        }
        else
        { //转移到同级
            string bmnb=GetZynbbm("2", ccjb, nbbm, hszx, boo, "no");
            string newsql = "update TB_JHZYML set FBDM='" + fbdm + "',ZYNBBM='" + bmnb + "',CCJB=" + ccjb + " where ZYDM='" + id + "'";
            bll.Query(newsql);
            Zynbbm(id, isGo, boo);
        }
        YjeBz();//
        return null;
    }

    /// <summary>
    /// 转移数据,递归生成Zynbbm
    /// </summary>
    /// <param></param>
    /// <returns></returns>
    public static void Zynbbm(string ID, bool isGo, bool boo)
    {
        if (isGo == false) { return; }//递归出口  
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sqlstart = "select CCJB,ZYNBBM from TB_JHZYML where ZYDM ='" + ID + "'";
        DataSet ds = bll.Query(sqlstart);
        int ccjb = int.Parse(ds.Tables[0].Rows[0][0].ToString());
        string zynbbm = ds.Tables[0].Rows[0][1].ToString();

        string sql = "select ZYDM,HSZXDM,CCJB,ZYNBBM from TB_JHZYML where FBDM ='" + ID + "'";
        DataSet dd = bll.Query(sql);
        int n = dd.Tables[0].Rows.Count;
        //根据传过来的ID查询库里的CCJB,ZYNBBM字段
        for (int j = 0; j < n; j++)
        {
            string[] childzydm = new string[n];
            string[] childhszxdm = new string[n];
            string[] childccjb = new string[n];
            string[] childnbbm = new string[n];
            childzydm[j] = dd.Tables[0].Rows[j][0].ToString();
            childhszxdm[j] = dd.Tables[0].Rows[j][1].ToString();
            childccjb[j] = dd.Tables[0].Rows[j][2].ToString();
            childnbbm[j] = dd.Tables[0].Rows[j][3].ToString();
            string nbbm = GetZynbbm("2", ccjb, zynbbm, childhszxdm[j], true, "no");

            //2.操作数据库更新ZYNBBM,CCJB
            string upnbbm = "update TB_JHZYML set ZYNBBM='" + nbbm + "', CCJB=" + (ccjb + 1) + " where ZYDM='" + childzydm[j] + "'";
            bll.Query(upnbbm);

            string sqlend = "select ZYDM,ZYBM from TB_JHZYML where FBDM ='" + childzydm[j] + "'";
            DataSet da = bll.Query(sqlend);
            int count = da.Tables[0].Rows.Count;
            if (da.Tables[0].Rows.Count > 0)
            {
                isGo = true;
                Zynbbm(childzydm[j], isGo, true);
            }
            else
            {
                isGo = false;
                Zynbbm(childzydm[j], isGo, true);
            }

        }
    }


    /// <summary>
    /// 叶节点标志
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static void YjeBz()
    {
        //执行增删改之后，判断是否为叶子节点，并更新叶子节点标志
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select distinct FBDM from TB_JHZYML";
        DataSet da = bll.Query(sql);
        StringBuilder yjdsql = new StringBuilder();//update yjdbz='1'
        StringBuilder zjdsql = new StringBuilder();//update yjdbz='0'
        yjdsql.Append("update TB_JHZYML set YJDBZ='1' where ZYDM not in(");
        zjdsql.Append("update TB_JHZYML set YJDBZ='0' where ZYDM in(");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            if (i < da.Tables[0].Rows.Count - 1)
            {
                yjdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                yjdsql.Append(",");

                zjdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                zjdsql.Append(",");

            }
            else
            {
                yjdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                yjdsql.Append(")");

                zjdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                zjdsql.Append(")");
            }

        }
        string sql1 = yjdsql.ToString();//update yjdbz='1'
        string sql0 = zjdsql.ToString();//update yjdbz='0'
        bll.Query(sql1);
        bll.Query(sql0);
    }

    //判断字段是否出现重复
    [AjaxPro.AjaxMethod]
    public static bool isRepeat(string column, string value)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select '" + column + "' from TB_JHZYML where CCJB!=0";
        DataSet da = bll.Query(sql);
        if(da.Tables[0].Rows.Count>0){
            for (int i = 0; i < da.Tables[0].Rows.Count;i++ )
            {
                if (value==da.Tables[0].Rows[i][column].ToString())
                {
                    return false;//出现重复，返回false
                }
            }
        }
        return true;//没有重复，返回true
    }

    #endregion;
}