﻿<%@ WebHandler Language="C#" Class="YDYSFB" %>

using System;
using System.Web;
using RMYH.BLL;
using System.Data;
using System.Text;
using System.Collections.Generic;
using Sybase.Data.AseClient;
using System.Collections;

public class YDYSFB : IHttpHandler {
    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "",YY="",NN="",HSZXDM="",GZBH="";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "BindNN":
                    YY = context.Request.QueryString["YY"];
                    res = GetMonth(YY);
                    break;
                case "seldateChange":
                    YY = context.Request.QueryString["YY"];
                    NN = context.Request.QueryString["NN"];
                    res = seldate(YY, NN).ToString();
                    break; 
                case "FB":
                    YY = context.Request.QueryString["YY"];
                    NN = context.Request.QueryString["NN"];
                    HSZXDM = context.Request.QueryString["HSZXDM"];
                    GZBH = context.Request.QueryString["GZBH"];
                    res = FB(YY, NN, HSZXDM, GZBH).ToString();
                    break;
                case "JF":
                    YY = context.Request.QueryString["YY"];
                    NN = context.Request.QueryString["NN"];
                    HSZXDM = context.Request.QueryString["HSZXDM"];
                    GZBH = context.Request.QueryString["GZBH"];
                    res = JF(YY, NN, HSZXDM, GZBH).ToString();
                    break;
            }
            context.Response.Write(res);
        }
    }
    public string GetMonth(string YY)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string NN = BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='" + YY + "'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    public int seldate(string YY, string NN)
    {
        int flag=0;
        string yynn = YY + NN;
        string str = "select BZ from TB_YDYSFB where YY='"+YY+"' AND NN='"+NN+"'";
        DataSet ds = bll.Query(str);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (int.Parse(ds.Tables[0].Rows[0][0].ToString()) == 1)
            {
                return flag = 1;
            }
        }
        return flag;
    }
    public int FB(string yy, string nn, string hszxdm, string gzbh)
    {
        int flag = 0;
        string sj = DateTime.Now.ToString();
        string isexit = "select * from TB_YDYSFB where YY='"+yy+"' AND NN='"+nn+"' and HSZXDM='"+hszxdm+"'";
        DataSet DSISEXIT = bll.Query(isexit);
        if (DSISEXIT.Tables[0].Rows.Count > 0)
        {
            string upt = "update TB_YDYSFB set BZ='1',SJ='" + sj + "',GZBH='" + gzbh + "' where YY='" + yy + "' AND NN='" + nn + "' and HSZXDM='" + hszxdm + "'";
            flag = bll.ExecuteSql(upt);
        }
        else
        {
            string add = "insert into TB_YDYSFB(YY,NN,HSZXDM,BZ,SJ,GZBH) VALUES('" + yy + "','" + nn + "','" + hszxdm + "','1','" + sj + "','" + gzbh + "')";
            flag = bll.ExecuteSql(add);
        }
        return flag;
    }
    public int JF(string yy, string nn, string hszxdm, string gzbh)
    {
        int flag = 0;
        string sj = DateTime.Now.ToString();
        string upt = "update TB_YDYSFB set BZ='0',SJ='" + sj + "',GZBH='" + gzbh + "' where YY='" + yy + "' AND NN='"+nn+"' and HSZXDM='" + hszxdm + "'";
        flag = bll.ExecuteSql(upt);
        return flag;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}