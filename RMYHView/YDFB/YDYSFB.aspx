﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YDYSFB.aspx.cs" Inherits="YDFB_YDYSFB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <title>月度预算发布</title>
    <script type="text/javascript">
        function BindYY() {
            $("#TxtYY").val($("#<%=HidYY.ClientID %>").val());
        }
        function BindNN() {
            $.ajax({
                type: 'get',
                url: 'YDYSFB.ashx',
                async: true,
                cache: false,
                data: {
                    action: 'BindNN',
                    YY: $("#<%=HidYY.ClientID %>").val()
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        $("#TxtNN").val(result);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        /// <summary>查询是否发布</summary>
        function seldateChange() {
            var yy = $("#TxtYY").val();
            var nn = $("#TxtNN").val();
            $.ajax({
                type: 'get',
                url: 'YDYSFB.ashx',
                async: true,
                cache: false,
                data: {
                    action: 'seldateChange',
                    YY: yy,
                    NN: nn
                },
                success: function (result) {
                    if (result == 0) {
                        $("#TxtXMMC").val("未发布") 
                    }
                    else {
                        $("#TxtXMMC").val("已发布") 
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        /// <summary>发布</summary>
        function FB() {
            if ($("#TxtXMMC").val() == "已发布") {
                alert("已发布的年月无法重复发布");
            }
            else {
                var yy = $("#TxtYY").val();
                var nn = $("#TxtNN").val();
                if (yy != "" && nn != "") {
                    $.ajax({
                        type: 'get',
                        url: 'YDYSFB.ashx',
                        async: true,
                        cache: false,
                        data: {
                            action: 'FB',
                            YY: $("#TxtYY").val(),
                            NN: $("#TxtNN").val(),
                            HSZXDM: $("#<%=HidHSZXDM.ClientID %>").val(),
                            GZBH: $("#<%=HidUser.ClientID %>").val()
                        },
                        success: function (result) {
                            if (result > 0) {
                                alert("发布成功");
                                seldateChange();
                            }
                            else {
                                alert("发布失败");
                            }
                        },
                        error: function () {
                            alert("加载失败!");
                        }
                    });
                }
                else {
                    alert("请选择年月");
                }
            }
        }
        function JF() {
            if ($("#TxtXMMC").val() == "未发布") {
                alert("未发布的年月无法解封");
            }
            else {
                var yy = $("#TxtYY").val();
                var nn = $("#TxtNN").val();
                if (yy != "" && nn != "") {
                    $.ajax({
                        type: 'get',
                        url: 'YDYSFB.ashx',
                        async: true,
                        cache: false,
                        data: {
                            action: 'JF',
                            YY: $("#TxtYY").val(),
                            NN: $("#TxtNN").val(),
                            HSZXDM: $("#<%=HidHSZXDM.ClientID %>").val(),
                            GZBH: $("#<%=HidUser.ClientID %>").val()
                        },
                        success: function (result) {
                            if (result > 0) {
                                alert("解封成功");
                                seldateChange();
                            }
                            else {
                                alert("解封失败");
                            }
                        },
                        error: function () {
                            alert("加载失败!");
                        }
                    });
                }
                else {
                    alert("请选择年月");
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" runat="server" id="HidHSZXDM" />
    <input type="hidden" runat="server" id="HidYY" />
    <input type="hidden" runat="server" id="HidUser" />
    <div style=" width:100%; height:100%">
    <table style=" margin: 15% auto;">
        <tr style=" height:50px">
         <td style=" text-align:right;">
            年份：
         </td>
         <td>
            <input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:seldateChange()})"  style="width:120px"  />
         </td>
         <td style=" text-align:right">
            月份：
         </td>
         <td>
            <input  type="text" id="TxtNN" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'MM',onpicked:seldateChange()})"  style="width:120px"  />
         </td>
        </tr>
        <tr style=" height:50px">
            <td style=" text-align:left">发布状态：</td>
            <td colspan="3"><input id="TxtXMMC"  disabled=disabled  style="width: 120px" type="text" class="easyui-validatebox" /></td>
        </tr>
        <tr style=" height:50px">
            <td colspan="2" style=" padding-left:100px"><input id="Button1" class="button5" style=" width:80px" type="button" value="发布" onclick="FB()" /></td>
            <td colspan="2"><input type="button" class="button5" value="解封" onclick="JF()" style="width: 80px"  id="xgbdh"/></td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
