﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using System.IO;
using System.Text;

public partial class TGXX_TGXX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(TGXX_TGXX));
    }
    /// <summary>
    /// 获取通告信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Tgxx()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet Tgxx = bll.Query("select * from TB_DLBZ");
        string rtn = Tgxx.Tables[0].Rows[0][0].ToString();
        return rtn;
    }
    /// <summary>
    /// 保存通告信息
    /// </summary>
    /// <param name="tgxx">通告信息</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Save(string tgxx)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet selsql = bll.Query("select * from TB_DLBZ");
        if (selsql.Tables[0].Rows.Count == 0)
        {
            try
            {
                string add = "";
                add = "insert into TB_DLBZ values('" + tgxx + "')";
                bll.ExecuteSql(add);
            }
            catch
            {

            }
            return "保存成功！";
        }
        else
        {
            try
            {
                string update = "";
                update = "update TB_DLBZ set BZXX='" + tgxx + "'";
                bll.ExecuteSql(update);
            }
            catch
            {

            }
            return "保存成功！";
        }
    }
    /// <summary>
    ///  加载通告消息 
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string loadTgxx()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> list = new List<string>();
        StringBuilder mbStr = new StringBuilder();
        StringBuilder faStr = new StringBuilder();
        var sql = "select * from TB_BZWJ";
        var count = "select count(*) from TB_BZWJ";
        DataSet da = bll.Query(sql);
        DataSet num = bll.Query(count);
        mbStr.Append("{\"total\":");
        mbStr.Append(num.Tables[0].Rows[0][0].ToString());
        mbStr.Append(",");
        mbStr.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            mbStr.Append("{");
            mbStr.Append("\"WJMC\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["WJMC"].ToString() + "\"");
            mbStr.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                mbStr.Append(",");
            }
        }
        mbStr.Append("]}");
        list.Add(faStr.ToString());
        list.Add(mbStr.ToString());

        string s = string.Join("", list.ToArray());
        return s;
    }
    /// <summary>
    /// 删除上传消息通告
    /// </summary>
    /// <param name="tgxx">文件名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteDate(string tgxx)
    {
        try
        {
            tgxx = tgxx.Replace(",", "','");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string delsql = "delete from TB_BZWJ where WJMC in('" + tgxx + "')";
            bll.ExecuteSql(delsql);
        }
        catch
        {
            return "删除失败！";
        }
        return "删除成功！";
    }

/// <summary>
/// 文件上传
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            try
            {
                //获取word物理地址
                string FullName = FileUpload1.PostedFile.FileName;//获取word物理地址
                //如果没选择文件就提示
                if (FullName == "" || FullName == null)
                {
                    Response.Write("<script>alert('请选择要上传的文件')</script>");
                }
                else
                {
                    FileInfo fi = new FileInfo(FullName);
                    //获取word名称
                    string name = fi.Name;
                    //获取word类型
                    string type = fi.Extension;
                    //如果格式不正确请选择上传正确的格式
                    if (type == ".doc" || type == ".docx"|| type==".pptx" || type==".ppt"|| type==".xls" || type==".xml" || type==".xlsx" || type==".txt")
                    {
                        string SavePath = Server.MapPath("../ExcelFile/TgxxFile");//word保存到文件夹下
                        this.FileUpload1.PostedFile.SaveAs(SavePath + "\\" + name);//保存路径
                        string sql = "insert into TB_BZWJ(WJMC,WJMCDZ) values('" + name + "','" + SavePath + "\\" + name + "')";
                        bll.ExecuteSql(sql);
                        Response.Write("<script>alert('上传成功')</script>");
                    }
                    else
                    {
                        Response.Write("<script>alert('上传的文件请选择word、excel、ppt、记事本')</script>");
                    }
                } 
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('"+ex.Message+"')</script>");
            }
        }
}