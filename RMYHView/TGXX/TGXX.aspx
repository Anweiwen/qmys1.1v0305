﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TGXX.aspx.cs" Inherits="TGXX_TGXX" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <input type="button" class="button5" value="保存" onclick="Save()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <div class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
        <div data-options="region:'west'" title="通告信息" style="width: 50%;">
            <textarea id="Txteartgxx" rows="16" style="width: 100%; height: 100%;" cols=""></textarea>
        </div>
        <div data-options="region:'center'" title="上传文件管理" style="width: 50%">
            <div class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
               <div id="DivMsg" style="width: 100%; height: 100%;" data-options="region:'center'">
                <%-- <%=GetDataGridList(2)%>--%>
                <table id="dg" style="width: 100%; height: 100%;" class="easyui-datagrid" title=""
                    data-options="autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: false,toolbar:'#tb'">
                    <thead>
                        <tr>
                            <th data-options="field:'WJMC',width:60">
                                文件名称
                            </th>
                        </tr>
                    </thead>
                </table>
                <div id="tb" style="padding: 5px; height: auto">
                    <div style="margin-bottom: 5px">
                        <input id="Button3" class="button5" type="button" value="删除" onclick="DeleteDate()" />
                    </div>
                </div>
            </div>

                <div data-options="region:'south'" title="文件上传" style="width: 100%; height: 30%">
                    <div id="fileUpload">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" Height="25px" />
                        <asp:Button ID="Button1" runat="server" Text="上传附件" class="button5" OnClick="Button1_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            Tgxx();
            setSize();
            loadTgxx();
        });
        function setSize() {
            var width = document.body.clientWidth * 0.25;
            var height = ((document.documentElement.clientHeight == 0) ? document.body.clientHeight : document.documentElement.clientHeight);
            $("#fileUpload").css("padding-left", width * 0.5 + "px");
            $("#fileUpload").css("padding-top", height * 0.10 + "px");
        }
        /// <summary>获取消息通告信息</summary>
        function loadTgxx() {
            var TgxxData = "";
            var rtn = TGXX_TGXX.loadTgxx().value;
            TgxxData = eval("(" + rtn + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', TgxxData);
        }
        /// <summary>分页</summary>
        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }

                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    loading: true,
                    showRefresh: false,
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }
        /// <summary>页面加载的时候获取通告信息</summary>
        function Tgxx() {
            var rtn = TGXX_TGXX.Tgxx().value;
            $("#Txteartgxx").val(rtn);
        }
        /// <summary>保存通告信息</summary>
        ////// <param name="Txteartgxx" type="String">通告信息</param>
        function Save() {
            var rtn = TGXX_TGXX.Save($("#Txteartgxx").val()).value;
            alert(rtn);
        }
        /// <summary>删除消息通告文件</summary>
        function DeleteDate() {
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length > 0) {
                var data = [];
                for (var i = 0; i < rows.length; i++) {
                    var item = [];
                    item.push(rows[i].WJMC);
                    data.push(item);
                }
                var rtn = TGXX_TGXX.DeleteDate(data.join(",")).value;
                alert(rtn);
                //刷新消息公告
                loadTgxx();
            }
            else {
                alert("请选择要删除的文件");
            }
        }
    </script>
</asp:Content>
