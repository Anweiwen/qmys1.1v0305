﻿<%@ WebHandler Language="C#" Class="SessionHandler" %>

using System;
using System.Web;
using System.Web.SessionState;

public class SessionHandler : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        string rtn = "";
        string action = context.Request.Params["action"];
        if (action == "ClearCache")
        {
            string CacheKey = "";
            string SessionID = context.Session.SessionID;
            //清空打开数据页是缓存模板页数据到内存中的数据
            if (SessionID != null && SessionID != "")
            {
                var caches=HttpRuntime.Cache.GetEnumerator();
                while (caches.MoveNext())
	            {
                    CacheKey = caches.Key.ToString();
                    if (CacheKey.IndexOf(SessionID) > -1)
                    {
                        RMYH.Common.DataCache.DelCache(CacheKey);
                    }
	            }
                //取消Session会话
                context.Session.Abandon();
                rtn = "ok";
            }
        }
        context.Response.Write(rtn);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}