﻿<%@ WebHandler Language="C#" Class="JHGLLogin" %>

using System;
using System.IO;
using System.Text;
using System.Net;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RMYH.BLL;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public class JHGLLogin : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            StreamReader sr = new StreamReader(context.Request.InputStream, Encoding.UTF8);
            switch (action)
            {
                //请求同步精益系统 用户的 url  'http://10.189.0.199/QMYSGL/JHGLLogin.ashx?action=sync-account'      请求的URL需提供给身份认证系统
                case "sync-account":
                    // 获得数据,解析JSON，反序列化
                    AccountDTO account = JsonConvert.DeserializeObject<AccountDTO>(sr.ReadToEnd());
                    SyncAccount(account);
                    sr.Close();
                    break;

                //请求同步精益系统 组织结构的 url  'http://10.189.0.199/QMYSGL/JHGLLogin.ashx?action=OrgReceiver'   请求的URL需提供给身份认证系统
                case "OrgReceiver":
                    // 获得数据,解析JSON，反序列化
                    OrgDTO org = JsonConvert.DeserializeObject<OrgDTO>(sr.ReadToEnd());
                    OrgReceiver(org);
                    sr.Close();
                    break;
            }
            context.Response.Write(res);
        }
    }

    /// <summary>
    /// 同步账号方法
    /// </summary>
    /// <param name="account"></param>
    public void SyncAccount(AccountDTO account)
    {
        string displayName = "身份认证用户", mobile = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Org org = JsonConvert.DeserializeObject<Org>(account.orgList[0]);

        string sql = "select USER_ID from TB_USER where USER_ID='" + account.accountName + "'";
        DataSet da = BLL.Query(sql);
        int count = da.Tables[0].Rows.Count;
           
        if (account.mappingAttr.Count != 0)
        {
            // account.mappingAttrs.displayName 对应的USER_NAME
            if (account.mappingAttr.ContainsKey("displayName"))
            {
                displayName = account.mappingAttr["displayName"];
            }
            // account.mappingAttrs.mobile 对应的PHONE
            if (account.mappingAttr.ContainsKey("mobile"))
            {
                mobile = account.mappingAttr["mobile"];
            }
        }

        try
        {
            //创建账号
            if ("CREATE".Equals(account.status))
            {
                // 处理账户创建的逻辑
                // account.accountName 对应USER_ID,USER_LOGIN
                if (count == 0)
                {
                    string addUser = "insert into TB_USER (USER_ID,USER_NAME,PHONE,USER_PSW,CC_NO,USER_LOGIN,ZYDM) values ('" + account.accountName + "','" + displayName + "','" + mobile + "','123456','10070003','" + account.accountName + "','" + org.code + "')";
                    BLL.Query(addUser);
                }
            }

            //更新账号
            if ("UPSERT".Equals(account.status))
            {

                // 处理更新账号的逻辑
                string upUser = "update TB_USER set USER_NAME='" + displayName + "',PHONE='" + mobile + "',USER_PSW='123456',CC_NO='10070003',USER_LOGIN='" + account.accountName + "',ZYDM='" + org.code + "' where USER_ID='" + account.accountName + "'";
                BLL.Query(upUser);
            }

            //删除账号的逻辑
            if ("DELETE".Equals(account.status))
            {
                // 处理删除账号的逻辑
                if (count > 0)
                {
                    string delUser = "delete from TB_USER where USER_ID='" + account.accountName + "'";
                    BLL.Query(delUser);
                }
            }

        }
        catch (Exception e)
        {
            HttpContext.Current.Response.StatusCode = 200;
            HttpContext.Current.Response.Write("{\"code\":\"error\",\"message\":" + e.ToString() + "}");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        HttpContext.Current.Response.StatusCode = 200;
        HttpContext.Current.Response.Write("{\"code\":\"success\"}");
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }



    /// <summary>
    /// 组织机构同步
    /// </summary>
    /// <param name="org"></param>
    public void OrgReceiver(OrgDTO org)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "select GROUP_ID from TB_GROUP where GROUP_ID='" + org.code + "'";
        DataSet dt = BLL.Query(sql);
        int count = dt.Tables[0].Rows.Count;
        
        try
        {
            if ("CREATE".Equals(org.status))
            {
                if (count == 0)
                {
                    string inSql = "insert into TB_GROUP (GROUP_ID,GROUP_NAME) values ('" + org.code + "','" + org.name + "')";
                    BLL.Query(inSql);
                }
            }

            if ("UPSERT".Equals(org.status))
            {
                // 处理组织机构更新的逻辑
                // 根据org.code查找您的系统中是否已经存在此组织机构
                string existSql = "select GROUP_ID,GROUP_NAME from TB_GROUP where GROUP_ID='" + org.code + "'";
                DataSet da = BLL.Query(existSql);

                // 如果存在则更新此组织机构
                if (da.Tables[0].Rows.Count > 0)
                {
                    // 检查信息中是否包含了newCode，如果有，则需要更新组织机构的代码
                    if (org.newCode != null)
                    {
                        string upCode = "update TB_GROUP set  GROUP_ID='" + org.newCode + "' where GROUP_ID='" + org.code + "'";
                        BLL.Query(upCode);
                    }
                }
            }

            if ("DELETE".Equals(org.status))
            {
                // 处理组织机构删除的逻辑
                // 根据org.code查找您的系统中是否已经存在此组织机构
                if (count > 0)
                {
                    string existSql = "select GROUP_ID,GROUP_NAME from TB_GROUP where GROUP_ID='" + org.code + "'";
                    DataSet da = BLL.Query(existSql);
                    // 如果存在则删除此组织机构
                    if (da.Tables[0].Rows.Count > 0)
                    {
                        string delSql = "delete from TB_GROUP where GROUP_ID='" + org.code + "'";
                        BLL.Query(delSql);
                    }
                }
                // 如果不存则不做处理
            }
        }
        catch (Exception e)
        {
            HttpContext.Current.Response.StatusCode = 200;
            HttpContext.Current.Response.Write("{\"code\":\"error\",\"message\":" + e.ToString() + "}");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        HttpContext.Current.Response.StatusCode = 200;
        HttpContext.Current.Response.Write("{\"code\":\"success\"}");
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}