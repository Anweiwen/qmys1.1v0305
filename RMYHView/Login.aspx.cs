﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Common;
using System.Text;
using RMYH.DBUtility;
using System.Reflection;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using Microsoft.CSharp;

public partial class Loginss : System.Web.UI.Page
{
    static string code="";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(Loginss));
        Session.Clear();
        HttpCookie cookie = Request.Cookies["RMYH_LOGIN"];

        if (PubConstant.SsoLogin == "true")
        {
            code = Request.QueryString["code"];
            if (code != "" && code != null) { SsoLogin(); }
        }
          
        if (!IsPostBack)
        {
            if (cookie != null)
            {
                txtuid.Value = cookie.Values.Get("USER_LOGIN");
                hidyy.Value = cookie.Values.Get("YY");
            }
        }
        getName();
    }

    /// <summary>
    /// 单点登录
    /// appid = "cqshqmys";
    /// appkey = "5ccqecgv4k0j6vrad9dj9bhqgetie6gy";
    /// </summary>
    public void SsoLogin()
    {
        string appid = "cqshqmys";
        string appkey = "5ccqecgv4k0j6vrad9dj9bhqgetie6gy";
        string user = SSO.Sso(appid, appkey, code);

        if (user != "")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string sql = "select USER_ID,USER_NAME,CC_NO from TB_USER where USER_LOGIN='" + user + "'";
            DataSet sa = bll.Query(sql);
            if (sa.Tables[0].Rows.Count > 0)
            {
                HttpContext.Current.Session["USERDM"] = sa.Tables[0].Rows[0]["USER_ID"].ToString();
                HttpContext.Current.Session["USERNAME"] = sa.Tables[0].Rows[0]["USER_NAME"].ToString();
                HttpContext.Current.Session["HSZXDM"] = sa.Tables[0].Rows[0]["CC_NO"].ToString();
                Response.Redirect("Index.aspx?rnd=" + new Random().Next(), false);
            }
        }
    }



    public bool IsExist(string code)
    {
        bool boo = false;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select USER_ID,USER_NAME from TB_USER where USER_ID=" + code + "";
        DataSet sa = bll.Query(sql);
        if (sa.Tables[0].Rows.Count > 0)
        {
            boo = true;
        }
        return boo;
    }

    public void getName()
    {
        string SQL = "select USER_NAME from TB_USER where USER_ID='" + txtuid.Value + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet SET = bll.Query(SQL);
        if (SET.Tables[0].Rows.Count > 0)
        {
            txtXM.Text = SET.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            txtXM.Text = "***********";
        }

    }

    /// <summary>
    /// 判断当前模板隐藏列设置,如果公式之间条件or成立一个则公式成立，若and全成立则成立
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SHEET">sheet页</param>
    /// <param name="JIAOSE">角色</param>
    /// <param name="MBLB">模板类别</param>
    /// <param name="NN">月份</param>
    /// <returns></returns>
    public string  YCLSZ(string MBDM,string SHEET,string JIAOSE,string MBLB,string NN)
    {
        //查询所有隐藏列设置数据
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT * FROM TB_MBCOLYC where MBDM='"+MBDM+"' AND SHEETNAME='"+SHEET+"'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            //需要隐藏的列
            string COLS = "";
            //隐藏的公式条件NN IN('12') and JIAOSE IN('10160014') or MBLX IN('4') and JIAOSE IN('10160014') or MBLB IN('4')
            string GS = "";
            int count = 0;
            string fhz = "";
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                COLS = set.Tables[0].Rows[i]["COLS"].ToString();
                GS = set.Tables[0].Rows[i]["GS"].ToString();
                //将公式中的and替换成& 将or替换成|
                GS = GS.Replace("and", "&").Replace("or", "|");
                if (COLS != "" && COLS != null && GS != "" && GS != null)
                {
                    string[] arrOR = GS.Split('|');
                    for (int j = 0; j < arrOR.Length; j++)
                    {
                        //and拆分
                        string[] arrAND = arrOR[j].Split('&');
                        //and条件之间成立的个数，成立为1，不成立为0
                        int andCount = 0;
                        for (int q = 0; q < arrAND.Length; q++)
                        {
                           
                            //单个公司成立的个数，成立为1，不成立为0
                            int dgGScount = 0;
                            //单个公式
                            string dgGS = arrAND[q].ToString();
                             //单个公式条件in 或者notin
                            int  tj = 0;
                            if (dgGS.IndexOf("NOTIN") > -1)
                            {
                                tj = 1;
                            }
                            else
                            {
                                tj = 0;
                            }
                            //单个公式第一个（
                            int index = dgGS.IndexOf("(");
                            //获取公式类型
                            string GSLX = dgGS.Substring(0, index);
                            //公式之间条件值
                            dgGS = dgGS.Substring(index, dgGS.Length-index);
                            //替换掉公式条件值中的（）
                            dgGS = dgGS.Replace(")","").Replace("(","");
                            for (int p = 0; p < dgGS.Split(',').Length; p++)
                            {
                                //如果公式类型是月份
                                if (GSLX.IndexOf("NN") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p] != "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p] == "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果公式类型是角色
                                else if (GSLX.IndexOf("JIAOSE") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p] != "'" + JIAOSE + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p] == "'" + JIAOSE + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果公式类型是模板类别
                                else if (GSLX.IndexOf("MBLX") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p] != "'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p] == "'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果是不存在的类别
                                else
                                {
                                    dgGScount = 0;
                                }
                            }
                            //单个条件不成立
                            if (dgGScount == 0)
                            {
                                andCount = 0;
                                break;
                            }
                            //单个条件成立
                            else
                            {
                                andCount = 1;
                            }
                        }
                        if (andCount != 0)
                        {
                            count = 1;
                            break;
                        }
                       
                    }
                    if (count ==1)
                    {
                        fhz += COLS+",";
                    }
                   
                }
            }
            return fhz = fhz == "" ? "" : fhz.Substring(fhz.Length - 1);
        }
        else
        {
            return "";
        }
       
    }
    /// <summary>
    ///获取通告信息
    /// </summary>
    /// <returns></returns>
    public string getTgxx()
    {
        string sql = "select * from TB_DLBZ";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet SET = bll.Query(sql);
        if (SET.Tables[0].Rows.Count > 0)
        {
            return SET.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            return "暂无通告信息";
        }
    }
    /// <summary>
    /// 获取用户名
    /// </summary>
    /// <param name="id">用户id</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getName(string id)
    {
        string SQL = "select USER_NAME from TB_USER where USER_ID='"+id+"'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet SET = bll.Query(SQL);
        if (SET.Tables[0].Rows.Count > 0)
        {
            return SET.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            return "";
        }
    
    }
    protected void Text1_ServerClick(object sender, EventArgs e)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();          
            string rtn = bll.toLogin(txtuid.Value, txtpwd.Value, hidyy.Value);
            if (rtn == "true")
            {
                //string year = DateTime.Now.Year.ToString();
                //string sql = "select * from sysobjects where name='BB_CZRZ_" + year + "'";
                //DataSet set = bll.Query(sql);
                //if (set.Tables[0].Rows.Count > 0)
                //{
                //    string time = DateTime.Now.ToString();
                //    string czr = Session["USERDM"].ToString();
                //    sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('登录成功','" + time + "','" + czr + "')";
                //    int count=bll.ExecuteSql(sql);
                //}
                //else
                //{
                //    sql = "create table BB_CZRZ_" + year + "( ";
                //    sql += "DM NUMERIC(10,0) IDENTITY,";
                //    sql += "RZMC NCHAR(100), ";
                //    sql += "MX NCHAR(100), ";
                //    sql += "DATE NCHAR(100), ";
                //    sql += "CZR NCHAR(100) ";
                //    sql += ")";
                //    int count = bll.ExecuteSql(sql);
                //    if (count == -1)
                //    {
                //        string time = DateTime.Now.ToString();
                //        string czr = Session["USERDM"].ToString();
                //        sql = "insert into BB_CZRZ_" + year + " (RZMC,DATE,CZR) VALUES ('登录成功','" + time + "','" + czr + "')";
                //        bll.ExecuteSql(sql);
                //    }
                //}

                HttpCookie cookie = new HttpCookie("RMYH_LOGIN");
                cookie.Expires = DateTime.Now.AddYears(1);
                cookie.Values.Add("USER_LOGIN", txtuid.Value);
                cookie.Values.Add("YY", hidyy.Value);
                Response.AppendCookie(cookie);
                Random rad = new Random();
                //MessageBox.Show(this,"登陆成功！");
                Response.Redirect("Index.aspx?rnd=" + rad.Next(), false);
            }
            else
            {
                MessageBox.Show(this, rtn);
            }
        }
        catch (Exception ee)
        {

            MessageBox.Show(this,ee.Message);
        }
    }
    #region
    /// <summary>
    /// 绑定大类
    /// </summary>
    [AjaxPro.AjaxMethod]
    public string GetDDLYear(string Y)
    {
        int Year = DateTime.Now.Year-5;
        string YY = "",Str = ""; 
        DataSet DS=new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();

        if (Y != null && Y!="")
        {
            YY = Y;
        }
        else
        {
            YY = DateTime.Now.Year.ToString();
        }
        ////获取最久远的计划方案
        //DS = BLL.Query("SELECT ISNULL(MIN(YY),'-1') YY FROM TB_JHFA");
        ////表示当前没有计划方案，就已当前年的作为开始年
        //if (DS.Tables[0].Rows[0]["YY"].ToString().Trim() == "-1")
        //{
        //    Year = DateTime.Now.Year;
        //}
        //else
        //{
        //    Year = int.Parse(DS.Tables[0].Rows[0]["YY"].ToString().Trim());
        //}
        for (int i = Year; i <= Year+10; i++)
        {

            if (YY.Trim().Equals(i.ToString().Trim()))
            {
                Str += "<option selected=true value='" + i.ToString() + "'>" + i.ToString() + "</option>";
            }
            else
            {
                Str += "<option value='" + i.ToString() + "'>" + i.ToString() + "</option>";
            }
        }
        return Str;
    }
    #endregion
}
