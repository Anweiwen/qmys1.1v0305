﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;
using System.Collections;
using RMYH.DBUtility;

public partial class MBSZ_JSQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(MBSZ_JSQX));
            //遍历树节点
            //string mbzq = Request.QueryString["MBZQ"];
            //TreeView1.ExpandAll();
            //gettree(mbzq);
        }
    }
    //public void gettree(string mbzq)
    //{
    //    DataSet DS = new DataSet();
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    StringBuilder strSql = new StringBuilder();
    //    if (mbzq == null)
    //    {
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "'");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    else
    //    {
    //        mbzq = mbzq.Replace(",", "','");
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "' AND MBZQ in ('" + mbzq + "')");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    DS = bll.Query(strSql.ToString());
    //    crertetree(TreeView1.Nodes, DS.Tables[0], "-1");
    //}
    //public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    //{
    //    DataRow[] row = tb.Select("PAR='" + par + "'");
    //    TreeNode node;
    //    for (int i = 0; i < row.Length; i++)
    //    {
    //        node = new TreeNode();
    //        node.Value = row[i]["MBDM"].ToString();
    //        node.Text = row[i]["MBMC"].ToString();
    //        if (tb.Select("PAR='" + node.Value + "'").Length > 0)
    //        {

    //            node.ImageUrl = "~/images/minus.gif";
    //        }
    //        else
    //        {
    //            node.ImageUrl = "~/images/noexpand.gif";
    //        }
    //        node.NavigateUrl = "javascript:setNodeStyle('" + node.Text + "');$('#" + MBDM.ClientID + "').val('" + row[i]["MBDM"].ToString() + "');getlist('', '', '');";
    //        tree.Add(node);
    //        crertetree(node.ChildNodes, tb, node.Value);
    //    }
    //}
    #region Ajax方法
 /// <summary>
 /// 刷新数据
 /// </summary>
 /// <param name="trid"></param>
 /// <param name="id"></param>
 /// <param name="intimgcount"></param>
 /// <param name="mbdm">模板代码</param>
 /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string mbdm)
    {
        return GetDataList.GetDataListstring("10144574", "", new string[] { ":MBDM" }, new string[] { mbdm }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144574");
    }
  /// <summary>
  /// 修改数据
  /// </summary>
  /// <param name="ID"></param>
  /// <param name="fileds"></param>
  /// <param name="values"></param>
  /// <param name="mbdm">模板代码</param>
  /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UpdateData(string[] ID, string fileds, string[] values, string mbdm)
    {
        try
        {
            string[] arr = new string[values.Length * 2];
            for (int i = 0; i < values.Length; i++)
            {
                arr[i * 2] = "delete from TB_MBJSQX where JSDM='" + ID[i] + "' and MBDM='" + mbdm + "'";
                arr[i * 2 + 1] = "insert into TB_MBJSQX(MBDM,JSDM,CXQX,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX) Values('" + mbdm + "','" + ID[i] + "','" + values[i].Split('|')[0] + "','" + values[i].Split('|')[1] + "','" + values[i].Split('|')[2] + "','" + values[i].Split('|')[3] + "','" + values[i].Split('|')[4] + "','" + values[i].Split('|')[5] + "','" + values[i].Split('|')[6] + "')";
            }
            return DbHelperOra.ExecuteSql(arr, null);
        }
        catch
        {
            return -1;
        }
      
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_MBJSQX", id, "JSDM");
        return ret;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBLX()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSMBLX'");
            string all = "1" + "," + "2" + "," + "3" + "," + "4" + "," + "5" + "," + "6" + "," + "7" + "," + "8" + "," + "9" + "," + "10";
            Str += "<option value='" + all + "'>选择全部</option>";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板周期获得相关信息
    /// </summary>
    /// <param name="FALX">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBZQ()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ'");
            string all = "1" + "," + "2" + "," + "3" + "," + "4";
            Str += "<option value='" + all + "'>选择全部</option>";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    #endregion
}