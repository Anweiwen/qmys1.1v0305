﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TBMB.aspx.cs" Inherits="MBSZ_TBMB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
    <link href="../CSS/demo.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/icon.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style1.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
 </head>
 <body>
 <div id="tt" class="easyui-tabs" fit="true">
          <div id="divMBSZ" title="填报模板设置" style="padding:5px" >
            <iframe  id="IfrMBSZ" name="IfrMBSZ" src="MBSZ.aspx" style="width: 100%;height:100%" frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	    </div>
	    <div id="divMBQX" title="角色模板权限" style="padding:5px">
            <iframe  id="IfrMBQX" name="IfrMBQX" src="JSMBQX.aspx" style="width: 100%;height:100%" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>
        <div id="divJSQX" title="填报角色权限" style="padding:5px">
            <iframe  id="IfrJSQX" name="IfrJSQX" src="JSQX.aspx" style="width: 100%;height:100%" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>             
	</div>
<input type="hidden" runat="server" id="hidloginout" />
    <script type="text/javascript">
        function GetExcelList() {
            var hg = $(window).height();
//            $("#IfrMBSZ").attr("src", "MBSZ.aspx");
//            $("#IfrMBQX").attr("src", "JSMBQX.aspx");
//            $("#IfrJSQX").attr("src", "JSQX.aspx");
            $("#tt").height(hg - 18);
            $("#IfrMBSZ").height(hg - 45);
            $("#IfrMBQX").height(hg - 45);
            $("#IfrJSQX").height(hg - 45);
        }
        $(document).ready(function () {
            if ($("#<%=hidloginout.ClientID %>").val() == "Out") {
                window.parent.parent.location = "<%=Request.ApplicationPath%>/Login.aspx?rnd=" + Math.random();
            } else {
                GetExcelList();
            }
        });
    </script>
</body>
 </html>