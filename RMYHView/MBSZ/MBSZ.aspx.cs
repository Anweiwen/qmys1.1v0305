﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Collections;
using RMYH.BLL.CDQX;
using Sybase.Data.AseClient;
using System.Data;
using RMYH.DBUtility;
using System.Text;
using RMYH.Model;

public partial class MBSZ_MBSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(MBSZ_MBSZ));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新方法
    /// </summary>
    /// <param name="trid"></param>
    /// <param name="id"></param>
    /// <param name="intimgcount"></param>
    /// <param name="mbmc">模板名称</param>
    /// <param name="mblx">模板类型</param>
    /// <param name="mbzq">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string mbmc, string mblx, string mbzq)
    {
        mbzq = mbzq.Replace(",", "','");
        if (mblx == "2" || mblx == "3")
        {
            return GetDataListstring("10146065", "", new string[] { ":MBMC", ":MBLX", ":USERDM", ":MBZQ" }, new string[] { mbmc, mblx, Session["USERDM"].ToString(), mbzq }, false, trid, id, intimgcount, mbzq, mblx);
        }
        if (mblx == "7")
        {
            return GetDataListstring("10145756", "", new string[] { ":MBMC", ":MBLX", ":USERDM", ":MBZQ" }, new string[] { mbmc, mblx, Session["USERDM"].ToString(), mbzq }, false, trid, id, intimgcount, mbzq, mblx);
        }
        return tabGetDataList.GetDataListstring("10144564", "", new string[] { ":MBMC", ":MBLX", ":USERDM", ":MBZQ" }, new string[] { mbmc, mblx, Session["USERDM"].ToString(), mbzq }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string mblx, string mbzq)
    {
        if (mblx == "2" || mblx == "3")
        {
            return getNewLines("10146065", false, "", mbzq, mblx);
        }
        else if (mblx == "7")
        {
            return getNewLines("10145756", false, "", mbzq, mblx);
        }
        else
        {
            return getNewLine("10144564", false, "", mblx);
        }
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_YSBBMB", id, "MBDM");
        }
        catch
        {
            return "删除失败！";
        }
        return ret;
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string mblx)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            //数据重重+1
            ArrayList al = new ArrayList();
            string filedsAdd = "";
            for (int i = 0; i < ID.Length; i++)
            {
                //模板名称
                string mbmc = values[i].Split('|')[1].Trim();
                //id不为空执行修改
                if (ID[i].Trim() != "")
                {
                    //模板类型是预算编制和预算价格编制的时候的修改
                    if (mblx == "2" || mblx == "3")
                    {
                        arr.Clear();
                        arrIdUpdate.Clear();
                        if (values[i].IndexOf(".") > 0)
                        {
                            arr.Add(values[i].Split('.')[0] + values[i].Split('.')[1].Substring(values[i].Split('.')[1].IndexOf("|")));  //values[i].Split('|')[2].Substring(0,8)
                        }
                        else
                        {
                            arr.Add(values[i]);

                        }
                        //查询数据库表里的数据
                        string sql = "select MBMC from TB_YSBBMB where MBDM NOT IN ('" + ID[i] + "')";
                        DataSet se = bll.Query(sql);
                        for (int j = 0; j < se.Tables[0].Rows.Count; j++)
                        {
                            //数据库中的mbmc
                            string sqlmbmc = se.Tables[0].Rows[j][0].ToString();
                            //如果数据库有中有和改的模板名称相同的就执行+1
                            if (mbmc == sqlmbmc)
                            {
                                al.Add(i + 1);
                            }
                        }
                        if (al.Count > 0)
                        {
                            return "已有模板名称相同的数据，请验证后保存";
                        }
                        else
                        {
                            arrIdUpdate.Add(ID[i]);
                            ret = bll.Update("10146065", "TB_YSBBMB", fileds, (String[])arr.ToArray(typeof(string)), "MBDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                        }
                    }
                    //模板类型是预算分解填报模板的修改
                    else if (mblx == "7")
                    {
                        arr.Clear();
                        arrIdUpdate.Clear();
                        if (values[i].IndexOf(".") > 0)
                        {
                            arr.Add(values[i].Split('.')[0] + values[i].Split('.')[1].Substring(values[i].Split('.')[1].IndexOf("|")));  //values[i].Split('|')[2].Substring(0,8)
                        }
                        else
                        {
                            arr.Add(values[i]);
                        }
                        //查询数据库表里的数据
                        string sql = "select MBMC from TB_YSBBMB where MBDM NOT IN ('" + ID[i] + "')";
                        DataSet se = bll.Query(sql);
                        for (int j = 0; j < se.Tables[0].Rows.Count; j++)
                        {
                            //数据库中的mbmc
                            string sqlmbmc = se.Tables[0].Rows[j][0].ToString();
                            //如果数据库有中有和改的模板名称相同的就执行+1
                            if (mbmc == sqlmbmc)
                            {
                                al.Add(i + 1);
                            }
                        }
                        if (al.Count > 0)
                        {
                            return "已有模板名称相同的数据，请验证后保存";
                        }
                        else
                        {
                            arrIdUpdate.Add(ID[i]);
                            ret = bll.Update("10145756", "TB_YSBBMB", fileds, (String[])arr.ToArray(typeof(string)), "MBDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                        }
                        //arr.Add(values[i]);
                    }
                    else
                    {
                        arr.Clear();
                        arrIdUpdate.Clear();
                        if (values[i].IndexOf(".") > 0)
                        {
                            arr.Add(values[i].Split('.')[0] + values[i].Split('.')[1].Substring(values[i].Split('.')[1].IndexOf("|")));  //values[i].Split('|')[2].Substring(0,8)
                        }
                        else
                        {
                            arr.Add(values[i]);
                        }
                        //查询数据库表里的数据
                        string sql = "select MBMC from TB_YSBBMB where MBDM NOT IN ('" + ID[i] + "')";
                        DataSet se = bll.Query(sql);
                        for (int j = 0; j < se.Tables[0].Rows.Count; j++)
                        {
                            //数据库中的mbmc
                            string sqlmbmc = se.Tables[0].Rows[j][0].ToString();
                            //如果数据库有中有和改的模板名称相同的就执行+1
                            if (mbmc == sqlmbmc)
                            {
                                al.Add(i + 1);
                            }
                        }
                        if (al.Count > 0)
                        {
                            return "已有模板名称相同的数据，请验证后保存";
                        }
                        else
                        {
                            arrIdUpdate.Add(ID[i]);
                            ret = bll.Update("10144564", "TB_YSBBMB", fileds, (String[])arr.ToArray(typeof(string)), "MBDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                        }
                        //arr.Add(values[i]);
                    }
                }
                //添加
                else
                {
                    //模板类型是预算编制和预算价格编制的时候的添加
                    if (mblx == "2" || mblx == "3")
                    {
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        filedsAdd = "MBWJ,MBLX," + fileds;
                        //string mbbm = GetStringPrimaryKey();
                        //string mbdm = "M" + ID[i] + ".xls";
                        string mbwj = "M" + ID[i] + ".XLS";
                        ARR1.Add(mbwj + "|" + mblx + "|" + values[i].Trim());
                        //查询数据库表里的数据
                        string sql = "select * from TB_YSBBMB where MBMC='" + mbmc + "'";
                        DataSet se = bll.Query(sql);
                        //数据库如果有就执行+1
                        if (se.Tables[0].Rows.Count != 0)
                        {
                            al.Add(i + 1);
                        }
                        else
                        {
                            arrIdAdd.Add(ID[i]);
                            ret = bll.Update("10146065", "TB_YSBBMB", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "MBDM", (String[])arrIdAdd.ToArray(typeof(string)));
                            //获得添加的结果
                            string[] value = (String[])ARR1.ToArray(typeof(string));
                            //获得模板编码
                            string MBBM = value[0].Split('|')[2];
                            //找到模板代码
                            DataSet selmbdm = bll.Query("select MBDM FROM TB_YSBBMB WHERE MBBM='" + MBBM + "'");
                            //把模板文件修改为M+项目代码+.xls
                            string updatembwj = "update TB_YSBBMB set MBWJ='M" + selmbdm.Tables[0].Rows[0][0].ToString() + ".XLS' WHERE MBDM='" + selmbdm.Tables[0].Rows[0][0].ToString() + "'";
                            bll.ExecuteSql(updatembwj);
                            //查询当前用户所拥有的角色
                            DataSet seljsdm = bll.Query("SELECT JSDM FROM TB_JSYH WHERE USERID='" + Session["USERDM"].ToString() + "'");
                            for (int j = 0; j < seljsdm.Tables[0].Rows.Count; j++)
                            {
                                //添加相对应的模板角色权限
                                string addMBJSQX = "insert into TB_MBJSQX(MBDM,JSDM,CXQX,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX) VALUES('" + selmbdm.Tables[0].Rows[0][0].ToString() + "','" + seljsdm.Tables[0].Rows[j][0].ToString() + "','1','1','1','0','0','0','1')";
                                bll.ExecuteSql(addMBJSQX);
                            }
                        }
                    }
                    //模板类型是预算分解填报模板的添加
                    else if (mblx == "7")
                    {
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        filedsAdd = "MBWJ,MBLX," + fileds;
                        //string mbbm = GetStringPrimaryKey();
                        //string mbdm = "M" + ID[i] + ".xls";
                        string mbwj = "M" + ID[i] + ".XLS";
                        ARR1.Add(mbwj + "|" + mblx + "|" + values[i].Trim());
                        //查询数据库表里的数据
                        string sql = "select * from TB_YSBBMB where MBMC='" + mbmc + "'";
                        DataSet se = bll.Query(sql);
                        //数据库如果有就执行+1
                        if (se.Tables[0].Rows.Count != 0)
                        {
                            al.Add(i + 1);
                        }
                        else
                        {
                            arrIdAdd.Add(ID[i]);
                            ret = bll.Update("10145756", "TB_YSBBMB", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "MBDM", (String[])arrIdAdd.ToArray(typeof(string)));
                            //获得添加的结果
                            string[] value = (String[])ARR1.ToArray(typeof(string));
                            //获得模板编码
                            string MBBM = value[0].Split('|')[2];
                            //找到模板代码
                            DataSet selmbdm = bll.Query("select MBDM FROM TB_YSBBMB WHERE MBBM='" + MBBM + "'");
                            //把模板文件修改为M+项目代码+.xls
                            string updatembwj = "update TB_YSBBMB set MBWJ='M" + selmbdm.Tables[0].Rows[0][0].ToString() + ".XLS' WHERE MBDM='" + selmbdm.Tables[0].Rows[0][0].ToString() + "'";
                            bll.ExecuteSql(updatembwj);
                            //查询当前用户所拥有的角色
                            DataSet seljsdm = bll.Query("SELECT JSDM FROM TB_JSYH WHERE USERID='" + Session["USERDM"].ToString() + "'");
                            for (int j = 0; j < seljsdm.Tables[0].Rows.Count; j++)
                            {
                                //添加相对应的模板角色权限
                                string addMBJSQX = "insert into TB_MBJSQX(MBDM,JSDM,CXQX,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX) VALUES('" + selmbdm.Tables[0].Rows[0][0].ToString() + "','" + seljsdm.Tables[0].Rows[j][0].ToString() + "','1','1','1','0','0','0','1')";
                                bll.ExecuteSql(addMBJSQX);
                            }
                        }
                    }
                    else
                    {
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        filedsAdd = "MBWJ,MBLX," + fileds;
                        //string mbbm = GetStringPrimaryKey();
                        //string mbdm = "M" + ID[i] + ".xls";
                        string mbwj = "M" + ID[i] + ".XLS";
                        ARR1.Add(mbwj + "|" + mblx + "|" + values[i].Trim());
                        //查询数据库表里的数据
                        string sql = "select * from TB_YSBBMB where MBMC='" + mbmc + "'";
                        DataSet se = bll.Query(sql);
                        //数据库如果有就执行+1
                        if (se.Tables[0].Rows.Count != 0)
                        {
                            al.Add(i + 1);
                        }
                        else
                        {
                            arrIdAdd.Add(ID[i]);
                            ret = bll.Update("10144564", "TB_YSBBMB", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "MBDM", (String[])arrIdAdd.ToArray(typeof(string)));
                            //获得添加的结果
                            string[] value = (String[])ARR1.ToArray(typeof(string));
                            //获得模板编码
                            string MBBM = value[0].Split('|')[2];
                            //找到模板代码
                            DataSet selmbdm = bll.Query("select MBDM FROM TB_YSBBMB WHERE MBBM='" + MBBM + "'");
                            //把模板文件修改为M+项目代码+.xls
                            string updatembwj = "update TB_YSBBMB set MBWJ='M" + selmbdm.Tables[0].Rows[0][0].ToString() + ".XLS' WHERE MBDM='" + selmbdm.Tables[0].Rows[0][0].ToString() + "'";
                            bll.ExecuteSql(updatembwj);
                            //查询当前用户所拥有的角色
                            DataSet seljsdm = bll.Query("SELECT JSDM FROM TB_JSYH WHERE USERID='" + Session["USERDM"].ToString() + "'");
                            for (int j = 0; j < seljsdm.Tables[0].Rows.Count; j++)
                            {
                                //添加相对应的模板角色权限
                                string addMBJSQX = "insert into TB_MBJSQX(MBDM,JSDM,CXQX,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX) VALUES('" + selmbdm.Tables[0].Rows[0][0].ToString() + "','" + seljsdm.Tables[0].Rows[j][0].ToString() + "','1','1','1','0','0','0','1')";
                                bll.ExecuteSql(addMBJSQX);
                            }
                        }
                    }
                }
            }
            //如果有重复的数据
            if (al.Count > 0)
            {
                return "已有模板名称相同的数据，请验证后保存";
            }
        }
        catch
        {
            return "保存失败！";
        }
        return "保存成功！";
    }
    //模板编码
    public static string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='16'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='16'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBLX()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSMBLX'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板周期获得相关信息
    /// </summary>
    /// <param name="FALX">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBZQ()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ'");
            string all = "1" + "," + "2" + "," + "3" + "," + "4";
            Str += "<option value='" + all + "'>选择全部</option>";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    [AjaxPro.AjaxMethod]
    public string UpdateMBLX(string mbdm, string mblx)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string update = "update TB_YSBBMB SET MBLX='" + mblx + "' WHERE MBDM='" + mbdm + "'";
            bll.ExecuteSql(update);
        }
        catch
        {

        }
        return "修改成功";
    }
    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="parId">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string parId)
    {
        StringBuilder str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(parId, ref str);
        return str.ToString();
    }
    #endregion
    #region
    private void RecursionChild(string parId, ref StringBuilder str)
    {
        DataSet ds = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT MLDM,MLBM,MLMC,CCJB,YJDBZ,FBDM from TB_YSBBML WHERE FBDM='" + parId + "' order by  MLBM,MLDM";
        //清空之前所有Table表
        ds.Tables.Clear();
        ds = bll.Query(sql);
        DataRow[] drRows = ds.Tables[0].Select("FBDM='" + parId + "'");
        if (drRows.Length <= 0) return;
        //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
        if (str.ToString() == "")
        {
            str.Append("[");
        }
        else
        {
            str.Append(",");
            str.Append("\"state\":\"closed\"");
            str.Append(",");
            str.Append("\"children\": [");
        }
        for (int i = 0; i < drRows.Length; i++)
        {
            DataRow dr = drRows[i];
            str.Append("{");
            str.Append("\"id\":\"" + dr["MLBM"] + "\"");
            str.Append(",");
            str.Append("\"text\":\"" + dr["MLMC"] + "\"");
            str.Append(",");
            str.Append("\"attributes\":{");
            str.Append("\"mldm\":\"" + dr["MLDM"] + "\"");
            str.Append(",");
            str.Append("\"ccjb\":\"" + dr["CCJB"] + "\"");
            str.Append(",");
            str.Append("\"yjdbz\":\"" + dr["YJDBZ"] + "\"");
            str.Append(",");
            str.Append("\"fbdm\":\"" + dr["FBDM"] + "\"");
            str.Append("}");
            str.Append(",");
            string sqlnest = "select 1 from TB_YSBBML where FBDM='" + dr["MLBM"] + "'";
            DataSet nest = bll.Query(sqlnest);
            if (nest.Tables[0].Rows.Count > 0)
            {
                str.Append("\"state\":\"closed\"");
            }
            else
            {
                str.Append("\"state\":\"open\"");
            }

            //递归查询子节点
            //RecursionChild(dr["XMDM"].ToString(), ref Str);
            if (i < drRows.Length - 1)
            {
                str.Append("},");
            }
            else
            {
                str.Append("}]");
            }
        }

    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string mbzq, string mblx)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "", mbzq, mblx);
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled, string mbzq, string mblx)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled, mbzq, mblx);
            }
            int intimgcount = int.Parse(strarr);
            strarr = "";
            for (int i = 0; i <= intimgcount; i++)
            {
                strarr += ",0";
            }
            return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled, mbzq);
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled, string mbzq, string mblx)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\"");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL);
            if (ds != null)
            {
                DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
                Hashtable htds = new Hashtable();
                const string querySql = "SELECT * FROM TB_YSBBML A LEFT JOIN TB_YSBBMB B ON A.MLDM=B.MLDM";
                DataSet dataSet = bll.Query(querySql);
                for (int i = 0; i < dr.Length; i++)
                {
                    htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString()).Replace(":MBZQ", "'" + mbzq + "'").Replace(":MBLX", mblx)));
                    //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
                }
                if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
                {
                    getdata(dsValue.Tables[0], ds, FiledName, "", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled, dataSet.Tables[0], "MLBM", false);
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        sb.Append("</table>");
        return sb.ToString();

    }

    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    /// <param name="preNum">对应预算目录树的根节点个数</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled, DataTable dataTable, string fieldName, bool isChildren)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        DataRow[] drRows;
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("FBDM='" + ParID + "'", FiledName);
        }
        if (string.IsNullOrEmpty(fieldName))
            drRows = dataTable.Select("", fieldName);
        else
            drRows = dataTable.Select("FBDM='" + ParID + "'", fieldName);
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        if (!isChildren)
        {
            trID += "_" + Guid.NewGuid();//生成行ID
            for (int i = 0; i < drRows.Length; i++)
            {
                bool bolextend = false;
                if (!string.IsNullOrEmpty(fieldName))
                    if (dataTable.Select("FBDM='" + drRows[i][fieldName] + "'").Length > 0)//判断是否有子节点
                        bolextend = true;
                sb.Append("<tr id='" + (trID + (i + 1).ToString().PadLeft(3, '0')) + "_' name=trdata ");
                sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5  style=\"width:60px\" ></td>");
                if (bolextend)//生成有子节点的图标
                    sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + (i + 1).ToString().PadLeft(3, '0') + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
                else//生成无子节点的图标
                    sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标

                for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                    {
                        if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                        {
                            if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                            else
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                        }
                        else
                        {
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                            //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                            sb.Append(drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                            //else
                            //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                            sb.Append("</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td style=\"table-layout:fixed\" name>");
                        switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                        {
                            case "1"://用户输入
                                switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                                {
                                    case "0"://textbox
                                        sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                        if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                            sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                        if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                            sb.Append(" onchange=EditData(this,1) ");
                                        else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                            sb.Append(" onchange=EditData(this,2) ");
                                        else
                                            sb.Append(" onchange=EditData(this,0) ");
                                        sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                        break;
                                    case "3"://时间控件
                                        sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                        sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                        if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                            sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                        sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case "2"://用户选择
                                switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                                {
                                    case "2"://选择                                    
                                        sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                        if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                            sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                        sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                        break;
                                    case "1"://复选框
                                        sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                        if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                            sb.Append(" checked ");
                                        if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                            sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                        sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                        break;
                                    case "4":
                                        sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 90).ToString() + "px' ");
                                        if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                            sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                        sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            default:
                                sb.Append(drRows[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                                break;
                        }
                        sb.Append("</td>");
                    }
                }
                sb.Append("</tr>");
                //生成子节点
                if (!string.IsNullOrEmpty(fieldName))
                {
                    if (int.Parse(drRows[i]["CCJB"].ToString()) > strarr.Length / 2)
                    {
                        //标记子节点是否需要生成父节点竖线
                        if (ParIsLast)
                            strarr += ",0";
                        else
                            strarr += ",1";
                    }
                    if (bolextend)
                    {
                        if (i == drRows.Length - 1)
                        {
                            if (int.Parse(drRows[i]["CCJB"].ToString()) == strarr.Length / 2)
                                if (!string.IsNullOrEmpty(strarr))
                                {
                                    strarr = strarr.Substring(0, int.Parse(drRows[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(drRows[i]["CCJB"].ToString()) * 2);
                                }
                        }
                    }
                    else
                    {
                        if (i == drRows.Length - 1)
                            ParIsLast = true;
                    }
                }
            }
        }

        trID += "_" + Guid.NewGuid();

        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5  style=\"width:60px\" ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td style=\"table-layout:fixed\" name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 90).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled, string mbzq)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            string querySql = model.SQL;
            string fbdm = TrID.Substring(TrID.Length - 4, 3);
            querySql = querySql.Insert(querySql.IndexOf("WHERE", StringComparison.Ordinal), " INNER JOIN TB_YSBBML E ON A.MLDM=E.MLDM ");
            querySql = querySql.Replace("%''%", "%%");
            querySql = querySql.Replace("A.MLDM IS NULL", "E.FBDM='" + fbdm + "'");
            //string sql = "SELECT * FROM TB_YSBBML A INNER JOIN TB_YSBBMB B ON A.MLDM=B.MLDM WHERE FBDM='" + fbdm + "'";
            dsValue = bll.Query(querySql);
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString()).Replace(":MBZQ", "'" + mbzq + "'")));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled, dsValue.Tables[0], "", true);
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return sb.ToString();

    }

    //private string GetResult(string mldm)
    //{
    //    StringBuilder sb = new StringBuilder();
    //    try
    //    {
    //        sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\"");
    //        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //        DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
    //        sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
    //        DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
    //        sb.Append("<tr align=\"center\" class=\"summary-title\" >");
    //        sb.Append("<td width='60px'>&nbsp;</td>");
    //        sb.Append("<td width='80px' >编码</td>");
    //        if (ds != null && ds.Tables[0].Rows.Count > 0)
    //        {
    //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //            {

    //                sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
    //                if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
    //                    sb.Append(" style=\"display:none\"");
    //                sb.Append(" >");
    //                sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
    //                sb.Append("</td>");
    //            }
    //        }
    //        sb.Append("</tr>");
    //        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
    //        TB_TABLESXModel model = new TB_TABLESXModel();
    //        model = tbll.GetModel(XMDM);
    //        for (int i = 0; i < arrfiled.Length; i++)
    //        {
    //            if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
    //                model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
    //            else
    //                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
    //        }
    //        DataSet dsValue = bll.Query(model.SQL);
    //        if (ds != null)
    //        {
    //            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
    //            Hashtable htds = new Hashtable();
    //            const string querySql = "SELECT * FROM TB_YSBBML A LEFT JOIN TB_YSBBMB B ON A.MLDM=B.MLDM";
    //            DataSet dataSet = bll.Query(querySql);
    //            for (int i = 0; i < dr.Length; i++)
    //            {
    //                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString()).Replace(":MBZQ", "'" + mbzq + "'").Replace(":MBLX", mblx)));
    //            }
    //            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
    //            {
    //                getdata(dsValue.Tables[0], ds, FiledName, "", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled, dataSet.Tables[0], "MLBM", true);
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception(e.Message);
    //    }
    //    sb.Append("</table>");
    //    return sb.ToString();

    //}
    #endregion
    #region  新增加数据方法

    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, string mblx)
    {
        return getNewLine(XMDM, false, "", mblx);
    }
    public static string getNewLines(string XMDM, string mbzq, string mblx)
    {
        return getNewLines(XMDM, false, "", mbzq, mblx);
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, string mblx)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":MBLX", mblx)));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "MBBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKey()) + "\"");
                                    }
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 80).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    public static string getNewLines(string XMDM, bool IsCheckBox, string readonlyfiled, string mbzq, string mblx)
    {
        try
        {
            mbzq = mbzq.Replace(",", "','");
            mblx = mblx.Replace(",", "','");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":MBLX", mblx).Replace(":MBZQ", "'" + mbzq + "'")));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "MBBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKey()) + "\"");
                                    }
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 80).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion
    #region 模板流程判断
    /// <summary>
    /// 判断当前模板是否被流程所用
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsUsingMB(string MBDM)
    {
        bool Flag = false;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Flag = BLL.Query("SELECT CJBM FROM TB_YSMBLC WHERE QZMBDM='" + MBDM + "' UNION SELECT CJBM FROM TB_YSMBLC WHERE MBDM='" + MBDM + "'").Tables[0].Rows.Count > 0 ? true : false;
        return Flag;
    }
    /// <summary>
    /// 根据模板代码查询模板周期
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBZQByMBDM(string MBDM)
    {
        string MBZQ = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (MBDM != "")
        {
            MBZQ = BLL.getDataSet("TB_YSBBMB", "MBZQ", "MBDM='" + MBDM + "'").Tables[0].Rows[0]["MBZQ"].ToString();
        }
        return MBZQ;
    }
    #endregion
}