﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class MBSZ_JSMBQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(MBSZ_JSMBQX));
            //string mbzq = Request.QueryString["MBZQ"];
            //string mblx = Request.QueryString["MBLX"];
            //string mbmc = Request.QueryString["MBMC"];
            //遍历树节点
            //TreeView1.ExpandAll();
            //gettree(mbzq,mblx,mbmc);
        }
    }

    //public void gettree(string mbzq,string mblx,string mbmc)
    //{
    //    DataSet DS = new DataSet();
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    StringBuilder strSql = new StringBuilder();
    //    if (mbzq == null&& mblx==null)
    //    {
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "'");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    else if (mblx != null && mbzq == null)
    //    {
    //        mblx = mblx.Replace(",", "','");
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "'and MBLX IN ('" + mblx + "')");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    else if (mbzq != null && mblx == null)
    //    {
    //        mbzq = mbzq.Replace(",", "','");
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "'and MBZQ IN ('" + mbzq + "')");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    else
    //    {
    //        mblx = mblx.Replace(",", "','");
    //        mbzq = mbzq.Replace(",", "','");
    //        strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
    //        strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID='" + Session["USERDM"].ToString() + "' AND MBZQ in ('" + mbzq + "') and MBLX IN ('" + mblx + "')");
    //        strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
    //    }
    //    DS = bll.Query(strSql.ToString());
    //    crertetree(TreeView1.Nodes, DS.Tables[0], "-1");
    //}
    //public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    //{
    //    DataRow[] row = tb.Select("PAR='" + par + "'");
    //    TreeNode node;
    //    for (int i = 0; i < row.Length; i++)
    //    {
    //        node = new TreeNode();
    //        node.Value = row[i]["MBDM"].ToString();
    //        node.Text = row[i]["MBMC"].ToString();
    //        if (tb.Select("PAR='" + node.Value + "'").Length > 0)
    //        {

    //            node.ImageUrl = "~/images/minus.gif";
    //        }
    //        else
    //        {
    //            node.ImageUrl = "~/images/noexpand.gif";
    //        }
    //        node.NavigateUrl = "javascript:BBCD('" + node.Text + "','"+node.Value+"')";
    //        tree.Add(node);
    //        crertetree(node.ChildNodes, tb, node.Value);
    //    }
    //}
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBLX()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSMBLX'");
            string all = "1" + "," + "2" + "," + "3" + "," + "4" + "," + "5" + "," + "6" + "," + "7" + "," + "8" + "," + "9" + "," + "10";
            Str += "<option value='" + all + "'>选择全部</option>";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板周期获得相关信息
    /// </summary>
    /// <param name="FALX">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBZQ()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ'");
            string all = "1" + "," + "2" + "," + "3" + "," + "4";
            Str += "<option value='" + all + "'>选择全部</option>";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
}