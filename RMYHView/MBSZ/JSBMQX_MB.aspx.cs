﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;
public partial class MBSZ_JSBMQX_MB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //遍历树节点
        AjaxPro.Utility.RegisterTypeForAjax(typeof(MBSZ_JSBMQX_MB));
        string mbzq = Request.QueryString["zq"].ToString();
        string mblx = Request.QueryString["lx"].ToString();
        string mbmc = Request.QueryString["mc"].ToString();
        TreeView1.ExpandAll();
        gettree(mbzq, mblx, mbmc);
    }
    /// <summary>
    /// 获取树
    /// </summary>
    /// <param name="mbzq">模板周期</param>
    /// <param name="mblx">模板类型</param>
    /// <param name="mbmc">模板名称</param>
    public void gettree(string mbzq, string mblx, string mbmc)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        string USERID = Session["USERDM"].ToString();
        if (mbzq == "null" && mblx == "null")
        {
            strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
            strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID=(CASE '"+USERID+"' WHEN 'SYS' THEN USERID ELSE '"+USERID+"' END)");
            strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
        }
        else
        {
            mblx = mblx.Replace(",", "','");
            mbzq = mbzq.Replace(",", "','");
            strSql.Append("select  A.MBDM,A.MBMC,'-2' PAR FROM  TB_YSBBMB A INNER JOIN TB_MBJSQX B ON A.MBDM=B.MBDM");
            strSql.Append(" INNER JOIN TB_JSYH C ON B.JSDM=C.JSDM WHERE USERID=(CASE '" + USERID + "' WHEN 'SYS' THEN USERID ELSE '" + USERID + "' END) AND MBZQ in ('" + mbzq + "') and MBLX IN ('" + mblx + "') and MBMC LIKE '%" + mbmc + "%'");
            strSql.Append(" union select '-2' MBDM,'模板名称' MBMC,'-1' PAR FROM TB_YSBBMB");
        }
        DS = bll.Query(strSql.ToString());
        crertetree(TreeView1.Nodes, DS.Tables[0], "-1");
    }
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("PAR='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["MBDM"].ToString();
            node.Text = row[i]["MBMC"].ToString();
            if (tb.Select("PAR='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:BBCD1('" + node.Text + "','" + node.Value + "')";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }


}
