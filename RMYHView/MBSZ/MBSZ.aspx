﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MBSZ.aspx.cs" Inherits="MBSZ_MBSZ" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    模板类型：<select id="SelMBLX" onchange=" MBLXChage() " style="width: 150px;"></select>&nbsp;&nbsp;
    模板周期：<select id="SelMBZQ" onchange=" MBZQChage() " style="width: 150px;"></select>&nbsp;&nbsp;
    模版名称：<input id="TxtMKM" style="width: 120px" type="text" />&nbsp;
    <input type="button" class="button5" value="查询" onclick=" getlist('', '', '') " />
    <input class="button5" onclick=" jsAddData() " type="button" value="添加" />
    <input type="button" class="button5" value="删除" onclick=" Del() " />
    <input type="button" class="button5" value="取消删除" onclick=" Cdel() " />
    <input id="Button11" class="button5" type="button" value="保存" onclick=" SetValues() " />
    <input id="Button1" class="button5" type="button" value="修改模板类型" onclick=" UpdateMBLX() "
        style="width: 100px" />
    <input id="btnSave" class="button5" type="button" value="保存到分类" onclick="saveToCategory()"
        style="width: 100px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <div class="easyui-layout" style="width: 100%; height: 100%;" fit="true">
        <div id="p" data-options="region:'west'" title="预算目录" style="width: 15%; padding: 10px">
            <ul id="tt" class="easyui-tree">
            </ul>
        </div>
        <div data-options="region:'center'" title="变动项目基础编码设置">
            <div style="width: 100%; height: 100%; overflow: auto" id="divTreeListView">
            </div>
        </div>
        <div id="Wincopy" class="easyui-window" title="修改模板方案类型" closed="true" style="width: 450px;
            height: 430px; padding: 20px; text-align: center;" minimizable="false" maximizable="false">
            <table>
                <tr style="width: 450px">
                    <td style="padding-top: 50px; padding-left: 50px; width: 450px" colspan="2">
                        模板类型：<select id="upMBLX" onchange=" LXChage() " style="width: 150px;"></select>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr style="width: 450px">
                    <td style="padding-top: 50px; padding-left: 110px; width: 100px">
                        <input id="Button4" class="button5" type="button" value="确定" onclick=" updateMblx() " />
                    </td>
                    <td style="padding-top: 50px;">
                        <input id="Button5" class="button5" type="button" value="退出" onclick=" Close() " />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <input type="hidden" id="hidindexid" value="MBDM" />
    <input type="hidden" id="hidcheckid" />
    <input type="hidden" id="hidNewLine" />
    <input type="hidden" id="hide_par" runat="server" />
    <input type="hidden" id="HidMC" runat="server" />
    <input type="hidden" id="hide_ccjb" runat="server" />
    <input type="hidden" id="hide_FBDM" />
    <script type="text/javascript">
        $(document).ready(function () {
            BindMBLX();
            BindMBZQ();
            getlist('', '', '');
            jsonTree();
        });
        var row = "";

        //获得列表
        function getlist(objtr, objid, intimagecount) {
            var rtnstr = MBSZ_MBSZ.LoadList(objtr, objid, intimagecount, $("#TxtMKM").val(), $("#SelMBLX").val(), $("#SelMBZQ").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            //模板名称不能重复
            $("#divTreeListView tr").find("input[name^='txtMBMC']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMBMC']").val(); //找到当前行的模板名
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtMBMC']"); //找到所有的模板名
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    } else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("模板名不能重复！");
                    $("#" + par + " input[name^='txtMBMC']").val("");
                }
            });
            //模板编码不能重复
            $("#divTreeListView tr").find("input[name^='txtMBBM']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMBBM']").val(); //
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtMBBM']"); //
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    } else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("模板编码不能重复！");
                    $("#" + par + " input[name^='txtMBBM']").val("");
                }
            });
            //起始行不能小于1
            $("#divTreeListView tr").find("input[name^='txtSTARTROW']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtSTARTROW']").val(); //找到当前行的起始行的值
                var reg = parvalue.replace(/^[0-9]*[1-9][0-9]*$/g, '');
                if (parvalue < 2 || reg != "") {
                    alert("起始行必须大于1的整数");
                    $("#" + par + " input[name^='txtSTARTROW']").val("2");
                }
            });
            //起始列不能小于1
            $("#divTreeListView tr").find("input[name^='txtSTARTCOL']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtSTARTCOL']").val(); //找到当前行的起始列的值
                var reg = parvalue.replace(/^[0-9]*[1-9][0-9]*$/g, '');
                if (parvalue < 2 || reg != "") {
                    alert("起始列必须大于1的整数");
                    $("#" + par + " input[name^='txtSTARTCOL']").val("2");
                }
            });
            //当前行最大列数不能小于1
            $("#divTreeListView tr").find("input[name^='txtMAXCOLS']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMAXCOLS']").val(); //找到当前行最大列数的值
                if (parvalue < 2) {
                    alert("最大列数必须大于1的整数");
                    $("#" + par + " input[name^='txtMAXCOLS']").val("20");
                }
            });
            row = "";
        }

        //下拉列表onchage
        function MBLXChage() {
            getlist('', '', '');
        }

        function MBZQChage() {
            getlist('', '', '');
        }

        function SetValues() {
            var mbmcValue = "";
            var MBMC = $("#divTreeListView input[name^=txtMBMC]");
            for (var i = 0; i < MBMC.length; i++) {
                if (MBMC[i].value.replace(/(\s*$)/g, "") == "") {
                    mbmcValue = "模板名称不能为空！"
                    break;
                }
            }
            var zydm = "";
            var ZYDM = $("#divTreeListView input[name^=tselZYDM]");
            for (var i = 0; i < ZYDM.length; i++) {
                if (ZYDM[i].value.replace(/(\s*$)/g, "") == "") {
                    zydm = "使用部门不能为空！"
                    break;
                }
            }
            var objnulls = $("[ISNULLS=N]");
            var strnullmessage = "";
            for (i = 0; i < objnulls.length; i++) {
                if (objnulls[i].value == "") {
                    strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                    break;
                }
            }
            obj = document.getElementsByName("readimage");
            var delid = "";
            var edtid = new Array();
            var edtobjfileds = "";
            var edtobjvalues = new Array();
            objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
            for (var i = 0; i < objss.length; i++) {
                delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
            }
            objss = $("img[src$='edit.jpg'], img[src$='new.jpg']").parent().parent(); //新增和修改
            for (var i = 0; i < objss.length; i++) {
                if (mbmcValue != "") {
                    alert(mbmcValue);
                    return false;
                }
                if (zydm != "") {
                    alert(zydm);
                    return false;
                }
                if (strnullmessage != "") {
                    alert(strnullmessage);
                    return false;
                }
                var objtd = $("tr[id=" + objss[i].id + "] td");
                var objfileds = "";
                var objvalues = "";
                for (var j = 2; j < objtd.length; j++) {
                    if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                        edtid[edtid.length] = objtd[j].innerHTML;
                    else {
                        var objinput = objtd[j].getElementsByTagName("input");
                        if (objinput.length > 0) {
                            if (objinput[0].name.substring(0, 3) == "txt") {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            } else if (objinput[0].name.substring(0, 3) == "sel") {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            } else if (objinput[0].name.substring(0, 3) == "chk") {
                                if (objinput[0].checked)
                                    objvalues += "|1";
                                else
                                    objvalues += "|0";
                                objfileds += "," + objinput[0].name.substring(3);
                            } else if (objinput[0].name.substring(0, 4) == "tsel") {
                                //                        objvalues += "|" + objinput[0].value.split('.')[0];
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(4);
                            }
                        } else {
                            objinput = objtd[j].getElementsByTagName("select");
                            if (objinput.length > 0) {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            }
                        }
                    }
                }
                if (objfileds != "") {
                    edtobjfileds = objfileds.substring(1);
                    edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
                }
            }
            if (edtobjfileds.length > 0)
                jsUpdateData(edtid, edtobjfileds, edtobjvalues);
            if (delid != "")
                jsDeleteData(delid.substring(1));
            $("#hidcheckid").val("");
            getlist('', '', '');
            return true;
        }

        //添加数据
        function jsAddData() {
            //if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(MBSZ_MBSZ.AddData($("#SelMBLX").val(), $("#SelMBZQ").val()).value);
            $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
            //模板名称不能重复
            $("#divTreeListView tr").find("input[name^='txtMBMC']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMBMC']").val(); //找到当前行的模板名
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtMBMC']"); //找到所有的模板名
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    } else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("模板名不能重复！");
                    $("#" + par + " input[name^='txtMBMC']").val("");
                }
            });
            //模板编码不能重复
            $("#divTreeListView tr").find("input[name^='txtMBBM']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMBBM']").val(); //
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtMBBM']"); //
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    } else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("模板编码不能重复！");
                    $("#" + par + " input[name^='txtMBBM']").val(parvalue);
                }
            });
            //模板文件不能重复
            $("#divTreeListView tr").find("input[name^='txtMBWJ']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMBWJ']").val(); //找到当前行的模板文件值
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtMBWJ']"); //找到所有的模板文件值
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    } else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("模板文件名不能重复！");
                    $("#" + par + " input[name^='txtMBWJ']").val("");
                }
            });
            $("#divTreeListView tr").find("input[name^='txtSTARTROW']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtSTARTROW']").val(); //找到当前行的起始行的值
                var reg = parvalue.replace(/\D/g, '');
                if (parvalue < 2 || reg == "") {
                    alert("起始行必须大于1的整数");
                    $("#" + par + " input[name^='txtSTARTROW']").val("2");
                }
            });
            $("#divTreeListView tr").find("input[name^='txtSTARTCOL']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtSTARTCOL']").val(); //找到当前行的起始列的值
                var reg = parvalue.replace(/\D/g, '');
                if (parvalue < 2 || reg == "") {
                    alert("起始列必须大于1的整数");
                    $("#" + par + " input[name^='txtSTARTCOL']").val("2");
                }
            });
            $("#divTreeListView tr").find("input[name^='txtMAXCOLS']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtMAXCOLS']").val(); //找到当前行最大列数的值;
                if (parvalue < 2) {
                    alert("最大列数必须大于1的整数");
                    $("#" + par + " input[name^='txtMAXCOLS']").val("20");
                }
            });
        }

        //修改数据
        function jsUpdateData(objid, objfileds, objvalues) {
            var rtn = MBSZ_MBSZ.UpdateData(objid, objfileds, objvalues, $("#SelMBLX").val()).value;
            alert(rtn);
        }

        //删除
        function jsDeleteData(obj) {
            var rtn = MBSZ_MBSZ.DeleteData(obj).value;
            if (rtn == "1") {
                alert("删除成功")
            } else {
                alert(rtn);
            }
        }

        function BindMBLX() {
            var rtn = MBSZ_MBSZ.GetMBLX().value;
            $("#SelMBLX").html(rtn);
        }

        function BindMBZQ() {
            var rtn = MBSZ_MBSZ.GetMBZQ().value;
            $("#SelMBZQ").html(rtn);
        }

        function EditData(obj, flag) {
            if (flag == "0") {
                if (obj.name == "selMBZQ") {
                    var trid = obj.parentNode.parentNode.id;
                    var MBDM = $("tr[id=" + trid + "]").find("td[name=tdMBDM]").html();
                    var rtn = MBSZ_MBSZ.IsUsingMB(MBDM).value;
                    if (rtn) {
                        alert("此模板已在流程中设置，请先删除流程后，在修改模板周期！");
                        var MBZQ = MBSZ_MBSZ.GetMBZQByMBDM(MBDM).value;
                        obj.value = MBZQ;
                        return;
                    }
                }
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
            } else if (flag == "1") {
                if (isNaN(obj.value)) {
                    alert("请输入数字类型数据");
                    obj.value = "";
                    obj.focus();
                } else {
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
                }
            } else if (flag == "2") {
                if (obj.value == "")
                    return;
                var isdates = false;
                var d = obj.value.split('-');
                if (1 < d.length && d.length < 4) {
                    m1 = parseInt(d[0]);
                    m2 = parseInt(d[1])
                    if ((!isNaN(m1)) && 1000 < m1 < 9999 && (!isNaN(m2)) && 1 < m1 < 12 && d[1].length == 2) //判断年月
                    {
                        if (d.length == 3) //判断是否带天
                        {
                            m3 = parseInt(d[1])
                            if (1 < m3 < 31 && d[2].length == 2) {
                                isdates = true;
                            }
                        } else {
                            isdates = true;
                        }
                    }
                }
                if (!isdates) {
                    alert("请输入正确的时间格式");
                    obj.value = "";
                    obj.focus();
                } else {
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";

                }
            }
        }

        /// <summary>找到模板代码</summary>
        //全局变量模板代码
        var mbdm = "";

        function onselects(obj) {
            $("tr[name^='trdata']").css("backgroundColor", "");
            var trid = obj.parentNode.parentNode.id;
            row = obj;
            $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());

            trid = obj.parentNode.parentNode.id;
            mbdm = $('tr[id=' + trid + '] td[name=\'tdMBDM\']').html();
        }

        /// <summary>绑定修改的模板类型</summary>
        function LXChage() {

        }

        function BindUpdateMBLX() {
            var rtn = MBSZ_MBSZ.GetMBLX().value;
            $("#upMBLX").html(rtn);
        }

        /// <summary>弹出模板类型</summary>
        function UpdateMBLX() {
            if (row == "") {
                alert("请选择要替换模板类型的模板");
            } else {
                $("#Wincopy").window('open');
                BindUpdateMBLX();
                //            var updateMBlx = MBSZ_MBSZ.UpdateMBLX(mbdm).value;
                //            alert(updateMBlx);
            }
        }

        function Close() {
            $("#Wincopy").window('close');
        }

        /// <summary>修改模板类型</summary>
        function updateMblx() {
            var updateMBlx = MBSZ_MBSZ.UpdateMBLX(mbdm, $("#upMBLX").val()).value;
            alert(updateMBlx);
            $("#Wincopy").window('close');
            getlist('', '', '');
        }
        //获取预算目录树
        function jsonTree() {
            var rtn = MBSZ_MBSZ.GetChildJson("");
            $('#tt').tree({
                lines: true,
                onBeforeExpand: function (node) {
                    var childrens = $('#tt').tree('getChildren', node.target);
                    if (childrens == false) {
                        nodeId = node.id;
                        pareNode = node.target;
                        var res = MBSZ_MBSZ.GetChildJson(node.id);
                        var Data = eval("(" + res.value + ")");
                        //var selected = $('#tt').tree('getSelected');
                        $('#tt').tree('append', {
                            parent: node.target,
                            data: Data
                        });
                    }
                },
                onExpand: function (node) {
                },
                onCollapse: function (node) {
                },
                onClick: function (node) {
                    // if ($('#tt').tree('isLeaf', node.target)) {//判断是否是叶子节点
                    //                    $("#HidTrID").val("");
                    //                    $("#hidcheckid").val("");
                    var mlbm = node.id;
                    var fbdm = node.attributes.fbdm;
                    if (fbdm == " ") {
                        $('#hide_FBDM').val(mlbm);
                    } else {
                        $('#hide_FBDM').val(fbdm);
//                        var rtnstr = MBSZ_MBSZ.getResult('XMDM', '', node.id, '', '0', '', xmbm).value;
//                        document.getElementById("divTreeListView").innerHTML = rtnstr;
                    }

                    //                    Bind();
                }

            });
            if (rtn.value != "") {
                var Data = eval("(" + rtn.value + ")");
                $("#tt").tree("loadData", Data);
            }
        }
        function saveToCategory() {
            if ($('#hidcheckid').val() && $('#hide_FBDM').val()) {
                $.ajax({
                    type: 'post',
                    url: 'MBSZHandler.ashx?action=saveToCategory',
                    data: {
                        fbdm: $('#hide_FBDM').val(),
                        mbdm: $('#hidcheckid').val()
                    },
                    success: function () {
                        jsonTree();
                        getlist("", "", "");
                        $("#hidcheckid").val("");
                        $("#HidTrID").val("");
                        $('#hide_FBDM').val("");
                    }
                });
            }
        }
        function ToExpand(obj, id) {
            var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
            var trs;
            if (obj.src.indexOf("tminus.gif") > -1) {
                obj.src = "../Images/Tree/tplus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                //            trs = $("#divTreeListView tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lminus.gif") > -1) {
                obj.src = "../Images/Tree/lplus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lplus.gif") > -1) {
                obj.src = "../Images/Tree/lminus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
            else if (obj.src.indexOf("tplus.gif") > -1) {
                obj.src = "../Images/Tree/tminus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                var istoload = true;
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                        istoload = false;
                    }
                }
                if (istoload == true) {
                    var XMDM = $("tr[id=" + id + "] td[name='tdXMDM']").html();
                    getRes(id, XMDM, $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                    divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                    for (var i = 0; i < trs.length; i++) {
                        if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                            trs[i].style.display = "";
                        }
                    }
                }
            }

        }
        function getRes(objtr, objid, intimagecount) {
            var rtnstr = MBSZ_MBSZ.LoadList(objtr, objid, intimagecount, $("#TxtMKM").val(), $("#SelMBLX").val(), $("#SelMBZQ").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "") {
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            } else {
                $("#" + objtr).after(rtnstr);
            }
            //Bind();
        }
    </script>
</asp:Content>
