﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YSCWDY.aspx.cs" Inherits="XMXX_YSCWDY" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>核算项目与财务科目对应</title>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
    <link href="../Content/themes/icon.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/style.css" />
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script src="../JS/Main.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JS/WdatePicker.js"></script>
    <script type="text/javascript">
        var param = "";
        $(function () {
            var nowdate = new Date();
            var year = nowdate.getFullYear();
            $("#Year").val(year);
            LoadXMFL();
            $("#DYGXGrid").datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                singleSelect: true,
                striped: true,
                pagination: true,
                pageSize: 30,
                toolbar: ['-', {
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        Edit("add");
                    }
                }, '-', {
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        Edit("edit");
                    }
                },
                 '-', {
                     text: '删除',
                     iconCls: 'icon-remove',
                     handler: function () {
                         Remove();
                     }
                 }],
                columns: [[
                    { field: 'XMFL', title: '项目分类编码', hidden: true },
                    { field: 'FLMC', title: '项目分类', width: 150 },
                    { field: 'XMDM', title: '项目代码', hidden: true },
                    { field: 'XMMC', title: '项目名称', width: 150 },
                    { field: 'F_KMBH', title: '财务科目编码', width: 150 },
					{ field: 'F_KMMC', title: '财务科目名称', width: 150 },
					{ field: 'SJLYMC', title: '数据来源名称', width: 150 }
				]],
                onHeaderContextMenu: function (e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                },
                onDblClickRow: function (rowIndex, rowData) {
                    Edit("edit");
                }
            })
            initDYDialog('dialogDY', '对应关系维护', 600, 200, $('#DYXX'));
            LoadXMMC();
            LoadCWKM(); 
            LoadSJLY();
        })
        function LoadXMMC() {
            var XMDM = '';
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=LoadXMMC&XMDM=' + XMDM,
                async: false,
                cache: false,
                success: function (result) {
                    $("#XMMC").combotree("clear");
                    var Data = JSON.parse(result);
                    $('#XMMC').combotree({
                        data: Data,
                        onBeforeExpand: function (node) {
                            $.ajax({
                                type: 'get',
                                url: 'YSCWDY.ashx?action=LoadXMMC&XMDM=' + node.id,
                                async: false,
                                cache: false,
                                success: function (result) {
                                    var Data = eval("(" + result + ")");
                                    $('#XMMC').combotree("tree").tree('append', {
                                        parent: node.target,
                                        data: Data
                                    });
                                }
                            });
                        }
                    })
                }
            });
        }
        function LoadCWKM() {
            var BM = '';
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=LoadCWKM&BM=' + BM,
                async: false,
                cache: false,
                success: function (result) {
                    if (result != "") {
                        var Data = eval("(" + result + ")");
                        $('#CWKM').combotree({
                            data: Data,
                            onBeforeExpand: function (node) {
                                $.ajax({
                                    type: 'get',
                                    url: 'YSCWDY.ashx?action=LoadCWKM&BM=' + node.id,
                                    async: false,
                                    cache: false,
                                    success: function (result) {
                                        var Data = eval("(" + result + ")");
                                        $('#CWKM').combotree("tree").tree('append', {
                                            parent: node.target,
                                            data: Data
                                        });
                                    }
                                });
                            }
                        })
                    }
                }
            });
        }

     function LoadSJLY() {
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=LoadSJLY',
                async: false,
                cache: false,
                success: function (result) {
                    $("#SJLY").combobox("clear");
                    var data = JSON.parse(result)
                    $("#SJLY").combobox("loadData", data);
                    if (result != "") {
                        $("#SJLY").combobox("setValue", data[0].id);
                    }
                }
            });
        }

        function initDYDialog(name, title, wid, hg, con) {
            $('#' + name).dialog({
                title: title,
                width: wid,
                height: hg,
                modal: true,
                closed: true,
                cache: false,
                buttons: [{
                    text: '保存',
                    iconCls: 'icon-ok',
                    handler: function () {
                        var XMFL = $("#TxtXMFL").val();
                        var FLDM = $("#XMFL").combobox("getValue");
                        var SJLY = $("#SJLY").combobox("getValue");
                        var XMDM = $("#XMMC").combotree("getValue");
                        var CWKM = $("#CWKM").combotree("getValue");
                        switch (param) {
                            case "add":
                                if (FLDM != "" && XMDM != "" && CWKM != "" && SJLY != "") {
                                    AddDYData(FLDM, XMDM, CWKM, SJLY);
                                }
                                else {
                                    alert("请填写完整信息！");
                                }
                                break;
                            case "edit":
                                var rows = $("#DYGXGrid").datagrid("getSelected");
                                var YSXMFL = rows.XMFL;
                                var YSXMDM = $("#HidXMDM").val();
                                var YSCWKM = $("#HidCWKM").val();
                                var YSSJLY = $("#SJLYMC").val();
                                if (XMDM == "") {
                                    XMDM = YSXMDM;
                                }
                                if (CWKM == "") {
                                    if (YSCWKM != "") {
                                        CWKM = YSCWKM;
                                    }
                                    if ($("#CWKM").combotree("getText") != "") {
                                        CWKM = $("#HidlastCWKM").val();
                                    }
                                }
                                if (SJLY == "") {
                                    SJLY = YSSJLY;
                                }
                                if (FLDM != "" && XMDM != "" && CWKM != "") {
                                    EditDYData(YSXMFL, YSXMDM, YSCWKM, FLDM, XMDM, CWKM,SJLY,YSSJLY);
                                }
                                else {
                                    alert("请填写完整信息！");
                                }
                                break;
                            default:

                        }
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function (param) {
                        $('#' + name).dialog('close');
                    }
                }],
                content: con
            });
        }
        function EditDYData(YSXMFL, YSXMDM, YSCWKM, XMFL, XMDM, CWKM,SJLY,YSSJLY) {
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=EditDYData&YSXMFL=' + YSXMFL + '&YSXMDM=' + YSXMDM + '&YSCWKM=' + YSCWKM + '&XMFL=' + XMFL + '&XMDM=' + XMDM + '&CWKM=' + CWKM + "&SJLY=" + SJLY +"&YSSJLY=" +YSSJLY,
                async: false,
                cache: false,
                success: function (result) {
                    if (result != "") {
                        var arr = new Array();
                        arr = result.split(',');
                        if (arr[0] == "1") {
                            alert("修改成功！");
                            InitDY();
                            $('#dialogDY').dialog('close');
                        }
                        else if (arr[0] == "0") {
                            alert(arr[1]);
                            return;
                        }
                    }
                    else {
                        alert("修改失败！");
                        return;
                    }
                }
            });
        }
        function AddDYData(XMFL, XMDM, CWKM,SJLY) {
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=AddDYData&XMFL=' + XMFL + '&XMDM=' + XMDM + '&CWKM=' + CWKM + "&SJLY=" + SJLY,
                async: false,
                cache: false,
                success: function (result) {
                    if (result != "") {
                        var arr = new Array();
                        arr = result.split(',');
                        if (arr[0] == "1") {
                            alert("添加成功！");
                            InitDY();
                            $('#dialogDY').dialog('close');
                        }
                        else if (arr[0] == "0") {
                            alert(arr[1]);
                            return;
                        }
                    }
                    else {
                        tishi("保存失败！");
                        return;
                    }
                }
            });
        }
        function Edit(flag) {
            param = flag;
            switch (flag) {
                case "add":
                    var XMFL = $("#XMFL").combobox("getText");
                    if (XMFL == "") {
                        alert("请先选择一个项目分类！");
                        return;
                    }
                    $("#TxtXMFL").val(XMFL);
                    break;
                case "edit":
                    var rows = $("#DYGXGrid").datagrid("getSelected")
                    if (rows == null) {
                        alert("请先选择一条记录！");
                        return;
                    }
                    else {
                        $("#TxtXMFL").val(rows.FLMC);
                        $("#HidSJLY").val(rows.SJLY);
                        $("#HidXMDM").val(rows.XMDM);
                        $("#HidCWKM").val(rows.F_KMBH);
                        $("#XMMC").combotree("setText", rows.XMMC);
                        if (rows.F_KMMC != "") {
                            $("#HidlastCWKM").val(rows.F_KMBH)
                            $("#CWKM").combotree("setText", rows.F_KMMC);
                        }
                        if (rows.SJLY != "") {
                            $("#HidSJLY").val(rows.SJLY)
                            $("#SJLY").combobox("setText", rows.SJLYMC);
                        }
                    }
                    break;
            }
            $("#dialogDY").dialog('open');
        }
        function Remove() {
            var rows = $("#DYGXGrid").datagrid("getSelected")
            if (rows == null) {
                alert("请先选择一条记录！");
                return;
            }
            else {
                $.messager.confirm('警告', '确定要删除这条记录吗？', function (r) {
                    if (r) {
                        var YSXMFL = rows.XMFL;
                        var YSSJLY = rows.SJLY;
                        var YSXMDM = rows.XMDM;
                        var YSCWKM = rows.F_KMBH;
                        $.ajax({
                            type: 'get',
                            url: 'YSCWDY.ashx?action=Remove&YSXMFL=' + YSXMFL + '&YSXMDM=' + YSXMDM + '&YSCWKM=' + YSCWKM + "&SJLY=" + YSSJLY,
                            async: false,
                            cache: false,
                            success: function (result) {
                                if (result != "") {
                                    var arr = new Array();
                                    arr = result.split(',');
                                    if (arr[0] == "1") {
                                        alert("删除成功！");
                                        InitDY();
                                    }
                                    else if (arr[0] == "0") {
                                        alert(arr[1]);
                                        return;
                                    }
                                }
                                else {
                                    tishi("保存失败！");
                                    return;
                                }
                            }
                        });
                    }
                });
            }
        }
        function LoadXMFL() {
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=LoadXMFL',
                async: false,
                cache: false,
                success: function (result) {
                    $("#XMFL").combobox("clear");
                    var data = JSON.parse(result)
                    $("#XMFL").combobox("loadData", data);
                    if (result != "") {
                        $("#XMFL").combobox("setValue", data[0].id);
                    }
                }
            });
        }

        function InitDY() {
            var Year = $("#Year").val();
            var XMFL = $("#XMFL").combobox("getValue");
            if (Year == "") {
                alert("请选择年！");
                return;
            }
            if (XMFL == "") {
                alert("请选择项目分类!");
                return;
            }
            $.ajax({
                type: 'get',
                url: 'YSCWDY.ashx?action=InitDY&Year=' + Year + '&XMFL=' + XMFL,
                async: false,
                cache: false,
                success: function (result) {
                    result = result.replace(/\\/g, '\\\\');
                    var Data = eval("(" + result + ")")
                    $("#DYGXGrid").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                }
            });
        }
        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }
                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }
    </script>
</head>
<body>
    <div class="easyui-tabs" id="tt" style="width: 100%; height: 100%;">
        <form id="form1" runat="server">
        <div id="Div1" runat="server" class="easyui-layout" style="width: 100%; height: 100%;">
            <div region="north" border="true" title="" style="height: 40px; padding: 2px; background: #B3DFDA;">
                <div style="float: left">
                    年：<input id="Year" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',readOnly:true})" />
                    &nbsp;</div>
                <div id="JHDiv" style="float: left">
                    项目分类：<input class="easyui-combobox" id="XMFL" name="XMFL" style="width: 150px" data-options="valueField:'id',textField:'text'" />&nbsp;
                </div>
                <input type="button" class="button5" value="查询" onclick="InitDY()" />
            </div>
            <div id="DYGXDiv" region="center" title="模版">
                <div id="DivDYGX" style="width: 100%; height: 100%; display: block;">
                    <table id="DYGXGrid" style="width: 100%; height: 100%;">
                    </table>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div id="dialogDY">
    </div>
    <div id="DYXX">
        <table>
            <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                            项目分类:
                        </th>
                        <th>
                            <input name="XMFL" id="TxtXMFL" type="text" style="width: 200px" disabled="disabled" />
                        </th>
                    </div>
                </td>
                <td>
                    <div>
                        <th>
                            项目名称:
                        </th>
                        <th>
                        <input class="easyui-combotree" id="XMMC" name="XMMC" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="padding-top: 50px;">
                        <th>
                            财务科目名称:
                        </th>
                        <th>
                        <input class="easyui-combotree" id="CWKM" name="CWKM" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>

                   <td>
                    <div style="padding-top: 50px;">
                        <th>
                            数据来源:
                        </th>
                        <th>
                        <input class="easyui-combobox" id="SJLY" name="SJLY" style="width: 200px" data-options="valueField:'id',textField:'text'" />
                        </th>
                    </div>
                </td>
            </tr>
            <input type="hidden" id="HidXMDM" />
            <input type="hidden" id="Hidden1" />
            <input type="hidden" id="HidSJLY" />
            <input type="hidden" id="HidlastCWKM" />
        </table>
    </div>
</body>
</html>
