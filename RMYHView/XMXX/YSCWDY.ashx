﻿<%@ WebHandler Language="C#" Class="YSCWDY" %>

using System;
using System.Web;
using System.Data;
using System.Text;
using RMYH.BLL;
using System.Web.SessionState;

public class YSCWDY : System.Web.UI.Page, IRequiresSessionState, IHttpHandler
{
    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "", Year = "", XMFL = "", XMDM = "", CWKM = "", YSXMFL = "", YSXMDM = "", YSCWKM = "",SJLY="";
        HttpCookie cookie = context.Request.Cookies["RMYH_LOGIN"];
        if (cookie != null)
        {
            Year = cookie.Values.Get("YY");
            Session["YEAR"] = Year;
        }
        else
        {
            Year = Session["YEAR"].ToString();
        }
        switch (action)
        {
            case "LoadXMFL":
                res = GetXMFL();
                break; 
            case "InitDY":
                Year = context.Request.QueryString["Year"];
                XMFL = context.Request.QueryString["XMFL"];
                res = InitDY(Year, XMFL);
                break;
            case "LoadXMMC":
                XMDM = context.Request.QueryString["XMDM"];
                res = LoadXMMC(XMDM);
                break;
            case "LoadCWKM":
                string BM = context.Request.QueryString["BM"];
                res = LoadCWKM(BM, Year);
                break;
            case "LoadSJLY"://加载数据来源
                res = LoadSJLY();
                break;
            case "AddDYData":
                XMFL = context.Request.QueryString["XMFL"];
                XMDM = context.Request.QueryString["XMDM"];
                CWKM = context.Request.QueryString["CWKM"];
                SJLY = context.Request.QueryString["SJLY"]; ///数据来源
                res = AddDYData(XMFL,XMDM,CWKM,SJLY);
                break;
            case "EditDYData":
                YSXMFL = context.Request.QueryString["YSXMFL"];
                YSXMDM = context.Request.QueryString["YSXMDM"];
                YSCWKM = context.Request.QueryString["YSCWKM"];
                XMFL = context.Request.QueryString["XMFL"];
                XMDM = context.Request.QueryString["XMDM"];
                CWKM = context.Request.QueryString["CWKM"];
                SJLY = context.Request.QueryString["SJLY"];///数据来源
                var YSSJLY = context.Request.QueryString["SJLY"];///数据来源
                res = EditDYData(YSXMFL,YSXMDM, YSCWKM,XMFL, XMDM, CWKM,SJLY,YSSJLY);
                break;
            case "Remove":
                YSXMFL = context.Request.QueryString["YSXMFL"];
                YSXMDM = context.Request.QueryString["YSXMDM"];
                YSCWKM = context.Request.QueryString["YSCWKM"];
                SJLY = context.Request.QueryString["SJLY"];      ///数据来源          
                res = Remove(YSXMFL, YSXMDM, YSCWKM,SJLY);
                break;
        }
        context.Response.Write(res);
    }
    public string Remove(string YSXMFL, string YSXMDM, string YSCWKM,string SJLY)
    {
        string res = "";
        string sql = "delete from TB_KMFYDY where F_KMBH='" + YSCWKM + "' AND XMFL='" + YSXMFL + "' AND XMDM='" + YSXMDM + "' AND SJLY=" + SJLY + "";
        string flag = bll.ExecuteSql(sql).ToString();
        if (flag == "1")
        {
            res = "1,删除成功！";
        }
        else
        {
            res = "0,删除失败！";
        }
        return res;
    }
    public string EditDYData(string YSXMFL, string YSXMDM, string YSCWKM, string XMFL, string XMDM, string CWKM,string SJLY,string YSSJLY)
    {
        string res = "";
        string existsql = "SELECT * FROM TB_KMFYDY WHERE F_KMBH='" + (YSCWKM =YSCWKM=="undefined"?null:YSCWKM)+ "' AND XMFL='" + YSXMFL + "' AND XMDM='" + YSXMDM + "'";
        DataTable existdt = bll.Query(existsql).Tables[0];
        if (existdt.Rows.Count > 0)
        {
            string sql = "SELECT * FROM TB_KMFYDY WHERE F_KMBH='" + CWKM + "' AND XMFL='" + XMFL + "' AND XMDM='" + XMDM + "' AND F_KMBH!='" + YSCWKM + "' AND XMFL!='" + YSXMFL + "' AND XMDM!='" + YSXMDM +"' AND SJLY!="+SJLY+ "";
            DataTable dt = bll.Query(sql).Tables[0];
            if (dt.Rows.Count > 0)
            {
                res = "0,已存在此记录";
            }
            else
            {
                string uptsql = "update TB_KMFYDY set F_KMBH='" + CWKM + "' , XMFL='" + XMFL + "' , XMDM='" + XMDM + "' , SJLY!="+SJLY+ "+ where F_KMBH='" + YSCWKM + "' AND XMFL='" + YSXMFL + "' AND XMDM='" + YSXMDM + "'";
                try
                {
                    string flag = bll.ExecuteSql(uptsql).ToString();
                    if (flag == "1")
                    {
                        res = "1,修改成功！";
                    }
                    else
                    {
                        res = "0,修改失败！";
                    }
                }
                catch
                { }
            }
        }
        else
        {
            string insertsql = "insert into TB_KMFYDY(F_KMBH,XMFL,XMDM,SJLY) VALUES('" + CWKM + "','" + XMFL + "','" + XMDM + "',"+ SJLY+")";
            string flag = bll.ExecuteSql(insertsql).ToString();
            if (flag == "1")
            {
                res = "1,修改成功！";
            }
            else
            {
                res = "0,修改失败！";
            }
        }
        return res;
    }
    public string AddDYData(string XMFL, string XMDM, string CWKM,string SJLY)
    {
        string res = "";
        string sql = "SELECT * FROM TB_KMFYDY WHERE F_KMBH='" + CWKM + "' AND XMFL='" + XMFL + "' AND XMDM='" + XMDM + "' AND SJLY=" + SJLY + "";
        DataTable dt = bll.Query(sql).Tables[0];
        if (dt.Rows.Count > 0)
        {
            res = "0,已存在此记录";
        }
        else
        {
            string insertsql = "insert into TB_KMFYDY(F_KMBH,XMFL,XMDM,SJLY) VALUES('" + CWKM + "','" + XMFL + "','" + XMDM + "'," + SJLY + ")";
            string flag = bll.ExecuteSql(insertsql).ToString();
            if (flag == "1")
            {
                res = "1,添加成功！";
            }
            else
            {
                res = "0,添加失败！";
            }
        }
        return res;
    }
    public string LoadCWKM(string BM,string Year)
    {
        StringBuilder Str = new StringBuilder();
        ChildCWKM(Year, BM, ref Str);
        return Str.ToString();
    }

 
    /// <summary>
    /// 获取部门子节点
    /// </summary>
    /// <param name="ParID">父辈ID</param>
    /// <param name="Str"></param>
    public void ChildCWKM(string Year, string BM, ref StringBuilder Str)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "";
        if (BM == "")
        {
            sql = "SELECT F_KMBH,F_KMMC,F_KMJS,F_MX FROM TB_CWLSKMXX WHERE YY='" + Year + "' and F_KMJS=1";
        }
        else
        {
            string CCJB = "";
            string CCJBsql = "SELECT F_KMJS FROM TB_CWLSKMXX WHERE YY='" + Year + "' AND F_KMBH = '" + BM + "'";
            DataTable CCJBDT = bll.Query(CCJBsql).Tables[0];
            if (CCJBDT.Rows.Count > 0)
            {
                CCJB = CCJBDT.Rows[0]["F_KMJS"].ToString();
            }
            sql = "SELECT F_KMBH,F_KMMC,F_KMJS,F_MX FROM TB_CWLSKMXX WHERE YY='" + Year + "' and F_KMJS=" + (Convert.ToInt32(CCJB) + 1) + " and F_KMBH like '" + BM + "%'";
        }
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                DataRow dr = DS.Tables[0].Rows[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["F_KMBH"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["F_KMMC"].ToString().Trim() + "\"");
                Str.Append(",");
                //Str.Append("\"F_KMJS\":\"" + dr["F_KMJS"].ToString().Trim() + "\"");
                //Str.Append(",");
                //Str.Append("\"F_MX\":\"" + dr["F_MX"].ToString().Trim() + "\"");
                //Str.Append(",");
                if (dr["F_MX"].ToString() == "0")
                {
                    Str.Append("\"state\":\"closed\"");
                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    public string LoadXMMC(string XMDM)
    {
        StringBuilder Str = new StringBuilder();
        ChildXMMC(XMDM, ref Str);
        return Str.ToString();
    }
    public void ChildXMMC(string XMDM, ref StringBuilder Str)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "SELECT XMDM,XMMC,PAR,YJDBZ FROM TB_XMXX WHERE XMLX<>'13' AND PAR='" + XMDM + "' ORDER BY XMBM ";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        if (DS.Tables[0].Rows.Count > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                DataRow dr = DS.Tables[0].Rows[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["XMDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["XMMC"].ToString().Trim() + "\"");
                Str.Append(",");
                //Str.Append("\"F_KMJS\":\"" + dr["F_KMJS"].ToString().Trim() + "\"");
                //Str.Append(",");
                //Str.Append("\"F_MX\":\"" + dr["F_MX"].ToString().Trim() + "\"");
                //Str.Append(",");
                string sqlChild = "SELECT * FROM TB_XMXX WHERE XMLX<>'13' AND PAR='" + dr["XMDM"].ToString() + "'";
                DataSet nest = BLL.Query(sqlChild);
                if (nest.Tables[0].Rows.Count > 0)
                {
                    Str.Append("\"state\":\"closed\"");
                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    
    public string InitDY(string Year, string XMFL)
    {
        string res = "";
        string JHFASql = "SELECT JHFADM FROM TB_JHFA WHERE YY='" + Year + "' and YSBS='5' AND FABS='3' AND DELBZ='1'";
        DataTable JHDt = bll.Query(JHFASql).Tables[0];
        string JHFADM = "";
        if (JHDt.Rows.Count > 0)
        { 
            JHFADM = JHDt.Rows[0]["JHFADM"].ToString();
            string sql = " SELECT DISTINCT  A.XMFL,D.XMMC  FLMC,A.XMDM,C.XMMCEX XMMC,A.F_KMBH,B.F_KMMC,C.XMBM,CS.XMMC SJLYMC,CS.XMDH_A SJLY  FROM TB_KMFYDY A ";
            sql += "LEFT JOIN TB_CWLSKMXX B ON A.F_KMBH=B.F_KMBH AND B.YY='2018' ,TB_XMXX C,TB_XMXX D,XT_CSSZ CS ";
            sql += "WHERE CS.XMDH_A=A.SJLY AND CS.XMFL='CWSJLY' AND  A.XMFL=D.XMDM AND D.XMLX='13' AND A.XMDM=C.XMDM AND A.XMFL='" + XMFL + "' UNION ";
            sql += " SELECT DISTINCT B.XMFL,D.XMMC FLMC,C.XMDM,C.XMMCEX XMMC,NULL,NULL ,C.XMBM,NULL,NULL ";
            sql += "FROM TB_BBFYSJ B,TB_XMXX C,TB_XMXX D WHERE     B.JHFADM='" + JHFADM + "' AND  B.XMDM=C.XMDM ";
            sql += "AND B.XMFL=D.XMDM AND D.XMLX='13' AND C.XMDM NOT IN(SELECT XMDM FROM TB_KMFYDY WHERE   B.XMFL=XMFL) ";
            sql += "AND B.XMFL='" + XMFL + "' ORDER BY XMFL,XMBM";
            DataTable dt = bll.Query(sql).Tables[0];
            res = FillData("easyui-datagrid", sql);
        }
        return res;
    }

    /// <summary>
    /// 加载数据来源
    /// </summary>
    /// <param name="SJLY">数据来源</param>
    /// <param name="Year">年份</param>
    /// <returns>数据来源列表</returns>
    public string LoadSJLY()
    {
        string sql = "SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='CWSJLY'";
        DataSet set = bll.Query(sql);
      
        StringBuilder Str = new StringBuilder();
        if (set.Tables[0].Rows.Count > 0)
        {
            Str.Append("[");
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                Str.Append("{\"id\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMDH_A"].ToString() + "\"" + ",");
                Str.Append("\"text\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMMC"].ToString() + "\"" + "}");
                if (i < set.Tables[0].Rows.Count - 1)
                {
                    Str.Append(",");
                }
            }
            Str.Append("]");
        }
        return Str.ToString();
    }


    
    public string GetXMFL()
    {
        string sql = "SELECT XMDM,XMMC,XMBM FROM TB_XMXX WHERE XMLX='13'";
        DataSet set = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        if (set.Tables[0].Rows.Count > 0)
        {
            Str.Append("[");
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                Str.Append("{\"id\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMDM"].ToString() + "\"" + ",");
                Str.Append("\"text\":");
                Str.Append("\"" + set.Tables[0].Rows[i]["XMMC"].ToString() + "\"" + "}");
                if (i < set.Tables[0].Rows.Count - 1)
                {
                    Str.Append(",");
                }
            }
            Str.Append("]");
        }
        return Str.ToString();
    }
    private string FillData(string _type, string _sql)
    {
        if (!string.IsNullOrEmpty(_type) && !string.IsNullOrEmpty(_sql))
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = new DataSet();
            ds = bll.Query(_sql);
            string res = "[]";
            if (ds.Tables[0].Rows.Count > 0)
            {
                switch (_type)
                {
                    case "easyui-tree":
                        res = gettreenode("", ds.Tables[0]);
                        break;
                    case "easyui-datagrid":
                        res = getdatagrid(ds.Tables[0]);
                        break;
                }
            }
            return res;
        }
        else
        {
            return "";
        }
    }
    public bool IsReusable {
        get {
            return false;
        }
    }
    private string getdatagrid(DataTable _table)
    {
        string res = "";
        string sr = "";
        res += "{\"total\":";
        res += _table.Rows.Count;
        res += ",";
        res += "\"rows\":[";
        for (int i = 0; i < _table.Rows.Count; i++)
        {
            DataRow row = _table.Rows[i];
            sr = "";
            if (row != null)
            {
                res += "{";
                for (int j = 0; j < _table.Columns.Count; j++)
                {
                    if (sr == "")
                    {
                        sr = _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                    else
                    {
                        sr += "," + _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                }
                if (i == _table.Rows.Count - 1)
                {
                    res += sr + "}";
                }
                else
                {
                    res += sr + "},";
                }
            }
        }
        return res + "]}";
    }
    private string gettreenode(string _fbdm, DataTable _table)
    {
        string res = "";
        DataRow[] rows = _table.Select(_table.Columns[2].ColumnName + "='" + _fbdm + "'");
        for (int i = 0; i < rows.Length; i++)
        {
            DataRow row = rows[i];
            if (row != null)
            {
                if (row[3].ToString() == "1")
                {
                    if (res == "")
                    {
                        res = "{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\"}";
                    }
                    else
                    {
                        res += ",{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\"}";
                    }
                }
                else if (row[3].ToString() == "0")
                {
                    if (res == "")
                    {
                        res = "{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                    else
                    {
                        res += ",{\"id\":\"" + row[0].ToString() + "\",\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                }
            }
        }
        return "[" + res + "]";
    }
}