﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using RMYH.DBUtility;
using System.Data;
using System.Text;
using RMYH.Model;
using System.Collections;

public partial class FYSZ_BMFYGL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(FYSZ_BMFYGL));
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return GetDataList.GetDataListstring("10144520", "", new string[] { ":YY" }, new string[] { Session["YY"].ToString() }, false, trid, id, intimgcount);
    }

    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144520");
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        try
        {

            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string[] arr = new string[values.Length * 2];
            ArrayList list = new ArrayList();
            string FYYSDM = "";
            string XMFLDM = "";
            string XMDM = "";
            string SQMBDM = "";
            string NSQMBDM = "";
            ArrayList al = new ArrayList();
            for (int i = 0; i < values.Length; i++)
            {
                FYYSDM = values[i].Split('|')[0].Trim().Split('.')[0];
                XMFLDM = values[i].Split('|')[1].Trim().Split('.')[0];
                XMDM = values[i].Split('|')[2].Trim().Split('.')[0];
                SQMBDM = values[i].Split('|')[3].Trim();
                NSQMBDM = values[i].Split('|')[8].Trim();
                if (!list.Contains(values[i].Split('|')[0].Trim() + values[i].Split('|')[1].Trim() + values[i].Split('|')[2].Trim() + values[i].Split('|')[3].Trim() + values[i].Split('|')[8].Trim()))
                {
                    list.Add(values[i].Split('|')[0].Trim() + values[i].Split('|')[1].Trim() + values[i].Split('|')[2].Trim() + values[i].Split('|')[3].Trim() + values[i].Split('|')[8].Trim());
                }
                else
                {
                    al.Add(i + 1);
                }
                if (ID[i].Trim() != "")
                {
                    if (values[i].IndexOf(".") > 0)
                    {
                        //获取项目代码
                        string id = values[i].Split('|')[0].Split('.')[0].ToString();
                        string xmdm = values[i].Split('|')[2].Split('.')[0].ToString();
                        arr[i * 2] = "delete from TB_BMFYGL where FYYSDM='" + values[i].Split('|')[6] + "'and XMFL='" + values[i].Split('|')[7] + "' and XMDM='" + values[i].Split('|')[5] + "' and SQMBDM='" + values[i].Split('|')[11] + "' and NSQMBDM='" + values[i].Split('|')[12]+ "'";
                        arr[i * 2 + 1] = "insert into TB_BMFYGL(FYYSDM,XMFL,XMDM,SQMBDM,FJMBDM,NSQMBDM,NFJMBDM,JELY) values ('" + id + "','" + values[i].Split('|')[1] + "','" + xmdm + "','" + values[i].Split('|')[3] + "','" + values[i].Split('|')[4] + "','" + values[i].Split('|')[8] + "','" + values[i].Split('|')[9] + "','" + values[i].Split('|')[10] + "')";
                    }
                    //查询数据库表里的数据
                    string sql = "select * from TB_BMFYGL where FYYSDM='" + FYYSDM + "' and XMFL='" + XMFLDM + "' and XMDM='" + XMDM + "' and SQMBDM='" + SQMBDM + "'  and NSQMBDM='" + NSQMBDM + "'";
                    DataSet se = bll.Query(sql);
                    //数据库如果有就执行+1
                    if (se.Tables[0].Rows.Count > 1)
                    {
                        al.Add(i + 1);
                    }
                }
                else if (values[i].IndexOf(".") > 0)
                {
                    //获取项目代码
                    string id = values[i].Split('|')[0].Split('.')[0].ToString();
                    string xmdm = values[i].Split('|')[2].Split('.')[0].ToString();
                    arr[i * 2] = "delete from TB_BMFYGL where FYYSDM='" + values[i].Split('|')[6] + "'and XMFL='" + values[i].Split('|')[7] + "' and XMDM='" + values[i].Split('|')[5] + "' and SQMBDM='" + SQMBDM + "' and NSQMBDM='" + NSQMBDM + "'";
                    arr[i * 2 + 1] = "insert into TB_BMFYGL(FYYSDM,XMFL,XMDM,SQMBDM,FJMBDM,NSQMBDM,NFJMBDM,JELY) values ('" + id + "','" + values[i].Split('|')[1] + "','" + xmdm + "','" + values[i].Split('|')[3] + "','" + values[i].Split('|')[4] + "','" + values[i].Split('|')[8] + "','" + values[i].Split('|')[9] + "','" + values[i].Split('|')[10] + "')";
                    //查询数据库表里的数据
                    string sql = "select * from TB_BMFYGL where FYYSDM='" + FYYSDM + "' and XMFL='" + XMFLDM + "' and XMDM='" + XMDM + "' and SQMBDM='" + SQMBDM + "'  and NSQMBDM='" + NSQMBDM + "'";
                    DataSet se = bll.Query(sql);
                    //数据库如果有就执行+1
                    if (se.Tables[0].Rows.Count > 0)
                    {
                        al.Add(i + 1);
                    }
                }
            }
            //如果有重复的数据
            if (al.Count > 0)
            {

                for (int i = 0; i < al.Count - 1; i++)
                {
                    for (int j = i + 1; j < al.Count; j++)
                    {
                        if (list[i].Equals(al[j]))
                        {
                            al.RemoveAt(j);
                            j--;
                        }
                    }
                }
                string cf = "";
                for (int i = 0; i < al.Count; i++)
                {
                    cf += al + ",";
                }

                return "有重复数据，请验证后保存";
            }
            else
            {
                DbHelperOra.ExecuteSql(arr, null);
                return "保存成功";
            }
        }
        catch
        {
            return "保存失败";
        }
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int DeleteData(string id)
    {
        string ret = "";
        string[] ID = id.Split(',');
        ret = "delete from TB_BMFYGL where FYYSDM='" + ID[0] + "' AND XMFL='" + ID[1]+ "' AND XMDM='" + ID[2] + "'AND SQMBDM='"+ID[3]+"' AND NSQMBDM='"+ID[4]+"' ";
        int Count = DbHelperOra.ExecuteSql(ret);
        return Count;
    }
    #endregion
}