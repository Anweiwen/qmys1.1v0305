﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
using RMYH.DBUtility;


public partial class ContentList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ID = Request.QueryString["ID"].ToString();
        gettree(ID);
    }
    public void gettree(string id)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        TB_ZDSXBModel model = new TB_ZDSXBModel();
        model = bll.GetModel(id);
        DS = bll.Query(model.SQL.Replace(":YY", "'" + HttpContext.Current.Session["YY"].ToString() + "'"));
        crertetree(TreeView1.Nodes, DS.Tables[0], "0");
    }
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("PAR='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["BM"].ToString();
            node.Text = row[i]["MC"].ToString();
            if (tb.Select("PAR='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:$(\"#Hid_DM\").val('" + node.Value + "'+\".\"+'"+ node.Text + "');onselects()";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }
}
