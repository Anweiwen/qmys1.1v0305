﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YSAutoTZ.aspx.cs" Inherits="YSTZ_YSAutoTZ" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	    <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
        <link rel="stylesheet" type="text/css" href="../CSS/style1.css" />
	    <script type="text/javascript" src="../JS/jquery.min.js"></script>
        <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
        <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
        <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
        <script type="text/javascript">
            var YSIndex = 0, YSArr, YSMB,LastMBMC="";
            function winResize() {
                var iwidth = $(window).width() - 10;     //减去10与body中margin:5px共同作用:为body留的边距  
                var iheight = $(window).height() - 10;
                $("#Fr4").layout({ width: iwidth, height: iheight });
            }
            //清空模版:
            function ClearMb() {
                $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
            }

            function Init() {
                ClearMb();
                $.ajax({
                    type: 'get',
                    url: 'YSAutoTZHandler.ashx',
                    data: {
                        action: 'AutoMBList',
                        JHFADM: $("#SelJHFA").val(),
                        MBMC: $("#TxtMB").val(),
                        ZYDM: $("#SelZY").val()
                    },
                    async: true,
                    cache: false,
                    success: function (result) {
                        if (result != "" && result != null) {
                            var Data = eval("(" + result + ")");
                            $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                        }
                    },
                    error: function () {
                        alert("加载失败!");
                    }
                });
            }


            //初始化EasyUI dataGrid表格设置
            function InitDataGrid() {
                $('#TabMB').datagrid({
                    singleSelect:false,
                    autoRowHeight: false,
                    rownumbers: true,
                    fitColumns: true,
                    pagination: true,
                    pageList: [10, 20, 30, 40, 50],
                    columns: [[
                                { field: 'MBDM', title: '模板代码', hidden: true },
                                { field: 'MBMC', title: '模板名称', width: 180 },
					            { field: 'MBWJ', title: '模板文件', hidden: true },
                                { field: 'MBZQ', title: '模板周期', hidden: true },
                                { field: 'ZYDM', title: '作业代码', hidden: true },
					            { field: 'MBLX', title: '模板类型', hidden: true }
				            ]]
                });
            }

            //EasyUI客户端分页
            function pagerFilter(data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }
                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
            //初始化计划方案类别
            function BindFALB() {
                var rtn = YSTZ_YSAutoTZ.GetFALB().value;
                $("#SelFALX").html(rtn);
            }
            //初始化年份
            function BindYY() {
                var rtn = YSTZ_YSAutoTZ.GetYY().value;
                $("#TxtYY").val(rtn);
            }
            //初始化月、季度
            function BindData() {
                var rtn = YSTZ_YSAutoTZ.GetData($("#SelFALX").val()).value;
                if (rtn == "-1") {
                    $("#<%=LabMM.ClientID%>").attr("style", "display:none");
                    $("#SelData").attr("style", "display:none");
                }
                else {
                    if ($("#SelFALX").val() == "2") {
                        $("#<%=LabMM.ClientID%>").html("季度：");
                    }
                    else {
                        $("#<%=LabMM.ClientID%>").html("月份：");
                    }
                    $("#<%=LabMM.ClientID%>").attr("style", "display:display");
                    $("#SelData").attr("style", "display:display");
                    document.getElementById("SelData").options.length = 0
                    $("#SelData").html(rtn);
                }

            }
            //初始化计划方案
            function BindJHFA() {
                var MM = "";
                if ($("#SelFALX").val() != "3") {
                    MM = $("#SelData").val();
                }
                var rtn = YSTZ_YSAutoTZ.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM, $("#SelHSZX").val()).value;
                $("#SelJHFA").html(rtn);
            }
            //初始化部门
            function InitZY() {
                var rtn = YSTZ_YSAutoTZ.InitZY().value;
                $("#SelZY").html(rtn);
            }
            //初始化核算中心
            function InitHSZX() {
                $.ajax({
                    type: 'get',
                    url: '../Excels/Data/ExcelData.ashx?action=InitHszx',
                    data: {
                        yy: '<%=YY%>',
                        userid: '<%=USERDM%>'
                    },
                    async: false,
                    cache: false,
                    success: function (result) {
                        $("#SelHSZX").empty();
                        $("#SelHSZX").append(result);
                    },
                    error: function (req) {
                        alert("加载核算中心失败！");
                        return;
                    }
                });
            }

            function InitYSYF() {
                $.ajax({
                    type: 'get',
                    url: '../Excels/Data/ExcelData.ashx',
                    data: {
                        action: "InitYsyf"
                    },
                    async: false,
                    cache: false,
                    success: function (result) {
                        $("#SelYSYF").empty();
                        $("#SelYSYF").append(result);
                    },
                    error: function (req) {
                        alert(req.responseText);
                        return;
                    }
                });
            }

            //方案改变事件
            function ChageFALX() {
                BindData();
                BindJHFA();
            }

            //预算模板自动调整上级模板
            function AutoYSTZ() {
                var MM = "", JD = "";
                if ($("#SelFALX").val() != "3") 
                {
                    if ($("#SelFALX").val() == "2") 
                    {
                        JD = $("#SelData").val();
                    }
                    else 
                    {
                        MM = $("#SelData").val();
                    }
                }
                YSMB = YSArr[YSIndex].split(',');
                GetOpenMBURL(YSMB[0],YSMB[1],YSMB[2],YSMB[3],MM,JD);
            }

            //打开下一个模板，更新Tab页的内容
            function UpdateTabContent(MBMC,content) {
                var tab;
                var YSTab = window.parent.$('#tt')
                if (LastMBMC == "") 
                {
                    YSTab.tabs("select", "预算自动调整");
                    tab = YSTab.tabs('getTab', "预算自动调整");
                }
                else 
                {
                    YSTab.tabs("select", LastMBMC);
                    tab = YSTab.tabs('getTab', LastMBMC);
                }
                LastMBMC = MBMC;
                YSTab.tabs('update', 
                {
                    tab: tab,
                    options: {
                        title: MBMC,
                        content: content
                    }
                });
            }

            function AutoTZMB() {
                //如果不是ie浏览器：
                if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                    $.messager.alert("提示框", "请使用IE内核的浏览器进行此操作！");
                    return;
                }
                else 
                {
                    $.messager.confirm('提示框：', '您确定要进行模板自动调整操作吗?', function (r) {
                        if (r) {
                            var DM = "";
                            var rows = $('#TabMB').datagrid('getSelections');
                            if (rows.length == 0) {
                                $.messager.alert("提示框", "请选择要自动调整的模板项！");
                                return;
                            }
                            else {
                                for (var i = 0; i < rows.length; i++) {
                                    DM = DM == "" ? rows[i].MBDM : DM + "," + rows[i].MBDM;
                                }

                                //获取预算调整相关的所有上级模板代码
                                var rtn = YSTZ_YSAutoTZ.YSTZParMB($("#SelJHFA").val(), DM).value;
                                if (rtn != "") {
                                    YSArr = rtn.split('|');
                                    //预算模板自动调整上级模板
                                    AutoYSTZ();
                                }
                                else {
                                    $.messager.alert("提示框", "您没有要调整的相关上级模板！");
                                    return;
                                }
                            }
                        }
                    });
                }
            }

            function GetOpenMBURL(MBDM, MBMC, MBWJ, MBLX, MM, JD) {
                var url = geturlpath() + "Excels/Data/MB.aspx"
                 + "?MBDM=" + MBDM + "&mbmc=" + getchineseurl(MBMC) + "&mbwj=" + getchineseurl(MBWJ) + "&mblx=" + MBLX
                 + "&yy=" + $("#TxtYY").val()+ "&nn=" + MM + "&jd=" + JD
                 + "&fadm=" + $("#SelJHFA").val() + "&fadm2=" + $("#SelJHFA").val()
                 + "&famc=" + getchineseurl($("#SelJHFA").find("option:selected").text())
                 + "&ysyf="+$("#SelYSYF").val()+"&userdm=<%=USERDM%>"
                 + "&username=" + getchineseurl("<%=USERNAME%>") + "&falb=" + $("#SelFALX").val()
                 + "&mblb=<%=MBLB %>" + "&isexe="
                 + "&hszxdm=" + $("#SelHSZX").val() + "&sfzlc=1&loginyy=<%=YY%>";

                //如果不是ie浏览器：
                if (!(window.ActiveXObject || "ActiveXObject" in window)) 
                {
                    url = decodeURI(url);
                }

                var content = "<iframe  id=\"IfrZT\" name=\"IfrZT\" style=\"width: 100%;height:100%\" src=" + url + "  frameborder=\"0\"; marginheight=\"0\" marginwidth=\"0\"></iframe>";
                //调用父页面中的打开Excel方法
                UpdateTabContent(MBMC,content);
//                window.parent.UpdateTabContent(LastMBMC,MBMC,content);
            }
            function geturlpath() {

                var path = window.document.location.pathname.substring(1);
                var Len = path.indexOf("/");
                return  "/" + path.substring(0, Len + 1);

            }
            $(function () {
                winResize();
                $(window).resize(function () {
                    winResize();
                });
                BindFALB();
                BindYY();
                InitHSZX();
                BindData();
                BindJHFA();
                InitZY();
                InitYSYF();
                InitDataGrid();
                Init();
            });
        </script>
        <style type="text/css">
            .style2
            {
                height: 28px;
            }
        </style>
  </head>
  <body>
     <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
      <div region="north" border="true" style="height: 70px; padding: 1px;background: #B3DFDA;">
        <table>
            <tr>
                <td class="style2">
                    核算中心：<select id="SelHSZX" onchange="BindJHFA()"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    预算月份： <select id="SelYSYF" disabled="disabled"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    使用部门： <select id="SelZY"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    模板名称：<input class="easyui-textbox" type="text" id="TxtMB" style="width:120px" />&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="style2">
                    周期：<select id="SelFALX" onchange="ChageFALX()"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                     年份：<input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:BindJHFA()})"  style="width:80px"  />&nbsp;&nbsp;
                    <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label> 
                    <select id="SelData" onchange="BindJHFA()"  style="width:90px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2" colspan="2">
                    方案名称： <select id="SelJHFA"  style="width:180px;"></select>&nbsp;&nbsp;
                    <input type="button" class="button5" value="查询" style="width:60px; " onclick ="Init()" />&nbsp;&nbsp;
                    <input type="button" class="button5" value="自动调整" style="width:80px; " onclick ="AutoTZMB()" />&nbsp;&nbsp;
                </td>
        </tr>
        </table>
       </div>
       <div id="DivCenter" region="center">
            <div id="DivMb" style="width: 100%; height: 100%;">
                <table id="TabMB" style="width: 100%; height:100%;"  title="预算调整模板列表">
                </table>
            </div>
       </div>
    </form>
 </body>
</html>
