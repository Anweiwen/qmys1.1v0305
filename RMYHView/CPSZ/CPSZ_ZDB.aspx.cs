﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using System.Collections;
using RMYH.Model;
using System.Text;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class CPSZ_CPSZ_ZDB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CPSZ_CPSZ_ZDB));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return GetDataList.GetDataListstring("10144703", "", new string[] {":YY",":HSZXDM" }, new string[] {Session["YY"].ToString(),Session["HSZXDM"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return getNewLine("10144703");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_JHCPBM", id, "CPDM");
        }
        catch
        {
            return "删除失败！";
        }

        return "删除成功！";
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string HSZXDM)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            for (int i = 0; i < ID.Length; i++)
            {
                string hszxdm = Session["HSZXDM"].ToString();
                string cpnbbm = GetStringPrimaryKeys();
                string cpdm = GetStringPrimaryKey();
                string cpbm = values[i].Split('|')[0].Trim();
                string cpmc = values[i].Split('|')[1].Trim();
                string SYBZ = values[i].Split('|')[2].Trim();
                string SCBS = values[i].Split('|')[3].Trim();
                string JLDW = values[i].Split('|')[4].Trim();
                string FYYSDM = values[i].Split('|')[5].Trim();
                string SJZZCPDM = values[i].Split('|')[6].Trim();
                string CBXMBM = values[i].Split('|')[7].Trim();
                string CWHSFS = values[i].Split('|')[8].Trim();
                string FLBH = values[i].Split('|')[9].Trim();
                string CWHSBM = values[i].Split('|')[10].Trim();
                if (ID[i].Trim() != "")
                {
                    if (FYYSDM.Trim() != "")
                    {
                        FYYSDM = FYYSDM.Split('.')[0].ToString();
                    }
                    if (SJZZCPDM.Trim() != "")
                    {
                        SJZZCPDM = SJZZCPDM.Split('.')[0].ToString();
                    }
                    if (CBXMBM.Trim() != "")
                    {
                        CBXMBM = CBXMBM.Split('.')[0].ToString();
                    }
                    string update = "";
                    update += "update TB_JHCPBM set CPBM='" + cpbm + "',CPMC='" + cpmc + "',SYBZ='" + SYBZ + "',SCBS='" + SCBS + "'";
                    update += ",JLDW='" + JLDW + "',FYYSDM='" + FYYSDM + "',SJZZCPDM='" + SJZZCPDM + "',CBXMBM='" + CBXMBM + "'";
                    update += ",CWHSFS='" + CWHSFS + "',FLBH='" + FLBH + "',CWHSBM='" + CWHSBM + "' where CPDM='" + ID[i] + "'";
                    bll.ExecuteSql(update);
                    //arr.Clear();
                    //arrIdUpdate.Clear();
                    //if (values[i].IndexOf(".") > 0)
                    //{
                    //    string fyysdm = values[i].Split('|')[5].Split('.')[0].ToString();
                    //    if (values[i].Split('|')[6].Trim() != "")
                    //    {
                    //        //string SJZZCPDM = values[i].Split('|')[6].Split('.')[0].ToString();
                    //        arr.Add(values[i].Split('|')[0] + "|" + values[i].Split('|')[1] + "|" + values[i].Split('|')[2] + "|" + values[i].Split('|')[3] + "|" + values[i].Split('|')[4] + "|" + fyysdm + "|" + SJZZCPDM);
                    //    }
                    //    else
                    //    {
                    //        arr.Add(values[i].Split('|')[0] + "|" + values[i].Split('|')[1] + "|" + values[i].Split('|')[2] + "|" + values[i].Split('|')[3] + "|" + values[i].Split('|')[4] + "|" + fyysdm + "|" + values[i].Split('|')[6]);
                    //    }
                    //}
                    //else
                    //{
                    //    arr.Add(values[i]);
                    //}
                    //arrIdUpdate.Add(ID[i]);
                    //ret = bll.Update("10144703", "TB_JHCPBM", fileds, (String[])arr.ToArray(typeof(string)), "CPDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                }
                else
                {
                    ARR1.Clear();
                    arrIdAdd.Clear();
                    string add = "";
                    add += "insert into TB_JHCPBM(CPDM,CPNBBM,CPBM,CPMC,SYBZ,CPBS,SCBS,JLDW,FYYSDM,SJZZCPDM,CBXMBM,CWHSFS,FLBH,CWHSBM,HSZXDM)";
                    add += " values('" + cpdm + "','" + cpnbbm + "','" + cpbm + "','" + cpmc + "','" + SYBZ + "'," + 3 + ",'" + SCBS + "','" + JLDW + "','" + FYYSDM + "'";
                    add += ",'" + SJZZCPDM + "','" + CBXMBM + "','" + CWHSFS + "','" + FLBH + "','" + CWHSBM + "','" + hszxdm + "')";
                    bll.ExecuteSql(add);
                    //string hszxdm = Session["HSZXDM"].ToString();
                    //string cpnbbm = GetStringPrimaryKeys();
                    //ARR1.Add(values[i]+ "|" + hszxdm+"|"+cpnbbm);
                    //arrIdAdd.Add(ID[i]);
                    //ret = bll.Update("10144703", "TB_JHCPBM", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "CPDM", (String[])arrIdAdd.ToArray(typeof(string)));
                }
            }
        }
        catch
        {
            return "保存失败！";
        }
        return "保存成功！";
    }
    //获取产品内部编码
    public string GetStringPrimaryKeys()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT FBBM,NEXTKYBM FROM TB_CURRENTBM WHERE BMFL='8'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["NEXTKYBM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTBM SET NEXTKYBM='" + BM + "' WHERE BMFL='8'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["FBBM"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    //获取产品编码
    public static string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='12'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='12'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 财务核算方式 和 分类编码联动
    /// </summary>
    /// <param name="XMDH_A"></param>
    /// <param name="CPDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string ZCBind(string XMDH_A, string CPDM)
    {
        bool Flag = false;
        string Str = "";
        string yy = Session["YY"].ToString();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        string selSql = "";
        selSql += "SELECT F_FLBH,F_FLMC FROM  TB_CWLSHSFL where '1'='" + XMDH_A + "' AND   YY='" + yy + "'";
        selSql += " UNION SELECT '-1' F_FLBH,'产品核算分类' F_FLMC FROM XT_CSSZ  WHERE '2'='" + XMDH_A + "' and XMFL='A001' AND XMDH_A=1";
        selSql += " UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSYSLB WHERE '3'='" + XMDH_A + "' AND YY='" + yy + "'";
        selSql += " UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSZDFZLB WHERE '4'='" + XMDH_A + "'  ORDER BY F_FLBH";
        ds = DbHelperOra.Query(selSql);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (ds.Tables[0].Rows[i]["F_FLBH"].ToString().Trim() == CPDM.Trim())
            {
                Flag = true;
                Str += "<option selected=true value='" + ds.Tables[0].Rows[i]["F_FLBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_FLMC"].ToString() + "</option>";
            }
            else
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["F_FLBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_FLMC"].ToString() + "</option>";
            }
        }
        if (CPDM.Trim() != " ")
        {
            if (Flag == false)
            {
                Str += "<option selected=true value=''> </option>";
            }
        }
        return Str;
    }
    /// <summary>
    /// 财务核算方式 、 分类编码 和 财务核算编码联动
    /// </summary>
    /// <param name="XMDH_A">财务核算方式</param>
    /// <param name="CPDM">分类编码</param>
    /// <param name="CWHS">财务核算编码联动</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string CWHSBind(string XMDH_A, string CPDM, string CWHS)
    {
        bool Flag = false;
        string Str = "";
        string yy = Session["YY"].ToString();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        string selSql = "";
        selSql += "SELECT F_HSBH,F_HSMC FROM  TB_CWLSHSZD where '1'='" + XMDH_A + "' AND   YY='" + yy + "' AND F_FLBH='" + CPDM + "'";
        selSql += " UNION SELECT F_CPBH,F_CPMC F_HSMC FROM TB_CWLSCPZD WHERE '2'='" + XMDH_A + "' and YY='" + yy + "'";
        selSql += " UNION SELECT F_YSBH,F_YSMC F_HSMC FROM TB_CWLSYSZD WHERE '3'='" + XMDH_A + "' AND YY='" + yy + "' AND F_LBBH='" + CPDM + "'";
        selSql += " UNION SELECT F_ZFBH,F_ZFMC F_HSMC FROM TB_CWLSZFXM WHERE '4'='" + XMDH_A + "' AND F_LBBH='" + CPDM + "'  ORDER BY 1";
        ds = DbHelperOra.Query(selSql);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (ds.Tables[0].Rows[i]["F_HSBH"].ToString().Trim() == CWHS.Trim())
            {
                Flag = true;
                Str += "<option selected=true value='" + ds.Tables[0].Rows[i]["F_HSBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_HSMC"].ToString() + "</option>";
            }
            else
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["F_HSBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_HSMC"].ToString() + "</option>";
            }
        }
        if (CWHS.Trim() != " ")
        {
            if (Flag == false)
            {
                Str += "<option selected=true value=''> </option>";
            }
        }
        return Str;
    }
    #endregion
    #region  新增加数据方法

    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM)
    {
        return getNewLine(XMDM, false, "");
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CPBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKey()) + "\"");
                                    }
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 80).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion
}