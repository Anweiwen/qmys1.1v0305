﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;
using System.Collections;
using RMYH.Model;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
public partial class CPSZ_CPSZ_ZY : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CPSZ_CPSZ_ZY));
        }
        //遍历树节点
        TreeView1.ExpandAll();
        gettree();
    }
    public void gettree()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        strSql.Append("SELECT  ZYDM,ZYMC,CCJB,YJDBZ,ZYNBBM,FBDM FROM TB_JHZYML WHERE SYBZ='1' ORDER BY ZYNBBM");
        DS = bll.Query(strSql.ToString());
        crertetree(TreeView1.Nodes, DS.Tables[0], "0");
    }
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("FBDM='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["ZYDM"].ToString();
            node.Text = row[i]["ZYMC"].ToString();
            if (tb.Select("FBDM='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:setNodeStyle('" + node.Text + "');$('#" + hide_FBDM.ClientID + "').val('" + row[i]["ZYDM"].ToString() + "');$('#" + hid_ZYNBBM.ClientID + "').val('" + row[i]["ZYNBBM"].ToString() + "');getlist('', '', '');";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }
    #region Ajax方法
   /// <summary>
   /// 刷新数据
   /// </summary>
   /// <param name="trid"></param>
   /// <param name="id"></param>
   /// <param name="intimgcount"></param>
   /// <param name="zynbbm">作业内部编码</param>
   /// <param name="zydm">作业代码</param>
   /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string zynbbm,string zydm)
    {
        return GetDataListstring("10144646", "", new string[] { ":ZYNBBM", ":YY", ":HSZXDM" }, new string[] { zynbbm,Session["YY"].ToString(), Session["HSZXDM"].ToString() }, false, trid, id, intimgcount,zydm);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string ZYDM)
    {
        return getNewLine("10144646",ZYDM);
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string zydm,string HSZXDM)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            string filedsAdd = "";
            for (int i = 0; i < ID.Length; i++)
            {
                string hszxdm = Session["HSZXDM"].ToString();
                string cpdm = GetStringPrimaryKey();
                string cpnbbm = GetStringPrimaryKeys();
                string cpbm = values[i].Split('|')[0].Trim();
                string cpmc = values[i].Split('|')[1].Trim();
                string sybz = values[i].Split('|')[2].Trim();
                string cpbs = values[i].Split('|')[3].Trim();
                string jldw = values[i].Split('|')[4].Trim();
                string djtrldm = values[i].Split('|')[5].Trim();
                if (ID[i].Trim() != "")
                {
                    if (values[0].Split('|')[6].Trim() == "")
                    {
                        string[] updatearr = new string[1];
                        arr.Clear();
                        arrIdUpdate.Clear();
                        arr.Add(values[i]);
                        arrIdUpdate.Add(ID[i]);
                        updatearr[0] = "update TB_JHCPBM set CPBM='" + cpbm + "',CPMC='" + cpmc + "',SYBZ='" + sybz + "',CPBS=" + cpbs + ",JLDW='" + jldw + "',DJTRLDM='" + djtrldm + "' where CPDM='" + ID[i] + "'";
                        DbHelperOra.ExecuteSql(updatearr, null);
                    }
                    else
                    {
                        arr.Clear();
                        arrIdUpdate.Clear();
                        arr.Add(values[i]);
                        arrIdUpdate.Add(ID[i]);
                        ret = bll.Update("10144646", "TB_JHCPBM", fileds, (String[])arr.ToArray(typeof(string)), "CPDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                    }
                }
                else
                {
                    if (values[0].Split('|')[3].ToString().Trim() == "" && values[0].Split('|')[6].Trim() != "")
                    {
                        string[] addarr = new string[1];
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        addarr[0] = "insert into TB_JHCPBM(CPDM,CPBM,CPMC,SYBZ,JLDW,DJTRLDM,DJXS,ZYDM,CPNBBM,HSZXDM)values('" + cpdm + "','" + cpbm + "','" + cpmc + "','" + sybz + "','" + jldw + "','" + djtrldm + "'," + values[0].Split('|')[6] + ",'" + zydm + "','" + cpnbbm + "','" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(addarr, null);
                    }
                    //定价系数为空的添加方法
                    else if (values[0].Split('|')[3].ToString().Trim() != "" && values[0].Split('|')[6].Trim() == "")
                    {
                        string[] addarr = new string[1];
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        addarr[0] = "insert into TB_JHCPBM(CPDM,CPBM,CPMC,SYBZ,CPBS,JLDW,DJTRLDM,ZYDM,CPNBBM,HSZXDM)values('"+cpdm+"','"+cpbm+"','"+cpmc+"','"+sybz+"',"+cpbs+",'"+jldw+"','"+djtrldm+"','"+zydm+"','"+cpnbbm+"','"+hszxdm+"')";
                        DbHelperOra.ExecuteSql(addarr, null);
                    }
                    else if (values[0].Split('|')[3].ToString().Trim() == "" && values[0].Split('|')[6].Trim() == "")
                    {
                        string[] addarr = new string[1];
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        addarr[0] = "insert into TB_JHCPBM(CPDM,CPBM,CPMC,SYBZ,JLDW,DJTRLDM,ZYDM,CPNBBM,HSZXDM)values('" + cpdm + "','" + cpbm + "','" + cpmc + "','" + sybz + "','" + jldw + "','" + djtrldm + "','" + zydm + "','" + cpnbbm + "','" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(addarr, null);
                    }
                    else
                    {
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        filedsAdd = fileds + ",ZYDM" + ",CPNBBM" + ",HSZXDM";
                        ARR1.Add(values[i] + "|" + zydm + "|" + cpnbbm + "|" + hszxdm);
                        arrIdAdd.Add(ID[i]);
                        ret = bll.Update("10144646", "TB_JHCPBM", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "CPDM", (String[])arrIdAdd.ToArray(typeof(string)));
                    }
                }
            }
        }
        catch
        {

        }
        return ret;
    }
    //获取产品内部编码
    public string GetStringPrimaryKeys()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT FBBM,NEXTKYBM FROM TB_CURRENTBM WHERE BMFL='8'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["NEXTKYBM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTBM SET NEXTKYBM='" + BM + "' WHERE BMFL='8'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["FBBM"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
   //获取主键产品代码
    public static string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='12'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='12'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("TB_JHCPBM", id, "CPDM");
        return ret;
    }
    #endregion

    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr,string zydm)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "",zydm);
    }
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled,string zydm)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled,zydm);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled,string zydm)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);

                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString()).Replace(":ZYDM", "'" + zydm + "'")));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    #region 重载添加方法 当参数带where条件的时候 把：替换掉
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM,string ZYDM)
    {
        return getNewLine(XMDM, false, "",ZYDM);
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled,string ZYDM)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":ZYDM", "'" + ZYDM + "'")));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CPBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKey()) + "\"");
                                    }
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion
}