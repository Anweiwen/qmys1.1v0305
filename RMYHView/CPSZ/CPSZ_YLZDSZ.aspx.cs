﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using System.Collections;
using System.Text;
using RMYH.Model;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class CPSZ_CPSZ_YLZDSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CPSZ_CPSZ_YLZDSZ));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return GetDataListstring("10146086", "CPDM", new string[] { ":YY", ":HSZXDM" }, new string[] { Session["YY"].ToString(), Session["HSZXDM"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 获取父辈代码
    /// </summary>
    /// <param name="cpdm">产品代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getFBDM(string cpdm)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet Fbdm = BLL.Query("select FBDM from TB_JHCPBM WHERE CPDM='" + cpdm + "'");
        string FBDM = Fbdm.Tables[0].Rows[0][0].ToString();
        return FBDM;
    }
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        StringBuilder strSql = new StringBuilder();
        StringBuilder sql1 = new StringBuilder();
        StringBuilder sql2 = new StringBuilder();
        StringBuilder sql3 = new StringBuilder();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        sql1.Append("SELECT XMDM FROM TB_BBFYSJ WHERE XMDM in ('" + id + "'");
        sql2.Append("SELECT XMDM FROM TB_MBROWSX WHERE XMDM in ('" + id + "'");
        sql3.Append("SELECT XMDM FROM TB_MBCOLSX WHERE XMDM in ('" + id + "'");

        string set = SecDel(id, ref strSql);
        string newRes;
        string[] arg = null;
        if (set != null)
        {
            newRes = set.Substring(0, set.Length - 1);
            arg = newRes.Split(',');

            for (int i = 0; i < arg.Length; i++)
            {
                sql1.Append(",");
                sql1.Append("'" + arg[i] + "'");
                sql2.Append(",");
                sql2.Append("'" + arg[i] + "'");
                sql3.Append(",");
                sql3.Append("'" + arg[i] + "'");

            }
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");
        }
        else
        {
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");

        }

        string sq1 = sql1.ToString();
        string sq2 = sql2.ToString();
        string sq3 = sql3.ToString();

        DataSet ds1 = bll.Query(sq1);
        DataSet ds2 = bll.Query(sq2);
        DataSet ds3 = bll.Query(sq3);
        int count1 = ds1.Tables[0].Rows.Count;
        int count2 = ds2.Tables[0].Rows.Count;
        int count3 = ds3.Tables[0].Rows.Count;

        if (count1 != 0 || count2 != 0 || count3 != 0)
        {
            return "该产品或子产品已经使用，不能删除！";
        }

        // string sql = "SELECT XMDM FROM TB_XMXX WHERE PAR='" + id + "'";
        ret = bll.DeleteData("TB_JHCPBM", id, "CPDM");
        if (arg != null)
        {
            for (int i = 0; i < arg.Length; i++)
            {
                bll.DeleteData("TB_JHCPBM", arg[i], "CPDM");
            }
        }

        return ret;
    }
    public string SecDel(string id, ref StringBuilder ids)
    {
        //string arr=new Array[];
        string sql = "SELECT CPDM,CPMC FROM TB_JHCPBM WHERE FBDM='" + id + "'";

        string res = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet da = bll.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            res = da.Tables[0].Rows[0][0].ToString();
            for (int i = 0; i < da.Tables[0].Rows.Count; i++)
            {

                ids.Append(da.Tables[0].Rows[i][0].ToString());
                ids.Append(",");
                SecDel(da.Tables[0].Rows[i][0].ToString(), ref ids);

            }
        }
        else
        {
            return null;
        }

        return ids.ToString();
    }
    /// <summary>
    /// 添加数据行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddNewData(int Len, bool flag, string nbbm)
    {           //AddNewData(Len,xmdm,xmbm,xmmc,par,ccjb,xmmcex,flag)
        string newrow = NewLine(Len.ToString(), flag, nbbm);
        return newrow;
    }
    public string NewLine(string strarr, bool flag, string nbbm)
    {
        int intimgcount = int.Parse(strarr);
        strarr = ""; ;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        for (int i = 0; i < intimgcount; i++)
        {
            strarr += ",0";
        }
        if (flag)//添加下级
        {

        }
        else//添加同级
        {

        }

        return getNewLine("10146086", false, "", flag, nbbm);
    }
    //获得产品编码
    public static string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='12'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='12'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    public static string GetStringPrimaryKeys(bool boo, string nbbm)
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        string bm = boo ? nbbm : nbbm.Substring(0, nbbm.Length - 4);
                        //string bm = nbbm;
                        //查询当前最大的代码值
                        cmd.CommandText = nbbm == null ? "select FBBM,NEXTKYBM from TB_CURRENTBM where FBBM in (select max(FBBM) from TB_CURRENTBM where BMFL='8'and len(FBBM)=8)" : "select FBBM,NEXTKYBM from TB_CURRENTBM where FBBM='" + bm + "'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["NEXTKYBM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTBM SET NEXTKYBM='" + BM + "' WHERE BMFL='8'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["FBBM"].ToString().Trim() + BM;
                            }
                        }
                        else
                        {
                            //查询开头为0002的代表作业编码的所有8位编码的最大值

                            string cpnbbm = nbbm == null ? "00080185" : nbbm;
                            cmd.CommandText = nbbm == null ? "INSERT INTO TB_CURRENTBM (FBBM,BMFL,NEXTKYBM,BZ) values ('00080185','" + 8 + "','0008',null)" : "INSERT INTO TB_CURRENTBM (FBBM,BMFL,NEXTKYBM,BZ) values ('" + nbbm + "','" + 8 + "','0008',null)";
                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = cpnbbm + "0001";
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="fileds"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            for (int i = 0; i < ID.Length; i++)
            {
                if (ID[i].Trim() != "")
                {
                    string cpbm = values[i].Split('|')[0];
                    string cpmc = values[i].Split('|')[1].Trim();
                    string sybz = values[i].Split('|')[2].Trim();
                    string jglbz = values[i].Split('|')[3].Trim();
                    string fyysdm = values[i].Split('|')[4].Trim();
                    if (fyysdm.Trim() != "")
                    {
                        fyysdm = fyysdm.Split('.')[0].ToString();
                    }
                    string jldw = values[i].Split('|')[5];
                    string cbxmbm = values[i].Split('|')[6];
                    if (cbxmbm.Trim() != "")
                    {
                        cbxmbm = cbxmbm.Split('.')[0].ToString();
                    }
                    string cwhsfs = values[i].Split('|')[7];
                    string flbh = values[i].Split('|')[8].Trim();
                    string cwhsbm = values[i].Split('|')[9].Trim();
                    string dhdw = values[i].Split('|')[10].Trim();
                    string update = "";
                    update += "update TB_JHCPBM set CPBM='" + cpbm + "',CPMC='" + cpmc + "',SYBZ='" + sybz + "',JGLBZ='" + jglbz + "'";
                    update += ",FYYSDM='" + fyysdm + "',JLDW='" + jldw + "',CBXMBM='" + cbxmbm + "'";
                    update += ",CWHSFS='" + cwhsfs + "',FLBH='" + flbh + "',CWHSBM='" + cwhsbm + "',DHDW='" + dhdw + "' where CPDM='" + ID[i] + "'";
                    bll.ExecuteSql(update);
                    //arr.Clear();
                    //arrIdUpdate.Clear();
                    //arr.Add(values[i]);
                    //arrIdUpdate.Add(ID[i]);
                    //ret = bll.Update("10144706", "TB_JHCPBM", fileds, (String[])arr.ToArray(typeof(string)), "CPDM", (String[])arrIdUpdate.ToArray(typeof(string)));
                }
            }
            return "修改成功！";
        }
        catch
        {
            return "修改失败！";
        }
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string[] ID, string fileds, string[] values, string nbbm, bool boo)
    {

        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            string filedsAdd = "";
            for (int i = 0; i < ID.Length; i++)
            {
                if (ID[i].Trim() != "")
                {
                    arr.Clear();
                    arrIdUpdate.Clear();
                    string cpbm = values[i].Split('|')[1].Trim();
                    string cpmc = values[i].Split('|')[2].Trim();
                    string sybz = values[i].Split('|')[3].Trim();
                    string jglbz = values[i].Split('|')[4].Trim();
                    string fyysdm = values[i].Split('|')[5].Trim();
                    if (fyysdm.Trim() != "")
                    {
                        fyysdm = fyysdm.Split('.')[0].ToString();
                    }
                    string jldw = values[i].Split('|')[8].Trim();
                    string cbxmbm = values[i].Split('|')[9].Trim();
                    if (cbxmbm.Trim() != "")
                    {
                        cbxmbm = cbxmbm.Split('.')[0].ToString();
                    }
                    string cwhsfs = values[i].Split('|')[10].Trim();
                    string flbh = values[i].Split('|')[11].Trim();
                    string cwhsbm = values[i].Split('|')[12].Trim();
                    string dhdw = values[i].Split('|')[13].Trim();
                    string update = "";
                    update += "update TB_JHCPBM set CPBM='" + cpbm + "',CPMC='" + cpmc + "',SYBZ='" + sybz + "',JGLBZ='" + jglbz + "'";
                    update += ",FYYSDM='" + fyysdm + "',JLDW='" + jldw + "',CBXMBM='" + cbxmbm + "'";
                    update += ",CWHSFS='" + cwhsfs + "',FLBH='" + flbh + "',CWHSBM='" + cwhsbm + "',DHDW='" + dhdw + "' where CPDM='" + ID[i] + "'";
                    bll.ExecuteSql(update);
                }
                else
                {
                    ARR1.Clear();
                    arrIdAdd.Clear();
                    string cpbm = values[i].Split('|')[1].Trim();
                    string cpmc = values[i].Split('|')[2].Trim();
                    string sybz = values[i].Split('|')[3].Trim();
                    string jglbz = values[i].Split('|')[4].Trim();
                    string fyysdm = values[i].Split('|')[5].Trim();
                    string ccjb = values[i].Split('|')[6].Trim();
                    string fbdm = values[i].Split('|')[7].Trim();
                    string jldw = values[i].Split('|')[8].Trim();
                    string cbxmbm = values[i].Split('|')[9].Trim();
                    string cwhsfs = values[i].Split('|')[10].Trim();
                    string flbh = values[i].Split('|')[11].Trim();
                    string cwhsbm = values[i].Split('|')[12].Trim();
                    string dhdw = values[i].Split('|')[13].Trim();
                    string hszxdm = Session["HSZXDM"].ToString();
                    string cpnbbm = GetStringPrimaryKeys(boo, nbbm);
                    string cpdm = GetStringPrimaryKey();
                    string add = "";
                    add += "insert into TB_JHCPBM(CPDM,CPNBBM,CPBM,CPMC,SYBZ,CPBS,JGLBZ,FYYSDM,CCJB,FBDM,JLDW,CBXMBM,CWHSFS,FLBH,CWHSBM,DHDW,HSZXDM)";
                    add += " values('" + cpdm + "','" + cpnbbm + "','" + cpbm + "','" + cpmc + "','" + sybz + "',"+6+",'" + jglbz + "','" + fyysdm + "'," + ccjb + "";
                    add += ",'" + fbdm + "','" + jldw + "','" + cbxmbm + "','" + cwhsfs + "','" + flbh + "','" + cwhsbm + "','" + dhdw + "','" + hszxdm + "')";
                    bll.ExecuteSql(add);
                    //ARR1.Add(values[i]+ "|" + hszxdm+"|"+cpnbbm);
                    //arrIdAdd.Add(ID[i]);
                    //ret = bll.Update("10144706", "TB_JHCPBM", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "CPDM", (String[])arrIdAdd.ToArray(typeof(string)));
                }
            }
            //fileds ="CPBM"+fileds + ",HSZXDM,CPNBBM";
            //string[] newvalues=new string[values.Length];
            //for (int i = 0; i < values.Length;i++ )
            //{
            //    string cpnbbm = GetStringPrimaryKeys();
            //    string cpbm=GetStringPrimaryKey();
            //    string hszxdm = Session["HSZXDM"].ToString();
            //    newvalues[i] =cpbm+"|"+values[i].Substring(1).Trim()+"|"+hszxdm+"|"+cpnbbm;
            //}
            //TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //ret = bll.Update("10144706", "TB_JHCPBM", fileds.Substring(1), newvalues, "CPDM", ID);
        }
        catch
        {

        }
        return ret;
    }
    /// <summary>
    /// 财务核算方式 和 分类编码联动
    /// </summary>
    /// <param name="XMDH_A"></param>
    /// <param name="CPDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string ZCBind(string XMDH_A, string CPDM)
    {
        bool Flag = false;
        string Str = "";
        string yy = Session["YY"].ToString();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        string selSql = "";
        selSql += "SELECT F_FLBH,F_FLMC FROM  TB_CWLSHSFL where '1'='" + XMDH_A + "' AND   YY='" + yy + "'";
        selSql += " UNION SELECT '-1' F_FLBH,'产品核算分类' F_FLMC FROM XT_CSSZ  WHERE '2'='" + XMDH_A + "' and XMFL='A001' AND XMDH_A=1";
        selSql += " UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSYSLB WHERE '3'='" + XMDH_A + "' AND YY='" + yy + "'";
        selSql += " UNION SELECT F_LBBH,F_LBMC FROM  TB_CWLSZDFZLB WHERE '4'='" + XMDH_A + "'  ORDER BY F_FLBH";
        ds = DbHelperOra.Query(selSql);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (ds.Tables[0].Rows[i]["F_FLBH"].ToString().Trim() == CPDM.Trim())
            {
                Flag = true;
                Str += "<option selected=true value='" + ds.Tables[0].Rows[i]["F_FLBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_FLMC"].ToString() + "</option>";
            }
            else
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["F_FLBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_FLMC"].ToString() + "</option>";
            }
        }
        if (CPDM.Trim() != " ")
        {
            if (Flag == false)
            {
                Str += "<option selected=true value=''> </option>";
            }
        }
        return Str;
    }
    /// <summary>
    /// 财务核算方式 、 分类编码 和 财务核算编码联动
    /// </summary>
    /// <param name="XMDH_A">财务核算方式</param>
    /// <param name="CPDM">分类编码</param>
    /// <param name="CWHS">财务核算编码联动</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string CWHSBind(string XMDH_A, string CPDM, string CWHS)
    {
        CWHS = "";
        bool Flag = false;
        string Str = "";
        string yy = Session["YY"].ToString();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        string selSql = "";
        selSql += "SELECT F_HSBH,F_HSMC FROM  TB_CWLSHSZD where '1'='" + XMDH_A + "' AND   YY='" + yy + "' AND F_FLBH='" + CPDM + "'";
        selSql += " UNION SELECT F_CPBH,F_CPMC F_HSMC FROM TB_CWLSCPZD WHERE '2'='" + XMDH_A + "' and YY='" + yy + "'";
        selSql += " UNION SELECT F_YSBH,F_YSMC F_HSMC FROM TB_CWLSYSZD WHERE '3'='" + XMDH_A + "' AND YY='" + yy + "' AND F_LBBH='" + CPDM + "'";
        selSql += " UNION SELECT F_ZFBH,F_ZFMC F_HSMC FROM TB_CWLSZFXM WHERE '4'='" + XMDH_A + "' AND F_LBBH='" + CPDM + "'  ORDER BY 1";
        ds = DbHelperOra.Query(selSql);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (ds.Tables[0].Rows[i]["F_HSBH"].ToString().Trim() == CWHS.Trim())
            {
                Flag = true;
                Str += "<option selected=true value='" + ds.Tables[0].Rows[i]["F_HSBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_HSMC"].ToString() + "</option>";
            }
            else
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["F_HSBH"].ToString() + "'>" + ds.Tables[0].Rows[i]["F_HSMC"].ToString() + "</option>";
            }
        }
        if (CWHS.Trim() != " ")
        {
            if (Flag == false)
            {
                Str += "<option selected=true value=''> </option>";
            }
        }
        return Str;
    }
    #endregion
    #region  新增加数据方法

    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool flag, string nbbm)
    {
        return getNewLine(XMDM, false, "", flag, nbbm);
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, bool flag, string nbbm)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                        {
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        }
                        else
                        {
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "");
                            //if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Trim() == "CPNBBM")
                            //{

                            //    //sb.Append(" value=\"" + (GetStringPrimaryKeys(flag, nbbm)) + "\"");
                            //    sb.Append(GetStringPrimaryKeys(flag, nbbm));
                            //    //sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKeys(flag, nbbm)) + "\"");
                            //}
                            sb.Append("</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "CPBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + (GetStringPrimaryKey()) + "\"");
                                    }
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 110).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    #endregion

    #region 重写公共方法
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 100) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='150px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);

                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["ZDZWM"].ToString() == "财务核算")
                                    {
                                        sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    }
                                    else
                                    {
                                        sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");

                                    }
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4"://树
                                    sb.Append("<INPUT TYPE=TEXT   STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 110).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion
}