﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CPSZ_CP.aspx.cs" Inherits="CPSZ_CPSZ_CP" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <input type="button" class="button5" onclick="jsAddData(false)" value="添加同级" />
    <input type="button" class="button5" onclick="jsAddData(true)" value="添加下级" />
    <input type="button" class="button5" onclick="TreeZK()" value="展开" />
    <input type="button" class="button5" onclick="TreeSQ()" value="收起" />
    <input type="button" class="button5" onclick="getlist('','','')" value="刷新" />
    <input type="button" class="button5" value="删除" onclick="Del()" />
    <input type="button" class="button5" value="取消删除" onclick="Cdel()" />
    <input type="button" class="button5" value="保存" onclick="save()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<div style="height:80%">
    <div id="divTreeListView"></div>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="CPDM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   
<input type="hidden" id="hide_ccjb" runat="server" /> 
<input type="hidden" id="HidMC" runat="server" /> 
<input type="hidden" id="HidTrID" />  

 

<script type="text/javascript">
    var bool = null;
    var xmdm = null; var par = null; var ccjb = null; var content = null; var gif = -1; var ob;nbbm = null;
    function jsUpdateData(objid, objfileds, objvalues, bool,nbbm) {
        if (bool != null) {
            var rtn = CPSZ_CPSZ_CP.AddData(objid, objfileds, objvalues, cpnbbm, bool).value;

        } else {
            var rtn = CPSZ_CPSZ_CP.UpdateData(objid, objfileds, objvalues).value;
        }
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    $(document).ready(function () {
        getlist('', '', '');
//        CheckChanged();
    });
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = CPSZ_CPSZ_CP.LoadList(objtr, objid, intimagecount).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
        //产品编码不能重复
        $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品编码不能重复！");
                $("#" + par + " input[name^='txtCPBM']").val("");
            }
        });
        //产品名称不能重复
        $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品名称不能重复！");
                $("#" + par + " input[name^='txtCPMC']").val("");
            }
        });
        CheckChanged();
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = CPSZ_CPSZ_CP.DeleteData(obj).value;
        if (rtn == "1") {
            alert("删除成功！");
        }
        else {
            alert("该产品已发生费用不能删除！");
        }

    }
    //添加数据
//    function jsAddNewData() {
//        if ($("#hidNewLine").val() == "")
//            $("#hidNewLine").val(CPSZ_CPSZ_CP.AddNewData().value);
//        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
    //    }
    function jsAddData(flag) {
        var tr = $("#divTreeListView tr:eq(1) td[name='tdCPNBBM']").length;
        if (flag && $("#hidcheckid").val() == "") {
            alert("请选择父节点");
            return;
        }
        else {
            if (tr != 0) {
                cpnbbm = $("#divTreeListView tr:eq(1) td[name='tdCPNBBM']")[0].innerHTML; //当不选择行就添加同行时，默认选中第一行数据
                fbdm = $("#divTreeListView tr:eq(1) td[name='tdFBDM']")[0].innerHTML;
            }

            if (gif != -1 && flag) {
                gif = -1;
            }
    
            jsAddNewData(flag);
        }
        bool = flag;
    }

    //添加数据
    function jsAddNewData(flag) {
        var Len;
        $("#HidTrID").val() == "" ? Len = 0 : Len = $("#" + $("#HidTrID").val()).find("img[src$='white.gif']").length;
        //var Len = $("#" + $("#HidTrID").val()).find("img[src$='white.gif']").length;
        //if ($("#hidNewLine").val() == "")
        $("#hidNewLine").val(CPSZ_CPSZ_CP.AddNewData(Len,flag,cpnbbm).value);
        var rtnstr = $("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', "");
        if ($("#HidTrID").val() == "") {
            $("#divTreeListView table").append(rtnstr);

        }
        else {
            $("#" + $("#HidTrID").val()).after(rtnstr);
        }
        //产品编码不能重复
        $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品编码不能重复！");
                $("#" + par + " input[name^='txtCPBM']").val(parvalue);
            }
        });
        //产品名称不能重复
        $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品名称不能重复！");
                $("#" + par + " input[name^='txtCPMC']").val("");
            }
        });

        //$("#hidNewLine").val("");
        // $("#HidTrID").val("")
    }
    function save() {
        if (bool == null) {
            SaveValues();   //修改的保存
        } else {
            AddValues();    //增加的保存
        }
    }

    //重写mainjs里的SetValue方法
    function SaveValues() {
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues,nbbm);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }



    function AddValues() {
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            var objfileds = "";
            var objvalues = "";
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");


            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val()) {
                    edtid[edtid.length] = objtd[j].innerHTML;
                }
                else if ((objtd[j].getAttribute("name") == "tdFBDM") && (objtd[j].getAttribute("name") != "")) {
                    if (bool) {//添加下级
                        if (xmdm == null) {
                            objvalues += "|" + '';
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        } else {
                            objvalues += "|" + xmdm;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        }

                    }
                    else {//添加同级
                        if (xmdm == null) {
                            objvalues += "|" + 0;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        } else {
                            var fbdm = CPSZ_CPSZ_CP.getFBDM(xmdm).value;
                            objvalues += "|" + fbdm;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        }
                    }

                }
                else if ((objtd[j].getAttribute("name") == "tdCCJB") && (objtd[j].getAttribute("name") != "")) {
                    if (bool) {//添加下级
                        if (ccjb == null) {
                            objvalues += "|" + 0;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        } else {
                            objvalues += "|" + (parseInt(ccjb) + 1);
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        }
                    }
                    else {//添加同级
                        if (ccjb == null) {
                            objvalues += "|" + 0;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        } else {
                            objvalues += "|" + ccjb;
                            objfileds += "," + objtd[j].getAttribute("name").substring(2);
                        }
                    }
                }
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);

                            //                            if (objinput[0].name == "txtXMMC") {
                            mcex = objinput[0].value;
                            //                            }

                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds;
                edtobjvalues[edtobjvalues.length] = objvalues;
            }
        }
        if (edtobjfileds.length > 0)
        jsUpdateData(edtid, edtobjfileds, edtobjvalues, bool,nbbm);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }

    function onselects(obj) {
        var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
        gif = $(obj).parent().parent().next().attr('id');
        if (divid == "divTreeListView") {
            $("div[id='divTreeListView'] tr[name^='trdata']").css("backgroundColor", "");
            trid = obj.parentNode.parentNode.id;
            xmdm = $('tr[id=' + trid + '] td[name=\'tdCPDM\']').html();
            xmbm = $('tr[id=' + trid + '] td input[name=\'txtCPBM\']').val();
            xmmc = $('tr[id=' + trid + '] td input[name=\'txtCPMC\']').val();
            par = $('tr[id=' + trid + '] td[name=\'tdFBDM\']').html();
            ccjb = $('tr[id=' + trid + '] td[name=\'tdCCJB\']').html();
            cpnbbm = $('tr[id=' + trid + '] td[name=\'tdCPNBBM\']').html();
            $("div[id='divTreeListView'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
            $("#HidTrID").val(trid);
        } else {
            $("div[id='dd'] tr[name^='trdata']").css("backgroundColor", "");
            trid = obj.parentNode.parentNode.id;
            $("div[id='dd'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
            $("#hideeasyui").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
        }
    }
    function collapseAll() {
        $('#tt').tree('collapseAll');
    }
    function expandAll() {
        $('#tt').tree('expandAll');
    }
    function TreeZK() {
        var tr = $("#divTreeListView img[src$='tplus.gif']");
        for (var i = 0; i < tr.length; ) {
            var trid = tr[i].parentNode.parentNode.id;
            ToExpand(tr[i], trid);
            i = 0;
            tr = $("#divTreeListView img[src$='tplus.gif']");
        }
    }
    function TreeSQ() {
        var tr = $("#divTreeListView img[src$='tminus.gif']");
        for (var i = 0; i < tr.length; i++) {
            var trid = tr[i].parentNode.parentNode.id;
            ToExpand(tr[i], trid);
            var CPDM = $("#" + trid + " td[name^='tdCPDM']").html();
        }
    }

    function ToExpand(obj, id) {
        var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
        var trs;
        if (obj.src.indexOf("tminus.gif") > -1) {
            obj.src = "../Images/Tree/tplus.gif";
            divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
            //            trs = $("#divTreeListView tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lminus.gif") > -1) {
            obj.src = "../Images/Tree/lplus.gif";
            divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lplus.gif") > -1) {
            obj.src = "../Images/Tree/lminus.gif";
            divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                }
            }
        }
        else if (obj.src.indexOf("tplus.gif") > -1) {
            obj.src = "../Images/Tree/tminus.gif";
            divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
            var istoload = true;
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                    istoload = false;
                }
            }
            if (istoload == true) {
                var CPDM = $("tr[id=" + id + "] td[name='tdCPDM']").html();
                getlist(id, CPDM, $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
        }

    }
    //财务核算方式 和 分类编码联动
    function CheckChanged() {
        $("select[name='selCWHSFS']").change();
        $("img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
    }
    function EditData(obj, flag) {
        if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
            obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
        var trid = obj.parentNode.parentNode.id;
        if (obj.name == "selCWHSFS") {
            var FA = $("#" + trid + " select[name='selFLBH']").val();
            var ss = CPSZ_CPSZ_CP.ZCBind(obj.value, FA).value;
            if (ss != null && ss != "") {
                $("#" + trid + " select[name='selFLBH']").html(ss);
                var CWHS = $("#" + trid + " select[name='selCWHSBM']").val();
                var CWHSBM = CPSZ_CPSZ_CP.CWHSBind(obj.value, FA, CWHS).value;
                if (CWHSBM != null && CWHSBM != "") {
                    $("#" + trid + " select[name='selCWHSBM']").html(CWHSBM);
                }
            }
            else {
                var s = $("#" + trid + " select[name='selFLBH']");
                s[0].options.length = 0;
                s[0][0] = new Option("", "");
                var CWHS = $("#" + trid + " select[name='selCWHSBM']");
                CWHS[0].options.length = 0;
                CWHS[0][0] = new Option("", "");
            }
        }
    }
</script>
</asp:Content>