﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class JSSZ_YHQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JSSZ_YHQX));
            //遍历树节点
            TreeView1.ExpandAll();
            gettree();
        }
    }
    /// <summary>
    /// 获取左边的树
    /// </summary>
    public void gettree()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        strSql.Append("select JSDM,NAME,0 PAR from TB_JIAOSE ");
        DS = bll.Query(strSql.ToString());
        crertetree(TreeView1.Nodes, DS.Tables[0], "0");
    }
    /// <summary>
    /// 点树节点传值
    /// </summary>
    /// <param name="tree"></param>
    /// <param name="tb"></param>
    /// <param name="par"></param>
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("PAR='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["JSDM"].ToString();
            node.Text = row[i]["NAME"].ToString();
            if (tb.Select("PAR='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:setNodeStyle('" + node.Text + "','" +node.Value + "');";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }
}