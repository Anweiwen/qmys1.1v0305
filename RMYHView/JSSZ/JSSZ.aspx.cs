﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using RMYH.DBUtility;
using System.Data;
using System.Text;
using Sybase.Data.AseClient;

public partial class JSSZ_JSSZ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(JSSZ_JSSZ));
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
      return GetDataList.GetDataListstring("10144442", "", new string[] {  }, new string[] {  }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144442");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_JIAOSE", id, "JSDM");
        }
        catch
        {
            return "删除失败！";
        }
        return "删除成功！";
    }
    /// <summary>
    /// 判断当前角色是否被流程所用
    /// </summary>
    /// <param name="jsdm">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool DeleteJsdm(string jsdm)
    {
        bool Flag = false;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet delJsdm = bll.Query("SELECT * FROM TB_YSMBLC WHERE JSDM  LIKE '%" + jsdm.Replace("'", "''") + "%'");
        if (delJsdm.Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        else
        {
            Flag = false;
        }
        return Flag;
    }
    //获得角色ID
    public static string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='16'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='16'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            for (int i = 0; i < ID.Length; i++)
            {
                if (ID[i].Trim() != "")
                {
                    string NAME = values[i].Split('|')[0].Trim();
                    string JSMS = values[i].Split('|')[1].Trim();
                    string CREATER = Session["USERDM"].ToString();
                    string update = "";
                    update = "update TB_JIAOSE set NAME='" + NAME + "',JSMS='" + JSMS + "',UPDATER='" + CREATER + "',UPDATETIME='" + DateTime.Now + "' where JSDM='" + ID[i] + "'";
                    bll.Query(update);
                }
                else
                {
                    string NAME = values[i].Split('|')[0].Trim();
                    string JSMS = values[i].Split('|')[1].Trim();
                    string JSDM = GetStringPrimaryKey();
                    string CREATER=Session["USERDM"].ToString();
                    string add = "";
                    add = "insert into TB_JIAOSE(JSDM,NAME,JSMS,ISYDY,CREATER,UPDATER,UPDATETIME,SYBZ) values('" + JSDM + "','" + NAME + "','" + JSMS + "',";
                    add += "'" + 0 + "','" + CREATER + "','" + CREATER + "','" + DateTime.Now + "','" + 0 + "')";
                    bll.ExecuteSql(add);
                }
            }
            
            //TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //ret = bll.Update("10144442", "TB_JIAOSE", fileds, values, "JSDM", ID);
        }
        catch
        {

        }
        return ret;
    }
}
#endregion