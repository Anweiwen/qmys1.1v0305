﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;
using RMYH.DBUtility;
using RMYH.BLL.CDQX;

public partial class JSSZ_USERQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(JSSZ_USERQX));
        if (!IsPostBack)
        {
            HidUSERDM.Value = Request.QueryString["JSDM"].ToString();
            //遍历树节点
            gettreelist();
            //给用户组赋权限
            check(HidUSERDM.Value);
            //展开树的节点
            TreeView2.ExpandAll();
        }
    }
    /// <summary>
    /// 查询用户和用户组的相关信息
    /// </summary>
    public void gettreelist()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT USER_ID,USER_NAME,'-2' PAR FROM TB_USER ";
        sql+="union ";
        sql+="SELECT '-2' USER_ID,'用户' USER_NAME,'-1' PAR FROM TB_USER";
        DataSet set=bll.Query(sql);
        //遍历树
        CreateTreeViewRecursive(TreeView2.Nodes, set.Tables[0], "-1");
    }
    /// <summary>
    /// 递归查询
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    private void CreateTreeViewRecursive(TreeNodeCollection nodes, DataTable dataSource, string ParCode)
    {
        string filter;
        DataRow[] drs;
        filter = "PAR='" + ParCode + "'";
        DataRow[] drarr = dataSource.Select(filter, "USER_ID");
        TreeNode node;
        foreach (DataRow dr in drarr)
        {
            node = new TreeNode();
            node.Text = dr["USER_NAME"].ToString();
            node.Value = dr["USER_ID"].ToString();
            node.Expanded = false;
            drs = dataSource.Select("PAR='" + node.Value + "'");
            if (drs.Length == 0)
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            else
            {
                node.ImageUrl = "~/images/minus.gif";
            }
            node.NavigateUrl = "javascript:void(0)";
            nodes.Add(node);
            CreateTreeViewRecursive(node.ChildNodes, dataSource, node.Value);
        }
    }
    /// <summary>
    /// 将当前登录人的权限目录明细的父节点依次递归选中
    /// </summary>
    /// <param name="node">当前权限节点的父节点</param>
    public void CheckedParentNode(TreeNode node)
    {
        if (node.Parent != null && node.Parent.Checked == false)
        {
            node.Parent.Checked = true;
            CheckedParentNode(node.Parent);
        }
    }
    /// <summary>
    /// 遍历当前树节点
    /// </summary>
    /// <param name="nodes">树节点</param>
    /// <param name="dt">当前权限集合</param>
    private void checkNodes(TreeNodeCollection nodes, DataTable dt)
    {
        DataRow[] row;
        BBFXXT_BBCDQXBLL BB = new BBFXXT_BBCDQXBLL();
        foreach (TreeNode node in nodes)
        {
            row = dt.Select("USERID='" + node.Value + "'");
            if (row.Length == 1)
            {
                node.Checked = true;
                if (node.Parent != null && node.Parent.Checked == false)
                {
                    node.Parent.Checked = true;
                    CheckedParentNode(node.Parent);
                }
            }
            checkNodes(node.ChildNodes, dt);
        }
    }
    /// <summary>
    /// 给用户或者用户组赋权限
    /// </summary>
    public void check(string Jsdm)
    {
        string BBSql = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //string sql = "SELECT * FROM TB_GROUP WHERE GROUP_ID='"+USERDM+"'";
        //判断被选择用户是用户组还是用户
        BBSql = "SELECT * FROM TB_JSYH WHERE JSDM='" + Jsdm + "'";
        DS = bll.Query(BBSql);
        checkNodes(TreeView2.Nodes, DS.Tables[0]);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        string InstSql = "", BBSql = "";
        //定义标识（0代表是用户组权限，1代表是用户权限）
        string JSDM = HidUSERDM.Value;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        try
        {
            //判断被选择用户是用户组还是用户
            BBSql = "DELETE FROM TB_JSYH WHERE JSDM='" + JSDM + "'";
            int Count = DbHelperOra.ExecuteSql(BBSql);
            //给用户赋权限
            foreach (TreeNode node in TreeView2.CheckedNodes)
            {
                if (node.ChildNodes.Count == 0)
                {
                    InstSql = "INSERT INTO TB_JSYH(JSDM,USERID) VALUES('" + JSDM + "','" + node.Value.Trim() + "')";
                    DbHelperOra.ExecuteSql(InstSql);
                }
            }
            Response.Write("<script>alert('保存成功！')</script>");
            //清除所有节点
            TreeView2.Nodes.Clear();
            //遍历树节点
            gettreelist();
            //给用户组赋权限
            check(JSDM);
            //展开树的节点
            TreeView2.ExpandAll();
            return;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}