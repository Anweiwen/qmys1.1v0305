﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class GSSZ_SJYDYX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(GSSZ_SJYDYX));
            TreeView1.ExpandAll();
            gettree();
        }
    }
    public void gettree()
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        strSql.Append("select SJYID,SJYMC,0 PAR from REPORT_CSSZ_SJY ");
        DS = bll.Query(strSql.ToString());
        crertetree(TreeView1.Nodes, DS.Tables[0], "0");
    }
    public void crertetree(TreeNodeCollection tree, DataTable tb, string par)
    {
        DataRow[] row = tb.Select("PAR='" + par + "'");
        TreeNode node;
        for (int i = 0; i < row.Length; i++)
        {
            node = new TreeNode();
            node.Value = row[i]["SJYID"].ToString();
            node.Text = row[i]["SJYMC"].ToString();
            if (tb.Select("PAR='" + node.Value + "'").Length > 0)
            {

                node.ImageUrl = "~/images/minus.gif";
            }
            else
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            node.NavigateUrl = "javascript:setNodeStyle('" + node.Text + "');$('#" + hide_FBDM.ClientID + "').val('" + row[i]["SJYID"].ToString() + "');getlist('', '', '');";
            tree.Add(node);
            crertetree(node.ChildNodes, tb, node.Value);
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string sjyid)
    {
        return GetDataList.GetDataListstring("10144612", "", new string[] { ":SJYID"}, new string[] { sjyid}, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144612");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        ret = bll.DeleteData("REPORT_CSSZ_SJYDYX", id, "DYXID");
        return ret;
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values,string sjyid)
    {
        string ret = "";
        try
        {
            fileds = fileds + ",SJYID";
            for (int i = 0; i < ID.Length; i++)
            {
                values[i] = values[i] + "|" + sjyid;
            }

            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.Update("10144612", "REPORT_CSSZ_SJYDYX", fileds, values, "DYXID", ID);
        }
        catch
        {

        }
        return ret;
    }
    #endregion
}