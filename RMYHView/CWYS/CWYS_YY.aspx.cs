﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using RMYH.Model;
using System.IO;

public partial class CWYS_CWYS_YY : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CWYS_CWYS_YY));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string yy)
    {
        return GetDataListstring("10144641", "", new string[] { ":YY1",":YY2" }, new string[] { yy,Session["YY"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return getNewLine("10144641");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_JHFA", id, "JHFADM");
        }
        catch
        {

            return "删除失败！";
        }
        return "删除成功！";
    }
    /// <summary>
    /// 获取数据库预算类型
    /// </summary>
    /// <param name="jsdm">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string selysbs(string mbdm)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet selysbs = bll.Query("SELECT YSBS FROM TB_JHFA WHERE JHFADM='" + mbdm + "'");
        ret = selysbs.Tables[0].Rows[0][0].ToString();
        return ret;
    }
    /// <summary>
    /// 此模板是否在模板流程发起使用
    /// </summary>
    /// <param name="jsdm">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool selmbdm(string mbdm)
    {
        bool Flag = false;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet selMbdm = bll.Query("SELECT * FROM TB_YSLCFS WHERE JHFADM='" + mbdm + "'");
        if (selMbdm.Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        else
        {
            Flag = false;
        }
        return Flag;
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
       
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            string filedsAdd = "";
            for (int i = 0; i < ID.Length; i++)
            {
                //计划方案名称
                string jhfaname = values[i].Split('|')[0];
                //年
                string YY = values[i].Split('|')[1];
                //hszxdm
                string hszxdm = values[i].Split('|')[2];
                //预算类型
                string YSBS = values[i].Split('|')[3];
                //是否最终方案
                string DELBZ = values[i].Split('|')[4];
                //审批流程
                string scbs = values[i].Split('|')[5];
                if (ID[i].Trim() != "")
                {
                    //预算标识是批复预算或分解的时候
                    if (YSBS == "4" || YSBS=="5")
                    {
                        DataSet PFFZBZ = bll.Query("select * from TB_YSBBMB where PFFZBZ='1'");
                            //判断是否批复默认复制模板，如果有就自动弹出复制框，没有就直接修改
                            if (PFFZBZ.Tables[0].Rows.Count != 0)
                            {
                                arr.Clear();
                                arrIdUpdate.Clear();
                                if (values[i].IndexOf(".") > 0)
                                {
                                    hszxdm = hszxdm.Split('.')[0];
                                    string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                                    bll.ExecuteSql(update);
                                }
                                else
                                {
                                    string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                                    bll.ExecuteSql(update);
                                }
                                return "1";
                            }
                            else
                            {
                                arr.Clear();
                                arrIdUpdate.Clear();
                                if (values[i].IndexOf(".") > 0)
                                {
                                    hszxdm = hszxdm.Split('.')[0];
                                    string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                                    bll.ExecuteSql(update);
                                }
                                else
                                {
                                    string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                                    bll.ExecuteSql(update);
                                }
                            }
                    }
                    else
                    {
                        arr.Clear();
                        arrIdUpdate.Clear();
                        if (values[i].IndexOf(".") > 0)
                        {
                            hszxdm = hszxdm.Split('.')[0];
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                            bll.ExecuteSql(update);
                        }
                        else
                        {
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + YY + "',HSZXDM='" + hszxdm + "',YSBS='" + YSBS + "',DELBZ='" + DELBZ + "',SCBS=" + scbs + " where JHFADM='" + ID[i] + "'";
                            bll.ExecuteSql(update);
                        }
                    }
                }
                else
                {
                    //预算标识是批复预算或分解的时候
                    if (YSBS == "4" || YSBS == "5")
                    {
                        string fadm = "";
                        DataSet PFFZBZ = bll.Query("select * from TB_YSBBMB where PFFZBZ='1'");
                        //判断是否批复默认复制模板，如果有就自动弹出复制框，没有就直接添加
                        if (PFFZBZ.Tables[0].Rows.Count != 0)
                        {
                            try
                            {

                                string add = "";
                                string cpbm = GetStringPrimaryKey();
                                string jhfandm = GetStringPrimaryKey();
                                add += "insert into TB_JHFA(JHFADM,JHFABM,JHFANAME,YY,NN,HSBZ,FABS,DELBZ,XTSJ,HSZXDM,SCBS,YSBS)";
                                add += " values('" + jhfandm + "','" + cpbm + "','" + jhfaname + "','" + YY + "','0" + 1 + "'";
                                add += ",'" + 0 + "','" + 3 + "','" + DELBZ + "','" + DateTime.Now + "','" + hszxdm + "'," + 1 + ",'" + YSBS + "')";
                                bll.ExecuteSql(add);
                                DataSet jhfadm = bll.Query("select JHFADM,DELBZ,YY,YSBS from TB_JHFA where JHFABM='" + cpbm + "'");
                                fadm = jhfadm.Tables[0].Rows[0][0].ToString();
                                DataSet sql = bll.Query("select YY,YSBS,DELBZ FROM TB_JHFA");
                                int count = 0;
                                for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
                                {
                                    //数据库的年
                                    string yy = sql.Tables[0].Rows[j][0].ToString();
                                    //数据库的预算类型
                                    string ysbs = sql.Tables[0].Rows[j][1].ToString();
                                    //数据库的最终方案
                                    string delbz = sql.Tables[0].Rows[j][2].ToString();
                                    //新添加的年
                                    string addyy = jhfadm.Tables[0].Rows[0][2].ToString();
                                    //新添加的预算类型
                                    string addYsbs = jhfadm.Tables[0].Rows[0][3].ToString();
                                    //新添加的最终方案
                                    string addDelbz = jhfadm.Tables[0].Rows[0][1].ToString();
                                    //年相同 预算类型相同 数据库的最终方案=1
                                    if (yy == addyy && ysbs == addYsbs && delbz == "1")
                                    {
                                        count += 1;
                                    }
                                }
                                if (count <= 0)
                                {
                                    string update = "update TB_JHFA SET DELBZ='1' WHERE JHFADM='" + jhfadm.Tables[0].Rows[0][0] + "'";
                                    bll.ExecuteSql(update);
                                    return "1" + "|" + fadm;
                                }
                            }
                            catch
                            {

                            }
                            return "1" + "|" + fadm;
                        }
                        else
                        {
                            try
                            {

                                string add = "";
                                string cpbm = GetStringPrimaryKey();
                                string jhfandm = GetStringPrimaryKey();
                                add += "insert into TB_JHFA(JHFADM,JHFABM,JHFANAME,YY,NN,HSBZ,FABS,DELBZ,XTSJ,HSZXDM,SCBS,YSBS)";
                                add += " values('" + jhfandm + "','" + cpbm + "','" + jhfaname + "','" + YY + "','0" + 1 + "'";
                                add += ",'" + 0 + "','" + 3 + "','" + DELBZ + "','" + DateTime.Now + "','" + hszxdm + "'," + 1 + ",'" + YSBS + "')";
                                bll.ExecuteSql(add);
                                DataSet jhfadm = bll.Query("select JHFADM,DELBZ,YY,YSBS from TB_JHFA where JHFABM='" + cpbm + "'");
                                fadm = jhfadm.Tables[0].Rows[0][0].ToString();
                                DataSet sql = bll.Query("select YY,YSBS,DELBZ FROM TB_JHFA");
                                int count = 0;
                                for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
                                {
                                    //数据库的年
                                    string yy = sql.Tables[0].Rows[j][0].ToString();
                                    //数据库的预算类型
                                    string ysbs = sql.Tables[0].Rows[j][1].ToString();
                                    //数据库的最终方案
                                    string delbz = sql.Tables[0].Rows[j][2].ToString();
                                    //新添加的年
                                    string addyy = jhfadm.Tables[0].Rows[0][2].ToString();
                                    //新添加的预算类型
                                    string addYsbs = jhfadm.Tables[0].Rows[0][3].ToString();
                                    //新添加的最终方案
                                    string addDelbz = jhfadm.Tables[0].Rows[0][1].ToString();
                                    //年相同 预算类型相同 数据库的最终方案=1
                                    if (yy == addyy && ysbs == addYsbs && delbz == "1")
                                    {
                                        count += 1;
                                    }
                                }
                                if (count <= 0)
                                {
                                    string update = "update TB_JHFA SET DELBZ='1' WHERE JHFADM='" + jhfadm.Tables[0].Rows[0][0] + "'";
                                    bll.ExecuteSql(update);
                                    return "添加成功";
                                }
                            }
                            catch
                            {

                            }
                            return "添加成功！";
                        }
                       
                    }
                    else
                    {
                        ARR1.Clear();
                        arrIdAdd.Clear();
                        filedsAdd = "JHFABM," + fileds;
                        string cpbm = GetStringPrimaryKey();
                        ARR1.Add(cpbm + "|" + values[i]);
                        arrIdAdd.Add(ID[i]);
                        ret = bll.Update("10144641", "TB_JHFA", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "JHFADM", (String[])arrIdAdd.ToArray(typeof(string)));
                        //获得添加的结果
                        string[] value = (String[])ARR1.ToArray(typeof(string));
                        string SCBS = value[i].Split('|')[6];
                        string jhfabm = value[i].Split('|')[0];
                        DataSet jhfadm = bll.Query("select JHFADM from TB_JHFA where JHFABM='" + jhfabm + "'");
                        //如果是否走审批流程没勾中 就执行修改
                        if (SCBS != "1")
                        {
                            string updateScbs = "update TB_JHFA set SCBS=1 where JHFADM='" + jhfadm.Tables[0].Rows[0][0] + "'";
                            bll.ExecuteSql(updateScbs);
                        }
                        DataSet sql = bll.Query("select YY,YSBS,DELBZ FROM TB_JHFA");
                        int count = 0;
                        for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
                        {
                            //数据库的年
                            string yy = sql.Tables[0].Rows[j][0].ToString();
                            //数据库的预算类型
                            string ysbs = sql.Tables[0].Rows[j][1].ToString();
                            //数据库的最终方案
                            string delbz = sql.Tables[0].Rows[j][2].ToString();
                            //新添加的年
                            string addyy = value[i].Split('|')[2];
                            //新添加的预算类型
                            string addYsbs = value[i].Split('|')[4];
                            //新添加的最终方案
                            string addDelbz = value[i].Split('|')[5];
                            //年相同 预算类型相同 数据库的最终方案=1
                            if (yy == addyy && ysbs == addYsbs && delbz == "1")
                            {
                                count += 1;
                            }
                        }
                        if (count <= 0)
                        {
                            string update = "update TB_JHFA SET DELBZ='1' WHERE JHFADM='" + jhfadm.Tables[0].Rows[0][0] + "'";
                            bll.ExecuteSql(update);
                            return "添加成功";
                        }
                    }
                }
            }
        }
        catch
        {
            return "保存失败！";
        }
        return "保存成功！";
    }
    public string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='6'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='6'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }

    /// <summary>
    /// 根据方案获得相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFA(string yy,string jhfandm)
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ysbs = bll.Query("select YSBS FROM TB_JHFA WHERE JHFADM='"+jhfandm+"'");
            //分解的时候复制分解和预算申报的
            if (ysbs.Tables[0].Rows[0][0].ToString().Trim() == "5")
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='3' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and JHFADM NOT IN('" + jhfandm + "') and YSBS in('3','5')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
                //申报和批复 只能在申报和批复之间相互复制
            else if (ysbs.Tables[0].Rows[0][0].ToString().Trim() == "3" || ysbs.Tables[0].Rows[0][0].ToString().Trim() == "4")
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='3' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and JHFADM NOT IN('" + jhfandm + "')and YSBS IN('3','4')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            else
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='3' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and JHFADM NOT IN('" + jhfandm + "')and YSBS NOT IN('3','4','5')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取年批复方案的方案
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetSelFA(string yy)
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + yy + "' AND FABS='3' AND YSBS='3' order by DELBZ desc");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
            }
        }
        catch
        { 
        
        }
        return Str;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
     [AjaxPro.AjaxMethod]
    public string loadMb()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> list = new List<string>();
        StringBuilder mbStr = new StringBuilder();
        StringBuilder faStr = new StringBuilder();
        var sql = "SELECT  * FROM TB_YSBBMB WHERE PFFZBZ='1'";
        var count = "SELECT count(*) FROM TB_YSBBMB WHERE PFFZBZ='1'";
        DataSet da = bll.Query(sql);
        DataSet num = bll.Query(count);
        mbStr.Append("{\"total\":");
        mbStr.Append(num.Tables[0].Rows[0][0].ToString());
        mbStr.Append(",");
        mbStr.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            mbStr.Append("{");
            mbStr.Append("\"MBDM\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["MBDM"].ToString() + "\"" + ",");
            mbStr.Append("\"MBMC\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["MBMC"].ToString() + "\"" );
            mbStr.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                mbStr.Append(",");
            }
        }
        mbStr.Append("]}");
        list.Add(faStr.ToString());
        list.Add(mbStr.ToString());

        string s = string.Join("", list.ToArray());
        return s;
    }
    /// <summary>
     /// 年批复方案的复制
    /// </summary>
    /// <param name="jhfadm"></param>
    /// <param name="mbdm"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
     public string ConfirmCopy(string jhfadm,string mbdm,string oldjhfadm)
     {
         try
         {
             TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
             ArrayList ArrList = new ArrayList();
             List<string> SqlList = new List<string>();
             string[] mbdmArry = mbdm.Split(',');
             for (int j = 0; j < mbdmArry.Length; j++)
             {
                 string res = HttpContext.Current.Server.MapPath("../Excels/XlsData/");
                 DataSet fileName = bll.Query("SELECT DISTINCT 'Jhfadm'+'~'+'" + jhfadm + "'+'~'+'Mb'+'~'+'M'+'" + mbdmArry[j] + "'+'.XLS',MBDM FROM TB_BBFYSJ WHERE JHFADM='" + jhfadm + "' and MBDM='" + mbdmArry[j] + "'");
                 for (int i = 0; i < fileName.Tables[0].Rows.Count; i++)
                 {
                     bool boo = File.Exists(res + fileName.Tables[0].Rows[i][0]);
                     if (boo)
                     {
                         string newFileName = "Jhfadm~" + oldjhfadm + "~Mb~M" + fileName.Tables[0].Rows[i][1] + ".XLS";
                         File.Copy(res + fileName.Tables[0].Rows[i][0].ToString(), res + newFileName, true);
                     }
                 }
             }
             mbdm = mbdm.Replace(",", "','");
             //将这些模板的文件以及模板数据全部删除
             string delsql = "delete from TB_BBFYSJ WHERE JHFADM='" + oldjhfadm + "' and MBDM IN('" + mbdm + "')";
             bll.ExecuteSql(delsql);
             //查询当前方案有哪些模板文件和数据
             DataSet sql = bll.Query("select * FROM TB_BBFYSJ WHERE JHFADM='" + jhfadm + "' and MBDM IN ('" + mbdm + "')");
             if (sql.Tables[0].Rows.Count == 0)
             {
                 return "该方案没有模板文件和数据";
             }
             else
             {
                 for (int i = 0; i < sql.Tables[0].Rows.Count; i++)
                 {
                     string insert = "insert into TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) ";
                     insert += "values(@JHFADM,@MBDM,@SHEETNAME,@ZYDM,@XMDM,@SJDX,@JSDX,@VALUE,@JLDW,@ZFVALUE,@XMFL,@STATE)";
                     SqlList.Add(insert);
                     AseParameter[] GSP = {
                            new AseParameter("@JHFADM",AseDbType.VarChar),
                            new AseParameter("@MBDM",AseDbType.VarChar),
                            new AseParameter("@SHEETNAME",AseDbType.VarChar),
                            new AseParameter("@ZYDM",AseDbType.VarChar),
                            new AseParameter("@XMDM",AseDbType.VarChar),
                            new AseParameter("@SJDX",AseDbType.VarChar),
                            new AseParameter("@JSDX",AseDbType.VarChar),
                            new AseParameter("@VALUE",AseDbType.Double,8),
                            new AseParameter("@JLDW",AseDbType.VarChar),
                            new AseParameter("@ZFVALUE",AseDbType.VarChar),
                            new AseParameter("@XMFL",AseDbType.VarChar),
                            new AseParameter("@STATE",AseDbType.VarChar)
                            };
                     GSP[0].Value = oldjhfadm;
                     GSP[1].Value = sql.Tables[0].Rows[i][1];
                     GSP[2].Value = sql.Tables[0].Rows[i][2];
                     GSP[3].Value = sql.Tables[0].Rows[i][3];
                     GSP[4].Value = sql.Tables[0].Rows[i][4];
                     GSP[5].Value = sql.Tables[0].Rows[i][5];
                     GSP[6].Value = sql.Tables[0].Rows[i][6];
                     if (sql.Tables[0].Rows[i][7] == "" || sql.Tables[0].Rows[i][7] == null || string.IsNullOrEmpty(sql.Tables[0].Rows[i][7].ToString()))
                     {
                         GSP[7].Value = DBNull.Value;
                     }
                     else
                     {
                         GSP[7].Value = sql.Tables[0].Rows[i][7];
                     }
                     GSP[8].Value = sql.Tables[0].Rows[i][8];
                     GSP[9].Value = sql.Tables[0].Rows[i][9];
                     GSP[10].Value = sql.Tables[0].Rows[i][10];
                     GSP[11].Value = sql.Tables[0].Rows[i][11];
                     ArrList.Add(GSP);
                     //string add = "insert into TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) values('" + oldjhfadm + "','" + sql.Tables[0].Rows[i][1] + "','" + sql.Tables[0].Rows[i][2] + "','" + sql.Tables[0].Rows[i][3] + "','" + sql.Tables[0].Rows[i][4] + "','" + sql.Tables[0].Rows[i][5] + "','" + sql.Tables[0].Rows[i][6] + "'," + sql.Tables[0].Rows[i][7] + ",'" + sql.Tables[0].Rows[i][8] + "','" + sql.Tables[0].Rows[i][9] + "','" + sql.Tables[0].Rows[i][10] + "','" + sql.Tables[0].Rows[i][11] + "')";
                     //SqlList.Add(add);
                   
                 }
             }
             bll.ExecuteSql(SqlList.ToArray(), ArrList);
         }
         catch
         {
             return "复制失败！";
         }
        return "复制成功！";
     }
    /// <summary>
    /// 复制
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int Copy(string newjhfadm,string oldjhfandm,string yy)
    {
        int result=0;
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            List<String> ArrList = new List<String>();
            //找到当前方案所在的月份
            DataSet nn = bll.Query("select NN from TB_JHFA where JHFADM='"+oldjhfandm+"'");
            //int rowsAffected = 0;
            DataSet hszxdm = bll.Query("select HSZXDM from TB_JHFA WHERE JHFADM='"+oldjhfandm+"'");
            AseParameter[] param=new AseParameter[]
            {
                new AseParameter("@P_NewJHFADM",newjhfadm),
                new AseParameter("@P_OldJHFADM",oldjhfandm),
                new AseParameter("@P_OldHSZXDM",hszxdm.Tables[0].Rows[0][0]),
            };
            DbHelperOra.RunProcedure("FZ_JHFADM_CopyFA", param, out result);
            string res = HttpContext.Current.Server.MapPath("../Excels/XlsData/");
            DataSet fileName = bll.Query("SELECT DISTINCT 'Jhfadm'+'~'+'" + oldjhfandm + "'+'~'+'Mb'+'~'+'M'+MBDM+'.XLS',MBDM FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            DataSet sql = bll.Query("select * FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            for (int i = 0; i < fileName.Tables[0].Rows.Count; i++)
            {
                bool boo = File.Exists(res + fileName.Tables[0].Rows[i][0]);
                if (boo)
                {
                    string newFileName = "Jhfadm~" + newjhfadm + "~Mb~M" + fileName.Tables[0].Rows[i][1] + ".XLS";
                    File.Copy(res + fileName.Tables[0].Rows[i][0].ToString(), res + newFileName, true);
                }
            }
            string del = "delete from TB_BBFYSJ where JHFADM='" + newjhfadm + "'";
            ArrList.Add(del);
            for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
            {
                string add = "insert into TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) values('" + newjhfadm + "','" + sql.Tables[0].Rows[j][1] + "','" + sql.Tables[0].Rows[j][2] + "','" + sql.Tables[0].Rows[j][3] + "','" + sql.Tables[0].Rows[j][4] + "','" + sql.Tables[0].Rows[j][5] + "','" + sql.Tables[0].Rows[j][6] + "'," + sql.Tables[0].Rows[j][7] + ",'" + sql.Tables[0].Rows[j][8] + "','" + sql.Tables[0].Rows[j][9] + "','" + sql.Tables[0].Rows[j][10] + "','" + sql.Tables[0].Rows[j][11] + "')";
                ArrList.Add(add);
            }
            bll.ExecuteSql(ArrList.ToArray(), null);
        }
        catch 
        {

        }
        return result;
    }
    #endregion
    #region
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 90).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px;\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue.Trim() == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM)
    {
        return getNewLine(XMDM, false, "");
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    #endregion
}