﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using System.Data;
using System.Collections;
using System.Text;
using RMYH.Model;
using System.IO;

public partial class CWYS_CWYS_JD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CWYS_CWYS_JD));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount)
    {
        return GetDataListstring("10144643", "", new string[] { ":YY"}, new string[] {Session["YY"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144643");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_JHFA", id, "JHFADM");
        }
        catch
        {
            return "删除失败！";
        }
        return "删除成功！";
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            for (int i = 0; i < ID.Length; i++)
            {
                //计划方案名称
                string jhfaname = values[i].Split('|')[0];
                //年
                string yy = values[i].Split('|')[1];
                //季度
                string jd = values[i].Split('|')[2];
                //hszxdm
                string hszxdm = values[i].Split('|')[3];
                //审批流程
                string scbs = values[i].Split('|')[4];
                if (ID[i].Trim() != "")//执行修改
                {
                    arr.Clear();
                    arrIdUpdate.Clear();
                    arrIdUpdate.Add(ID[i]);
                    if (jd == "1")
                    {
                        if (values[i].IndexOf(".") > 0)
                        {
                            hszxdm = hszxdm.Split('.')[0];
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 1 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                        else
                        {
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 1 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                    }
                    else if (jd == "2")
                    {
                        if (values[i].IndexOf(".") > 0)
                        {
                            hszxdm = hszxdm.Split('.')[0];
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 4 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                        else
                        {
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 4 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                    }
                    else if (jd == "3")
                    {
                        if (values[i].IndexOf(".") > 0)
                        {
                            hszxdm = hszxdm.Split('.')[0];
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 7 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                        else
                        {
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='0" + 7 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                    }
                    else if (jd == "4")
                    {
                        if (values[i].IndexOf(".") > 0)
                        {
                            hszxdm = hszxdm.Split('.')[0];
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='" + 10 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                        else
                        {
                            string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',JD='" + jd + "',HSZXDM='" + hszxdm + "',SCBS=" + scbs + ",NN='" + 10 + "' where JHFADM='" + ID[i] + "' ";
                            bll.ExecuteSql(update);
                        }
                    }
                    //if (values[i].IndexOf(".") > 0)
                    //{
                    //    arr.Add(values[i].Split('.')[0] + values[i].Split('.')[1].Substring(values[i].Split('.')[1].IndexOf("|")));
                    //}
                    //else
                    //{
                    //    arr.Add(values[i]);
                    //}

                    //ret = bll.Update("10144643", "TB_JHFA", fileds, (String[])arr.ToArray(typeof(string)), "JHFADM", (String[])arrIdUpdate.ToArray(typeof(string)));
                }
                else//执行添加
                {
                    ARR1.Clear();
                    arrIdAdd.Clear();
                    string cpbm = GetStringPrimaryKey();
                    arrIdAdd.Add(ID[i]);
                    string add;
                    if (scbs == "0")
                    {
                        scbs = "1";
                    }
                    //1季度的时候月对应1月份
                    if (jd == "1")
                    {
                        add = "insert into TB_JHFA(JHFANAME,YY,JD,HSZXDM,SCBS,JHFABM,XTSJ,NN,JHFADM,HSBZ,YSBS,FABS) values('" + jhfaname + "','" + yy + "','" + jd + "','" + hszxdm + "'," + scbs + ",'" + cpbm + "','" + DateTime.Now + "','0" + 1 + "','" + cpbm + "','" + 0 + "','" + 0 + "','" + 2 + "')";
                        bll.ExecuteSql(add);
                    }
                    else if (jd == "2")
                    {
                        add = "insert into TB_JHFA(JHFANAME,YY,JD,HSZXDM,SCBS,JHFABM,XTSJ,NN,JHFADM,HSBZ,YSBS,FABS) values('" + jhfaname + "','" + yy + "','" + jd + "','" + hszxdm + "'," + scbs + ",'" + cpbm + "','" + DateTime.Now + "','0" + 4 + "','" + cpbm + "','" + 0 + "','" + 0 + "','" + 2 + "')";
                        bll.ExecuteSql(add);
                    }
                    else if (jd == "3")
                    {
                        add = "insert into TB_JHFA(JHFANAME,YY,JD,HSZXDM,SCBS,JHFABM,XTSJ,NN,JHFADM,HSBZ,YSBS,FABS) values('" + jhfaname + "','" + yy + "','" + jd + "','" + hszxdm + "'," + scbs + ",'" + cpbm + "','" + DateTime.Now + "','0" + 7 + "','" + cpbm + "','" + 0 + "','" + 0 + "','" + 2 + "')";
                        bll.ExecuteSql(add);
                    }
                    else
                    {
                        add = "insert into TB_JHFA(JHFANAME,YY,JD,HSZXDM,SCBS,JHFABM,XTSJ,NN,JHFADM,HSBZ,YSBS,FABS) values('" + jhfaname + "','" + yy + "','" + jd + "','" + hszxdm + "'," + scbs + ",'" + cpbm + "','" + DateTime.Now + "','" + 10 + "','" + cpbm + "','" + 0 + "','" + 0 + "','" + 2 + "')";
                        bll.ExecuteSql(add);
                    }
                    //ret = bll.Update("10144643", "TB_JHFA", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "JHFADM", (String[])arrIdAdd.ToArray(typeof(string)));
                     //获得添加的结果
                    //string[] value = add.Split('|');
                    //string SCBS = value[i].Split('|')[5];
                    //string jhfabm = value[i].Split('|')[0];
                    //DataSet jhfadm = bll.Query("select JHFADM from TB_JHFA where JHFABM='"+jhfabm+"'");
                    ////如果是否走审批流程没勾中 就执行修改
                    //if(SCBS!="1")
                    //{
                    //    string updateScbs = "update TB_JHFA set SCBS=1 where JHFADM='"+jhfadm.Tables[0].Rows[0][0]+"'";
                    //    bll.ExecuteSql(updateScbs);
                    //}
                }
            }
        }
        catch
        {
            return "保存失败！";
        }
        return "保存成功！";
    }
    //取GETNEXTDM(6)
    public string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='6'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='6'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    #endregion
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 根据方案获得相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFA(string yy,string jd, string jhfandm)
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            if (int.Parse(jd) == 0)
            {

                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='2' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and JHFADM NOT IN('" + jhfandm + "')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            else
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='2' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and A.JD='" + jd + "' and JHFADM NOT IN('" + jhfandm + "')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 季度
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetQuarter()
    {
        string str = "";
        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 复制
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int Copy(string newjhfadm, string oldjhfandm,string yy)
    {
        int result = 0;
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //int rowsAffected = 0;
            DataSet hszxdm = bll.Query("select HSZXDM from TB_JHFA WHERE JHFADM='" + oldjhfandm + "'");
            DataSet nn = bll.Query("select NN from TB_JHFA where JHFADM='" + oldjhfandm + "'");
            AseParameter[] param = new AseParameter[]
            {
                new AseParameter("@P_NewJHFADM",newjhfadm),
                new AseParameter("@P_OldJHFADM",oldjhfandm),
                new AseParameter("@P_OldHSZXDM",hszxdm.Tables[0].Rows[0][0]),
            };
            DbHelperOra.RunProcedure("FZ_JHFADM_CopyFA", param, out result);
            string res = HttpContext.Current.Server.MapPath("../Excels/XlsData/");
            DataSet fileName = bll.Query("SELECT DISTINCT 'Jhfadm'+'~'+'" + oldjhfandm + "'+'~'+'Mb'+'~'+'M'+MBDM+'.XLS',MBDM FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            DataSet sql = bll.Query("select * FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            for (int i = 0; i < fileName.Tables[0].Rows.Count; i++)
            {
                bool boo = File.Exists(res + fileName.Tables[0].Rows[i][0]);
                if (boo)
                {
                    string newFileName = "Jhfadm~" + newjhfadm + "~Mb~M" + fileName.Tables[0].Rows[i][1] + ".XLS";
                    File.Copy(res + fileName.Tables[0].Rows[i][0].ToString(), res + newFileName, true);
                }
            }
            string del = "delete from TB_BBFYSJ where JHFADM='" + newjhfadm + "'";
            bll.ExecuteSql(del);
            for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
            {
                string add = "insert into TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) values('" + newjhfadm + "','" + sql.Tables[0].Rows[j][1] + "','" + sql.Tables[0].Rows[j][2] + "','" + sql.Tables[0].Rows[j][3] + "','" + sql.Tables[0].Rows[j][4] + "','" + sql.Tables[0].Rows[j][5] + "','" + sql.Tables[0].Rows[j][6] + "'," + sql.Tables[0].Rows[j][7] + ",'" + sql.Tables[0].Rows[j][8] + "','" + sql.Tables[0].Rows[j][9] + "','" + sql.Tables[0].Rows[j][10] + "','" + sql.Tables[0].Rows[j][11] + "')";
                bll.ExecuteSql(add);
            }
        }
        catch
        {


        }
        return result;
    }

    #region
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 80).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px;\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue.Trim() == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    #endregion
}