﻿<%@ page language="C#" autoeventwireup="true" inherits="YSTB_MBLCImage, App_Web_pdbjcyxk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
    <script type="text/javascript" src="../JS/jquery-1.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="DivTitle"  runat="server" style="position:absolute;text-align:center">
        </div>
        <div id="DivShow" runat="server" style=" position:absolute;text-align:center">
        </div>
        <input type="hidden" runat="server" id="HidCJBM" />
        <input type="hidden" runat="server" id="HidMBDM" />
        <input type="hidden" runat="server" id="HidMB" />
         <script type="text/javascript">
             function Init(OBJ) {
                 if (OBJ != "") {
                    var Arr = OBJ.split('@');
                    $("#<%=HidCJBM.ClientID %>").val(Arr[0].toString());
                    $("#<%=HidMBDM.ClientID %>").val(Arr[1].toString());
                    $("#<%=HidMB.ClientID %>").val(Arr[2].toString());
                    var rtn = YSTB_MBLCImage.GetLCImage($("#<%=HidCJBM.ClientID %>").val(), $("#<%=HidMBDM.ClientID %>").val(), Arr[3].toString(),Arr[4].toString()).value;
                    var str = rtn.split('@');
                    var x = $(window).width() / 2 - (parseInt(str[1].toString()) * 280 - 100) / 2;
                    var y = $(window).height() / 2;
                    var y1 = $(window).height() / 4;
                    $("#DivShow").css("top", (y > 0 ? y : 0) + "px");
                    $("#DivShow").css("left", (x > 0 ? x : 0) + "px");
                    $("#DivTitle").css("top", (y1 > 0 ? y1 : 0) + "px");
                    $("#DivTitle").css("left", (x > 0 ? x + 150 : 0) + "px");
                    document.getElementById("DivTitle").innerHTML = "<strong>模板名称：</strong>" + $("#<%=HidMB.ClientID %>").val();
                    document.getElementById("DivShow").innerHTML = str[0].toString();
                 }
                 else {
                     document.getElementById("DivTitle").innerHTML = "";
                     document.getElementById("DivShow").innerHTML = "";
                 }     
             }
        </script>
    </form>
</body>
</html>
