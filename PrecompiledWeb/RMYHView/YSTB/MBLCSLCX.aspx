﻿<%@ page title="" language="C#" autoeventwireup="true" inherits="YSTB_MBLCSLCX, App_Web_pdbjcyxk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Init() {
            ClearMb();
            $.ajax({
                type: 'get',
                url: 'MBLCHandler.ashx',
                data: {
                    JHFADM: $("#SelJHFA").val(),
                    MBMC: $("#TxtMB").val(),
                    action: "LS",
                    ZYDM: $("#SelZY").val(),
                    ZT:''
                },
                async: true,
                cache: false,
                success: function (result) {

                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function InitDataGrid() {
            $('#TabMB').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageSize: 15,
                pageList: [10, 15, 20, 25, 30, 35, 40],
                onDblClickRow: function (rowIndex, rowData) {
                    var MM = "", JD = "", WJ = "", ML = "", LB = "", Arr;
                    var USERDM = '<%=USERDM %>';
                    var USERNAME = '<%=USERNAME %>';
                    var HSZXDM = '<%=HSZXDM %>';
                    var YS = YSTB_MBLCSLCX.GetYSYF().value;
                    var MB = YSTB_MBLCSLCX.GetMBInfo(rowData.DQMBDM).value;
                    if (MB != "") {
                        Arr = MB.split('@');
                        WJ = Arr[0].toString();
                        ML = Arr[1].toString();
                    }
                    LB = YSTB_MBLCSLCX.GetFAInfo($("#SelJHFA").val()).value;
                    if ($("#SelFALX").val() != "3") {
                        if ($("#SelFALX").val() == "2") {
                            JD = $("#SelData").val();
                        }
                        else {
                            MM = $("#SelData").val();
                        }
                    }
                    OpenMb(rowData.DQMBDM, rowData.MBMC, WJ, $("#TxtYY").val(), MM, JD, $("#SelJHFA").val(), $("#SelJHFA")[0][$("#SelJHFA")[0].selectedIndex].text, YS, USERDM, USERNAME, "0,1", LB, ML, HSZXDM, "1");
                },
                columns: [[
                            { field: 'DQMBDM', title: '模板代码', hidden: true },
                            { field: 'MBMC', title: '模板名称', width: 180 },
                            { field: 'ZYMC', title: '使用部门', width: 150 },
                            { field: 'FSR', title: '发送人', width: 100 },
                        	{ field: 'CLR', title: '处理人', width: 100 },
                        	{ field: 'PHONE', title: '联系电话', width: 120 },
                        	{ field: 'SPZT', title: '处理内容', width: 100 },
                        	{ field: 'CLSJ', title: '处理时间', width: 120 },
                            { field: 'JHFADM', title: '计划方案代码', hidden: true },
                            { field: 'CJBM', title: '层级编码', hidden: true },
                            { field: 'JSDM', title: '角色代码', hidden: true }
                        ]]
            });

        }
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        function BindFALB() {
            var rtn = YSTB_MBLCSLCX.GetFALB().value;
            $("#SelFALX").html(rtn);
        }
        function BindYY() {
            var rtn = YSTB_MBLCSLCX.GetYY().value;
            $("#TxtYY").val(rtn);
        }
        function BindData() {
            var rtn = YSTB_MBLCSLCX.GetData($("#SelFALX").val()).value;
            if (rtn == "-1") {
                $("#<%=LabMM.ClientID%>").attr("style", "display:none");
                $("#SelData").attr("style", "display:none");
            }
            else {
                if ($("#SelFALX").val() == "2") {
                    $("#<%=LabMM.ClientID%>").html("季度：");
                }
                else {
                    $("#<%=LabMM.ClientID%>").html("月份：");
                }
                $("#<%=LabMM.ClientID%>").attr("style", "display:display");
                $("#SelData").attr("style", "display:display");
                document.getElementById("SelData").options.length = 0
                $("#SelData").html(rtn);
            }

        }
        function BindJHFA() {
            var MM = "";
            if ($("#SelFALX").val() != "3") {
                MM = $("#SelData").val();
            }
            var rtn = YSTB_MBLCSLCX.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM).value;
            $("#SelJHFA").html(rtn);
        }
        function ChageFALX() {
            BindData();
            BindJHFA();
        }
        function WinPsw(Flag) {
            if (Flag == true) {
              var selected = $("#TabMB").datagrid('getSelected');
              if (selected) {
                  $("#WinPsw").window('open');
              }
              else {
                  alert("请选择要状态还原的项！");
                  return;
              }
            }
            else {
                $("#WinPsw").window('close');
            }
        }
        //初始化部门
        function InitZY() {
            var rtn = YSTB_MBLCSLCX.InitZY().value;
            $("#SelZY").html(rtn);
        }
        //获取当前页面控件的值
        function GetControlsValue() 
        {
            return $("#SelFALX").val() + "@" + $("#TxtYY").val() + "@" + $("#SelData").val() + "@" + $("#SelJHFA").val() + "@" + $("#SelZY").val() + "@" + $("#TxtMB").val();
        }
        //将上个页面传过来的值，赋值给当前页面的控件
        function SetControlsValue(OBJ) 
        {
            var Arr = OBJ.split('@');
            if ($("#SelFALX").val() == Arr[0] && $("#TxtYY").val() == Arr[1] && ""+$("#SelData").val()+"" == Arr[2] && $("#SelJHFA").val() == Arr[3] && $("#SelZY").val() == Arr[4] && $("#TxtMB").val() == Arr[5]) 
            {
                return;
            }
            else 
            {
                $("#SelFALX").val(Arr[0]);
                ChageFALX();
                $("#TxtYY").val(Arr[1]);
                $("#SelData").val(Arr[2]);
                BindJHFA();
                $("#SelJHFA").val(Arr[3]);
                $("#SelZY").val(Arr[4]);
                $("#TxtMB").textbox("setValue", Arr[5]);
                $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
                InitDataGrid();
                Init();
            }
        }
        function OpenMb(MBDM, MBMC, MBWJ, YY, MM, JD, JHFADM, JHFANAME, YSYF, USERDM, USERNAME, ISDone, FALB, ML, HSZXDM, SFZLC) {
            var url = "pageoffice://|" + geturlpath() + "../Excels/Data/MB.aspx?mbmc=" + getchineseurl(MBMC)
                 + "&mbdm=" + MBDM + "&mbwj=" + getchineseurl(MBWJ) + "&yy=" + YY
                 + "&nn=" + MM + "&jd=" + JD
                 + "&fadm=" + JHFADM + "&fadm2="
                 + "&famc=" + getchineseurl(JHFANAME)
                 + "&ysyf=" + YSYF + "&userdm=" + USERDM
                 + "&username=" + getchineseurl(USERNAME) + "&falb=" + FALB
                 + "&mblb=queryflow&isexe=" + ISDone + "&mblx=" + ML
                 + "&hszxdm=" + HSZXDM
                 + "&sfzlc=" + SFZLC;

            //如果不是IE浏览器
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            window.location.href = url + "|||";
        }
        //清空模版:
        function ClearMb() {
            $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
        }
        $(function () {
            BindFALB();
            BindYY();
            BindData();
            BindJHFA();
            InitZY();
            InitDataGrid();
            Init();
//            ClearMb();
            $("#BtnZTHY").click(function () {
                if (YSTB_MBLCSLCX.IsExistUSER($("#TxtPsw").val()).value) {
                    if (confirm("您确定要执行状态还原操作吗？")) {
                        var selected = $("#TabMB").datagrid('getSelected');
                        if (selected) {
                            var MBDM = selected.DQMBDM;
                            var JHFADM = selected.JHFADM;
                            var CJBM = selected.CJBM;
                            var JSDM = selected.JSDM;
                            var rtn = YSTB_MBLCSLCX.SPZTHY(JHFADM, MBDM, JSDM, CJBM).value;
                            if (rtn > 0) {
                                alert("还原成功！");
                                WinPsw(false);
                                Init();
                                InitDataGrid();
                                return;
                            } else {
                                alert("还原失败！");
                                return;
                            }
                        } else {
                            alert("请选择要状态还原的项！");
                            return;
                        }
                    }
                }
                else {
                    alert("密码输入有误，请重新输入！");
                    $("#TxtPsw").val("");
                    return;
                }
            });
        });
    </script>
    <style type="text/css">
        .style1
        {
            height: 28px;
        }
    </style>
 </head>
 <body>
   <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
      <div region="north" border="true" style="height:70px; padding: 1px;background: #B3DFDA;">
        <table>
            <tr>
                <td class="style1">
                    方案类别：<select id="SelFALX" onchange="ChageFALX()"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style1">
                    年份：<input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:BindJHFA()})"  style="width:80px"  />&nbsp;&nbsp;
                    <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label> 
                    <select id="SelData" onchange="BindJHFA()"  style="width:90px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style1">
                    方案名称： <select id="SelJHFA"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                 <td>
                    使用部门： <select id="SelZY"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
                <td>
                模板名称：<input class="easyui-textbox" type="text" id="TxtMB" style="width:120px" />&nbsp;&nbsp;
                </td>
                <td>
                    <input type="button" class="button5" value="查询" style="width:60px; " onclick ="Init()" />&nbsp;&nbsp;
                    <input type="button" class="button5" value="状态还原" onclick="WinPsw(true)" style="width:100px; " />&nbsp;&nbsp;
                </td>
           </tr>
        </table>
       </div>
       <div id="DivCenter" region="center">
            <div id="DivMb" style="width: 100%; height: 100%;">
                <table id="TabMB" style="width: 100%; height:100%;"  title="预算模板流程历史" data-options="singleSelect:true">
                </table>
            </div>
       </div>
        <div id="WinPsw" class="easyui-window" title="审批状态还原" closed="true" style="width:400px;height:220px;padding:20px;text-align: center;">
            <div>管理员密码：<input type="password" id="TxtPsw" style="width:120px" /></div><br/>
            <input type="button"  id="BtnZTHY" value="确定"  class="button5" style="width:60px" />&nbsp;&nbsp;
            <input type="button" class="button5" value="取消" onclick="WinPsw(false)" style="width:60px" />
        </div>
    </form>
 </body>
</html>

