﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="YSTB_HZXMSZ, App_Web_pdbjcyxk" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div>
&nbsp;&nbsp;&nbsp;汇总项目：<select class="easyui-combobox"  name="HZXM" id="HZXM"  style="width:150px;"></select>&nbsp;&nbsp;
<input type="button" class="button5" value="完全展开" style="width:80px;" onclick ="TreeZD(true)" />&nbsp;&nbsp;
<input type="button" class="button5" value="完全收起" style="width:80px; " onclick ="TreeZD(false)" />&nbsp;&nbsp;
<input id="Button11" class="button5"  type="button" style="width:60px;" value="保存"  onclick="Save()"/>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<br />
   <div style="margin-left:12px;" id="divListView">
       <div style="background-color:#ADD8E6;"><span style="font-weight: 700" >汇总项目列表</span></div>
       <br />
       <ul id="tt"  class="easyui-tree" data-options="checkbox:true">
       </ul>
   </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<script type="text/javascript">
    function getlist() {
        var rtnstr = YSTB_HZXMSZ.GetHZXMJson().value;
        if (rtnstr != "" && rtnstr!=null) {
            var Data = eval("(" + rtnstr + ")");
            $("#tt").tree("loadData", Data);
        }
        $("#tt").tree({
            lines: true
        });
     }
     function LoadXM() {
         var rtn = YSTB_HZXMSZ.GetHZXM().value;
         $("#HZXM").html(rtn);
         $("#HZXM").combobox({
             panelHeight: "auto",
             onSelect: function (record) {
                 //清除之前选中的项
                 var CH = $("#tt").tree("getChecked");
                 for (var i = 0; i < CH.length; i++) {
                     $("#tt").tree("uncheck", CH[i].target);
                 }
                 var XMDM = $("#HZXM").combobox("getValue");
                 var rtn = YSTB_HZXMSZ.GetCheckedNode(XMDM).value;
                 if (rtn != "" && rtn != null) {

                     var DM = rtn.split(',');
                     for (var i = 0; i < DM.length; i++) {
                         var node = $('#tt').tree("find", DM[i].toString());
                         $("#tt").tree("check", node.target);
                     }
                 }
                 //全部展开
                 TreeZD(true);
             }
         });
     }
     function TreeZD(Flag) {
         if (Flag == true) {
             $("#tt").tree("expandAll");
         } else {
             $("#tt").tree("collapseAll");
         }
     }
     function Save() {
         var XMDM = "";
         var DM = $("#HZXM").combobox("getValue");
         if (DM == "") 
         {
             alert("汇总项目不能为空！");
             return;
         }
         var node = $("#tt").tree("getChecked");
         for (var i = 0; i < node.length; i++) 
         {
             if (node[i].attributes.YJDBZ == "1") 
             {
                 if (XMDM == "") {
                     XMDM = node[i].id;
                 }
                 else {
                     XMDM = XMDM+","+node[i].id;
                 }
             }
         }
         if (node.length > 0) {
             if (XMDM == "") {
                 alert("请选择叶子节点！");
                 return;
             }
         }       
         var rtn = YSTB_HZXMSZ.InsertXM(XMDM,DM).value;
         if (rtn > 0) {
             alert("保存成功！");
             return;
         } else {
             alert("保存失败！");
             return;
         }
     }
     $(document).ready(function () {
         getlist();
         LoadXM();
     });
</script>

</asp:Content>

