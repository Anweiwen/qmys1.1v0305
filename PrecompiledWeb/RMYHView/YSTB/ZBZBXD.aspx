﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="YSTB_ZBZBXD, App_Web_pdbjcyxk" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    年份：<input  type="text" id="TxtYY" class="Wdate"  onclick="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"  style="width:120px"  />&nbsp;&nbsp;
    <input type="button" class="button5" value="查询" style="width:60px; " onclick ="getlist('','','')" />&nbsp;&nbsp;
    <input type="button" class="button5" value="添加" style="width:60px; " onclick ="jsAddData()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="删除" style="width:60px;" onclick="Del()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:80px;"/>&nbsp;&nbsp;
    <input id="Button2" class="button5"  type="button" value="保存" style="width:60px;"  onclick="Saves()"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divListView"></div>
    <div id="WinXM" class="easyui-window" title="指标项目选取" closed="true" style="width:950px;height:430px;padding:20px;text-align: center; ">
    <div style=" text-align:left">
      <%--  <input type="button" value="展开" onclick="TreeZK(1)" class="button5" style="width:60px" />&nbsp;&nbsp;
        <input type="button" value="收起" onclick="TreeSQ(1)" class="button5" style="width:60px" />&nbsp;&nbsp;      --%>
        <input type="button" value="确定" onclick="getChecked()" class="button5" style="width:60px" />&nbsp;&nbsp;
        <input type="button" value="退出" onclick="Win(false,-1,1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
        <br /><br />
        <div id="XMXQ"></div>
        <ul checkbox:true id="tt" class="easyui-tree"> 
    </div>
    </div>
  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
   <input type="hidden" id="hidindexid" value="XMDM,MBDM,ZYDM,YY,JSDX" />
   <input type="hidden"   id="hidcheckid" />
   <input  type="hidden" id="hidNewLine"/>
   <input  type="hidden" id="HidXM"/>
   <input  type="hidden" id="HidZY"/>
   <input  type="hidden" id="HidXMBZ"/>
   <input  type="hidden" id="HidZYBZ"/>
   <input  type="hidden" id="HidSelectedXM"/>
   <input  type="hidden" id="HidSelectedZY"/>
   <input type="hidden" id="HidTrID" />
    <input  type="hidden" id="hidyy" runat="server"/>
   <input  type="hidden" id="hidhszxdm" runat="server"/>
   <input  type="hidden" id="hiduserdm" runat="server"/>
    <input  type="hidden" id="hidSQL" runat="server"/>
    <input  type="hidden" id="hidsqls" runat="server"/>
     <input  type="hidden" id="KJID" />
   <script type="text/javascript">
       var XMOBJ, ZYOBJ;
       function getlist(objtr, objid, intimagecount) {
           var rtnstr = YSTB_ZBZBXD.LoadList(objtr, objid, intimagecount,$("#TxtYY").val()).value;
           if (objtr == "" && objid == "" && intimagecount == "")
               document.getElementById("divListView").innerHTML = rtnstr;
           else
               $("#" + objtr).after(rtnstr);
           var tr = $("#divListView tr");
           if (tr.length == 1) 
           {
               InsertLastZB();
           }
           else 
           {
               if (tr.length > 0) 
               {
                   LoadName();
               }
           }


       }

       //本年没有数据，将上年的数据插入本年
       function InsertLastZB() {
            var rtn = YSTB_ZBZBXD.InsertLastZB($("#TxtYY").val()).value;
            if (rtn > 0) {
                getlist('', '', '');
            }         
       }
       function LoadName() {
           var tr = $("#divListView tr");
           for (var i = 1; i < tr.length; i++) {
               var trid = tr[i].id;
               var XM = $("#" + trid + " input[name=tselXMDM]");
               var ZY = $("#" + trid + " input[name=tselZYDM]");
               XM.val(YSTB_ZBZBXD.GetXMMC(XM.val()).value);
               ZY.val(YSTB_ZBZBXD.GetZYNAME(ZY.val()).value);
           }
       }
       function GetXMList(objtr, objid, intimagecount,ID) {
           var rtnstr = YSTB_ZBZBXD.GetXMList(objtr, objid, intimagecount,ID).value;
           if (objtr == "" && objid == "" && intimagecount == "")
               document.getElementById("XMXQ").innerHTML = rtnstr;
           else
               $("#" + objtr).after(rtnstr);
           $("#XMXQ td[name$='tdXMBM']").attr("style", "text-align:left");
       }
       function GetZYList(objtr, objid, intimagecount) {
           var rtnstr = YSTB_ZBZBXD.GetZYList(objtr, objid, intimagecount).value;
           if (objtr == "" && objid == "" && intimagecount == "")
               document.getElementById("ZYXQ").innerHTML = rtnstr;
           else
               $("#" + objtr).after(rtnstr);
           $("#ZYXQ td[name$='tdZYNBBM']").attr("style", "text-align:left");
       }
       function dateChange() {
           getlist('','','');
       }
       function InitData() {
           $("#TxtYY").val(YSTB_ZBZBXD.InitData().value);
       }
       function jsAddData() {
           if ($("#hidNewLine").val() == "")
               $("#hidNewLine").val(YSTB_ZBZBXD.AddData().value);
           var Len = $("#divListView tr").length;
           $("#divListView table").append($("#hidNewLine").val().replace('{000}', Len).replace('{000}', Len));
           var tr = $("#divListView tr");
           if (tr.length > 2) {
               var T = tr[tr.length - 2];
               var trid = T.id;
               var MBDM = $("#" + trid + " select[name=selMBDM]").val();
               $("#tr" + Len + " select[name=selMBDM]").val(MBDM);
               
           }
       }
       function onselects(obj) {
           //表示当前操作的是指标项目列表
           if ($("#HidXM").val() == "-1") {
               $("tr[name^='trdata']").css("backgroundColor", "");
               var trid = obj.parentNode.parentNode.id;
               $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
               $("#HidSelectedXM").val($("#" + trid).find("td[name^='tdXMDM']").html() + "." + $("#" + trid).find("td[name^='tdXMMC']").html());
               var MBDM = "", XMDM = "", YY = "", ZYDM = "", JSDX = "";
              
               var trid = obj.parentNode.parentNode.id;
               //获取联合主键的值
               $("#HidTrID").val(trid);
               var ZJ = $("#hidindexid").val().split(',');
               MBDM = $("tr[id=" + trid + "] select[name^=sel" + ZJ[1].toString() + "]").val();
               XMDM = $("tr[id=" + trid + "] input[name^=tsel" + ZJ[0].toString() + "]").val();
               ZYDM = $("tr[id=" + trid + "] input[name^=tsel" + ZJ[2].toString() + "]").val();
               YY = $("#" + trid).find("td[name^=td" + ZJ[3].toString() + "]").html();
               JSDX = $("tr[id=" + trid + "] select[name^=sel" + ZJ[4].toString() + "]").val();
               $("#hidcheckid").val(XMDM + "," + MBDM + "," + ZYDM + "," + YY + "," + JSDX);
           }
           else {
               var MBDM = "", XMDM = "", YY = "", ZYDM = "",JSDX="";
               $("tr[name^='trdata']").css("backgroundColor", "");
               var trid = obj.parentNode.parentNode.id;
               $("#HidTrID").val(trid);
               $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
               //获取联合主键的值
               var ZJ = $("#hidindexid").val().split(',');
               MBDM = $("tr[id=" + trid + "] select[name^=sel" + ZJ[1].toString() + "]").val();
               XMDM = $("tr[id=" + trid + "] input[name^=tsel" + ZJ[0].toString() + "]").val();
               ZYDM = $("tr[id=" + trid + "] input[name^=tsel" + ZJ[2].toString() + "]").val();
               YY = $("#" + trid).find("td[name^=td" + ZJ[3].toString() + "]").html();
               JSDX = $("tr[id=" + trid + "] select[name^=sel" + ZJ[4].toString() + "]").val();
               $("#hidcheckid").val(XMDM + "," + MBDM + "," + ZYDM + "," + YY+","+JSDX);
           }

       }
       var OBJSS;
       var KJZ;
       function selectvalues(obj, id) {
           KJZ = obj;
           OBJSS = obj;
           $("#<%=hidsqls.ClientID %>").val(obj);
           //判断当前打开的是指标项目窗口还是作业目录窗口
           if (obj.parentNode.innerHTML.indexOf("tselXMDM") > -1) 
           {
               XMOBJ = obj;
               Win(true, 0, 1, id);
           }
           else 
           {
               var par = obj.parentNode.parentNode.id;
               var parvalue = $("#" + par + " input[name^='tselGS']").val(); //找到当前行的计划方案名称
               var userdm = $("#<%=hiduserdm.ClientID %>").val();
               var yy = $("#<%=hidyy.ClientID %>").val();
               var hszxdm = $("#<%=hidhszxdm.ClientID %>").val();
               //window.open("bbgsjs.aspx?USERDM=" + userdm + "&YY=" + yy + "&HSZXDM=" + hszxdm + "&GS=" + encodeURIComponent(parvalue) + "&rnd=" + Math.random(), "", "Width:450px;Height:500px; dialogLeft:400px; dialogTop:400px; status:no; directories:yes;scrollbars:no;Resizable=no; ");


               var iWidth = 500;
               var iHeight = 600;
               var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
               var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
               var win = window.open("bbgsjs.aspx?USERDM=" + userdm + "&YY=" + yy + "&HSZXDM=" + hszxdm + "&GS=" + encodeURIComponent(parvalue) + "&rnd=" + Math.random(), "弹出窗口", "width=" + iWidth + ", height=" + iHeight + ",top=" + iTop + ",left=" + iLeft + ",toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no,alwaysRaised=yes,depended=yes");
           }
       }
       function getChidValue(rnt) {
           var par = KJZ.parentNode.parentNode.id;
           //           KJZ.parentNode.childNodes[0].value = value;
           if (rnt) {
               var parvalue = $("#" + par + " input[name^='tselGS']").val(); //找到当前行的计划方案名称
               var gs = rnt.split('~')[0].replace(/\s+/g, "");
               var gsjx = rnt.split('~')[1].replace(/\s+/g, "");
               if (gsjx == "") {
                   gsjx = YSTB_ZBZBXD.retjx(rnt.split('~')[0]).value;
               }
               //判断当前公式是否改变
               if (parvalue != gs) {
                   $("#" + par + " input[name^='tselGS']").val(gs);
                   $("#" + par + " input[name^='txtGSJX']").val(gsjx);
                   if (KJZ.parentNode.parentNode.getElementsByTagName("img")[KJZ.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                       KJZ.parentNode.parentNode.getElementsByTagName("img")[KJZ.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
               }
           }
       }
      

      
       //Flag=true，表示打开窗体；等于false，表示关闭窗体
       //BZ等于1表示点击“确定”按钮；等于-1，表示点击“关闭”按钮
       //IsXMWin等于1，表示当前操作的是指标项目窗体；等于2，表示当前操作的是作业目录窗体
       function Win(Flag, BZ, IsXMWin, id) {
           if (IsXMWin == 1) 
           {
               if (Flag == true) {
                   $("#HidXM").val("-1");
                   $("#WinXM").window('open');
                   //GetXMList('', '', '', id);
                   jsonTree(id);
               }
               else 
               {
                   if (BZ == -1) {
                       $("#HidXMBZ").val("1");
                   }
                   else {
                       $("#HidXMBZ").val("");
                   }
                   $("#WinXM").window('close');
               }
           } 
           else 
           {
               if (Flag == true) 
               {
                   $("#HidZY").val("-1");
                   $("#WinZY").window('open');
                   GetZYList('','','');
               }
               else 
               {
                   if (BZ == -1) {
                       $("#HidZYBZ").val("1");
                   }
                   else {
                       $("#HidZYBZ").val("");
                   }
                   $("#WinZY").window('close');
               }
           }
       }
       function WinXMCloseEvent() {
           $("#WinXM").window({
               modal: true,
               closable: false,
               collapsible: false,
               minimizable: false,
               maximizable: false,
               onBeforeClose: function () {
                   if ($("#HidXMBZ").val() != "1") {
                       if ($("#HidSelectedXM").val() != "") {
                           XMOBJ.parentNode.childNodes[0].value = $("#HidSelectedXM").val();
                           if ($("#" + XMOBJ.parentNode.parentNode.id + " img[name=readimage]")[0].href.indexOf("new.jpg") == -1) {
                               $("#" + XMOBJ.parentNode.parentNode.id + " img[name=readimage]").attr("src", "../Images/Tree/edit.jpg");
                           }
                           $("#HidSelectedXM").val("");
                           $("#HidXMBZ").val("");
                           $("#HidXM").val("");
                       }
                   }
                   XMOBJ = null;
               }
           });
       }
       function WinZYCloseEvent() {
           $("#WinZY").window({
               modal: true,
               closable: false,
               collapsible: false,
               minimizable: false,
               maximizable: false,
               onBeforeClose: function () {
                   if ($("#HidZYBZ").val() != "1") {
                       if ($("#HidSelectedZY").val() != "") {
                           ZYOBJ.parentNode.childNodes[0].value = $("#HidSelectedZY").val();
                           if ($("#" + ZYOBJ.parentNode.parentNode.id + " img[name=readimage]")[0].href.indexOf("new.jpg") == -1) {
                               $("#" + ZYOBJ.parentNode.parentNode.id + " img[name=readimage]").attr("src", "../Images/Tree/edit.jpg");
                           }
                           $("#HidSelectedZY").val("");
                           $("#HidZYBZ").val("");
                           $("#HidZY").val("");
                       }
                   }
                   ZYOBJ = null;
               }
           });
       }
       function TreeZK(Flag) {
           var ID;
           if (Flag == "1") {
               ID = "XMXQ";
           } else {
               ID = "ZYXQ";
           }
           var tr = $("#"+ID+" img[src$='tplus.gif']");
           for (var i = 0; i < tr.length; ) {
               var trid = tr[i].parentNode.parentNode.id;
               ToExpand(tr[i], trid);
               i = 0;
               tr = $("#"+ID+" img[src$='tplus.gif']");
           }
       }
       function TreeSQ(Flag) {
           var ID;
           if (Flag == "1") {
               ID = "XMXQ";
           } else {
               ID = "ZYXQ";
           }
           var tr = $("#"+ID+" img[src$='tminus.gif']");
           for (var i = 0; i < tr.length; i++) {
               var trid = tr[i].parentNode.parentNode.id;
               var PAR = $("#" + trid + " td[name^='tdPAR']").html();
               if (PAR == 0) {
                   ToExpand(tr[i], trid);
               }
           }
       }
       function ToExpand(obj, id) {
           var ID;
           var trid = obj.parentNode.parentNode.id;
           if ($("#" + trid).find("td[name^=tdXMDM]").length > 0) {
               ID = "XMXQ";
           } else {
               ID = "ZYXQ";
           }
           var trs;
           if (obj.src.indexOf("tminus.gif") > -1) {
               obj.src = "../Images/Tree/tplus.gif";
               trs = $("#"+ID+" tr");
               for (var i = 0; i < trs.length; i++) {
                   if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                       trs[i].style.display = "none";
                       var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                       if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                           objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                       }
                       else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                           objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                       }
                   }
               }
           }
           else if (obj.src.indexOf("lminus.gif") > -1) {
               obj.src = "../Images/Tree/lplus.gif";
               trs = $("#" + ID + " tr");
               for (var i = 0; i < trs.length; i++) {
                   if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                       trs[i].style.display = "none";
                       var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                       if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                           objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                       }
                       else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                           objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                       }
                   }
               }
           }
           else if (obj.src.indexOf("lplus.gif") > -1) {
               obj.src = "../Images/Tree/lminus.gif";
               trs = $("#" + ID + " tr");
               for (var i = 0; i < trs.length; i++) {
                   if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                       trs[i].style.display = "";
                   }
               }
           }
           else if (obj.src.indexOf("tplus.gif") > -1) {
               obj.src = "../Images/Tree/tminus.gif";
               trs = $("#" + ID + " tr");
               var istoload = true;
               for (var i = 0; i < trs.length; i++) {
                   if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                       trs[i].style.display = "";
                       istoload = false;
                   }
               }
               if (istoload == true) {
                   if (ID == "XMXQ") {
                       GetXMList(id, $("tr[id=" + id + "] td[name$='tdXMDM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                   } else {
                       GetZYList(id, $("tr[id=" + id + "] td[name$='tdZYDM']").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                   }
                   trs = $("#" + ID + " tr");
                   for (var i = 0; i < trs.length; i++) {
                       if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                           trs[i].style.display = "";
                       }
                   }
               }
           }
       }
       
       function Saves() {
           var objnulls = $("[ISNULLS=N]");
           var strnullmessage = "";
           for (i = 0; i < objnulls.length; i++) {
               if (objnulls[i].value.trim() == "") {
                   strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                   alert(strnullmessage);
                   return;
               }
           }
           var E = $("#divListView img[src$='edit.jpg']");
           var D = $("#divListView img[src$='delete.gif']");
           var N = $("#divListView img[src$='new.jpg']");
           var NewStr = "", UpdateStr = "", DelStr = "", XMDM = "", MBDM = "", YY = "", JLDW = "", Value = "",XM="",MB="",GS="",GSJX="",WCFW="";
           if (E.length == 0 && D.length == 0 && N.length == 0) {
               alert("您没有新增、修改或者删除的项！");
               return;
           }
           else {
               if (N.length > 0) {
                   if ($("#TxtYY").val() == "") {
                       alert("年份不能为空！");
                       return;
                   }
               }
           }
           //拼接新增的相关信息字符串
           for (var i = 0; i < N.length; i++) {
               var trid = N[i].parentNode.parentNode.id;
               if (trid != "") {
                   XMDM = $("tr[id=" + trid + "] input[name^=tselXMDM]").val();
                   MBDM = $("tr[id=" + trid + "] select[name^=selMBDM]").val();
                   JLDW = $("tr[id=" + trid + "] select[name^=selJLDW]").val();
                   GS = $("tr[id=" + trid + "] input[name^=tselGS]").val();
                   GSJX = $("tr[id=" + trid + "] input[name^=txtGSJX]").val();
                   WCFW = $("tr[id=" + trid + "] input[name^=txtWCFW]").val();
                   Value = $("#" + trid + " input[name^='txtVALUE']").val();
                   if (NewStr == "") {
                       NewStr = GS + "~" + GSJX + "~" + WCFW + "~" + XMDM + "~" + MBDM + "~" + JLDW + "~" + Value + "~" + $("#TxtYY").val();
                   }
                   else {
                       NewStr = NewStr + "&" + GS + "~" + GSJX + "~" + WCFW + "~" + XMDM + "~" + MBDM + "~" + JLDW + "~" + Value + "~" + $("#TxtYY").val();
                   }
               }
           }
           //拼接修改的字符串
           for (var i = 0; i < E.length; i++) {
               var trid = E[i].parentNode.parentNode.id;
               if (trid != "") {
                   XMDM = $("tr[id=" + trid + "] input[name^=tselXMDM]").val();
                   MBDM = $("tr[id=" + trid + "] select[name^=selMBDM]").val();
                   JLDW = $("tr[id=" + trid + "] select[name^=selJLDW]").val();
                   GS = $("tr[id=" + trid + "] input[name^=tselGS]").val();
                   GSJX = $("tr[id=" + trid + "] input[name^=txtGSJX]").val();
                   WCFW = $("tr[id=" + trid + "] input[name^=txtWCFW]").val();
                   Value = $("#" + trid + " input[name^='txtVALUE']").val();
                   YY = $("#" + trid).find("td[name^=tdYY]").html();
                   XM = $("#" + trid).find("td[name^=tdXM]").html();
                   MB = $("#" + trid).find("td[name^=tdMB]").html();
                   ZY = $("#" + trid).find("td[name^=tdZY]").html();
                   JS = $("#" + trid).find("td[name^=tdJS]").html();
                   if (UpdateStr == "") {
                       UpdateStr = GS + "~" + GSJX + "~" + WCFW + "~" + XMDM + "~" + MBDM + "~" + JLDW + "~" + Value + "~" + YY + "~" + XM + "~" + MB;
                   }
                   else {
                       UpdateStr = UpdateStr + "&" + GS + "~" + GSJX + "~" + WCFW + "~" + XMDM + "~" + MBDM + "~" + JLDW + "~" + Value + "~" + YY + "~" + XM + "~" + MB;
                   }
               }
           }
           //拼接删除的相关信息的字符串
           for (var j = 0; j < D.length; j++) {
               var trid = D[j].parentNode.parentNode.id;
               if (trid != "") {
                   YY = $("#" + trid).find("td[name^=tdYY]").html();
                   XM = $("#" + trid).find("td[name^=tdXM]").html();
                   MB = $("#" + trid).find("td[name^=tdMB]").html();
                
                   if (DelStr == "") {
                       DelStr = XM + "~" + MB + "~" + YY + "~";
                   }
                   else {
                       DelStr = DelStr + "&" + XM + "~" + MB + "~" + YY + "~";
                   }
               }
           }
           var rtn = YSTB_ZBZBXD.UpdateORDel(NewStr,UpdateStr,DelStr).value;
           if (rtn > 0) {
               alert("保存成功！");
               getlist('', '', '');
               return;
           }
           else {
               alert("保存失败！");
               return;
           }
       }
       function Del() {
           debugger;
           if ($("#HidTrID").val() != "")
               $("#" + $("#HidTrID").val() +" img[name=readimage]").attr("src", "../Images/Tree/delete.gif");
       }
       function Cdel() {
           if ($("#HidTrID").val() != "")
               $("#" + $("#HidTrID").val() +" img[name=readimage]").attr("src", "../Images/Tree/noexpand.gif");
       }

       function jsonTree(id) {
           var rtn = YSTB_ZBZBXD.GetChildJson("0", $("#<%=hidSQL.ClientID %>").val(),id);
           var dj = null;
           $('#tt').tree({
               lines: true,
               checkbox: true,
               cascadeCheck: false,
               onBeforeExpand: function (node) {
                   var childrens = $('#tt').tree('getChildren', node.target);
                   if (childrens == false) {
                       var res = YSTB_ZBZBXD.GetChildJson(node.id, $("#<%=hidSQL.ClientID %>").val(),id);
                       var Data = eval("(" + res.value + ")");
                       //var selected = $('#tt').tree('getSelected');
                       $('#tt').tree('append', {
                           parent: node.target,
                           data: Data
                       });
                   }
               },
               onExpand: function (node) {

               },
               onCollapse: function (node) {

               },
               onClick: function (node) {

               }
              

           });
           if (rtn.value != "") {
               var Data = eval("(" + rtn.value + ")");
               $("#tt").tree("loadData", Data);
           }
       }


       function getChecked() {
           var nodes = $('#tt').tree('getChecked');
           if (nodes.length > 1) {
               alert("财务模板条件值只能一个，请重新选择");
           }
           else {
               var s = "";
               for (var i = 0; i < nodes.length; i++) {
                   s += nodes[i].id.replace(/(\s*$)/g, "") + "." + nodes[i].text.replace(/(\s*$)/g, "") + "|";
               }
               if (s != "") {
                   s = s.substring(0, s.length - 1);
               }
               var trid = OBJSS.parentNode.parentNode.id;
               if ($("#" + trid + " input[name=tselXMDM]").val() != s) {
                   if (OBJSS.parentNode.parentNode.getElementsByTagName("img")[OBJSS.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                       OBJSS.parentNode.parentNode.getElementsByTagName("img")[OBJSS.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
               }
               $("#" + trid + " input[name=tselXMDM]").val(s);
               $("#WinXM").window('close');
           }


       }
       function GSChanged(obj) {
           if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
               obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
           var par = obj.parentNode.parentNode.id;
           var parvalue = $("#" + par + " input[name^='tselGS']").val();
           gsjx = YSTB_ZBZBXD.retjx(parvalue).value;
           $("#" + par + " input[name^='txtGSJX']").val(gsjx);
       }
       $(document).ready(function () {
           InitData();
           getlist('', '', '');
           WinXMCloseEvent();
           WinZYCloseEvent();
       });
   </script>
</asp:Content>

