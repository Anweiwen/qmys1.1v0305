﻿<%@ WebHandler Language="C#" Class="MBLCListHandler" %>

using System;
using System.Data;
using System.Web;
using RMYH.BLL;
using RMYH.DAL;
using System.IO;
using System.Text;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
public class MBLCListHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];  //by liguosheng
        //string JHFADM = context.Request.QueryString["JHFADM"].ToString();
        //string MBMC = context.Request.QueryString["MBMC"].ToString();
        //string ZYDM = context.Request.QueryString["ZYDM"].ToString();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //string MBStr=BLL.GetAllMBCXJSON(JHFADM, ZYDM, MBMC);
        //context.Response.Write(MBStr);
        string JHFADM ="";
        string MBMC ="";
        string ZYDM="";
        string MBStr = "";
        string STATE = "";
        string HSZXDM = "";
        
        if (string.IsNullOrEmpty(action))
        {
             JHFADM = context.Request.QueryString["JHFADM"].ToString();
             MBMC = context.Request.QueryString["MBMC"].ToString();
             ZYDM = context.Request.QueryString["ZYDM"].ToString();
             HSZXDM = context.Request.QueryString["HSZXDM"].ToString();
             STATE = context.Request.QueryString["ZT"].ToString();
             MBStr = BLL.GetAllMBCXJSON(JHFADM, ZYDM, MBMC, HSZXDM, STATE);
        
        }else{           
            switch (action)
            { 
                case "GetDownLoadsList":
                    JHFADM = context.Request.QueryString["jhfadm"];
                    ZYDM = context.Request.QueryString["zydm"];
                    MBMC = context.Request.QueryString["mbmc"];
                    HSZXDM = context.Request.QueryString["hszxdm"];
                    STATE = context.Request.QueryString["state"];
                    MBStr = DownLoadsList(JHFADM,ZYDM,MBMC,HSZXDM,STATE);
                    break;
                case "GetDownLoads":
                    JHFADM = context.Request["jhfadm"];
                    string excelFiles = context.Request["excelFiles"];
                    string zipFileName = context.Request["zipFileName"];
                    GetDownLoad(JHFADM, excelFiles, zipFileName);
                    break;
            }
           
        }
        context.Response.Write(MBStr);
    }



    /// <summary>
    /// 查询计划方案下所有的模板状态,重写李建峰的 RMYH.DAL里，TB_ZDSXBDAL.cs的GetAllMBCXJSON 方法，去掉了一些不需要的结果字段，其他的查询条件完全一样，前端支持多选，以方便批量下载
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="ZYDM">作业代码</param>
    /// <returns></returns>
    public string GetAllMBCXJSON(string JHFADM, string ZYDM, string MBMC, string HSZXDM, string ZT)
    {
        int Total = 0;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
        DataSet dsValue = new DataSet();
        //获取非受限角色
        string JS = BLL.getNotQXJSDM(HSZXDM,JHFADM);
        StringBuilder Str = new StringBuilder();
        StringBuilder strSql = new StringBuilder();
        string COLUMNNAME = "", JSNAME = "", MBZQ = "", MBLX = "";
        DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }

        if (ZT.Equals("-1") || ZT.Equals("2"))
        {
            strSql.Append(" SELECT DISTINCT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'未处理' STATE FROM  TB_YSBBMB B,TB_YSMBLC A,TB_JHZYML C");
            strSql.Append("  WHERE B.MBDM NOT IN(SELECT DQMBDM FROM TB_MBLCSB WHERE JHFADM='" + JHFADM + "') AND B.MBZQ='" + MBZQ + "'");
            strSql.Append("  AND A.QZMBDM=B.MBDM AND A.CJBM IN(SELECT MAX(CJBM) FROM TB_YSMBLC WHERE QZMBDM=B.MBDM AND MBLX='" + MBLX + "' AND CJBM NOT IN(SELECT L1.CJBM FROM TB_YSMBLC L1,TB_YSMBLC L2,TB_YSBBMB B1 WHERE L1.JSDM=L2.JSDM AND L2.QZMBDM=L1.QZMBDM AND L1.CJBM<>L2.CJBM AND L1.CJBM LIKE '%'+SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4)+'%' AND L1.MBLX='" + MBLX + "' AND L2.MBLX='" + MBLX + "' AND B1.MBDM=L1.QZMBDM AND B1.MBZQ='" + MBZQ + "')) AND A.MBLX='" + MBLX + "'");
            strSql.Append("  AND A.LX<>'0'");
            strSql.Append("  AND B.ZYDM=C.ZYDM");
            if (ZYDM != "0")
            {
                strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
            }
            strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
        }
        if (ZT.Equals("3") || ZT.Equals("-1"))
        {
            if (!strSql.ToString().Trim().Equals(""))
            {
                strSql.Append(" UNION");
            }
            strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'已处理' STATE FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C");
            strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='" + MBZQ + "'");
            strSql.Append(" AND A.CLSJ IN(SELECT MAX(CLSJ) FROM TB_MBLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.DQMBDM=A.DQMBDM AND NOT EXISTS(SELECT * FROM TB_MBLCSB S2 WHERE S2.JHFADM='" + JHFADM + "' AND  S2.DQMBDM=S.DQMBDM AND STATE=-1 ))");
            strSql.Append(" AND A.JHFADM='" + JHFADM + "'");
            strSql.Append("  AND B.ZYDM=C.ZYDM");
            if (ZYDM != "0")
            {
                strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
            }
            strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
        }
        if (ZT.Equals("0") || ZT.Equals("-1"))
        {
            if (!strSql.ToString().Trim().Equals(""))
            {
                strSql.Append(" UNION");
            }
            strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待填报' STATE  FROM  TB_YSMBLC A,TB_YSBBMB B,TB_JHZYML C,TB_YSLCFS FS");
            strSql.Append(" WHERE A.QZMBDM=B.MBDM AND A.MBLX='" + MBLX + "' AND B.MBZQ='" + MBZQ + "'  AND A.MBLX=FS.YSLX AND FS.ISFQ=1 AND FS.JHFADM='" + JHFADM + "' AND LX='0'");
            strSql.Append(" AND NOT EXISTS (SELECT DISTINCT DQMBDM FROM TB_YSMBLC L,TB_MBLCSB S WHERE  S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND S.JHFADM='" + JHFADM + "' AND S.DQMBDM=A.QZMBDM AND S.JSDM=A.JSDM AND S.CJBM=A.CJBM AND STATE=1 AND LX='0' AND L.MBLX='" + MBLX + "')");
            strSql.Append("  AND B.ZYDM=C.ZYDM");
            if (ZYDM != "0")
            {
                strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
            }
            strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
            strSql.Append(" UNION");
            strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待填报' STATE  FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C,TB_YSMBLC LC");
            strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='" + MBZQ + "' AND A.JHFADM='" + JHFADM + "' AND A.STATE=-1 AND A.DQMBDM=LC.QZMBDM AND A.JSDM=A.JSDM AND A.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' AND LC.LX='0'");
            strSql.Append("  AND B.ZYDM=C.ZYDM");
            if (ZYDM != "0")
            {
                strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
            }
            strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
        }
        if (ZT.Equals("1") || ZT.Equals("-1"))
        {
            if (!strSql.ToString().Trim().Equals(""))
            {
                strSql.Append(" UNION");
            }
            strSql.Append(" SELECT B.MBDM,B.MBMC,B.MBWJ,C.ZYMC,A.JSDM DQJSDM,NULL DQJSNAME,'待审核' STATE  FROM  TB_MBLCSB A,TB_YSBBMB B,TB_JHZYML C,TB_YSMBLC LC");
            //strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='" + MBZQ + "'  AND A.CJBM IN (SELECT CJBM FROM TB_MBLCSB S2 WHERE S2.JHFADM='" + JHFADM + "' AND S2.STATE=-1 ) ");
            strSql.Append(" WHERE A.DQMBDM=B.MBDM AND B.MBZQ='" + MBZQ + "'  AND A.JHFADM='" + JHFADM + "' AND A.STATE=-1 AND A.DQMBDM=LC.QZMBDM AND A.JSDM=A.JSDM AND A.CJBM=LC.CJBM AND LC.MBLX='" + MBLX + "' AND LC.LX<>'0'");
            strSql.Append("  AND B.ZYDM=C.ZYDM");
            if (ZYDM != "0")
            {
                strSql.Append("  AND B.ZYDM='" + ZYDM + "'");
            }
            strSql.Append(" AND B.MBMC LIKE '%" + MBMC + "%'");
        }
        strSql.Append("  ORDER BY C.ZYMC,STATE");

        return strSql.ToString();
    }
    
    
    /// <summary>
    /// 获取下载模板列表，并提示哪些模板不存在  
    /// </summary>
    /// <param name="jhfadm">计方案代码</param>
    /// <param name="zydm">作业代码</param>
    /// <param name="mbmc">模板名称</param>
    /// <returns></returns>
    public string DownLoadsList(string jhfadm,string zydm,string mbmc,string hszxdm,string zt)
    {
        string COLUMNNAME = ""; bool boo = false;
        StringBuilder Str = new StringBuilder();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = GetAllMBCXJSON(jhfadm, zydm, mbmc,hszxdm,zt);
        string path = HttpContext.Current.Server.MapPath("../Excels/XlsData/");
        DataSet da = BLL.Query(sql);
        //获取查询条件的总行数
        Str.Append("{\"total\":" + da.Tables[0].Rows.Count + ",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            for (int j = 0; j < da.Tables[0].Columns.Count; j++)
            {
                COLUMNNAME = da.Tables[0].Columns[j].ColumnName;
                Str.Append("\"" + COLUMNNAME + "\":");
                Str.Append("\"" + da.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                if (j < da.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
                else {
                    Str.Append(",");
                    Str.Append("\"SFCZ\":");
                    //判断服务器上模板文件是否存在
                    boo = File.Exists(path + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + da.Tables[0].Rows[i]["MBWJ"].ToString());
                    if (boo)//存在
                    {
                        Str.Append("\"" + "模板存在" + "\"");
                    }
                    else//不存在
                    {
                        Str.Append("\"" + "模板不存在" + "\"");
                    }
                
                }
            }
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
        
    }

    /// <summary>
    /// 批量进行多个文件压缩到一个文件  
    /// </summary>
    /// <param name="jhfadm"></param>
    /// <param name="excelFiles"></param>
    /// <param name="zipFileName"></param>
    /// <returns></returns>
    public void GetDownLoad(string jhfadm, string excelFiles, string zipFileName)
    {
        string excelfiles = "";
        string filesName = "";
        string[] files = excelFiles.Split('|');
        MemoryStream ms = new MemoryStream();
        byte[] buffer = null;
        using (ZipFile file = ZipFile.Create(ms))
        {
            file.BeginUpdate();

            file.NameTransform = new MyNameTransfom();
            foreach (string item in files)
            {
                excelfiles = item.Split(',')[0];
                filesName = item.Split(',')[1];
                if (File.Exists(HttpContext.Current.Server.MapPath("../Excels/XlsData/") + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + excelfiles))
                {
                    file.Add(HttpContext.Current.Server.MapPath("../Excels/XlsData/") + "Jhfadm" + "~" + jhfadm + "~" + "Mb" + "~" + excelfiles, filesName + ".XLS");//添加文件并重命名
                }
            }
            file.CommitUpdate();
            buffer = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(buffer, 0, buffer.Length);   //读取文件内容(1次读ms.Length/1024M)  
            ms.Flush();
            ms.Close();
        }
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/x-zip-compressed";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(zipFileName));
        HttpContext.Current.Response.BinaryWrite(buffer);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();  
     }

    /// <summary>
    /// 处理生成的文件夹内容，否则会把生成的excel目录也压缩进去
    /// </summary>
    public class MyNameTransfom : ICSharpCode.SharpZipLib.Core.INameTransform
    {

        #region INameTransform 成员

        public string TransformDirectory(string name)
        {
            return null;
        }

        public string TransformFile(string name)
        {
            return Path.GetFileName(name);
        }

        #endregion
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}