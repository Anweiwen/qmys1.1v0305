﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="YSTB_YSLCFS, App_Web_pdbjcyxk" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    预算类型：<select id="SelYSLX" onchange="getlist('','','')"  style="width:90px;"></select>&nbsp;&nbsp;
    <input type="button" class="button5" value="添加" style="width:60px; " onclick ="jsAddData()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="删除" style="width:60px;" onclick="Del()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:80px;"/>&nbsp;&nbsp;
    <input id="Button2" class="button5"  type="button" value="保存" style="width:60px;"  onclick="Saves()"/>&nbsp;&nbsp;
    <input id="Button1" class="button5"  type="button" value="公用非受限角色设置" style="width:150px;"  onclick="WinJS(true,0)"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
     <div id="divListView">
     </div>
     <div id="WinJS" class="easyui-window" title="角色选取" minimizable="false" closed="true" style="width:800px;height:350px;padding:20px;text-align: center; ">
        <div style=" text-align:left;width: 100%; height: 100%;" class="easyui-layout" fit="true">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                角色名称：<input type="text" id="TxtJS" style="width:120px" />&nbsp;&nbsp;
                <input type="button" value="查询" onclick="GetJSList()" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
            <div data-options="region:'center'" style="width:100%; height:100%;">
                <div id="JSXQ"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="" />
<input type="hidden"   id="hidcheckid" />
<input  type="hidden" id="hidNewLine"/>
<input  type="hidden" id="HidTrID"/>
<input  type="hidden" id="HidFlag"/>
<input  type="hidden" id="HidFADM"/>
<script type="text/javascript">
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = YSTB_YSLCFS.LoadList(objtr, objid, intimagecount, $("#SelYSLX").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divListView").innerHTML = rtnstr;
        $("#HidTrID").val("");
        Check();
        $("#divListView select[name^=selYSLX]").attr("disabled", "disabled");
    }
    function jsAddData() {
        if ($("#hidNewLine").val() == "") {
            $("#hidNewLine").val(YSTB_YSLCFS.AddData($("#SelYSLX").val()).value);
//            debugger;
//            var L = $("#hidNewLine").val().lastIndexOf("</td><td name>");
//            var Str = $("#hidNewLine").val().substring(0, L).trim() + "<input  name=\"TxtData\" onchange=\"DataChange(this)\" onclick=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'})\" style=\"width:150px\"  class=\"Wdate\"  />" + $("#hidNewLine").val().substring(L).trim();
//            $("#hidNewLine").val(Str);
        }
        var Len = $("#divListView tr").length;
        $("#divListView table").append($("#hidNewLine").val().replace('{000}', Len).replace('{000}', Len));
    }
    function Saves() {
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value.trim() == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                alert(strnullmessage);
                return;
            }
        }
        var E = $("#divListView img[src$='edit.jpg']");
        var D = $("#divListView img[src$='delete.gif']");
        var N = $("#divListView img[src$='new.jpg']");
        var NewStr = "", UpdateStr = "", DelStr = "", YSLX = "", JHFADM = "",LX="",FA="",ISFQ,JZSJ="",TBBZ="";
        if (E.length == 0 && D.length == 0 && N.length == 0) {
            alert("您没有新增、修改或者删除的项！");
            return;
        }
        if (D.length > 0) {
            if (confirm("您确定要执行删除操作吗？如果选择“确定”，之前的填报、审批记录将一并被删除！确定要删除吗？") == false) {
                return;
            }
        }
        //拼接新增的相关信息字符串
        for (var i = 0; i < N.length; i++) {
            var trid = N[i].parentNode.parentNode.id;
            if (trid != "") {
                YSLX = $("tr[id=" + trid + "] select[name^=selYSLX]").val();
                JHFADM = $("tr[id=" + trid + "] select[name^=selJHFADM]").val();
                ISFQ = $("#" + trid + " input[name^='chkISFQ']")[0].checked == true ? "1" : "0";
                JZSJ = $("#" + trid + " input[name^=TxtData]").val();
                TBBZ = $("#" + trid + " input[name^='chkXZTBBZ']")[0].checked == true ? "1" : "0";
                if (NewStr == "") {
                    NewStr = YSLX + "," + JHFADM + "," + ISFQ+","+JZSJ+","+TBBZ;
                }
                else {
                    NewStr = NewStr + "|" + YSLX + "," + JHFADM + "," + ISFQ + "," + JZSJ + "," + TBBZ;
                }
            }
        }
        //拼接修改的字符串
        for (var i = 0; i < E.length; i++) {
            var trid = E[i].parentNode.parentNode.id;
            if (trid != "") {
                YSLX = $("tr[id=" + trid + "] select[name^=selYSLX]").val();
                JHFADM = $("tr[id=" + trid + "] select[name^=selJHFADM]").val();
                ISFQ = $("#" + trid + " input[name^='chkISFQ']")[0].checked == true ? "1" : "0";
                LX = $("#" + trid).find("td[name^=tdLX]").html();
                FA = $("#" + trid).find("td[name^=tdFA]").html();
                JZSJ = $("#" + trid + " input[name^=TxtData]").val();
                TBBZ = $("#" + trid + " input[name^='chkXZTBBZ']")[0].checked == true ? "1" : "0";
                if (UpdateStr == "") {
                    UpdateStr = YSLX + "," + JHFADM + "," + ISFQ + "," + LX + "," + FA + "," + JZSJ + "," + TBBZ;
                }
                else {
                    UpdateStr = UpdateStr + "|" + YSLX + "," + JHFADM + "," + ISFQ + "," + LX + "," + FA + "," + JZSJ + "," + TBBZ;
                }
            }
        }
        //拼接删除的相关信息的字符串
        for (var j = 0; j < D.length; j++) {
            var trid = D[j].parentNode.parentNode.id;
            if (trid != "") {
                LX = $("#" + trid).find("td[name^=tdLX]").html();
                FA = $("#" + trid).find("td[name^=tdFA]").html();
                if (DelStr == "") {
                    DelStr = LX + "," + FA;
                }
                else {
                    DelStr = DelStr + "|" + LX + "," + FA ;
                }
            }
        }
        var rtn = YSTB_YSLCFS.UpdateORDel(NewStr, UpdateStr, DelStr).value;
        if (rtn > 0) {
            alert("保存成功！");
            getlist('', '', '');
            return;
        }
        else {
            alert("保存失败！");
            return;
        }
    }
    function Check() {
        $("select[name='selYSLX']").change();
        $("img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
    }
    function EditData(obj, flag) {
        var trid = obj.parentNode.parentNode.id;
        if (obj.name == "selYSLX") 
        {
            var FA = $("#" + trid).find("td[name^=tdFA]").html();
            var ss = YSTB_YSLCFS.BindJHFA(obj.value,FA).value;
            if (ss != null && ss != "") 
            {
                $("#" + trid + " select[name='selJHFADM']").html(ss);
            }
            else 
            {
                var s = $("#" + trid + " select[name='selJHFADM']");
                s[0].options.length = 0;
                s[0][0] = new Option("", "");
            }
        }
        else if (obj.name == "chkXZTBBZ") {
            if (obj.checked == true) 
            {
                var JZSJ = $("#" + trid + " input[name='TxtData']").val();
                if (JZSJ == "" || JZSJ == null || JZSJ == "undefined") 
                {
                    alert("截止时间不为空的情况下，才可以勾选限制填报标记！");
                    obj.checked = false;
                    return;
                }
            }
        }
        if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
            obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
    }
    function onselects(obj) {
        $("tr[name^='trdata']").css("backgroundColor", "");
        var trid = obj.parentNode.parentNode.id;
        $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
        $("#HidTrID").val(trid);
    }
    function Del() {
        if ($("#HidTrID").val() != "")
            $("#" + $("#HidTrID").val() + " img[name=readimage]").attr("src", "../Images/Tree/delete.gif");
    }
    function Cdel() {
        if ($("#HidTrID").val() != "")
            $("#" + $("#HidTrID").val() + " img[name=readimage]").attr("src", "../Images/Tree/noexpand.gif");
    }
    //将截止时间列变为时间框
    function InitData() {
        $("#divListView").find("td[name^='tdTBSJXZ']").each(function (index, element) {
            element.innerHTML = "<input  name=\"TxtData\" value='" + element.innerHTML + "'  onchange=\"DataChange(this)\" onclick=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'})\" style=\"width:150px\"  class=\"Wdate\" />";
        });
    }
    //时间框改变时，将行前面的图标设置成编辑图标（添加除外）
    function DataChange(obj) {
        var trid = obj.parentNode.parentNode.id;
        var rtn = YSTB_YSLCFS.InitData().value;
        if (obj.value != "") 
        {
            if (obj.value < rtn) 
            {
                alert("您选择的时间不能小于当前时间，请重新选择！");
                obj.value = "";
                return;
            }
            else 
            {
                var XZ = $("#" + trid + " input[name='chkXZTBBZ']");
                if (XZ[0].checked == false) {
                    XZ[0].checked = true;
                }
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.toString().indexOf("new.jpg") == -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
            }
        }
        else 
        {
            var XZ = $("#" + trid + " input[name='chkXZTBBZ']");
            if (XZ[0].checked == true) 
            {
                XZ[0].checked = false;
            }
            if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.toString().indexOf("new.jpg") == -1)
                obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
        }
    }
    //刷新角色列表
    function GetJSList() {
        var rtnstr = YSTB_YSLCFS.GetJSList($("#TxtJS").val()).value;
        document.getElementById("JSXQ").innerHTML = rtnstr;
        $("#JSXQ INPUT[type=button]").attr("style", "display:none");
    }
    //打开或者关闭角色列表框
    function WinJS(Flag, BZ) 
    {
        if (Flag == true) 
        {
            $("#WinJS").window('open');
            GetJSList();
            GetCheckList();
        }
        else {
            if (BZ == 1) 
            {
                if (confirm("您确定要执行此操作吗？")) 
                {
                    var JSDM = "";
                    var CK = $("#JSXQ INPUT:checked");
                    for (var i = 0; i < CK.length; i++) 
                    {
                        var trid = CK[i].parentNode.parentNode.id;
                        var DM = $("#" + trid).find("td[name^='tdJSDM']")[0].innerHTML;
                        if (JSDM == "") 
                        {
                            JSDM = DM;
                        }
                        else 
                        {
                            JSDM = JSDM + "," + DM;
                        }
                    }
                    var rtn = YSTB_YSLCFS.AddJS(JSDM,$("#HidFlag").val(), $("#HidFADM").val()).value;
                    if (rtn > 0) 
                    {
                        $("#HidFlag").val("");
                        $("#HidFADM").val("");
                        alert("保存成功！");
                        getlist('', '', '');
                    }
                    else 
                    {
                        alert("保存失败！");
                        return;
                    }
                    $("#WinJS").window('close');
                }
            }
            else 
            {
                $("#HidFlag").val("");
                $("#HidFADM").val("");
                $("#WinJS").window('close');
            }
        }
    }
    //将之前保存的角色选中
    function GetCheckList() {
        var rtn = YSTB_YSLCFS.GetCheckedJS($("#HidFlag").val(), $("#HidFADM").val()).value;
        if (rtn != "" && rtn!=null) 
        {
            var Arr = rtn.split(',');
            for (var i = 0; i < Arr.length; i++) {
                td = $("#JSXQ td[name=tdJSDM]").filter(function () { return this.innerHTML == "" + Arr[i] + "" });
                if (td.length > 0) {
                    trid = td[0].parentNode.id;
                    CK = $("#" + trid + " input[name=chkISCHECK]");
                    if (CK[0].checked == false) {
                        CK[0].checked = true;
                    }
                }
            }
        }
    }
    function BindYSLX() {
        var rtn = YSTB_YSLCFS.GetYSLX().value;
        $("#SelYSLX").html(rtn);
    }
    //打开设置方案非受限角色窗口
    function SZJS(JHFADM) 
    {
        $("#HidFlag").val("1");
        $("#HidFADM").val(JHFADM);
        WinJS(true, 0);
    }
    function WinJSCloseEvent() {
        $("#WinJS").window({
            onBeforeClose: function () {
                $("#HidFlag").val("");
                $("#HidFADM").val("");
            }
        });
    }
    $(document).ready(function () {
        BindYSLX();
        getlist('', '', '');
        WinJSCloseEvent();
    });
</script>
</asp:Content>

