﻿<%@ page language="C#" autoeventwireup="true" inherits="YSTB_LoadTree, App_Web_pdbjcyxk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="~/Content/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="~/Content/themes/icon.css" />
    <link rel="Stylesheet" type="text/css" href="/CSS/demo.css" />
    <link rel="stylesheet" type="text/css" href="~/CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="~/CSS/style.css" />
    <script src="<%=Request.ApplicationPath%>/JS/jquery.min.js" type="text/javascript"></script>   
    <script src="<%=Request.ApplicationPath%>/JS/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="<%=Request.ApplicationPath%>/JS/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="<%=Request.ApplicationPath%>/JS/mainScript.js" type="text/javascript"></script>
    <script src="<%=Request.ApplicationPath%>/JS/Main.js" type="text/javascript"></script>
    <script src="<%=Request.ApplicationPath%>/JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="<%=Request.ApplicationPath%>/WdatePicker/WdatePicker.js" type="text/javascript"></script>
</head>
<body>
   <form id="FORM1" runat="server"  class="easyui-layout" fit="true">
     <div style=" text-align:left;width: 100%; height: 100%;">
            <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                <input type="button" value="确定" onclick="OpenTree(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                <input type="button" value="退出" onclick="OpenTree(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
            </div>
             <div data-options="region:'center'" style="width:100%; height:100%;">
                   <table id="TabTreeGrid" class="easyui-treegrid" style="width: 100%; height:100%;">
                        <thead>  
                        <tr>
                          <%--  <th data-options="field:'ck',checkbox:true">选择</th>      --%>
                            <th data-options="field:'MBDM', hidden: true">模板代码</th>   
                            <th data-options="field:'MBBM',hidden: true">模板编码</th>   
                            <th data-options="field:'MBMC',width:150">模板名称</th>   
                            <th data-options="field:'LX',width:60">流程类型</th>  
                            <th data-options="field:'JSDM', hidden: true">角色代码</th>   
                            <th data-options="field:'JSNAME',width:120">角色</th>   
                            <th data-options="field:'XMMC',width:40">模板类型</th>   
                            <th data-options="field:'CJBM', hidden: true">层级编码</th>  
                        </tr>   
                    </thead>   
                </table>
             </div>
       </div>
       <input type="hidden" runat="server" id="HidYSLX" />
       <input type="hidden" runat="server" id="HidMBZQ" />
       <script type="text/javascript">
           function InitTreeGrid() {
               $('#TabTreeGrid').treegrid({
                   singleSelect: false,
                   lines: true,
                   idField: 'CJBM',
                   treeField: 'MBMC',
                   autoRowHeight: false,
                   fitColumns: true,
                   url: "LoadTreeHandle.ashx?CJBM=0&YSLX=" + $("#<%=HidYSLX.ClientID %>").val() + "&MBZQ=" + $("#<%=HidMBZQ.ClientID %>").val(),
                   onBeforeExpand: function (row) {
                       var url = "LoadTreeHandle.ashx?CJBM=" + row.CJBM + "&YSLX=" + $("#<%=HidYSLX.ClientID %>").val() + "&MBZQ=" + $("#<%=HidMBZQ.ClientID %>").val();
                       $(this).treegrid("options").url = url;
                       return true;
                   }
               });
           }
           $(document).ready(function () {
               InitTreeGrid();
           });   
    </script>
   </form>
</body>
</html>
