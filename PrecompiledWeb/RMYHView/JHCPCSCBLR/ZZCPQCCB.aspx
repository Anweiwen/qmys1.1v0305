﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="JHCPCSCBLR_ZZCPQCCB, App_Web_2xyqzyir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 <div style="padding: 3px;">
        方案类别：<select id="SelFALB" onchange="FALBChage()" style="width: 50px;"></select>&nbsp;&nbsp;
        查询期：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
            style="width: 120px" />
        <div id="idMonth" style="display: inline;">
            月：<select id="SelMonth" onchange="MonthChage()" style="width: 100px;"></select></div>
        <div id="idQuarter" style="display: none;">
            季度：<select id="SelQuarter" onchange="QuarterChage()" style="width: 100px;"></select></div>
        方案名：<select id="SelFANAME" onchange="FANAMEChage()" style="width: 150px;"></select>&nbsp;&nbsp;
        <input id="Button1" class="button5" type="button" value="保存" onclick="SetValues()" />
        <input id="Button2" class="button5" style="width: 100px" type="button" value="采集期初成本"
            onclick="Collection()" />
       <input id="excel" class="button5" style="width:100px" type="button" value="导出excel" onclick="Excel()" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divTreeListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="ZZCPDM" />
<input type="hidden"   id="hidcheckid" />
<script type="text/javascript">
    $(document).ready(function () {
        BindFALB();
        InitData();
        BindMonths();
        BindQuarter();
        BindFANAME();
        getlist('', '', '');
    });
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = JHCPCSCBLR_ZZCPQCCB.LoadList(objtr, objid, intimagecount, $("#SelFANAME").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = JHCPCSCBLR_ZZCPQCCB.UpdateData(objid, objfileds, objvalues, $("#SelFANAME").val()).value;
            alert(rtn);
    }
    //改变方案类别
    function FALBChage() {
        var faidx = $("#SelFALB").val();
        if (faidx == "1") {//月
            //显示月份
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        else if (faidx == "2") {//季度
            //隐藏月
            $("#idMonth").css("display", "none");
            //显示季度
            $("#idQuarter").css("display", "inline");
            //清空月
            $("#SelMonth").val("0");
        }
        else if (faidx == "3") {//年
            //隐藏月
            $("#idMonth").css("display", "none");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
            //清空月
            $("#SelMonth").val("0");
        }
        else {//其它
            //显示月
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        BindFANAME();
        getlist('', '', '');
    }
    function dateChange() {
        BindFANAME();
        getlist('', '', '');
    }
    function FANAMEChage() {
        getlist('', '', '');
    }
    function MonthChage() {
        BindFANAME();
        getlist('', '', '');
    }
    function QuarterChage() {
        BindFANAME();
        getlist('', '', '');
    }
    //年份
    function InitData() {
        $("#TxtYY").val(JHCPCSCBLR_ZZCPQCCB.InitData().value);
    }
    //月份
    function BindMonths() {
        $("#SelMonth").html(JHCPCSCBLR_ZZCPQCCB.GetMonths().value);
    }
    //季度
    function BindQuarter() {
        $("#SelQuarter").html(JHCPCSCBLR_ZZCPQCCB.GetQuarter().value);
    }
    //方案类别
    function BindFALB() {
        var rtn = JHCPCSCBLR_ZZCPQCCB.GetFALB().value;
        $("#SelFALB").html(rtn);
    }
    //方案名
    function BindFANAME() {
        var rtn = JHCPCSCBLR_ZZCPQCCB.GetFANAME($("#SelFALB").val(), $("#TxtYY").val(), $("#SelMonth").val(), $("#SelQuarter").val()).value;
        $("#SelFANAME").html(rtn);
    }
    //采集财务数据
    function Collection() {
        var rtn = JHCPCSCBLR_ZZCPQCCB.Collection($("#TxtYY").val(), $("#SelMonth").val(), $("#SelFANAME").val()).value;
        getlist('', '', '');
        alert(rtn);
    }
    //导出excel
    function Excel() {
        var rtn = JHCPCSCBLR_ZZCPQCCB.ExportExcelByDataTable($("#SelFANAME").val(), $("#TxtYY").val()).value;
        window.open("<%=Request.ApplicationPath%>/ExcelFile/" + rtn);
    }
</script>
</asp:Content>

