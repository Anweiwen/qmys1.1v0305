﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="JHCPCSCBLR_YJZH, App_Web_2xyqzyir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
        方案类别：<select id="SelFALB" onchange="FALBChage()" style="width: 50px;"></select>&nbsp;&nbsp;
        查询期：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
            style="width: 120px" />
        <div id="idMonth" style="display: inline;">
            月：<select id="SelMonth" onchange="MonthChage()" style="width: 100px;"></select></div>
        <div id="idQuarter" style="display: none;">
            季度：<select id="SelQuarter" onchange="QuarterChage()" style="width: 100px;"></select></div>
        方案名：<select id="SelFANAME" onchange="FANAMEChage()" style="width: 150px;"></select>
    <input type="button" class="button5" value="油价转换" onclick="Conversion()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style="width: 100%; height: 100%">
        <tr>
            <td style=" padding-left:25%">
                元/吨：<input id="TxtYD" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")' />&nbsp;
                桶/吨：<input id="TxtTD" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")' />&nbsp;
                元/美元：<input id="TxtMYY" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")' />&nbsp;
                美元/桶：<input id="TxtMYT" style="width: 120px" type="text" disabled="disabled" />&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<script type="text/javascript">
    $(document).ready(function () {
        BindFALB();
        InitData();
        BindMonths();
        BindQuarter();
        BindFANAME();
        YD();
        TD();
        MYY();
        MYT();
    });
    //改变方案类别
    function FALBChage() {
        var faidx = $("#SelFALB").val();
        if (faidx == "1") {
            //显示月份
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        else if (faidx == "2") {
            //隐藏月
            $("#idMonth").css("display", "none");
            //显示季度
            $("#idQuarter").css("display", "inline");
            //清空月
            $("#SelMonth").val("0");
        }
        else if (faidx == "3") {
            //隐藏月
            $("#idMonth").css("display", "none");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
            //清空月
            $("#SelMonth").val("0");
        }
        else {
            //显示月
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        BindFANAME();
    }
    //改变年
    function dateChange() {
        BindFANAME();
        YD();
        TD();
        MYY();
        MYT();
    }
    //改变方案
    function FANAMEChage() {
        YD();
        TD();
        MYY();
        MYT();
    }
    //改变月
    function MonthChage() {
        BindFANAME();
        YD();
        TD();
        MYY();
        MYT();
    }
    //改变季度
    function QuarterChage() {
        BindFANAME();
        YD();
        TD();
        MYY();
        MYT();
    }
    /// <summary>年份</summary>
    function InitData() {
        $("#TxtYY").val(JHCPCSCBLR_YJZH.InitData().value);
    }
    /// <summary>月份</summary>
    function BindMonths() {
        $("#SelMonth").html(JHCPCSCBLR_YJZH.GetMonths().value);
    }
    /// <summary>季度</summary>
    function BindQuarter() {
        $("#SelQuarter").html(JHCPCSCBLR_YJZH.GetQuarter().value);
    }
    /// <summary>方案类别</summary>
    function BindFALB() {
        var rtn = JHCPCSCBLR_YJZH.GetFALB().value;
        $("#SelFALB").html(rtn);
    }
    /// <summary>方案名</summary>
    function BindFANAME() {
        var rtn = JHCPCSCBLR_YJZH.GetFANAME($("#SelFALB").val(), $("#TxtYY").val(), $("#SelMonth").val(), $("#SelQuarter").val()).value;
        $("#SelFANAME").html(rtn);
    }
    /// <summary>获取 元/吨</summary>
    function YD() {
        var rtn = JHCPCSCBLR_YJZH.YD($("#SelFANAME").val()).value;
        $("#TxtYD").val(rtn);
    }
    /// <summary>获取 桶/吨</summary>
    function TD() {
        var rtn = JHCPCSCBLR_YJZH.TD($("#SelFANAME").val()).value;
        $("#TxtTD").val(rtn);
    }
    /// <summary>获取 美元/元</summary>
    function MYY() {
        var rtn = JHCPCSCBLR_YJZH.MYY($("#SelFANAME").val()).value;
        $("#TxtMYY").val(rtn);
    }
    /// <summary>获取 美元/桶</summary>
    function MYT() {
        var rtn = JHCPCSCBLR_YJZH.MYT($("#SelFANAME").val()).value;
        $("#TxtMYT").val(rtn);
    }
    /// <summary>油价转换</summary>
    function Conversion() {
        var rtn = JHCPCSCBLR_YJZH.Conversion($("#TxtYD").val(), $("#TxtTD").val(), $("#TxtMYY").val(), $("#SelFANAME").val()).value;
        $("#TxtMYT").val(rtn);
    }
</script>
</asp:Content>

