﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="JHCPCSCBLR_FCRLDLJG, App_Web_2xyqzyir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div style="padding: 3px;">
        方案类别：<select id="SelFALB" onchange="FALBChage()" style="width: 50px;"></select>&nbsp;&nbsp;
        查询期：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
            style="width: 120px" />
        <div id="idMonth" style="display: inline;">
            月：<select id="SelMonth" onchange="MonthChage()" style="width: 100px;"></select></div>
        <div id="idQuarter" style="display: none;">
            季度：<select id="SelQuarter" onchange="QuarterChage()" style="width: 100px;"></select></div>
        方案名：<select id="SelFANAME" onchange="FANAMEChage()" style="width: 150px;"></select>&nbsp;&nbsp;
        <input id="Button11" class="button5" type="button" value="保存" onclick="SetValues()" />
        <input id="excel" class="button5" style="width:100px" type="button" value="导出excel" onclick="Excel()" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<div id="divTreeListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="YLBM" />
<input type="hidden"   id="hidcheckid" />
<script type="text/javascript">
    $(document).ready(function () {
        BindFALB();
        InitData();
        BindMonths();
        BindQuarter();
        BindFANAME();
        getlist('', '', '');
    });
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = JHCPCSCBLR_FCRLDLJG.LoadList(objtr, objid, intimagecount, $("#SelFANAME").val(), $("#TxtYY").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = JHCPCSCBLR_FCRLDLJG.UpdateData(objid, objfileds, objvalues, $("#SelFANAME").val()).value;
        alert(rtn);
    }
    //改变方案类别
    function FALBChage() {
        var faidx = $("#SelFALB").val();
        if (faidx == "1") {
            //显示月份
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        else if (faidx == "2") {
            //隐藏月
            $("#idMonth").css("display", "none");
            //显示季度
            $("#idQuarter").css("display", "inline");
            //清空月
            $("#SelMonth").val("0");
        }
        else if (faidx == "3") {
            //隐藏月
            $("#idMonth").css("display", "none");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
            //清空月
            $("#SelMonth").val("0");
        }
        else {
            //显示月
            $("#idMonth").css("display", "inline");
            //隐藏季度
            $("#idQuarter").css("display", "none");
            //清空季度
            $("#SelQuarter").val("0");
        }
        BindFANAME();
        getlist('', '', '');
    }
    //改变年
    function dateChange() {
        BindFANAME();
        getlist('', '', '');
    }
    //改变方案
    function FANAMEChage() {
        getlist('', '', '');
    }
    //改变月
    function MonthChage() {
        BindFANAME();
        getlist('', '', '');
    }
    //改变季度
    function QuarterChage() {
        BindFANAME();
        getlist('', '', '');
    }
    //年份
    function InitData() {
        $("#TxtYY").val(JHCPCSCBLR_FCRLDLJG.InitData().value);
    }
    //月份
    function BindMonths() {
        $("#SelMonth").html(JHCPCSCBLR_FCRLDLJG.GetMonths().value);
    }
    //季度
    function BindQuarter() {
        $("#SelQuarter").html(JHCPCSCBLR_FCRLDLJG.GetQuarter().value);
    }
    //方案类别
    function BindFALB() {
        var rtn = JHCPCSCBLR_FCRLDLJG.GetFALB().value;
        $("#SelFALB").html(rtn);
    }
    //方案名
    function BindFANAME() {
        var rtn = JHCPCSCBLR_FCRLDLJG.GetFANAME($("#SelFALB").val(), $("#TxtYY").val(), $("#SelMonth").val(), $("#SelQuarter").val()).value;
        $("#SelFANAME").html(rtn);
    }
    //导出excel
    function Excel() {
        var rtn = JHCPCSCBLR_FCRLDLJG.ExportExcelByDataTable($("#SelFANAME").val(), $("#TxtYY").val()).value;
        window.open("<%=Request.ApplicationPath%>/ExcelFile/" + rtn);
    }
</script>
</asp:Content>



