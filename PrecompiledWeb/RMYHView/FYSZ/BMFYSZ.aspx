﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="FYSZ_BMFYSZ, App_Web_lv5wv2wr" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<br
      <table id="tbbutton">
       <tr>
        <td><input type="button" class="button5"  value="刷新" onclick ="getlist('', '', '')" /></td>
        <td><input class="button5" onclick="jsAddData()" type="button" value="添加" />  </td>
        <td><input type="button" class="button5" value="删除" onclick="Del()" /></td>
        <td><input type="button" class="button5" value="取消删除" onclick="Cdel()" /></td>
        <td><input id="Button11" class="button5"  type="button" value="保存"  onclick="SetValues()" /></td>
         <td><input class="button5" type="button" value="退出" onclick ="PageClose(this)" /> </td>
       </tr>
     </table>
     
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style="height:100%">
    <tr>
      <td style="width:230px; height:100%;border: solid 1px #9ACB34;">
         <div class="summary-title" id="dsd">  &nbsp;使用部门</div>
         <div style=" width:230;height:100%" id="TreeVie"><asp:TreeView runat="server" ID="TreeView1"></asp:TreeView></div>
       </td>
       <td style =" width :100%;vertical-align: top">
            <div style="width:100%; height:100%;border: solid 1px #9ACB34; " id="div3">
                 <div  style="overflow:scroll" id="divTreeListView"></div>
            </div>
       </td>
    </tr>
    </table>
    <div id="SjzyDiv" style=" display:none">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="ID" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   
<input type="hidden" id="hide_FBDM" runat="server" /> 
<input type="hidden" id="hide_ccjb" runat="server" /> 
<input type="hidden" id="HidMC" runat="server" /> 

<script type="text/javascript">

    $(document).ready(function () {
        getlist('', '', '');
    });
    //树形菜单选中加粗
    function setNodeStyle(text) {
        debugger;
        var objs = document.getElementById("TreeVie").getElementsByTagName("a");
        $("#TreeVie a").css('fontWeight', '');
        for (var i = 0; i < objs.length; i++) {
            if (objs[i].innerText == text) {
                objs[i].style.fontWeight = "bold";
                return;
            }
        }
    }
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = FYSZ_BMFYSZ.LoadList(objtr, objid, intimagecount, $("#<%=hide_FBDM.ClientID %>").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    //添加数据
    function jsAddData() {
        if ($("#<%=hide_FBDM.ClientID %>").val() == "") {
            alert("请选择报表目录");
        }
        else {
           if($("#hidNewLine").val() == "")
            $("#hidNewLine").val(FYSZ_BMFYSZ.AddData().value);
            $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));

        }
   
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        if ($("#<%=hide_FBDM.ClientID %>").val() == "") {
            alert("请选择报表目录");
        }
        else {
            var rtn = FYSZ_BMFYSZ.UpdateData(objid, objfileds, objvalues, $("#<%=hide_FBDM.ClientID %>").val()).value;
            if (rtn != "" && rtn.substring(0, 1) == "0") {
                if (rtn.length > 1) {
                    alert(rtn.substring(1));
                }
            }
        }
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = FYSZ_BMFYSZ.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
</script>
</asp:Content>