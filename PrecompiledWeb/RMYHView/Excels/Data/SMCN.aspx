﻿<%@ page language="C#" autoeventwireup="true" inherits="Excels_Data_Default, App_Web_ofwlznev" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html style="width: 600px;height: 450px" >
<head>
    <script language="javascript" type="text/javascript">
        function setVar(v_sel) {
            document.getElementById("txta").value = v_sel.value;
        }

        function phone(id, value) {
            var rex = /^1[3-8]+\d{9}$/;
            //var rex=/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
            //区号：前面一个0，后面跟2-3位数字 ： 0\d{2,3}
            //电话号码：7-8位数字： \d{7,8
            //分机号：一般都是3位数字： \d{3,}
            //这样连接起来就是验证电话的正则表达式了：/^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/		 
            var rex2 = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/ | /^(\d{7,8})(-(\d{3,}))?$/;
            // --正确格式为：XXXX-XXXXXXX，XXXX-XXXXXXXX，XXX-XXXXXXX，XXX-XXXXXXXX，XXXXXXX，XXXXXXXX。
            var rex3 = /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/; 
            if (rex.test(value) || rex3.test(value)) {

            } else {
                alert("电话格式不匹配，请重新输入！");
                document.getElementById(id).value = "";
            }
        }

    </script>
</head>
<body>
<h1 align="center" style="color:red ">书面承诺</h1>
    <p align="left" style = "text-indent: 2em;line-height:1.3;font-size:15px">本表业务数据经过我们严谨、详细测算并填报，我确定表格内容真实、准确、完整地反映了相关业务的实际情况，不存在任何虚假、错误、重复、遗漏、误导性信息，其编制和审核程序公司从严、合规管理要求。</p>
    <table>
    <tr><td>部门名称：</td><td><input id="Department" readonly="readonly"/></td></tr>
    <tr id="Sm"><td>文字说明：</td><td align="left"><select name="sel1" id="Select1" onchange="setVar(this);"></select></td></tr>
    </table>
    <textarea  id="txta" name="txta" style="width: 100%;height: 35%;"></textarea>
    <table>
        <tr id="TB"><td>填报人：</td><td><input id="TBR" style="border:0;border-bottom:1px solid #000; text-align:center" readonly="readonly"></td><td>联系电话：</td><td><input id="TBRPHONE" style="border:0;border-bottom:1px solid #000;text-align:center" onBlur="phone(document.getElementById('TBRPHONE').id,document.getElementById('TBRPHONE').value)"></td></tr>
        <tr id="SH"><td>审核人：</td><td><input id="SHR" style="border:0;border-bottom:1px solid #000; text-align:center" readonly="readonly"></td><td>联系电话：</td><td><input id="SHRPHONE" style="border:0;border-bottom:1px solid #000;text-align:center" onBlur="phone(document.getElementById('SHRPHONE').id,document.getElementById('SHRPHONE').value)"></td></tr>
    </table>
</body>
</html>
