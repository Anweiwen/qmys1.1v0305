﻿<%@ page language="C#" autoeventwireup="true" enablesessionstate="ReadOnly" inherits="ExcelData, App_Web_ofwlznev" validaterequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>基础模板填报</title>

    <link rel="stylesheet" type="text/css" href="../../Spread/jquery-easyui-1.5/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="../../Spread/jquery-easyui-1.5/themes/icon.css"/>
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript" src="../../Spread/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../../Spread/jquery-easyui-1.5/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../Spread/jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
    <script src="../../JS/json2.js" type="text/javascript"></script>
    <script src="../../JS/Main.js" type="text/javascript"></script>
    <script src="../../JS/MBDataLoader.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Mblb = "";

        //        function test1() {
        //            debugger;
        //            var txt = '{ "employees" : [{ "firstName":"John" , "lastName":"Doe" },'
        //            + '{ "firstName":"Anna" , "lastName":"Smith" },' + '{ "firstName":"Peter" , "lastName":"Jones" } ]}';

        //            var obj = eval("(" + txt + ")");
        //          
        //            alert(obj.employees[1].firstName);
        //        }
        function getchineseurl(_param) {
            return encodeURIComponent(_param);
        }
        function formatmonth(_month) {
            if (_month) {
                if (_month.length == 1) {
                    _month = "0" + _month;
                }
                return _month;
            }
            else {
                alert('月份为空！');
            }
        }
        function CreateDataGrdMb() {
            //添加DataGrdMb：
            if ("<%=MBLB%>" == "queryflow") {
                $('#DataGrdMb').datagrid({
                    autoRowHeight: false,
                    rownumbers: true,
                    fitColumns: true,
                    singleSelect: true,
                    striped: true,
                    pagination: true,
                    pageSize: 15,
                    pageList: [10, 15, 20, 25, 30, 35, 40],
                    loadMsg: '正在加载数据.....',
                    columns: [[
                    { field: 'DQJSDM', title: '当前角色代码', hidden: true },
					{ field: 'DQJSNAME', title: '当前角色', width: 80 },
                    { field: 'MBDM', title: '模板代码', hidden: true },
					{ field: 'MBMC', title: '模板名称', width: 120 },
                    { field: 'MBWJ', title: '模板文件', hidden: true },
                    { field: 'NEXTJSDM', title: '后续模板代码', hidden: true },
					{ field: 'NEXTMBMC', title: '后续模板名称', width: 120 },
					{ field: 'NEXTJSNAME', title: '后续角色', width: 60, align: 'center' },
                    { field: 'STATE', title: '状态', width: 60 }
				]],
                    onHeaderContextMenu: function (e, field) {
                        e.preventDefault();
                        if (!cmenu) {
                            createColumnMenu();
                        }
                        cmenu.menu('show', {
                            left: e.pageX,
                            top: e.pageY
                        });
                    },
                    onDblClickRow: function (rowIndex, rowData) {
                        OpenMb();
                    }
                });
            }
            //如果当前模块是： 批复预算基础数据维护
            //            else if ("<%=MBLB%>" == "respfj") {
            //                $('#DataGrdMb').datagrid({
            //                    checkOnSelect: true,
            //                    frozenColumns: [[{
            //                        field: 'ck',
            //                        checkbox: true
            //                    }]],
            //                    autoRowHeight: false,
            //                    rownumbers: true,
            //                    fitColumns: true,
            //                    singleSelect: false,
            //                    striped: true,
            //                    //                    pagination: true,
            //                    //                    pageSize: 15,
            //                    //                    pageList: [10, 15, 20, 25, 30, 35, 40],
            //                    loadMsg: '正在加载数据......',
            //                    toolbar: [{}, '-', {
            //                        text: '自动计算',
            //                        iconCls: 'icon-add',
            //                        handler: function () {
            //                            //自动计算上级模板：
            //                            AutoCal();
            //                        }
            //                    }, '-', {
            //                        text: '取消选择',
            //                        iconCls: 'icon-undo',
            //                        handler: function () {
            //                            $('#DataGrdMb').datagrid('unselectAll');
            //                        }
            //                    }],
            //                    columns: [[
            //                                { field: 'MBLX', title: '模板类型ID', hidden: true },
            //                                { field: 'MBDM', title: '模板代码', hidden: true },
            //            					{ field: 'XMMC', title: '模板类型', hidden: true },
            //            					{ field: 'MBMC', title: '模板名称', width: 120 },
            //            					{ field: 'MBWJ', title: '模板文件', hidden: true },
            //            					{ field: 'MBZQ', title: '周期代码', hidden: true },
            //                                { field: 'ZYMC', title: '部门名称', width: 60, align: 'center' },
            //            					{ field: 'MBZQMC', title: '模板周期', width: 60, align: 'center' },
            //                                { field: 'ZT', title: '计算状态', width: 60, align: 'center' },
            //                                { field: 'SFZLC', title: '是否流程', hidden: true },
            //                                { field: 'ISZLC', title: '流程信息', hidden: true },
            //                                { field: 'JSDM', title: '角色代码', hidden: true },
            //            					{ field: 'CJBM', title: '层级编码', hidden: true }
            //            				]],
            //                    onHeaderContextMenu: function (e, field) {
            //                        e.preventDefault();
            //                        if (!cmenu) {
            //                            createColumnMenu();
            //                        }
            //                        cmenu.menu('show', {
            //                            left: e.pageX,
            //                            top: e.pageY
            //                        });
            //                    },
            //                    onDblClickRow: function (rowIndex, rowData) {
            //                        OpenMb(rowData);
            //                    },
            //                    onLoadSuccess: function (data) {
            //                        if (data) {
            //                            $.each(data.rows, function (index, item) {
            //                                if (item.ZT == "已修改") {
            //                                    $('#DataGrdMb').datagrid('checkRow', index);
            //                                }
            //                            });
            //                        }
            //                    }
            //                });
            //            }
            else {
                $('#DataGrdMb').datagrid({
                    autoRowHeight: false,
                    rownumbers: true,
                    fitColumns: true,
                    singleSelect: true,
                    striped: true,
                    pagination: true,
                    pageSize: 15,
                    pageList: [10, 15, 20, 25, 30, 35, 40],
                    loadMsg: '正在加载数据.....',
                    columns: [[
                    { field: 'MBLX', title: '模板类型ID', hidden: true },
                    { field: 'MBDM', title: '模板代码', hidden: true },
					{ field: 'XMMC', title: '模板类型', hidden: true },
					{ field: 'MBMC', title: '模板名称', width: 120 },
					{ field: 'MBWJ', title: '模板文件', hidden: true },
					{ field: 'MBZQ', title: '周期代码', hidden: true },
                    { field: 'ZYMC', title: '部门名称', width: 60, align: 'center' },
					{ field: 'MBZQMC', title: '模板周期', width: 60, align: 'center' },
                    { field: 'SFZLC', title: '是否流程', hidden: true },
                    { field: 'ISZLC', title: '流程信息', hidden: true },
                    { field: 'JSDM', title: '角色代码', hidden: true },
					{ field: 'CJBM', title: '层级编码', hidden: true }
				]],
                    onHeaderContextMenu: function (e, field) {
                        e.preventDefault();
                        if (!cmenu) {
                            createColumnMenu();
                        }
                        cmenu.menu('show', {
                            left: e.pageX,
                            top: e.pageY
                        });
                    },
                    onDblClickRow: function (rowIndex, rowData) {
                        OpenMb();
                    }
                });
            }
        }

        function autoclaclick() {
            //            var chkaucal = document.getElementById('chkautocal');
            //            CreateDataGrdMb();
            //如果是选中了自动计算功能
            //            if (chkaucal.checked == true) {

            //                alert('true');
            //            } else {
            //                alert('false');
            //            }
        }

        function datagridloading(_datagrid) {
            var opts = _datagrid.datagrid("options");
            var wrap = $.data(_datagrid[0], "datagrid").panel;
            if (opts.loadMsg) {
                $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: wrap.width(), height: wrap.height() }).appendTo(wrap);
                $("<div class=\"datagrid-mask-msg\"></div>").html(opts.loadMsg).appendTo(wrap).css({ display: "block", left: (wrap.width() - $("div.datagrid-mask-msg", wrap).outerWidth()) / 2, top: (wrap.height() - $("div.datagrid-mask-msg", wrap).outerHeight()) / 2 });
            }
        }
        function datagridloaded(_datagrid) {
            _datagrid.datagrid("getPager").pagination("loaded");
            var wrap = $.data(_datagrid[0], "datagrid").panel;
            wrap.find("div.datagrid-mask-msg").remove();
            wrap.find("div.datagrid-mask").remove();
        }

        //根据登录的年份，加载年下拉框：
        function InitYear() {
            var yy = parseInt('<%=YY%>');
            var s = "";
            for (var i = yy - 4; i < yy + 5; i++) {
                if (i == yy) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelYear").append(s);
        }

        //根据当前月份，加载月下拉框：
        function InitMonth() {
            var mm = parseInt('<%=MM%>');
            var s = "";
            for (var i = 1; i <= 12; i++) {
                if (i == mm) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelMonth").append(s);
        }

        //根据当前月份，加载季度下拉框：
        function InitQuarter() {
            var mm1 = parseInt('<%=MM%>');
            var q = Math.ceil(mm1 / 3);
            var s = "";
            for (var i = 1; i <= 4; i++) {
                if (i == q) {
                    s = s + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    s = s + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#SelQuarter").append(s);
        }

        //打开复制模板界面  by liguosheng
        function openDialog() {
            refreshCopy();
            var str = "";
            var faData = "";
            var mbData = "";
            var FaId = "";

            //加载年
            var yy = parseInt($('#SelYear').val());
            var ys = "";
            for (var i = yy - 4; i < yy + 5; i++) {
                if (i == yy) {
                    ys = ys + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    ys = ys + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#year").empty();
            $("#year").append(ys);

            //加载月
            var mm = parseInt($('#SelMonth').val());
            var ms = "";
            for (var i = 1; i <= 12; i++) {
                if (i == mm) {
                    ms = ms + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    ms = ms + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#month").empty();
            $("#month").append(ms);

            //加载季
            var mm1 = parseInt($('#SelMonth').val());
            var q = Math.ceil(mm1 / 3);
            var qs = "";
            for (var i = 1; i <= 4; i++) {
                if (i == q) {
                    qs = qs + "<option value='" + i + "' selected='selected'>" + i + "</option>";
                } else {
                    qs = qs + "<option value='" + i + "'>" + i + "</option>";
                }
            }
            $("#quarter").empty();
            $("#quarter").append(qs);

            //调用张端的初始化年月和部门的方法，装载部门
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "InitBm",
                    userid: '<%=USERID%>'
                },
                async: false,
                cache: false,
                success: function (result) {
                    $("#dep").empty();
                    $("#dep").append(result);
                    reSetFa(Mblb, $('#SelHszx').val(), $('#SelFalb').val(), $('#SelFa').val(), $('#SelYear').val(), $('#SelMonth').val(), $('#SelQuarter').val(), $('#SelDep').val()); //加载要复制的方案列表
                },
                error: function () {
                    alert("装入作业目录错误!");
                }
            });
        }

        //重置要复制的方案列表
        function reSetFa(mb, selhszx, selfa, fadm, yy, nn, jd, zydm) {
            var faData = "";
            var mbData = "";
            debugger;
            //根据所选择的部门和年月日，加载要复制的方案！
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=LoadFa&Mblb="' + mb + '"&HSZXDM="' + selhszx + '"&SelFalb="' + selfa + '"&jhfadm="' + fadm + '"&YY="' + yy + '"&NN="' + nn + '"&JD="' + jd + '"&ZYDM="' + zydm + '"',
                async: false,
                cache: false,
                success: function (result) {
                    if (result == "0") {
                        $("#selectfa").empty();
                        $('#dg').datagrid('loadData', { rows: [] });
                        prompt("没有其他需要复制的方案！");
                        return;
                    } else {
                        $("#selectfa").empty();
                        $("#selectfa").append(result);
                        reSetMb(mb, selfa, fadm, yy, nn, jd, zydm); //重置模板复制的方法
                        $('#mbsel').window('open');
                    }
                },
                error: function (req, info, obj) {
                    BmShowWinError('错误!', req.responseText);
                }
            });

        }

        //重置模板复制列表
        function reSetMb(mb, selfa, fadm, yy, nn, jd, zydm) {
            //加载模板复制的显示列表
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=CopyMb&Mblb="' + mb + '"&SelFalb="' + selfa + '"&jhfadm="' + fadm + '"&YY="' + yy + '"&NN="' + nn + '"&JD="' + jd + '"&ZYDM="' + zydm + '"',
                async: false,
                cache: false,
                success: function (result) {
                    $('#dg').datagrid('loadData', { rows: [] });
                    $('#dg').datagrid('loadData', eval("(" + result + ")"));
                    $('#mbsel').window('open');
                },
                error: function (req, info, obj) {
                    BmShowWinError('错误!', req.responseText);
                }
            });
        }

        //by liguosheng
        function saveMb() {
            if ($('#SelFa').val() == null) {
                prompt("当前时间没有生成方案，无法复制！");
                return;
            }
            //var rows = $('#dg').datagrid('getSelected');
            var res = "";
            var rows = $('#dg').datagrid('getSelections');

            //var fzfadm = $('#selectfa').combobox('getValue');
            var fzfadm = $('#selectfa').val();
            if (rows == "") {
                $.messager.show({
                    title: '提示',
                    msg: '请选择要复制的模板！！！！！',
                    timeout: 3000,
                    showType: 'slide'
                });
                return;
            }

            var mbdm = "";
            var filename = "";
            var newfilename = "";
            for (var i = 0; i < rows.length; i++) {
                mbdm += rows[i].MBDM;
                filename += getsavefilename(fzfadm, rows[i].MBWJ) + "~" + rows[i].MBMC;
                newfilename += "jhfadm" + "~" + $('#SelFa').val() + "~" + "Mb" + "~" + rows[i].MBWJ;
                if (i < rows.length - 1) {
                    mbdm += ",";
                    filename += ",";
                    newfilename += ",";
                }
            }

            $.messager.confirm('提示信息', '是否将被复制方案名称的下列模板数据复制到当前方案名中，此复制将覆盖当前方案模板数据，是否继续？', function (r) {
                if (r) {
                    $.ajax({
                        url: 'ExcelData.ashx?action=SaveCopyMb',
                        type: 'POST',
                        /*
                        请求ashx文件的时候 要把contentType去掉，还有就是
                        data 格式为 {”key”,”value”};切记 不要再 大括号外面加双引号，
                        这样就会在ashx页面取不到数据而失败
                        */
                        //contentType: 'application/json; charset=utf8',
                        data: {
                            "jhfadm": $('#SelFa').val(),
                            "mbdm": mbdm,
                            "fzfadm": fzfadm,
                            "filename": filename,
                            "newfilename": newfilename
                        },
                        cache: false,
                        success: function (result) {
                            res = result.split('|');
                            if (res[1] == "") {
                                prompt("复制成功！");
                            } else if (res[0] == "1" && res[1] != "") {
                                prompt("【" + res[1] + "】" + ",要复制方案的模板文件不存在，无法复制！其余模板复制成功！");
                            } else if (res[1] != "") {
                                prompt("【" + res[1] + "】" + ",要复制方案的模板文件不存在，无法复制！");
                            }
                        },
                        error: function (req, info, obj) {
                            BmShowWinError('错误!', req.responseText);
                        }
                    });
                }
            })
        }

        function InitFa() {
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "GetFa",
                    hszxdm: $("#SelHszx").val(),
                    yy: $("#SelYear").val(),
                    nn: $("#SelMonth").val(),
                    jd: $("#SelQuarter").val(),
                    fabs: $("#SelFalb").val(),
                    mblb: Mblb
                },
                async: true,
                cache: false,
                success: function (result) {
                    $("#SelFa").empty();
                    $("#SelFa").append(result);
                    $("#SelFa2").empty();
                    $("#SelFa2 ").append(result);
                    ClearMb();
                    //加载当前方案的截止时间和剩余时间
                    GetFASYTS();
                },
                error: function (req) {
                    BmShowWinError('装入方案错误!', req.responseText);
                }
            });
        }

        //装入部门：
        function InitBm() {
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "InitBm",
                    userid: '<%=USERID%>'
                },
                async: true,
                cache: false,
                success: function (result) {
                    $("#SelDep").empty();
                    $("#SelDep").append(result);
                },
                error: function () {
                    alert("装入作业目录错误!");
                }
            });
        }

        //        function InitZyml(){
        //         $.ajax({
        //                type: 'get',
        //                url: 'ExcelData.ashx',
        //                data:{
        //                action:"InitZyml",
        //                hszxdm:$("#SelHszx").val()
        //                },
        //                async: true,
        //                cache:false,
        //                success: function (result) {
        //                 $("#TreeZyml").tree('loadData',eval(result));
        //                },
        //                error: function () {
        //                    alert("装入作业目录错误!");
        //                }
        //            });
        //        }

        function getisexe(_val) {
            var isd = "-1";
            if (_val == "0") {
                isd = "-1";
            } else if (_val == "1") {
                isd = "0,1";
            }
            return isd;
        }

        //装入模板：
        function InitMb() {
            datagridloading($("#DataGrdMb"));
            var isd = getisexe($("#SelExe").val());
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "InitMb",
                    isexe: isd,
                    jhfadm: $("#SelFa").val(),
                    mblb: Mblb,
                    mbmc: $("#TxtMbmc").val(),
                    zydm: $("#SelDep").val()
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result) {
                        //                        $("#DataGrdMb").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', eval(result));
                        if ("<%=MBLB%>" == "respfj") {
                            $("#DataGrdMb").datagrid('loadData', eval(result));
                        } else {
                            $("#DataGrdMb").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', eval(result));
                        }
                    }
                    datagridloaded($("#DataGrdMb"));
                },
                error: function (req) {
                    datagridloaded($("#DataGrdMb"));
                    BmShowWinError('装入模板错误!', req.responseText);
                }
            });
        }

        //清空模板:
        function ClearMb() {
            $("#DataGrdMb").datagrid('loadData', { total: 0, rows: [] });
        }

        function InitYsyf() {
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "InitYsyf"
                },
                async: true,
                cache: false,
                success: function (result) {
                    $("#SelYsyf").empty();
                    $("#SelYsyf").append(result);
                },
                error: function (req) {
                    BmShowWinError('装入预算月份错误!', req.responseText);
                }
            });
        }

        function refresh() {
            var faidx = $("#SelFalb").val();
            switch (parseInt(faidx)) {
                //月：                                                
                case 1:
                    $("#DivMonth").css("display", "inline");
                    $("#DivQuarter").css("display", "none");
                    break;
                //季：                                                
                case 2:
                    $("#DivMonth").css("display", "none");
                    $("#DivQuarter").css("display", "inline");
                    break;
                //年：                                                
                case 3:
                    $("#DivMonth").css("display", "none");
                    $("#DivQuarter").css("display", "none");
                    break;
                //其他：                                                
                case 4:
                    $("#DivMonth").css("display", "inline");
                    $("#DivQuarter").css("display", "none");
                    break;
            }
        }

        //提示未完成的模板：
        function UndoMb(_jhfadm, _mbdm, _mblb, _isexe, _url, hszxdm, _mbmc) {
            debugger;
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=UndoMb',
                data: {
                    jhfadm: _jhfadm,
                    mbdm: _mbdm,
                    mblb: _mblb,
                    isexe: _isexe,
                    hszxdm: hszxdm
                },
                async: true,
                cache: false,
                success: function (result) {
                    debugger;
                    if (result != '' && result != undefined) {
                        msgbox('以下部门尚未审核，请督促报送', result, 'OpenPOBrowser(\'' + _mbdm + '\',\'' + _mbmc + '\',\'' + _url + '\')', null, 0, null, '确定', '', 1);
                        //                        alert(result);
                    } else {
                        OpenPOBrowser(_mbdm, _mbmc, _url);
                    }
                },
                error: function (req) {
                    BmShowWinError('未完成模板提示错误!', req.responseText);
                    OpenPOBrowser(_mbdm, _mbmc, _url);
                }
            });
        }

        //打开POBrowser：
        function OpenPOBrowser(_mbdm, _mbmc, _url) {
            $("#HidMBDM").val(_mbdm);
            OpenWebExcel(_mbdm, _mbmc, _url, $("#SelFa").val(), $("#SelFa2").val());
        }

        //打开SpreadJS网页
        function OpenWebExcel(ID, MC, paramStr, FADM, FADM2) {
            if ($('#tt').tabs('exists', MC)) {
                $('#tt').tabs('select', MC);
                //如果Title为空，表示打开的是一般的菜单页，否则打开的是数据填报页
                if (FADM != "" && FADM != undefined && FADM != null) {
                    var NewFileName = "Jhfadm~" + FADM + "~Jhfadm2~" + FADM2 + "~Mb~" + ID;
                    var TabFrame = $('#tt').tabs('getTab', MC).find("iframe");
                    if (TabFrame.length > 0) {
                        var OldFileName = TabFrame.contents().find("#HidFAMB").val();
                        if (NewFileName != OldFileName) {
                            var path = window.document.location.pathname.substring(1);
                            var Len = path.indexOf("/");
                            var root = "/" + path.substring(0, Len + 1);
                            root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random();
                            var content = '<iframe scrolling="auto" id="' + ID + '"  frameborder="0"  src="' + root + '" style="width:100%;height:100%;"></iframe>';
                            var tab = $('#tt').tabs('getTab', MC);
                            $('#tt').tabs('update', {
                                tab: tab,
                                options: {
                                    content: content
                                }
                            });
                        }
                    }
                }
            }
            else {
                var path = window.document.location.pathname.substring(1);
                var Len = path.indexOf("/");
                var root = "/" + path.substring(0, Len + 1);
                root = root + "Excels/Data/EXCELMB.aspx?" + paramStr + "&rand=" + Math.random(); ;
                var content = '<iframe scrolling="auto" id="' + ID + '"  frameborder="0"  src="' + root + '" style="width:100%;height:100%;"></iframe>';
                $('#tt').tabs('add', {
                    title: MC,
                    content: content,
                    closable: true
                });
            }
        }

        //打开模板：
        function OpenMb() {
            var selected = $("#DataGrdMb").datagrid('getSelected');
            if (selected) {
                //只有待执行的时候，才判断关联下级模板是否完成
                if ($("#SelExe").val() == "0") {
                    //只有走流程的才判断关联下级模板是否完成
                    if (selected.SFZLC == 1) {
                        //var MBStr = ExcelData.GetDCLMB(selected.MBDM, selected.JSDM, selected.HSZXDM, selected.LCDM, $('#SelFa').val()).value;
                        var MBStr = ExcelData.GetDCLMB($('#SelFa').val(), selected.MBDM, selected.JSDM).value;
                        //                        MBStr = "11";
                        if (MBStr != "" && MBStr != null) {
                            //                            debugger;
                            msgbox('以下部门尚未审核,请督促报送', MBStr, 'GetOpenMBURL()', null, 0, null, '确定', '否', 1);
                            //                            if (confirm(MBStr + "\r\n\r\n 您确定要打开吗？")) {
                            //                                GetOpenMBURL(selected);
                            //                            }
                        }
                        else {
                            GetOpenMBURL(selected);
                        }
                    }
                    else {
                        GetOpenMBURL(selected);
                    }
                }
                else {
                    GetOpenMBURL(selected);
                }
            }
            else {
                alert("请先选择模板！");
            }
        }

        //获取打开模板的URL（张哥之前OpenMB方法里面的代码）
        function GetOpenMBURL(selected) {
            if (null == selected) { selected = $("#DataGrdMb").datagrid('getSelected'); }
            var url = "MBSZ=ExcelData&MBMC=" + getchineseurl(selected.MBMC)
                 + "&MBDM=" + selected.MBDM + "&MBWJ=" + getchineseurl(selected.MBWJ) + "&YY=" + $("#SelYear").val()
                 + "&NN=" + formatmonth($("#SelMonth").val()) + "&JD=" + $("#SelQuarter").val()
                 + "&FADM=" + $("#SelFa").val() + "&FADM2=" + $("#SelFa2").val()
                 + "&FAMC=" + getchineseurl($("#SelFa").find("option:selected").text())
                 + "&YSYF=" + formatmonth($("#SelYsyf").val()) + "&USERDM=<%=USERID%>"
                 + "&USERNAME=" + getchineseurl("<%=USERNAME%>") + "&FALB=" + $("#SelFalb").val()
                 + "&MBLB=<%=MBLB%>" + "&ISEXE=" + getisexe($("#SelExe").val()) + "&MBLX=" + selected.MBLX
                 + "&HSZXDM=" + $("#SelHszx").val() + "&SFZLC=" + selected.SFZLC + "&LOGINYY=<%=YY%>"
                 + "&JSDM=" + selected.JSDM + "&LCDM=" + selected.LCDM + "&ACTIONRIGHT=" + selected.ACTIONRIGHT;

            //如果不是ie浏览器：
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            if ("<%=MBLB %>" == "baseys" || "<%=MBLB %>" == "resoys") {
                UndoMb($("#SelFa").val(), selected.MBDM, "<%=MBLB%>", getisexe($("#SelExe").val()), url, $("#SelHszx").val(), selected.MBMC);
            }
            else {
                OpenPOBrowser(selected.MBDM, selected.MBMC, url);
            }
        }

        function getWindowHeight() {
            return $(window).height();
        }

        function getWindowWidth() {
            return $(window).width();
        }

        function windowResize() {
            var width = getWindowWidth();
            var height = getWindowHeight();

            if (width != $('form#form1').width()) {
                $('form#form1').width(width);
            }

            if (height != $('form#form1').height()) {
                $('form#form1').height(height);
            }

            $('form#form1').layout();
        }

        function InitHszx(_asy) {
            if (_asy === undefined)
                _asy = true;
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=InitHszx',
                data: {
                    yy: '<%=YY%>',
                    userid: '<%=USERID%>'
                },
                async: _asy,
                cache: false,
                success: function (result) {
                    $("#SelHszx").empty();
                    $("#SelHszx").append(result);
                },
                error: function (req) {
                    BmShowWinError('装入核算中心错误!', req.responseText);
                }
            });
        }

        /// <summary>装入是否审核、填报信息</summary>
        function InitSelExe() {
            var selexe = $("#SelExe");
            selexe.empty();
            //填报：
            if ('<%=MBLB %>' == 'basesb' || '<%=MBLB %>' == 'resosb' || '<%=MBLB %>' == 'respfj') {
                selexe.append('<option value="0" selected="selected">未填报</option>');
                selexe.append('<option value="1">已填报</option>');
            }
            //审核：
            else if ('<%=MBLB %>' == 'basesp' || '<%=MBLB %>' == 'resosp' || '<%=MBLB %>' == 'respsp') {
                selexe.append('<option value="0" selected="selected">未审核</option>');
                selexe.append('<option value="1">已审核</option>');
            }
            //生成：
            else if ('<%=MBLB %>' == 'baseys' || '<%=MBLB %>' == 'resoys' || '<%=MBLB %>' == 'respys') {
                selexe.append('<option value="0" selected="selected">未生成</option>');
                selexe.append('<option value="1">已生成</option>');
            }
            //完成：
            else {
                selexe.append('<option value="0" selected="selected">未完成</option>');
                selexe.append('<option value="1">已完成</option>');
            }

        }

        function InitFalb(_asy) {
            if (_asy === undefined)
                _asy = true;
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=InitFalb',
                async: _asy,
                cache: false,
                success: function (result) {
                    $("#SelFalb").empty();
                    $("#SelFalb").append(result);
                    $("#SelFalb").val("3");
                    refresh();
                    //                    InitFa();
                },
                error: function (req, info, obj) {
                    BmShowWinError('装入方案类别错误!', req.responseText);
                }
            });
        }

        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }

                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    loading: true,
                    showRefresh: false,
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }

        //获取当前选中模板所有的处理意见
        function getMBCLYJ() {
            var row = $('#DataGrdMb').datagrid('getSelected');
            if (row == null) {
                prompt("请先选择模板！");
                return;
            } else {
                $.ajax({
                    type: 'get',
                    url: 'ExcelData.ashx?action=GetMBCLYJ&jhfadm=' + $('#SelFa').val() + '&mbdm=' + row.MBDM + '',
                    async: false,
                    cache: false,
                    success: function (result) {
                        result = result.replace(/(\r)+|(\n)+|(\r\n)+/g, "<br>"); //替换json中的回车换行符
                        var Data = eval("(" + result + ")");
                        $('#yj').datagrid('loadData', Data);
                    },
                    error: function (req, info, obj) {
                        BmShowWinError('错误!', req.responseText);
                    }
                });
            }
            $('#grid').window('open');
        }


        function openSMCN(rowdata) {
            if (rowdata == null) { prompt("请先选择一行记录！"); return; }
            var con = OpenSmqr();
            var arr = rowdata.SPYJ.split('|');
            if (arr == "") {
                con = upData(con, "id=\"Department\"", "id=\"Department\" value=\"" + "" + "\"");
                con = upData(con, "</textarea>", "" + "</textarea>");
                con = upData(con, "id=\"TBR\"", "id=\"TBR\" value=\"" + "" + "\"");
                con = upData(con, "id=\"TBRPHONE\"", "id=\"TBRPHONE\" value=\"" + "" + "\"");
                con = upData(con, "id=\"SHR\"", "id=\"SHR\" value=\"" + "" + "\"");
                con = upData(con, "id=\"SHRPHONE\"", "id=\"SHRPHONE\" value=\"" + "" + "\"");
            } else {
                arr[1] = arr[1].replace(/<br>/g, "\n");
                con = upData(con, "id=\"Department\"", "id=\"Department\" value=\"" + arr[0] + "\"");
                con = upData(con, "</textarea>", arr[1] + "</textarea>");
                con = upData(con, "id=\"TBR\"", "id=\"TBR\" value=\"" + arr[2] + "\"");
                con = upData(con, "id=\"TBSJ\"", "id=\"TBSJ\" value=\"" + arr[3] + "\"");
                con = upData(con, "id=\"TBRPHONE\"", "id=\"TBRPHONE\" value=\"" + arr[4] + "\" readonly=\"readonly\"");
                con = upData(con, "id=\"SHR\"", "id=\"SHR\" value=\"" + arr[5] + "\"");
                con = upData(con, "id=\"SHSJ\"", "id=\"SHSJ\" value=\"" + arr[6] + "\"");
                con = upData(con, "id=\"SHRPHONE\"", "id=\"SHRPHONE\" value=\"" + arr[7] + "\" readonly=\"readonly\"");
            }

            if (arr[2] == "") { con = upData(con, "id=\"TB\"", "id=\"TB\" style=\"display:none;\""); }
            if (arr[5] == "") { con = upData(con, "id=\"SH\"", "id=\"SH\" style=\"display:none;\""); }
            $('#SMQR').dialog({
                content: con
            });
            $('#SMQR').window('open');

        }


        function OpenSmqr() {
            var content = "";
            $.ajax({
                url: 'SMCN.html', //这里是静态页的地址
                type: 'GET', //静态页用get方法，否则服务器会抛出405错误
                async: false, //同步
                cache: false,
                dataType: 'html',
                success: function (data) {
                    //                    if ("<%=MBLB%>" == "basesb" || "<%=MBLB%>" == "resosb" || "<%=MBLB%>" == "baseys" || "<%=MBLB%>" == "resoys") {//当前菜单属于预算编制填报或与预算分解分解填报，预算编制-生成上报预算和预算分解-预算分解报表生成
                    //                        data = upData(data, "id=\"SH\"", "id=\"SH\" style=\"display:none;\"");
                    //                    }
                    //                    if ("<%=MBLB%>" == "basesp" || "<%=MBLB%>" == "resosp") {//当前菜单属于预算编制报表审批或预算分解报表审批
                    //                        data = upData(data, "id=\"TB\"", "id=\"TB\" style=\"display:none;\"");
                    //                    }
                    data = upData(data, "id=\"txta\"", "id=\"txta\" readonly=\"readonly\"");
                    data = upData(data, "id=\"Sm\"", "id=\"Sm\" style=\"display:none;\"");
                    content = data;
                }
            })
            return content;
        }

        function upData(_data, element, text) {
            _data = _data.replace(element, text);
            return _data;
        }

        function refreshCopy() {
            var faidx = $("#SelFalb").val();
            switch (parseInt(faidx)) {
                //月：                                                 
                case 1:
                    $("#CopyMonth").css("display", "inline");
                    $("#CopyQuarter").css("display", "none");
                    break;
                //季：                                                 
                case 2:
                    $("#CopyMonth").css("display", "none");
                    $("#CopyQuarter").css("display", "inline");
                    break;
                //年：                                                 
                case 3:
                    $("#CopyMonth").css("display", "none");
                    $("#CopyQuarter").css("display", "none");
                    break;
                //其他：                                                 
                case 4:
                    $("#CopyMonth").css("display", "inline");
                    $("#CopyQuarter").css("display", "none");
                    break;
            }
        }

        function prompt(rtn, wid, hg) {
            $.messager.show({
                title: '温馨提示',
                msg: rtn,
                width: wid,
                height: hg,
                timeout: 1000,
                showType: 'slide'
            });
        }

        //获取当前方案的截止时间以及剩余时间
        function GetFASYTS() {
            var Flag = false;
            Mblb = '<%=MBLB%>';
            //只有预算编制和预算分解的相关菜单才显示截止时间和剩余时间
            if (Mblb == "basesb" || Mblb == "basesp" || Mblb == "baseys" || Mblb == "resosb" || Mblb == "resosp" || Mblb == "resoys") {
                Flag = true;
            }
            //只有预算编制和预算分解的待执行状态才显示截止时间和剩余时间
            if ($("#SelExe").val() == "0" && Flag == true) {
                if (ExcelData.IsLCOver($("#SelFa").val()).value == true) {
                    $("#JZ")[0].innerHTML = "";
                }
                else {
                    $.ajax({
                        type: 'get',
                        url: 'FASQHandler.ashx',
                        data: {
                            JHFADM: $("#SelFa").val(),
                            USERDM: '<%=USERID%>',
                            HSZXDM: '<%=HSZXDM%>'
                        },
                        async: true,
                        cache: false,
                        success: function (result) {
                            $("#JZ")[0].innerHTML = result;
                        },
                        error: function (req) {
                            $("#JZ")[0].innerHTML = "";
                        }
                    });
                }
            }
            else {
                $("#JZ")[0].innerHTML = "";
            }
        }

        function geturlpath() {
            var href = document.location.href;
            var h = href.split("/");
            href = "";
            for (var i = 0; i < h.length - 1; i++) {
                href += h[i] + "/";
            }
            return href;
        }

        function getDownLoad() {
            datagridloading($("#DownLoadMb"));
            $('#DownLoadMb').datagrid('unselectAll');
            var isd = getisexe($("#SelExe").val());
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "GetDownLoadsList",
                    isexe: isd,
                    jhfadm: $("#SelFa").val(),
                    mblb: Mblb,
                    mbmc: $("#TxtMbmc").val(),
                    zydm: $("#SelDep").val()
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result) {
                        $("#DownLoadMb").datagrid('loadData', eval("(" + result + ")"));
                    }
                    datagridloaded($("#DownLoadMb"));
                },
                error: function (req) {
                    datagridloaded($("#DownLoadMb"));
                    BmShowWinError('装入模板错误!', req.responseText);
                }
            });
            $('#DownDialog').window('open');
        }

        /// <summary>计算单个模板</summary>
        /// <param name="_arymbobj" type="Object">Json</param>
        function CalMbAry(_arymbobj) {
            if (_arymbobj != undefined) {
                //模板数组：
                dialogshow('开始自动计算模板......');
                for (var i = 0; i < _arymbobj.mbs.length; i++) {
                    try {
                    } catch (e) {
                    }
                    //由于改动量较大暂时不放在后台计算：
                    //                    $.ajax({
                    //                        type: 'post',
                    //                        url: 'ExcelData.ashx?action=CalMbAry',
                    //                        data: {
                    //                            jhfadm: $("#SelFa").val(),
                    //                            mbdm: _arymbobj.mbs[i].mbdm
                    //                        },
                    //                        async: true,
                    //                        cache: false,
                    //                        success: function (result) {
                    //                            //result为模板代码：
                    //                            if (result != undefined) {
                    //                                var bmbs = false;
                    //                                for (j = 0; j < _arymbobj.mbs.length; j++) {
                    //                                    if (_arymbobj.mbs[j].mbdm == result) {
                    //                                        _arymbobj.mbs[j].isexed = true;
                    //                                    }
                    //                                }
                    //                            }
                    //                        },
                    //                        error: function (req) {
                    //                            BmShowWinError('计算模板错误!', req.responseText);
                    //                        }
                    //                    });
                }

            }
        }

        /// <summary>批复预算自动计算功能</summary>
        function AutoCal() {
            var mbdm = "";
            var mbwj = "";
            var selrows = $("#DataGrdMb").datagrid('getSelections');
            if (selrows.length == 0) {
                prompt("请选择模板需要自动计算的模板！");
                return;
                //如果选择了自动计算的模板：
            } else {
                for (var i = 0; i < selrows.length; i++) {
                    if (i == selrows.length - 1) {
                        mbdm += selrows[i].MBDM;
                        mbwj += selrows[i].MBWJ;
                    } else {
                        mbdm += selrows[i].MBDM + ",";
                        mbwj += selrows[i].MBWJ + ",";
                    }
                }
                $.ajax({
                    type: 'post',
                    url: 'ExcelData.ashx?action=AutoCal',
                    data: {
                        jhfadm: $("#SelFa").val(),
                        mbdm: mbdm,
                        mbwj: mbwj,
                        userid: '<%=USERID %>'
                    },
                    async: true,
                    cache: false,
                    success: function (result) {
                        var aryresobj = eval('(' + result + ')');
                        //计算模板：
                        CalMbAry(aryresobj);
                    },
                    error: function (req) {
                        datagridloaded($("#DataGrdMb"));
                        BmShowWinError('自动计算错误!', req.responseText);
                    }
                });
            }
        }

        function DownLoadMbs() {
            var files = '';
            var zipName = '模板批量下载压缩包.zip';
            var rows = $('#DownLoadMb').datagrid('getSelections');
            if (rows.length == 0) {
                prompt("请选择模板文件！");
                return;
            }
            for (var i = 0; i < rows.length; i++) {
                files += rows[i].MBWJ + "," + rows[i].MBMC;
                if (i < rows.length - 1) {
                    files += '|';
                }
            }


            $('#jhfadm').attr('value', $("#SelFa").val());
            $('#excelFiles').attr('value', files);
            $('#zipFileName').attr('value', zipName);
            $('#A4').submit();
            files = '';
        }


        $(function () {
            if ("<%=logout%>" == "true") {
                window.parent.location = "<%=Request.ApplicationPath%>/Login.aspx?rnd=" + Math.random();
            }
            Mblb = '<%=MBLB%>';
            //如果是预算分解类型则显示对比方案：
            if (Mblb == 'otheryd' || Mblb == 'otherdy') {
                $("#DivFa2").css('display', 'inline');
            }

            windowResize();
            $(window).resize(function () {
                windowResize();
            });

            $("#BtnOpenMb").linkbutton('enable');
            InitSelExe();
            InitHszx(false);
            InitFalb(false);
            InitYear();
            InitMonth();
            InitQuarter();
            InitYsyf();
            InitFa();
            InitBm();

            $("#SelFalb").change(function () {
                refresh();
                InitFa();
            });

            $("#SelFa").change(function () {
                ClearMb();
                //获取当前方案的截止时间和剩余时间
                GetFASYTS();
            });

            $("#SelHszx").change(function () {
                InitFa();
            });

            $("#SelMonth").change(function () {
                InitFa();
            });

            $("#SelYear").change(function () {
                InitFa();
            });

            $("#SelExe").change(function () {
                ClearMb();
                //加载当前方案的截止时间和剩余时间
                GetFASYTS();
            });


            $("#year").change(function () {
                //reSetFa(Mblb, $('#SelFalb').val(), $('#SelFa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
                reSetFa(Mblb, $('#SelHszx').val(), $('#SelFalb').val(), $('#SelFa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
            });

            $("#month").change(function () {
                reSetFa(Mblb, $('#SelHszx').val(), $('#SelFalb').val(), $('#SelFa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
            });

            $("#quarter").change(function () {
                reSetFa(Mblb, $('#SelHszx').val(), $('#SelFalb').val(), $('#SelFa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
            });

            $("#selectfa").change(function () {
                reSetMb(Mblb, $('#SelFalb').val(), $('#selectfa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
            });

            $("#dep").change(function () {
                if ($('#selectfa').val() == null) {

                } else {
                    reSetFa(Mblb, $('#SelHszx').val(), $('#SelFalb').val(), $('#SelFa').val(), $('#year').val(), $('#month').val(), $('#quarter').val(), $('#dep').val());
                }

            });

            //查询检索的时候添加回车事件：
            $('#TxtMbmc').textbox('textbox').keydown(function (e) {
                if (e.keyCode == 13) {
                    InitMb();
                }
            });

            CreateDataGrdMb();


            //复制模板数据界面
            $('#dg').datagrid({
                idField: 'MBDM',
                width: function () { return document.body.clientWidth },
                nowrap: true,
                rownumbers: true,
                fit: true,
                animate: false,
                fitColumns: true,
                collapsible: true,
                lines: true,
                maximizable: true,
                maximized: true,
                singleSelect: false,
                frozenColumns: [[{ field: 'ck', checkbox: true}]],
                rownumbers: true,
                columns: [[
                { field: 'MBDM', title: '模板代码', width: 120, align: 'center', hidden: true },
                { field: 'MBMC', title: '模板名称', width: 120, align: 'left' },
                { field: 'MBWJ', title: '模板文件', width: 70, align: 'center' },
				{ field: 'SFCZ', title: '是否存在', width: 70, align: 'center',
				    formatter: function (value) {
				        if (value == "模板不存在") {
				            return " <font color='red'>" + value + "</font>";
				        } else {
				            return value;
				        }
				    }
				}
			]]
            });



            //批量下载的datagrid，支持多选
            $('#DownLoadMb').datagrid({
                idField: 'MBDM',
                checkOnSelect: true,
                frozenColumns: [[{
                    field: 'ck',
                    checkbox: true
                }]],
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                singleSelect: false,
                striped: true,
                toolbar: [{}, '-', {
                    text: '批量下载',
                    iconCls: 'icon-add',
                    handler: function () {
                        DownLoadMbs();
                    }
                }, '-', {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function () {
                        $('#DownLoadMb').datagrid('unselectAll');
                    }
                }],
                loadMsg: '正在加载数据.....',
                columns: [[
                    { field: 'MBDM', title: '模板代码', width: 120, align: 'center', hidden: true },
                    { field: 'XMMC', title: '模板类型', width: 120, align: 'center' },
                    { field: 'MBMC', title: '模板名称', width: 120, align: 'center' },
                    { field: 'MBWJ', title: '模板文件', width: 70, align: 'center' },
					{ field: 'SFCZ', title: '是否存在', width: 70, align: 'center',
					    formatter: function (value) {
					        if (value == "模板不存在") {
					            return " <font color='red'>" + value + "</font>";
					        } else {
					            return value;
					        }
					    }
					}
				]],
                onHeaderContextMenu: function (e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }
            });


            var pager = $('#DataGrdMb').datagrid('getPager');

            pager.pagination({
                loading: true,
                showRefresh: false
            });

            $('#yj').datagrid({
                fitColumns: true,
                singleSelect: true,
                pagePosition: 'bottom',
                pageNumber: 1,
                pageSize: 10,
                pageList: [10, 20, 30, 40, 50],
                toolbar: [{
                    iconCls: 'icon-edit',
                    text: '查看',
                    handler: function () {
                        openSMCN($('#yj').datagrid('getSelected'));
                    }
                }, '-', {
                    iconCls: 'icon-help',
                    handler: function () {
                        alert('help')
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                    openSMCN(rowData);
                }

            });
        });

        var cmenu;
        function createColumnMenu() {
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function (item) {
                    if (item.iconCls == 'icon-ok') {
                        $('#DataGrdMb').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#DataGrdMb').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
            });
            var fields = $('#DataGrdMb').datagrid('getColumnFields');
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var col = $('#DataGrdMb').datagrid('getColumnOption', field);
                if (col.hidden) {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-empty'
                    });
                } else {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-ok'
                    });
                }
            }
        }
    </script>
</head>
<body>
    <div class="easyui-tabs" id="tt" style="width:100%;height:100%;">
        <div title="模板填报" style="width:100%;height:100%;fit:true">
            <form id="form1" runat="server" class="easyui-layout" style="width: 100%; height: 100%;">
            <div region="north" border="true"  style="height: 95px; padding: 1px;
                background: #B3DFDA;">
                <div style="padding: 3px;">
                    &nbsp; 核算中心：
                    <select id="SelHszx" style="width: 182px; display: inline;">
                    </select>
                    &nbsp;
                    <div id="DivYsyf" style="display: inline;">
                        &nbsp;财务实际发生月份：
                        <select id="SelYsyf"  disabled="disabled" style="width: 40px; display: inline;">
                        </select>
                    </div>
                    &nbsp; 执行状态：
                    <select id="SelExe" style="width: 70px; display: inline;">
                        <%--      <option value="0" selected="selected">未执行</option>
                        <option value="1">已执行</option>--%>
                    </select>
                    部门：
                    <select id="SelDep" style="width: 120px; display: inline;">
                    </select>
                    模板名称：<input id="TxtMbmc" class="easyui-textbox" style="width: 120px;" type="text" />&nbsp;
                    <a href="#" id="BtnInitMb" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
                        onclick="InitMb();" style="width: 75px;">查询</a>
                </div>
                <div style="padding: 3px;">
                    &nbsp; 方案类别：
                    <select id="SelFalb" style="width: 55px; display: inline;">
                    </select>
                    &nbsp;查询期：
                    <select id="SelYear" style="width: 56px; display: inline;">
                    </select>
                    &nbsp;年
                    <div id="DivMonth" style="display: inline;">
                        <select id="SelMonth" style="width: 40px;">
                        </select>
                        &nbsp;月
                    </div>
                    <div id="DivQuarter" style="display: inline;">
                        <select id="SelQuarter" style="width: 40px;">
                        </select>
                        &nbsp;季
                    </div>
                    &nbsp; 计划方案：
                    <select id="SelFa" style="width: 230px; display: inline;">
                    </select>
                    <%--  <input id="chkautocal" type="checkbox" style="width: 16px; height: 15px; vertical-align: middle;
                        display: inline;" onclick="autoclaclick();" />&nbsp;自动计算--%>
                    <div id="DivFa2" style="display: none;">
                        &nbsp; 对比方案：
                        <select id="SelFa2" style="width: 230px;">
                        </select>
                    </div>
                    &nbsp; &nbsp;&nbsp;&nbsp; <b><span id="JZ" style="color: Red; font-size: 18px;"></span>
                    </b>
                </div>
            </div>
            <%--    <div id="layoutwest" region="west" split="true" title="部门列表" style="width: 25%; padding: 2px;">
                <ul id="TreeZyml" class="easyui-tree" data-options="animate:true,dnd:true" style="width: 100%;
                    height: 100%;">
                </ul>
            </div>--%>
            <div id="DivCenter" region="center" title="模板">
                <div id="DivMb" style="width: 100%; height: 100%; display: block;">
                    <table id="DataGrdMb" style="width: 100%; height: 100%;">
                    </table>
                </div>
                <div id="mbsel" class="easyui-dialog" title="模板复制" style="width: 600px; height: 420px;"
                    data-options="iconCls:'icon-save',draggable:true,resizable:true,modal:true,maximizable: true,closed: true,cache: false,  buttons: [{text: '复制',iconCls: 'icon-ok', handler: function () {saveMb();}}, {text: '取消',iconCls: 'icon-undo',handler: function () {$('#mbsel').dialog('close');}}]">
                    <div id="cc" class="easyui-layout" fit="true">
                        <div data-options="region:'north',title:'',split:false" style="height: 25px;">
                            <div id="sel">
                                <select id="dep" style="width: 120px; display: inline;">
                                </select>部门,
                                <div id="CopyYear" style="display: inline;">
                                    <select id="year" name="year" style="width: 60px;">
                                    </select>&nbsp;年</div>
                                <div id="CopyQuarter" style="display: inline;">
                                    <select id="quarter" name="quarter" style="width: 60px;">
                                    </select>&nbsp;季</div>
                                <div id="CopyMonth" style="display: inline;">
                                    <select id="month" name="month" style="width: 60px;">
                                    </select>&nbsp;月</div>
                                ,要复制的方案:<%--<input id="selectfa" class="easyui-combobox" data-options="valueField:'id',textField:'text',panelHeight:'auto',onChange:function(newValue,oldValue){}"
                                    style="width: 150px"></input>--%>
                                <select id="selectfa" style="width: 160px; display: inline;">
                                </select>
                            </div>
                        </div>
                        <div data-options="region:'center',title:''" style="padding: 5px; background: #eee;">
                            <table id="dg"<%-- class="easyui-datagrid" data-options="width: function (){return document.body.clientWidth},nowrap: true,rownumbers: true,fit: true,animate: false,fitColumns: true,collapsible:true,lines: true,maximizable: true,maximized: true,singleSelect: false,frozenColumns: [[{field: 'ck',checkbox: true }]], <%--pageSize: 10,pageList: [10, 20, 30, 40], pagination: true,--%> rownumbers: true,idField: 'MBDM'"--%>>
        <%--                        <thead>
                                    <tr>
                                        <th data-options="field:'MBDM',width:20">
                                            模板代码
                                        </th>
                                        <th data-options="field:'MBMC',width:60">
                                            模板名称
                                        </th>
                                        <th data-options="field:'MBWJ',width:60,align:'right',hidden:'true'">
                                            模板文件
                                        </th>
                                        <th data-options="field:'SFCZ',width:30,align:'center'">
                                            模板文件
                                        </th>
                                    </tr>
                                </thead>--%>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div region="south" split="true" style="height: 37px; padding: 1px;">
                <a href="#" id="BtnOpenMb" class="easyui-linkbutton" style="margin: 0px 40px 0px 40px;"
                    data-options="iconCls:'icon-ok'" onclick="OpenMb();">打开模板</a> <a href="#" id="A1"
                        class="easyui-linkbutton" style="margin: 0px 20px" data-options="iconCls:'icon-add'"
                        onclick="openDialog();">复制模板数据</a> <a href="#" id="A2" class="easyui-linkbutton"
                            style="margin: 0px 20px" data-options="iconCls:'icon-add'" onclick="getMBCLYJ();">
                            查看审核，打回意见</a> <a href="#" id="A3" class="easyui-linkbutton" style="margin: 0px 20px"
                                data-options="iconCls:'icon-add'" onclick="getDownLoad();">下载模板</a>
                <div id="DownDialog" class="easyui-dialog" title="模板下载" style="width: 600px; height: 380px;"
                    data-options="iconCls:'icon-save',resizable:true,modal:false,closed: true">
                    <table id="DownLoadMb" style="width: 100%; height: 100%;">
                    </table>
                </div>
                <%--    <form method="post" action="xxxxx" target="ajaxifr" enctype="multipart/form-data">
                    ...表单项目
                </form>--%>
                <%--       <a href="#" id="A1111" class="easyui-linkbutton" style="margin: 0px 40px 0px 40px;"
                    data-options="iconCls:'icon-ok'" onclick="test1();">test1</a>--%>
                <%--                        <a href="#" id="A2"
                        class="easyui-linkbutton" style="margin: 0px 40px 0px 40px;" data-options="iconCls:'icon-ok'"
                        onclick="test2();">test2</a>--%>
            </div>
            
            <div id="grid" class="easyui-dialog" data-options="closed:true,title:'处理意见'">
                <table class="easyui-datagrid" id="yj" style="width: 500px; height: 250px" data-options="">
                    <thead>
                        <tr>
                            <th data-options="field:'MBDM',width:130,hidden:true">
                                模板代码
                            </th>
                            <th data-options="field:'MBMC',width:130">
                                模板名称
                            </th>
                            <th data-options="field:'USER_NAME',width:130">
                                用户名称
                            </th>
                            <th data-options="field:'CLSJ',width:130">
                                处理时间
                            </th>
                            <th data-options="field:'SPYJ',width:100,hidden:true">
                                审核意见
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- #include file="BmDialog.htm" -->
            <div id="SMQR" class="easyui-dialog" title="书面确认" data-options="maximizable: true,resizable: true,closed: true"
                style="width: 610px; height: 420px; padding: 10px; text-align: right;">
            </div>
            <input id="HidMBDM" type="hidden" runat="server" /> 
            </form>
            <form id="A4" method="post" action="ExcelData.ashx?action=GetDownLoads" style="display: none">
            <input id="jhfadm" name="jhfadm" value="" type="hidden" />
            <input id="excelFiles" name="excelFiles" value="" type="hidden" />
            <input id="zipFileName" name="zipFileName" value="" type="hidden" />
            </form> 
         </div>
    </div>
</body>
</html>
