﻿<%@ WebHandler Language="C#" Class="MB" %>
using System;
using System.Web;
using RMYH.DBUtility;
using System.Collections;
using RMYH.BLL;
using System.Text;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;
using System.Collections.Generic;

//using NPOI.HSSF;
//using System.IO;
//using NPOI.HSSF.UserModel;
//using NPOI.SS.UserModel;
public class zonecalroot
{
    public string sheetname { get; set; }
}
public class hidebyformularoot
{
    public string MBDM { get; set; }
    public string USERID { get; set; }
    public string MBLX { get; set; }
    public string NN { get; set; }
    public string SHEETNAMES { get; set; }
}
public class savedataroot
{
    public string count { get; set; }
    public string fadm { get; set; }
    public string mbdm { get; set; }
    public string userid { get; set; }
    public savedatagrids[] grids { get; set; }
}
public class savedatagrids
{
    public string ZYDM { get; set; }
    public string CPDM { get; set; }
    public string SJDX { get; set; }
    public string JSDX { get; set; }
    public string VAL { get; set; }
    public string ZFVAL { get; set; }
    public string JLDW { get; set; }
    public string XMFL { get; set; }
    public string SHEETNAME { get; set; }
}


public class savetrccroot
{
    public string hszxdm { get; set; }
    public string fadm { get; set; }
    public string count { get; set; }
    public savetrccgrids[] grids { get; set; }
}
public class savetrccgrids
{
    public string LX { get; set; }
    public string ZYDM { get; set; }
    public string ZZBCPDM { get; set; }
    public string ZZCPDM { get; set; }
    public string FADM { get; set; }
    public string CPDM { get; set; }
    public string TRL { get; set; }
    public string QC { get; set; }
    public string QM { get; set; }
    public string CCL { get; set; }
    public string YK { get; set; }
}



public class getgsroot
{
    public string count { get; set; }
    public string fadm { get; set; }
    public string fadm2 { get; set; }
    public string mbdm { get; set; }
    public string yy { get; set; }
    public string nn { get; set; }
    public string fabs { get; set; }
    public string loginyy { get; set; }
    public getgsgrids[] grids { get; set; }
}
public class getgsgrids
{
    public string idx { get; set; }
    public string row { get; set; }
    public string col { get; set; }
    public string gs { get; set; }
}

public class MB : IHttpHandler
{
    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);
    const int xlToRight = -4161;
    const int xlUp = -4162;
    const int xlToLeft = -4159;
    const int xlDown = -4121;

    public void ProcessRequest(HttpContext context)
    {
        string action = context.Request.Params["action"];
        //如果是存储数据：
        if (action == "savedata")
        {
            string jsonData = context.Request.Form["ParamStr"];
            JavaScriptSerializer js = new JavaScriptSerializer();
            savedataroot resdata = js.Deserialize<savedataroot>(jsonData);
            string icount = resdata.count;
            string fadm = resdata.fadm;
            string mbdm = resdata.mbdm;
            string userid = resdata.userid;
            ArrayList sqls = new ArrayList();
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("select MBLX,MBZQ from TB_YSBBMB WHERE MBDM='" + mbdm + "'");
            DataSet dsxjmb = bll.Query("SELECT M2.MBDM,M2.ZYDM FROM TB_YSMBXJB JB,TB_YSBBMB M1,TB_YSBBMB M2 WHERE JB.MBDM=M1.MBDM AND JB.CHILDMBDM=M2.MBDM AND JB.MBDM='" + mbdm + "'");
            if (ds.Tables[0].Rows[0]["MBLX"].ToString() == "2" && ds.Tables[0].Rows[0]["MBZQ"].ToString() == "3" && dsxjmb.Tables[0].Rows.Count > 0)
            {
                sqls.Add("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'");
                if (null != icount)
                {
                    if (!icount.Equals("0"))
                    {
                        for (int i = 0; i < int.Parse(icount); i++)
                        {
                            string zydm = resdata.grids[i].ZYDM;
                            string xmdm = resdata.grids[i].CPDM;
                            string sjdx = resdata.grids[i].SJDX;
                            string jsdx = resdata.grids[i].JSDX;
                            string value = resdata.grids[i].VAL;
                            string zfvalue = resdata.grids[i].ZFVAL;
                            string jldw = resdata.grids[i].JLDW;
                            string xmfl = resdata.grids[i].XMFL;
                            string sheetname = resdata.grids[i].SHEETNAME;
                            string sql = "";
                            //循环遍历关联下级模板
                            for (int j = 0; j < dsxjmb.Tables[0].Rows.Count; j++)
                            {
                                //如果当前模板作业代码等于关联下级模板
                                if (zydm == dsxjmb.Tables[0].Rows[j]["ZYDM"].ToString())
                                {
                                    //查找关联下级模板的预算金额
                                    DataSet DsValue = bll.Query("select VALUE from TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' AND ZYDM='" + zydm + "' AND XMDM='" + xmdm + "' AND SJDX='" + sjdx + "' AND JSDX='" + jsdx + "'");
                                    if (DsValue.Tables[0].Rows.Count > 0)
                                    {
                                        //如果预算金额不等，则修改关联下级模板的预算金额
                                        if (value != DsValue.Tables[0].Rows[0]["VALUE"].ToString())
                                        {
                                            sql = "update TB_BBFYSJ set VALUE=" + value + ",XGBZ='1' WHERE JHFADM='" + fadm + "' AND MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' AND ZYDM='" + zydm + "' AND XMDM='" + xmdm + "' AND SJDX='" + sjdx + "' AND JSDX='" + jsdx + "'";
                                            sqls.Add(sql);
                                            //找到有该模板上报权限的用户
                                            DataSet UserDs = bll.Query("SELECT JSDM FROM TB_MBJSQX WHERE MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' and SBQX='1'");
                                            if (UserDs.Tables[0].Rows.Count > 0)
                                            {
                                                //找到当前修改用户是哪个部门的
                                                DataSet UpZYMC = bll.Query("SELECT ZYMC FROM TB_JHZYML A,TB_YSBBMB B WHERE A.ZYDM=B.ZYDM AND MBDM='" + mbdm + "'");
                                                string ThisZy = UpZYMC.Tables[0].Rows[0]["ZYMC"].ToString();
                                                //修改的部门
                                                DataSet ZYMCDs = bll.Query("SELECT ZYMC FROM TB_JHZYML WHERE ZYDM='" + zydm + "'");
                                                string zymc = ZYMCDs.Tables[0].Rows[0]["ZYMC"].ToString();
                                                //当前用户名
                                                DataSet UserName = bll.Query("select USER_NAME FROM TB_USER WHERE USER_ID='" + userid + "'");
                                                string user_name = UserName.Tables[0].Rows[0]["USER_NAME"].ToString();
                                                string MESSAGESJ = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                //当前费用
                                                DataSet XmxxDS = bll.Query("select XMMC from TB_XMXX WHERE XMDM='" + xmdm + "'");
                                                string xmxx = XmxxDS.Tables[0].Rows[0]["XMMC"].ToString();
                                                //远费用
                                                DataSet oldValueDs = bll.Query("select VALUE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' AND ZYDM='" + zydm + "' AND XMDM='" + xmdm + "' AND SJDX='" + sjdx + "' AND JSDX='" + jsdx + "'");
                                                string oldValue = oldValueDs.Tables[0].Rows[0]["VALUE"].ToString();
                                                string info = ThisZy + user_name + "于" + MESSAGESJ + "将" + zymc + xmxx + "由" + oldValue + "万元调整为" + value + "万元";
                                                //发送消息到有该模板的用户
                                                for (int k = 0; k < UserDs.Tables[0].Rows.Count; k++)
                                                {
                                                    string jsjsdm = UserDs.Tables[0].Rows[k]["JSDM"].ToString();
                                                    sql = "insert into TB_MESSAGE(SJ,FSUSER,JSJSDM,INFO) VALUES('" + MESSAGESJ + "','" + user_name + "','" + jsjsdm + "','" + info + "')";
                                                    sqls.Add(sql);
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        //如果关联下级模板没数，直接添加
                                        if (value != "")
                                        {
                                            sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,VALUE,JLDW,SHEETNAME,XGBZ) VALUES('"
                                            + fadm + "','" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                                            + "',CONVERT(NUMERIC(22,8),'" + value + "'),'" + jldw + "','" + sheetname + "','1')";
                                            sqls.Add(sql);
                                            //找到有该模板上报权限的用户
                                            DataSet UserDs = bll.Query("SELECT JSDM FROM TB_MBJSQX WHERE MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' and SBQX='1'");
                                            if (UserDs.Tables[0].Rows.Count > 0)
                                            {
                                                //找到当前修改用户是哪个部门的
                                                DataSet UpZYMC = bll.Query("SELECT ZYMC FROM TB_JHZYML A,TB_YSBBMB B WHERE A.ZYDM=B.ZYDM AND MBDM='" + mbdm + "'");
                                                string ThisZy = UpZYMC.Tables[0].Rows[0]["ZYMC"].ToString();
                                                //修改的部门
                                                DataSet ZYMCDs = bll.Query("SELECT ZYMC FROM TB_JHZYML WHERE ZYDM='" + zydm + "'");
                                                string zymc = ZYMCDs.Tables[0].Rows[0]["ZYMC"].ToString();
                                                //当前用户名
                                                DataSet UserName = bll.Query("select USER_NAME FROM TB_USER WHERE USER_ID='" + userid + "'");
                                                string user_name = UserName.Tables[0].Rows[0]["USER_NAME"].ToString();
                                                string MESSAGESJ = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                //当前费用
                                                DataSet XmxxDS = bll.Query("select XMMC from TB_XMXX WHERE XMDM='" + xmdm + "'");
                                                string xmxx = XmxxDS.Tables[0].Rows[0]["XMMC"].ToString();
                                                //远费用
                                                DataSet oldValueDs = bll.Query("select VALUE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + dsxjmb.Tables[0].Rows[j]["MBDM"].ToString() + "' AND ZYDM='" + zydm + "' AND XMDM='" + xmdm + "' AND SJDX='" + sjdx + "' AND JSDX='" + jsdx + "'");
                                                string info = ThisZy + user_name + "于" + MESSAGESJ + "将" + zymc + xmxx + "调整为" + value + "万元";
                                                //发送消息到有该模板的用户
                                                for (int k = 0; k < UserDs.Tables[0].Rows.Count; k++)
                                                {
                                                    string jsjsdm = UserDs.Tables[0].Rows[k]["JSDM"].ToString();
                                                    sql = "insert into TB_MESSAGE(SJ,FSUSER,JSJSDM,INFO) VALUES('" + MESSAGESJ + "','" + user_name + "','" + jsjsdm + "','" + info + "')";
                                                    sqls.Add(sql);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(value))
                            {
                                sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,VALUE,JLDW,SHEETNAME) VALUES('"
                                   + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                                   + "',CONVERT(NUMERIC(22,8),'" + value + "'),'" + jldw + "','" + sheetname + "')";
                            }
                            else
                            {
                                sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,ZFVALUE,JLDW,SHEETNAME) VALUES('"
                                  + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                                  + "','" + zfvalue + "','" + jldw + "','" + sheetname + "')";
                            }
                            sqls.Add(sql);
                        }
                    }
                    //去掉事务：
                    //DbHelperOra.ExecuteSqlTran(sqls);
                    for (int i = 0; i < sqls.Count; i++)
                    {
                        DbHelperOra.ExecuteSql(sqls[i].ToString());
                    }
                    context.Response.Write("保存完成！");
                }
                else
                {
                    context.Response.Write("保存失败！");
                }
            }
            else
            {
                sqls.Add("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'");
                if (null != icount)
                {
                    if (!icount.Equals("0"))
                    {
                        for (int i = 0; i < int.Parse(icount); i++)
                        {
                            string zydm = resdata.grids[i].ZYDM;
                            string xmdm = resdata.grids[i].CPDM;
                            string sjdx = resdata.grids[i].SJDX;
                            string jsdx = resdata.grids[i].JSDX;
                            string value = resdata.grids[i].VAL;
                            string zfvalue = resdata.grids[i].ZFVAL;
                            string jldw = resdata.grids[i].JLDW;
                            string xmfl = resdata.grids[i].XMFL;
                            string sheetname = resdata.grids[i].SHEETNAME;
                            string sql = "";
                            if (!String.IsNullOrEmpty(value))
                            {
                                sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,VALUE,JLDW,SHEETNAME) VALUES('"
                                    + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                                    + "',CONVERT(NUMERIC(22,8),'" + value + "'),'" + jldw + "','" + sheetname + "')";
                            }
                            else
                            {
                                sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,ZFVALUE,JLDW,SHEETNAME) VALUES('"
                                    + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                                    + "','" + zfvalue + "','" + jldw + "','" + sheetname + "')";
                            }
                            sqls.Add(sql);
                        }
                    }
                    //去掉事务：
                    //DbHelperOra.ExecuteSqlTran(sqls);
                    for (int i = 0; i < sqls.Count; i++)
                    {
                        DbHelperOra.ExecuteSql(sqls[i].ToString());
                    }
                    context.Response.Write("保存完成！");
                }
                else
                {
                    context.Response.Write("保存失败！");
                }
            }
        }
    //    if (action == "savedata")
    //    {
    //        string jsonData = context.Request.Form["ParamStr"];
    //        JavaScriptSerializer js = new JavaScriptSerializer();
    //        savedataroot resdata = js.Deserialize<savedataroot>(jsonData);
    //        string icount = resdata.count;
    //        string fadm = resdata.fadm;
    //        string mbdm = resdata.mbdm;
    //        ArrayList sqls = new ArrayList();
    //        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //        //查询审批费用数据标志
    //        string Flag = "select FLAG from TB_SPBBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'";
    //        DataSet DsFlag = bll.Query(Flag);
    //        //如果没有数据执行添加
    //        if (DsFlag.Tables[0].Rows.Count <= 0)
    //        {
    //            //先清空当前方案的模板
    //            string del = "DELETE FROM TB_SPBBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'";
    //            sqls.Add(del);
    //            //添加数据到TB_SPBBFYSJ
    //            string add = "insert into TB_SPBBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE,TIME,FLAG)";
    //            add += " SELECT JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE,TIME,'0' FROM TB_BBFYSJ WHERE MBDM='" + mbdm + "' AND JHFADM='" + fadm + "'";
    //            sqls.Add(add);
    //            sqls.Add("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'");
    //        }
    //        //标志不为0
    //        else if (DsFlag.Tables[0].Rows[0][0].ToString() != "0")
    //        {
    //            //先清空当前方案的模板
    //            string del = "DELETE FROM TB_SPBBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'";
    //            sqls.Add(del);
    //            //添加数据到TB_SPBBFYSJ
    //            string add = "insert into TB_SPBBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE,TIME,FLAG)";
    //            add += " SELECT JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE,TIME,'0' FROM TB_BBFYSJ WHERE MBDM='" + mbdm + "' AND JHFADM='" + fadm + "'";
    //            sqls.Add(add);
    //        }
    //        sqls.Add("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'");
    //        if (null != icount)
    //        {
    //            if (!icount.Equals("0"))
    //            {
    //                for (int i = 0; i < int.Parse(icount); i++)
    //                {
    //                    string zydm = resdata.grids[i].ZYDM;
    //                    string xmdm = resdata.grids[i].CPDM;
    //                    string sjdx = resdata.grids[i].SJDX;
    //                    string jsdx = resdata.grids[i].JSDX;
    //                    string value = resdata.grids[i].VAL;
    //                    string zfvalue = resdata.grids[i].ZFVAL;
    //                    string jldw = resdata.grids[i].JLDW;
    //                    string xmfl = resdata.grids[i].XMFL;
    //                    string sheetname = resdata.grids[i].SHEETNAME;
    //                    string sql = "";
    //                    if (!String.IsNullOrEmpty(value))
    //                    {
    //                        sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,VALUE,JLDW,SHEETNAME) VALUES('"
    //                            + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
    //                            + "',CONVERT(NUMERIC(22,8),'" + value + "'),'" + jldw + "','" + sheetname + "')";
    //                    }
    //                    else
    //                    {
    //                        sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,ZFVALUE,JLDW,SHEETNAME) VALUES('"
    //                            + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
    //                            + "','" + zfvalue + "','" + jldw + "','" + sheetname + "')";
    //                    }
    //                    sqls.Add(sql);
    //                }
    //            }
    //            //去掉事务：
    //            //DbHelperOra.ExecuteSqlTran(sqls);
    //            for (int i = 0; i < sqls.Count; i++)
    //            {
    //                DbHelperOra.ExecuteSql(sqls[i].ToString());
    //            }
    //            context.Response.Write("保存完成！");
    //        }
    //        else
    //        {
    //            context.Response.Write("保存失败！");
    //        }
    //}
    //如果是取值操作：
        else if (action == "getgs")
        {
            string jsonData = context.Request.Form["gs"];
            JavaScriptSerializer js = new JavaScriptSerializer();
            getgsroot resdata = js.Deserialize<getgsroot>(jsonData);
            string icount = resdata.count;
            string fadm = resdata.fadm;
            string mbdm = resdata.mbdm;
            string fadm2 = resdata.fadm2;
            //当前服务器时间年月：
            string bnyy = resdata.yy;
            string bnnn = resdata.nn;
            string fabs = resdata.fabs;
            string loginyy = resdata.loginyy;
            string resjson = "{\"count\":" + icount + ",\"grids\":[";
            for (int i = 0; i < int.Parse(icount); i++)
            {
                if (i > 0)
                {
                    resjson += ",";
                }
                string idx = resdata.grids[i].idx;
                string row = resdata.grids[i].row;
                string col = resdata.grids[i].col;
                string gs = resdata.grids[i].gs;
                try
                {
                    resjson += "{\"gs\":\"" + dealExcel.GetFunVal(gs, bnyy, bnnn, fadm, fadm2, fabs, loginyy).Replace("\r", "").Replace("\n", "") + "\"}";
                }
                catch (Exception ex)
                {
                    throw new Exception("第" + idx.ToString() + "个标签页，" + row.ToString() + "行、" + col.ToString() + "列，公式：" + gs
                        + "出错！\n" + ex.Message);
                }
            }
            resjson += "]}";
            context.Response.Write(resjson);
            context.Response.End();
        }
        //如果是存储数据：
        else if (action == "savedata1")
        {
            string icount = context.Request.Form["count"];
            string fadm = context.Request.Form["fadm"];
            string mbdm = context.Request.Form["mbdm"];
            ArrayList sqls = new ArrayList();
            sqls.Add("DELETE FROM TB_BBFYSJ WHERE JHFADM='" + fadm + "' AND MBDM='" + mbdm + "'");
            if (null != icount)
            {
                if (!icount.Equals("0"))
                {
                    for (int i = 0; i < int.Parse(icount); i++)
                    {
                        string zydm = context.Request.Form["grids[" + i + "][ZYDM]"];
                        string xmdm = context.Request.Form["grids[" + i + "][CPDM]"];
                        string sjdx = context.Request.Form["grids[" + i + "][SJDX]"];
                        string jsdx = context.Request.Form["grids[" + i + "][JSDX]"];
                        string value = context.Request.Form["grids[" + i + "][VAL]"];
                        string zfvalue = context.Request.Form["grids[" + i + "][ZFVAL]"];
                        string jldw = context.Request.Form["grids[" + i + "][JLDW]"];
                        string xmfl = context.Request.Form["grids[" + i + "][XMFL]"];
                        string sheetname = context.Request.Form["grids[" + i + "][SHEETNAME]"];
                        string sql = "";
                        if (!String.IsNullOrEmpty(value))
                        {
                            sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,VALUE,JLDW,SHEETNAME) VALUES('"
                               + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                               + "',CONVERT(NUMERIC(22,8),'" + value + "'),'" + jldw + "','" + sheetname + "')";
                        }
                        else
                        {
                            sql = "INSERT INTO TB_BBFYSJ(JHFADM,MBDM,ZYDM,XMFL,XMDM,JSDX,SJDX,ZFVALUE,JLDW,SHEETNAME) VALUES('"
                              + fadm + "','" + mbdm + "','" + zydm + "','" + xmfl + "','" + xmdm + "','" + jsdx + "','" + sjdx
                              + "','" + zfvalue + "','" + jldw + "','" + sheetname + "')";
                        }
                        sqls.Add(sql);
                    }
                }
                //去掉事务：
                //DbHelperOra.ExecuteSqlTran(sqls);
                for (int i = 0; i < sqls.Count; i++)
                {
                    DbHelperOra.ExecuteSql(sqls[i].ToString());
                }
                context.Response.Write("保存完成！");
            }
            else
            {
                context.Response.Write("保存失败！");
            }

        }
        //如果是取值操作：
        else if (action == "getgs1")
        {
            string icount = context.Request.Form["count"];
            string fadm = context.Request.Form["fadm"];
            string fadm2 = context.Request.Form["fadm2"];
            //当前服务器时间年月：
            string bnyy = context.Request.Form["yy"];
            string bnnn = context.Request.Form["nn"];
            string fabs = context.Request.Form["fabs"];
            string loginyy = context.Request.Form["loginyy"];
            string resjson = "{\"count\":" + icount + ",\"grids\":[";
            for (int i = 0; i < int.Parse(icount); i++)
            {
                if (i > 0)
                {
                    resjson += ",";
                }
                string idx = context.Request.Form["grids[" + i.ToString() + "][idx]"];
                string row = context.Request.Form["grids[" + i.ToString() + "][row]"];
                string col = context.Request.Form["grids[" + i.ToString() + "][col]"];
                string gs = context.Request.Form["grids[" + i.ToString() + "][gs]"];
                try
                {
                    //resjson += "{\"idx\":\"" + idx + "\",\"row\":\"" + row + "\",\"col\":\""
                    //    + col + "\",\"gs\":\"" + dealExcel.GetFunVal(gs, bnyy, bnnn, fadm, fadm2, fabs, loginyy).Replace("\r", "").Replace("\n", "") + "\"}";
                    resjson += "{\"gs\":\"" + dealExcel.GetFunVal(gs, bnyy, bnnn, fadm, fadm2, fabs, loginyy).Replace("\r", "").Replace("\n", "") + "\"}";
                }
                catch (Exception ex)
                {
                    throw new Exception("第" + idx.ToString() + "个标签页，" + row.ToString() + "行、" + col.ToString() + "列，公式：" + gs
                        + "出错！\n" + ex.Message);
                }
            }
            resjson += "]}";
            //context.Response.Clear();
            //System.IO.File.WriteAllText(@"C:\test.txt", context.Request.Form.ToString(), Encoding.UTF8);
            context.Response.Write(resjson);
            context.Response.End();
        }
        //如果是获取服务器年月：
        else if (action == "getyynn")
        {
            try
            {
                //string userid = HttpContext.Current.Session["USERDM"].ToString();
                string sql = "SELECT CONVERT(VARCHAR(20),GETDATE(),112) DT";
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = new DataSet();
                ds = bll.Query(sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    context.Response.Write(ds.Tables[0].Rows[0]["DT"].ToString());
                }
                else
                {
                    context.Response.Write("");
                }
            }
            catch
            {
                context.Response.Write("");
            }
        }
        //如果是获取投入产出数据：
        else if (action == "inittrcc")
        {
            string hszxdm = context.Request.QueryString["hszxdm"];
            string sql = "SELECT * FROM V_GETFLOW WHERE HSZXDM='" + hszxdm + "' ORDER BY ORDERBM";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = new DataSet();
            ds = bll.Query(sql);
            string resjson = "{\"count\":" + ds.Tables[0].Rows.Count + ",\"grids\":[";
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i > 0)
                    {
                        resjson += ",";
                    }
                    else
                    {
                        resjson += "{\"bm\":\"编码\",\"zymc\":\"作业名称\",\"famc\":\"方案名称\",\"lxmc\":\"流向名称\","
                        + "\"cpmc\":\"产品名称\",\"trl\":\"投入量\",\"ccl\":\"产出量\",\"qc\":\"期初量\","
                        + "\"qm\":\"期末量\",\"yk\":\"盈亏量\"},";
                    }
                    string BM = ds.Tables[0].Rows[i]["BM"].ToString().Replace("\"", "\\\"");
                    string ZYMC = ds.Tables[0].Rows[i]["ZYMC"].ToString();
                    string FAMC = ds.Tables[0].Rows[i]["FAMC"].ToString();
                    string LXMC = ds.Tables[0].Rows[i]["LXMC"].ToString();
                    string CPMC = ds.Tables[0].Rows[i]["CPMC"].ToString();
                    string TRL = ds.Tables[0].Rows[i]["TRL"].ToString();
                    string CCL = ds.Tables[0].Rows[i]["CCL"].ToString();
                    string QC = ds.Tables[0].Rows[i]["QC"].ToString();
                    string QM = ds.Tables[0].Rows[i]["QM"].ToString();
                    string YK = ds.Tables[0].Rows[i]["YK"].ToString();
                    resjson += "{\"bm\":\"" + BM + "\",\"zymc\":\"" + ZYMC + "\",\"famc\":\"" + FAMC + "\",\"lxmc\":\"" + LXMC + "\","
                        + "\"cpmc\":\"" + CPMC + "\",\"trl\":\"" + TRL + "\",\"ccl\":\"" + CCL + "\",\"qc\":\"" + QC + "\","
                        + "\"qm\":\"" + QM + "\",\"yk\":\"" + YK + "\"}";
                }
            }
            resjson += "]}";
            context.Response.Write(resjson);
        }
        //如果是保存投入产出数据：
        else if (action == "savetrcc")
        {
            string jsonData = context.Request.Form["ParamStr"];
            JavaScriptSerializer js = new JavaScriptSerializer();
            savetrccroot resdata = js.Deserialize<savetrccroot>(jsonData);
            string hszxdm = context.Request.QueryString["hszxdm"];
            string jhfadm = context.Request.QueryString["fadm"]; ;
            string icount = resdata.count;
            ArrayList sqls = new ArrayList();
            string sql = "";
            string cpdm = "";
            string trfadm = "";
            string trl = "";
            string ccl = "";
            string qc = "";
            string qm = "";
            string yk = "";
            sqls.Add("DELETE FROM TB_JHZYTR WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHZYCC WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHZZBCPTHGC WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            sqls.Add("DELETE FROM TB_JHZZCPTHGC WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHBCPQCQM WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            sqls.Add("DELETE FROM TB_JHZZCPQCQM WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            for (int i = 0; i < int.Parse(icount); i++)
            {
                string lx = resdata.grids[i].LX;
                string zydm = resdata.grids[i].ZYDM;
                string zzbcpdm = resdata.grids[i].ZZBCPDM;
                string zzcpdm = resdata.grids[i].ZZCPDM;
                //如果是作业：
                if (!String.IsNullOrEmpty(zydm))
                {
                    trfadm = resdata.grids[i].FADM;
                    cpdm = resdata.grids[i].CPDM;
                    switch (lx)
                    {
                        //作业投入：
                        case "1":
                            trl = resdata.grids[i].TRL;
                            sql = "INSERT INTO TB_JHZYTR(HSZXDM,JHFADM,ZYDM,TRFADM,TRCPDM,FAZCLL,FYYSDM,JGLDYBZ) SELECT HSZXDM,'"
                                + jhfadm + "',ZYDM,FADM,ZFCPDM," + GetSl(trl)
                                + ",FYYSDM,JGLDYBZ FROM TB_JHTHCPGC WHERE HSZXDM='" + hszxdm + "' AND ZYDM='" + zydm + "' AND FADM='" + trfadm
                                + "' AND ZFCPDM='" + cpdm + "'";
                            sqls.Add(sql);
                            break;
                        //作业产出或损失：
                        case "2":
                        case "3":
                            ccl = resdata.grids[i].CCL;
                            sql = "INSERT INTO TB_JHZYCC(HSZXDM,JHFADM,ZYDM,TRFADM,CCCPDM,CPCLL) VALUES('"
                                + hszxdm + "','" + jhfadm + "','" + zydm + "','" + trfadm + "','"
                                + cpdm + "'," + GetSl(ccl) + ")";
                            sqls.Add(sql);
                            break;
                    }
                }
                //如果是自制半成品：
                else if (!String.IsNullOrEmpty(zzbcpdm))
                {
                    switch (lx)
                    {
                        //自制半成品合计：
                        case "0":
                            qc = resdata.grids[i].QC;
                            qm = resdata.grids[i].QM;
                            trl = resdata.grids[i].TRL;
                            ccl = resdata.grids[i].CCL;
                            yk = resdata.grids[i].YK;
                            sql = "INSERT INTO TB_JHBCPQCQM(HSZXDM,JHFADM,ZZBCPDM,QCCL,TRCL,NGCL,QMCL,YKCL) SELECT '" + hszxdm + "','"
                                + jhfadm + "','" + zzbcpdm + "'," + GetSl(qc) + "," + GetSl(trl) + "," + GetSl(ccl) + ","
                                + GetSl(qm) + "," + GetSl(yk);
                            sqls.Add(sql);
                            break;
                        //自制半成品投入：
                        case "1":
                            cpdm = resdata.grids[i].CPDM;
                            trl = resdata.grids[i].TRL;
                            sql = "INSERT INTO TB_JHZZBCPTHGC(HSZXDM,JHFADM,CPDM,ZFCPDM,ZFL) VALUES('"
                                + hszxdm + "','" + jhfadm + "','" + zzbcpdm + "','" + cpdm + "'," + GetSl(trl) + ")";
                            sqls.Add(sql);
                            break;
                    }

                }
                //如果是产品：
                else if (!String.IsNullOrEmpty(zzcpdm))
                {
                    switch (lx)
                    {
                        //产品合计：
                        case "0":
                            qc = resdata.grids[i].QC;
                            qm = resdata.grids[i].QM;
                            trl = resdata.grids[i].TRL;
                            ccl = resdata.grids[i].CCL;
                            yk = resdata.grids[i].YK;
                            sql = "INSERT INTO TB_JHZZCPQCQM(HSZXDM,JHFADM,ZZCPDM,QCCL,TRCL,ZCCL,QMCL,YKCL) SELECT '" + hszxdm + "','"
                                + jhfadm + "','" + zzcpdm + "'," + GetSl(qc) + "," + GetSl(trl) + "," + GetSl(ccl) + ","
                                + GetSl(qm) + "," + GetSl(yk);
                            sqls.Add(sql);
                            break;
                        //产品投入：
                        case "1":
                            cpdm = resdata.grids[i].CPDM;
                            trl = resdata.grids[i].TRL;
                            sql = "INSERT INTO TB_JHZZCPTHGC(HSZXDM,JHFADM,CPDM,ZFCPDM,ZFL) VALUES('"
                                + hszxdm + "','" + jhfadm + "','" + zzcpdm + "','" + cpdm + "'," + GetSl(trl) + ")";
                            sqls.Add(sql);
                            break;
                    }
                }
            }

            DbHelperOra.ExecuteSqlTran(sqls);
            context.Response.Write("保存完成！");
        }
        //如果是保存投入产出数据：
        else if (action == "savetrcc1")
        {
            string hszxdm = context.Request.QueryString["hszxdm"];
            string jhfadm = context.Request.QueryString["fadm"];
            string icount = context.Request.Form["count"];
            ArrayList sqls = new ArrayList();
            string sql = "";
            string cpdm = "";
            string trfadm = "";
            string trl = "";
            string ccl = "";
            string qc = "";
            string qm = "";
            string yk = "";
            sqls.Add("DELETE FROM TB_JHZYTR WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHZYCC WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHZZBCPTHGC WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            sqls.Add("DELETE FROM TB_JHZZCPTHGC WHERE HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'");
            sqls.Add("DELETE FROM TB_JHBCPQCQM WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            sqls.Add("DELETE FROM TB_JHZZCPQCQM WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'");
            for (int i = 0; i < int.Parse(icount); i++)
            {
                string lx = context.Request.Form["grids[" + i + "][LX]"];
                string zydm = context.Request.Form["grids[" + i + "][ZYDM]"];
                string zzbcpdm = context.Request.Form["grids[" + i + "][ZZBCPDM]"];
                string zzcpdm = context.Request.Form["grids[" + i + "][ZZCPDM]"];
                //如果是作业：
                if (!String.IsNullOrEmpty(zydm))
                {
                    trfadm = context.Request.Form["grids[" + i + "][FADM]"];
                    cpdm = context.Request.Form["grids[" + i + "][CPDM]"];
                    switch (lx)
                    {
                        //作业投入：
                        case "1":
                            trl = context.Request.Form["grids[" + i + "][TRL]"];
                            sql = "INSERT INTO TB_JHZYTR(HSZXDM,JHFADM,ZYDM,TRFADM,TRCPDM,FAZCLL,FYYSDM,JGLDYBZ) SELECT HSZXDM,'"
                               + jhfadm + "',ZYDM,FADM,ZFCPDM," + GetSl(trl)
                               + ",FYYSDM,JGLDYBZ FROM TB_JHTHCPGC WHERE HSZXDM='" + hszxdm + "' AND ZYDM='" + zydm + "' AND FADM='" + trfadm
                               + "' AND ZFCPDM='" + cpdm + "'";
                            sqls.Add(sql);
                            break;
                        //作业产出或损失：
                        case "2":
                        case "3":
                            ccl = context.Request.Form["grids[" + i + "][CCL]"];
                            sql = "INSERT INTO TB_JHZYCC(HSZXDM,JHFADM,ZYDM,TRFADM,CCCPDM,CPCLL) VALUES('"
                               + hszxdm + "','" + jhfadm + "','" + zydm + "','" + trfadm + "','"
                               + cpdm + "'," + GetSl(ccl) + ")";
                            sqls.Add(sql);
                            break;
                    }
                }
                //如果是自制半成品：
                else if (!String.IsNullOrEmpty(zzbcpdm))
                {
                    switch (lx)
                    {
                        //自制半成品合计：
                        case "0":
                            qc = context.Request.Form["grids[" + i + "][QC]"];
                            qm = context.Request.Form["grids[" + i + "][QM]"];
                            trl = context.Request.Form["grids[" + i + "][TRL]"];
                            ccl = context.Request.Form["grids[" + i + "][CCL]"];
                            yk = context.Request.Form["grids[" + i + "][YK]"];
                            sql = "INSERT INTO TB_JHBCPQCQM(HSZXDM,JHFADM,ZZBCPDM,QCCL,TRCL,NGCL,QMCL,YKCL) SELECT '" + hszxdm + "','"
                               + jhfadm + "','" + zzbcpdm + "'," + GetSl(qc) + "," + GetSl(trl) + "," + GetSl(ccl) + ","
                               + GetSl(qm) + "," + GetSl(yk);
                            sqls.Add(sql);
                            break;
                        //自制半成品投入：
                        case "1":
                            cpdm = context.Request.Form["grids[" + i + "][CPDM]"];
                            trl = context.Request.Form["grids[" + i + "][TRL]"];
                            sql = "INSERT INTO TB_JHZZBCPTHGC(HSZXDM,JHFADM,CPDM,ZFCPDM,ZFL) VALUES('"
                               + hszxdm + "','" + jhfadm + "','" + zzbcpdm + "','" + cpdm + "'," + GetSl(trl) + ")";
                            sqls.Add(sql);
                            break;
                    }

                }
                //如果是产品：
                else if (!String.IsNullOrEmpty(zzcpdm))
                {
                    switch (lx)
                    {
                        //产品合计：
                        case "0":
                            qc = context.Request.Form["grids[" + i + "][QC]"];
                            qm = context.Request.Form["grids[" + i + "][QM]"];
                            trl = context.Request.Form["grids[" + i + "][TRL]"];
                            ccl = context.Request.Form["grids[" + i + "][CCL]"];
                            yk = context.Request.Form["grids[" + i + "][YK]"];
                            sql = "INSERT INTO TB_JHZZCPQCQM(HSZXDM,JHFADM,ZZCPDM,QCCL,TRCL,ZCCL,QMCL,YKCL) SELECT '" + hszxdm + "','"
                               + jhfadm + "','" + zzcpdm + "'," + GetSl(qc) + "," + GetSl(trl) + "," + GetSl(ccl) + ","
                               + GetSl(qm) + "," + GetSl(yk);
                            sqls.Add(sql);
                            break;
                        //产品投入：
                        case "1":
                            cpdm = context.Request.Form["grids[" + i + "][CPDM]"];
                            trl = context.Request.Form["grids[" + i + "][TRL]"];
                            sql = "INSERT INTO TB_JHZZCPTHGC(HSZXDM,JHFADM,CPDM,ZFCPDM,ZFL) VALUES('"
                               + hszxdm + "','" + jhfadm + "','" + zzcpdm + "','" + cpdm + "'," + GetSl(trl) + ")";
                            sqls.Add(sql);
                            break;
                    }
                }
            }

            DbHelperOra.ExecuteSqlTran(sqls);
            context.Response.Write("保存完成！");
        }
        //如果是区域计算：
        //else if (action == "zonecal")
        //{
        //    string gsyy = context.Request.QueryString["gsyy"];
        //    string yy = context.Request.QueryString["yy"];
        //    string gsnn = context.Request.QueryString["gsnn"];
        //    string fadm = context.Request.QueryString["fadm"];
        //    string fadm2 = context.Request.QueryString["fadm2"];
        //    string fabs = context.Request.QueryString["fabs"];
        //    string adr = context.Request.QueryString["adr"];
        //    string wjm = context.Request.QueryString["wjm"];
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    string jsonData = context.Request.Form["ParamStr"];
        //    zonecalroot resdata = js.Deserialize<zonecalroot>(jsonData);
        //    string sheetname = resdata.sheetname;
        //    string resjson = dealExcel.QYJS(gsyy, gsnn, fadm, fadm2, fabs, adr, wjm, sheetname, yy).Replace("\r", "").Replace("\n", "");
        //    resjson = (resjson == "") ? "{}" : resjson;
        //    context.Response.Write(resjson);
        //    context.Response.End();
   
        //}
        ////如果是全区域计算：
        //else if (action == "allzonecal")
        //{
        //    string gsyy = context.Request.QueryString["gsyy"];
        //    string gsnn = context.Request.QueryString["gsnn"];
        //    string fadm = context.Request.QueryString["fadm"];
        //    string fadm2 = context.Request.QueryString["fadm2"];
        //    string fabs = context.Request.QueryString["fabs"];
        //    string wjm = context.Request.QueryString["wjm"];
        //    string yy = context.Request.QueryString["yy"];
        //    string resjson = dealExcel.QQYJS(gsyy, gsnn, fadm, fadm2, fabs, wjm, yy).Replace("\r", "").Replace("\n", "");
        //    resjson = (resjson == "") ? "{}" : resjson;
        //    context.Response.Write(resjson);
        //}
        //else if (action == "getrowcol")
        //{
        //    string wjm = context.Request.QueryString["wjm"];
        //    string resjson = dealExcel.GETROWCOL(wjm).Replace("\r", "").Replace("\n", "");
        //    resjson = (resjson == "") ? "{}" : resjson;
        //    context.Response.Write(resjson);
        //}
        //校验公式：
        else if (action == "checkformula")
        {
            string mbdm = context.Request.QueryString["mbdm"];
            string gsyy = context.Request.QueryString["gsyy"];
            string gsnn = context.Request.QueryString["gsnn"];
            string fadm = context.Request.QueryString["fadm"];
            string fadm2 = context.Request.QueryString["fadm2"];
            string fabs = context.Request.QueryString["fabs"];
            string yy = context.Request.QueryString["yy"];
            context.Response.Write(GetDataList.JXGS(mbdm, gsyy, gsnn, fadm, fadm2, fabs, yy));
        }
        //方案模版中平衡校验：
        else if (action == "fambcheck")
        {
            string hszxdm = context.Request.QueryString["hszxdm"];
            string fadm = context.Request.QueryString["fadm"];
            context.Response.Write(fambcheck(hszxdm, fadm));
        }
        //返回状态，确认是否有需要打开承诺书  by liguosheng
        else if (action == "isDisPlaySmcn")
        {
            string mbdm = context.Request.QueryString["mbdm"];
            string userid = context.Request.QueryString["userid"];
            string res = isDisPlaySmcn(mbdm, userid);
            context.Response.Write(res);
        }
        //返回书面承诺SMCN.html的内容  by liguosheng
        else if (action == "getValue")
        {
            string MBDM = context.Request.QueryString["MBDM"];
            string USER_ID = context.Request.QueryString["USER_ID"];
            string FLAG = context.Request.QueryString["FLAG"];
            string res = getValue(MBDM, USER_ID, FLAG);
            context.Response.Write(res);
        }
        //如果是校验行列是否为空的公式：
        else if (action == "getJygs")
        {
            string MBDM = context.Request.QueryString["MBDM"];
            string res = getJygs(MBDM);
            context.Response.Write(res);
        }
        //如果是更新批复状态：
        else if (action == "dealrespondstatus")
        {
            string JHFADM = context.Request.QueryString["jhfadm"];
            string MBDM = context.Request.QueryString["mbdm"];
            string USERID = context.Request.QueryString["userid"];
            context.Response.Write(dealrespondstatus(JHFADM, MBDM, USERID));
        }
        //根据公示隐藏列：
        else if (action == "hidecolbyformula")
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            string jsonData = context.Request.Form["ParamStr"];
            hidebyformularoot resdata = js.Deserialize<hidebyformularoot>(jsonData);
            string MBDM = resdata.MBDM;
            string USERID = resdata.USERID;
            string MBLX = resdata.MBLX;
            string NN = resdata.NN;
            //将'09'改为'9':
            if (!NN.Equals("") && NN[0] == '0')
            {
                NN = NN.Substring(1);
            }
            string SHEETNAMES = resdata.SHEETNAMES;
            context.Response.Write(hidecolbyformula(MBDM, USERID, MBLX, NN, SHEETNAMES));
        }
        //根据公示隐藏列：
        else if (action == "hidecolbyformula1")
        {

            string MBDM = context.Request.QueryString["mbdm"];
            string USERID = context.Request.QueryString["userid"];
            string MBLX = context.Request.QueryString["mblx"];
            string NN = context.Request.QueryString["nn"];
            //将'09'改为'9':
            if (!NN.Equals("") && NN[0] == '0')
            {
                NN = NN.Substring(1);
            }
            string SHEETNAMES = context.Request.Form["sheetnames"];
            context.Response.Write(hidecolbyformula(MBDM, USERID, MBLX, NN, SHEETNAMES));
        }
        //根据版计算列：
        else if (action == "calcolsbytemplet")
        {
            string MBDM = context.Request.QueryString["mbdm"];
            string MBLX = context.Request.QueryString["mblx"];
            context.Response.Write(calcolsbytemplet(MBDM, MBLX));
        }
        else if (action == "SelHLX")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            int flag = 0;
            string mbdm = context.Request.QueryString["mbdm"];
            string sheetName = context.Server.UrlDecode(context.Request.QueryString["sheetName"]);
            string sql = "SELECT HXLX FROM TB_MBSHEETSX WHERE  MBDM='" + mbdm + "'  AND SHEETNAME='" + sheetName + "'";
            DataSet ds = bll.Query(sql);
            flag = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            context.Response.Write(flag);
        }
        else if (action == "InsertBDXM")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string hszxdm = context.Request.QueryString["hszxdm"];
            string xmfl = context.Request.QueryString["xmfl"];
            string mbdm = context.Request.QueryString["mbdm"];
            string add = "INSERT INTO TB_BMBDXMDM(HSZXDM,BMDM,XMDM,BDXMDM,BDXMMC) SELECT B.HSZXDM,B.BMDM,'" + xmfl + "',B.XMDM,A.XMMC";
            add += " FROM V_XMZD A, TB_BMYSXM B WHERE A.CPBS=6 AND A.XMDM=B.XMDM AND A.HSZXDM='" + hszxdm + "' AND BMDM=( SELECT";
            add += " ZYDM FROM TB_YSBBMB WHERE MBDM='" + mbdm + "') AND A.XMDM NOT IN(SELECT BDXMDM FROM TB_BMBDXMDM C WHERE ";
            add += " HSZXDM='" + hszxdm + "' AND BMDM=(SELECT ZYDM FROM TB_YSBBMB WHERE MBDM='" + mbdm + "'))";
            bll.ExecuteSql(add);
        }
        else if (action == "GetDataGrid")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            List<string> list = new List<string>();
            StringBuilder mbStr = new StringBuilder();
            StringBuilder faStr = new StringBuilder();
            string hszxdm = context.Request.QueryString["hszxdm"];
            string zydm = context.Request.QueryString["zydm"];
            string cpdm = context.Request.QueryString["cpdm"];
            string sql = "SELECT HSZXDM,BMDM,XMDM,BDXMDM,BDXMMC FROM TB_BMBDXMDM WHERE HSZXDM='" + hszxdm + "' ";
            sql += " AND BMDM='" + zydm + "' AND XMDM='" + cpdm + "'";
            DataSet da = bll.Query(sql);
            mbStr.Append("{\"total\":");
            mbStr.Append(da.Tables[0].Rows.Count);
            mbStr.Append(",");
            mbStr.Append("\"rows\":[");
            for (int i = 0; i < da.Tables[0].Rows.Count; i++)
            {
                mbStr.Append("{");
                mbStr.Append("\"BDXMDM\":\"" + da.Tables[0].Rows[i]["BDXMDM"].ToString().Trim() + "\"");
                mbStr.Append(",");
                mbStr.Append("\"BDXMMC\":\"" + da.Tables[0].Rows[i]["BDXMMC"].ToString().Trim() + "\"");
                mbStr.Append("}");
                if (i < da.Tables[0].Rows.Count - 1)
                {
                    mbStr.Append(",");
                }
            }
            mbStr.Append("]}");
            list.Add(faStr.ToString());
            list.Add(mbStr.ToString());

            string s = string.Join("", list.ToArray());
            context.Response.Write(s);
        }
        else if (action == "Selxmmc")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string xmdm = context.Request.QueryString["xmdm"];
            string sql = "select XMMC from TB_XMXX WHERE XMDM='" + xmdm + "'";
            sql += " union";
            sql += " select ZYMC from TB_JHZYML WHERE ZYDM='" + xmdm + "'";
            DataSet Ds = bll.Query(sql);
            context.Response.Write(Ds.Tables[0].Rows[0][0].ToString());
        }
        else if (action == "XMFLXZ")
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            StringBuilder Str = new StringBuilder();
            string sql = " SELECT XMDM BM,XMMC MC FROM TB_XMXX WHERE XMLX='13'  ORDER BY XMBM ";
            DataSet da = bll.Query(sql);
            if (da.Tables[0].Rows.Count > 0)
            {
                Str.Append("[");
                for (int i = 0; i < da.Tables[0].Rows.Count; i++)
                {
                    Str.Append("{");
                    Str.Append("\"id\":\"" + da.Tables[0].Rows[i]["BM"].ToString() + "\"");
                    Str.Append(",");
                    Str.Append("\"text\":\"" + da.Tables[0].Rows[i]["MC"].ToString().Trim() + "[" + da.Tables[0].Rows[i]["BM"].ToString() + "]" + "\"");
                    if (i < da.Tables[0].Rows.Count - 1)
                    {
                        Str.Append("},");
                    }
                    else
                    {
                        Str.Append("}]");
                    }
                }
            }
            context.Response.Write(Str.ToString());
        }
    }

    
    private string GetSl(string _reqstring)
    {

        return String.IsNullOrEmpty(_reqstring) ? "0" : Math.Round(double.Parse(_reqstring), 8).ToString();
    }

    public int getNum(char ch)
    {
        if (ch >= 'a' && ch <= 'z')
            return ch - 'a' + 1;
        if (ch >= 'A' && ch <= 'Z')
            return ch - 'A' + 1;
        else
            return -1;
    }

    public int f(String str)
    {
        char[] ch = str.ToCharArray();
        int ret = 0;
        for (int i = 0; i < ch.Length; i++)
        {
            ret *= 26;
            ret += getNum(ch[i]);
        }
        return ret;
    }

    /// <summary>
    /// 根据模板设置计算列
    /// </summary>
    /// <param name="_mbdm">模板代码</param>
    /// <param name="_mblx">模板类型3:编制 4批复 5分解 </param>
    /// <returns></returns>
    public string calcolsbytemplet(string _mbdm, string _mblx)
    {
        string Res = "{\"sheets\":[";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT * FROM TB_QQYJSCOL WHERE MBDM='" + _mbdm + "' AND MBLX='" + _mblx + "'";
        DataSet set = bll.Query(sql);
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            if (Res.Equals("{\"sheets\":["))
            {
                Res += "{\"name\":\"" + set.Tables[0].Rows[i]["SHEETNAME"] + "\",\"cols\":\"" + set.Tables[0].Rows[i]["COLS"] + "\"}";
            }
            else
            {
                Res += ",{\"name\":\"" + set.Tables[0].Rows[i]["SHEETNAME"] + "\",\"cols\":\"" + set.Tables[0].Rows[i]["COLS"] + "\"}";
            }
        }
        Res += "]}";
        return Res;
    }

    /// <summary>
    /// 根据公式隐藏模版
    /// </summary>
    /// <param name="_mbdm">模版代码</param>
    /// <param name="_userid">用户id</param>
    /// <param name="_mblx">模版类型0：填报,1：分解,2:批复</param>
    /// <param name="_nn">月份</param>
    /// <returns></returns>
    public string hidecolbyformula(string _mbdm, string _userid, string _mblx, string _nn, string _sheetnames)
    {
        string Res = "{\"sheets\":[";
        string jsdm = "";
        //获取角色：12323123,3123323,21212121
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT * FROM TB_JSYH WHERE USERID='" + _userid + "'";
        DataSet set = bll.Query(sql);
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            if (i == 0)
            {
                jsdm = set.Tables[0].Rows[i]["JSDM"].ToString();
            }
            else
            {
                jsdm += "," + set.Tables[0].Rows[i]["JSDM"].ToString();
            }
        }
        string[] arysheet = _sheetnames.Split(',');
        for (int i = 0; i < arysheet.Length; i++)
        {
            string sheetname = arysheet[i];
            if (Res.Equals("{\"sheets\":["))
            {
                Res += "{\"name\":\"" + sheetname + "\",\"cols\":\"" + GetDataList.YCLSZ(_mbdm, sheetname, jsdm, _mblx, _nn) + "\"}";
            }
            else
            {
                Res += ",{\"name\":\"" + sheetname + "\",\"cols\":\"" + GetDataList.YCLSZ(_mbdm, sheetname, jsdm, _mblx, _nn) + "\"}";
            }
        }
        Res += "]}";
        return Res;
    }

    public string getstr(string _str)
    {
        if (_str.IndexOf(',') >= 0)
        {
            return _str.Substring(0, _str.IndexOf(','));
        }
        else
        {
            return _str;
        }
    }

    /// <summary>
    /// 处理后台更新批复状态操作
    /// </summary>
    /// <param name="_jhfadm">计划方案代码</param>
    /// <param name="_mbdm">模版代码</param>
    /// <param name="_userid">用户id</param>
    /// <returns>response字符串</returns>
    public string dealrespondstatus(string _jhfadm, string _mbdm, string _userid)
    {
        string res = "";
        ArrayList sqls = new ArrayList();
        //删除指定计划方案和模版代码
        sqls.Add("DELETE FROM TB_PFMBZT WHERE JHFADM='" + _jhfadm + "' AND MBDM='" + _mbdm + "'");
        sqls.Add("INSERT INTO TB_PFMBZT(JHFADM,MBDM,USERID,SJ,ZT,SFCOPY) VALUES('" + _jhfadm + "','" + _mbdm + "','" +
            _userid + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','0')");
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //按递归顺序返回上级模版
        string mbs = bll.GetAllParMB(_mbdm);
        if (!mbs.Equals(""))
        {
            string[] smbs = mbs.Split(',');
            foreach (string strmbdm in smbs)
            {
                sqls.Add("DELETE FROM TB_PFMBZT WHERE JHFADM='" + _jhfadm + "' AND MBDM='" + strmbdm + "'");
            }
        }
        DbHelperOra.ExecuteSqlTran(sqls);
        res = "更新完成！";
        return res;
    }

    /// <summary>
    /// 获取校验公式以N_JYGS开头的数据，此数据意思是对比指定列数据是否对应，比如数据列有数据，名称列必须不能为空
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    public string getJygs(string MBDM)
    {
        string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID FROM dbo.TB_MBJYGS T where T.MBDM='" + MBDM + "' AND T.GS LIKE 'N_JYGS%'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet SET = BLL.Query(sql);
        string FhJygs = "";
        //如果存在N_JYGS开始的校验公式：
        if (SET.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < SET.Tables[0].Rows.Count; i++)
            {
                FhJygs += SET.Tables[0].Rows[i]["SHEETNAME"].ToString() + "|" + SET.Tables[0].Rows[i]["GS"].ToString();
                if (i < SET.Tables[0].Rows.Count - 1)
                {
                    FhJygs += "+";
                }
            }
            return FhJygs;

        }
        //如果不存在N_JYGS开始的校验公式：
        else
        {
            return "";
        }

    }

    /// <summary>
    /// 杀掉excel进程
    /// </summary>
    /// <param name="theApp">excelapp</param>
    private void killexcel(Excel.Application theApp)
    {
        int id = 0;
        IntPtr intptr = new IntPtr(theApp.Hwnd);
        System.Diagnostics.Process p = null;
        try
        {
            GetWindowThreadProcessId(intptr, out id);
            p = System.Diagnostics.Process.GetProcessById(id);
            if (p != null)
            {
                p.Kill();
                p.WaitForExit();
                p.Dispose();
            }
        }
        catch (Exception ex)
        {

        }
    }

    //生产方案模版校验：
    public string fambcheck(string hszxdm, string jhfadm)
    {
        string res = "";
        DataSet ds = GetDataList.JyPh(hszxdm, jhfadm);
        //如果校验出了问题：
        if (null != ds)
        {
            string sr = "";
            DataTable _table = ds.Tables[0];
            if (null != _table && _table.Rows.Count > 0)
            {
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    DataRow row = _table.Rows[i];
                    sr = "";
                    if (row != null)
                    {
                        if (res == "")
                        {
                            res = "{";
                        }
                        else
                        {
                            res += ",{";
                        }
                        for (int j = 0; j < _table.Columns.Count; j++)
                        {
                            if (sr == "")
                            {
                                sr = "\"" + _table.Columns[j].ColumnName + "\":\"" + row[j].ToString() + "\"";
                            }
                            else
                            {
                                sr += ",\"" + _table.Columns[j].ColumnName + "\":\"" + row[j].ToString() + "\"";
                            }
                        }
                        res += sr + "}";
                    }
                }
                res = "[" + res + "]";
            }
        }
        return res;
    }

    /// <summary>
    /// 根据JSDM，ZYDM判断是否显示书面承诺
    /// </summary>
    /// <param name="mbdm">模板代码</param>
    /// <param name="userid">用户ID</param>
    /// <returns>字符串“1”或“0”，1代表显示承诺书，0代表不显示承诺书</returns>
    public string isDisPlaySmcn(string mbdm, string userid)
    {
        string flag = "1";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT JSDM FROM TB_MBSJXZYH where JSDM in (select distinct J.JSDM from TB_USER U,TB_JSYH J where U.USER_ID=J.USERID and U.USER_ID='" + userid + "')";
        DataSet da = bll.Query(sql);
        DataSet ds = new DataSet();
        DataSet dd = new DataSet();
        if (da.Tables[0].Rows.Count > 0)//当前用户的角色在TB_MBSJXZYH.JSDM中(表示不受限角色）,则表示财务部门角色
        {
            //查询当前用户的作业代码
            string sqlZydm = "select ZYDM from TB_USER where USER_ID='" + userid + "'";
            ds = bll.Query(sqlZydm);

            //查询模板的作业代码
            string sql1 = "select ZYDM from TB_YSBBMB where MBDM='" + mbdm + "'";
            dd = bll.Query(sql1);

            if (ds.Tables[0].Rows.Count > 0 && dd.Tables[0].Rows.Count > 0)
            {
                //如果当前用户的作业代码!=各个模板的部门代码TB_YSBBMB.ZYDM,则不显示承诺书，如果=的话则提示承诺书
                if (ds.Tables[0].Rows[0]["ZYDM"].ToString() != dd.Tables[0].Rows[0]["ZYDM"].ToString())
                {
                    //显示承诺书返回1，不显示返回0
                    flag = "0";
                }
            }

        }
        return flag;
    }

    /// <summary>
    /// 为书面承诺界面赋值，已供用户选择
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="USER_ID">用户ID</param>
    /// <param name="FLAG">打回，上报，审核标识</param>
    /// <returns>书面承诺显示的内容，以‘|’区分</returns>
    public string getValue(string MBDM, string USER_ID, string FLAG)
    {
        string xmmc = ""; string zymc = ""; string user = "";
        string sql = "SELECT XMMC FROM XT_CSSZ WHERE XMFL='SPYJ'";  //同意，不同意，打回，提交上报  几个常用选项
        string McSql = "SELECT B.ZYMC FROM TB_YSBBMB A,TB_JHZYML B WHERE A.ZYDM=B.ZYDM AND A.MBDM='" + MBDM + "'";//部门名称
        string UserSql = "SELECT USER_NAME,PHONE FROM TB_USER WHERE USER_ID='" + USER_ID + "'";//填报人和审查人
        ArrayList list = new ArrayList();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();

        //switch (FLAG)
        //{
        //    case "0"://打回
        //        sql += " and XMMC='打回'";
        //        break;
        //    case "1"://上报
        //        sql += " and XMMC='提交上报'";
        //        break;
        //    case "2"://审批
        //        sql += " and XMMC='同意'";
        //        break;
        //}

        ds = bll.Query(sql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                xmmc += ds.Tables[0].Rows[i]["XMMC"].ToString().Trim();
                if (i < ds.Tables[0].Rows.Count - 1) { xmmc += "|"; }
            }
        }

        //xmmc = ds.Tables[0].Rows[0]["XMMC"].ToString().Trim();
        ds.Clear();

        ds = bll.Query(McSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                zymc += ds.Tables[0].Rows[i]["ZYMC"].ToString().Trim();
                if (i < ds.Tables[0].Rows.Count - 1) { zymc += "|"; }
            }
        }
        ds.Clear();

        ds = bll.Query(UserSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                user += ds.Tables[0].Rows[i]["USER_NAME"].ToString().Trim();
                user += "|";
                user += ds.Tables[0].Rows[i]["PHONE"].ToString().Trim();
                if (i < ds.Tables[0].Rows.Count - 1) { user += "`"; }
            }
        }
        ds.Clear();

        list.Add(xmmc);
        list.Add(zymc);
        list.Add(user);

        string str = string.Join(",", (string[])list.ToArray(typeof(string)));

        return str;
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}