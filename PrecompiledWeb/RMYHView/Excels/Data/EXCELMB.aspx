﻿<%@ page title="" language="C#" masterpagefile="~/Spread/WebExcel.master" autoeventwireup="true" inherits="Excels_Data_EXCELMB, App_Web_3hyblsze" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" Runat="Server">
    <script type="text/javascript" src="<%=Request.ApplicationPath%>/JS/MBDataLoader.js" ></script>
    <script type="text/javascript">

        //获取按钮权限
        function GetButtonQX() {
            if (ParamDic["MBSZ"] == "ExcelData") {
                $.ajax({
                    type: 'get',
                    url: 'InitMBHandle.ashx',
                    data: {
                        action: 'GetMBQX',
                        FADM: ParamDic["FADM"],
                        MBDM: ParamDic["MBDM"],
                        MBLB: ParamDic["MBLB"],
                        MBLX: ParamDic["MBLX"],
                        ISEXE: ParamDic["ISEXE"],
                        SFZLC: ParamDic["SFZLC"],
                        USERDM: ParamDic["USERDM"],
                        HSZXDM: ParamDic["HSZXDM"],
                        LCDM: ParamDic["LCDM"]
                    },
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result != "" && result != null) {
                            Btn = result;
                            QX = eval('({' + result + '})');
                        }
                    }
                });
            }
            //设置按钮权限
            SetButtonQX();
        }

        //设置按钮权限
        function SetButtonQX() {
            //设置页面按钮对应的div的显示和隐藏
            $("#ExcelData").attr("style", "display:inline");
            //启用模板设置页面按钮可用
            if (Btn != "" && Btn != undefined && Btn != null) 
            {
                var BtnID, ID;
                var display = "";
                var Arr = Btn.split(',');
                for (var i = 0; i < Arr.length; i++) 
                {
                    BtnID = Arr[i].split(':');
                    display = BtnID[1] == "1" ? false : true;
                    ID = BtnID[0].substring(1, BtnID[0].length - 1);
                    if (ID == "BDDK") {
                        $("#doImport").attr("disabled",display);
                    }
                    if (ID == "BDLC") {
                        $("#doExport").attr("disabled",display);
                    }
                    $("#" + ID).attr("disabled",display);
                }
            }
        }

//        /**
//        * Created by LI on 2017-05-17 .
//        * 请求服务器端json数据，在页面展示
//        */
//        function openJson(selected, IsExist) {
//            var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
//            var excelIo = new GC.Spread.Excel.IO();
//            var path = window.document.location.pathname.substring(1);
//            var Len = path.lastIndexOf("/");
//            var root = "/" + path.substring(0, Len + 1);
////            if (ParamDic["MBSZ"] == "ExcelData" && IsExist) {
////                root = root + "Excels/Data/";
////            }
////            else {
////                root = root + "Excels/ExcelMb/";
////            }
//            var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected + "&rand=" + Math.random();
//            var xhr = new XMLHttpRequest();
//            xhr.open('GET', url, true);
//            xhr.responseType = 'text';

//            xhr.onload = function (e) {
//                if (this.status == 200) {
//                    //设置自定义公式
//                    MySelfMethod();
//                    // get binary data as a response
//                    importJson(JSON.parse(this.response));

//                    //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
//                    //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
//                    if (ParamDic["MBSZ"] == "ExcelData") {
//                        //如果存在数据页的话，则执行下面操作
//                        if (IsExist) {
//                            //打开Excel之后的操作
//                            AfterOpen();
//                            dialogshow('已填报文件加载完成!');
//                        }
//                        else {
//                            //不存在执行下面操作
//                            AfterOpenMb();
//                            dialogshow('模板文件加载完成!');
//                        }
//                    }
//                    delSpacseValue(); //将模板单元格值为"" 设置为null
//                }
//            };

//            xhr.send();
//        }

        $(function () {
            easyuiload();
            GetButtonQX();
            $("#bardiv").attr("style", "width:140%;display:inline-table");
            //显示计划方案名称
            $("#DivTitle").html("  计划方案:【" + ParamDic["FAMC"] + "】");
            //加载Excel数据页
            debugger;
            InitData();
            var activeSheet = spread.getActiveSheet();
            activeSheet.setColumnVisible(0, false);
            activeSheet.setRowVisible(0, false);
            easyuidisLoad();
        })
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div id="ExcelData">
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doImport"
                disabled="disabled"
                title="@toolBar.importFile@">
            <span class="fa fa-folder-open fa-2x"></span>
            </br>
            <span>本地打开</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="doExport"
                disabled="disabled"
                title="@toolBar.export.title@">
            <span class="fa icon-DC fa-2x"></span>
            </br>
            <span>本地保存</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="HQZXMB" onclick="OpenMbFile()"
            disabled="disabled"
            title="@toolBar.HQZXMB@">
            <span class="fa icon-HQZXMB fa-2x"></span>
                </br>
        <span>获取最新模板</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SCSJ" onclick="InitTrCc()"
                disabled="disabled"
                title="@toolBar.SCSJ@">
            <span class="fa icon-SCSJ fa-2x"></span>
            </br>
        <span>生成数据</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="ZJSJ" onclick="InsertTrCc()"
                disabled="disabled"
                title="@toolBar.ZJSJ@">
            <span class="fa icon-ZJSJ fa-2x"></span>
            </br>
        <span>追加数据</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="JSBB" onclick="ZoneCal()"
                disabled="disabled"
                title="@toolBar.JSBB@">
            <span class="fa icon-QYJS fa-2x"></span>
                </br>
        <span>计算报表</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="QQYJS" onclick="AllZoneCal()"
                disabled="disabled"
                title="@toolBar.QQYJS@">
            <span class="fa icon-QQYJS fa-2x"></span>
            </br>
        <span>全区域计算</span>
        </button>
                            
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="HQHLSX" onclick="GetRowCol()"
                disabled="disabled"
                title="@toolBar.HQHLSX@">
            <span class="fa fa-table fa-2x"></span>
                </br>
        <span>获取行列属性</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="INSERTBDH" onclick="InsertRow()"
                disabled="disabled"
                title="@toolBar.INSERTBDH@">
            <span class="fa fa-align-justify fa-2x"></span>
            </br>
        <span>插入变动行</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DELBDH" onclick="DelBdRow()"
                disabled="disabled"
                title="@toolBar.DELBDH@">
            <span class="fa fa-eraser fa-2x"></span>
            </br>
        <span>删除变动行</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="BC" onclick="SaveUpClick()"
                disabled="disabled"
                title="@toolBar.BC@">
            <span class="fa fa-floppy-o fa-2x"></span>
            </br>
        <span>保存</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SB" onclick="DataUp()"
                disabled="disabled"
                title="@toolBar.SB@">
            <span class="fa icon-SB fa-2x"></span>
                </br>
        <span>上报</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="SP" onclick="DataApp()"
                disabled="disabled"
                title="@toolBar.SP@">
            <span class="fa icon-SP fa-2x"></span>
                </br>
        <span>审批</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DH" onclick="DataBack()"
                disabled="disabled"
                title="@toolBar.DH@">
            <span class="fa icon-DH fa-2x"></span>
            </br>
        <span>退回</span>
        </button>
        <button type="button" class="btn btn-default btn-toolbar localize-tooltip" id="DY" onclick="Print()"
                disabled="disabled"
                title="@toolBar.DY@">
            <span class="fa fa-print fa-2x"></span>
                </br>
        <span>打印</span>
        </button>
    </div>

     <input type="hidden" id="HidDH" />
     <input type="hidden" id="HidJS" />
     <input type="hidden" id="HidTJ" />
     <input type="hidden" id="HidBC" />
</asp:Content>