﻿<%@ page language="C#" autoeventwireup="true" inherits="word, App_Web_o32qxxac" %>

<%@ Register Assembly="PageOffice, Version=3.0.0.1, Culture=neutral, PublicKeyToken=1d75ee5788809228"
    Namespace="PageOffice" TagPrefix="po" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>最简单打开保存Excel文件</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script src="../JS/day.js" type="text/javascript"></script>
</head>
<body>
    <script type="text/javascript">
        function Save() {
            document.getElementById("PageOfficeCtrl1").WebSave();
        }
        function AfterDocumentOpened() { 
        
        }
        function OpenExcel() {
            debugger;
            //document.getElementById("PageOfficeCtrl1").ServerPage = "pageoffice/server.aspx"; //设置服务器页面
            document.getElementById("PageOfficeCtrl1").JsFunction_OnExcelCellClick = "OnCellClick()"; //点击Excel中的指定的单元格，调用js函数OnCellClick()弹出一个可以选择部门的对话框
            //document.getElementById("PageOfficeCtrl1").WebOpen("../doc/test.xls", "xlsNormalEdit", "aaa");//打开文件
        }
        function OnCellClick() {
            debugger;
            //获取选中的excel区域
            var Address = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
            $("#<%=hidAddress.ClientID %>").val(Address);

        
        }
        $(document).ready(function () { OpenExcel(); });    
    </script>
    <form id="form1" runat="server">
    <input type="hidden" id="hidAddress" runat="server" />
    <div style=" width:auto; height:700px;">
        <po:PageOfficeCtrl ID="PageOfficeCtrl1" runat="server"  >
        </po:PageOfficeCtrl>
    </div>
    </form>
</body>
</html>
