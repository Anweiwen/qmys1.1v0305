﻿<%@ page language="C#" autoeventwireup="true" inherits="SIS_EditExcel, App_Web_bcedtavn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>编辑EXCEL模板</title>
     <link href="../../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../../CSS/style.css" type="text/css" rel="stylesheet" />
    <script src="../../JS/jquery-1.3.1.min.js" type="text/javascript"></script>
     <script src="../../JS/mainScript.js" type="text/javascript"></script>

     <script type="text/javascript">
         function AddData() 
         {
             var type = $("#hidType").val();
             var f = false;
             if (type == "add") {
                 var s = SIS_EditExcel.AddData($("#xname").val()).value;
                 if(s.split(',')[0]=="True")
                 {
                     f=true;
                 }
                 $("#hidID").val(s.split(',')[1]);
             } else if (type == "update") {
                 f = SIS_EditExcel.UpdateData($("#hidID").val(), $("#xname").val()).value;
             }

             if (f == true) {
                 alert("操作成功");
                 window.returnValue =$("#hidID").val() +","+$("#xname").val();
                 this.close();
             } else {
                 alert("操作失败");
                 window.returnValue ="" +","+"";
             }
            
         }

         function cancelt() {
             window.returnValue = false;
             this.close();

         }
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <table style ="height :80px;">
        <tr >
          <td>
             项目名称：
          </td>
           <td>
             <input type="text" id="xname" runat ="server" />
          </td>
        </tr>
        <tr align ="center">
          <td colspan ="2">
          <input type="button" value="确定" onclick ="AddData()" class ="button" />
          <input type="button" value="取消" onclick="cancelt()" class ="button" />
          </td>
        </tr>
      </table>
      <input type="hidden" id="hidType" runat ="server" />
      <input type="hidden" id="hidID" runat ="server" />
      <input type="hidden" id="hidName" runat ="server" />
    </div>
    </form>
</body>
</html>
