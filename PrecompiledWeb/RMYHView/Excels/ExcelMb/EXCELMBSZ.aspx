<%@ page language="C#" autoeventwireup="true" inherits="Excels_ExcelMb_EXCELMBSZ1, App_Web_bcedtavn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>基础模版填报</title>
    <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
    <link href="../../Content/themes/icon.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style.css" />
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
        
    </style>
    <script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <%--<script src="../../JS/Jpageoffice.js" type="text/javascript"></script>--%>
     <script src="../../JS/Main.js" type="text/javascript"></script>
    <script type="text/javascript">

        function InitMb() {
            var lx=$("#SelMBLX").val();
            var mc = $("#TxtMKM").val();
            var zq = $("#SelZQ").val();
         $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data:{
                    action: "InitMb",
                     lxs:lx,
                     mcs: mc,
                     mbzq:zq
                },
                async: true,
                cache:false,
                success: function (result) {
                 $("#DataGrdMb").datagrid({loadFilter:pagerFilter}).datagrid('loadData',eval(result));
                },
                error: function () {
                    alert("错误!");
                }
            });
        }

        function InitFalb(){
        $.ajax({
                type: 'get',
                url: 'ExcelData.ashx?action=InitFalb',
                async: true,
                success: function (result) {
                    $("#SelFalb").empty();
                    $("#SelFalb").append(result);
                    refresh();
                    InitFa();
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
        function pagerFilter(data) {
            if (data) {
                if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                    data = {
                        total: data.length,
                        rows: data
                    }
                }

                var dg = $(this);
                var opts = dg.datagrid('options');
                var pager = dg.datagrid('getPager');
                pager.pagination({
                    onSelectPage: function (pageNum, pageSize) {
                        opts.pageNumber = pageNum;
                        opts.pageSize = pageSize;
                        pager.pagination('refresh', {
                            pageNumber: pageNum,
                            pageSize: pageSize
                        });
                        dg.datagrid('loadData', data);
                    }
                });
                if (!data.originalRows) {
                    data.originalRows = (data.rows);
                }
                var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
                var end = start + parseInt(opts.pageSize);
                data.rows = (data.originalRows.slice(start, end));
                return data;
            }
        }

        $(function () {
            BindMBLX();
            BindMBZQ();
            InitMb();
            $('#DataGrdMb').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                singleSelect: true,
                striped: true,
                pagination: true,
                pageSize: 10,
                columns: [[
                    { field: 'MBDM', title: '模版代码', hidden: true },
                    { field: 'MBBM', title: '模板编码', width: 120 },
                    { field: 'MBLX', title: '模版类型ID', hidden: true },
					{ field: 'MBMC', title: '模版名称', width: 200 },
					{ field: 'MBWJ', title: '模版文件', hidden: true },
					{ field: 'XMMC', title: '模板周期', width: 100 },
				]],
                onHeaderContextMenu: function (e, field) {
                    e.preventDefault();
                    if (!cmenu) {
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                },
                onDblClickRow: function (rowIndex, rowData) {
                    //OpenMb();
                    //OpemWebExcel("EXCELMBSZ", rowData.MBDM, rowData.MBMC, rowData.MBWJ);
                    $("#HidMBDM").val(rowData.MBDM);
                    var url = "MBSZ=EXCELMBSZ&MBDM=" + rowData.MBDM + "&MBWJ=" + rowData.MBWJ + "&MBMC=" + rowData.MBMC;
                    //调用Main.js 文件里的OpemWebExcel
                    //OpemWebExcel(rowData.MBDM, rowData.MBMC, url)
                    OpenWebExcel(rowData.MBDM, rowData.MBMC, url)
                }

            });
        });

        var cmenu;
        function createColumnMenu() {
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function (item) {
                    if (item.iconCls == 'icon-ok') {
                        $('#DataGrdMb').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#DataGrdMb').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
            });
            var fields = $('#DataGrdMb').datagrid('getColumnFields');
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var col = $('#DataGrdMb').datagrid('getColumnOption', field);
                if (col.hidden) {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-empty'
                    });
                } else {
                    cmenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-ok'
                    });
                }
            }
        }

        function BindMBLX() {
            var rtn = Excels_ExcelMb_EXCELMBSZ1.GetMBLX().value;
            $("#SelMBLX").html(rtn);
        }
        function BindMBZQ() {
            var rtn = Excels_ExcelMb_EXCELMBSZ1.GetMBZQ().value;
            $("#SelZQ").html(rtn);
        }


        function OpenMb() {    
//            var userdm = $("#<%=HidUserdm.ClientID %>").val();
//            var yy = $("#<%=HidYy.ClientID %>").val();
//            var hszxdm = $("#<%=HidHszxdm.ClientID %>").val();
//            var selected = $("#DataGrdMb").datagrid('getSelected');
//            var url = "pageoffice://|" + geturlpath() + "ExcelMb.aspx?userdm=" + userdm + "&yy=" + yy + "&hszxdm=" + hszxdm + "&mbmc=" + encodeURIComponent(encodeURIComponent(selected.MBMC)) + "&mbdm=" + encodeURIComponent(selected.MBDM) + "&mbwj=" + encodeURIComponent(selected.MBWJ) + "";
            //            window.location.href = url + "|||";
            var selected = $('#DataGrdMb').datagrid('getSelected');
            if (selected != null) {
                $("#HidMBDM").val(selected.MBDM);
                var url = "MBSZ=EXCELMBSZ&MBDM=" + selected.MBDM + "&MBWJ=" + selected.MBWJ + "&MBMC=" + selected.MBMC;
                OpemWebExcel(selected.MBDM, selected.MBMC, url)
            } else {
                prompt("请先选择行！！！");
            }
        }

        function OpenWebExcel(ID, MC, paramStr) {
            var path = window.document.location.pathname.substring(1);
            var Len = path.lastIndexOf("/");
            var root = "/" + path.substring(0, Len + 1);
            root = root + "MBSZ.aspx?" + paramStr + "&rand=" + Math.random();
            var content = '<iframe scrolling="auto" id="' + ID + '"  frameborder="0"  src="' + root + '" style="width:100%;height:100%;"></iframe>';
            $('#tt').tabs('add', {
                title: MC,
                content: content,
                closable: true
            });
        }
       
    </script>
</head>
<body>
   <div class="easyui-tabs" id="tt" style="width:100%;height:100%;">
   <div title="模版定义" style="width:100%;height:100%;fit:true">
    <form id="form1" runat="server" class="easyui-layout" style="width: 100%; height: 100%;">
     
    <div region="north" border="true" title="模版填报" style="height: 60px; padding: 2px;
        background: #B3DFDA;">
        模板类型：<select id="SelMBLX" onchange="InitMb()" style="width:150px;"></select>&nbsp;&nbsp;
        模版周期：<select id="SelZQ" onchange="InitMb()" style="width:100px;"></select>&nbsp;
        模版名称：<input id="TxtMKM" style="width: 120px" type="text" />&nbsp;
       <input type="button" class="button5"  value="查询" onclick ="InitMb()" />
        <a href="#" id="BtnOpenMb" class="easyui-linkbutton" style="margin: 0px 40px 0px 40px;"
                data-options="iconCls:'icon-ok'" onclick="OpenMb();">打开模版</a> 
    </div>
        <div id="DivCenter" region="center" title="模版">
        <div id="DivMb" style="width: 100%; height: 100%; display: block;">
            <table id="DataGrdMb" style="width: 100%; height: 100%;">
            </table>
        </div>
       </div>
       <%-- <div region="south" split="true" style="height: 40px; padding: 2px;">
            
        </div>  --%>
        <input id="HidUserdm" type="hidden" runat="server" />
        <input id="HidHszxdm" type="hidden" runat="server" />
        <input id="HidYy" type="hidden" runat="server" /> 
        <input id="HidMBDM" type="hidden" runat="server" /> 
        <asp:Button ID="Button1" runat="server" Text="Button" />
     
    </form>
   </div>
   </div>
   
</body>
</html>
