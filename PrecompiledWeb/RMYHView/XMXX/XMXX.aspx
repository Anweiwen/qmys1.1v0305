﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="XMXX_XMXX, App_Web_g4zafhae" title="项目 Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      <table id="tbbutton"> 
       <tr>
        <td><input type="button" class="button5" onclick="jsAddData(false)" value="添加同级" /></td>  
        <td><input type="button" class="button5" onclick="jsAddData(true)" value="添加下级" /></td>
        <td><input type="button" class="button5" onclick="TreeZK(this),expandAll()" value="完全展开" /></td> 
        <td><input type="button" class="button5" onclick="TreeSQ(),collapseAll()" value="完全收起" /></td>
        <td><input type="button" class="button5" onclick="xmzy()" value="项目转移" /></td>
        <td><input type="button" class="button5" onclick="getlist('','',''),jsonTree()" value="刷新" /></td>      
        <td><input type="button" class="button5" value="删除" onclick="Del()" /></td>
        <td><input type="button" class="button5" value="取消删除" onclick="Cdel()" /></td>
        <td><input type="button" class="button5" value="保存" onclick="Saves()" /></td>
        <td> &nbsp; &nbsp;&nbsp;名称：<input id="TxtName" class="easyui-searchbox" type="text" /></td>
        <td><input type="button" class="button5" value="查询" onclick="getResult('','','','','','')" /></td> 
       </tr>  
     </table>   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">            
<div class="easyui-layout" style="width:100%;height:100%;" fit="true">
	<div id="p" data-options="region:'west'" title="项目目录" style="width:15%;padding:10px">
		<ul id="tt" class="easyui-tree"> 
        
	</div>
    <div data-options="region:'center'" title="项目设置">
     
    <div  style="width:100%;height: 100%; overflow: auto" id="divTreeListView"></div>   
    <input type="hidden" id="hidindexid" value="XMDM" />
    <input type="hidden"   id="hidcheckid" />
    <input type="hidden"   id="hideeasyui" />
    <input type="hidden" id="hidNewLine" />  
    <input type="hidden" id="HidTrID" />  
 
    <div id="dd" class="easyui-window" closed="true" data-options="
				    iconCls: 'icon-save',
				    toolbar: [{
					    text:'转移到同级',
					    iconCls:'icon-add',
					    handler:function(){
						    jsNewAddData(false)
					    }
				    },'-',{
					    text:'转移到下级',
					    iconCls:'icon-add',
					    handler:function(){
						    jsNewAddData(true)
					    }
				    },'-',{
					    text:'完全展开',
					    iconCls:'icon-add',
					    handler:function(){
						    TreeZK(this)
					    }
				    } ,'-',{
					    text:'保存',
					    iconCls:'icon-save',
					    handler:function(){
						    newSave()
					    }
				    }],
				    buttons: [{
					    text:'Ok',
					    iconCls:'icon-ok',
					    handler:function(){
						    alert('ok');
					    }
				    },{
					    text:'Cancel',
					    handler:function(){
						    alert('cancel');
					    }
				    }]
			    ">
    </div>         
      
    </div>  
    <script type="text/javascript">
        var ob;
        var gif = -1;
        var bool = null;
        var jpg = null;
        var xmdm = null;
        var fbbm = " ";
        var xmbm = null;
        var xmmc = null;
        var par = null;
        var ccjb = null;
        var xmmcex = null;
        var content = null;
        var treenode = null;
        var isTrue = null;
        var onsel = 0;
        var nodeId = "";
        var pareNode = "";
        var arr = new Array();

        function jsUpdateData(objid, objfileds, objvalues, xmdm, xmbm, xmmc, bool) {
            if (bool != null) {
                var rtn = XMXX_XMXX.AddData(objid, objfileds, objvalues, xmdm, xmbm, xmmc, bool).value;
            } else {
                var rtn = XMXX_XMXX.UpdateData(objid, objfileds, objvalues, xmbm, xmmc, bool).value;
            }
            if (rtn != "" && rtn.substring(0, 1) == "0")
                alert(rtn.substring(1));
            jsonTree();
        }

        function jsDeleteData(obj) {
            var rtn = XMXX_XMXX.DelData(obj).value;
            jsonTree();
            $.messager.show({
                title: '温馨提示',
                msg: rtn,
                timeout: 1000,
                showType: 'slide'
            });
        }

        function getlist(objtr, objid, intimagecount) {
            var rtnstr = XMXX_XMXX.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "") {
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            }
            else {
                $("#" + objtr).after(rtnstr);
            }
            Bind();

            bool = null;
            jpg = null;
            xmdm = null;
            xmbm = null;
            xmmc = null;
            par = null;
            ccjb = null;
            xmmcex = null;
            gif = -1;
            fbbm = " ";
            content = null;
            isTrue = null;
            treenode = null;
            $("#HidTrID").val("");
            $("#hidcheckid").val("");

        }

        function getResult(dm, objtr, objid, intimagecount, strarr, readonlyfiled) {
            if ($("#TxtName").val() == "") {
                getlist('', '', '');
                return;
            }
            var rtnstr = XMXX_XMXX.getResult("XMDM", objtr, objid, $("#TxtName").val(), "", "").value;
            if (objtr == "" && objid == "" && intimagecount == "") {
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            }
            else {
                $("#" + objtr).after(rtnstr);
            }
            Bind();
        }

        function getRes(objtr, objid, intimagecount) {
            var rtnstr = XMXX_XMXX.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "") {
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            } else {
                $("#" + objtr).after(rtnstr);
            }
            Bind();
        }

        function newgetlist(objtr, objid, intimagecount) {

            var rtnstr = XMXX_XMXX.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "") {
                document.getElementById("dd").innerHTML = rtnstr;
            }
            else
                $("#" + objtr).after(rtnstr);
        }

        function jsonTree() {
            var rtn = XMXX_XMXX.GetChildJson("");
            var dj = null;
            $('#tt').tree({
                lines: true,
                checkbox: true,
                cascadeCheck: false,
                onBeforeExpand: function (node) {
                    var childrens = $('#tt').tree('getChildren', node.target);

                    if (childrens == false) {
                        nodeId = node.id;
                        pareNode = node.target;
                        var res = XMXX_XMXX.GetChildJson(node.id);
                        var Data = eval("(" + res.value + ")");
                        //var selected = $('#tt').tree('getSelected');
                        $('#tt').tree('append', {
                            parent: node.target,
                            data: Data
                        });
                    }
                },
                onExpand: function (node) {
                },
                onCollapse: function (node) {

                },
                onClick: function (node) {
                    // if ($('#tt').tree('isLeaf', node.target)) {//判断是否是叶子节点
                    $("#HidTrID").val("");
                    $("#hidcheckid").val("");
                    xmdm = node.id;
                    xmmc = node.text;
                    par = node.attributes.par;
                    ccjb = node.attributes.ccjb;
                    xmbm = node.attributes.xmbm;
                    fbbm = node.attributes.xmbm.substr(0, node.attributes.xmbm.length - 4);
                    subxmbm = node.attributes.xmbm.substr(node.attributes.xmbm.length - 4);
                    xmmcex = node.attributes.xmmcex;
                    treenode = node;
                    onsel = 0;
                    var rtnstr = XMXX_XMXX.getResult('XMDM', '', node.id, '', '0', '').value;
                    document.getElementById("divTreeListView").innerHTML = rtnstr;
                    Bind();
                }

            });
            if (rtn.value != "") {
                var Data = eval("(" + rtn.value + ")");
                $("#tt").tree("loadData", Data);
            }
        }

        function collapseAll() {
            $('#tt').tree('collapseAll');
        }
        function expandAll() {
            $('#tt').tree('expandAll');
        }

        function jsAddData(flag) {

            if (jpg != null && jpg.indexOf("/Images/Tree/new.jpg") != -1) {
                tishi("请选择已有的数据行添加！！！");
                return;
            }
            if (flag && $("#hidcheckid").val() == "") {
                tishi("请选择父节点");
                return;
            }
            else {
                if (gif != -1 && flag) {
                    ob.click(); //添加下级前展开节点
                    gif = -1;
                }
                //找到页面同级或下级的最大xmdm代码
                var maxbm = MaxBm(xmdm, par, flag);
                jsAddNewData(flag, maxbm, treenode);
            }
            bool = flag;
        }

        //添加数据
        function jsAddNewData(flag, maxbm, treenode) {
            var Len;
            $("#HidTrID").val() == "" ? Len = 0 : Len = $("#" + $("#HidTrID").val()).find("img[src$='white.gif']").length;
            //var Len = $("#" + $("#HidTrID").val()).find("img[src$='white.gif']").length;
            //if ($("#hidNewLine").val() == "")

            if (treenode != null && onsel == 0) {
                $("#hidNewLine").val(XMXX_XMXX.AddNewData(Len, treenode.id, treenode.attributes.xmbm, treenode.text, treenode.attributes.par, treenode.attributes.ccjb, treenode.attributes.xmmcex, flag, maxbm, "yes").value);
            } else {
                $("#hidNewLine").val(XMXX_XMXX.AddNewData(Len, xmdm, xmbm, xmmc, par, ccjb, xmmcex, flag, maxbm, "no").value);
            }
            var rtnstr = $("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', "");
            if ($("#HidTrID").val() == "") {
                $("#divTreeListView table").append(rtnstr);
            }
            else {
                if (flag) {//添加下级
                    var td = $("#divTreeListView td[name=tdPAR]").filter(function () {
                        return this.innerHTML == "" + xmdm + ""
                    }).length;
                    if (td == 0) {//下级不存在，直接在选中行后添加
                        $("#" + $("#HidTrID").val()).after(rtnstr);
                        arr.push(xmdm);
                    } else {
                        $("#divTreeListView td[name=tdPAR]").filter(function () {
                            return this.innerHTML == "" + xmdm + ""
                        }).parent().last().after(rtnstr);
                        arr.push(xmdm);
                    }
                } else {//添加同级
                    $("#divTreeListView td[name=tdPAR]").filter(function () {
                        return this.innerHTML == "" + par + ""
                    }).parent().last().after(rtnstr);

                    arr.push(xmdm);
                }
            }
            Bind();
        }

        function TreeZK(obj) {
            var tr = null;
            var tableid = $(obj).parent().parent().parent().parent().attr('id');
            tableid == "tbbutton" ? tr = $("#divTreeListView img[src$='tplus.gif']") : tr = $("#dd img[src$='tplus.gif']");
            // var tr = $("#divTreeListView img[src$='tplus.gif']");
            for (var i = 0; i < tr.length; ) {
                var trid = tr[i].parentNode.parentNode.id;
                ToExpand(tr[i], trid);
                i = 0;
                tableid == "tbbutton" ? tr = $("#divTreeListView img[src$='tplus.gif']") : tr = $("#dd img[src$='tplus.gif']");
                //   tr = $("#divTreeListView img[src$='tplus.gif']");
            }
        }

        function TreeSQ() {
            var tr = $("#divTreeListView img[src$='tminus.gif']");
            for (var i = 0; i < tr.length; i++) {
                var trid = tr[i].parentNode.parentNode.id;
                ToExpand(tr[i], trid);
                var xmdm = $("#" + trid + " td[name^='tdXMDM']").html();
                //                if (xmdm.length == 2) {
                //                    ToExpand(tr[i], trid);
                //                }
            }
        }

        function xmzy() {
            if ($("#hidcheckid").val() == "") {
                tishi("请先选择需要转移的项目！！！");
            } else {
                //var rtnstr = XMXX_XMXX.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
                var res = XMXX_XMXX.BeUsed($("#hidcheckid").val()).value;
                if (res == "1") {
                    tishi("项目已经使用，不能转移！");
                } else {
                    $('#dd').dialog({
                        title: '项目转移',
                        width: 950,
                        height: 400,
                        closed: false,
                        cache: false,
                        modal: true,
                        content: $.ajax({
                            url: "XMXX.aspx", //这里是静态页的地址
                            type: "GET", //静态页用get方法，否则服务器会抛出405错误
                            success: function (data) {
                                // $("#matable").clone(true).appendTo($("#dd")).attr("id", "newtable");   //克隆页面内容到指定元素下
                                newgetlist('', '', '');
                                isTrue = null;
                                //$("#matable").attr("id", "newtable");
                                //$("#dd table td input[class='button5']").attr("onclick", "newselects(this)");
                            }
                        })
                    });
                }
            }
        }

        function ToExpand(obj, id) {
            var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
            var trs;
            if (obj.src.indexOf("tminus.gif") > -1) {
                obj.src = "../Images/Tree/tplus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                //            trs = $("#divTreeListView tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lminus.gif") > -1) {
                obj.src = "../Images/Tree/lplus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lplus.gif") > -1) {
                obj.src = "../Images/Tree/lminus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
            else if (obj.src.indexOf("tplus.gif") > -1) {
                obj.src = "../Images/Tree/tminus.gif";
                divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                var istoload = true;
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                        istoload = false;
                    }
                }
                if (istoload == true) {
                    var XMDM = $("tr[id=" + id + "] td[name='tdXMDM']").html();
                    getRes(id, XMDM, $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                    divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                    for (var i = 0; i < trs.length; i++) {
                        if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                            trs[i].style.display = "";
                        }
                    }
                }
            }

        }

        function Saves() {
            var objnulls = $("[ISNULLS=N]");
            var strnullmessage = "";
            for (i = 0; i < objnulls.length; i++) {
                if (objnulls[i].value.trim() == "") {
                    strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                    tishi(strnullmessage);
                    return;
                }
            }
            var E = $("#divTreeListView img[src$='edit.jpg']");
            var D = $("#divTreeListView img[src$='delete.gif']");
            var N = $("#divTreeListView img[src$='new.jpg']");
            var NewStr = "", UpdateStr = "", DelStr = "", FBBM = "", SUBXMBM = "", XMBM = "", XMMC = "", PAR = "", YJDBZ = "", SYBZ = "", CCJB = "", XMLX = "", XMMCEX = "";
            if (E.length == 0 && D.length == 0 && N.length == 0) {
                tishi("您没有新增、修改或者删除的项！");
                return;
            }

            //拼接新增的相关信息字符串
            for (var i = 0; i < N.length; i++) {
                var trid = N[i].parentNode.parentNode.id;
                if (trid != "") { //$.trim(content)
                    FBBM = $("tr[id=" + trid + "] td[name^=tdFBBM]").html();
                    SUBXMBM = $("tr[id=" + trid + "] td input[name^=txtSUBXMBM]").val();
                    XMMC = $("tr[id=" + trid + "] td input[name^=txtXMMC]").val();
                    CCJB = $("tr[id=" + trid + "] td[name^=tdCCJB]").html();
                    PAR = $.trim($("tr[id=" + trid + "] td[name^=tdPAR]").html());
                    XMMCEX = $.trim($("tr[id=" + trid + "] td[name^=tdXMMCEX]").html());
                    YJDBZ = $("tr[id=" + trid + "] td input[name^=chkYJDBZ]")[0].checked ? "1" : "0";
                    SYBZ = $("tr[id=" + trid + "] td input[name^=chkSYBZ]")[0].checked ? "1" : "0";
                    XMLX = $("tr[id=" + trid + "] td select[name^=selXMLX]").val();
                    var Bm = FBBM == "" ? SUBXMBM : (FBBM + SUBXMBM);
                    var modMc = XMMCEX == "" ? XMMC : (XMMCEX + "\\" + XMMC);
                    if (NewStr == "") {
                        NewStr = Bm + "|" + XMMC + "|" + PAR + "|" + YJDBZ + "|" + SYBZ + "|" + CCJB + "|" + XMLX + "|" + modMc;
                    }
                    else {
                        NewStr = NewStr + "," + Bm + "|" + XMMC + "|" + PAR + "|" + YJDBZ + "|" + SYBZ + "|" + CCJB + "|" + XMLX + "|" + modMc;
                    }
                }
            }
            //拼接修改的字符串
            for (var i = 0; i < E.length; i++) {
                var trid = E[i].parentNode.parentNode.id;
                if (trid != "") {
                    XMDM = $('tr[id=' + trid + '] td[name^=tdXMDM]').html();
                    FBBM = $("tr[id=" + trid + "] td[name^=tdFBBM]").html();
                    SUBXMBM = $("tr[id=" + trid + "] td input[name^=txtSUBXMBM]").val();
                    XMMC = $("tr[id=" + trid + "] td input[name^=txtXMMC]").val();
                    CCJB = $("tr[id=" + trid + "] td[name^=tdCCJB]").html();
                    PAR = $.trim($("tr[id=" + trid + "] td[name^=tdPAR]").html());
                    XMMCEX = $.trim($("tr[id=" + trid + "] td[name^=tdXMMCEX]").html());
                    YJDBZ = $("tr[id=" + trid + "] td input[name^=chkYJDBZ]")[0].checked ? "1" : "0";
                    SYBZ = $("tr[id=" + trid + "] td input[name^=chkSYBZ]")[0].checked ? "1" : "0";
                    XMLX = $("tr[id=" + trid + "] td select[name^=selXMLX]").val();
                    var modMc = XMMCEX.lastIndexOf('\\') > -1 ? (XMMCEX.substr(0, XMMCEX.lastIndexOf('\\')) + "\\" + XMMC) : XMMC;
                    if (UpdateStr == "") {
                        UpdateStr = XMDM + "|" + (FBBM + SUBXMBM) + "|" + XMMC + "|" + PAR + "|" + YJDBZ + "|" + SYBZ + "|" + CCJB + "|" + XMLX + "|" + modMc;
                    }
                    else {
                        UpdateStr = UpdateStr + "," + XMDM + "|" + (FBBM + SUBXMBM) + "|" + XMMC + "|" + PAR + "|" + YJDBZ + "|" + SYBZ + "|" + CCJB + "|" + XMLX + "|" + modMc;
                    }
                }
            }
            //拼接删除的相关信息的字符串
            for (var j = 0; j < D.length; j++) {
                var trid = D[j].parentNode.parentNode.id;
                if (trid != "") {
                    XMDM = $('tr[id=' + trid + '] td[name^=tdXMDM]').html();
                    if (DelStr == "") {
                        DelStr = XMDM;
                    }
                    else {
                        DelStr = DelStr + "," + XMDM;
                    }
                }
            }
            var rtn = XMXX_XMXX.UpdateORDel(NewStr, UpdateStr, DelStr).value;
            if (rtn[0] > 0) {
                tishi("保存成功！");
            }
            else {
                tishi("保存失败！");
            }
            var rtnstr = "";
            if (treenode == null) {
                rtnstr = XMXX_XMXX.LoadList('', '', '', '').value;
            } else {
                rtnstr = XMXX_XMXX.getResult('XMDM', '', treenode.id, '', '0', '').value;
            }

            document.getElementById("divTreeListView").innerHTML = rtnstr;
            Bind();

            if (rtn[1] != "") {
                var newData = eval("(" + rtn[1] + ")"); //添加操作之后同步easyui tree的树
                for (var j = 0; j < newData.length; j++) {
                    var node = $('#tt').tree('find', newData[j].attributes.par);
                    if (node != null && treenode != null) {
                        $('#tt').tree('append', {
                            parent: node.target,
                            data: newData[j]
                        });
                    } else if (node == null && treenode != null) {
                        $('#tt').tree('append', {
                            data: newData[j]
                        });
                    }
                }
            }

            if (rtn[2] != "") {
                var upData = eval("(" + rtn[2] + ")"); //修改操作之后同步easyui tree的树
                for (var i = 0; i < upData.length; i++) {
                    var node = $('#tt').tree('find', upData[i]['id']);
                    if (node) {
                        $('#tt').tree('update', {
                            target: node.target,
                            text: upData[i]['text'],
                            state: upData[i]['state'],
                            attributes: {
                                xmbm: upData[i]['attributes']['xmbm'],
                                par: upData[i]['attributes']['par'],
                                ccjb: upData[i]['attributes']['ccjb'],
                                yjdbz: upData[i]['attributes']['yjdbz'],
                                sybz: upData[i]['attributes']['sybz'],
                                xmlx: upData[i]['attributes']['xmlx'],
                                xmmcex: upData[i]['attributes']['xmmcex']
                            }
                        });
                    }
                }
            }

            if (rtn[3].length != 0) {//删除操作之后同步easyui tree的树
                var len = rtn[3].length;
                var delData = eval("(" + rtn[3] + ")");
                for (var i = 0; i < delData.length; i++) {
                    var node = $('#tt').tree('find', delData[i].id);
                    $('#tt').tree('remove', node.target);
                }
            }

            bool = null;
            jpg = null;
            xmdm = null;
            xmbm = null;
            xmmc = null;
            par = null;
            ccjb = null;
            xmmcex = null;
            gif = -1;
            fbbm = " ";
            content = null;
            isTrue = null;
            onsel = 0;
            arr.length = 0;
            $("#HidTrID").val("");
            $("#hidcheckid").val("");
        }

        function newSave() {
            if ($("#hidcheckid").val() != "" && $("#hideeasyui").val() != "") {
                if ($("#hidcheckid").val() == $("#hideeasyui").val()) {
                    tishi('要转移到的项目不能与被转移项目相同！！！');
                    return;
                } else {
                    var res = XMXX_XMXX.CheckChildren(isTrue, $("#hidcheckid").val(), $("#hideeasyui").val()).value;
                    if (res != null) {
                        tishi(res);
                        return;
                    }
                }
                var res = XMXX_XMXX.Migrate(isTrue, $("#hidcheckid").val(), $("#hideeasyui").val(), xmdm, xmbm, xmmc).value;
                tishi(res);
                getlist('', '', '');
                jsonTree();
            } else {
                tishi("请选择需要转移的项目！！！");
            }
            $("#hidcheckid").val("");
            $("#hideeasyui").val("");
            $('#dd').dialog('close');
        }

        function jsNewAddData(br) {
            if ($("#hidcheckid").val() != "" && $("#hideeasyui").val() != "") {
                if (br) {    //转移到下级
                    isTrue = br;
                } else {     //转移到同级
                    isTrue = br;
                }
            } else {
                tishi("请选择需要转移的项目和转移到的项目！");
            }
        }

        function onselects(obj) {
            //treenode = null;
            onsel = 1; //onsel = 1时代表选中“选择”按钮，onsel = 0代表未选中“选择”按钮
            var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
            var num = $(obj).parent().next()[0].childNodes.length;
            for (var i = 0; i < num - 1; i++) {
                $(obj).parent().next()[0].childNodes[i].src.indexOf("tplus.gif") > -1 ? gif = $(obj).parent().next()[0].childNodes[i].src.indexOf("tplus.gif") : "";
                $(obj).parent().next()[0].childNodes[i].src.indexOf("tplus.gif") > -1 ? ob = $(obj).parent().next()[0].childNodes[i] : "";
            }

            if (divid == "divTreeListView") {
                $("div[id='divTreeListView'] tr[name^='trdata']").css("backgroundColor", "");
                trid = obj.parentNode.parentNode.id;
                jpg = obj.parentNode.parentNode.childNodes[1].innerHTML;
                xmdm = $('tr[id=' + trid + '] td[name=\'tdXMDM\']').html();
                fbbm = $('tr[id=' + trid + '] td[name=\'tdFBBM\']').html();
                subxmbm = $('tr[id=' + trid + '] td input[name=\'txtSUBXMBM\']').val();
                xmmc = $('tr[id=' + trid + '] td input[name=\'txtXMMC\']').val();
                par = $('tr[id=' + trid + '] td[name=\'tdPAR\']').html();
                ccjb = $('tr[id=' + trid + '] td[name=\'tdCCJB\']').html();
                xmbm = $('tr[id=' + trid + '] td[name=\'tdXMBM\']').html();
                xmmcex = $('tr[id=' + trid + '] td[name=\'tdXMMCEX\']').html();
                $("div[id='divTreeListView'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
                $("#HidTrID").val(trid);
            } else {
                $("div[id='dd'] tr[name^='trdata']").css("backgroundColor", "");
                trid = obj.parentNode.parentNode.id;
                $("div[id='dd'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                $("#hideeasyui").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
            }

        }

        function MaxBm(xmdm, par, flag) {
            var tr = $("#divTreeListView td[name=tdPAR]").filter(function () {
                return this.innerHTML == "" + xmdm + ""
            });
            var tr1 = $("#divTreeListView td[name=tdPAR]").filter(function () {
                return this.innerHTML == "" + par + ""
            });
            var len = flag ? tr.length : tr1.length;
            var maxxmbm = "";

            if (flag) {//添加下级

                maxxmbm = len == 0 ? "0" : $("#divTreeListView td[name=tdPAR]").filter(function () {
                    return this.innerHTML == "" + xmdm + ""
                }).parent().last()[0].childNodes[4].childNodes[0].value

            } else { //添加同级

                if (xmdm == null || par == null) {
                    var le = $("#divTreeListView td[name='tdXMDM']").parent().length;
                    if (le == 0) {
                        return "";
                    }
                    maxxmbm = $("#divTreeListView td[name='tdXMDM']").parent().last()[0].childNodes[4].childNodes[0].value;
                } else {
                    var le = $("#divTreeListView td[name='tdXMDM']").parent().length;
                    if (le == 0) {
                        return "";
                    }
                    if (treenode != null && onsel == 0) {
                        maxxmbm = $("#divTreeListView td[name=tdPAR]").filter(function () {
                            return this.innerHTML == "" + treenode.id + ""
                        }).parent().last()[0].childNodes[4].childNodes[0].value
                    } else {
                        maxxmbm = $("#divTreeListView td[name=tdPAR]").filter(function () {
                            return this.innerHTML == "" + par + ""
                        }).parent().last()[0].childNodes[4].childNodes[0].value
                    }

                    //                    if (treenode == null) {
                    //                        maxxmbm = $("#divTreeListView td[name=tdPAR]").filter(function () {
                    //                            return this.innerHTML == "" + par + ""
                    //                        }).parent().last()[0].childNodes[4].childNodes[0].value
                    //                    }
                }

            }
            return maxxmbm;

        }

        function tishi(rtn) {
            $.messager.show({
                title: '温馨提示',
                msg: rtn,
                timeout: 1000,
                showType: 'slide'
            });
        }

        function Bind() {
            $("#divTreeListView td[name=\'tdFBBM\']").attr("align", "left");
            //绑定正则检验
            $("#divTreeListView td input[name=\'txtXMMC\']").bind({
                change: function () {
                    var id = $(this).parent().parent().attr('id');
                    checkName(id, $("#divTreeListView tr[id='" + id + "'] td input[name=\'txtXMMC\']").val());
                }
            });

            $("#divTreeListView td input[name=\'txtSUBXMBM\']").bind({
                change: function () {
                    var id = $(this).parent().parent().attr('id');
                    checkBM(id, $("#divTreeListView tr[id='" + id + "'] td input[name=\'txtSUBXMBM\']").val());
                }
            });

            //判断XMBM不重复
            $("#divTreeListView tr").find("input[name^='txtSUBXMBM']").change(function () {
                var par = this.parentNode.parentNode.id;
                var bmval = $("#" + par + " td[name^='tdFBBM']").html() + $("#" + par + " input[name^='txtSUBXMBM']").val(); //找到当前行的公式名称
                var fbbh = $("#divTreeListView tr[id!='" + par + "']").find("td[name^='tdFBBM']"); //找到所有ID公式名称
                var subxmbm = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtSUBXMBM']"); //找到所有ID公式名称
                var count = 0;
                for (var i = 0; i < subxmbm.length; i++) {
                    var values = fbbh[i].innerText + subxmbm[i].value;
                    if (values == bmval) {
                        count += 1;
                    }
                    else {
                        count;
                    }
                }
                if (count > 0) {
                    tishi("编码不能重复");
                    $("#" + par + " input[name^='txtSUBXMBM']").val("");
                }
            });

            function checkName(id, value) {
                //vkeywords=/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]{1}[^`~!@$%^&()+=|\\\][\]\{\}:;'\,.<>?]{0,19}$/;
                vkeywords = /^[\u4E00-\u9FA5a-zA-Z0-9_]{0,}$/; //汉字 英文字母 数字 下划线组成，0-20位
                //vkeywords = /^[u4e00-u9fa5]{0,}$/;
                if (value == null || value == "") {
                    tishi("请输入汉字！");
                    return false;
                }
                if (!vkeywords.test(value)) {
                    $("#divTreeListView tr[id='" + id + "'] td input[name='txtXMMC']").val("");
                    tishi("请输入正确的数据类型！");
                    return false;
                }
                return true;
            }

            function checkBM(id, value) {
                vkeywords = /^[0-9]{4}$/; //汉字 英文字母 数字 下划线组成，3-20位
                if (value == null || value == "") {
                    tishi("请输入数字！");
                    return false;
                }
                if (!vkeywords.test(value)) {
                    $("#divTreeListView tr[id='" + id + "'] td input[name='txtSUBXMBM']").val("");
                    tishi("请输入正确的数据类型！");
                    return false;
                }
                return true;
            }

            //是否叶子节点设置为只读
            //            $("input[name='chkYJDBZ']").click(
            //                function () {
            //                    this.checked = !this.checked;
            //                }
            //            );
        }

        $(document).ready(function () {
            getlist('', '', '');
            $("#divTreeListView td[name=\'tdFBBM\']").attr("align", "left");
            jsonTree();
            Bind();
        });    
</script>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">

</asp:Content>