﻿<%@ WebHandler Language="C#" Class="YSTZHandler" %>

using System;
using System.Web;
using RMYH.BLL;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Collections.Generic;

public class YSTZHandler : IHttpHandler,IRequiresSessionState  {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string ACTION = context.Request.QueryString["action"];
        string JHFADM = context.Request.QueryString["JHFADM"];
        string ZYDM = context.Request.QueryString["ZYDM"];
        string TZZT = context.Request.QueryString["ZT"];
        string MBNAME = context.Request.QueryString["MBMC"];
        string MBDM = context.Request.QueryString["MBDM"];
        string BZ = context.Request.QueryString["BZ"];
        context.Response.Clear();
        string res = "";
        res = GetData(ACTION, JHFADM,ZYDM, TZZT,MBNAME,MBDM,BZ);
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取调整方案的相关信息
    /// </summary>
    /// <param name="action">执行操作标志</param>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="ZYDM">作业代码</param>
    /// <param name="TZZT">调整状态（1表示未调整；2表示已调整）</param>
    /// <param name="MBNAME">模板名称</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="BZ">SB表示当前操作的是调整上报页面；SP表示当前操作的是调整审批页面</param>
    /// <returns>返回Json字符串</returns>
    public string GetData(string action,string JHFADM,string ZYDM,string TZZT,string MBNAME,string MBDM,string BZ) 
    {
        int Total = 0;
        string SQL="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        if (action == "SB")
        {
            //获取调整方案的模板列表信息

            string USERDM = HttpContext.Current.Session["USERDM"].ToString();
            SQL = "SELECT DISTINCT A.MBDM,B.MBMC,B.MBWJ,B.ZYDM,B.MBLX,CASE J.MBZT WHEN NULL THEN '未调整' WHEN '1' THEN '调整中' WHEN '2' THEN '已上报' ELSE '已审批' END MBZT,A.JSDM,J.TZGZBH,U.USER_NAME  FROM TB_MBJSQX A";
            SQL += " JOIN TB_YSBBMB B ON A.MBDM=B.MBDM AND MBMC LIKE '%" + MBNAME + "%'";
            if (ZYDM != "0")
            {
                SQL += " AND ZYDM='" + ZYDM + "'";
            }
            SQL += " JOIN TB_YSJSXGMB G ON G.MBDM=A.MBDM AND G.JSDM=A.JSDM";
            if (TZZT == "1")
            {
                SQL += " AND G.CZLX='0'";
            }
            SQL += " JOIN TB_JHFA FA ON FA.LCDM=G.LCDM AND FA.JHFADM='" + JHFADM + "' AND FA.YSBS='6'";
            SQL += " LEFT JOIN TB_YSTZJL J ON J.JSDM=A.JSDM AND J.MBDM=A.MBDM AND J.MBZT<>'1' AND J.JHFADM='" + JHFADM + "'";
            SQL += " LEFT JOIN TB_USER U ON U.USER_ID=J.TZGZBH";
            //主表条件
            SQL += " WHERE A.JSDM IN(SELECT JSDM FROM TB_JSYH WHERE USERID='" + USERDM + "') AND SBQX='1' AND CXQX='1' AND XGQX='1'";
            SQL += " AND A.MBDM";
            if (TZZT == "1")
            {
                SQL += " NOT";
            }
            SQL += " IN(SELECT MBDM FROM TB_YSTZJL WHERE MBZT<>'1' AND JHFADM='" + JHFADM + "' AND JSDM IN(SELECT JSDM FROM TB_JSYH WHERE USERID='" + USERDM + "'))";
            DS = BLL.Query(SQL);
            //获取查询条件的总行数
            Total = DS.Tables[0].Rows.Count;
            Str.Append("{\"total\":" + Total + ",");
            Str.Append("\"rows\":[");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Str.Append("{");
                Str.Append("\"MBDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBMC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBWJ\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBWJ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"ZYDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["ZYDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBLX\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBLX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBZT\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBZT"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["JSDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"TZGZBH\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["TZGZBH"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"USER_NAME\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["USER_NAME"].ToString() + "\"");
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}");
                }
            }
            Str.Append("]}");
        }
        else if (action == "TZDB")
        {
            //查询模板项目更改前后的对比列表Json数据

            SQL = "SELECT F.ZYMC,D.XMMC+'-'+C.XMMC MC,H.XMMC SJDX,E.XMMC JSDX,A.VALUE FVALUE,B.VALUE LVALUE,G.DWMC FROM TB_BBFYSJ A,TB_BBFYSJ B,TB_XMXX C,TB_XMXX D,TB_JSDX E,TB_JHZYML F,TB_JLDW G,XT_CSSZ H";
            SQL += " WHERE A.JHFADM='" + JHFADM + "' AND B.JHFADM='" + JHFADM + "' AND A.MBDM='" + MBDM + "' AND B.MBDM='" + MBDM + "'";
            SQL += " AND A.MBDM=B.MBDM AND A.XMDM=B.XMDM AND A.SJDX=B.SJDX";
            SQL += " AND A.XMFL=B.XMFL AND A.JSDX=B.JSDX and A.ZYDM=B.ZYDM";
            SQL += " AND ISNULL(A.VALUE,0)<>ISNULL(B.VALUE,0)";
            SQL += " AND B.XMDM=C.XMDM";
            SQL += " AND B.XMFL=D.XMDM";
            SQL += " AND B.JSDX=E.DM";
            SQL += " AND B.ZYDM=F.ZYDM";
            SQL += " AND B.JLDW=G.DWDM";
            SQL += " AND B.SJDX=H.ZFCS AND H.XMFL='YSSJDX'";
            DS = BLL.Query(SQL);
            //获取查询条件的总行数
            Total = DS.Tables[0].Rows.Count;
            Str.Append("{\"total\":" + Total + ",");
            Str.Append("\"rows\":[");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Str.Append("{");
                Str.Append("\"ZYMC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["ZYMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"SJDX\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["SJDX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDX\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["JSDX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"FVALUE\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["FVALUE"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"LVALUE\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["LVALUE"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"DWMC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["DWMC"].ToString() + "\"");
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}");
                }
            }
            Str.Append("]}");
        }
        else if (action == "SP")
        {
            TB_YSTZBLL TZ = new TB_YSTZBLL();
            //获取流程待处理模板的所有上级节点模板的ID值
            string ID = TZ.GetNotDealAllParID(JHFADM);
            string ZT = TZZT == "1" ? "-1" : "0,1";
            SQL = "SELECT DISTINCT S.DQMBDM MBDM,A.MBMC,A.MBWJ,A.ZYDM,Z.ZYMC,A.MBLX,CASE S.STATE WHEN '-1' THEN '未审核' WHEN '0' THEN '打回'  ELSE '已审核' END MBZT,S.JSDM FROM TB_YSLCSB S,TB_YSBBMB A,TB_MBJSQX QX,TB_JSYH YH,TB_JHZYML Z,TB_YSJSXGMB G";
            SQL += " WHERE S.JHFADM='" + JHFADM + "' AND S.LCDM= AND STATE IN(" + ZT + ")";
            SQL += " AND S.DQMBDM=A.MBDM AND A.MBMC LIKE '%" + MBNAME + "%'";
            SQL += " AND S.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'";
            SQL += " AND S.DQMBDM=QX.MBDM AND S.JSDM=QX.JSDM AND QX.CXQX='1'";
            SQL += " AND A.ZYDM=Z.ZYDM";
            if (ZYDM != "0")
            {
                SQL += " AND ZYDM='" + ZYDM + "'";
            }

            SQL += " AND S.DQMBDM=M.MBDM AND S.JSDM=M.JSDM AND S.LCDM=M.LCDM AND S.HSZXDM=M.HSZXDM";
            //排除之前审批后，模板被打回的处理记录
            if (TZZT == "2")
            {
                SQL += " AND M.ID NOT IN(" + ID + ")";
            }
            //获取查询条件的总行数
            Total = DS.Tables[0].Rows.Count;
            Str.Append("{\"total\":" + Total + ",");
            Str.Append("\"rows\":[");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                Str.Append("{");
                Str.Append("\"MBDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBMC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBWJ\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBWJ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"ZYDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["ZYDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"ZYMC\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["ZYMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBLX\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBLX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBZT\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["MBZT"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDM\":");
                Str.Append("\"" + DS.Tables[0].Rows[i]["JSDM"].ToString() + "\"");

                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}");
                }
            }
            Str.Append("]}");
        }
        else if (action == "Auto")
        {
             
        }
        return Str.ToString();
    }

    public string GetAutoTZMB(string MBStr,string LCDM,string HSZXDM)
    {
        string SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Dictionary<string,int> MB = new Dictionary<string,int>();
        
        string[] Arr = MBStr.Split(',');
        for (int i = 0; i < Arr.Length; i++)
        {
            //判断当前模板有没有被加入字典中
            if (MB.ContainsKey(Arr[i]))
            {
                MB.Add(Arr[i],1); 
            }
            SQL = "SELECT MBDM,JSDM FROM TB_YSJSMBZMB WHERE CHILDMBDM='"+Arr[i].ToString()+"' AND LCDM='"+LCDM+"' AND HSZXDM='"+HSZXDM+"'";
            DS=BLL.Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                 
            }
        }
        return string.Empty;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}