﻿<%@ page language="C#" autoeventwireup="true" inherits="YSTZ_YSTZSQ, App_Web_s5lcgnmx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/style1.css" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function winResize() {
            var iwidth = $(window).width() - 10;     //减去10与body中margin:5px共同作用:为body留的边距  
            var iheight = $(window).height() - 10;
            $("#Fr4").layout({ width: iwidth, height: iheight });                        
        }
        //加载调整模板列表
        function Init() {
            var action = "";
            //TZSB表示上报页面；TZSP表示调整审批页面
            if ($("#<%=HidLB.ClientID %>").val() == "TZSB") {
                action = "SB";
            } else if ($("#<%=HidLB.ClientID %>").val() == "TZSP") {
                action = "SP";
            }
            $.ajax({
                type: 'get',
                url: 'YSTZHandler.ashx',
                data: {
                    action: action,
                    JHFADM: $("#SelJHFA").val(),
                    MBMC: $("#TxtMB").val(),
                    ZYDM: $("#SelZY").val(),
                    ZT: $("#DDLZT").val()
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        //初始化EasyUI dataGrid表格设置
        function InitDataGrid() {
            $('#TabMB').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageList: [10, 20, 30, 40, 50],
                onDblClickRow: function (rowIndex, rowData) {
                   
                },
                columns: [[
                            { field: 'MBDM', title: '模板代码', hidden: true },
                            { field: 'MBMC', title: '模板名称', width: 180},
					        { field: 'MBWJ', title: '模板文件', hidden: true },
                            { field: 'ZYDM', title: '作业代码', hidden: true},
					        { field: 'MBLX', title: '模板类型', hidden: true},
					        { field: 'MBZT', title: '调整状态', width:100 },
					        { field: 'JSDM', title: '角色代码', hidden: true },
                            { field: 'TZGZBH', title: '用户代码', hidden: true},
                            { field: 'USER_NAME', title: '调整人员', width:100 }
				        ]]
            });
        }

        //初始化EasyUI dataGrid表格设置
        function InitSPDataGrid() {
            $('#TabMB').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageList: [10, 20, 30, 40, 50],
                onDblClickRow: function (rowIndex, rowData) {

                },
                columns: [[
                            { field: 'MBDM', title: '模板代码', hidden: true },
                            { field: 'MBMC', title: '模板名称', width: 180 },
					        { field: 'MBWJ', title: '模板文件', hidden: true },
                            { field: 'ZYDM', title: '作业代码', hidden: true },
                            { field: 'ZYMC', title: '使用部门', width: 150 },
					        { field: 'MBLX', title: '模板类型', hidden: true },
					        { field: 'MBZT', title: '调整状态', width: 100 },
					        { field: 'JSDM', title: '角色代码', hidden: true }
				        ]]
            });
        }

        //初始化EasyUI dataGrid表格设置
        function InitYSTZDataDB() {
            $('#TabTZ').datagrid({
                url: 'YSTZHandler.ashx?action=TZDB&&JHFADM=&&MBDM=',
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageList: [10, 20, 30, 40, 50],
                onDblClickRow: function (rowIndex, rowData) {

                },
                columns: [[
                            { field: 'ZYMC', title: '单位名称', width: 150 },
                            { field: 'MC', title: '项目名称', width: 150 },
					        { field: 'SJDX', title: '时间对象', width: 100 },
                            { field: 'JSDX', title: '数据对象', width: 100 },
					        { field: 'FVALUE', title: '调整前数值', width: 80 },
					        { field: 'LVALUE', title: '调整后数值', width: 80 },
					        { field: 'DWMC', title: '计量单位', width: 80 }
				        ]]
            });
        }

        //EasyUI客户端分页
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        //初始化计划方案类别
        function BindFALB() {
            var rtn = YSTZ_YSTZSQ.GetFALB().value;
            $("#SelFALX").html(rtn);
        }
        //初始化年份
        function BindYY() {
            var rtn = YSTZ_YSTZSQ.GetYY().value;
            $("#TxtYY").val(rtn);
        }
        //初始化月、季度
        function BindData() {
            var rtn = YSTZ_YSTZSQ.GetData($("#SelFALX").val()).value;
            if (rtn == "-1") {
                $("#<%=LabMM.ClientID%>").attr("style", "display:none");
                $("#SelData").attr("style", "display:none");
            }
            else {
                if ($("#SelFALX").val() == "2") {
                    $("#<%=LabMM.ClientID%>").html("季度：");
                }
                else {
                    $("#<%=LabMM.ClientID%>").html("月份：");
                }
                $("#<%=LabMM.ClientID%>").attr("style", "display:display");
                $("#SelData").attr("style", "display:display");
                document.getElementById("SelData").options.length = 0
                $("#SelData").html(rtn);
            }

        }
        //初始化计划方案
        function BindJHFA() {
            var MM = "";
            if ($("#SelFALX").val() != "3") {
                MM = $("#SelData").val();
            }
            var rtn = YSTZ_YSTZSQ.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM).value;
            $("#SelJHFA").html(rtn);
        }
        //初始化部门
        function InitZY() {
            var rtn = YSTZ_YSTZSQ.InitZY().value;
            $("#SelZY").html(rtn);
        }
        //方案改变事件
        function ChageFALX() {
            BindData();
            BindJHFA();
        }
        function WinOpen() {
            $("#WinYSTZ")[0].style.display = "block";
            $("#WinYSTZ").window("open");
        }
        //初始化预算调整下拉框
        function InitTZZT() {
            var rtn = YSTZ_YSTZSQ.InitYSZT().value;
            $("#DDLZT").html(rtn);
        }
        $(function () {
            winResize();
            $(window).resize(function () {
                winResize();
            });
            BindFALB();
            BindYY();
            BindData();
            BindJHFA();
            InitZY();
            InitTZZT();
            //TZSB表示当前页面为调整申请页面；TZSP表示当前页面为调整审批页面
            if ($("#<%=HidLB.ClientID %>").val() == "TZSB") {
                InitDataGrid();
            } else if ($("#<%=HidLB.ClientID %>").val() == "TZSP") {
                InitSPDataGrid();
            }

            InitYSTZDataDB();
        });
    </script>
    <style type="text/css">
        .style2
        {
            height: 28px;
        }
    </style>
  </head>
  <body>
     <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
      <div region="north" border="true" style="height: 70px; padding: 1px;background: #B3DFDA;">
        <table>
            <tr>
                <td class="style2">
                    周期：<select id="SelFALX" onchange="ChageFALX()"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                     年份：<input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:BindJHFA()})"  style="width:80px"  />&nbsp;&nbsp;
                    <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label> 
                    <select id="SelData" onchange="BindJHFA()"  style="width:90px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    方案名称： <select id="SelJHFA"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="style2">
                    使用部门： <select id="SelZY"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    状态：
                        <select id="DDLZT" style="width:120px"></select>
                </td>
                <td class="style2">
                    模板名称：<input class="easyui-textbox" type="text" id="TxtMB" style="width:120px" />&nbsp;&nbsp;
                    <input type="button" class="button5" value="查询" style="width:50px; " onclick ="WinOpen()" />&nbsp;&nbsp;
                </td>
        </tr>
        </table>
       </div>
       <div id="DivCenter" region="center">
            <div id="DivMb" style="width: 100%; height: 100%;">
                <table id="TabMB" style="width: 100%; height:100%;"  title="预算调整模板列表" data-options="singleSelect:true">
                </table>
            </div>
       </div>
        <div id="WinYSTZ" class="easyui-window" title="预算调整明细查询" closed="true" minimizable="false" maximizable="false" collapsible="false"  style="width:800px;height:500px;padding:5px;text-align: center;display:none">
            <div style="width:100%; height:100%">
                <div style="width:100%; height:93%; text-align:center">
                    <table id="TabTZ" style="width: 100%; height:78%"></table>
                    <fieldset>
                        <legend style=" text-align:left">处理意见</legend>
                        <textarea id="TxtYJ" rows="3" style=" width:100%;"> </textarea>
                    </fieldset>
                </div>
                <div style="width:100%; height:7%; text-align:center">
                    <input type="button" class="button5" value="确定" style="width:50px;" onclick ="" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" class="button5" value="放弃" style="width:50px; " onclick ="" />
                </div>
            </div>
        </div>
        <input type="hidden" id="HidLB" runat="server" />
    </form>
 </body>
</html>

