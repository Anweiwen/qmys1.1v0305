﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="YJZH_YJZH, App_Web_gegfa5hp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<div id="Divcontent">
       元/吨：<input id="TxtYD" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")'/>&nbsp;
    桶/吨：<input id="TxtTD" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")'/>&nbsp;
    元/美元：<input id="TxtMYY" style="width: 120px" type="text" onkeyup='this.value=this.value.replace(/[^\-?\d.]/g,"")'/>&nbsp;
    美元/桶：<input id="TxtMYT" style="width: 120px" type="text" disabled="disabled" />&nbsp;
    <input type="button" class="button5" value="油价转换" onclick="Conversion()" />
 </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<script type="text/javascript">
    $(document).ready(function () {
        YD();
        TD();
        MYY();
        MYT();
        setSize();
    });
    //获取页面的上左边距 页面显示剧中
    function setSize() {
        var width = document.body.clientWidth * 0.25;
        var height = ((document.documentElement.clientHeight == 0) ? document.body.clientHeight : document.documentElement.clientHeight);
        $("#Divcontent").css("padding-left", width + "px");
        $("#Divcontent").css("padding-top", height * 0.5 + "px");
    }
    //获取 元/吨
    function YD() {
        var rtn = YJZH_YJZH.YD().value;
        $("#TxtYD").val(rtn);
    }
    //获取 桶/吨
    function TD() {
        var rtn = YJZH_YJZH.TD().value;
        $("#TxtTD").val(rtn);
    }
    //获取 美元/元
    function MYY() {
        var rtn = YJZH_YJZH.MYY().value;
        $("#TxtMYY").val(rtn);
    }
    //获取 获取 美元/桶
    function MYT() {
        var rtn = YJZH_YJZH.MYT().value;
        $("#TxtMYT").val(rtn);
    }
    //油价转换
    function Conversion() {
        var rtn = YJZH_YJZH.Conversion($("#TxtYD").val(), $("#TxtTD").val(), $("#TxtMYY").val()).value;
        $("#TxtMYT").val(rtn);
    }
 </script>
</asp:Content>

