﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="MBSZ_JSQX, App_Web_mzjmb31c" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    模板类型：<select id="SelMBLX" onchange="MBLXChage()" style="width:150px;"></select>&nbsp;&nbsp;
    模板周期：<select id="SelMBZQ" onchange="MBZQChage()" style="width: 150px;"></select>&nbsp;&nbsp;
    模版名称：<input id="TxtMKM" style="width: 120px" type="text" />&nbsp;&nbsp;
    <input id="Button1" class="button5"  type="button" value="查询"  onclick="FHSYJ()"/>&nbsp;
    <input type="button" class="button5" value="刷新" onclick="getlist('', '', '')" />
    <input type="button" class="button5" value="删除" onclick="Del()" />
    <input type="button" class="button5" value="取消删除" onclick="Cdel()" />
    <input id="Button11" class="button5" type="button" value="保存" onclick="SetValues()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style="height:100%;border: solid 1px #9ACB34; border-collapse:collapse">
    <tr>
       <td style=" height:100%;border: solid 1px #9ACB34; width:300px; margin-top:10px">
         <div style=" width:300px;height:100%;"><iframe  id="Iframe1" name="ifr" style=" height:100%;"frameborder="0"; marginheight="0" marginwidth="0"></iframe></div>
      </td>
       <td style =" width :100%;vertical-align:top">
            <div style="width:100%; border:solid 1px #9ACB34;" id="div3">
                 <div id="divTreeListView" style="overflow:auto"></div>
            </div>
       </td>
    </tr>
    </table>
    <div id="SjzyDiv" style=" display:none">
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="JSDM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   
<input type="hidden" id="hide_FBDM" runat="server" /> 
<input type="hidden" id="MBDM" runat="server" /> 
<input type="hidden" id="hide_ccjb" runat="server" /> 
<input type="hidden" id="HidMC" runat="server" /> 
<script type="text/javascript">
    $(document).ready(function () {
        BindMBZQ();
        BindMBLX();
        MBZQChage();
//        treeList();
        getlist('', '', '');
    });
    function BBCD(text, mbdm) {
        $("#<%=MBDM.ClientID %>").val(mbdm);
        getlist('', '', '');
    }
//    function treeList() {
//        var hg = $(window).height();
//        $("#div3").height(hg - 45);
//    }
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = MBSZ_JSQX.LoadList(objtr, objid, intimagecount, $("#<%=MBDM.ClientID %>").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    //添加数据
    function jsAddData() {
        if ($("#<%=hide_FBDM.ClientID %>").val() == "") {
            alert("请选择角色列表");
        }
        else {
            if ($("#divTreeListView tr").length > 1) {
                alert("不能重复添加权限");
            }
            else {
                if ($("#hidNewLine").val() == "")
                    $("#hidNewLine").val(MBSZ_JSQX.AddData().value);
                $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
            }
        }
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        if ($("#<%=MBDM.ClientID %>").val() == "") {
            alert("请选择角色列表");
        }
        else {
            var rtn = MBSZ_JSQX.UpdateData(objid, objfileds, objvalues, $("#<%=MBDM.ClientID %>").val()).value;
            if (rtn != "") {
                if (rtn.length > 1) {
                    alert(rtn.substring(1));
                }
            }
            else {

                alert("保存成功！");
            }
        }
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = MBSZ_JSQX.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function MBZQChage() {
        $("#Iframe1").attr("src", "JSBMQX_MB.aspx?mc=" + $("#TxtMKM").val() + "&zq=" + $("#SelMBZQ").val() + "&lx=" + $("#SelMBLX").val() + "");
    }
    function MBLXChage() {
        $("#Iframe1").attr("src", "JSBMQX_MB.aspx?mc=" + $("#TxtMKM").val() + "&zq=" + $("#SelMBZQ").val() + "&lx=" + $("#SelMBLX").val() + "");
    }
    function FHSYJ() {
        $("#Iframe1").attr("src", "JSBMQX_MB.aspx?mc=" + $("#TxtMKM").val() + "&zq=" + $("#SelMBZQ").val() + "&lx=" + $("#SelMBLX").val() + "");
    }
    function BindMBLX() {
        var rtn = MBSZ_JSQX.GetMBLX().value;
        $("#SelMBLX").html(rtn);
    }
    function BindMBZQ() {
        var rtn = MBSZ_JSQX.GetMBZQ().value;
        $("#SelMBZQ").html(rtn);
    }

</script>
</asp:Content>