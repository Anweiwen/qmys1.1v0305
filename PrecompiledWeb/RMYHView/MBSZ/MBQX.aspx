﻿<%@ page language="C#" autoeventwireup="true" inherits="MBSZ_MBQX, App_Web_mzjmb31c" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link href="../CSS/style.css" type="text/css" rel="Stylesheet" />
<script type="text/javascript" src="../JS/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../JS/mainScript.js"></script>
<link href="../CSS/style1.css" type="text/css" rel="Stylesheet" />
</head>
<body style="margin-left: 3px; margin-top: 0px; overflow:auto">
<form id="Form1" runat="server">
<div>
<table id="tbbutton">
    <tr>
        <td>
        &nbsp;
        <asp:Button CssClass="button5" ID="BtnSave" runat="server" Text="保存" onclick="Button2_Click" />
        </td>
        <td>
        &nbsp;
        <input id="Button11" class="button5"  type="button" value="刷新"  onclick="FHSYJ()"/>
        </td>
        <td>
        &nbsp;
        <input id="Button1" class="button5"  type="button" value="全选"  onclick="QX()"/>&nbsp;&nbsp;
        </td>
    </tr>
</table>
</div>
<br />
<div id="DivTree" runat="server">
<asp:TreeView runat="server" ID="TreeView2"  onclick="xuanzhe()"  ShowCheckBoxes="All" >
</asp:TreeView>
</div>
<input type="hidden" id="HidUSERDM" runat ="server" />
<input type="hidden" id="HidCCJB" runat ="server" />
<script type="text/javascript">
    $("#tbbutton input[type=button]").mouseenter(function () { $(this).addClass("button1").removeClass("button"); })
    $("#tbbutton input[type=button]").mouseleave(function () { $(this).addClass("button").removeClass("button1"); })
    $("#<%=BtnSave.ClientID %>").mouseenter(function () { $(this).addClass("button1").removeClass("button"); })
    $("#<%=BtnSave.ClientID %>").mouseleave(function () { $(this).addClass("button").removeClass("button1"); })

    var QXBJ = 1;
    function xuanzhe() {
        var o = window.event.srcElement;
        if (o.tagName == "INPUT" && o.type == "checkbox") //点击treeview的checkbox是触发
        {
            var d = o.id; //获得当前checkbox的id;
            var e = d.replace("CheckBox", "Nodes"); //通过查看脚本信息,获得包含所有子节点div的id
            var div = window.document.getElementById(e); //获得div对象
            if (div != null)  //如果不为空则表示,存在子节点
            {
                var check = div.getElementsByTagName("INPUT"); //获得div中所有的已input开始的标记
                for (i = 0; i < check.length; i++) {
                    if (check[i].type == "checkbox") //如果是checkbox
                    {
                        check[i].checked = o.checked; //字节点的状态和父节点的状态相同,即达到全选
                    }
                }
            }
            else  //点子节点的时候,使父节点的状态改变,即不为全选
            {
                var divid = o.parentElement.parentElement.parentElement.parentElement.parentElement; //子节点所在的div
                var id = divid.id.replace("Nodes", "CheckBox"); //获得根节点的id
                var checkbox = divid.getElementsByTagName("INPUT"); //获取所有子节点数
                var s = 0;
                for (i = 0; i < checkbox.length; i++) {
                    if (checkbox[i].checked)  //判断有多少子节点被选中
                    {
                        s++;
                    }
                }
                if (s > 0)  //如果全部选中 或者 选择的是另外一个根节点的子节点 ，
                {                               //    则开始的根节点的状态仍然为选中状态
                    window.document.getElementById(id).checked = true;
                }
                else {
                    //否则为没选中状态
                    window.document.getElementById(id).checked = false;
                }
            }
        }
    }
    function QX() {
        var tree = document.getElementById("TreeView2").getElementsByTagName("INPUT");
        if (QXBJ == 1) {
            for (var i = 0; i < tree.length; i++) {
                if (tree[i].type == "checkbox" && tree[i].checked == false) {
                    tree[i].checked = true;
                }
            }
            QXBJ = 0;
        }
        else {
            for (var i = 0; i < tree.length; i++) {
                if (tree[i].type == "checkbox" && tree[i].checked == true) {
                    tree[i].checked = false;
                }
            }
            QXBJ = 1;
        }
    }
    function FHSYJ() {
        document.location = "MBQX.aspx?MBDM=" + $("#<%=HidUSERDM.ClientID%>").val() + "&rnd=" + Math.random();
    }
//    function FZQX() {
//        var rtn = window.showModalDialog("JSMBSZ.aspx?MBDM=" + $("#<%=HidUSERDM.ClientID%>").val() + "&rnt=" + Math.random(), "", "dialogWidth=400px;dialogHeight=600px;toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
//        FHSYJ();
//    }
    $("#tbbutton input[type=button]").mouseenter(function () { $(this).removeClass("button").addClass("button1"); })
    $("#tbbutton input[type=button]").mouseleave(function () { $(this).removeClass("button1").addClass("button"); })
    $("#BtnSave").mouseenter(function () { $(this).removeClass("button").addClass("button1"); })
    $("#BtnSave").mouseleave(function () { $(this).removeClass("button1").addClass("button"); })
</script>
</form>
</body>
</html>



