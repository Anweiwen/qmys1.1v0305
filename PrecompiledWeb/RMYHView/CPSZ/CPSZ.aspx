﻿<%@ page language="C#" autoeventwireup="true" inherits="CPSZ_CPSZ, App_Web_gungd2wi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
 </head>
 <body style="100%">
        <div id="tt" class="easyui-tabs" fit="true">
            <div id="divZY" title="作业产品设置" style="padding:5px">
                <iframe  id="IfrZY" name="IfrZY" style="width: 100%;height:100%"   frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	        </div>
	        <div id="divZZB" title="自动半产品设置" style="padding:5px">
                <iframe  id="IfrZZB" name="IfrZZB" style="width: 100%;height:100%"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	        </div> 
            <div id="divCP" title="最终产设置" style="padding:5px">
                <iframe  id="IfrCP" name="IfrCP" style="width: 100%;height:100%"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	        </div> 
	    </div>
     <input type="hidden" runat="server" id="hidloginout" />
      <script type="text/javascript">
          function GetExcelList() {
              var hg = $(document).height();
              $("#IfrZY").attr("src", "CPSZ_ZY.aspx");
              $("#IfrZZB").attr("src", "CPSZ_ZDB.aspx");
              $("#IfrCP").attr("src", "CPSZ_CP.aspx");
              $("#tt")[0].style.height = hg - 18;
              $("#IfrZY").height(hg-45);
              $("#IfrZZB").height(hg-45);
              $("#IfrCP").height(hg-45);
          }
          $(document).ready(function () {
              if ($("#<%=hidloginout.ClientID %>").val() == "Out") {
                  window.parent.parent.location = "<%=Request.ApplicationPath%>/Login.aspx?rnd=" + Math.random();
              } else {
                  GetExcelList();
              }
          });
     </script>
 </body>
 </html>