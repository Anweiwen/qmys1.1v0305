﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="CPSZ_CPSZ_ZY, App_Web_gungd2wi" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      <table id="tbbutton">
       <tr>
        <td><input type="button" class="button5"  value="刷新" onclick ="getlist('', '', '')" /></td>
        <td><input class="button5" onclick="jsAddData()" type="button" value="添加" />  </td>
        <td><input type="button" class="button5" value="删除" onclick="Del()" /></td>
        <td><input type="button" class="button5" value="取消删除" onclick="Cdel()" /></td>
        <td><input id="Button11" class="button5"  type="button" value="保存"  onclick="SetValues()" /></td>
       </tr>
     </table>
     
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style="height:98%">
    <tr>
      <td style="height:100%;border: solid 1px #9ACB34;">
         <div class="summary-title" id="dsd">  &nbsp;作业名称</div>
         <div style=" width:300px;height:97%; overflow:auto" id="TreeVie"><asp:TreeView runat="server" ID="TreeView1"></asp:TreeView></div>
       </td>
       <td style =" width :100%;vertical-align: top">
            <div style="width:100%; height:100%;border: solid 1px #9ACB34; " id="div3">
                 <div  id="divTreeListView"></div>
            </div>
       </td>
    </tr>
    </table>
    <div id="SjzyDiv" style=" display:none">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="CPDM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   
<input type="hidden" id="hide_FBDM" runat="server" /> 
<input type="hidden" id="hid_ZYNBBM" runat="server" />
<input type="hidden" id="hide_ccjb" runat="server" /> 
<input type="hidden" id="HidMC" runat="server" /> 

<script type="text/javascript">

    $(document).ready(function () {
        getlist('', '', '');
    });

    //树形菜单选中加粗
    function setNodeStyle(text) {
        var objs = document.getElementById("TreeVie").getElementsByTagName("a");
        $("#TreeVie a").css('fontWeight', '');
        for (var i = 0; i < objs.length; i++) {
            if (objs[i].innerText == text) {
                objs[i].style.fontWeight = "bold";
                return;
            }
        }
    }
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = CPSZ_CPSZ_ZY.LoadList(objtr, objid, intimagecount, $("#<%=hid_ZYNBBM.ClientID %>").val(), $("#<%=hide_FBDM.ClientID %>").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        //产品编码不能重复
        $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品编码不能重复！");
                $("#" + par + " input[name^='txtCPBM']").val("");
            }
        });
        //产品名称不能重复
        $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品名称不能重复！");
                $("#" + par + " input[name^='txtCPMC']").val("");
            }
        });
    }
    function SetValues() {
        var cpmcValue = "";
        var SJYMC = $("#divTreeListView input[name^=txtCPMC]");
        for (var i = 0; i < SJYMC.length; i++) {
            if (SJYMC[i].value.replace(/(\s*$)/g, "") == "") {
                cpmcValue = "产品名称不能为空！"
                break;
            }
        }
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (cpmcValue != "") {
                alert(cpmcValue);
                return false;
            }
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }
    //添加数据
    function jsAddData() {
            if ($("#<%=hide_FBDM.ClientID %>").val() == "") {
                alert("请选择要添加的作业名！");
            }
            else {

                $("#hidNewLine").val(CPSZ_CPSZ_ZY.AddData($("#<%=hide_FBDM.ClientID %>").val()).value);
                $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));

            }
            //产品编码不能重复
            $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    }
                    else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("产品编码不能重复！");
                    $("#" + par + " input[name^='txtCPBM']").val(parvalue);
                }
            });
            //产品名称不能重复
            $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
                var par = this.parentNode.parentNode.id;
                var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
                var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
                var count = 0;
                for (var i = 0; i < selmbwj.length; i++) {
                    var values = selmbwj[i].value;
                    if (values == parvalue) {
                        count += 1;
                    }
                    else {
                        count;
                    }
                }
                if (count > 0) {
                    alert("产品名称不能重复！");
                    $("#" + par + " input[name^='txtCPMC']").val("");
                }
            });
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        if ($("#<%=hide_FBDM.ClientID %>").val() == "") {
            alert("请选择报表目录");
        }
        else {
            var rtn = CPSZ_CPSZ_ZY.UpdateData(objid, objfileds, objvalues, $("#<%=hide_FBDM.ClientID %>").val()).value;
            if (rtn != "" && rtn.substring(0, 1) == "0") {
                if (rtn.length > 1) {
                    alert(rtn.substring(1));
                }
            }
        }
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = CPSZ_CPSZ_ZY.DeleteData(obj).value;
        if (rtn == "1") {
            alert("删除成功！");
        }
        else {
            alert(rtn);
        }
    }
</script>
</asp:Content>