﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="SYS_MenuListQX, App_Web_eqmkw1i3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table>
    <tr>
        <td>
           <input id="Button2" class="button5"  type="button" value="展开" style="width:60px"  onclick="MenuTreeZD(true)"/>&nbsp;      
        </td>
        <td>
        &nbsp;
         <input id="Button4" class="button5"  type="button" value="收起" style="width:60px"  onclick="MenuTreeZD(false)"/>&nbsp;  
        </td>
        <td>
        &nbsp;
         <input id="Button11" class="button5"  type="button" value="刷新" style="width:60px"  onclick="Refresh()"/>
        </td>
        <td>
        &nbsp;
       <input type="button" class="button5" value="复制权限" style="width:100px" onclick ="Win(true,1)" />
        </td>
        <td>
        &nbsp;
         <input id="Button3" class="button5"  type="button" value="保存" style="width:60px"  onclick="Saves()"/>
        </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table style=" width:100%; height:100%;border: solid 1px #9ACB34; border-collapse:collapse">
    <tr>
      <td style="width:200px; height:100%;border: solid 1px #9ACB34;">
         <div style=" width:200px;height:100%;overflow: auto">
             <div style="height:18px; width:200px; border: solid 1px #9ACB34; background-color: #77ADDB;  vertical-align:middle; text-align: left;">
                 <table>
                     <tr>
                          <td>角色列表</td>
                          <td style=" width:50px; text-align:right;"><img src="../Content/themes/icons/redo.png" alt="全部展开" style=" cursor:pointer" onclick="TreeZD(true)" /></td>
                          <td style=" width:50px; text-align:right"><img src="../Content/themes/icons/undo.png" alt="全部收起" style=" cursor:pointer" onclick="TreeZD(false)" /></td>
                     </tr>
                 </table>
             </div>
             <br />
             <ul id="USERtt"  class="easyui-tree"></ul>
         </div>
      </td>
       <td style ="vertical-align: top; width:100%;height:100%;border: solid 1px #9ACB34;">
            <div style="background-color: #77ADDB;height:20px;"><span>菜单列表</span></div>
            <br />
            <div style="margin-left:12px;" id="divListView">
                <ul id="Menutt"  class="easyui-tree" data-options="checkbox:true"></ul>
            </div>
             <div id="WinJS" class="easyui-window" title="角色选取" closed="true" style="width:300px;height:400px;padding:20px;text-align: center; ">
                <div style=" text-align:left">
                    <input type="button" value="确定" onclick="Win(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                    <input type="button" value="退出" onclick="Win(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
                    <br /><br />
                     <ul id="JStt"  class="easyui-tree"></ul>
                </div>
            </div>
       </td>
    </tr>
  </table> 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input  type="hidden" id="HidJSDM"/>
    <input  type="hidden" id="HidJS"/>
<script type="text/javascript">
    function LoadUSERList() {
        var rtnstr = SYS_MenuListQX.GetUSERJson().value;
        if (rtnstr != "" && rtnstr != null) {
            var Data = eval("(" + rtnstr + ")");
            $("#USERtt").tree("loadData", Data);
        }
        $("#USERtt").tree({
            lines: true,
            onClick: function (node) {
                $("#HidJSDM").val(node.id);
                checkList();
            }
        });
    }
    function checkList() {
        //清除之前选中的项
        var CH = $("#Menutt").tree("getChecked");
        for (var i = 0; i < CH.length; i++) {
            $("#Menutt").tree("uncheck", CH[i].target);
        }
        //给当前选择的用户或者角色赋权限值
        var rtn = SYS_MenuListQX.getCheckedFUNCID($("#HidJSDM").val(),"1").value;
        if (rtn != "") {
            var CD = rtn.split(',');
            for (var i = 0; i < CD.length; i++) {
                var node = $('#Menutt').tree("find", CD[i].toString());
                $("#Menutt").tree("check", node.target);
            }
        }
    }
    function LoadJSList() {
        var rtnstr = SYS_MenuListQX.GetUSERJson().value;
        if (rtnstr != "" && rtnstr != null) {
            var Data = eval("(" + rtnstr + ")");
            $("#JStt").tree("loadData", Data);
        }
        $("#JStt").tree({
            lines: true,
            onClick: function (node) {
                var rtn = SYS_MenuListQX.IsExistQX(node.id).value;
                if (rtn == true) {
                    $("#HidJS").val(node.id);
                } else {
                    alert("当前选择的角色没有权限!");
                    $("#HidJS").val("");
                    return;
                }
            }
        });
    }
    function Refresh() {
        LoadMenuList();
        checkList();
    }
    function LoadMenuList() {
        var rtnstr = SYS_MenuListQX.GetMenuListJson().value;
        if (rtnstr != "" && rtnstr != null) {
            var Data = eval("(" + rtnstr + ")");
            $("#Menutt").tree("loadData", Data);
        }
        $("#Menutt").tree({
            lines: true
        });
    }
    function TreeZD(Flag) {
        if (Flag == true) {
            $("#USERtt").tree("expandAll");
        } else {
            $("#USERtt").tree("collapseAll");
        }
    }
    function MenuTreeZD(Flag) {
        if (Flag == true) {
            $("#Menutt").tree("expandAll");
        } else {
            $("#Menutt").tree("collapseAll");
        }
    }
    function Saves() {
        if ($("#HidJSDM").val() == "") {
            alert("请选择角色！");
            return;
        }
        else {
            var CD = "";
            var CH = $("#Menutt").tree("getChecked");
            for (var i = 0; i < CH.length; i++) {
                if (CH[i].attributes.YJDBZ.toString() == "1") {
                    CD = CD == "" ? CH[i].id.toString() : CD + "," + CH[i].id.toString();
                }
            }
            var rtn = SYS_MenuListQX.Save($("#HidJSDM").val(), "1", CD).value;
            if (rtn > 0) {
                alert("保存成功!");
                return;
            } else {
                alert("保存失败!");
                return;
            }
        }
    }
    function Win(Flag, IsOK) 
    {
        if (Flag == true) 
        {
            if ($("#HidJSDM").val() == "") 
            {
                alert("请选择角色！");
                return;
            }
            else 
            {
                $("#WinJS").window('open');
                LoadJSList();
            }
        }
        else 
        {
            if (IsOK == 1) 
            {
                if ($("#HidJS").val() == "") 
                {
                    alert("您没有选择要复制权限的角色！");
                    return;
                }
                else 
                {
                    if (confirm("您确定要复制此角色权限吗？")) 
                    {
                        var rtn = SYS_MenuListQX.FZQX($("#HidJSDM").val(), $("#HidJS").val()).value;
                        if (rtn > 0) 
                        {
                            alert("操作成功!");
                            $("#HidJS").val("");
                            checkList();
                            $("#WinJS").window('close');
                            return;
                        }
                        else 
                        {
                            alert("操作失败!");
                            return;
                        }
                    }
                }
            }
            else 
            {
                $("#HidJS").val("");
                $("#WinJS").window('close');
            }
        }
    }
    function InitWin() {
        $("#WinJS").window("hcenter");
        $("#WinJS").window({
            top: 0
        });
    }
    $(document).ready(function () {
        LoadUSERList();
        LoadMenuList();
        InitWin();
    });
</script>
</asp:Content>

