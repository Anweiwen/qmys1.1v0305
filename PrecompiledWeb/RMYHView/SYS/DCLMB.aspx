﻿<%@ page title="" language="C#" autoeventwireup="true" inherits="SYS_DCLMB, App_Web_eqmkw1i3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Timer;
        function Init() {
            $.ajax({
                type: 'get',
                url: 'DCLMBHandler.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'DCL',
                    QSSJ: '',
                    JZSJ: '',
                    NAME: '',
                    HSZXDM: '<%=HSZXDM %>'
                },
                success: function (result) {
                    $('#TabMB').datagrid({
//                        rowStyler:function (index, row) {
//                            if (row.BZ == "1") {
//                                return 'background-color:Red;color:Red';
//                            }
//                        },
                        onDblClickRow: function (rowIndex, rowData) {
                            if ($("#TxtYY").val() == "") {
                                alert("预算月份不能为空!");
                                return;
                            }
                            var MB, MC, WJ, YY, MM, JD, FA, FANAME, YF, USERDM, USERNAME, MBLX, ISDone, LB, ML, HSZXDM, SFZLC, CJBM;
                            FA = rowData.JHFADM;
                            FANAME = rowData.JHFANAME;
                            MB = rowData.MBDM;
                            MC = rowData.MBMC;
                            WJ = rowData.MBWJ;
                            YY = rowData.YY;
                            MM = rowData.NN;
                            JD = rowData.JD;
                            YF = $("#TxtYY").val();
                            USERDM = '<%=USERDM %>',
                            USERNAME = '<%=USERNAME %>',
                            HSZXDM = '<%=HSZXDM %>',
                            MBLX = rowData.MBLX;
                            LB = rowData.FABS;
                            ML = rowData.ML;
                            ISDone = "-1";
                            SFZLC = rowData.SFZLC;
                            //                            CJBM = rowData.CJBM;
                            //打开模板
                            OpenMb(MB, MC, WJ, YY, MM, JD, FA, FANAME, YF, USERDM, USERNAME, MBLX, ISDone, LB, ML, HSZXDM, SFZLC);
                        }
                    });
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        //清空模版:
        function ClearMb() {
            $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
        }
        function InitMsg() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'Msg',
                    QSSJ: $("#StartTime").datebox("getValue"),
                    JZSJ: $("#EndTime").datebox("getValue"),
                    NAME: ''
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        result = result.replace(/(\r)+|(\n)+|(\r\n)+/g, "");
                        var Data = eval("(" + result + ")");
                        $("#TabMsg").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    window.clearInterval(Timer);
                    alert("加载失败!");

                }
            });
        }
        function InitMsgHistory() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                async: true,
                cache: false,
                data: {
                    USERDM:  '<%=USERDM %>',
                    action: 'History',
                    QSSJ: $("#QSSJ").datebox("getValue"),
                    JZSJ: $("#JZSJ").datebox("getValue"),
                    NAME:''
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMsgHistory").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function InitJS() {
            $.ajax({
                type: 'get',
                url: 'Message.ashx',
                singleSelect: false,
                checkbox:true,
                async: true,
                cache: false,
                data: {
                    USERDM: '<%=USERDM %>',
                    action: 'JS',
                    QSSJ: $("#QSSJ").datebox("getValue"),
                    JZSJ: $("#JZSJ").datebox("getValue"),
                    NAME: $("#TxtJS").val()
                },
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabJS").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        //          打开模版：模板参数

        //          MBDM      模板代码
        //          MBMC      模板名称
        //          MBWJ      模板文件
        //          YY        年份
        //          MM        月份
        //          JD        季度
        //          JHFADM    计划方案代码
        //          JHFANAME  计划方案名称
        //          YSYF      预算月份
        //          USERDM    用户代码
        //          USERNAME  用户名称
        //          MBLX      模板类别 basesb代表上报； basesp 代表审批
        //          ISDone    0代表待操作； 1代表已操作
        //          FALB      方案类别
        //          ML      模板类型
        function OpenMb(MBDM, MBMC, MBWJ, YY, MM, JD, JHFADM, JHFANAME, YSYF, USERDM, USERNAME, MBLX, ISDone, FALB, ML, HSZXDM, SFZLC) {
            debugger;
            var url = "pageoffice://|" + geturlpath() + "MB.aspx?mbmc=" + getchineseurl(MBMC)
                 + "&mbdm=" + MBDM + "&mbwj=" + getchineseurl(MBWJ) + "&yy=" + YY
                 + "&nn=" + MM + "&jd=" + JD
                 + "&fadm=" + JHFADM + "&fadm2="
                 + "&famc=" + getchineseurl(JHFANAME)
                 + "&ysyf=" + YSYF + "&userdm=" + USERDM
                 + "&username=" + getchineseurl(USERNAME) + "&falb=" + FALB
                 + "&mblb=" + MBLX + "&isexe=" + ISDone + "&mblx=" + ML
                 + "&hszxdm=" + HSZXDM
                 + "&sfzlc=" + SFZLC;
//                 + "&cjbm=" + CJBM;
            
                //如果不是IE浏览器
                if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                    url = decodeURI(url);
                }
                window.location.href = url + "|||";
            }
            function BindMM() {             
                $.ajax({
                    type: 'get',
                    url: 'InitDataHandler.ashx',
                    async: true,
                    cache: false,
                    data: {
                        action: 'YS',
                        YY: '<%=YY %>'
                    },
                    success: function (result) {
                        if (result != "" && result != null) {
                            $("#TxtYY").val(result);
                        }
                    },
                    error: function () {
                        alert("加载失败!");
                    }
                });
            }
            function geturlpath() {
                var href = document.location.href;
                var h = href.split("/");
                href = "";
                for (i = 0; i < h.length - 2; i++) {
                    href += h[i] + "/";
                }
                if (href != "") {
                    href = href + "Excels/Data/";
                }
                return href;
            }
            function getchineseurl(_param) {
                return encodeURIComponent(encodeURIComponent(_param));
            }
            function WinJS(Flag, BZ) {
                if (Flag == true) {
                    $("#WinJS").window('open');
                    InitJS();
                }
                else 
                {
                    if (BZ == 1) {
                        var JSDM = "";
                        var CK = $("#TabJS").datagrid("getChecked");
                        if (CK.length == 0) {
                            alert("您没有选择要发送消息的角色!");
                            return;
                        }
                        for (var i = 0; i < CK.length; i++) {
                            var DM = CK[i].DM;
                            var NAME =CK[i].NAME;
                            if (JSDM == "") {
                                JSDM = DM + "." + NAME;
                            }
                            else {
                                JSDM = JSDM + "," + DM + "." + NAME;
                            }
                        }
                        $("#TxtJSDM").val(JSDM);
                    }
                    $("#WinJS").window('close');
                }
            }
            function WinMsg(Flag) 
            {
                if (Flag == true) 
                {
                    $("#WinMsg").window('open');
                    InitMsgHistory();
                }
                else {
                    $("#WinMsg").window('close');
                }
            }
            function onQSSJ(data) {
                var StartTime = $("#QSSJ").datebox("getValue");
                var EndTime = $("#JZSJ").datebox("getValue");
                if (StartTime != null && StartTime != "") 
                {
                    if (StartTime > EndTime) 
                    {
                        alert("起始时间不能大于截止时间!");
                        $("#QSSJ").datebox("setValue", EndTime);
                        return;
                    }
                    else 
                    {
                        //时间改变后重新加载数据
                        InitMsgHistory();
                    }
                }
                else 
                {
                    alert("起始时间不能为空！");
                    return;
                }
            }
            function onJZSJ(date) 
            {
                var StartTime = $("#QSSJ").datebox("getValue");
                var EndTime = $("#JZSJ").datebox("getValue");
                if (EndTime != null && EndTime != "") 
                {
                    if (EndTime < StartTime) 
                    {
                        alert("截止时间不能小于起始时间!");
                        $("#JZSJ").datebox("setValue", StartTime);
                        return;
                    }
                    else 
                    {
                        //时间改变后重新加载数据
                        InitMsgHistory();
                    }
                }
                else 
                {
                    alert("截止时间不能为空！");
                    return;
                }
            }
            function onStartTime(data) {
                var StartTime = $("#StartTime").datebox("getValue");
                var EndTime = $("#EndTime").datebox("getValue");
                if (StartTime != null && StartTime != "") {
                    if (StartTime > EndTime) {
                        alert("起始时间不能大于截止时间!");
                        $("#StartTime").datebox("setValue", EndTime);
                        return;
                    }
                    else {
                        //时间改变后重新加载数据
                        InitMsg();
                    }
                }
                else {
                    alert("起始时间不能为空！");
                    return;
                }
            }
            function onEndTime(date) {
                var StartTime = $("#StartTime").datebox("getValue");
                var EndTime = $("#EndTime").datebox("getValue");
                if (EndTime != null && EndTime != "") {
                    if (EndTime < StartTime) {
                        alert("截止时间不能小于起始时间!");
                        $("#EndTime").datebox("setValue", StartTime);
                        return;
                    }
                    else {
                        //时间改变后重新加载数据
                        InitMsg();
                    }
                }
                else {
                    alert("截止时间不能为空！");
                    return;
                }
            }
            function InitData() {
                $.ajax({
                    type: 'get',
                    url: 'InitDataHandler.ashx',
                    async: true,
                    cache: false,
                    data: {
                        action: 'Data',
                        YY: ''
                    },
                    success: function (result) {
                        if (result != "" && result != null) {
                            var QSSJ = "", JZSJ = "";
                            QSSJ = result.split('@')[0].toString();
                            JZSJ = result.split('@')[1].toString();

                            $("#StartTime").datebox("setValue", QSSJ);
                            $("#EndTime").datebox("setValue", JZSJ);

                            $("#QSSJ").datebox("setValue", QSSJ);
                            $("#JZSJ").datebox("setValue", JZSJ);

                            InitMsg();
                            Timer = window.setInterval("InitMsg()", 20000);
                        }
                    },
                    error: function () {
                        alert("加载失败!");
                    }
                });       
            }
            function SendMsg() {
                if ($("#TxtMsg").val() == "") {
                    alert("发送的消息不能为空!");
                    return;
                }
                if ($("#TxtJSDM").val() == "") {
                    alert("请先选择要发送消息的角色！");
                    return;
                }
                if (confirm("您确定要发送此消息吗?")) 
                {
                    var rtn = SYS_DCLMB.SendMsg($("#TxtJSDM").val(), $("#TxtMsg").val()).value;
                    var Arr = rtn.split('@');
                    if (parseInt(Arr[0].toString()) > 0) 
                    {
                        alert("发送成功！");
                        $("#TxtMsg").val("");
                        $("#TxtJSDM").val("");
                        InitMsg();
                        return;
                    }
                    else {
                        alert(Arr[1].toString());
                        return;
                    }
                }
            }
            function GSH(val, row) {
                if (row.BZ == "1") {
                    return '<span style="color:red;">' + val + '</span>';
                }
                else {
                    return val;
                }
            }
            $(function () {
                BindMM();
                InitData();
                ClearMb();
                loadTgxx();
            });
            /// <summary>牛森炎新加</summary>
            /// <summary>获取消息通告信息</summary>
            function loadTgxx() {
                var TgxxData = "";
                var rtn = SYS_DCLMB.loadTgxx().value;
                TgxxData = eval("(" + rtn + ")");
                $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', TgxxData);
            }
            /// <summary>牛森炎新加</summary>
            /// <summary>下载消息通告文件</summary>
            function Download() {
                //选中的行
                var rows = $('#dg').datagrid('getSelections');
                if (rows.length > 0) {
                    var data = [];
                    for (var i = 0; i < rows.length; i++) {
                        var item = [];
                        item.push(rows[i].WJMC);
                        data.push(item);
                    }
                    var res = geturlpaths() + "../ExcelFile/TgxxFile/" + data.join(",");
                    window.open(res);
                }
                else {
                    alert("请选择要下载的文件");
                }
            }
            function geturlpaths() {
                var href = document.location.href;
                var h = href.split("/");
                href = "";
                for (i = 0; i < h.length - 1; i++) {
                    href += h[i] + "/";
                }
                return href;
            }
    </script>
</head>
<body>
    <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width:100%; height:100%; top:5px">
     <div data-options="region:'west'" title="待处理任务"   style="width:50%;">
        <div class="easyui-layout" fit="true"  style="width: 100%; height: 100%;">
            <div data-options="region:'north'" style=" height:60px; background:#ADD8E6; padding:15px">
                财务实际发生月份：<input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'MM'})"  style="width:80px"  />&nbsp;&nbsp;  
                <input type="button" value="查询" onclick="Init()" class="button5" style="width:60px" />&nbsp;&nbsp;  
            </div>
           <div id="DivMb" style="width: 100%; height: 100%;" data-options="region:'center'">
              <%--   <%=GetDataGridList(1)%>--%>
              <table id="TabMB" style="width: 100%;height: 100%;  " class="easyui-datagrid" title="" data-options="singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true, pageSize: 15,pageList: [10, 15, 20, 25, 30, 35, 40],"><thead><tr><th  data-options="field:'SYTS',width:100,formatter:GSH">剩余工作日</th><th style="display:none" data-options="hidden:true,field:'BZ',width:0">标记</th><th style="display:none" data-options="hidden:true,field:'JHFADM',width:0">计划方案代码</th><th data-options="field:'JHFANAME',width:150,formatter:GSH">方案名称</th><th style="display:none" data-options="hidden:true,field:'MBDM',width:0">模板代码</th><th data-options="field:'MBMC',width:250,formatter:GSH">报表名称</th><th style="display:none" data-options="hidden:true,field:'JSDM',width:0">角色代码</th><th data-options="field:'JSNAME',width:180,formatter:GSH">角色</th><th style="display:none" data-options="hidden:true,field:'CJBM',width:0">层级编码</th><th style="display:none" data-options="hidden:true,field:'MBWJ',width:0">模板文件</th><th style="display:none" data-options="hidden:true,field:'YY',width:0">年份</th><th style="display:none" data-options="hidden:true,field:'NN',width:0">月份</th><th style="display:none" data-options="hidden:true,field:'JD',width:0">季度</th><th style="display:none" data-options="hidden:true,field:'MBLX',width:0">上报类型</th><th style="display:none" data-options="hidden:true,field:'FABS',width:0">方案标识</th><th style="display:none" data-options="hidden:true,field:'ML',width:0">模板类型</th><th style="display:none" data-options="hidden:true,field:'SFZLC',width:0">是否走流程</th><th data-options="field:'TBSJXZ',width:120,formatter:GSH">截止时间</th></tr></thead></table>
           </div>
        </div>
     </div>
      <div  data-options="region:'center'"  title="消息公告" style="width:50%;">
         <div class="easyui-layout" fit="true"  style="width: 100%; height: 100%;"> 
          <div style="height:60px; background:#ADD8E6;" data-options="region:'north'">
              <table>
                <tr>
                    <td>
                        起始时间：<input id="StartTime" style="width:100px" class="easyui-datebox" data-options="onSelect:onStartTime" />&nbsp; &nbsp;
                        结束时间：<input id="EndTime" style="width:100px" class="easyui-datebox" data-options="onSelect:onEndTime"/> &nbsp; &nbsp;
                        <input id="Button5" class="button5"  type="button" value="历史发送记录" style="width:120px"  onclick="WinMsg(true)"/>&nbsp;
                    </td>
                 </tr>
                 <tr>
                    <td>
                       发送内容：<input id="TxtMsg" type="text" style="width:200px"   />&nbsp;&nbsp;
                      <input type="text" id="TxtJSDM" disabled="disabled" style="width:120px"  />&nbsp;&nbsp;
                      <input id="Button1" class="button5"  type="button" value="角色选择" style="width:70px"  onclick="WinJS(true,0)"/>&nbsp; 
                      <input id="Button2" class="button5"  type="button" value="发送" style="width:50px"  onclick="SendMsg()"/>&nbsp;
                    </td>
                 </tr>
              </table>
          </div>
          <div id="DivMsg" style="width: 100%; height: 100%;"  data-options="region:'center'">
               <%-- <%=GetDataGridList(2)%>--%>
               <table id="TabMsg" style="width: 100%;height: 100%;  " class="easyui-datagrid" title="" data-options="nowrap:false,singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]"><thead><tr><th data-options="field:'SJ',width:100">消息接收时间</th><th data-options="field:'USER_NAME',width:50">发送人</th><th data-options="field:'INFO',width:250">消息内容</th></tr></thead></table>
          </div>

          <div id="DivDownLoad" data-options="region:'south'" title="消息通告文件下载"  style="width: 100%; height: 30%;">
                 <table id="dg" class="easyui-datagrid" style="height:100%;width:100%"  data-options="autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: true,toolbar:'#tb'">
                     <thead>
                         <tr>
                             <th data-options="field:'WJMC',width:60">
                                 文件名称
                             </th>
                         </tr>
                     </thead>
                 </table>
              <div id="tb" style="padding: 5px; height: auto">
                  <div style="margin-bottom: 5px">
                       <input id="Button3" class="button5" type="button" value="下载" onclick="Download()" />
                  </div>
              </div>
          </div>
          <div id="WinJS" class="easyui-window" title="角色选取"  closed="true" data-options="minimizable:false" style="width:700px;height:400px;padding:10px;text-align: center; ">
            <div style=" text-align:left" class="easyui-layout" fit="true"  style="width: 100%; height: 100%;">
               <div style="height:30px; background:#ADD8E6;"data-options="region:'north'" >
                    角色名称：<input type="text" id="TxtJS" style="width:120px" />&nbsp;&nbsp;
                    <input type="button" value="查询" onclick="InitJS()" class="button5" style="width:60px" />&nbsp;&nbsp;
                    <input type="button" value="确定" onclick="WinJS(false,1)" class="button5" style="width:60px" />&nbsp;&nbsp;
                    <input type="button" value="退出" onclick="WinJS(false,-1)" class="button5" style="width:60px" />&nbsp;&nbsp;         
               </div>
               <div id="JSXQ" data-options="region:'center'"  style="width: 100%; height: 60%;">
                    <%--    <%=GetDataGridList(4)%>--%>
                    <table id="TabJS" style="width: 100%;height: 100%;  " class="easyui-datagrid" title="" data-options="idField:'DM',autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]"><thead><tr><th data-options="field:'ck',checkbox:true ">选择</th><th style="display:none" data-options="hidden:true,field:'DM',width:0">角色代码</th><th data-options="field:'NAME',width:150">角色名称</th><th data-options="field:'JSMS',width:180">角色描述</th></tr></thead></table>
               </div>
           </div>
         </div>
         <div id="WinMsg" class="easyui-window" title="历史消息查询" data-options="minimizable:false"  closed="true" style="width:800px;height:450px;padding:20px;text-align: center; ">
            <div style=" text-align:left" class="easyui-layout" fit="true"  style="width: 100%; height: 100%;">
               <div style="height:30px;background:#ADD8E6;"data-options="region:'north'" >
                     起始时间：<input id="QSSJ" style="width:100px" class="easyui-datebox" data-options="onSelect:onQSSJ" />&nbsp; &nbsp;
                     结束时间：<input id="JZSJ" style="width:100px" class="easyui-datebox" data-options="onSelect:onJZSJ"/> &nbsp; &nbsp;
                    <input type="button" value="退出" onclick="WinMsg(false)" class="button5" style="width:60px" />&nbsp;&nbsp;         
               </div>
               <div id="MsgHistory" data-options="region:'center'" style="width:100%; height:100%;">
              <%--   <%=GetDataGridList(3)%>--%>
              <table id="TabMsgHistory" style="width: 100%;height: 100%;  " class="easyui-datagrid" title="" data-options="nowrap:false,singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]"><thead><tr><th data-options="field:'SJ',width:100">发送时间</th><th data-options="field:'NAME',width:120">接收角色</th><th data-options="field:'INFO',width:250">消息内容</th></tr></thead></table>
               </div>
           </div>
         </div>
       </div>
    </div>
   </form>
  </body>
</html>

