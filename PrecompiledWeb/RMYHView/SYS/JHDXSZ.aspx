﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="SYS_JHDXSZ, App_Web_eqmkw1i3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <input type="button" class="button5" value="添加" style="width:60px; " onclick ="jsAddData()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="删除" style="width:60px;" onclick="Del()" />&nbsp;&nbsp;
    <input type="button" class="button5" value="取消删除" onclick="Cdel()"  style="width:80px;"/>&nbsp;&nbsp;
    <input id="Button2" class="button5"  type="button" value="保存" style="width:60px;"  onclick="Saves()"/>&nbsp;&nbsp;
    <input id="Button1" class="button5"  type="button" value="导出到Excel" style="width:100px;"  onclick="DC()"/><br /><br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
 <input type="hidden" id="hidindexid" value="DM" />
 <input type="hidden"   id="hidcheckid" />
 <input  type="hidden" id="hidNewLine"/>
<script type="text/javascript">
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = SYS_JHDXSZ.LoadList(objtr, objid, intimagecount).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divListView").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
    }
    function jsAddData() {
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(SYS_JHDXSZ.AddData().value);
        var Len=$("#divListView tr").length;
        $("#divListView table").append($("#hidNewLine").val().replace('{000}', Len).replace('{000}', Len));
        //获取最大的顺序号
        var SX = SYS_JHDXSZ.GetMAXSX().value;
        var N = $("#divListView img[src$='new.jpg']");
        for (var i = 0; i < N.length; i++) {
            var trid = N[i].parentNode.parentNode.id;
            if ("tr" + Len != trid) {
                var SXH = $("#" + trid + " input[name=txtSX]").val();
                if (parseInt(SXH) >parseInt(SX)) {
                    SX = SXH;
                }
            }
        }
        $("#tr" + Len + " input[name=txtSX]").val(parseInt(SX)+1);
    }
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = SYS_JHDXSZ.UpdateData(objid, objfileds, objvalues).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function jsDeleteData(obj) {
        var rtn = SYS_JHDXSZ.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function Saves() {
//        var XM = $("#divListView img[src$='edit.jpg'],img[src$='new.jpg']").find("input[name=txtXMDM][value='']");
//        var BM = $("#divListView img[src$='edit.jpg'],img[src$='new.jpg']").find("input[name=txtZFBM][value='']");
//        if (XM.length > 0) {
//            alert("计算对象名称不能为空！");
//            return;
//        }
//        if (BM.length > 0) {
//            alert("字母别名不能为空！");
//            return;
//        }
        var Flag = true;
//        var JSDM = "'1','2','3','4','5'";
        var D = $("#divListView img[name=readimage][src$='delete.gif']");
        for (var i = 0; i < D.length; i++) 
        {
            var trid = D[i].parentNode.parentNode.id;
            var DM = $("#" + trid).find("td[name^='tdDM']").html();
            if (parseFloat(DM)<=100) {
                Flag = false;
                break;
            }
        }
        if (Flag == false) {
            alert("系统的计算对象不能删除！");
            return;
        }
        else 
        {
            if (SetValues()) 
            {
                alert("保存成功！");
                return;
            }
            else 
            {
                alert("保存失败！");
                return;
            }
        }
    }
    function EditData(obj, flag) {
        if (flag == "0") 
        {
            //系统的计算对象（代码：1,2,3,4,5）不能删除，只能修改计量单位、顺序，其他项也不能修改
            if (obj.name == "txtXMMC" || obj.name == "txtZFBM" || obj.name == "selSJLX") 
            {
//                var JSDM = "'1','2','3','4','5'";
                var rtn = SYS_JHDXSZ.GetJHDXStr($("#" + obj.parentNode.parentNode.id).find("td[name^='tdDM']").html()).value;
                var JS = rtn.split('|');
                var DM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdDM']").html();
                if (parseFloat(DM)<=100) 
                {
                    alert("此项不能修改！");
                    //将修改后的值恢复到原来的值
                    if (obj.name == "txtXMMC") 
                    {
                        obj.value = JS[0].toString();
                    }
                    if (obj.name == "txtZFBM") 
                    {
                        obj.value = JS[1].toString();
                    }
                    if (obj.name == "selSJLX") 
                    {
                        obj.value = JS[2].toString();
                    }
                    return;
                }
                if (obj.value != null && obj.value != "") {
                    //判断计算对象名称中是否含有特殊字符、别名是否重复
                    if (obj.name == "txtXMMC") {
                        var Reg = /^([\u4E00-\u9FA5]|\w)*$/;
                        if (Reg.test(obj.value) == false) {
                            alert("计算对象名称中不能含有特殊字符！");
                            obj.value = JS[0].toString();
                            return;
                        }
                        //判断计算对象名字不能重复
                        var tr = $("#divListView tr");
                        var JSDM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdDM']").html();
                        for (var i = 1; i < tr.length; i++) {
                            var trid = tr[i].id;
                            var DM = $("#" + trid).find("td[name^='tdDM']").html();
                            var XMMC = $("#" + trid + " input[name='txtXMMC']").val();
                            if (DM != JSDM) {
                                if (XMMC == obj.value) {
                                    alert("计算对象名称不能重复！");
                                    obj.value = "";
                                    return;
                                }
                            }
                            else {
                                var Flag = true;
                                var BM = SYS_JHDXSZ.GetChineseSpell(obj.value).value;
                                //判断字母别名能不能重复
                                var tr = $("#divListView tr");
                                var JSDM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdDM']").html();
                                for (var i = 1; i < tr.length; i++) {
                                    var trid = tr[i].id;
                                    var DM = $("#" + trid).find("td[name^='tdDM']").html();
                                    var ZFBM = $("#" + trid + " input[name='txtZFBM']").val();
                                    if (DM != JSDM) {
                                        if (ZFBM.toString().toUpperCase() == BM.toString().toUpperCase()) {
                                            Flag = false;
                                            return;
                                        }
                                    }
                                }
                                if (Flag == true) {
                                    $("#" + obj.parentNode.parentNode.id + " input[name='txtZFBM']").val(BM);
                                }
                            }
                        }
                    }
                }
                else 
                {
                    alert("计算对象不能为空！");
                    return;
                }
                if (obj.value != null && obj.value != "") {
                    if (obj.name == "txtZFBM") {
                        //判断字母别名能不能重复
                        var tr = $("#divListView tr");
                        var JSDM = $("#" + obj.parentNode.parentNode.id).find("td[name^='tdDM']").html();
                        for (var i = 1; i < tr.length; i++) {
                            var trid = tr[i].id;
                            var DM = $("#" + trid).find("td[name^='tdDM']").html();
                            var ZFBM = $("#" + trid + " input[name='txtZFBM']").val();
                            if (DM != JSDM) {
                                if (ZFBM.toString().toUpperCase() == obj.value.toString().toUpperCase()) {
                                    alert("字母别名不能重复！");
                                    obj.value = "";
                                    return;
                                }
                            }
                        }
                    }
                }
                else {
                    alert("字母别名不能为空！");
                    return;
                }
            }
            if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
        }
    }
    function DC() {
        var rtn=SYS_JHDXSZ.DC().value;
        window.open("../ExcelFile/" + rtn);
    }
    $(document).ready(function () {
        getlist('','','');
    });
</script>
</asp:Content>

