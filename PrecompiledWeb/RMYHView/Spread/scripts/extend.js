/**
* Created by LGS on 2017-05-17 .
*/
var searchmap = {};
var c = /^[a-zA-Z0-9\u4e00-\u9fa5]{1}/;  //首字符为字母,数字或汉字！

/**
 * Created by LI on 2017-05-17 .
 * 单元格属性操作
 */
function initCellPerperty() {

    //设置单元格属性
    $("#setDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var tag = $("#cellTagInputMessage").val();

        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                rowIndex = sel.row,
                colIndex = sel.col,
                rowCount = sel.rowCount,
                colCount = sel.colCount,
                maxRow = rowIndex + rowCount,
                maxColumn = colIndex + colCount,
                r, c;

            if (rowIndex === -1 && colIndex === -1) {
                sheet.tag(tag);
            } else if (rowIndex === -1) {
                for (c = colIndex; c < maxColumn; c++) {
                    sheet.setTag(-1, c, tag);
                }
            } else if (colIndex === -1) {
                for (r = rowIndex; r < maxRow; r++) {
                    sheet.setTag(r, -1, tag);
                }
            } else {
                for (r = rowIndex; r < maxRow; r++) {
                    for (c = colIndex; c < maxColumn; c++) {
                        sheet.setTag(r, c, tag);
                    }
                }
            }
        }
        sheet.resumePaint();
    });

    //获取单元格属性
    $("#getDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        var sel = selections[0],
            row = sel.row,
            col = sel.col,
            $tag = $("#cellTagInputMessage");

        if (row === -1 && col === -1) {
            $tag.val(sheet.tag());
        } else {
            $tag.val(sheet.getTag(row, col));
        }
    });

    //清除单元格属性
    $("#clearDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                row = sel.row,
                col = sel.col;

            if (row === -1 && col === -1) {
                sheet.tag(null);
            } else {
                sheet.clear(row, col, sel.rowCount, sel.colCount, spreadNS.SheetArea.viewport, spreadNS.StorageType.tag);
            }
        }

        sheet.resumePaint();
    });

    //查找单元格属性
    $("#searchTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        //'0':Column First (ZOrder)    "1":Row First (NOrder)
        var searchOrder = parseInt('0', 10);

        if (isNaN(searchOrder)) {
            return;
        }

        var condition = new spreadNS.Search.SearchCondition();
        condition.searchTarget = spreadNS.Search.SearchFoundFlags.cellTag;
        condition.searchString = $("#cellTagInputTitle").val();
        condition.findBeginRow = sheet.getActiveRowIndex();
        condition.findBeginColumn = sheet.getActiveColumnIndex();

        condition.searchOrder = searchOrder;
        if (searchOrder === 0) {
            condition.findBeginColumn++;
        } else {
            condition.findBeginRow++;
        }

        var result = sheet.search(condition);
        if (result.foundRowIndex < 0 && result.foundColumnIndex < 0) {
            condition.findBeginRow = 0;
            condition.findBeginColumn = 0;
            result = sheet.search(condition);
        }

        var row = result.foundRowIndex,
            col = result.foundColumnIndex;

        if (row < 0 && col< 0) {
            $("#cellTagInputMessage").val("Not found");
        }
        else {
            sheet.setActiveCell(row, col);
            $("#cellTagInputMessage").val(sheet.getTag(row, col));
        }
    });
}



/**
* Created by LI on 2017-05-17 .
* 请求服务器端excel在页面展示
*/
function openexcel(selected) {
    var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
    var excelIo = new GC.Spread.Excel.IO();
    // Download Excel file
    var path = geturlpath();
    var url = "";
    if (selected[0] == "EXCELMBSZ") {
        url = path + 'ExcelData.ashx?action=GetExcelorJson&mbwj=' + selected[3] + '';
    }
    if (selected[0] == "ExcelData") {
        url = path + 'ExcelData.ashx?action=GetExcelorJson&jhfadm=' + selected[1] + '&mbmc=' + selected[2] + '';
    }
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';

    xhr.onload = function (e) {
        if (this.status == 200) {
            // get binary data as a response
            var blob = this.response;
            // convert Excel to JSON
            excelIo.open(blob, function (json) {
                importJson(json);
            }, function (e) {
                // process error
                alert(e.errorMessage);
            }, {});
        }
    };

    xhr.send();
    setCheckValue("tabStripVisible", "true");
}

/**
* Created by LI on 2017-05-17 .
* 请求服务器端json数据，在页面展示
*/
function openJson(selected) {
    var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
    var excelIo = new GC.Spread.Excel.IO();
    // Download Excel file
    var path = geturlpath();
    var url = "";
    if (selected[0] == "EXCELMBSZ") {
        url = path + 'ExcelData.ashx?action=GetExcelorJson&mbmc=' + selected[2] + '&mbwj=' + selected[3] + '';
    }
    if (selected[0] == "ExcelData") {
        url = path + 'ExcelData.ashx?action=GetExcelorJson&jhfadm=' + selected[1] + '&mbmc=' + selected[2] + '';
    }
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';

    xhr.onload = function (e) {
        if (this.status == 200) {
            // get binary data as a response
            importJson(this.response);
        }
    };

    xhr.send();
}
/**
* Created by LI on 2017-05-17 .
* 保存excel或json到服务器端
*/
function savetoS() {
    var path = geturlpath();
    //savetoServer("file", row[1], path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');   //保存excel
    saveJsonToServer("file", row[1], path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');  //保存json
}

function initSpreadSreach(spread) {
    $("#searchNext").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {}; //清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 0;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookEnd(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookBefore(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        }

        if (searchResult != null && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
            spread.setActiveSheetIndex(searchResult.foundSheetIndex);
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var con = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).value();
                var resformat = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).formula();
                var coordinate = '$' + searchResult.foundRowIndex + '$' + searchResult.foundColumnIndex;
                var id = searchResult.foundRowIndex.toString() + searchResult.foundColumnIndex.toString();
                var format = resformat == null ? "" : resformat;
                document.getElementById("tablecontent").innerHTML = '<tr id="' + id + '"><td>' + spread.getSheet(searchResult.foundSheetIndex).name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                initTrClick(); //设置table的点击事件
                sheet.repaint();
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("Not Found");
        }
    });
    $("#searchAll").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {}; //清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 1;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            //searchResult = getResultSearchinSheetEnd(searchCondition);
            //if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
            //    searchResult = getResultSearchinWorkbookEnd(searchCondition);
            //}
            //if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
            //    searchResult = getResultSearchinWorkbookBefore(searchCondition);
            //}
            //if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
            //    searchResult = getResultSearchinSheetBefore(searchCondition);
            //}
            searchResult = getResultSearch(searchCondition);
        }

        if (searchResult != null && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
            //                    spread.setActiveSheetIndex(searchResult.foundSheetIndex);
            //                    var sheet = spread.getActiveSheet();
            //                    sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var res = '', n = 0, con, resformat, coordinate, id, format;
                for (var key in searchResult) {
                    //填充查询结果到表格上展示。。。
                    con = searchResult[key].z.value();
                    resformat = searchResult[key].z.formula();
                    coordinate = '$' + searchResult[key].z.row + '$' + searchResult[key].z.col;
                    id = searchResult[key].z.row.toString() + searchResult[key].z.col.toString();
                    format = resformat == null ? "" : resformat;
                    res += '<tr id="' + id + '"><td>' + searchResult[key].z.sheet.name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                    if (n == 0) {//光标定位第一个元素
                        spread.setActiveSheet(searchResult[key].z.sheet.name());
                        var sheet = spread.getActiveSheet();
                        sheet.setActiveCell(searchResult[key].z.row, searchResult[key].z.col);
                        n++;
                    }
                }
                document.getElementById("tablecontent").innerHTML = res;
                initTrClick(); //设置table的点击事件
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("Not Found");
        }
    });
    $("#replaceNext").click(function () {
        var sheet = "";
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        var c = /^[a-zA-Z0-9\u4e00-\u9fa5]{1}/; //首字符为字母或汉字！
        if (JSON.stringify(searchmap) != "{}") { //if条件不支持IE8
            if (searchmap.foundSheetIndex != undefined) {//if条件判断searchmap是单个对象，不是map的{key:value}结构
                spread.setActiveSheet(spread.getSheet(searchmap.foundSheetIndex).name());
                sheet = spread.getActiveSheet();
                if (lookin == "值") {
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value(repval);
                    var id = searchmap.foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                }
                if (lookin == "公式") {
                    var val = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula(); //获取原来的公式
                    val = val.replace(new RegExp(oldval, "gi"), repval);
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula(val);
                    var id = searchmap[key].foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                    $("#" + id + "").children()[3].innerText = val; //更新table的公式
                }
            } else {
                if (lookin == "值") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(searchmap[key].z.sheet.name());
                        sheet = spread.getActiveSheet();
                        sheet.getCell(searchmap[key].z.row, searchmap[key].z.col).value(repval);
                        var id = searchmap[key].z.row.toString() + searchmap[key].z.col.toString();
                        $("#" + id + "").children()[2].innerText = sheet.getCell(searchmap[key].z.row, searchmap[key].z.col).value() //更新值
                        delete (searchmap[key]); //删除已经替换的元素
                        break;
                    }
                }
                if (lookin == "公式") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(searchmap[key].z.sheet.name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(searchmap[key].z.row, searchmap[key].z.col).formula(); //获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(searchmap[key].z.row, searchmap[key].z.col).formula(val); //替换单元格的公式
                            var id = searchmap[key].z.row.toString() + searchmap[key].z.col.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(searchmap[key].z.row, searchmap[key].z.col).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                            var RowIndex = searchmap[key].foundRowIndex;
                            var ColIndex = searchmap[key].foundColumnIndex;
                            var SheetIndex = searchmap[key].foundSheetIndex;
                            var FoundFlag = searchmap[key].searchFoundFlag;
                            delete (searchmap[key]); //删除已经替换的元素

                            searchmap[key] = {
                                foundRowIndex: RowIndex,
                                foundColumnIndex: ColIndex,
                                foundSheetIndex: SheetIndex,
                                foundString: val,
                                searchFoundFlag: FoundFlag
                            };
                        } else {
                            alert('首字符不能为非字母！');
                        }
                        ;

                        break;
                    }
                }
            }
        }
    });
    $("#replaceAll").click(function () {
        spread.suspendPaint();
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var sheet = spread.getActiveSheet();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        if (JSON.stringify(searchmap) != "{}") { //判断searchmap是否为空，if条件不支持IE8
            if (searchmap.foundSheetIndex == undefined) {//if条件判断searchmap是map的{key:value}结构，不是单个对象
                if (lookin == "值") {
                    for (var key in searchmap) {
                        if (searchmap.hasOwnProperty(key) === true) {
                            var keys = key.split('_');
                            spread.setActiveSheet(spread.getSheet(keys[0]).name());
                            //spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                            sheet = spread.getActiveSheet();
                            sheet.getCell(parseInt(keys[1]), parseInt(keys[2])).value(repval);
                            //sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value(repval);
                            //var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                            //$("#" + id + "").children()[2].innerText = repval //更新值
                        }      
                        
                    }
                }
                if (lookin == "公式") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula(); //获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula(val); //替换单元格的公式
                            var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                        } else {
                            alert('首字符不能为非字母！');
                            continue;
                        }
                    }
                }
            } else {

            }
        }
        spread.resumePaint();
    });
}
/**
* 表格的单击和双击事件
* */
function initTrClick() {
    if ($("#tablecontent tr").length > 0) {
        $("#tablecontent tr").click(function (e) {
            spread.setActiveSheet($(e.target).parent().children()[0].innerText); //设置活动sheet页
            var x = $(e.target).parent().children()[1].innerText.split('$')[1];
            var y = $(e.target).parent().children()[1].innerText.split('$')[2];
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(parseInt(x), parseInt(y)); //设置活动单元格
        });

        $("#tablecontent tr").dblclick(function (e) {
            $("#workbook").val($(e.target).parent().children()[0].innerText);
            $("#cells").val($(e.target).parent().children()[1].innerText);
            $("#val").val($(e.target).parent().children()[2].innerText);
            $("#formula").val($(e.target).parent().children()[3].innerText);
            $('#myModal').modal(); //打开bootstroop dialog
        });
    }
}


function getSpanInfo(sheet, row, col) {
    var span = sheet.getSpans(new spreadNS.Range(row, col, 1, 1));
    if (span.length > 0) {
        return { rowSpan: span[0].rowCount, colSpan: span[0].colCount };
    } else {
        return { rowSpan: 1, colSpan: 1 };
    }
}

function getResultSearchinSheetEnd(searchCondition) {
    var sheet = spread.getActiveSheet();
    if (searchCondition.searchAll == 0) {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (searchCondition.searchAll == 1) {
        if (searchCondition.startSheetIndex == -1 && searchCondition.endSheetIndex == -1) {
            searchCondition.startSheetIndex = 0;
            searchCondition.endSheetIndex = spread.getSheetCount() - 1;
        } else {
            searchCondition.startSheetIndex = spread.getActiveSheetIndex();
            searchCondition.endSheetIndex = spread.getActiveSheetIndex();
        }
    }

    if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.zOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = sheet.getActiveRowIndex();
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex() + 1
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    } else if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.nOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = searchCondition.findBeginRow = sheet.getActiveRowIndex() + 1;
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex()
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    }

    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    }
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinSheetBefore(searchCondition) {
    var sheet = spread.getActiveSheet();
    searchCondition.startSheetIndex = spread.getActiveSheetIndex();
    searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.findBeginRow = sel.row;
        searchCondition.findBeginColumn = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    } else {
        searchCondition.rowStart = -1;
        searchCondition.columnStart = -1;
        searchCondition.findBeginRow = -1;
        searchCondition.findBeginColumn = -1;
        searchCondition.rowEnd = sheet.getActiveRowIndex();
        searchCondition.columnEnd = sheet.getActiveColumnIndex();
    }

    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookEnd(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = spread.getActiveSheetIndex() + 1;
    searchCondition.endSheetIndex = -1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookBefore(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = -1
    searchCondition.endSheetIndex = spread.getActiveSheetIndex() - 1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

//获取查找条件
function getSearchCondition() {
    var searchCondition = new spreadNS.Search.SearchCondition();
    var findWhat = $("div[data-name='searchontent'] input").val(); //$("#txtSearchWhat").prop("value");
    var within = $("div[id='within'] span[class='display']").text(); //$("#searchWithin").prop("value");
    var order = $("div[id='searchin'] span[class='display']").text(); //$("#searchOrder").prop("value");
    var lookin = $("div[id='lookin'] span[class='display']").text(); //$("#searchLookin").prop("value");
    var matchCase = $("div[data-name='matchCcase']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"; //$("#chkSearchMachCase").prop("checked");
    var matchEntire = $("div[data-name='matchExactly']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"; //$("#chkSearchMachEntire").prop("checked");
    var useWildCards = $("div[data-name='useWildcards']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"//$("#chkSearchUseWildCards").prop("checked");

    searchCondition.searchString = findWhat;
    if (within == "工作表") {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (order == "列") {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.nOrder;
    } else {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.zOrder;
    }
    if (lookin == "公式") {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellFormula;
    } else {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellText;
    }

    if (!matchCase) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.ignoreCase;
    }
    if (matchEntire) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.exactMatch;
    }
    if (useWildCards) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.useWildCards;
    }


    return searchCondition;
}