﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="CWYS_CWYS_YY, App_Web_zkah552a" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 年份：<input  type="text" id="TxtGetYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:seldateChange()})"  style="width:120px"  />&nbsp;&nbsp;
    <input type="button" class="button5" value="刷新" onclick="getlist('', '', '')" />
    <input class="button5" onclick="jsAddData()" type="button" value="添加" />
    <input type="button" class="button5" value="删除" onclick="Del()" />
    <input type="button" class="button5" value="取消删除" onclick="Cdel()" />
    <input id="Button11" class="button5" type="button" value="保存" onclick="SetValues()" />
    <input id="Button1" class="button5" type="button" value="复制" onclick="Copy()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div id="divTreeListView"></div>
    <div id="Wincopy" class="easyui-window" title="年方案复制" closed="true" style="width: 450px;
        height: 430px; padding: 20px; text-align: center; " minimizable="false" maximizable="false">
        <table>
            <tr style="width:450px">
                <td style=" padding-top: 100px; padding-left: 100px; width:450px" colspan="2">
                     年份：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
                    style="width: 120px" />
                </td>
            </tr>
            <tr style="width:450px">
                <td style="padding-top: 50px; padding-left: 50px;width:450px" colspan="2">
                     计划方案名称：<select id="SelFA" onchange="FAChage()" style="width: 150px;"></select>&nbsp;&nbsp;
                </td>
            </tr>
            <tr style="width:450px">
                <td style="padding-top: 50px; padding-left:110px; width:100px">
                    <input id="Button4" class="button5" type="button" value="复制" onclick="Copys()" />
                </td>
                <td style="padding-top: 50px;">
                    <input id="Button5" class="button5" type="button" value="退出" onclick="Close()" />
                </td>
            </tr>
        </table>
    </div>
     <div id="Div1" class="easyui-window" title="模板复制" closed="true" style="width: 500px;
        height: 400px; padding: 20px; text-align: center; " minimizable="false" maximizable="false">
         <div class="easyui-layout" fit="true"  style="width: 100%; height: 100%;">
             <div data-options="region:'north',title:'',split:false" style="height: 30px; background:#ADD8E6;">
                 <div id="sel">
                     选择复制的方案：<select id="SelectFa" onchange="SelFAChage()" style="width: 150px;"></select>
                 </div>
             </div>
             <div data-options="region:'center',title:''" style="width: 100%; height: 100%; padding: 5px;
                 background: #eee;">
                 <table id="dg" class="easyui-datagrid" style="height:100%;width:100%"  data-options="singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50],frozenColumns: [[{field: 'ck',checkbox: true }]],singleSelect: false,idField:'MBDM'">
                     <thead>
                         <tr>
                             <th data-options="field:'MBDM',width:20">
                                 模板代码
                             </th>
                             <th data-options="field:'MBMC',width:60">
                                 模板名称
                             </th>
                         </tr>
                     </thead>
                 </table>
             </div>
              <div style=" text-align:center;height: 30px; background:#ADD8E6;" data-options="region:'south',title:'',split:false" >

                 <input id="Button3" class="button5" type="button" value="确认" onclick="ConfirmCopy()" />
                <input id="Button2" class="button5" type="button" value="退出" onclick="CloseWindow()" />
             </div>
            
         </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input type="hidden" id="hidindexid" value="JHFADM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" /> 
<input type="hidden" id="oldjhfadm" /> 
<script type="text/javascript">
 var row="";
 $(document).ready(function () {
     InitData();
     GetYY();
     getlist('', '', '');
 });
 /// <summary>按年份查询</summary>
 function seldateChange() {
     getlist('', '', '');
    }
    /// <summary>年份</summary>
    /// <param name="TxtYY" type="String">年</param
    function GetYY() {
        $("#TxtGetYY").val(CWYS_CWYS_YY.InitData().value);
    }
    /// <summary>获得列表</summary>
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = CWYS_CWYS_YY.LoadList(objtr, objid, intimagecount, $("#TxtGetYY").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        /// <summary>每种预算类型只能有一个最终方案</summary>
        $("#divTreeListView tr").find("input[name^='chkDELBZ']").change(function () {
            var par = this.parentNode.parentNode.id;
            var yy = $("#" + par + " input[name^='txtYY']").val();//当前行年
            var parvalue = $("#" + par + " select[name^='selYSBS']").val();//当前行预算类型
            var check = $("#" + par + " input[name^='chkDELBZ']")[0].checked;
            var selysbs = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selYSBS']");//找到所有的预算类型
            var selyy = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtYY']"); //找到所有的年
            var count = 0;
            for (var i = 0; i < selysbs.length && i < selyy.length; i++) {
                var values = selysbs[i].value;
                var thisyy = selyy[i].value;
                var ids = selysbs[i].parentNode.parentNode.id;
                var checks = $("#" + ids + " input[name^='chkDELBZ']")[0];
                if (values == parvalue && thisyy==yy && values != "2" && checks.checked == true) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("每种预算类型只能有一个最终方案");
                this.checked = false;
            }
        });
        /// <summary>在预算流程发起中使用的方案不能修改</summary>
        $("#divTreeListView tr").find("select[name^='selYSBS']").change(function () {
           
            var par = this.parentNode.parentNode.id;
            var jhfadm = $("#" + par + " td[name^='tdJHFADM']")[0].innerHTML; //当前行的计划方案代码
            var selysbs = CWYS_CWYS_YY.selysbs(jhfadm).value;
            var selYslcfs = CWYS_CWYS_YY.selmbdm(jhfadm).value;
            if (selYslcfs) {
                $("#" + par + " select[name^='selYSBS']").val(selysbs);

                alert("此方案已在预算流程发起中使用，预算类型不能修改！");
                return;
            }
        });
        /// <summary>计划方案名称不能重复</summary>
        $("#divTreeListView tr").find("input[name^='txtJHFANAME']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtJHFANAME']").val(); //找到当前行的计划方案名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtJHFANAME']"); //找到所有的计划方案名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("计划方案名称不能重复！");
                $("#" + par + " input[name^='txtJHFANAME']").val("");
            }
        });
        /// <summary>年份必须是数字</summary>
        $("#divTreeListView tr").find("input[name^='txtYY']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtYY']").val(); //找到当前行的起始列的值
            var reg = parvalue.replace(/\D/g, '');
            if (reg == "") {
                alert("年必须是数字类型");
            }
        });
        row = "";
    }
    /// <summary>选择事件</summary>
    /// <param name="jhfadm" type="String">计划方案代码</param>
    function onselects(obj) {
        $("tr[name^='trdata']").css("backgroundColor", "");
        var trid = obj.parentNode.parentNode.id;
        row = obj;
        $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
        $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());

        trid = obj.parentNode.parentNode.id;
        jhfadm = $('tr[id=' + trid + '] td[name=\'tdJHFADM\']').html();
    }
    /// <summary>打开复制窗口</summary>
    /// <param name="jhfadm" type="String">计划方案代码</param>
    function Copy() {
    //如果没选择行就提示
        if (row == "") {
            alert("请选择要复制的方案");
        } else {
            $("#Wincopy").window('open');
            BindFA(jhfadm);
        }
    }
    /// <summary>关闭复制窗口</summary>
    function Close() {
        $("#Wincopy").window('close');
    }
    function FAChage(){

    }
    /// <summary>复制</summary>
    /// <param name="SelFA" type="String">选择的方案</param>
    /// <param name="TxtYY" type="String">选择的年</param>
    function Copys() {
        if (confirm("你确定要将该方案复制到选择的方案中？")) {
            var rtn = CWYS_CWYS_YY.Copy(jhfadm,$("#SelFA").val(), $("#TxtYY").val()).value;
            if (rtn > 0) {
                alert("复制成功！");
                
            }
            else {
                alert("复制失败！没有数据可复制");
            }
        }
        row = "";
    }
    /// <summary>方案改变</summary>
    /// <param name="jhfadm" type="String">计划方案代码</param>
    function dateChange() {
        BindFA(jhfadm);
    }
    /// <summary>绑定方案</summary>
    /// <param name="jhfadm" type="String">计划方案代码</param>
    /// <param name="TxtYY" type="String">年</param>
    /// <param name="SelFA" type="String">选择的方案</param>
    function BindFA(jhfadm) {
        var rtn = CWYS_CWYS_YY.GetFA($("#TxtYY").val(), jhfadm).value;
        $("#SelFA").html(rtn);
        jhfadm = "";
    }
    /// <summary>年份</summary>
    /// <param name="TxtYY" type="String">年</param
    function InitData() {
        $("#TxtYY").val(CWYS_CWYS_YY.InitData().value);
    }
    /// <summary>保存方法</summary>
    function SetValues() {
        var jhfanameValue = "";
        var JHFANAME = $("#divTreeListView input[name^=txtJHFANAME]");
        for (var i = 0; i < JHFANAME.length; i++) {
            if (JHFANAME[i].value.replace(/(\s*$)/g, "") == "") {
                jhfanameValue = "计划方案名称不能为空！"
                break;
            }
        }
        var yyValue = "";
        var YY = $("#divTreeListView input[name^=txtYY]");
        for (var i = 0; i < YY.length; i++) {
            if (YY[i].value.replace(/(\s*$)/g, "") == "") {
                yyValue = "年不能为空！"
                break;
            }
        }
        var hszxdmValue = "";
        var HSZXDM = $("#divTreeListView input[name^=tselHSZXDM]");
        for (var i = 0; i < HSZXDM.length; i++) {
            if (HSZXDM[i].value.replace(/(\s*$)/g, "") == "") {
                hszxdmValue = "所属核算中心不能为空";
                break;
            }
        }
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (jhfanameValue != "") {
                alert(jhfanameValue);
                return false;
            }
            if (yyValue != "") {
                alert(yyValue);
                return false;
            }
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            if (hszxdmValue != "") {
                alert(hszxdmValue);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }

    /// <summary>添加方法</summary>
    function jsAddData() {
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(CWYS_CWYS_YY.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
        /// <summary>每种预算类型只能有一个最终方案</summary>
        $("#divTreeListView tr").find("input[name^='chkDELBZ']").change(function () {
            var par = this.parentNode.parentNode.id;
            var yy = $("#" + par + " input[name^='txtYY']").val();
            var parvalue = $("#" + par + " select[name^='selYSBS']").val();
            var check = $("#" + par + " input[name^='chkDELBZ']")[0].checked;
            var selysbs = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selYSBS']");
            var selyy = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtYY']"); //找到所有的年
            var count = 0;
            for (var i = 0; i < selysbs.length && i < selyy.length; i++) {
                var values = selysbs[i].value;
                var thisyy = selyy[i].value;
                var ids = selysbs[i].parentNode.parentNode.id;
                var checks = $("#" + ids + " input[name^='chkDELBZ']")[0];
                if (values == parvalue && thisyy == yy && values != "2" && checks.checked == true) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("每种预算类型只能有一个最终方案");
                this.checked = false;
            }
        });
        /// <summary>计划方案名称不能重复</summary>
        $("#divTreeListView tr").find("input[name^='txtJHFANAME']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtJHFANAME']").val(); //找到当前行的计划方案名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtJHFANAME']"); //找到所有的计划方案名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("计划方案名称不能重复！");
                $("#" + par + " input[name^='txtJHFANAME']").val("");
            }
        });
        /// <summary>年份必须是数字</summary>
        $("#divTreeListView tr").find("input[name^='txtYY']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtYY']").val(); //找到当前行的起始列的值
            var reg = parvalue.replace(/\D/g, '');
            if (reg == "") {
                alert("年必须是数字类型");
            }
        });
    }
    /// <summary>年批复方案修改保存时弹出窗口</summary>
    /// <param name="年" type="String">年</param>
    function Confirm(objid, yy) {
        $("#oldjhfadm").val(objid);
        $("#Div1").window('open');
        BindSelFA(yy);
        loaddata();
        //默认全选中
        $('#dg').datagrid('selectAll');
    }
    /// <summary>关闭年批复方案</summary>
    function CloseWindow() {
        $("#Div1").window('close');
    }
    //保存复选框的id值
    var checkedItems = [];

    function ischeckItem() {
        for (var i = 0; i < checkedItems.length; i++) {
            $('#dg').datagrid('selectRecord', checkedItems[i]); //根据id选中行   
        }
    }
    //批复确定
    function ConfirmCopy() {
        if (confirm("这些模板数据将被覆盖是否继续？")) {
            
            //选中的行
            var rows = $('#dg').datagrid('getSelections');
            var data = [];
            for (var i = 0; i < rows.length; i++) {
                var item = [];
                item.push(rows[i].MBDM);
                data.push(item);
            }
            var rtn = CWYS_CWYS_YY.ConfirmCopy($("#SelectFa").val(), data.join(","), $("#oldjhfadm").val()).value;
            alert(rtn);
            CloseWindow();
        }
    }
    function loaddata() {
       
        var faData = "";
        var rtn = CWYS_CWYS_YY.loadMb().value;
        faData = eval("(" + rtn + ")");
        $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', faData);
    }

    function pagerFilter(data) {
        if (data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }

            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                loading: true,
                showRefresh: false,
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
    }
    function SelFAChage() {

    }
    //绑定年批复方案的方案
    /// <param name="yy" type="String">年</param>
    function BindSelFA(yy) {
        var rtn = CWYS_CWYS_YY.GetSelFA(yy).value;
        $("#SelectFa").html(rtn);
    }
    /// <summary>修改数据</summary>
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = CWYS_CWYS_YY.UpdateData(objid, objfileds, objvalues).value;
        var id = rtn.split('|')[1];
        rtn = rtn.split('|')[0];
        if (rtn == "1") {
            for (var j = 0; j < objid.length; j++) {
                if (objid[j].replace(/\s+/g,"") != "") {
                        for (var i = 0; i < objvalues.length; i++) {
                            var yy = objvalues[i].split('|')[1]
                        }
                        Confirm(objid, yy);
                }
                else {
                        for (var i = 0; i < objvalues.length; i++) {
                            var yy = objvalues[i].split('|')[1]
                        }
                        objid[0] = id;
                        Confirm(objid, yy);
                }
            }
                
        }
        else {
            alert(rtn);
        }
    }
    /// <summary>删除</summary>
    function jsDeleteData(obj) {
        var rtn = CWYS_CWYS_YY.DeleteData(obj).value;
        alert(rtn);
    }
</script>
</asp:Content>