﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="CWYS_CWYS_NN, App_Web_zkah552a" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 年份：<input  type="text" id="TxtGetYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:seldateChange()})"  style="width:120px"  />&nbsp;&nbsp;
        <input type="button" class="button5"  value="刷新" onclick ="getlist('', '', '')" />
        <input class="button5" onclick="jsAddData()" type="button" value="添加" /> 
        <input type="button" class="button5" value="删除" onclick="Del()" />
        <input type="button" class="button5" value="取消删除" onclick="Cdel()" />
        <input id="Button11" class="button5"  type="button" value="保存"  onclick="SetValues()" />
        <input id="Button1" class="button5" type="button" value="复制" onclick="Copy()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div  id="divTreeListView"></div>
    <div id="Wincopy" class="easyui-window" title="月方案复制" closed="true" style="width: 450px;
        height: 430px; padding: 20px; text-align: center;" minimizable="false" maximizable="false">
          <table>
            <tr style="width:450px">
                <td style=" padding-top: 100px; padding-left: 60px; width:160px;">
                     年份：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
                    style="width: 120px" />
                </td>
                <td style=" padding-top:100px">
                 月份：<select id="SelMonth" onchange="MonthChage()" style="width: 80px;"></select>
                </td>
            </tr>
            <tr style="width:450px">
                <td style="padding-top: 50px; padding-left: 50px;width:450px" colspan="2">
                     计划方案名称：<select id="SelFA" onchange="FAChage()" style="width: 150px;"></select>&nbsp;&nbsp;
                </td>
            </tr>
            <tr style="width:450px">
                <td style="padding-top: 50px; padding-left:110px; width:100px">
                    <input id="Button4" class="button5" type="button" value="复制" onclick="Copys()" />
                </td>
                <td style="padding-top: 50px;">
                    <input id="Button5" class="button5" type="button" value="退出" onclick="Close()" />
                </td>
            </tr>
        </table>
<%--        <div style="text-align: left">
            <div style="margin-top: 100px; margin-left: 50px">
                年份：<input type="text" id="TxtYY" class="Wdate" onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:dateChange()})"
                    style="width: 80px" />
                月份：<select id="SelMonth" onchange="MonthChage()" style="width: 80px;"></select>
            </div>
            <div style="margin-top: 50px; margin-left: 50px">
                计划方案名称：<select id="SelFA" onchange="FAChage()" style="width: 150px;"></select>&nbsp;&nbsp;
            </div>
            <div style="margin-top: 50px;">
                <div style="margin-left:110px;float:left">
                    <input id="Button2" class="button5" type="button" value="复制" onclick="Copys()" />
                </div>&nbsp;&nbsp;
                <div style="margin-right:110px;float:right">
                    <input id="Button3" class="button5" type="button" value="退出" onclick="Close()" />
                </div>
            </div>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <input type="hidden" id="hidindexid" value="JHFADM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" /> 
<script type="text/javascript">
    var row = "";
    $(document).ready(function () {
        InitData();
        GetYY();
        BindMonths();
        getlist('', '', '');
    });
    /// <summary>按年份查询</summary>
    function seldateChange() {
        getlist('', '', '');
    }
    /// <summary>年份</summary>
    /// <param name="TxtYY" type="String">年</param
    function GetYY() {
        $("#TxtGetYY").val(CWYS_CWYS_NN.InitData().value);
    }
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = CWYS_CWYS_NN.LoadList(objtr, objid, intimagecount, $("#TxtGetYY").val()).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        /// <summary>每种预算类型只能有一个最终方案</summary>
        $("#divTreeListView tr").find("input[name^='chkDELBZ']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            //当前行年
            var yy = $("#" + par + " input[name^='txtYY']").val();
            //当前行月
            var nn = $("#" + par + " select[name^='selNN']").val();
            //当前行预算类型
            var parvalue = $("#" + par + " select[name^='selYSBS']").val();
            var check = $("#" + par + " input[name^='chkDELBZ']")[0].checked;
            //找到所有的预算类型
            var selysbs = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selYSBS']");
            //找到所有的年
            var selyy = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtYY']");
            //找到所有的月
            var selnn = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selNN']");
            var count = 0;
            for (var i = 0;i < selysbs.length &&i < selyy.length && i < selnn.length; i++) {
                var values = selysbs[i].value;
                var thisyy = selyy[i].value;
                var thisnn = selnn[i].value;
                var ids = selysbs[i].parentNode.parentNode.id;
                var checks = $("#" + ids + " input[name^='chkDELBZ']")[0];
                if (values == parvalue && thisyy == yy && thisnn == nn && values != "2" && checks.checked == true) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("每种预算类型只能有一个最终方案");
                this.checked = false;
            }
        });

        //计划方案名称不能重复
        $("#divTreeListView tr").find("input[name^='txtJHFANAME']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtJHFANAME']").val(); //找到当前行的计划方案名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtJHFANAME']"); //找到所有的计划方案名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("计划方案名称不能重复！");
                $("#" + par + " input[name^='txtJHFANAME']").val("");
            }
        });
        //在预算流程发起中使用的方案不能修改
        $("#divTreeListView tr").find("select[name^='selYSBS']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            var jhfadm = $("#" + par + " td[name^='tdJHFADM']")[0].innerHTML; //当前行的计划方案代码
            var selysbs = CWYS_CWYS_NN.selysbs(jhfadm).value;
            var selYslcfs = CWYS_CWYS_NN.selmbdm(jhfadm).value;
            if (selYslcfs) {
                $("#" + par + " select[name^='selYSBS']").val(selysbs);

                alert("此方案已在预算流程发起中使用，预算类型不能修改！");
                return;
            }
        });
        row = "";
    }
    function onselects(obj) {
        $("tr[name^='trdata']").css("backgroundColor", "");
        var trid = obj.parentNode.parentNode.id;
        row = obj;
        $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
        $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());

        trid = obj.parentNode.parentNode.id;
        jhfadm = $('tr[id=' + trid + '] td[name=\'tdJHFADM\']').html();
    }
    //打开复制窗口
    function Copy() {
        if (row == "") {
            alert("请选择要复制的方案");
        }
        else {
            $('#Wincopy').window({ left: "500px", top: "150px" });
            $("#Wincopy").window('open');
            BindFA(jhfadm);
        }
    }
    //关闭复制窗口
    function Close() {
        $("#Wincopy").window('close');
    }
    function FAChage() {

    }
    function Copys() {
        if (confirm("你确定要将该方案复制到选择的方案中？")) {
            var rtn = CWYS_CWYS_NN.Copy(jhfadm, $("#SelFA").val(), $("#TxtYY").val()).value;
            if (rtn > 0) {
                alert("复制成功！");
            }
            else {
                alert("复制失败！没有数据可复制");
            }
        }
        row = "";
    }
    function dateChange() {
        BindFA(jhfadm);
    }
    function MonthChage() {
        BindFA(jhfadm);
    }
    //月份
    function BindMonths() {
        $("#SelMonth").html(CWYS_CWYS_NN.GetMonths().value);
    }
    //绑定方案
    function BindFA(jhfadm) {
        var rtn = CWYS_CWYS_NN.GetFA($("#TxtYY").val(), $("#SelMonth").val(), jhfadm).value;
        $("#SelFA").html(rtn);
        jhfadm = "";
    }
    //年份
    function InitData() {
        $("#TxtYY").val(CWYS_CWYS_NN.InitData().value);
    }
    function SetValues() {
        var jhfanameValue = "";
        var JHFANAME = $("#divTreeListView input[name^=txtJHFANAME]");
        for (var i = 0; i < JHFANAME.length; i++) {
            if (JHFANAME[i].value.replace(/(\s*$)/g, "") == "") {
                jhfanameValue = "计划方案名称不能为空！"
                break;
            }
        }
        var yyValue = "";
        var YY = $("#divTreeListView input[name^=txtYY]");
        for (var i = 0; i < YY.length; i++) {
            if (YY[i].value.replace(/(\s*$)/g, "") == "") {
                yyValue = "年不能为空！"
                break;
            }
        }
        var hszxdmValue = "";
        var HSZXDM = $("#divTreeListView input[name^=tselHSZXDM]");
        for (var i = 0; i < HSZXDM.length; i++) {
            if (HSZXDM[i].value.replace(/(\s*$)/g, "") == "") {
                hszxdmValue = "所属核算中心不能为空";
                break;
            }
        }
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (jhfanameValue != "") {
                alert(jhfanameValue);
                return false;
            }
            if (yyValue != "") {
                alert(yyValue);
                return false;
            }
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            if (hszxdmValue != "") {
                alert(hszxdmValue);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }
    //添加数据
    function jsAddData() {
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(CWYS_CWYS_NN.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
        /// <summary>每种预算类型只能有一个最终方案</summary>
        $("#divTreeListView tr").find("input[name^='chkDELBZ']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            //当前行年
            var yy = $("#" + par + " input[name^='txtYY']").val();
            //当前行月
            var nn = $("#" + par + " select[name^='selNN']").val();
            //当前行预算类型
            var parvalue = $("#" + par + " select[name^='selYSBS']").val();
            var check = $("#" + par + " input[name^='chkDELBZ']")[0].checked;
            //找到所有的预算类型
            var selysbs = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selYSBS']");
            //找到所有的年
            var selyy = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtYY']");
            //找到所有的月
            var selnn = $("#divTreeListView tr[id!='" + par + "']").find("select[name^='selNN']");
            var count = 0;
            for (var i = 0; i < selysbs.length && i < selyy.length && i < selnn.length; i++) {
                var values = selysbs[i].value;
                var thisyy = selyy[i].value;
                var thisnn = selnn[i].value;
                var ids = selysbs[i].parentNode.parentNode.id;
                var checks = $("#" + ids + " input[name^='chkDELBZ']")[0];
                if (values == parvalue && thisyy == yy && thisnn == nn && values != "2" && checks.checked == true) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("每种预算类型只能有一个最终方案");
                this.checked = false;
            }
        });
        //计划方案名称不能重复
        $("#divTreeListView tr").find("input[name^='txtJHFANAME']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtJHFANAME']").val(); //找到当前行的计划方案名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtJHFANAME']"); //找到所有的计划方案名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("计划方案名称不能重复！");
                $("#" + par + " input[name^='txtJHFANAME']").val("");
            }
        });
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = CWYS_CWYS_NN.UpdateData(objid, objfileds, objvalues).value;
        alert(rtn);
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = CWYS_CWYS_NN.DeleteData(obj).value;
        alert(rtn);
    }
</script>
</asp:Content>
