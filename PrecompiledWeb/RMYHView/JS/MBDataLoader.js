﻿
var cmenu;
var localopen = false;
var xcolor = "#C2F6A2";
//处理模板请求URL
var rootPath = "../Data/MB.ashx?";
//当前服务器年，月
var bnyy = "", bnnn = "", GSYY = "", GSMM = "", USERDM = "", smqr = ""; SmqrWidth = "610"; SmqrHeight = "480",smqr="",add="";
//Excel保护模式下的默认密码
var xlProtectPassword = 'boomlink';
//加载模板数据
function InitData() {
    //当前服务器年、月;
    GetYyNn();
    //获取公式的年份、月份
    getGSYYMM(ParamDic["FADM"], ParamDic["FALB"], ParamDic["YY"], ParamDic["YSYF"]);
    if (ParamDic["MBLX"] == "1") {
        InitImbal();
    }
    $('#dlgmsg').dialog({
        modal: true
    });
    //SpreadJS页面不确定在哪儿显示PageOffice上的标题（待定）
    var filename = getsavefilename(ParamDic["FADM"], ParamDic["MBWJ"]);
    var filenamejson = filename.substring(0, filename.lastIndexOf('.')) + ".json";
    //var filename="Jhfadm~10050157~Mb~M10161896.XLS"
    IsExcelOrJsonExisted(filename, filenamejson);


    // fileexist('../Excels/XlsData/' + filename,
    //     //如果存在填报文件：
    //     function () {
    //         dialogshowwin();
    //         dialogshow('存在已填报文件,正在加载......', true);
    //         //打开Spread JS的Excel页面
    //         $("#HidSaveFileName").val(filename);
    //         //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
    //         openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
    //     },
    //     //如果不存在填报文件：
    //     function () {
    //         fileexist('../Excels/Xls/' + ParamDic["MBWJ"],
    //             //如果存在模板文件：
    //             function () {
    //                 dialogshowwin();
    //                 dialogshow('不存在已填报文件,正在加载模板......', true);
    //                 //打开Spread JS的Excel页面
    //                 $("#HidSaveFileName").val(ParamDic["MBWJ"]);
    //                 openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
    //                 //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
    //                 //openexcel(row + "&FileName=" + $("#HidSaveFileName").val(), false);
    //             },
    //             function () {
    //                 alert('模板文件和数据文件均不存在，请检测文件是否存在！\n \n 有可能服务器文件损坏导致......');
    //                 return;
    //             }
    //         );
    //     }
    // );
}


function IsExcelOrJsonExisted(filename, jsonname) {
    var path = geturlpath();
    $.ajax({
        type: "post",
        url: path + "ExcelData.ashx?action=IsExcelOrJsonExisted",
        data: {
            filename: filename,
            jsonname: jsonname
        },
        success: function(result) {
            if (result == "1") {
                dialogshowwin();
                dialogshow('存在已填报文件,正在加载......', true);
                //打开Spread JS的Excel页面
                $("#HidSaveFileName").val(filename);
                //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
                openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
            } else {
                fileexist('../Excels/Xls/' + ParamDic["MBWJ"],
                    //如果存在模板文件：
                    function() {
                        dialogshowwin();
                        dialogshow('不存在已填报文件,正在加载模板......', true);
                        //打开Spread JS的Excel页面
                        $("#HidSaveFileName").val(ParamDic["MBWJ"]);
                        openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                        //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                        //openexcel(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                    },
                    function() {
                        alert('模板文件和数据文件均不存在，请检测文件是否存在！\n \n 有可能服务器文件损坏导致......');
                        return;
                    }
                );
            }
        },
        error: function(e) {
            alert(e);
        }
    });
}



//模板打开以后执行的事件：
function AfterOpenMb() 
{
    if (!localopen) 
    {
        dialogshowwin();
        dialogshow('解析模板公式......');
        AfterOpenMbPost();
    }
    else 
    {
        lockallsheet();
    }
}


// -----------ajax方法-----------//
function AfterOpenMbPost() {
    try {
    debugger;
        $.ajax({
            type: 'get',
            url: '../Data/MBDataHandler.ashx',
            cache: false,
            async: true,
            data: {
                GSYY: GSYY,
                GSMM: GSMM,
                action: 'getgs',
                YY: ParamDic["YY"],
                FADM: ParamDic["FADM"],
                FADM2: ParamDic["FADM2"],
                FABS: ParamDic["FALB"],
                FileName: $("#HidSaveFileName").val()
            },
            success: function (result) {
                var sucjson = eval('(' + result + ')');
                if (sucjson) {
                    var count = parseInt(sucjson["count"]) - parseInt(sucjson["Len"]);
                    dialogshow('总计【' + count.toString() + '】个公式......');
                    if (parseInt(sucjson["count"]) > 0) {
                        if (!setgs(sucjson, ParamDic["YY"], ParamDic["NN"], ParamDic["JD"], ParamDic["FALB"], GSYY, GSMM, ParamDic["YSYF"])) {
                            $.messager.alert("提示框", "解析公式回写错误！");
                        }
                    }
                }
                AfterOpen(false);
            },
            error: function (req) {
                BmShowWinError('公式解析错误!', req.responseText);
                AfterOpen(false);
            }
        });
    }
    catch (error) {
        $.messager.alert("错误框", error.message);
        return;
    }
}
//回写解析后的结果到excel上(用数组解析提高回写效率)：
function setgs(_json, _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf) {
    var Lastsheet;
    //结束Sheet页编辑
    for (var i = 0; i < spread.getSheetCount(); i++) {
        sheet = spread.sheets[i];
        EndSheetEdit(sheet);
    }
    if (spread != null && spread != undefined) {
        unprotectallsheet();
        var icount = _json["count"];
        var idx = -1;
        for (var i = 0; i < icount; i++) {
            var jsonrow = _json["grids"][i];
            var sheet = getsheetbyname(jsonrow["name"]);
            if (sheet) {
                if (Lastsheet == null || Lastsheet == undefined || Lastsheet == "") {
                    sheet.suspendPaint();
                }
                else {
                    //如果发现前一个单元格和现在操作的单元格不属于同一个单元格的话，则进行上一个sheet页重绘
                    if (Lastsheet.name() != sheet.name()) {
                        //重新绘制Excel
                        Lastsheet.resumePaint();
                        sheet.suspendPaint();
                    }
                }
                var cel = sheet.getCell(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]));
                if (jsonrow["gs"].indexOf('~~') >= 0) {
                    cel.value(setparam(jsonrow["gs"], _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf));
                }
                else
                    cel.formula(jsonrow["gs"]);
                sheet.comments.remove(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]));
                if (cel.formula() != null && cel.formula() != "" && cel.formula() != undefined) {
                    //添加公式备注
                    var comment = new GC.Spread.Sheets.Comments.Comment();
                    comment.text('公式：\n' + insertFlag(jsonrow["gs"], '\n', 20));
                    comment.autoSize(true);
                    cel.comment(comment);
                }
                Lastsheet = sheet;
            }
        }
        //重新绘制Excel
        Lastsheet.resumePaint();
        protectallsheet();
    }
    return true;
}

//按默认颜色解锁同时隐藏各标签页中第一行第一列：
function AfterOpen(_issaved) {
    //如果不是本地打开：
    if (!localopen) 
    {
        dialogshowwin();
        dialogshow('保护填报页......');
        //如果不是生产方案类型模版：
        if (ParamDic["MBLX"] != "1") {
            dialogshow('隐藏首行首列......');
            hideallrowcol();
            dialogshow('隐藏无效列......');
            //根据月份隐藏列：
            hiddenmonthcol(ParamDic["NN"], ParamDic["FALB"]);
            dialogshow('计算列......');
            //根据模板计算列，回调按公式隐藏列：
            calcolsbytemplet(ParamDic["MBDM"],ParamDic["MBLB"]);
            dialogshow('锁定相关单元格......');
            protectworkbook();
            //如果当前是不能保存的则锁定所有标签页：
            if (QX["BC"] != 1) 
            {
                lockExcelAllCells();
            } 
            else 
            {
                lockallsheet();
            }
        }
        else 
        {
            dialogshow('隐藏首列......');
            pohidefirstcol(GetFirstSheet());
            dialogshow('锁定相关单元格......');
            lockfirstsheet();
        }
        dialogclose();
        //如果是打开上报文件，当前状态先设置为已保存；如果是打开模板文件，当前状态设置为未保存：
        if (_issaved == true) 
        {
            $("#HidBC").val("1");
        }
        else 
        {
            $("#HidBC").val("0");
        }
//        po.application().DisplayFormulaBar = true;
    }
    else 
    {
        lockallsheet();
    }
}

//解锁第一个标签页指定颜色区域：
function lockfirstsheet(_color) {
    if (spread != null && spread != undefined) 
    {
        var sheet = spread.sheets[0];
        //结束Sheet页编辑
        EndSheetEdit(sheet);
        if (sheet) 
        {
            sheet.options.isProtected = false;
        }
    }
}

//Excel密码保护
function protectworkbook() {
    var sheet;
    if (spread != null && spread != undefined) {
        for (var i = 0; i < spread.getSheetCount(); i++) {
            sheet = spread.sheets[i];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            sheet.options.isProtected = true;
        }
    }
}

//保护工作表
function protectsheet(_sheet, _pswd) {
    if (!_sheet) 
    {
        _sheet = activesheet();
    }
    else 
    {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    _sheet.options.isProtected = true;

}

//锁定当前所有标签页
//原lockworkbook()方法
function lockExcelAllCells() 
{
    if (spread != null && spread != undefined) 
    {
        for (var idx = 0; idx < spread.getSheetCount(); idx++) 
        {
            var sheet = spread.sheets[idx];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            dialogshow('完全锁定第' + (idx+1).toString() + '标签页：【' + sheet.name() + '】数据......');
            unprotectsheet(sheet);
            sheet.getRange(-1, 0, -1, sheet.getColumnCount(), GC.Spread.Sheets.SheetArea.viewport).locked(true);
            sheet.options.protectionOptions = {
                allowSelectLockedCells: true
            };
            protectsheet(sheet);
        }
    }
}

//取消保护工作表：
function unprotectsheet(_sheet, _pswd) 
{
    if (!_sheet) 
    {
        _sheet = activesheet();
    }
    else 
    {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    _sheet.options.isProtected = false;

}

//根据sheet页名称获取sheet：
function getsheetbyname(_name) {
    if (_name) {
        if (spread != null && spread != undefined) {
            for (var idx = 0; idx < spread.getSheetCount(); idx++) {
                var sheet = spread.sheets[idx];
                if (sheet.name().trim() == _name.trim()) {
                    return sheet;
                }
            }
        }
    }
    return null;
}

/// <summary>根据模板设置计算列</summary>
function calcolsbytemplet(MBDM,MBLB) {
    //3：编制,4:批复,5：分解
    var mblx = '0';
    if (MBLB == 'basesb' || MBLB == 'basesp' || MBLB == 'baseys') {
        mblx = '3';
        //批复:
    } else if (MBLB == 'respfj' || MBLB == 'respsp' || MBLB == 'respys') {
        mblx = '4';
        //分解:
    } else if (MBLB == 'resosb' || MBLB == 'resosp' || MBLB == 'resoys') {
        mblx = '5';
    }
    if (mblx != '0') {
        $.ajax({
            type: 'get',
            url: rootPath + 'action=calcolsbytemplet&mbdm=' + MBDM + '&mblx=' + mblx,
            cache: false,
            async: false,
            success: function (result) {
                //计算指定列：
                if (result != '') {
                    try {
                        var calcols = eval('(' + result + ')');
                    }
                    catch (e) {
                        BmShowWinError('公式解析错误!', result);
                    }
                    for (var i = 0; i < calcols['sheets'].length; i++) {
                        var sheetname = calcols['sheets'][i]["name"];
                        var cols = calcols['sheets'][i]["cols"];
                        var sheet = getsheetbyname(sheetname);
                        //结束Sheet页编辑
                        EndSheetEdit(sheet);
                        if (sheet != null) {
                            if (cols != '') {
                                var Address = "";
                                var rowCount = sheet.getRowCount();
                                //获取列地址
                                Address = GetColAddress(cols, rowCount);
                                //此处区域计算不提示计算完成：
                                spread.setActiveSheet(sheetname.trim());
                                //区域计算
                                ZoneCal(false,false, Address);
                            }
                        }
                        else {
                            alert('标签页：【' + sheetname + '】不存在！');
                        }
                    }
                    //根据公式隐藏列：
                    dialogshow('按公式隐藏列......');
                    hidecolbyformula();
                }
            },
            error: function (req) {
                BmShowWinError('获取按公式隐藏列错误！', req.responseText);
            }
        });
    }
}

//获取标签页名称按','分割
function getsheetnames() {
    var sheet;
    var res = "";
    if (spread != null && spread != undefined) 
    {
        for (var idx = 0; idx < spread.getSheetCount(); idx++) {
            sheet = spread.sheets[idx];
            res=res == "" ? sheet.name() : res + "," + sheet.name();   
        }
    }
    else 
    {
        alert("对象初始化错误！");
    }
    return res.trim();
}

//原pageoffice中的hidecolbyformula(_hidejson)
//解析json隐藏相关数据列：
function hidecolbyformula2(_hidejson) 
{
    if (spread != null && spread != undefined) 
    {
        for (var i = 0; i < _hidejson['sheets'].length; i++) 
        {
            var sheetname = _hidejson['sheets'][i]["name"];
            var cols = _hidejson['sheets'][i]["cols"];
            var sheet = getsheetbyname(sheetname);
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            if (sheet != null) 
            {
                if (cols != '') 
                {
                    var arycol = cols.split(',');
                    for (var j = 0; j < arycol.length; j++) 
                    {
                        sheet.setColumnVisible(GetColIndexByColChar(arycol[j]), false);
                    }
                }
            }
            else 
            {
                alert('标签页：【' + sheetname + '】不存在！');
            }
        }
    }
    else 
    {
        alert("未获取初始化对象！");
    }
}

//根据公式隐藏列
function hidecolbyformula() {
    //0：填报，1：分解,2:批复
    var mblx = '0';
    //分解:
    if (ParamDic["MBLB"] == 'resosb' || ParamDic["MBLB"] == 'resosp' || ParamDic["MBLB"] == 'resoys') {
        mblx = '1';
        //批复:
    } else if (ParamDic["MBLB"] == 'respfj' || ParamDic["MBLB"] == 'respsp' || ParamDic["MBLB"] == 'respys') {
        mblx = '2';
    }
    //sheet页名字字符串：
    var sheetnames = "{\"MBDM\":\""+ParamDic["MBDM"]+"\",\"USERID\":\""+ParamDic["USERDM"]+"\",\"MBLX\":\"" + mblx
            + "\",\"NN\":\""+ParamDic["NN"]+"\",\"SHEETNAMES\":\"" + getsheetnames() + "\"}";
    var url = rootPath+'action=hidecolbyformula';
    ajaxpost(url, false, sheetnames, function (xmlHttp) {
        if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
            // 本地提示：加载中/处理中 
        }
        else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) 
        {
            //隐藏指定列：
            if (xmlHttp.responseText != '') 
            {
                try 
                {
                    var hideformula = eval('(' + xmlHttp.responseText + ')');
                    hidecolbyformula2(hideformula);
                }
                catch (e) 
                {
                    BmShowWinError('公式解析错误!', xmlHttp.responseText);
                }
            }
        }
        else 
        {
            BmShowWinError('获取按公式隐藏列错误！', xmlHttp.responseText);
        }
    });
}
//全区域计算：
function AllZoneCal() {
    var sheet;
    for (var i = 0; i < spread.getSheetCount(); i++) {
        sheet = spread.sheets[i];
        //结束Sheet页编辑
        EndSheetEdit(sheet);
    }
    var shint = '正在计算区域数据，请稍候......';
    dialogshowwin();
    dialogshow(shint, true);
    //请求区域计算数据：
    $.ajax({
        type: 'get',
        url: "../Excels/Data/MBDataHandler.ashx",
        data: {
            action: "QQYJS",
            GSYY: GSYY,
            GSMM: GSMM,
            YY: ParamDic["YY"],
            FADM: ParamDic["FADM"],
            FADM2: ParamDic["FADM2"],
            FABS: ParamDic["FALB"],
            FileName: ParamDic["MBWJ"]
        },
        cache: false,
        success: function (result) {
            shint = '回写区域计算数据......';
            dialogshow(shint);
            result = result.replace(/\r/g, "").replace(/\n/g, "");
            try {
                var sucjson = eval("(" + result + ")");
            } catch (e) {
                BmShowWinError('公式解析错误!', result);
            }
            if (sucjson) {
                if (parseInt(sucjson["count"]) > 0) {
                    if (!writezone(sucjson,
                        ParamDic["YY"],
                        ParamDic["NN"],
                        ParamDic["JD"],
                        ParamDic["FALB"],
                        bnyy.toString(),
                        bnnn.toString(),
                        ParamDic["YSYF"])) {
                        alert("区域计算回写错误！");
                    } else {
                        alert('区域计算数据生成完毕！');
                    }
                } else {
                    alert('区域计算数据生成完毕！');
                }
            }
            dialogclose();
        },
        error: function (req) {
            dialogclose();
            BmShowWinError('请求区域计算数据错误!', req.responseText);
        }
    });
}

//获取当前Sheet页中的所有选中区域
//function GetSheetAllAreaStr() 
//{
//    var Address = "";
//    var selections = spread.getActiveSheet().getSelections();
//    if (selections != null) 
//    {
//        for (var i = 0; i < selections.length; i++) 
//        {
//            if (Address == "") 
//            {
//                if (selections[i].colCount== 1 && selections[i].rowCount==1) 
//                {
//                    Address = "$" + GetColCharByColIndex(selections[i].col) + "$" + selections[i].row+1;
//                }
//                else 
//                {
//                    Address = "$" + GetColCharByColIndex(selections[i].col) + "$" + (selections[i].row+1) + ":$" + GetColCharByColIndex(selections[i].col + selections[i].colCount - 1) + "$" + (selections[i].row + selections[i].rowCount);
//                }
//            }
//            else 
//            {
//                if (selections[i].colCount == 1 && selections[i].rowCount == 1) 
//                {
//                    Address = Address + ",$" + GetColCharByColIndex(selections[i].col) + "$" + selections[i].row+1;
//                }
//                else 
//                {
//                    Address = Address + ",$" + GetColCharByColIndex(selections[i].col) + "$" + (selections[i].row+1) + ":$" + GetColCharByColIndex(selections[i].col + selections[i].colCount - 1) + "$" + (selections[i].row + selections[i].rowCount);
//                }
//            }
//        }
//    }
//    return Address;
//}

//获取当前Sheet页中的所有选中区域
function GetSheetAllAreaStr() {
    var Address = "";
    if (spread != null && spread != undefined) {
        for (var k = 0; k < spread.getSheetCount(); k++) {
            var sheet = spread.sheets[k];
            var selections = sheet.getSelections();
            if (selections != null) {
                for (var i = 0; i < selections.length; i++) {
                    if (Address == "") {

                        Address = sheet.name()+","+ selections[i].row + "," + selections[i].rowCount + "," + selections[i].col + "," + selections[i].colCount;
                    }
                    else {
                        Address = Address + "|" + sheet.name() + "," + selections[i].row + "," + selections[i].rowCount + "," + selections[i].col + "," + selections[i].colCount;
                    }
                }
            }
        }
   
    }
    return Address;
}

//区域计算：
function ZoneCal(_async, _isconfirm, _addr) {
    if (_addr == "" || _addr == null || _addr == undefined) {
        _addr = GetSheetAllAreaStr();
    }
    //请求当前选中区域数据解析结果：
    if (_isconfirm == false) 
    {
        dealZoneCal(_addr, _async, _isconfirm);
    }
    else 
    {
        $.messager.confirm('提示：', '是否重新计算当前选定数据?', function (r) {
            if (r) {
                dialogshowwin();
                dealZoneCal(_addr, true, true, function () {
                    dialogclose();
                });
            }
        });
    }
}

function dealZoneCal(_addr, _async, _isconfirm, _fn) {
    if (_addr == "" || _addr == null || _addr == undefined) {
        $.messager.alert("提示框", "请选择要计算的区域！");
        return;
    }
    //结束Sheet页编辑
    for (var i = 0; i < spread.getSheetCount(); i++) 
    {
        sheet = spread.sheets[i];
        EndSheetEdit(sheet);
    }
    var shint = '正在计算区域数据，请稍候......';
    dialogshow(shint, true);
    if (_async != false) {
        _async = true;
    }
    $.ajax({
        type: 'post',
        url: '../Data/MBDataHandler.ashx',
        data: {
            action: "QYJS",
            GSYY: GSYY,
            GSNN: GSMM,
            Address: _addr,
            FADM: ParamDic["FADM"],
            FADM2: ParamDic["FADM2"],
            FABS: ParamDic["FALB"],
            FileName: ParamDic["MBWJ"],
            serveryy: ParamDic["YY"]
        },
        async: _async,
        complete: function (xmlHttp) {
            if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                // 本地提示：加载中/处理中 
            }
            else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                if (xmlHttp.responseText != '') {
                    try {
                        shint = '回写区域计算数据......';
                        dialogshow(shint);
                        var sucjson = eval('(' + xmlHttp.responseText + ')');
                    } catch (e) {
                        BmShowWinError('回写区域计算数据错误!', xmlHttp.responseText);
                    }
                    if (sucjson) {
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!writezone(sucjson, ParamDic["YY"], ParamDic["NN"], ParamDic["JD"], ParamDic["FALB"], bnyy.toString(), bnnn.toString(), ParamDic["YSYF"])) {
                                alert("区域计算回写错误！");
                            }
                        }
                        if (_isconfirm == true) {
                            alert('区域计算数据生成完毕！');
                        };
                    }

                }
                if (_fn != undefined) {
                    _fn();
                }
            } else {
                dialogclose();
                BmShowWinError('请求区域计算数据错误!', xmlHttp.responseText);

            }
        }
    });
}

//根据json格式回写excel:
function writezone(_json, _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf) {
    var Lastsheet;
    //结束Sheet页编辑
    for (var i = 0; i < spread.getSheetCount(); i++) {
        sheet = spread.sheets[i];
        EndSheetEdit(sheet);
    }
    if (spread != null && spread != undefined) 
    {
        unprotectallsheet();
        spread.suspendPaint();
        spread.suspendCalcService();
        var icount = _json["count"];
        var idx = -1;
        for (var i = 0; i < icount; i++) 
        {
            var jsonrow = _json["grids"][i];
            var sheet = getsheetbyname(jsonrow["name"]);
            if (sheet) 
            {
//                if (Lastsheet == null || Lastsheet == undefined || Lastsheet == "") 
//                {
//                    sheet.suspendPaint();
//                    sheet.suspendCalcService(false);
//                }
//                else 
//                {
//                    //如果发现前一个单元格和现在操作的单元格不属于同一个单元格的话，则进行上一个sheet页重绘
//                    if (Lastsheet.name() != sheet.name()) 
//                    {
//                        //重新绘制Excel
//                        Lastsheet.resumeCalcService(true);
//                        Lastsheet.resumePaint();
//                        sheet.suspendPaint();
//                        sheet.suspendCalcService(false);
//                    }
//                }
                var cel = sheet.getCell(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]));
                //设置区域计算单元格精度：
                if (jsonrow["formatter"] != "") 
                {
                    var style = sheet.getStyle(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]));
                    style.formatter = jsonrow["formatter"];
                    sheet.setStyle(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]), style);
                }

                if (jsonrow["gs"].indexOf('~~') >= 0) 
                {
                    cel.value(setparam(jsonrow["gs"], _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf));
                }
                else
                    cel.formula(jsonrow["gs"]);
                sheet.comments.remove(parseInt(jsonrow["row"]), parseInt(jsonrow["col"]));
                if (cel.formula() != null && cel.formula() != "" && cel.formula()!=undefined) 
                {
                    //添加公式备注
                    var comment = new GC.Spread.Sheets.Comments.Comment();
                    comment.text('公式：\n' + insertFlag(jsonrow["gs"], '\n', 20));
                    comment.autoSize(true);
                    cel.comment(comment);
                }
                //Lastsheet = sheet;
            }
        }
        //重新绘制Excel
        spread.resumeCalcService();
        spread.resumePaint();
        protectallsheet();
    }
    return true;
}

//Ajax Post提交方法
function ajaxpost(_url, _async, _strjson, _fn) {
    //添加随机数

    _url += '&rnd=' + Math.random();

    $.ajax({
        type: 'post',
        url: _url,
        cache: false,
        async: _async,
        data: {
            ParamStr: _strjson
        },
        complete: function (xmlHttp) {
            _fn(xmlHttp);
        }
    });
}
//col2num
//将列字母（例如：A、B、C）转化成0、1、2下标
function GetColIndexByColChar(str) {
    var Index=0;
    //A的码表大小
    str = str.toUpperCase();
    var base = 'A'.charCodeAt(0);
    for (var i = 0; i < str.length; i++) {
        Index = Index * 26 + str.charCodeAt(i) - base + 1;
    }
    return Index - 1;
}

/**
* 将数字索引转换为英文字母
* @param {Number} colIndex
*/
//原num2col()方法
function GetColCharByColIndex(colIndex) {
    if (colIndex < 0) {
        return "";
    }
    var str = "";
    var result = "";
    var A = 'A';
    while (colIndex != -1) {
        var num = (colIndex + 1) % 26; // 取余
        var c = A.charCodeAt(0) + num - 1;
        colIndex = Math.floor((colIndex + 1) / 26) - 1; //返回值小于等于其数值参数的最大整数值。
        // 对于26的特殊处理
        if (num == 0) {
            str = 'Z';
            colIndex -= 1; //退位
        } else {
            str = String.fromCharCode(c);
        }
        // 3.插入
        result += str;
    }
    if (result.length > 1) {
        result = result.split('').reverse().join("");
    }
    return result;
}

////将区域计算列字母转化成对应的下标
//function GetColAddress(str, RowCount) {
//    var Add = "", LastCol="";
//    var CurrentIndex = 0, Len = 0,LastIndex = 0;
//    //转化所有的字母为大写字母
//    str = str.toUpperCase();
//    var Arr = str.split(',');
//    var base = 'A'.charCodeAt(0); //找到A的码表大小
//    for (var i = 0; i < Arr.length; i++) 
//    {
//        if (Add == "") 
//        {
//            Add = "$" + Arr[i] + "$0";
//            //获取英文字母列对应的列下表
//            LastIndex = GetColIndexByColChar(Arr[i]);
//        }
//        else 
//        {
//            //获取英文字母列对应的列下表
//            CurrentIndex = GetColIndexByColChar(Arr[i]);
//            if (CurrentIndex - LastIndex != 1) 
//            {
//                Add = Add + ":$" + LastCol + "$" + RowCount + ",$" + Arr[i] + "$0";
//               
//            }
//            LastIndex = CurrentIndex;
//        }

//        if (i == Arr.length - 1) 
//        {
//            Add = Add + ":$" + Arr[i] + "$" + RowCount;
//        }
//        LastCol = Arr[i];
//    }
//    return Add;
//}

//将区域计算列字母转化成对应的下标
function GetColAddress(str, RowCount) 
{
    var row = 0;
    var Add = "", LastCol = "",col ="",qy;
    var CurrentIndex = 0, Len = 0, LastIndex = -1;
    //转化所有的字母为大写字母
    str = str.toUpperCase();
    var Arr = str.split(',');
    var base = 'A'.charCodeAt(0); //找到A的码表大小
    for (var i = 0; i < Arr.length; i++) 
    {
        if (col == "") 
        {
            col = Arr[i];
        }
        //获取英文字母列对应的列下表
        CurrentIndex = GetColIndexByColChar(Arr[i]);
        if (LastIndex != -1) 
        {
            if (CurrentIndex - LastIndex != 1) 
            {
                qy = "0," + RowCount + "," + GetColIndexByColChar(col) + "," + (LastIndex - GetColIndexByColChar(col) + 1).toString();
                Add = add == "" ? qy : Add + "|" + qy;
                col = Arr[i];
                LastIndex = CurrentIndex;
            }
            else
            {
                //获取英文字母列对应的列下表
                LastIndex = CurrentIndex;
            }
        }
        else
        {
            //获取英文字母列对应的列下表
            LastIndex = CurrentIndex;
        }
        if (i == Arr.length - 1) 
        {
            qy = "0," + RowCount + "," + GetColIndexByColChar(col) + "," + (CurrentIndex - GetColIndexByColChar(col) + 1).toString();
            Add = add == "" ? qy : Add + "|" + qy;
        }
    }
    return Add;
}

//去除合计列
//如果当前选择月份是12月份，隐藏剩余月份预计列
function hiddenmonthcol(_nn, _falb) {
    if (_nn == "12" && _falb == "1") 
    {
        for (var idx = 0; idx < spread.getSheetCount(); idx++) 
        {
            var sheet = spread.sheets[idx];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            for (var i = 0; i < sheet.getColumnCount(); i++) 
            {
                var cel = sheet.getCell(0, i);
                if (cel != undefined) 
                {
                    if (cel.Formula.indexOf("SYYFYJ") > -1) 
                    {
                        var address = cel.Address;
                        var LH = address.split('$')[1];
                        sheet.setColumnVisible(GetColIndexByColChar(LH), false);
                    }
                }
            }
        }
    }
}

//获取当前Excel激活页
function activesheet() {
    if (spread != null && spread != undefined) {
        var sheet = spread.getActiveSheet();
        //结束Sheet页编辑
        EndSheetEdit(sheet);
        return sheet;
    }
    else {
        alert("未获取初始化对象！");
    }
}

//隐藏行：
function hidefirstrow(_sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    }
    else {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    _sheet.setRowVisible(0, false);
}

//隐藏列:
function pohidefirstcol(_sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    //显示所有的列，在隐藏第一列
    for (var i = 0; i < _sheet.getColumnCount(); i++) {
        _sheet.setColumnVisible(i,true);
    }
    _sheet.setColumnVisible(0, false);
}

//隐藏当前所有标签页第一行，第一列：
function hideallrowcol() {
    if (spread != null && spread != undefined) {
        var scount = spread.getSheetCount();
        for (var idx =0; idx < scount; idx++) {
            var sheet = spread.sheets[idx];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            pohidefirstcol(sheet);
            hidefirstrow(sheet);
        }
    }
}

//function setgs(_json, _aryrag) {
//    var sheet;
//    if (spread != null && spread != undefined) {
//        var icount = _json["count"];
//        for (var j = 0; j < spread.getSheetCount(); j++) 
//        {
//            sheet = spread.sheets[j];
//            //结束Sheet页编辑
//            EndSheetEdit(sheet);
//        }
//        unprotectallsheet();
//        for (var i = 0; i < icount; i++) {
//            var gs = _json["grids"][i]["gs"];
//            var gscom = insertFlag(gs, '\n', 20);
//            _aryrag[i].formula(gs);
//            //添加公式备注
//            var comment = new GC.Spread.Sheets.Comments.Comment();
//            comment.text('公式：\n' + gscom);
//            comment.autoSize(true);
//            _aryrag[i].comment(comment);
//        }
//        protectallsheet();
//    }
//    return true;
//}

//在字符串指定位置插入回车换行符：
function insertFlag(_str, _flg, _sn) {
    var newstr = "";
    for (var i = 0; i < _str.length; i += _sn) {
        var tmp = _str.substring(i, i + _sn);
        newstr += tmp + _flg;
    }
    return newstr;
}

//锁定sheet页
function lockallsheet() {
    if (spread != null && spread != undefined) {
        for (var i = 0; i < spread.getSheetCount(); i++) {
            var sheet = spread.sheets[i];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            sheet.options.isProtected = true;
            dialogshow('锁定第' + (i+1).toString() + '标签页：【' + sheet.name() + '】数据......');
        }
    }
}

/// <summary>获取服务器年、月</summary>
function GetYyNn() {
    $.ajax({
        type: 'get',
        url: rootPath + 'action=getyynn',
        cache: false,
        success: function (result) {
            if (result != null && result != '') {
                bnyy = result.toString().substr(0, 4);
                bnnn = result.toString().substr(4, 2);
            }
        },
        error: function (req) {
            dialogclose();
            BmShowWinError('获取服务器年月错误!', req.responseText);
        }
    });
}

//添加不平衡模板：
function InitImbal() {
    $('#GrdImba').datagrid({
        autoRowHeight: false,
        rownumbers: true,
        singleSelect: true,
        striped: true,
        loadMsg: '正在加载数据.....',
        columns: [[
            { field: 'CPDM', title: '产品代码', hidden: true },
		    { field: 'CPMC', title: '产品名称', width: 120 },
            { field: 'CL', title: '产量', width: 80 },
            { field: 'LXMC', title: '流向名称', width: 120 },
            { field: 'ZFL', title: '流量', width: 80 },
            { field: 'ZYBM', title: '作业编码', hidden: true },
            { field: 'CPBM', title: '产品编码', hidden: true },
            { field: 'CE', title: '差量', width: 80 },
            { field: 'BS', title: '标识', hidden: true },
	    ]],
        onHeaderContextMenu: function (e, field) {
            e.preventDefault();
            if (!cmenu) {
                createColumnMenu();
            }
            cmenu.menu('show', {
                left: e.pageX,
                top: e.pageY
            });
        }
    });
}

function pagerFilter(data) {
    if (data) {
        if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
            data = {
                total: data.length,
                rows: data
            }
        }

        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            loading: true,
            showRefresh: false,
            onSelectPage: function (pageNum, pageSize) {
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh', {
                    pageNumber: pageNum,
                    pageSize: pageSize
                });
                dg.datagrid('loadData', data);
            }
        });
        if (!data.originalRows) {
            data.originalRows = (data.rows);
        }
        var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));
        return data;
    }
}

function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#GrdImba').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#GrdImba').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#GrdImba').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#GrdImba').datagrid('getColumnOption', field);
        if (col.hidden) {
            cmenu.menu('appendItem', {
                text: col.title,
                name: field,
                iconCls: 'icon-empty'
            });
        } else {
            cmenu.menu('appendItem', {
                text: col.title,
                name: field,
                iconCls: 'icon-ok'
            });
        }
    }
}

/**
* 获取要保存的数据文件名：
* 
*/
function getsavefilename(_jhfadm, _mbwj) {
    if (_jhfadm && _mbwj) {
        return "Jhfadm~" + _jhfadm + "~Mb~" + _mbwj;
    }
}

//查询服务器的url是否存在：
function fileexist(_url, _fnsucess, _fnerror) {
    $.ajax({
        type: 'get',
        url: _url,
        cache: false,
        complete: function (xmlhttp) {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    _fnsucess();
                }
                else if (xmlhttp.status == 404) {
                    _fnerror();
                }
            }
        }
    });
}
//判断打开模板还是文件
function IsExcelOrJsonExisted(filename, jsonname) {
    var path = geturlpath();
    $.ajax({
        type: "post",
        url: path + "ExcelData.ashx?action=IsExcelOrJsonExisted",
        data: {
            filename: filename,
            jsonname: jsonname
        },
        success: function (result) {
            if (result == "1") {
                dialogshowwin();
                dialogshow('存在已填报文件,正在加载......', true);
                //打开Spread JS的Excel页面
                $("#HidSaveFileName").val(filename);
                //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
                openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), true);
            } else {
                fileexist('../Excels/Xls/' + ParamDic["MBWJ"],
                    //如果存在模板文件：
                    function () {
                        dialogshowwin();
                        dialogshow('不存在已填报文件,正在加载模板......', true);
                        //打开Spread JS的Excel页面
                        $("#HidSaveFileName").val(ParamDic["MBWJ"]);
                        openExcelOrJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                        //openJson(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                        //openexcel(row + "&FileName=" + $("#HidSaveFileName").val(), false);
                    },
                    function () {
                        alert('模板文件和数据文件均不存在，请检测文件是否存在！\n \n 有可能服务器文件损坏导致......');
                        return;
                    }
                );
            }
        },
        error: function (e) {
            alert(e);
        }
    })
}
//获取当前页面公式并返回json(通过find内置函数提高效率)：
function getgs(_yy, _nn, _jd, _fadm1, _fadm2, _fabs, _bnyy, _bnnn, _ysyf, _aryrag, _loginyy) {
    if (spread != null && spread!=undefined) 
    {
        //获取当前Excel所有的Sheet页个数
        var scount = spread.getSheetCount();
        var resjson = "\"grids\":[";
        var icount = 0;
        for (var idx = 0; idx <scount; idx++) 
        {
            var sheet = spread.sheets[idx];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            //停止绘制
            sheet.suspendPaint();
            //取消sheet页保护
            sheet.options.isProtected = false;
            dialogshow('获取第' + (idx+1).toString() + '标签页：【' + sheet.name() + '】公式......');
            var Formula = '', Cel;
            for (var i = 0; i < sheet.getRowCount(); i++) 
            {
                for (var j = 0; j < sheet.getColumnCount(); j++) {
                    Cel = sheet.getCell(i, j);
                    if (Cel != undefined) 
                    {
                        var Formula = Cel.formula();
                        if (Formula != "" && Formula != undefined) 
                        {
                            if (Formula.indexOf("N_") >= 0) 
                            {
                                _aryrag.push(Cel);
                                icount++;
                                if (resjson != "\"grids\":[") 
                                {
                                    resjson += ",";
                                }
                                Formula = Formula.replace(/N_/g, "");
                                if (Formula.indexOf('=') == 0) 
                                {
                                    Formula = Formula.substring(1, Formula.length);
                                }
                                //公式中如果存在回车或换行符则替换：
                                Formula = Formula.replace(/"/g, "\\\"").replace(/\r/g, '').replace(/\n/g, '');
                                resjson += "{\"idx\":" + idx + ",\"row\":" + i + ",\"col\":" + j + ",\"gs\":\"" + Formula + "\"}";
                            }
                        }
                        if (Cel.value() != null && Cel.value() != undefined && Cel.value() != "") {
                            //替换Excel文件中的年、月、季度
                            if (Cel.value().toString().indexOf("~~") > -1) {
                                //清除单元格公式
                                ClearCellFormula(Cel);
                                Cel.value(setparam(Cel.value(), _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf));
                            }
                        }
                    }
               }
           }
           sheet.resumePaint();
           sheet.options.isProtected = true;
        }
        resjson += "]";
        resjson = "({\"count\":" + icount + ",\"yy\":\"" + _bnyy + "\",\"nn\":\"" + _bnnn
                        + "\",\"fadm\":\"" + _fadm1 + "\",\"fadm2\":\"" + _fadm2 + "\",\"fabs\":\""
                        + _fabs + "\",\"loginyy\":\"" + _loginyy + "\"," + resjson + "})";
        return eval(resjson);
    }
}

//取消保护所有工作表：
function unprotectallsheet() {
    if (spread != null && spread != undefined) {
        for (var i = 0; i < spread.getSheetCount(); i++) {
            var sheet = spread.sheets[i];
            //结束Sheet页编辑
            EndSheetEdit(sheet);
            //取消sheet页保护
            sheet.options.isProtected = false;
        }
    }
}

//保护所有工作表：
function protectallsheet() {
    if (spread != null && spread != undefined) {
        for (var i = 0; i < spread.getSheetCount(); i++) {
            var sheet = spread.sheets[i];
            //sheet页保护
            sheet.options.isProtected = true;
        }
    }
}

//替换年、月、季度等参数：
function setparam(_val, _yy, _nn, _jd, _fabs, _bnyy, _bnnn, _ysyf) {
    if (!_fabs) {
        _fabs = '1';
    }
    if (!_bnyy) {
        _bnyy = _yy;
    }
    if (!_ysyf) {
        _ysyf = _nn;
    }
    if (_nn.indexOf('0') == 0 && _nn.length == 2) {
        _nn = _nn.substring(1);
    }
    if (_ysyf.indexOf('0') == 0 && _ysyf.length == 2) {
        _ysyf = _ysyf.substring(1);
    }
    if (_bnnn.indexOf('0') == 0 && _bnnn.length == 2) {
        _bnnn = _bnnn.substring(1);
    }
    _val = _val.toString().trim().replace(/~~bnyy/g, _bnyy + '年').replace(/~~BNYY/g, _bnyy + '年');
    //更改bnnn和snn替换：
    //        _val = _val.toString().replace(/~~BNNN/g, _ysyf + '月').replace(/~~bnnn/g, _ysyf + '月');
    //        _val = _val.toString().replace(/~~SNN/g, _ysyf + '月').replace(/~~snn/g, _ysyf + '月');

    switch (_fabs) {
        //月：                                                                                                      
        case '1':
            //其他
        case '4':
            _val = _val.toString().trim().replace(/~~yy/g, _yy + '年').replace(/~~nn/g, _nn + '月').replace(/~~jd/g, '');
            _val = _val.toString().trim().replace(/~~YY/g, _yy + '年').replace(/~~NN/g, _nn + '月').replace(/~~JD/g, '');
            var isnn = parseInt(_nn);
            isnn = (isnn == 1) ? isnn : isnn - 1;
            //                _val = _val.toString().trim().replace(/~~snn/g, isnn.toString() + '月').replace(/~~SNN/g, isnn.toString() + '月');
            _val = _val.toString().trim().replace(/~~bnnn/g, isnn.toString() + '月').replace(/~~BNNN/g, isnn.toString() + '月');
            var ixnn = parseInt(_nn);
            ixnn = (ixnn == 12) ? ixnn : ixnn + 1;
            _val = _val.toString().trim().replace(/~~xnn/g, ixnn.toString() + '月').replace(/~~XNN/g, ixnn.toString() + '月');
            //SNN改成，如果计划方案月份<=预算月份，那么这个SNN应该是方案的月份-1，如果计划方案月份>预算月份，那么SNN应该=预算月份
            var iysyf = parseInt(_nn) <= parseInt(_ysyf) ? parseInt(_nn) - 1 : parseInt(_ysyf);
            _val = _val.toString().replace(/~~SNN/g, iysyf.toString() + '月').replace(/~~snn/g, iysyf.toString() + '月');
            break;
        //季度：                                                                                                      
        case '2':
            _val = _val.toString().trim().replace(/~~yy/g, _yy + '年').replace(/~~nn/g, '').replace(/~~jd/g, _jd + '季度');
            _val = _val.toString().trim().replace(/~~YY/g, _yy + '年').replace(/~~NN/g, '').replace(/~~JD/g, _jd + '季度');
            break;
        //年：                                                                                                      
        case '3':
            _val = _val.toString().trim().replace(/~~yy/g, _yy + '年').replace(/~~nn/g, '').replace(/~~jd/g, '');
            _val = _val.toString().trim().replace(/~~YY/g, _yy + '年').replace(/~~NN/g, '').replace(/~~JD/g, '');
            var isnn = parseInt(_bnnn);
            isnn = (isnn == 1) ? isnn : isnn - 1;
            //                _val = _val.toString().trim().replace(/~~snn/g, isnn.toString() + '月').replace(/~~SNN/g, isnn.toString() + '月');
            _val = _val.toString().trim().replace(/~~bnnn/g, isnn.toString() + '月').replace(/~~BNNN/g, isnn.toString() + '月');
            var ixnn = parseInt(_bnnn);
            ixnn = (ixnn == 12) ? ixnn : ixnn + 1;
            _val = _val.toString().trim().replace(/~~xnn/g, ixnn.toString() + '月').replace(/~~XNN/g, ixnn.toString() + '月');
            break;
    }
    //更改bnnn和snn替换：
    _val = _val.toString().replace(/~~SNN/g, _ysyf + '月').replace(/~~snn/g, _ysyf + '月');
    return _val;
}

//获取公式的年份、月份
function getGSYYMM(FADM,FALB,YY,YSYF) 
{
    $.ajax({
        type: 'get',
        url: '../Data/InitMBHandle.ashx',
        data: {
            action: 'getgsyymm',
            YY: YY,
            FADM: FADM,
            FALB: FALB,
            YSYF: YSYF
        },
        async: false,
        cache: false,
        success: function (result) {
            if (result != "" && result != null) {
                var Arr = result.split(',');
                GSYY = Arr[0].toString();
                GSMM = Arr[1].toString();
            }
        }
    });
}

//根据索引号获取sheet
function getsheetbyidx(_idx) {
    if (!_idx) {
        _idx = 0;
    }
    if (spread != null && spread != undefined) {
        if (_idx < spread.getSheetCount() && _idx >= 0)
            return spread.sheets[_idx];
    }
}

//清除第一列value值：
function clearfirstrow(_sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    var Cel;
    //判断sheet页有没有被保护，有的话，先解保护
    if (_sheet.options.isProtected) 
    {
        _sheet.suspendPaint(); 
        unprotectsheet(_sheet);
        for (var i = 0; i < _sheet.getColumnCount(); i++) {
            Cel = _sheet.getCell(i,0);
            //清除单元格公式
            ClearCellFormula(Cel);
            Cel.value(null);
        }
        _sheet.resumePaint();
        protectsheet(_sheet);

    }
    else 
    {
        _sheet.suspendPaint();
        for (var i = 0; i < _sheet.getColumnCount(); i++) {
            Cel = _sheet.getCell(i, 0);
            //清除单元格公式
            ClearCellFormula(Cel);
            Cel.value(null);
        }
        _sheet.resumePaint();
    }
}

//清除第一行value值：
function clearfirstcol(_sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束Sheet页编辑
        EndSheetEdit(_sheet);
    }
    var Cel;
    if (_sheet.options.isProtected) {
        _sheet.suspendPaint(); 
        unprotectsheet(_sheet);
        for (var i = 0; i < _sheet.getRowCount(); i++) {
            Cel = _sheet.getCell(0, i);
            //清除单元格公式
            ClearCellFormula(Cel);
            Cel.value(null);
        }
        _sheet.resumePaint();
        protectsheet(_sheet);
    }
    else 
    {
        _sheet.suspendPaint();
        for (var i = 0; i < _sheet.getRowCount(); i++) {
            Cel = _sheet.getCell(0, i);
            //清除单元格公式
            ClearCellFormula(Cel);
            Cel.value(null);
        }
        _sheet.resumePaint();
    }
}

//根据json格式回写excel行列属性:
function writerowcol(_json,sheetName) {
    if (spread != null && spread != undefined) {
        var icount = _json["count"];
        //取消单元格保护
        unprotectallsheet();
        var sheet = getsheetbyname(sheetName);
        //结束Sheet页编辑
        EndSheetEdit(sheet);
        //清空第一列
        clearfirstcol(sheet);
        //清空第一行
        clearfirstrow(sheet);
        sheet.suspendPaint();
        for (var i = 0; i < icount; i++) {
            var jsonrow = _json["grids"][i];
            if (sheet) 
            {
                sheet.getCell(parseInt(jsonrow["row"]), parseInt(jsonrow["col"])).value(jsonrow["gs"]);
            }
        }
        sheet.resumePaint();
        protectallsheet();
    }
    return true;
}


//获取模板行列属性：
function GetRowCol() {
//请求获取模板行列属性：
    $.messager.confirm('提示：',
        '是否重新获取模板行列属性?',
        function(r) {
            if (r) {
                var count = spread.getSheetCount();
                for (var i = 0; i < count; i++) {
                    var sheet = spread.getSheet(i);
                    var surl = '../Data/MBDataHandler.ashx?action=GetRowAndCol&FileName=' +
                        ParamDic["MBWJ"] +
                        '&sheetName=' +
                        sheet.name();
                    //结束Sheet页编辑
                    EndSheetEdit(sheet);
                    var shint = '正在获取后台行列属性，请稍候......';
                    dialogshowwin();
                    dialogshow(shint, true);
                    //请求获取行列属性数据：
                    $.ajax({
                        type: 'get',
                        url: surl,
                        cache: false,
                        async: false,
                        success: function(result) {
                            shint = '当前数据文件回写行列属性......';
                            dialogshow(shint);
                            result = result.replace(/\r/g, "").replace(/\n/g, "");
                            try {
                                var sucjson = eval("(" + result + ")");
                            } catch (e) {
                                BmShowWinError('公式解析错误!', result);
                            }
                            if (sucjson) {
                                if (parseInt(sucjson["count"]) > 0) {
                                    if (!writerowcol(sucjson, sheet.name())) {
                                        dialogclose();
                                        $.messager.alert("提示框", sheet.name() + "行列属性回写错误！");
                                    } else {
                                        dialogclose();
                                        $.messager.alert("提示框", sheet.name() + '行列属性回写完毕！');
                                    }
                                }
                            }
                            if (i == count - 1) {
                                dialogclose();
                            }
                        },
                        error: function(req) {
                            dialogclose();
                            BmShowWinError('行列属性获取错误!', req.responseText);
                        }
                    });
                }
                //var sheet = spread.getActiveSheet();
//                var surl = '../Excels/Data/MBDataHandler.ashx?action=GetRowAndCol&FileName=' +
//                    ParamDic["MBWJ"] +
//                    '&sheetName=' +
//                    sheet.name();
//                //结束Sheet页编辑
//                EndSheetEdit(sheet);
//                var shint = '正在获取后台行列属性，请稍候......';
//                dialogshowwin();
//                dialogshow(shint, true);
                //请求获取行列属性数据：
//                $.ajax({
//                    type: 'get',
//                    url: surl,
//                    cache: false,
//                    success: function(result) {
//                        shint = '当前数据文件回写行列属性......';
//                        dialogshow(shint);
//                        result = result.replace(/\r/g, "").replace(/\n/g, "");
//                        try {
//                            var sucjson = eval("(" + result + ")");
//                        } catch (e) {
//                            BmShowWinError('公式解析错误!', result);
//                        }
//                        if (sucjson) {
//                            if (parseInt(sucjson["count"]) > 0) {
//                                if (!writerowcol(sucjson, sheet.name())) {
//                                    dialogclose();
//                                    $.messager.alert("提示框", "行列属性回写错误！");
//                                } else {
//                                    dialogclose();
//                                    $.messager.alert("提示框", '行列属性回写完毕！');
//                                }
//                            }
//                        }
//                        dialogclose();
//                    },
//                    error: function(req) {
//                        dialogclose();
//                        BmShowWinError('行列属性获取错误!', req.responseText);
//                    }
//                });
            }
        },
        function() {});
}

//N_JYGS($B$1:$D$8,$E$1:$F$8)
//获取校验公式以N_JYGS开头的数据，此数据意思是对比指定列数据是否对应，比如数据列有数据，名称列必须不能为空
function JyTsgs(_tjflag) {
    var mbdm = ParamDic["MBDM"];
    var message = "";
    $.ajax({
        type: 'get',
        url: '../Data/MB.ashx',
        data: {
            action: 'getJygs',
            MBDM: mbdm
        },
        async: false,
        cache: false,
        success: function (result) {
            if (result != '') {
                //获取各校验公式：
                for (var i = 0; i < result.split('+').length; i++) {
                    var sheet = result.split('+')[i].split('|')[0];
                    var gs = result.split('+')[i].split('|')[1];
                    //解析校验公式：
                    message += jy(sheet, gs);
                }
            }
            //如果公式校验有问题，则提示：
            if (message != '') {
                alert('指定列校验错误：\n\r' + message);
                //如果公式校验正确则message没有返回值，进入提交申请或者审批
            } else {
             $.messager.confirm('提示：',
                '是否要保存数据和文件?',
                function(r) {
                    if (r) {
                        if (ParamDic["MBLX"] == '1') {
                            SaveTrCc(_fn);
                        } else {
                            SaveData(false, _fn);
                        }
                        //如果是"批复预算基础数据维护"
                        if ((ParamDic["MBLB"] == 'respfj') && (ParamDic["SFZLC"] == '0')) {
                            DealRespondStatus();
                        }
                    } else {
                        if (IsCheckFormula(_fn)) {
                            eval(_fn);
                        }
                    }
                },
                function() {});
                $("#HidTJ").val(_tjflag);
                //SaveData(false);
                //OpenSmqr(_tjflag);
            }
        },
        error: function (req) {
            dialogclose();
            BmShowWinError('请求校验数据错误!', req.responseText);
        }
    });
}

/// <summary>解析校验公式看是否有不符合条件的单元格</summary>
/// <returns type="String" />
/// <param name="_sheet" type="String">Sheet页名</param>
/// <param name="gs" type="String">要解析的公式</param>
function jy(_sheet, gs) {
    var message = '';
    if (_sheet) {
        var index = gs.indexOf("(");
        var _gs;
        _gs = gs.toString().trim().replace(/\r/g, '').replace(/\n/g, '');
        if (index > -1) {
            _gs = _gs.substring(index + 1, gs.length);
        }
        else {
            message = '校验公式有错误!';
            return message;
        }
        if (_gs) {
            var addr = _gs.split(',')[0];
            if (addr == "") {
                message = '校验公式有错误!';
                return message;
            }
            var JyqyAddr = _gs.split(',')[1].toString();
            if (JyqyAddr == "") {
                message = '校验公式有错误!';
                return message;
            }
            JyqyAddr = JyqyAddr.substring(0, JyqyAddr.length - 1);
            var JyTs = new Array();
            var qswzL = "";
            var qswzH = "";
            var zzwzL = "";
            var zzwzH = "";
            var JyqyQswzL = "";
            var JyqyQswzH = "";
            var JyqyZzwzL = "";
            var JyqyZzwzH = "";
            if (addr.split(':').length == 1) {
                //起始位置列
                qswzL = addr.split('$')[1].replace("$", "");
                //起始位置行
                qswzH = addr.split('$')[2].replace("$", "");
                //终止位置列
                zzwzL = addr.split('$')[1].replace("$", "");
                //终止位置行
                zzwzH = addr.split('$')[2].replace("$", "");
            }
            else {
                //起始位置列
                qswzL = addr.split(':')[0].split('$')[1].replace("$", "");
                //起始位置行
                qswzH = addr.split(':')[0].split('$')[2].replace("$", "");
                //终止位置列
                zzwzL = addr.split(':')[1].split('$')[1].replace("$", "");
                //终止位置行
                zzwzH = addr.split(':')[1].split('$')[2].replace("$", "");
            }
            if (JyqyAddr.split(':').length == 1) {
                //起始位置列
                JyqyQswzL = JyqyAddr.split('$')[1].replace("$", "");
                //起始位置行
                JyqyQswzH = JyqyAddr.split('$')[2].replace("$", "");
                //终止位置列
                JyqyZzwzL = JyqyAddr.split('$')[1].replace("$", "");
                //终止位置行
                JyqyZzwzH = JyqyAddr.split('$')[2].replace("$", "");
            }
            else {
                //起始位置列
                JyqyQswzL = JyqyAddr.split(':')[0].split('$')[1].replace("$", "");
                //起始位置行
                JyqyQswzH = JyqyAddr.split(':')[0].split('$')[2].replace("$", "");
                //终止位置列
                JyqyZzwzL = JyqyAddr.split(':')[1].split('$')[1].replace("$", "");
                //终止位置行
                JyqyZzwzH = JyqyAddr.split(':')[1].split('$')[2].replace("$", "");
            }
            if (qswzH != JyqyQswzH || zzwzH != JyqyZzwzH) {
                message = '起始位置行或终止位置行不对应，公式有误，请修改校验公式!';
                return message;
            }
            //当前sheet页
            var sheet = getsheetbyname(_sheet);
            if (sheet == null || sheet == undefined) {
                message = '[' + _sheet + ']标签页已不存在，请在模板设置中重新设置校验公式!';
                return message;
            }
            //获取首列的行列属性
            //数值列数组
            var arrSz = new Array();
            //名称对比列数组
            var arrSzJy = new Array();
            //获取所有数值列数据添加到数组中
            for (var j = parseInt(qswzH); j <= parseInt(zzwzH); j++) {
                //数值行数据
                var szhSj = "";
                for (var i = GetColIndexByColChar(qswzL); i <= GetColIndexByColChar(zzwzL); i++) {
                    var rowcolval = "";
                    //获取单元格的值
                    var value = sheet.getCell(j,i).value();
                    if (value != undefined && value != "" && value.toString().trim() != "") {
                        szhSj += "T";
                    }
                }
                arrSz.push(szhSj);
            }
            //获取所有数值对比列数据添加到数组中
            for (var j = parseInt(JyqyQswzH); j <= parseInt(JyqyZzwzH); j++) {
                //数值行数据
                var szhSjJY = "";
                for (var i = GetColIndexByColChar(JyqyQswzL); i <= GetColIndexByColChar(JyqyZzwzL); i++) {
                    var rowcolval = "";
                    var value = sheet.getCell(j, i).value();
                    if (value != undefined && value != "" && value.toString().trim() != "") {
                        szhSjJY += "T";
                    }
                }
                arrSzJy.push(szhSjJY);
            }
            //获取列数
            var Ls = GetColIndexByColChar(JyqyZzwzL) - GetColIndexByColChar(JyqyQswzL) + 1;
            for (var i = 0; i < arrSzJy.length; i++) {
                if (arrSz[i].toString() != "" && arrSzJy[i].toString().length != Ls) {
                    var HH = parseInt(i) + parseInt(JyqyQswzH);
                    message += sheet.name() + "页" + HH + "行名称或名称说明为空\n";
                }
            }
        }
    }
    return message;
}
//ajax 同步发送请求，确认是否有需要打开承诺书，需要打开返回true,否则返回false
function isOpenSmcn() {
    var bool = '0';
    $.ajax({
        url: rootPath+'action=isDisPlaySmcn&mbdm=' +ParamDic["MBDM"]+ '&userid=' +ParamDic["USERDM"],
        type: 'GET',
        async: false, //表示同步
        cache: false,
        success: function (data) {
            bool = data;
        }

    });
    return bool;
}
function selectValue(_tjflag) {
    var MBDM = ParamDic["MBDM"];
    var USER_ID = ParamDic["USERDM"];
    var res = "";
    $.ajax({
        url: rootPath+'action=getValue&MBDM=' + MBDM + '&USER_ID=' + USER_ID + '&FLAG=' + _tjflag + '',
        type: 'GET',
        async: false, //同步
        success: function (data) {
            res = data;
        }
    });
    return res;
}

function upData(_data, element, text) {
    _data = _data.replace(element, text);
    return _data;
}
function time() {
    var date = new Date();
    var year = date.getFullYear();
    var month = (date.getMonth() + 1) < 10 ? ("0" + "" + (date.getMonth() + 1)) : (date.getMonth() + 1);
    var day = date.getDate() < 10 ? ("0" + "" + date.getDate()) : date.getDate();
    var hour = date.getHours() < 10 ? ("0" + "" + date.getHours()) : date.getHours();
    var minute = date.getMinutes() < 10 ? ("0" + "" + date.getMinutes()) : date.getMinutes();
    var second = date.getSeconds() < 10 ? ("0" + "" + date.getSeconds()) : date.getSeconds();
    document.getElementById("TBSJ").value = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    document.getElementById("SHSJ").value = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
}

function OpenSmqr(_tjflag) {
    var boo = isOpenSmcn(); //显示承诺书返回1，不显示返回0
    if (boo == "0") {
        //执行操作：
         if($("#HidDH").val()=="")
    {
        $("#HidDH").val(ParamDic["MBDM"]);
    }
    if($("#HidJS").val()=="")
    {
        $("#HidJS").val(ParamDic["JSDM"]);
    }
        TJ($("#HidTJ").val(), $("#HidDH").val(), $("#HidJS").val());
        return;
    }
    var res = selectValue(_tjflag);
    var mblx = ParamDic["MBLB"];
    var context = res.split(','); //返回的部门内容，文字说明内容，用户信息
    var selects = context[0].split('|'); //返回的文字说明内容
    var department = context[1]; //返回的部门内容
    var user = context[2].split('|'); //返回的用户信息
    var username = user[0];
    var userphone = user[1];
    var option = "";
    for (var i = 0; i < selects.length; i++) {
        if (_tjflag == '0' && selects[i] == '不同意') {
            option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
        } else if (_tjflag == '-1' && selects[i] == '提交上报') {
            option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
        } else if (_tjflag == '1' && selects[i] == '同意') {
            option += "<option value ='" + selects[i] + "'selected = true>" + selects[i] + "</option>";
        } else {
            option += "<option value ='" + selects[i] + "'>" + selects[i] + "</option>";
        }
    }

    $.ajax({
        url: '../Data/SMCN.html', //这里是静态页的地址
        type: 'GET', //静态页用get方法，否则服务器会抛出405错误
        dataType: 'html',
        cache: false,
        success: function (data) {
            if (mblx == "basesb" || mblx == "resosb" || mblx == "baseys" || mblx == "resoys") {//当前菜单属于预算编制填报或与预算分解分解填报，预算编制-生成上报预算和预算分解-预算分解报表生成
                data = upData(data, "id=\"SH\"", "id=\"SH\" style=\"display:none;\"");
                data = upData(data, "id=\"TBR\"", "id=\"TBR\" value=\"" + username + "\"");
                data = upData(data, "id=\"TBRPHONE\"", "id=\"TBRPHONE\" value=\"" + userphone + "\"");
            }
            if (mblx == "basesp" || mblx == "resosp") {//当前菜单属于预算编制报表审批或预算分解报表审批
                data = upData(data, "id=\"TB\"", "id=\"TB\" style=\"display:none;\"");
                data = upData(data, "id=\"SHR\"", "id=\"SHR\" value=\"" + username + "\"");
                data = upData(data, "id=\"SHRPHONE\"", "id=\"SHRPHONE\" value=\"" + userphone + "\"");
            }
            if (_tjflag == '0') { //退回
                SmqrHeight = '450';
                $('#SMQR').panel('resize', { width: 610, height: SmqrHeight });
                data = upData(data, "</textarea>", "不同意" + "</textarea>");
            }
            if (_tjflag == '-1') { //上报
                data = upData(data, "</textarea>", "上报" + "</textarea>");
            }
            if (_tjflag == '1') {//审批
                data = upData(data, "</textarea>", "同意" + "</textarea>");
            }
            data = upData(data, "id=\"Department\"", "id=\"Department\" value=\"" + department + "\" readonly=\"readonly\"");
            data = upData(data, "</select>", option + "</select>");
            $('#SMQR').dialog({
                content: data,
                top: (_bodyHeight - SmqrHeight) / 2,
                left: (_bodyWidth - SmqrWidth) / 2,
                onClose: function () {
                }
            })
            //退回的时候重新布局设置，设置相应的css样式
            if (_tjflag == '0') {//退回
                $('#con').css("display", "none");
                $('#th').css("display", "inline");
                $('h2').css("padding", "5px,0px,20px,0px");
                $('#dep').css("border-collapse", "separate"); $('#dep').css("border-spacing", "10px"); //设置table的行间距
                $('#txta').css("height", "50%");
            }
            $('#SMQR').window('open');
            time();
        }
    })
}

//获取书面确认页面的信息
function getContent() {
    smqr = $('#Department').val() + "|" + $('#txta').val() + "|" + $('#TBR').val() + "|" + $('#TBSJ').val() + "|" + $('#TBRPHONE').val() + "|" + $('#SHR').val() + "|" + $('#SHSJ').val() + "|" + $('#SHRPHONE').val();
    $('#SMQR').window('close');
    if($("#HidDH").val()=="")
    {
        $("#HidDH").val(ParamDic["MBDM"]);
    }
    if($("#HidJS").val()=="")
    {
        $("#HidJS").val(ParamDic["JSDM"]);
    }
    TJ($("#HidTJ").val(),$("#HidDH").val(),$("#HidJS").val());
}

function close() {
    $('#SMQR').window('close');
    $("#HidTJ").val("");
}

//上报流程函数：
function TJ(_flag,MBDM,JSDM) {
    var hint = '是否确认执行';
    if (_flag == '-1') {
        hint += '上报';
    } else if (_flag == '1') {
        hint += '审批';
    } else if (_flag == '0') {
        hint += '打回';
    }
    hint += '?';
    $.messager.confirm('提示：', hint, function (r) {
        if (r) {
            $.ajax({
                url: '../Data/InitMBHandle.ashx',
                type: 'post',
                async: true,
                cache: false,
                data: {
                    MBDM: MBDM,
                    JSDM: JSDM,
                    State: _flag,
                    action: "SP",
                    LCDM: ParamDic["LCDM"],
                    FADM: ParamDic["FADM"],
                    HSZXDM: ParamDic["HSZXDM"],
                    SPYJ:smqr,
                    ACTIONRIGHT: ParamDic["ACTIONRIGHT"]
                },
                success: function (rtn) {
                    $("#HidDH").val("");
                    if (rtn >= 1) {
                        alert("操作成功！");
                        $("#HidTJ").val("");
                    } else if (rtn == 0) {
                        alert("操作失败！");
                    } else if (rtn == -1) {
                     $.ajax({
                      url: '../Data/InitMBHandle.ashx?action=FailedMb&MBDM='+MBDM+'&LCDM='+ParamDic["LCDM"]+'&JSDM='+JSDM+'&HSZXDM='+ParamDic["HSZXDM"]+'&FADM='+ParamDic["FADM"]+'',
                      type: 'post',
                      async: true,
                      cache: false,
                      success: function (rtn)
                       {
                         $.messager.alert("未完成流程的模板",rtn);
                       }
                     });
//                        alert("您没有权限！");
                    } else if (rtn == -2) {
                        alert("预算流程没有发起！");
                    } else if (rtn == -3) {
                        alert("当前数据已经存在，不能重复提交！");
                    } else if (rtn == -4) {
                        alert("操作模板作为其他模板的子节点时，角色设置不一致！");
                    }
                    //window.location.reload();
                    //权限方法重新获取一下（因为填报或者审批后，按钮权限发生改变）
                    GetButtonQX();
                },
                error: function (result) {
                    $("#HidDH").val("");
                    alert(result);
                }
            });
        }
    }, function () {
    });
}

//打开模板并执行相关取数函数：
function OpenMbFile() {
    //打开Spread JS的Excel页面
    debugger;
    $("#HidSaveFileName").val(ParamDic["MBWJ"]);
    openJson(row + "&FileName=" + $("#HidSaveFileName").val(),false);
  
}



//返回指定worksheet的行
function maxrows(_s) 
{
    if (_s == null) 
    {
        return activesheet().getRange(0, -1, activesheet().getRowCount(), -1, GC.Spread.Sheets.SheetArea.viewport);
    }
    else 
    {
        return _s.getRange(0, -1, activesheet().getRowCount(), -1, GC.Spread.Sheets.SheetArea.viewport);
    }
}

//返回指定worksheet的列
function maxcols(_s) 
{
    if (_s == null) 
    {
        return activesheet().getRange(-1,0,-1,activesheet().getColumnCount(), GC.Spread.Sheets.SheetArea.viewport);
    }
    else 
    {
        return _s.getRange(-1, 0, -1, _s.getColumnCount(), GC.Spread.Sheets.SheetArea.viewport);
    }
}

//清除区域中单元格中的公式
function ClearAreaFormula(sheet, rowIndex, colIndex, rowCount, colCount) {
    var Cel;
    for (var i = rowIndex; i < rowCount + rowIndex; i++) {
        for (var j = colIndex; j < colIndex+colCount; j++) {
            Cel = sheet.getCell(i, j);
            //清除单元格公式
            ClearCellFormula(Cel);
            Cel.value(null);
        }
    }
}

//清除指定sheet页单元格：
function clearsheet(_sheet) 
{
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    //停止绘制
    _sheet.suspendPaint();
    //取消sheet页保护
    unprotectsheet(_sheet);
    //清除单元格公式以及值
    ClearAreaFormula(_sheet, 0, 0, _sheet.getRowCount(), _sheet.getColumnCount());
    //_sheet.getRange(0, 0, _sheet.getRowCount(), _sheet.getColumnCount(), GC.Spread.Sheets.SheetArea.viewport).value(null);
    //重绘
    _sheet.resumePaint();
}

//判断单元格是否在区域内，如果在区域内则解锁区域；不在区域的话，则解锁单元格
function IsInArea(sheet, ri, ci) {
    var Flag = false;
    //停止绘制
    sheet.suspendPaint();
    //获取当前sheet被选中的所有区域
    var Arr = sheet.getSpans();
    for (var i = 0; i < Arr.length; i++) {
        //表示此单元格在这个区域内
        if (ri >= Arr[i].row && ri <= (Arr[i].row + Arr[i].rowCount - 1) && ci >= Arr[i].col && ci <= (Arr[i].col + Arr[i].colCount - 1)) {
            Arr[i].locked(false);
            Flag = true;
            break;
        }
    }
    if (Flag == false) {
        sheet.getCell(ri, ci).locked(false);
    }
    //重绘
    sheet.resumePaint();
}

//在excel上填写投入产出行：
function writetrccrow(_sheet, _jsonrow, _ridx) {
    if (_sheet == null) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    //停止绘制
    _sheet.suspendPaint();
    var Cel;
    var colidx = 0;
    for (var colkey in _jsonrow) {
        if (_sheet) {
            Cel = _sheet.getCell(_ridx, colidx);
            ClearCellFormula(Cel);
            Cel.value(_jsonrow[colkey]);
            if (_ridx == 0) {
                //设置标题加粗样式
                setCellStyle(_sheet,_ridx,colidx, "font-weight", false, ["700", "bold"], "normal");
            }
            else {
                if (colkey == 'bm') {
                    var CI = 0;
                    var jsonbm = eval('({' + _jsonrow[colkey] + '})');
                    //设置单元格的背景色
                    var cellStyle = new spreadNS.Style();
                    cellStyle.backColor = xcolor;
                    //合计
                    if (jsonbm["LX"] == "0") {

                        if (jsonbm["ZZBCPDM"] != undefined || jsonbm["ZZCPDM"] != undefined) {
                            //解除单元格或者区域的锁以及设置单元格背景色
                            for (var i = 6; i <= 9; i++) {
                                //判断单元格是否在区域内，如果在区域内则解锁区域；不在区域的话，则解锁单元格
                                IsInArea(_sheet, _ridx, i);
                                //设置单元格的背景色
                                _sheet.setStyle(_ridx, i, cellStyle, GC.Spread.Sheets.SheetArea.viewport);
                            }
                        }
                    }
                    else if (jsonbm["LX"] == "1") {
                        //投入：       
                        CI = 5;
                    }
                    else if (jsonbm["LX"] == "2" || jsonbm["LX"] == "3") {
                        //2：产出;3：损失      
                        CI = 6;
                    }
                    //只有当CI是投入、产出、损失的时候才调用
                    if (CI != 0) {
                        //判断单元格是否在区域内，如果在区域内则解锁区域；不在区域的话，则解锁单元格
                        IsInArea(_sheet, _ridx, CI);
                        //设置单元格的背景色
                        _sheet.setStyle(_ridx, CI, cellStyle, GC.Spread.Sheets.SheetArea.viewport);
                    }
                }
            }
        }
        colidx++;
    }
    //重绘
    _sheet.resumePaint();
}

//设置自适应列
function autoFitColumn(sheet) 
{
    for (var i = 0; i < sheet.getColumnCount(); i++) {
        sheet.autoFitColumn(i);
    }
}

//设置自适应行
function autoFitRow(sheet) {
    for (var i = 0; i < sheet.getRowCount(); i++) {
        sheet.autoFitRow(i);
    }
}

//在excel上填写投入产出样式：
function writetrccstyle(_sheet, _scount) {
    if (_sheet == null) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    //停止绘制
    _sheet.suspendPaint();
    //设置Sheet页显示的行数
    //_sheet.setRowCount(_scount, GC.Spread.Sheets.SheetArea.viewport);
    //设置区域Range(0, 0, _scount, 10）的边框
    _sheet.getRange(0, 0, _scount, 10, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#000000", GC.Spread.Sheets.LineStyle.thin), { all: true }, 3);
    //设置列自适应
    autoFitColumn(_sheet);
    //冻结第一行
    _sheet.frozenRowCount(1);
    //隐藏第一列
    pohidefirstcol();
    //重绘
    _sheet.resumePaint();
}


//回写投入产出数据到excel上：
function poinittrcc(_json, _sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    if (spread != null && spread != undefined) {
        var icount = _json["count"];
        _sheet.setRowCount(icount + 1, GC.Spread.Sheets.SheetArea.viewport);
        for (var i = 0; i <=icount; i++) {
            var jsonrow = _json["grids"][i];
            writetrccrow(_sheet, jsonrow, i);
        }
        writetrccstyle(_sheet, (icount+1).toString());
    }
    return true;
}

//设置指定单元格：
function getsumjson(_sheet, _json, _r) {
    if (_sheet && _json && _r) {
        var zydm = _json['ZYDM'];
        var zzbcpdm = _json['ZZBCPDM'];
        var zzcpdm = _json['ZZCPDM'];
        _r++;
        var bmjson = JSON.parse('{' + _sheet.getCell(_r, 0).value() + '}');
        var lx = bmjson['LX'];
        var trs = '', tre = '', ccs = '', cce = '', tr = '', cc = '', res = '';
        while ((lx == '1' || lx == '2' || lx == '3') && _sheet.getCell(_r, 0).value() != undefined) {
            bmjson = JSON.parse('{' + _sheet.getCell(_r, 0).value() + '}');
            //如果是投入：
            if (lx == bmjson['LX'] && lx == '1') {
                if (trs == '') {
                    trs = _r.toString();
                }
                tre = _r.toString();
                //如果是产出或者损失：
            } else if (bmjson['LX'] == '2' || bmjson['LX'] == '3') {
                if (ccs == '') {
                    ccs = _r.toString();
                }
                cce = _r.toString();
            }
            lx = bmjson['LX'];
            _r++;
        }
    }
    if (trs != '' && tre != '') {
        tr = 'SUM(F' + (parseInt(trs)+1).toString() + ':F' + (parseInt(tre)+1).toString() + ')';
    }
    if (ccs != '' && cce != '') {
        cc = 'SUM(G' + (parseInt(ccs) + 1).toString() + ':G' + (parseInt(cce) + 1).toString() + ')';
    }
    res = '{"trl"\:"' + tr + '","ccl"\:"' + cc + '"}';
    return JSON.parse(res);
}

//根据投入产出当设置情况，对合计行添加合计sum公式:
function posettrccsum(_sheet) {
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    if (spread != null && spread != undefined) {
        //停止绘制
        _sheet.suspendPaint();
        var r = _sheet.getRowCount();
        unprotectsheet(_sheet);
        for (var i = 1; i < r; i++) {
            if (_sheet.getCell(i, 0).value() == undefined) break;
            var rowjson = JSON.parse('{' + _sheet.getCell(i, 0).value() + '}');
            var lx = rowjson['LX'];
            if (lx == '0') {
                var jsonres = getsumjson(_sheet, rowjson, i);
                if (jsonres) {
                    //追加投入求和公式：
                    if (jsonres['trl'] != '') {
                        _sheet.getCell(i, 5).formula(jsonres['trl']);
                    }
                    //追加产出求和公式：
                    if (jsonres['ccl'] != '') {
                        _sheet.getCell(i, 6).formula(jsonres['ccl']);
                    }
                }
            }
        }
        //重绘
        _sheet.resumePaint();
        protectsheet(_sheet);

    }
    return true;
}

//获取第一个sheet页
function GetFirstSheet() {
    if (spread != null && spread != undefined) {
        var sheet = spread.sheets[0];
        //结束Sheet页编辑
        EndSheetEdit(sheet);
        return sheet;
    }
    else {
        alert("未获取初始化对象！");
    }
}

//生成投入产出数据：
function InitTrCc() {
    //var firstsheet = GetFirstSheet();
    var firstsheet = activesheet();
    $.messager.confirm('提示：', '清空【' + firstsheet.name() + '】模板数据?', function (r) {
        if (r) {
            EndExcelEdit();
            dialogshowwin();
            dialogshow('正在处理数据，请稍候......', true);
            dialogshow('清空【' + firstsheet.name() + '】模板页......');
            clearsheet(firstsheet);
            dialogshow('获取投入产出数据.....');
            //请求投入产出模板数据：
            $.ajax({
                type: 'get',
                url: rootPath + 'action=inittrcc&hszxdm=' + ParamDic["HSZXDM"],
                cache: false,
                success: function (result) {
                    dialogshow('回写投入产出数据......');
                    result = result.replace(/\r/g, "").replace(/\n/g, "");
                    try {
                        var sucjson = eval("(" + result + ")");
                    }
                    catch (e) {
                        BmShowWinError('公式解析错误!', result);
                    }
                    if (sucjson) {
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!poinittrcc(sucjson, firstsheet)) {
                                alert("投入产出回写错误！");
                            }
                            dialogshow('设置投入产出合计公式......');
                            if (!posettrccsum(firstsheet)) {
                                alert("设置投入产出公式错误！");
                            }
                            dialogshow('投入产出数据生成完毕！');
                            pohidefirstcol(firstsheet);
                            //激活第一个sheet页
                            spread.setActiveSheetIndex(0);
                        }
                    }
                    dialogclose();
                },
                error: function (req) {
                    //激活第一个sheet页
                    spread.setActiveSheetIndex(0);
                    dialogclose();
                    BmShowWinError('请求投入产出数据错误', req.responseText);
                }
            });
        }
    }, function () {
    });
}

//结束sheet页编辑状态
function EndSheetEdit(sheet) {
    //判断当前sheet页是否处于编辑状态（0：非编辑状态；否则处于编辑状态）
    if (sheet.editorStatus() != 0) {
        sheet.endEdit(false);
    }
}

//取消Excel单元格编辑
function EndExcelEdit() {
    var sheet;
    if (spread != null && spread != undefined && spread != "") {
        //结束Excel的sheet页的单元格编辑状态
        for (var i = 0; i < spread.getSheetCount(); i++) {
            sheet = spread.sheets[i];
            //结束sheet编辑
            EndSheetEdit(sheet);
        }
    }
}
//清除单元格公式
function ClearCellFormula(Cell) {
    //如果当前单元格上有公式的话，则先清除公式！再赋值！否则赋值失败
    if (Cell.formula() != "" && Cell.formula() != undefined && Cell.formula()!=null) {
        Cell.formula("");
    }
}

//在现有模板基础上追加投入产出数据：
function InsertTrCc() {
    //var firstsheet = GetFirstSheet();
    var firstsheet = activesheet();
    $.messager.confirm('提示：', '是否根据数据库内容变更【' + firstsheet.name() + '】模板数据?', function (r) {
        if (r) {
            EndExcelEdit();
            dialogshowwin();
            dialogshow('正在处理数据，请稍候......', true);
            dialogshow('获取投入产出数据......');
            //请求投入产出模板数据：
            $.ajax({
                type: 'get',
                url: rootPath + 'action=inittrcc&hszxdm=' + ParamDic["HSZXDM"],
                cache: false,
                success: function (result) {
                    dialogshow('回写投入产出数据......');
                    result = result.replace(/\r/g, "").replace(/\n/g, "");
                    try {
                        var sucjson = eval("(" + result + ")");
                    } catch (e) {
                        BmShowWinError('公式解析错误!', result);
                    }
                    if (sucjson) {
                        if (parseInt(sucjson["count"]) > 0) {
                            if (!poinserttrcc(sucjson, firstsheet)) {
                                alert("投入产出回写错误！");
                            }
                            dialogshow('设置投入产出合计公式......');
                            if (!posettrccsum(firstsheet)) {
                                alert("设置投入产出公式错误！");
                            }
                            dialogshow('投入产出数据生成完毕！');
                            pohidefirstcol(firstsheet);
                            //激活第一个sheet页
                            spread.setActiveSheetIndex(0);
                        }
                    }
                    dialogclose();
                },
                error: function (req) {
                    //激活第一个sheet页
                    spread.setActiveSheetIndex(0);
                    dialogclose();
                    BmShowWinError('请求投入产出数据错误', req.responseText);
                }
            });

        }
    }, function () {
    });
}

//返回sheet页中的已使用的行数：
function usedrowscnt(_sheet) {
    return _sheet.getRowCount();
}

//插入和删除当前模版的投入产出数据：
function poinserttrcc(_json, _sheet) 
{
    //判断是否为同一行：
    function isrow(_srcjson, _destjson) 
    {
        if (_srcjson && _destjson) 
        {
            return _srcjson['ZYDM'] == _destjson['ZYDM'] && _srcjson['LX'] == _destjson['LX'] && _srcjson['FADM'] == _destjson['FADM']
            && _srcjson['CPDM'] == _destjson['CPDM'] && _srcjson['ZZBCPDM'] == _destjson['ZZBCPDM'] && _srcjson['ZZCPDM'] == _destjson['ZZCPDM'];
        }
        return false;
    }
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        //结束sheet编辑
        EndSheetEdit(_sheet);
    }
    unprotectsheet(_sheet);
    if (spread != null && spread != undefined) {
        var icount = _json['count'];
        //先删除当前网格中不在数据库中的行，获取sheet页上有效的行数：
        for (var i = usedrowscnt(_sheet) - 1; i > 0; i--) 
        {
            if (_sheet.getCell(i, 0).value() != undefined && _sheet.getCell(i, 0).value().toString().trim() != "") 
            {
                var jsonsheetbm = eval('({' + _sheet.getCell(i, 0).value() + '})');
                if (jsonsheetbm) 
                {
                    var isexist = false;
                    //获取数据库中的bm：
                    for (var j = 1; j < icount+1; j++) {
                        var jsondatarow = _json['grids'][j];
                        var jsondatabm = eval('({' + jsondatarow['bm'] + '})');
                        if (jsondatabm) {
                            if (isrow(jsonsheetbm, jsondatabm)) {
                                isexist = true;
                                break;
                            }
                        }
                    }
                    //如果不存在代表数据库中删除了模版信息,则删除相应的表格行：
                    if (!isexist) {
                        _sheet.deleteRows(i,1);
                    }
                }
            }
        }

        //设置sheet的总行数
        //_sheet.setRowCount(icount + 1, GC.Spread.Sheets.SheetArea.viewport);

        //下面i< icount+1是因为_json['grids']中的行数包含标题列，而 icount表示的是数据的行数，所以+1

        //插入网格上不存在的行：
        for (var i = 0; i < icount+1; i++) {
            var jsonrow = _json['grids'][i];
            if (i == 0) {
                writetrccrow(_sheet, jsonrow, i);
                continue;
            }
            var jsondatabm = eval('({' + jsonrow['bm'] + '})');
            if (jsondatabm) 
            {
                if (_sheet.getCell(i, 0).value() != undefined && _sheet.getCell(i, 0).value().toString().trim() != "") 
                {
                    var jsonsheetbm = eval('({' + _sheet.getCell(i, 0).value() + '})');
                    if (jsonsheetbm)
                    {
                        if (!isrow(jsondatabm, jsonsheetbm)) 
                        {
                            //添加一行记录
                            _sheet.addRows(i, 1);
                            //写投入产出行
                            writetrccrow(_sheet, jsonrow, i);
                            // var rowStyle = new GC.Spread.Sheets.Style();
                            // spread.getActiveSheet().setStyle(i, -1, rowStyle, GC.Spread.Sheets.SheetArea.viewport);
                        }
                    }
                }
                else 
                {
                    //写投入产出行
                    writetrccrow(_sheet, jsonrow, i);
                }
            }
        }
        writetrccstyle(_sheet, (icount+1).toString());
    }
    protectsheet(_sheet);
    return true;
}

//保存上传按钮:
function SaveUpClick(_fn) {
    //结束Excel编辑
    EndExcelEdit();
    if (localfilename != '') {
        alert('您打开的本地文档：' + localfilename + '与模板不符！\n\n请重新打开文件名为：' + ParamDic["MBMC"] + '.XLS的文件！');
    } else {
        if (state == 1) {
            checkformula('1');
        } else if (state == 2) {
            checkformula('2');
        } else {
            checkformula('1');
        }
//        $.messager.confirm('提示：',
//            '是否要保存数据和文件?',
//            function(r) {
//                if (r) {
//                    if (ParamDic["MBLX"] == '1') {
//                        SaveTrCc(_fn);
//                    } else {
//                        SaveData(false, _fn);
//                    }
//                    //如果是"批复预算基础数据维护"
//                    if ((ParamDic["MBLB"] == 'respfj') && (ParamDic["SFZLC"] == '0')) {
//                        DealRespondStatus();
//                    }
//                } else {
//                    if (IsCheckFormula(_fn)) {
//                        eval(_fn);
//                    }
//                }
//            },
//            function() {});
    }
}

//保存指定标签页数据，即按行列属性保存数据
function SaveData(_istrcc, _fn) {
    var jsondata = null;
    dialogshowwin();
    dialogshow('正在保存数据......', true);
    dialogshow('遍历单元格获取要存储的数据......');
    if (_istrcc) {
        jsondata = posavedata(ParamDic["MBDM"],ParamDic["FADM"],ParamDic["USERDM"], _istrcc);
    } else {
        jsondata = posavedata(ParamDic["MBDM"],ParamDic["FADM"],ParamDic["USERDM"]);
    }
    dialogshow('存储填报数据......');
    alert('保存完成！');
    if (jsondata && isJson(jsondata)) {
        dialogshow('总计：【' + jsondata['count'] + '】条填报数据，请稍候......');
        ajaxpost(rootPath + 'action=savedata', true, JSON.stringify(jsondata), function (xmlHttp) {
            if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                // 本地提示：加载中/处理中 
            }
            else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                if (xmlHttp.responseText == '保存完成！') {
                    SaveFile();
                    //提示是否上报或审批：
                    if (!IsCheckFormula(_fn) && (QX["SB"] == '1' || QX["SP"] == '1')) {
                        if (QX["SB"] == '1') {
                            //msgbox('提示', '保存完成，是否将数据继续上报?', 'DataUp()', 1, 1, 'Warning', '是(上报)', '否(继续编辑)');
                            easyuiConfirm('提示', '保存完成，是否将数据继续上报?',function(){DataUp()},function(){easyuidisLoad()});
                        } else if (QX["SP"] == '1') {
                            //msgbox('提示', '保存完成，是否将数据继续审批?', 'DataApp()', 1, 1, 'Warning', '是(上报)', '否(继续审批)');
                            easyuiConfirm('提示', '保存完成，是否将数据继续审批?',function(){DataApp()},function(){easyuidisLoad()});
                        }
                    }
                    else {
                        if (IsCheckFormula(_fn)) {
                            eval(_fn);
                        }
                    }
                }
            } else {
                dialogclose();
                BmShowWinError('数据保存错误!', xmlHttp.responseText);
            }
        });
    }
    else {
        dialogshow('保护数据页......');
        protectallsheet();
        dialogclose();
    }
}

//保存投入产出数据：
function SaveTrCc(_fn) {
    //获取到数据json：
    dialogshowwin();
    dialogshow('正在获取投入产出数据......', true);
    var trccjson = pogettrccjson(GetFirstSheet());
    if (trccjson) {
        dialogshow('正在保存投入产出数据......');
        ajaxpost(rootPath + 'action=savetrcc&hszxdm=' + ParamDic["HSZXDM"] + '&fadm=' + ParamDic["FADM"], true, JSON.stringify(trccjson), function (xmlHttp) {
            if (xmlHttp.readyState == 1 || xmlHttp.readyState == 2 || xmlHttp.readyState == 3) {
                // 本地提示：加载中/处理中 
            }
            else if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                //如果对于生产方案模板的填报中后几个标签页也存在填报页，则放开此注释同时注释掉该function中SaveData(true);以下代码：
                var filename = getsavefilename(ParamDic["FADM"], ParamDic["MBWJ"]);
                $("#HidSaveFileName").val(filename);
                //Excel已Json形式保存
                SaveExcel(2,$("#HidSaveFileName").val());
                dialogclose();
                protectsheet(GetFirstSheet());
                $.messager.alert("提示框",xmlHttp.responseText);
                if (IsCheckFormula(_fn)) {
                    eval(_fn);
                }
                $("#HidBC").val("1");
            } else {
                dialogclose();
                BmShowWinError('投入产出数据保存错误!', xmlHttp.responseText);

            }
        });
    } else {
        dialogclose();
    }
}
  /// <summary>提示</summary>
        function tishi(rtn) {
            $.messager.alert({
                title: '温馨提示',
                msg: rtn
            });
        }
         function initDialog(name, title, wid, hg, con) {
            $('#' + name).dialog({
                title: title,
                width: wid,
                height: hg,
                closed: true,
                cache: false,
                buttons: [{
                    text: '保存',
                    iconCls: 'icon-ok',
                    handler: function () {
                        saveBDXM(param);
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-undo',
                    handler: function () {
                        $('#' + name).dialog('close');
                    }
                }],
                content: con

            });
        }
/// <summary>插入变动行</summary>
function InsertRow()
{
    debugger;
    var mbdm=ParamDic["MBDM"];
    var hszxdm=ParamDic["HSZXDM"];
    var sheet = spread.sheets[0];
    var a=sheet.getSelections();
    var sheetName = sheet.name();
    var FatherRow = a[0].row;
    if(sheet.getCell(parseInt(FatherRow), 0).value()!=null)
    {
        var sel=sheet.getCell(parseInt(FatherRow), 0).value().toString();
         //第一列的属性
        var FirstColSX = "";
        //查找首列的行类型
            $.ajax({
                type: 'get',
                url: '../Data/MB.ashx?action=SelHLX&mbdm=' + mbdm + '&sheetName=' + sheetName,
                async: false,
                cache: false,
                success: function (result) {
                    FirstColSX = result
                }
            });
        if (sel == 0 || sel == undefined || FirstColSX == "1" || FirstColSX == "4") {
                tishi("行属性无项目，无法插入变动行");
        }
        else
        {
            if (sel != undefined && sel != null && sel != "" && sel.indexOf(",BZ:YZ") == -1) {
              var xmfl = "";
              var arr = new Array();
              arr = sel.split(',');
              var cpdm = "";
              var bdxmdm = "";
              var zydm = "";
               if (FirstColSX == "2") {
                        if (sel.indexOf("|") > 0) {
                            cpdm = arr[0].split('|')[0].split(':')[1];
                            bdxmdm = arr[0].split('|')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            cpdm = arr[0].split(':')[1];
                            xmfl = arr[1].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                        //查找最大列数
                        var maxCol = colscnt(sheet);
                        //查找第一行中有zydm的
                        for (var n = 1; n < maxCol; n++) {
                            var firstrowValue = sheet.Cells(parseInt(1), parseInt(n)).Value;
                            if (firstrowValue != undefined && firstrowValue != "") {
                                if (firstrowValue.indexOf("ZYDM:") > -1) {
                                    var ZYDM = firstrowValue.substring(firstrowValue.indexOf("ZYDM:"), firstrowValue.indexOf(","));
                                    zydm = ZYDM.substring(ZYDM.indexOf(":")+1);
                                    break;
                                }
                            }
                        }
                    }
                    if (FirstColSX == "3") {
                        if (sel.indexOf("|") > 0) {
                            zydm = arr[0].split(':')[1];
                            cpdm = arr[1].split('|')[0].split(':')[1];
                            bdxmdm = arr[1].split('|')[1];
                            xmfl = arr[2].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            zydm = arr[0].split(':')[1];
                            cpdm = arr[1].split(':')[1];
                            xmfl = arr[2].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                    }
//                    if (FirstColSX == "4") {

//                    }
                    if (FirstColSX == "5") {
                        if (sel.indexOf("|") > -1) {
                            zydm = arr[0].split(':')[1];
                            cpdm = arr[1].split('|')[0].split(':')[1];
                            bdxmdm = arr[1].split('|')[1];
                            xmfl = arr[3].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: false });
                        }
                        else {
                            zydm = arr[0].split(':')[1];
                            cpdm = arr[1].split(':')[1];
                            xmfl = arr[3].split(':')[1];
                            $("#TxtXMMC").attr('disabled', 'disabled');
                            $("#cslx").combobox({ disabled: true });
                        }
                    }
                     $.ajax({
                        type: 'post',
                        url: '../Data/MB.ashx?action=InsertBDXM&hszxdm=' + hszxdm + '&xmfl=' + xmfl + '&mbdm=' + mbdm,
                        async: false,
                        cache: false,
                        success: function (result) {
                        }
                    });
                     GetDataGrid(hszxdm, zydm, cpdm);
                    thisxmdm = cpdm;
                    thiszydm = zydm;
                    XMFLXZ();
                    $("#cslx").combobox({ panelHeight: 200 })
            }
        }
        $("#SZBDH").window('open');
         $('#dg').datagrid('clearSelections');
                    var rows = $('#dg').datagrid('getRows');
                    var index = -1;
                    for (var i = 0; i < rows.length; i++) {
                        if (rows[i].BDXMDM == bdxmdm) {
                            index = $("#dg").datagrid('getRowIndex', rows[i]);
                        }
                    }
                     if (index != -1) {
                        $('#dg').datagrid('selectRow', index);
                        var row = $("#dg").datagrid('getSelections');
                        $("#TxtselXm").val(row[0].BDXMMC);
                    }
          //                                var row = $("#dg").datagrid('getSelections');
                    //for循环判断combox的方法getValue获取的值为json的text的时候，将其值赋值为相应的id
                    for (var i = 0; i < selxmfl.length; i++) {
                        if ($("#cslx").combobox('getText').trim() == selxmfl[i].text) {
                            $("#cslx").combobox('setValue', selxmfl[i].id);
                        }
                    }
                    var cslx = $("#cslx").combobox('setValues', xmfl);
                    initDialog('dialogBDXM', '设置变动行项', 300, 120, $('#BDXM'));
                     if (sel.indexOf("|") > 0) {
                        if (index != -1) {
                            var row = $("#dg").datagrid('getSelections');
                            $("#TxtXMMC").val(row[0].BDXMMC);
                        }
                    }
                    else {
                        $.ajax({
                            type: 'get',
                            url: '../Data/MB.ashx?action=Selxmmc&xmdm=' + cpdm + '',
                            async: false,
                            cache: false,
                            success: function (result) {
                                $("#TxtXMMC").val(result);
                            }
                        });
                    }
    }
    else
    {
       tishi("行属性无项目，无法插入变动行");
    }
}
 /// <summary>获取变动项目的列表</summary>
        function GetDataGrid(hszxdm, zydm,cpdm) {
            $.ajax({
                type: 'post',
                url: '../Data/MB.ashx?action=GetDataGrid&hszxdm=' + hszxdm + '&zydm=' + zydm + '&cpdm='+cpdm,
                async: false,
                cache: false,
                success: function (result) {
                    var Data = eval("(" + result + ")");
                    $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
                }
            });
        }
         var selxmfl = "";
        /// <summary>获取变动行的项目分类</summary>
        function XMFLXZ() {
            $.ajax({
                type: 'get',
                url: '../Data/MB.ashx?action=XMFLXZ',
                async: false,
                cache: false,
                success: function (result) {
                    var Data = eval("(" + result + ")");
                    selxmfl = Data;
                    $("#cslx").combobox("loadData", Data);
                }
            });
        }
//上报：
function DataUp() {
    //如果是"批复预算基础数据维护"
    if (ParamDic["MBLB"] == 'respfj') 
    {
        DealRespondStatus();
    }
    //如果是生产方案模板：
//    if (ParamDic["MBLX"] == '1') 
//    {
//        IsSave("CheckFamb('-1')");
//    }
//    else 
//    {
//        IsSave("checkformula('-1')");
//    }
}

//审批：
function DataApp() 
{
    if (ParamDic["MBLX"] == '1') 
    {
        IsSave("CheckFamb('1')");
    }
    else 
    {
        IsSave("checkformula('1')");
    }
}

function IsCheckFormula(_fn) {
    if (_fn == undefined) {
        return false;
    } else {
        return (_fn.toString().indexOf('checkformula') >= 0 || _fn.toString().indexOf('CheckFamb') >= 0) ? true : false;
    }
}

//只保存文件：
function SaveFile() {
    var filename = getsavefilename(ParamDic["FADM"], ParamDic["MBWJ"]);
    $("#HidSaveFileName").val(filename);
    dialogshow('保存相关数据文件......');
    //保存Excel文件到Json
    SaveExcel(2,$("#HidSaveFileName").val());
    dialogshow('保护数据页......');
    protectallsheet();
    dialogclose();
}

//按行列属性存储：
function posavedata(_mbdm, _fadm, _userid, _istrcc) {

    if (spread != null && spread != null) {
        
        var scount = spread.getSheetCount();
        //默认遍历所有标签页：
        var firstsheetidx = 0;
        var resjson = "grids:[";
        var icount = 0;
        //如果是投入产出模版填报则遍历除第一个标签页以后的所有标签页：
        if (_istrcc != undefined && _istrcc) {
            firstsheetidx = 1;
        }
        for (var idx = firstsheetidx; idx < scount; idx++) {
            var sheet = spread.sheets[idx];
            //结束sheet编辑
            EndSheetEdit(sheet);
            dialogshow('获取第' + (idx+1).toString() + '填报页：【' + sheet.name() + '】数据......');
            if (sheet) {
                if (_mbdm) {
                    var colnum = colscnt(sheet);
                    var rownum = rowscnt(sheet);
                    var aryr = new Array();
                    var aryc = new Array();
                    getfindary(sheet, 0, 0, 1, colnum, '*', aryr);
                    getfindary(sheet,0,0,rownum,1,'*', aryc);
                    var totel = aryr.length * aryc.length;
                    try {
                        for (var i = 0; i < aryc.length; i++) {
                            for (var j = 0; j < aryr.length; j++) {
                                var irow = parseInt(aryc[i].row);
                                var icol = parseInt(aryr[j].col);
                                var tempcel = sheet.getCell(irow, icol);
                                if (tempcel.value()) {
                                    var grdjsonstr = aryc[i].value().toString() + "," + aryr[j].value().toString();
                                    var grdjson = grdjsonstr.replace(/:/g, "\":\"").replace(/,/g, "\",\"");
                                    grdjson = "{\"" + grdjson + "\"}";

                                    //此处判断是否为json字符串，如果不是则跳过：
                                    var json = JSON.parse(grdjson);
                                    if (json['JSDX'] != undefined && json['JSDX'].trim() != '' && json['ZYDM'] != undefined && json['ZYDM'].trim() != ''
                                        && json['CPDM'] != undefined && json['CPDM'].trim() != '' && json['SJDX'] != undefined && json['SJDX'].trim() != ''
                                        && json['XMFL'] != undefined && json['XMFL'].trim() != '') {
                                        dialogshow('', false, (parseInt(i + 1) * aryr.length + parseInt(j + 1)) * 100 / totel);
                                        var jsdx = json['JSDX'];
                                        //如果计算对象是保存字符型数据：
                                        if (jsdx.indexOf('|') >= 0) {
                                            icount++;
                                            if (resjson != "grids:[") {
                                                resjson = resjson + ",";
                                            }
//                                            var rowcolval = grdjsonstr + ",ZFVAL:" + tempcel.Value2 + ",SHEETNAME:" + sheet.name();
                                            var rowcolval = grdjsonstr + ",ZFVAL:" + tempcel.value() + ",SHEETNAME:" + sheet.name();
                                            rowcolval = rowcolval.replace(/:/g, "\":\"").replace(/,/g, "\",\"").replace(/\|/g, "");
                                            resjson += "{\"" + rowcolval + "\"}";
                                            //如果保存的是数值类型则需要校验：
                                        } else {
                                            if (chkcell(sheet, irow, icol)) {
                                                icount++;
                                                if (resjson != "grids:[") {
                                                    resjson = resjson + ",";
                                                }
//                                                var rowcolval = grdjsonstr + ",VAL:" + tempcel.Value2 + ",SHEETNAME:" + sheet.Name;
                                                var rowcolval = grdjsonstr + ",VAL:" + tempcel.value() + ",SHEETNAME:" + sheet.name();
                                                rowcolval = rowcolval.replace(/:/g, "\":\"").replace(/,/g, "\",\"");
                                                resjson += "{\"" + rowcolval + "\"}";
                                            } else {
                                                return null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        if (sheet && tempcel) {
                            spread.setActiveSheet(sheet.name());
                            alert('填报页【' + sheet.name() + '】单元格【$' + GetColCharByColIndex(tempcel.col) + '$'+(tempcel.row+1)+'】模版行列属性解析出错\n\n请检查模版设置！');
                        } else {
                            alert("模版行列属性解析出错\n\n请检查模版设置！");
                        }
                        return null;
                    }
                }
            }
        }
        resjson += "]";
        if (icount == 0) {
                resjson = "({\"count\":" + icount.toString() + ",\"mbdm\":\"" + _mbdm + "\",\"fadm\":\"" + _fadm + "\",\"userid\":\"" + _userid + "\"})";
            } else {
                resjson = "({\"count\":" + icount.toString() + ",\"mbdm\":\"" + _mbdm + "\",\"fadm\":\"" + _fadm + "\",\"userid\":\"" + _userid + "\"," + resjson + "})";
            }
        return eval(resjson);
    }
}

//sheet：当前操作的sheet页

//rowIndex：行索引

//colIndex：列索引

//rowCount：行数

//colCount：列数

//_findstr：要查询的字符串

//_ary：返回存储单元格的集合

//在指定区域查找值不等于undefined的单元格返回到数组中

function getfindary(sheet, rowIndex, colIndex, rowCount, colCount, _findstr, _ary) 
{
    var Cell;
    _ary.length=0;
    for (var i = rowIndex; i < rowIndex + rowCount; i++) 
    {
        for (var j = colIndex; j < colIndex + colCount; j++) 
        {
            Cell = sheet.getCell(i, j);
            if (Cell.value() != undefined) 
            {
                _ary.push(Cell);
            }
        }
    }
}

//判断对象是否为json：
function isJson(obj) {
    var isjson = typeof (obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;
    return isjson;
}

//寻找第一列中有数据的最大的行数
function rowscnt(_s) {
    var rowIndex = 0;
    if (_s == null) 
    {
        _s=activesheet();
    }

    //寻找第一列中有数据的最大的行数
    for (var i = _s.getRowCount()-1; i >=0; i--) {
        if (_s.getCell(i, 0).value() != "" && _s.getCell(i, 0).value() != null && _s.getCell(i, 0).value() != undefined) {
            rowIndex = i;
            break;
        }
    }

    return rowIndex+1;
}

//返回指定worksheet第一行有数据列号：
function colscnt(_s) {
    var colIndex = 0;
    if (_s == null) 
    {
        _s=activesheet();
    }

    //寻找第一行中有数据的最大的列号
    for (var i = _s.getColumnCount() - 1; i >= 0; i--) {
        if (_s.getCell(i, 0).value() != "" && _s.getCell(i, 0).value() != null && _s.getCell(i, 0).value() != undefined) {
            colIndex = i;
            break;
        }
    }

    return colIndex;
}

//Flag：1表示保存成Excel文件
//      2表示保存成Json
function SaveExcel(Flag,FileName) {
    var path = "../Data/";
    if (Flag == 1)
        savetoServer("file", $("#HidSaveFileName").val(), path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');   //保存excel
    else
        saveJsonToServer("file", $("#HidSaveFileName").val(), path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');  //保存json
    //是否保存标记
    $("#HidBC").val("1");
}

//生产方案模板校验：
function CheckFamb(_tjflag) {
    //模板校验：
    var surl = rootPath + 'action=fambcheck&hszxdm=' + ParamDic["HSZXDM"] + '&fadm=' + ParamDic["FADM"];
    //结束Excel编辑
    EndExcelEdit();
    //请求：
    $.ajax({
        type: 'get',
        url: surl,
        cache: false,
        success: function (result) {
            dialogclose();
            //代表校验正常：
            if (result == '') {
                //提交申请或者审批
                $("#HidTJ").val(_tjflag);
                OpenSmqr(_tjflag);
            } else {
                var GrdImbl = $("#GrdImba");
                datagridloading(GrdImbl);
                var WinImba = $("#WinImba");
                WinImba.window({

                });
                WinImba.window('open');
                result = result.replace(/\r/g, "").replace(/\n/g, "");
                try {
                    var jsonres = eval(result);
                } catch (e) {
                    BmShowWinError('公式解析错误!', result);
                }
                GrdImbl.datagrid('loadData', jsonres);
                datagridloaded(GrdImbl);
            }
        },
        error: function (req) {
            dialogclose();
            BmShowWinError('请求校验数据错误!', req.responseText);
        }
    });
}


function datagridloaded(_datagrid) {
    _datagrid.datagrid("getPager").pagination("loaded");
    var wrap = $.data(_datagrid[0], "datagrid").panel;
    wrap.find("div.datagrid-mask-msg").remove();
    wrap.find("div.datagrid-mask").remove();
}

//获取校验公式
function GetCheckFormula(rtn) 
{
    var JYGS = rtn["JYGS"];
    var MBLX = rtn["MBLX"];
    var YSBS = rtn["YSBS"];
    var JYGSCount = rtn["JYGSCount"];
    var JYMBLX = rtn["JYMBLX"];
    var JYMBLXCount = rtn["JYMBLXCount"];
    var JYYSBS = rtn["JYYSBS"];
    var JYYSBSCount = rtn["JYYSBSCount"];
    var JsonRow, sheet;
    var gsjx = "", gs = "", INFOMATION = "", sheetName = "", LX = "", JYJG = "", JYJGJG = "";
    try 
    {
        for (var i = 0; i < JYGSCount; i++) 
        {
            JsonRow = JYGS[i];
            LX = JsonRow["LX"];
            gs = JsonRow["GS"];
            gsjx = JsonRow["GSJX"]
            sheetName = JsonRow["sheetName"];
            INFOMATION = JsonRow["INFOMATION"];

            if (spread.getSheetFromName(sheetName) == null) 
            {
                //如果找不到的话，则找最后一个sheet页
                sheet = spread.getSheet(spread.getSheetCount() - 1);
                JYJG = JYJG == "" ? "excel出错，找不到sheet页+" + sheetName + "<br>" : JYJG + "excel出错，找不到sheet页+" + sheetName + "<br>";
            }
            else 
            {
                sheet = spread.getSheetFromName(sheetName);
            }

            if (gs.startsWith("=")) {
                gs = gs.substring(1, gs.Length - 1);
            }
            //如果sheet页是被保护的话，则先解保护
            if (sheet.options.isProtected == true) {
                sheet.options.isProtected == false;
            }
            sheet.getCell(0, 0).formula(gs);
            var JG = sheet.getCell(0, 0).value();
            if (JG == false) 
            {
                if (LX == "0") 
                {
                    if (INFOMATION.trim() == "") 
                    {
                        JYJG = JYJG == "" ? sheetName + "|" + gsjx + INFOMATION + "<br>" : JYJG + sheetName + "|" + gsjx + INFOMATION + "<br>";
                    }
                    else 
                    {
                        JYJG = JYJG == "" ? sheetName + "|" + INFOMATION + "<br>" : JYJG + sheetName + "|" + INFOMATION + "<br>";
                    }
                }
                else if (LX == "1") 
                {
                    if (INFOMATION.trim() == "") 
                    {

                        JYJGJG = JYJGJG == "" ? sheetName + "|" + gsjx + INFOMATION + "<br>" : JYJGJG + sheetName + "|" + gsjx + INFOMATION + "<br>";
                    }
                    else 
                    {
                        JYJGJG = JYJGJG == "" ? sheetName + "|" + INFOMATION + "<br>" : JYJGJG + sheetName + "|" + INFOMATION + "<br>";
                    }
                }
            }
            sheet.getCell(0, 0).formula(null);
            sheet.getCell(0, 0).value(null);
        }

        if (MBLX == "4" && YSBS == "4" && JYMBLXCount > 0) 
        {
            var value = "";
            var GS = "";
            var WCFW = "";
            for (var j = 0; j < JYMBLXCount; j++) 
            {
                JsonRow = JYMBLX[i];
                value = JsonRow["VALUE"].toString();
                if (value == "")
                    value = "0";
                GS = JsonRow["GS"].toString();
                if (GS == "")
                    GS = "0";
                WCFW = JsonRow["WCFW"].toString();
                if (WCFW == "")
                    WCFW = "0";
                //利用第一个单元格设置公式，为下面获取公式的值
                sheet.getCell(0, 0).formula(GS);
                //获取公式值
                var JG = sheet.getCell(0, 0).value().toString();
                //清空公式
                sheet.getCell(0, 0).formula(null);

                if (Math.abs(parseFloat(JG) - parseFloat(value)) > parseFloat(WCFW)) 
                {
                    if (JsonRow["LX"] == "0") 
                    {
                        JYJG = JYJG == "" ? "总部指标下达" + JsonRow["XMMC"].toString() + "超过误差值请修改<br>" : JYJG + "总部指标下达" + JsonRow["XMMC"].toString() + "超过误差值请修改<br>";
                    }
                    else if (JsonRow["LX"] == "1") 
                    {
                        JYJGJG = JYJGJG == "" ? "总部指标下达" + JsonRow["XMMC"].toString() + "超过误差值请修改<br>" : JYJGJG + "总部指标下达" + JsonRow["XMMC"].toString() + "超过误差值请修改<br>";
                    }
                }
            }
        }

        if (YSBS == "4") 
        {
            if (parseInt(JYYSBSCount) > 0) 
            {
                var sql1value = "", sql2value = "", wcfw = "";
                for (var i = 0; i < parseInt(JYYSBSCount); i++) 
                {
                    JsonRow = JYYSBS[i];
                    sql2value = parseFloat(JsonRow["sql2value"].toString());
                    sql1value = parseFloat(JsonRow["sql1value"].toString());
                    wcfw = parseFloat(JsonRow["WCFW"].ToString());
                    if (sql2value - sql1value > wcfw) 
                    {
                        if (JsonRow["LX"] == "0") 
                        {
                            JYJG = JYJG == "" ? JsonRow["XMMC"].toString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改<br>" : JYJG + JsonRow["XMMC"].toString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改<br>";
                        }
                        else if (JsonRow["LX"] == "1") 
                        {
                            JYJGJG = JYJGJG == "" ? JsonRow["XMMC"].toString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改<br>" : JYJGJG + JsonRow["XMMC"].toString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改<br>";
                        }
                    }
                }

            }
        }
    }
    finally 
    {
        //如果sheet页是没有保护的话，则保护
        if (sheet.options.isProtected == false) {
            sheet.options.isProtected == true;
        }
    }

    if (JYJG != "") 
    {
        return JYJG + "||" + "0";
    }
    else if (JYJGJG != "") 
    {
        return JYJGJG + "||" + "1"; ;
    }
    else 
    {
        return "";
    }
}

//校验公式，上报和审批操作前执行：
function checkformula(_tjflag) {
    //校验公式：
    var surl = '../Data/MBDataHandler.ashx';
    //结束Excel编辑
    EndExcelEdit();
    dialogshowwin();
    var shint = '正在校验数据，请稍候......';
    dialogshow(shint, true);
    //请求校验公式：
    $.ajax({
        type: 'get',
        url: surl,
        cache: false,
        data: {
            action: "CheckFormula",
            MBDM: ParamDic["MBDM"],
            GSYY: GSYY,
            GSNN: GSMM,
            FADM: ParamDic["FADM"],
            FADM2: ParamDic["FADM2"],
            FABS: ParamDic["FALB"],
            YY: ParamDic["YY"]
        },
        success: function (result) 
        {
            dialogclose();
            if(result!="")
            {
                var rtn = eval("(" + result + ")");
                result = GetCheckFormula(rtn);
            }
            //代表校验正常：
            if (result == '') 
            {
                //校验行列是否为空公式：
                JyTsgs(_tjflag);
                //如果有警告或者错误提示：
            } 
            else 
            {
                var aryres = result.split('||');
                var message = '';
                for (var i = 0; i < aryres.length - 1; i++) {
                    if (message == '') {
                        message = aryres[i];
                    } else {
                        message = message + '||' + aryres[i];
                    }
                }
                //如果为'1'则警告
                if (aryres[aryres.length - 1] == '1') {
                    $.messager.confirm('出现以下警告，是否继续：', message, function (r) {
                        if (r) {
                            JyTsgs(_tjflag);
                        }
                    }, function () {

                    });
                    //如果为'0'则为错误：
                } else if (aryres[aryres.length - 1] == '0') {
                    $.messager.alert("警告框",'公式校验错误：\n\r' + message);
                } else {
                    JyTsgs(_tjflag);
                }
            }
        },
        error: function (req) {
            dialogclose();
            BmShowWinError('请求校验数据错误!', req.responseText);
        }
    });
}

/// <summary>处理批复状态</summary>
function DealRespondStatus() {
    //更新批复状态：
    $.ajax({
        type: 'post',
        url: rootPath+'action=dealrespondstatus&jhfadm='+ParamDic["FADM"]+'&mbdm='+ParamDic["MBDM"]+'&userid='+ParamDic["USERDM"],
        cache: false,
        error: function (req) {
            BmShowWinError('更新批复状态错误！', req.responseText);
        }
    });
}

//有修改是否保存：
function IsSave(_fn) {
    if (spread!=null && spread!=undefined) {
        //结束Excel编辑
        EndExcelEdit();
        //如果是已经修改了：
        if ($("#HidBC").val()!="1" && QX["BC"] == "1") {
            SaveUpClick(_fn);
        } else {
            if (IsCheckFormula(_fn)) {
                eval(_fn);
            }
        }
    }
}

//将投入产出数据拼装成json
function pogettrccjson(_sheet) {
    var resjson = { "icount": 0, "grids": [] };
    if (!_sheet) {
        _sheet = activesheet();
    } else {
        EndExcelEdit();
    }
    if (spread!= null && spread != undefined) {
        var icount = 0;
        var rownum = rowscnt(_sheet);
        for (var j = 1; j <= rownum; j++) {
            if (_sheet.getCell(j,0).value() && _sheet.getCell(j, 0).value() !== undefined && _sheet.getCell(j, 0).value().toString().trim() != "") {
                var rowjson = JSON.parse('{' + _sheet.getCell(j, 0).value() + '}');
                switch (rowjson["LX"]) {
                    //合计
                    case '0':
                        var badd = false;
                        if (rowjson["ZZBCPDM"] != undefined || rowjson["ZZCPDM"] != undefined) {
                            if (_sheet.getCell(j, 5).value() && _sheet.getCell(j, 5).value().toString().trim() != "") {
                                if (chkcell(_sheet, j, 5)) {
                                    setJson(rowjson, "TRL", _sheet.getCell(j, 5).value());
                                    badd = true;
                                }
                                else return null;
                            }
                            if (_sheet.getCell(j, 6).value() && _sheet.getCell(j, 6).value().toString().trim() != "") {
                                if (chkcell(_sheet, j, 6)) {
                                    setJson(rowjson, "CCL", _sheet.getCell(j, 6).value());
                                    badd = true;
                                }
                                else return null;
                            }
                            if (_sheet.getCell(j, 7).value() && _sheet.getCell(j, 7).value().toString().trim() != "") {
                                if (chkcell(_sheet, j, 7)) {
                                    setJson(rowjson, "QC", _sheet.getCell(j, 8).value());
                                    badd = true;
                                }
                                else return null;
                            }
                            if (_sheet.getCell(j, 8).value() && _sheet.getCell(j,8).value().toString().trim() != "") {
                                if (chkcell(_sheet, j, 8)) {
                                    setJson(rowjson, "QM", _sheet.getCell(j, 8).value());
                                    badd = true;
                                }
                                else return null;
                            }
                            if (_sheet.getCell(j, 9).value() && _sheet.getCell(j, 9).value().toString().trim() != "") {
                                if (chkcell(_sheet, j, 9)) {
                                    setJson(rowjson, "YK", _sheet.getCell(j, 9).value());
                                    badd = true;
                                }
                                else return null;
                            }
                            if (badd) {
                                resjson.grids.push(rowjson);
                                icount++;
                            }
                        }
                        break;
                    //投入：                                                                                                                                                                           
                    case '1':
                        if (_sheet.getCell(j, 5).value() && _sheet.getCell(j, 5).value().toString().trim() != "") {
                            //                                debugger;
                            if (chkcell(_sheet, j, 5)) {
                                setJson(rowjson, "TRL", _sheet.getCell(j, 5).value());
                                resjson.grids.push(rowjson);
                                icount++;
                            }
                            else
                                return null;
                        }
                        break;
                    //产出：                                                                                                                                                                           
                    case '2':
                        if (_sheet.getCell(j, 6).value() && _sheet.getCell(j, 6).value().toString().trim() != "") {
                            if (chkcell(_sheet, j, 6)) {
                                setJson(rowjson, "CCL", _sheet.getCell(j, 6).value());
                                resjson.grids.push(rowjson);
                                icount++;
                            }
                            else return null;
                        }
                        break;
                    //损失：                                                                                                                                                                          
                    case '3':
                        if (_sheet.getCell(j, 6).value() && _sheet.getCell(j, 6).value().toString().trim() != "") {
                            if (chkcell(_sheet, j, 6)) {
                                setJson(rowjson, "CCL", _sheet.getCell(j, 6).value());
                                resjson.grids.push(rowjson);
                                icount++;
                            }
                            else return null;
                        }
                        break;
                }
            }
        }
        setJson(resjson, "count", icount);
    }
    return resjson;
}

/// <summary>检验单元格录入的合法性：</summary>
/// <returns type="boolean">boolean类型返回值</returns>
/// <param name="_sheet" type="Sheet">excel的sheet页</param>
/// <param name="_row" type="Int">excel的第?行</param>
/// <param name="_col" type="Int">excel的第?列</param>
function chkcell(_sheet, _row, _col) {
    //返回指定单元格：
    var cel = _sheet.getCell(_row, _col);
    //如果当前sheet页为空则提取当前活动页
    if (_sheet == null) {
        _sheet = activesheet();
        //如果当前sheet页不为空则调用excel允许呼叫函数否则会出现“呼叫被拒绝提示”
    } else {
        EndExcelEdit();
    }
    //如果单元格的值cel.value()，显示{_error: "#DIV/0!", _code: 7} 这种非法公式结果，暂时认为单元格是合法的
    if (typeof(cel.value())=="object") {
        return true;
    }
    //如果当前网格值不为空并且是小于100000000000000的浮点数则返回true
    if (!isNaN(cel.value()) && parseFloat(cel.value()) < 100000000000000 && cel.value().toString().trim() != "") {
        return true;
        //否则定位到非法单元格并提示同时返回false
    } else {
        if (cel.value().trim() != "") {
            //激活sheet页
            spread.setActiveSheet(sheet.name());
            //选中单元格
            sheet.setActiveCell(cel.row, cel.col, 1, 1)
            if (isNaN(cel.value())) {
                alert('当前单元格【$' + GetColCharByColIndex(cel.col) + '$' + (cel.row + 1) + '】不是数值类型，请检验！');
            } else {
                alert('当前单元格【$' + GetColCharByColIndex(cel.col) + '$' + (cel.row + 1) + '】数值越界，请检验！');
            }
        }
        return false;
    }
}

//添加或者修改json数据
function setJson(_json, _name, _value) {
    if (!isJson(_json)) return null;
    _json[_name] = _value;
    return _json;
}

//打回：
function DataBack() {
    WinJS(true, 0);
}

//上报、审批、打回、保存权限获取：
function WinJS(Flag, BZ) 
{
    var HidIsOnlyOne = $("#HidIsOnlyOne");
    if (Flag == true) 
    {
        $("#WinJS").window('open');
        //加载打回树
        LoadDHTree();
    }
    else 
    {
        var nodes = $("#tt123").tree("getChecked");
        //表示点击了关闭按钮
        if (BZ == -1) 
        {
            $("#WinJS").window('close');
        }
        else {
            //获取打回的数据
            DHData();
            if (nodes.length == 0) 
            {
                alert("请选择要打回的模板！");
                return;
            }
            else 
            {
//                var DH = "", MB1 = "", JS = "", CJ = "";
//                for (var i = 0; i < nodes.length; i++) 
//                {
//                    MB1 = nodes[i].id;
//                    CJ = nodes[i].attributes.CJBM;
//                    JS = nodes[i].attributes.JSDM;
//                    DH = DH == "" ? MB1 + "&" + JS + "&" + CJ : DH + "|" + MB1 + "&" + JS + "&" + CJ;
//                }
//                //提交打回审批
//                if (DH != "") 
//                {
//                    $("#HidDH").val(DH);
//                }
                $("#WinJS").window('close');
                $("#HidTJ").val("0");
                OpenSmqr("0");
            }
        }
    }
}

//加载打回树
function LoadDHTree() {
    $.ajax({
        type: 'get',
        url: '../Data/InitMBHandle.ashx?action=GetDHTree&JSDM=' + ParamDic["JSDM"] + '&LCDM=' + ParamDic["LCDM"] + '&HSZXDM=' + ParamDic["HSZXDM"] + '&JHFADM=' + ParamDic["FADM"] + '&MBDM='+ParamDic["MBDM"]+'',
        cache: false,
        async: true,
        success: function (result) {
            if (result != "" && result != null) {
                var data = eval("(" + result + ")");
                $("#tt123").tree({
                    checkbox: true,
                    data: data.rows,
                    onBeforeExpand: function (node) {
                        var childrens = $('#tt123').tree('getChildren', node.target);
                        if (childrens == false) {
                            var LCDM = ParamDic["LCDM"];
                            var JHFADM = ParamDic["FADM"];
                            var attributes=node.attributes;
                            $.ajax({
                                type: 'get',
                                url: '../Data/InitMBHandle.ashx?action=GetMB&JSDM=' + node.id + '&LCDM=' + LCDM + '&JHFADM=' + JHFADM + '&THISJSDM='+ParamDic["JSDM"]+'&MBDM='+ParamDic["MBDM"]+'&HSZXDM=' + ParamDic["HSZXDM"] + '',
                                cache: false,
                                async: false,
                                checkbox: true,
                                success: function (result) {
                                    var childData = eval("(" + result + ")");
                                    $("#tt123").tree('append', {
                                        parent: node.target,
                                        data: childData.rows,
                                    });
                                },
                                error: function () {
                                    $.messager.alert("获取子节点失败！");
                                    return;
                                }
                            });
                        }
                    },
                    onCheck: function (node, checked) {
                        if (checked == true) {
                            if (node.attributes == 0) {
                                var childnodes = $("#tt123").tree("getChildren", node.target);
                                var father = node.id;
                                for (var i = 0; i < childnodes.length; i++) {
                                    var mbdm = childnodes[i].id;
                                    var chekcCount = [];
                                    $.ajax({
                                        type: 'get',
                                        url: '../Data/InitMBHandle.ashx',
                                        data: {
                                            action: 'GetCheckMb',
                                            MBDM: mbdm,
                                            LCDM: ParamDic["LCDM"],
                                            JHFADM:ParamDic["FADM"],
                                            FatherDm: father
                                        },
                                        success: function (result) {
                                            var count = result.split(',').length;
                                            for (var j = 0; j < count; j++) {
                                                var xjtree = $("#tt123").tree("find", result.split(',')[j]);
                                                if (xjtree != null) {
                                                    var childnode = $('#tt123').tree("getChildren", xjtree.target);
                                                    for (var i = 0; i < childnode.length; i++) {
                                                        if (childnode[i].id == childnodes[i].id) {
                                                            if (childnode[i].checked == true) {
                                                                chekcCount.push(childnode[i].id);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (chekcCount.length != 0) {
                                                $('#tt123').tree('uncheck', node.target);
                                                 $.messager.alert("已有相同的模板被勾选","已有相同的模板被勾选");
                                            }
                                        }
                                    });
                                }
                            }
                            else {
                                var father = $("#tt123").tree("getParent", node.target).id;
                                var thisfather = $("#tt123").tree("find", father);
                                if(thisfather.attributes==1)
                                {
                                    father=$("#tt123").tree("getParent", thisfather.target).id;
                                }
                                var mbdm = node.id;
                                $.ajax({
                                    type: 'get',
                                    url: '../Data/InitMBHandle.ashx',
                                    data: {
                                        action: 'GetCheckMb',
                                        MBDM: mbdm,
                                        LCDM: ParamDic["LCDM"],
                                        JHFADM:ParamDic["FADM"],
                                        FatherDm: father
                                    },
                                     async: false,
                                    success: function (result) {
                                        var count = result.split(',').length;
                                        var chekcCount = [];
                                        for (var j = 0; j < count; j++) {
                                            var xjtree = $("#tt123").tree("find", result.split(',')[j]);
                                            if (xjtree != null) {
                                                var childnode = $('#tt123').tree("getChildren", xjtree.target);
                                                for (var i = 0; i < childnode.length; i++) {
                                                    if (childnode[i].id == mbdm) {
                                                        if (childnode[i].checked == true) {
                                                            chekcCount.push(childnode[i].id);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (chekcCount.length != 0) {
                                            $('#tt123').tree('uncheck', node.target);
                                            $.messager.alert("已有相同的模板被勾选","已有相同的模板被勾选");
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
             $("#tt123").tree("expandAll");
        },
        error: function (req) {
            $.messager.alert("获取打回树失败！");
            return;
        }
    });
}
var Childfatherdata="";
var ChichildData="";
///打回模板
function DHData() {
debugger;
    var node = $("#tt123").tree('getChecked');
    if (node.length > 0) 
    {
        var fatherdata = [];
        var childData = [];
        var thisjsdm = ParamDic["JSDM"];
        var thismbdm = ParamDic["MBDM"];
        fatherdata.push(thisjsdm);
        childData.push(thismbdm);
        for (var i = 0; i < node.length; i++) 
        {
            if (node[i].attributes == 1) {
                var childdm = [];
                var fatherdm = [];
                var parent = $("#tt123").tree('getParent', node[i].target);
//                if(parent.attributes==1)
//                {
//                   var mbdm=node[i].id;
//                   Childnode(mbdm);
//                   fatherdata.push(Childfatherdata);
//                   childData.push(ChichildData);
//                }
//                else
//                {
                    childdm.push(node[i].id, node[i].text, parent.id);
                    fatherdm.push(parent.id);
                    childData.push(childdm);
                    fatherdata.push(fatherdm);
//                }
            }
        }
        //设置打回标记
        $("#HidTJ").val("0");
        $("#HidJS").val(fatherdata.join("|"));
        $("#HidDH").val(childData.join("|"));
        $("#WinJS").window('close');
        OpenSmqr("0");
    }
    else 
    {
        $.messager.alert("请选择要打回的模板");
    }
}

//function Childnode(mbdm)
//{
//      $.ajax({
//            type: 'get',
//            url: '../Excels/Data/InitMBHandle.ashx?action=Childnode&MBDM='+mbdm+'&LCDM='+ParamDic["LCDM"]+'&JHFADM='+ParamDic["FADM"]+'&HSZXDM='+ParamDic["HSZXDM"]+'',
//            async: false,
//            cache: false,
//            success: function (result) {
//            debugger
//                Childfatherdata=result.split(',')[0];
//                ChichildData=result.split(',')[1];
//            }
//        });
//}
//填报、审批、打回处理
//action:SB：填报
//       SP：审批
//       DH：打回
// MBDM：模板代码
// JSDM：角色代码
function SP(action, MBDM, JSDM) {
    $.ajax({
        type: 'get',
        url: '../Data/InitMBHandle.ashx',
        data: {
            MBDM: MBDM,
            JSDM: JSDM,
            action: action,
            LCDM:ParamDic["LCDM"],
            JHFADM: ParamDic["FADM"],
            HSZXDM: ParamDic["HSZXDM"],
            SPYJ:smqr,
            ACTIONRIGHT: ParamDic["ACTIONRIGHT"]
        },
        async: true,
        cache: false,
        success: function (result) {
           
        },
        error: function () {
            $.messager.alert("操作失败!");
            return;
        }
    });
}

//打印
function Print() {
    debugger;
    var active_sheet = spread.getActiveSheet();
//    var rowCount = active_sheet.getRowCount();
//    var columnCount = active_sheet.getColumnCount();
//    active_sheet.setRowPageBreak(rowCount, true);
//    active_sheet.setColumnPageBreak(columnCount, true);
//    spread.print();

    var printInfo = active_sheet.printInfo();
    printInfo.orientation(GC.Spread.Sheets.Print.PrintPageOrientation.portrait);
    spread.print();

}

//出示对话框：
function dialogshowwin() {
    var dlgmsg = $('#dlgmsg');
    dlgmsg.dialog({
        width: 400,
        height: 220,
        top: ($(window).height() - 200) * 0.5,
        left: ($(window).width() - 400) * 0.5
    });
    dlgmsg.dialog('open');
}

function dialogshow(_shint, _clear, _val) {
    var bmdlgdivtxt = $('#bmdlgdivtxt');
    var bmdlgprg = $('#bmdlgprg');
    var bmdlgtxt = $('#bmdlgtxt');
    if (_val) {
        if (bmdlgdivtxt.css("height") != '80px') {
            bmdlgdivtxt.css("height", "80px");
            bmdlgprg.css("display", "block");
        }
        bmdlgprg.progressbar('setValue', parseInt(_val));
    } else {
        bmdlgprg.css("display", "none");
        if (bmdlgdivtxt.css("height") != '130px')
            bmdlgdivtxt.css("height", "130px");
    }
    if (_shint && _shint != '') {
        bmdlgtxt.textbox('readonly', true);
        if (_clear != true) {
            var cont = bmdlgtxt.textbox('getValue');
            _shint = _shint + '\n\r' + cont;
        }
        bmdlgtxt.textbox('setValue', _shint);
    }
    $('#dlgmsg').dialog('refresh');
}

//关闭对话框：
function dialogclose() {
    $('#bmdlgtxt').textbox('setValue', '');
    $('#dlgmsg').dialog('close');
}

//打开错误提示对话框：
function BmShowWinError(_title, _content, _isnotvisble) {
    $('#winerror').dialog({
        content: _title,
        modal: true,
        width: 400,
        height: 200,
        top: ($(window).height() - 200) * 0.5,
        left: ($(window).width() - 400) * 0.5,
        onClose: function () {
            $('#winerror').dialog('options').content = '';
        },
        buttons: [{
            text: '查看明细',
            iconCls: 'icon-ok',
            handler: function () {
                
                $('#winerror').dialog({
                    content: _content,
                    width: 800,
                    height: 400,
                    top: ($(window).height() - 400) * 0.5,
                    left: ($(window).width() - 800) * 0.5
                });
                $('.dialog-button').css('text-align', 'center');
                $('#winerror').dialog('open');
            }
        }, {
            text: '关闭提示',
            iconCls: 'icon-cancel',
            handler: function () {
                BmShowWinClose();
            }
        }]
    });
    $('.dialog-button').css('text-align', 'center');
    $('#winerror').dialog('open');
}

//关闭错误提示对话框：
function BmShowWinClose() {
    $('#winerror').dialog('close');
}


//弹出easyui遮罩层
function easyuiload() {  
    $("<div class=\"datagrid-mask\"></div>").css({ display: "block",opacity:1, width: "100%", height: "100%"}).appendTo("body");  
    //$("<div class=\"datagrid-mask-msg\" id=\"CS\"></div>").html("请稍候。。。。。").appendTo("body").css({ display: "block",height:40,left: ($(document.body).outerWidth(true) - 190) / 2, top: ($(window).height() - 45) / 2 });
}
//取消easyui遮罩层
function easyuidisLoad() {  
    $(".datagrid-mask").remove();  
    //$(".datagrid-mask-msg").remove();  
}

//提示框
function easyuiConfirm(title,msg,_funok,_funno){
    easyuiload();
    $.messager.confirm(title, msg, function(r){
	    if (r){
		   _funok();
	    }else{
           _funno();
        }
    });
}



function msgbox(title, content, func, cancel, focus, icon, okval, cancelval, scroll) {
    /*		
    参数列表说明:
    title :弹出对话框的标题,标题内容最好在25个字符内,否则会导致显示图片的异常															
    text  :弹出对话框的内容,可以使用HTML代码,例如<font color='red'>删除么?</font>,如果直接带入函数,注意转义
    func  :弹出对话框点击确认后执行的函数,需要写全函数的引用,例如add(),如果直接带入函数,注意转义。
    cancel:弹出对话框是否显示取消按钮,为空的话不显示,为1时显示
    focus :弹出对话框焦点的位置,0焦点在确认按钮上,1在取消按钮上,为空时默认在确认按钮上
    icon  :弹出对话框的图标
    */
    if (null != icon) {
        icon = "../Data/msgbox_" + icon + ".png";
    }
    create_mask();
    if (null == okval) {
        okval = '确定';
    }
    if (null == cancelval) {
        cancelval = '取消';
    }
    var temp = "<div style=\"width:520px;border: 2px solid #E0ECFF;background-color: #fff; font-weight: bold;font-size: 12px;\" >"
				+ "<div style=\"line-height:25px; padding:0px 5px;	background-color: #E0ECFF;\">" + title + "</div>"
				+ "<table  cellspacing=\"0\" border=\"0\"><tr>";
    if (null != icon) {
        temp += "<td style=\" padding:0px 0px 0px 20px; \"><img src=\"" + icon + "\" width=\"64\" height=\"64\"></td>";
    }
    if (null != scroll) {
        temp += "<td ><div style=\"width:480px;height:200px;background-color: #fff; font-weight: bold;font-size: 12px;padding:20px; text-align:left; overflow-y:scroll;\">"
    } else {
        temp += "<td ><div style=\"background-color: #fff; font-weight: bold;font-size: 12px;padding:20px; text-align:center;\">"
    }
    temp += content
				+ "</div></td></tr></table>"
				+ "<div style=\"text-align:center; padding:10px 0px 10px;background-color: #E0ECFF;\"><input type='button'  style=\"border:1px solid #CCC; background-color:#CCC; width:90px; height:25px;\" value='" + okval + "'id=\"msgconfirmb\"   onclick=\"remove();" + func + ";\">";
    if (null != cancel) {
        temp += "&nbsp;&nbsp;&nbsp;<input type='button' style=\"border:1px solid #CCC; background-color:#CCC; width:90px; height:25px;\" value='" + cancelval + "'  id=\"msgcancelb\"   onClick='remove()'>";
    }
    temp += "</div></div>";
    create_msgbox(540, 300, temp);
    if (focus == 0 || focus == "0" || null == focus) { document.getElementById("msgconfirmb").focus(); }
    else if (focus == 1 || focus == "1") { document.getElementById("msgcancelb").focus(); }
}

function get_width() {
    return (document.body.clientWidth + document.body.scrollLeft);
}

function get_height() {
    return (document.body.clientHeight + document.body.scrollTop);
}

function get_left(w) {
    var bw = document.body.clientWidth;
    var bh = document.body.clientHeight;
    w = parseFloat(w);
    return (bw / 2 - w / 2 + document.body.scrollLeft);
}

function get_top(h) {
    var bw = document.documentElement.clientWidth;
    var bh = document.documentElement.clientHeight;
    h = parseFloat(h);
    return (bh / 2 - h / 2 + document.body.scrollTop);
}

function create_mask() {//创建遮罩层的函数
    var bw = document.documentElement.clientWidth;
    var bh = document.documentElement.clientHeight;
    var mask = document.createElement("div");
    mask.id = "mask";
    mask.style.position = "absolute";
    mask.style.filter = "alpha(opacity=30)"; 
    mask.style.opacity = 0.3; //Mozilla的不透明设置
    mask.style.background = "#ccc";
    mask.style.backgroundcolor = "transparent";
    mask.style.top = "0px";
    mask.style.left = "0px";
    mask.style.width = bw+"px";
    mask.style.height = bh+"px";
    mask.style.zIndex = 1000;
    document.body.appendChild(mask);
}
function create_msgbox(w, h, t) {//创建弹出对话框的函数
    //            debugger;
    var box = document.createElement("div");
    box.id = "msgbox";
    box.style.position = "absolute";
    box.style.width = w + "px";
    box.style.height = h + "px";
    box.style.overflow = "visible";
    box.innerHTML = t;
    box.style.zIndex = 1001;
    document.body.appendChild(box);
    re_pos();
}
function re_mask() {
    /*
    更改遮罩层的大小,确保在滚动以及窗口大小改变时还可以覆盖所有的内容
    */
    var mask = document.getElementById("mask");
    if (null == mask) return;
    mask.style.width = get_width() + "px";
    mask.style.height = get_height() + "px";
}
function re_pos() {
    /*
    更改弹出对话框层的位置,确保在滚动以及窗口大小改变时一直保持在网页的最中间
    */
    //            debugger;
    var box = document.getElementById("msgbox");
    if (null != box) {
        var w = box.style.width;
        var h = box.style.height;
        box.style.left = get_left(w) + "px";
        box.style.top = get_top(h) + "px";
    }
}
function remove() {
    /*
    清除遮罩层以及弹出的对话框
    */
    var mask = document.getElementById("mask");
    var msgbox = document.getElementById("msgbox");
    if (null == mask && null == msgbox) return;
    document.body.removeChild(mask);
    document.body.removeChild(msgbox);
}

function re_show() {
    /*
    重新显示遮罩层以及弹出窗口元素
    */
    re_pos();
    re_mask();
}
function load_func() {
    /*
    加载函数,覆盖window的onresize和onscroll函数
    */
    window.onresize = re_show;
    window.onscroll = re_show;
}

/// <summary>对datagrid控件添加遮罩</summary>
/// <param name="_datagrid" type="datagird">要进行遮罩的datagrid控件</param>
function datagridloading(_datagrid) {
    var opts = _datagrid.datagrid("options");
    var wrap = $.data(_datagrid[0], "datagrid").panel;
    if (opts.loadMsg) {
        $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: wrap.width(), height: wrap.height() }).appendTo(wrap);
        $("<div class=\"datagrid-mask-msg\"></div>").html(opts.loadMsg).appendTo(wrap).css({ display: "block", left: (wrap.width() - $("div.datagrid-mask-msg", wrap).outerWidth()) / 2, top: (wrap.height() - $("div.datagrid-mask-msg", wrap).outerHeight()) / 2 });
    }
}

function datagridloaded(_datagrid) {
    _datagrid.datagrid("getPager").pagination("loaded");
    var wrap = $.data(_datagrid[0], "datagrid").panel;
    wrap.find("div.datagrid-mask-msg").remove();
    wrap.find("div.datagrid-mask").remove();
}

