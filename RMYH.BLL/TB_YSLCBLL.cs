﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using RMYH.DAL;

namespace RMYH.BLL
{
    public partial class TB_YSLCBLL
    {
        private readonly TB_YSLCDAL dal = new TB_YSLCDAL();
        /// 获取模板
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="state">状态(-1:表示上报；0表示打回；1表示审批)</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        /// <param name="actionright">模块还是模板</param>
        /// <returns>-1表示有未完成的前置模板,Flag=0表示失败，Flag>1表示成功</returns>
        public int HQMB(string jsdm, string mbdm, string state, int lcdm, string jhfadm, string hszxdm, string actionright,string spyj)
        {
            return dal.HQMB(jsdm,mbdm,state,lcdm,jhfadm,hszxdm,actionright,spyj);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <param name="_mblb">模板类型</param>
        /// <returns></returns>
        public string GetMBListSQL(string JHFADM, string ISDone, string _mblb, string ZYDM)
        {
            return dal.GetMBListSQL(JHFADM, ISDone, _mblb, ZYDM);
        }
        /// <summary>
        /// 查看操作人未审批或者已审批的模板
        /// </summary>
        /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
        /// <param name="MBLX">模板类型 (TB_YSBBMB.MBLX) </param>
        /// <param name="IsDone">-1代表未做；0,1代表已做</param>
        /// <returns></returns>
        public DataTable GetMBCZQX(string MBDM, int CZLX, string IsDone, string JHFADM, string MBLB, string ISZLC, string USERDM, string HSZXDM)
        {
            return dal.GetMBCZQX(MBDM, CZLX, IsDone, JHFADM, MBLB, ISZLC, USERDM, HSZXDM);
        }
    }
}
