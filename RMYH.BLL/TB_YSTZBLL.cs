﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RMYH.DAL;
using System.Collections;

namespace RMYH.BLL
{
    public class TB_YSTZBLL
    {
        private readonly TB_YSTZDAL dal = new TB_YSTZDAL();

        /// <summary>
        /// 预算调整时，向预算记录表中插入相关的记录
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <param name="JSDM">调整人所属角色</param>
        /// <param name="USERDM">调整人</param>
        /// <param name="List">返回存储SQL的List<String>集合</param>
        public void InsertYSTZJL(string JHFADM, string MBDM, string ZT, string JSDM, string USERDM, ref List<String> List, string MenuBZ)
        {
             dal.InsertYSTZJL(JHFADM,MBDM,ZT,JSDM,USERDM, ref List,MenuBZ);
        }

        /// <summary>
        /// 预算调整时，向预算记录表中插入相关的记录
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <param name="JSDM">调整人所属角色</param>
        /// <param name="USERDM">调整人</param>
        /// <param name="List">返回存储SQL的List<String>集合</param>
        public void InsertTZJL(string JHFADM, string MBDM, string ZT, string JSDM, string USERDM, ref ArrayList List, string MenuBZ)
        {
            dal.InsertTZJL(JHFADM, MBDM, ZT, JSDM, USERDM, ref List, MenuBZ);
        }

        /// <summary>
        /// 预算调整填报或者审批处理事件
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="USERDM">用户代码</param>
        /// <param name="MenuType">菜单类型</param>
        /// <param name="ZT">操作标识（1表示保存；2表示上报；3表示审批）</param>
        /// <returns>返回0表示执行失败；返回1表示执行成功</returns>
        public int YSTZSBOrSP(string JHFADM, string MBDM, string JSDM, string ZT, string USERDM, string MenuType)
        {
            return dal.YSTZSBOrSP(JHFADM,MBDM,JSDM,ZT,USERDM,MenuType);
        }
        /// <summary>
        /// 获取插上个节点的模板修改项目列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        ///<param name="TZBZ">调整页面标识（TZSB表示上报；TZSP表示审批）</param>
        public string GetLastTZMX(string JHFADM, string MBDM, string JSDM, string TZBZ)
        {
            return dal.GetLastTZMX(JHFADM,MBDM,JSDM,TZBZ);
        }

        /// <summary>
        /// 获取流程待处理模板的所有上级节点模板的ID值
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetNotDealAllParID(string JHFADM)
        {
            return dal.GetNotDealAllParID(JHFADM);
        }
    }
}
