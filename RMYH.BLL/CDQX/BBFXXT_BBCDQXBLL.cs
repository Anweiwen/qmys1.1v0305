﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RMYH.BLL.CDQX;
using System.Data;

namespace RMYH.BLL.CDQX
{
    public partial class BBFXXT_BBCDQXBLL
    {
        private readonly RMYH.DAL.CDQX.BBFXXT_BBCDQXDAL dal = new RMYH.DAL.CDQX.BBFXXT_BBCDQXDAL();
        public BBFXXT_BBCDQXBLL()
        {
 
        }
        #region Method
        /// <summary>
        /// 判断当前用户是否为用户组
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <returns></returns>
        public bool IsUSEROrGroup(string USERDM)
        {
            return dal.IsUSEROrGroup(USERDM);
        } 
        /// <summary>
        /// 获取当前用户或者用户组的报表权限
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="Flag">标识（true代表用户组；false代表用户）</param>
        /// <param name="BZ">标识（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public DataSet GetBBBMByUSERDM(string USERDM, bool Flag,string BZ)
        {
            return dal.GetBBBMByUSERDM(USERDM,Flag,BZ);
        }
        /// <summary>
        /// 将权限数据集赋给相应的用户或者用户组
        /// </summary>
        /// <param name="USERDM">用户或者用户组</param>
        /// <param name="DS">权限数据集</param>
        /// <param name="Flag">是否为用户组（true代表是用户组；false代表是用户）</param>
        /// <param name="BZ">标识报表或者菜单（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public int InSertBBQX(string USERDM, DataSet DS, bool Flag,string BZ)
        {
            return dal.InSertBBQX(USERDM,DS,Flag,BZ);
        }
        /// <summary>
        /// 判断用户是否有此报表菜单的权限
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="BBBM">报表代码</param>
        /// <param name="F">标识（true代表用户组；false代表用户）</param>
        /// <param name="BZ">标识报表或者菜单（0代表菜单，1代表报表）</param>
        /// <returns></returns>
        public bool IsHavingBBByUSERDM(string USERDM, string BBBM, bool F,string BZ)
        {
            return dal.IsHavingBBByUSERDM(USERDM,BBBM,F,BZ);
        }
        /// <summary>
        /// 根据用户组代码获取旗下的用户
        /// </summary>
        /// <param name="GROUP_ID">用户组代码</param>
        /// <returns></returns>
        public DataSet GetUSERDMByGroupDM(string GROUP_ID)
        {
            return dal.GetUSERDMByGroupDM(GROUP_ID);
        }
         /// <summary>
        /// 向TB_BBBZXMBM中插入数据
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMFL">项目分类代码</param>
        /// <param name="XMBZBM">项目标准编码</param>
        /// <param name="CCJB">层次界别</param>
        /// <param name="YJDBZ">叶子节点标识（1代表是，0代表不是叶子节点）</param>
        /// <param name="FBDM">父节点代码</param>
        /// <returns></returns>
        public int InsertXMFLMX(string ID, string XMFL, string XMBZBM, int CCJB, string YJDBZ, string FBDM,string xmmc)
        {
            return dal.InsertXMFLMX(ID, XMFL, XMBZBM, CCJB, YJDBZ, FBDM,xmmc);
        }
         /// <summary>
        /// 根据主键ID获取CCJB字段值
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public int GetCCJBByID(string ID)
        {
            return dal.GetCCJBByID(ID);
        }
        /// <summary>
        /// 根据主键ID获取项目标识代码
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public string GetXMBZDMByID(string ID)
        {
            return dal.GetXMBZDMByID(ID);
        }
         /// <summary>
        /// 判断当前的项目标准编码是否存在
        /// </summary>
        /// <param name="XMBZBM">项目标准编码</param>
        /// <returns></returns>
        public bool IsHavingXMBZBM(string XMBZBM)
        {
            return dal.IsHavingXMBZBM(XMBZBM);
        }
        /// <summary>
        /// 根据ID更新相关的项目明细相关的信息
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMBZBM">项目标准代码</param>
        /// <param name="XMMC">项目名称</param>
        /// <param name="IsGXBM">是否更新标准编码</param>
        /// <returns></returns>
        public int UpdateXMMXByID(string ID, string XMBZBM, string XMMC, bool IsGXBM)
        {
            return dal.UpdateXMMXByID(ID,XMBZBM,XMMC,IsGXBM);
        }
        /// <summary>
        /// 根据ID更新项目标编码
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <param name="XMBZBM">项目标准代码</param>
        /// <returns></returns>
        public int UpdateXMBZBMByID(string ID, string XMBZBM)
        {
            return dal.UpdateXMBZBMByID(ID,XMBZBM);
        }
        /// <summary>
        /// 根据年份和核算中心代码查询相关的信息
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetHSZXInfo(string HSZXDM, string YY)
        {
            return dal.GetHSZXInfo(HSZXDM,YY);
        }
        /// <summary>
        /// 根据年份和核算中心代码查询相关的产品类型信息
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetCPLXInfo(string HSZXDM, string YY,string jhlx)
        {
            return dal.GetCPLXInfo(HSZXDM,YY,jhlx);
        }
        /// <summary>
        /// 向表中插入数据
        /// </summary>
        /// <param name="ID">项目ID</param>
        /// <param name="DM">实际代码</param>
        /// <returns></returns>
        public int InsertABCM(string ID, string DM,string hszxdm)
        {
            return dal.InsertABCM(ID,DM,hszxdm);
        }
        /// <summary>
        /// 根据ID 获取项目分类明细的相关信息
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public DataSet GetBZBMInfoByID(string ID)
        {
            return dal.GetBZBMInfoByID(ID);
        }
        /// <summary>
        /// 根据表名字段值条件，查询相关的字段内容数据集
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="CXZD">查询字段</param>
        /// <param name="TJ">条件字段</param>
        /// <param name="TJValues">条件字段值</param>
        /// <returns>返回数据集</returns>
        public DataSet GetData(string TableName, string CXZD, string TJ, string TJValues)
        {
            return dal.GetData(TableName,CXZD,TJ,TJValues);
        }
        /// <summary>
        /// 判断当前菜单是否为父节点报表
        /// </summary>
        /// <param name="ID">报表ID</param>
        /// <param name="NAME">报表名称</param>
        /// <returns></returns>
        public bool ISBBMX(string ID, string NAME)
        {
            return dal.ISBBMX(ID,NAME);
        }
         /// <summary>
        /// 根据报表代码以及年份获取此报表下面所有的列
        /// </summary>
        /// <param name="YY">年份</param>
        /// <param name="COLBM">报表代码</param>
        /// <returns></returns>
        public DataSet GetCoLNameDS(string YY, string COLBM)
        {
            return dal.GetCoLNameDS(YY,COLBM);
        }
         /// <summary>
        /// 新增物料的相关操作
        /// </summary>
        /// <param name="WLName">物料名称</param>
        /// <param name="JLDW">计量单位</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <param name="MM">月份</param>
        /// <param name="DD">天数</param>
        /// <param name="USERDM">当前登录人代码</param>
        /// <param name="ZYDM">作业代码</param>
        /// <param name="COLBM">报表编码</param>
        /// <returns></returns>
        public int InsertWL(string WLName, string JLDW, string HSZXDM, string YY, string MM, string DD, string USERDM, string ZYDM, string COLBM,string BZNR)
        {
            return dal.InsertWL(WLName, JLDW, HSZXDM, YY, MM, DD, USERDM, ZYDM, COLBM,BZNR);
        }
        /// <summary>
        /// 将项目名称以及公式，插入到数据库
        /// </summary>
        /// <param name="GSStr">项目以及公式的字符串</param>
        /// <param name="FUNCID">模板代码</param>
        /// <returns></returns>
        public int InsertGS(string GSStr, string FUNCID)
        {
            return dal.InsertGS(GSStr,FUNCID);
        }
        /// <summary>
        /// 将Excel中的数据，插入到数据库
        /// </summary>
        /// <param name="GSStr">项目以及公式的字符串</param>
        /// <param name="FUNCID">模板代码</param>
        /// <param name="Data">日期</param>
        /// <param name="TableName">数据表名称</param>
        /// <returns></returns>
        public int InsertDataByGS(string GSStr, string FUNCID, string Data, string TableName)
        {
            return dal.InsertDataByGS(GSStr, FUNCID, Data, TableName);
        }
        /// <summary>
        /// 根据sql语句查询相关的数据
        /// </summary>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="YY">年份</param>
        /// <returns></returns>
        public DataSet GetDataBySql(string HSZXDM, string YY)
        {
            return dal.GetDataBySql(HSZXDM,YY);
        }
        /// <summary>
        /// 根据项目代码和采集装置的名称删除记录
        /// </summary>
        /// <param name="ID">需删除的主键的字符串</param>
        /// <param name="EID">需修改的主键字符串</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="WLDM">物料代码</param>
        /// <returns></returns>
        public int DelDWL(string ID,string EID,string HSZXDM,string WLDM)
        {
            return dal.DelDWL(ID,EID,HSZXDM,WLDM);
        }
        /// <summary>
        /// 根据核算中心代码和作业代码查询相关采集项目是否存在
        /// </summary>
        /// <param name="ZYDM">作业代码</param>
        /// <returns></returns>
        public bool IsExistDWL(string ZYDM)
        {
            return dal.IsExistDWL(ZYDM);
        }
        #endregion
    }
}
