using System;
using System.Data;
using System.Collections.Generic;
using RMYH.Common;
using RMYH.Model;
using System.Collections;
using RMYH.DAL;
namespace RMYH.BLL
{
	/// <summary>
	/// TB_ZDSXB
	/// </summary>
	public partial class TB_ZDSXBBLL
	{
        private readonly TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
		public TB_ZDSXBBLL()
		{}
		#region  Method
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TB_ZDSXBModel GetModel(string XMDM)
        {
            return dal.GetModel(XMDM);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 执行查询语句
        /// </summary>
        /// <param name="SQL"></param>
        /// <returns></returns>
        public DataSet Query(string SQL)
        {
            return dal.Query(SQL);
        }
        /// <summary>
        /// 根据角色代码获取用户代码
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetUSERDMByJSDM(string JSDM)
        {
            return dal.GetUSERDMByJSDM(JSDM);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMDM"></param>
        /// <param name="IDName"></param>
        /// <param name="IDValue"></param>
        /// <returns></returns>
        public DataSet Query(string XMDM, string IDName, string IDValue)
        {
            return dal.Query(XMDM, IDName, IDValue);
        }
        /// <summary>
        /// 增加、修改数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="filedname">字段名,字段间以“,”隔开</param>
        /// <param name="filedvalue">字段值，字段间以“|”隔开</param>
        /// <param name="id">主键名</param>
        /// <param name="idvalue">主键值</param>
        /// <returns></returns>
        //public string Update(string tablename, string filedname, string[] filedvalue, string id, string[] idvalue)
        //{
        //    return Update("", tablename, filedname, filedvalue, id, idvalue, null, null);
        //}
        /// <summary>
        /// 增加、修改数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="filedname">字段名,字段间以“,”隔开</param>
        /// <param name="filedvalue">字段值，字段间以“|”隔开</param>
        /// <param name="id">主键名</param>
        /// <param name="idvalue">主键值</param>
        /// <returns></returns>
        public string Update(string xmdm, string tablename, string filedname, string[] filedvalue, string id, string[] idvalue)
        {
            return Update(xmdm, tablename, filedname, filedvalue, id, idvalue, null, null);
        }
        /// <summary>
        /// 增加、修改数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="filedname">字段名,字段间以“,”隔开</param>
        /// <param name="filedvalue">字段值，字段间以“|”隔开</param>
        /// <param name="id">主键名</param>
        /// <param name="idvalue">主键值</param>
        /// <param name="extfiledname">隐藏保存字段，字段间以“,”隔开</param>
        /// <param name="extvalue">隐藏保存值，值间以“,”隔开</param>
        /// <returns></returns>
        //public string Update(string tablename, string filedname, string[] filedvalue, string id, string[] idvalue,string[] extfiledname,string[] extvalue)
        //{
        //    return Update("", tablename, filedname, filedvalue, id, idvalue, extfiledname, extvalue);
        //}
        /// <summary>
        /// 增加、修改数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="filedname">字段名,字段间以“,”隔开</param>
        /// <param name="filedvalue">字段值，字段间以“|”隔开</param>
        /// <param name="id">主键名</param>
        /// <param name="idvalue">主键值</param>
        /// <param name="extfiledname">隐藏保存字段，字段间以“,”隔开</param>
        /// <param name="extvalue">隐藏保存值，值间以“,”隔开</param>
        /// <returns></returns>
        public string Update(string xmdm, string tablename, string filedname, string[] filedvalue, string id, string[] idvalue, string[] extfiledname, string[] extvalue)
        {
            return dal.Update(xmdm, tablename, filedname, filedvalue, id, idvalue, extfiledname, extvalue);
        }
        public string GetSeed(string TableName, string Key)
        {
            return dal.GetSeed(TableName, Key);
        }
        /// <summary>
        /// 返回自动生成的字符串主键值
        /// </summary>
        /// <returns></returns>
        public string GetStringPrimaryKey()
        {
            return dal.GetStringPrimaryKey();
        }
        /// <summary>
        /// 添加默认值数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="XMDM">项目代码</param>
        /// <param name="IndexFile">主键字段（数据库中行标记一般为ID）</param>
        /// <param name="IndexValue">主键值</param>
        /// <param name="IsTheChild">当前是否添加子节点</param>
        /// <param name="IsChild">是否多层树状结构</param>
        /// <param name="strfiled">自定义字段</param>
        /// <param name="strvalue">自定义字段值</param>
        /// <returns></returns>
        public bool AddData(string tablename, string XMDM, string IndexFile, string[] strfiled, string[] strvalue)
        {
            return dal.AddData(tablename, XMDM, IndexFile, "", false, false, strfiled, strvalue, "");
        }
        /// <summary>
        /// 添加默认值数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="XMDM">项目代码</param>
        /// <param name="IndexFile">主键字段（数据库中行标记一般为ID）</param>
        /// <param name="IndexValue">主键值</param>
        /// <param name="IsTheChild">当前是否添加子节点</param>
        /// <param name="IsChild">是否多层树状结构</param>
        /// <param name="strfiled">自定义字段</param>
        /// <param name="strvalue">自定义字段值</param>
        /// <returns></returns>
        public bool AddData(string tablename, string XMDM, string IndexFile, string IndexValue, bool IsChild, bool IsTheChild, string[] strfiled, string[] strvalue)
        {
            return dal.AddData(tablename, XMDM, IndexFile, IndexValue, IsChild, IsTheChild, strfiled, strvalue, "");
        }
        /// <summary>
        /// 添加默认值数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="XMDM">项目代码</param>
        /// <param name="IndexFile">主键字段（数据库中行标记一般为ID）</param>
        /// <param name="IndexValue">主键值</param>
        /// <param name="IsTheChild">当前是否添加子节点</param>
        /// <param name="IsChild">是否多层树状结构</param>
        /// <param name="strfiled">自定义字段</param>
        /// <param name="strvalue">自定义字段值</param>
        /// <param name="BM">内部编码（父节点编码+当前记录代码）</param>
        /// <returns></returns>
        public bool AddData(string tablename, string XMDM, string IndexFile, string IndexValue, bool IsChild, bool IsTheChild, string[] strfiled, string[] strvalue, string BM)
        {
            return dal.AddData(tablename, XMDM, IndexFile, IndexValue, IsChild, IsTheChild, strfiled, strvalue, BM);
        }
        /// <summary>
        ///  删除数据
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="DM">主键值，同时删除多个以","隔开</param>
        /// <returns></returns>
        public string DeleteData(string tablename, string DM, string filedname)
        {
            return dal.DeleteData(tablename, DM, filedname);
        }
        /// <summary>
        /// 系统登录
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public string toLogin(string uid, string pwd,string Year)
        {
            return dal.toLogin(uid, pwd,Year);
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldpwd"></param>
        /// <param name="newpwd"></param>
        /// <returns></returns>
        public int ChangePWD(string oldpwd, string newpwd)
        {
            return dal.ChangePWD(oldpwd, newpwd);
        }
        /// <summary>
        /// 获取权限菜单
        /// </summary>
        /// <returns></returns>
        public DataSet GetListMenu(string USERDM)
        {
            return dal.GetListMenu(USERDM);
        }
        public DataSet GetAllMenuByUSERDM(string USERDM)
        {
            return dal.GetAllMenuByUSERDM(USERDM);
        }
        /// <summary>
        /// 获取自定义菜单
        /// </summary>
        /// <returns></returns>
        public DataSet GetCustomMenu()
        {
            return dal.GetCustomMenu();
        }
        /// <summary>
        /// 执行简单的查询语句
        /// </summary>
        /// <param name="tablename">查询表名</param>
        /// <param name="fileds">字段名，多个字段时以逗号隔开</param>
        /// <param name="where">条件，不包含where</param>
        /// <returns></returns>
        public DataSet getDataSet(string tablename, string fileds, string where)
        {
            return dal.getDataSet(tablename, fileds, where);
        }
        /// <summary>
        /// 执行简单的查询语句
        /// </summary>
        /// <param name="tablename">查询表名</param>
        /// <param name="fileds">字段名，多个字段时以逗号隔开</param>
        /// <returns></returns>
        public DataSet getDataSet(string tablename, string fileds)
        {
            return dal.getDataSet(tablename, fileds, "");
        }
        /// <summary>
        /// 批量执行SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <param name="cmdParms">条件字符串</param>
        /// <returns></returns>
        public int ExecuteSql(string[] sqlString, ArrayList cmdParms)
        {
            return dal.ExecuteSql(sqlString, cmdParms);
        }
        /// <summary>
        /// 执行单条SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <returns></returns>
        public int ExecuteSql(string sqlString)
        {
            return dal.ExecuteSql(sqlString);
        }
        /// <summary>
        /// 数据更新（带自定义行）
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="hFiled">行字段</param>
        /// <param name="lFiled">列字段</param>
        /// <param name="strValues">参数值，行记录以“,”隔开，数据之间以“|”隔开，数据间的顺序为 行代码|列代码|值</param>
        /// <param name="YYNNDD">时间字段，不允许为空</param>
        /// <returns></returns>
        public int UpdateHang(string tablename, string hFiled, string lFiled, string strValues, string YYNNDD)
        {
            return dal.UpdateHang(tablename, hFiled, lFiled, strValues, YYNNDD);
        }
        /// <summary>
        /// 添加审批状态
        /// </summary>
        /// <param name="SPXMDM">待审批项目代码</param>
        /// <param name="XMDM">流程项目代码</param>
        /// <param name="zt">审批状态</param>
        /// <param name="SPYJ">审批意见</param>
        /// <returns>0：执行异常；1：执行成功；2：当前用户没有审批权利；3：此单据已经审批结束；</returns>
        public int addSH(string SPXMDM, string XMDM, int zt, string SPYJ)
        {
            return dal.addSH(SPXMDM, XMDM, zt, SPYJ);
        }
        /// <summary>
        /// 获取部门及用户信息列表
        /// </summary>
        /// <returns></returns>
        public DataSet getDeptUser()
        {
            return dal.getDeptUser();
        }
        /// <summary>
        /// 获取菜单标签
        /// </summary>
        /// <returns></returns>
        public string getMenuTitle(string strurl)
        {
            return dal.getMenuTitle(strurl);
        }
         /// <summary>
        /// 递归调用，查询模板类型的填报的模板代码字符串
        /// </summary>
        /// <param name="MBDM"></param>
        /// <returns></returns>
        public string GetDHMB(string MBDM,string JHFADM,string MBLB)
        {
            return dal.GetDHMB(MBDM,JHFADM,MBLB);
        }
         /// <summary>
        /// 获取层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLB">模板类别</param>
        /// <returns></returns>
        public string GetCJBM(string JHFADM, string MBDM, string MBLB)
        {
            return dal.GetCJBM(JHFADM,MBDM,MBLB);
        }
        /// <summary>
        /// 模板数据上报
        /// </summary>
        /// <param name="ZT">1 填报上报;2 审批;3 打回</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="DHMBDM">打回到的模板</param>
        /// <returns>Flag大于1，表示执行成功；Flag等于0，表示的执行不成功；Flag等于-1，表示没有权限</returns>
        public int ZTTJ(int ZT, string MBDM, string JHFADM,string DHMBDM,string MBLB,string DHYY,string SPYJ,string SFZLC)
        {
            return dal.ZTTJ(ZT, MBDM, JHFADM,DHMBDM,MBLB,DHYY,SPYJ,SFZLC);
        }
        /// <summary>
        /// 查看操作人未审批或者已审批的模板
        /// </summary>
        /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
        /// <param name="MBLX">模板类型 (TB_YSBBMB.MBLX) </param>
        /// <param name="IsDone">-1代表未做；0,1代表已做</param>
        /// <returns></returns>
        public DataTable GetMBCZQX(string MBDM, int CZLX, string IsDone, string JHFADM, string MBLB, string ISZLC, string USERDM, string HSZXDM)
        {
            return dal.GetMBCZQX(MBDM,CZLX,IsDone,JHFADM,MBLB,ISZLC,USERDM,HSZXDM);
        }
          /// <summary>
        /// 根据计划方案代码获取方案表示（月、年、季度、其他）
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetMBZQByJHFADM(string JHFADM)
        {
            return dal.GetMBZQByJHFADM(JHFADM);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <param name="_mblb">模板类型</param>
        /// <returns></returns>
        public string GetMBListSQL(string JHFADM, string ISDone, string _mblb,string ZYDM)
        {
            return dal.GetMBListSQL(JHFADM,ISDone,_mblb,ZYDM);
        }
        /// <summary>
        /// 获取还未处理的前置模板
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLB">模板代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <returns></returns>
        public string GetDCLQZMBStr(string JHFADM, string MBDM, string MBLB, string ISDone,string HSZXDM)
        {
            return dal.GetDCLQZMBStr(JHFADM,MBDM,MBLB,ISDone,HSZXDM);
        }
        /// <summary>
        /// 执行审批状态还原方法
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public int SPZTHY(string JHFADM, string MBDM, string JSDM, string CJBM)
        {
            return dal.SPZTHY(JHFADM,MBDM,JSDM,CJBM);
        }
         /// <summary>
        /// 查询方案的预算标识
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetYSBSByJHFADM(string JHFADM)
        {
            return dal.GetYSBSByJHFADM(JHFADM);
        }
                /// <summary>
        /// 根据计划方案代码，获取已执行的模板代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetParCJBM(string JHFADM, string MBDM, string MBLX)
        {
            return dal.GetParCJBM(JHFADM,MBDM,MBLX);
        }
        /// <summary>
        /// 查询计划方案下所有的模板状态
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ZYDM">作业代码</param>
        /// <returns></returns>
        public string GetAllMBCXJSON(string JHFADM,string ZYDM,string MBMC,string HSZXDM,string ZT)
        {
            return dal.GetAllMBCXJSON(JHFADM,ZYDM,MBMC,HSZXDM,ZT);
        }
        /// <summary>
        /// 查询待处理模板的所有上级的层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns></returns>
        public string GetDCLCJBM(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            return dal.GetDCLCJBM(JHFADM, MBMC, MBZQ, MBLX);
        }
        /// <summary>
        /// 获取待处理模板以及其所有上级的层级编码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns></returns>
        public string GetDCLAndParCJBM(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            return dal.GetDCLAndParCJBM(JHFADM, MBMC, MBZQ, MBLX);
        }
        /// <summary>
        /// 获取角色代码和模板代码相同且层级编码的相关信息
        /// </summary>
        /// <param name="JSDM">角色代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetCJBM(string JSDM, string MBDM, string CJBM, string JHFADM,string YSLX)
        {
            return dal.GetCJBM(JHFADM,MBDM,CJBM,JHFADM,YSLX);
        }
         /// <summary>
        /// 获取角色代码和模板代码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetMBANDJS(string CJBM)
        {
            return dal.GetMBANDJS(CJBM);
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetChird(string CJBM, string JHFADM,string YSLX)
        {
            return dal.GetChird(CJBM,JHFADM,YSLX);
        }
        /// <summary>
        /// 获取层级编码下的所有顶节点
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <returns></returns>
        public string GetPar(string CJBM, string JHFADM,string YSLX)
        {
            return dal.GetPar(CJBM,JHFADM,YSLX);
        }
        /// <summary>
        /// 根据模板关联下级模板
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="JSDM">角色代码</param>
        /// <returns></returns>
        public string GetDCLChirldMBName(string JHFADM, string MBDM, string JSDM) {
            return dal.GetDCLChirldMBName(JHFADM,MBDM,JSDM);
        }
       /// <summary>
        /// 获取当前模板下的所有模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <returns></returns>
        public string GetAllChirldCJBM(string CJBM, string MBDM, string JSDM, string MBLX, string LX) 
        {
            return dal.GetAllChirldCJBM(CJBM,MBDM,JSDM,MBLX,LX);
        }
         /// <summary>
        /// 获取当前模板下的所有父节点模板的层级编码
        /// </summary>
        /// <param name="CJBM">层级编码</param>
        /// <param name="JSDM">角色代码</param>
        /// <param name="LX">填报类型</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">预算类型</param>
        /// <returns></returns>
        public string GetAllParCJBM(string CJBM, string MBDM, string JSDM, string MBLX)
        {
            return dal.GetAllParCJBM(CJBM, MBDM, JSDM, MBLX);
        }
         /// <summary>
        /// 获取非受限的角色代码
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <returns></returns>
        public string getNotQXJSDM(string HSZXDM,string JHFADM)
        {
            return dal.getNotQXJSDM(HSZXDM,JHFADM);
        }
         /// <summary>
        /// 根据计划方案获取方案的截止时间
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetJZSJByJHFADM(string JHFADM)
        {
            return dal.GetJZSJByJHFADM(JHFADM);
        }
        /// <summary>
        /// 获取配置显示天数或小时
        /// </summary>
        /// <returns></returns>
        public string GetSZCS()
        {
            return dal.GetSZCS();
        }
        /// <summary>
        /// 判断当前用户是否是受截止时间限制的用户
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public bool IsSQUSER(string USERDM, string HSZXDM,string JHFADM)
        {
            return dal.IsSQUSER(USERDM,HSZXDM,JHFADM);
        }
        /// <summary>
        /// 根据及计划方案代码获取是否走流程标记
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>true代表走流程；false代表不走流程</returns>
        public bool IsLCSP(string JHFADM)
        {
            return dal.IsLCSP(JHFADM);
        }
        /// <summary>
        /// 函数功能：获取当前待处理的模板以及所有的上级模板的层及编码
        /// 编程思想：通过GetDCLAndParCJBM函数获取所有待处理模板以及上级模板的层级编码，通过这些层及编码查询当前JHFADM代码的计划方案有没有做完
        ///           具体SQL语句包含两部分：当前还没有填报的模板、当前处于流程中在途的模板（即：在TB_MBLCSB表中等于JHFADM的记录中，STATE=-1的记录）
        ///           如果查询的数据集不为空的话，表示当前还有未处理的记录，否则表示当前计划方案的流程已经走完
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>true代表流程已经完成；false代表流程还未完成</returns>
        public bool IsLCOver(string JHFADM)
        {
            return dal.IsLCOver(JHFADM);
        }
        /// <summary>
        /// 函数功能：根据模板代码，获取其所有父节点的模板代码（相同的模板代码，取层级编码最短的递归）
        /// 编程思想：通过模板代码获取流程表中层级编码最短的结点信息，依次递归，直到顶节点
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <returns>返回模板代码拼接的字符串（用逗号隔开）</returns>
        public string GetAllParMB(string MBDM)
        {
            return dal.GetAllParMB(MBDM);
        }
                /// <summary>
        /// 函数功能：根据待填报和待审核，获取所有的上级模板的模板代码
        /// 编程思想：查询待填报、待审核的信息（sql语句），递归每一个待填报、待审批的模板代码，直至顶节点！
        ///           每个待处理模板的上级模板都存储到List<string>当中
        ///           最后，通过两层循环，将重复的上级模板代码去掉，合成一个字符串
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBMC">模板名称</param>
        /// <param name="MBZQ">模板周期</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns>返回用逗号隔开的所有上级模板代码</returns>
        public string GetDCLParMB(string JHFADM, string MBMC, string MBZQ, string MBLX)
        {
            return dal.GetDCLParMB(JHFADM,MBMC,MBZQ,MBLX);
        }
        /// <summary>
        /// 根据计划方案代码和核算中心代码获取非受限角色和名称
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <returns>返回值包含两部分（角色代码和角色名称）两者之间用特殊符号^分割；角色代码和角色名称内部间用逗号隔开</returns>
        public string getFSXJS(string JHFADM, string HSZXDM)
        {
            return dal.getFSXJS(JHFADM,HSZXDM);
        }

        /// <summary>
        ///  update流程语句
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="ColumnList">列名</param>
        /// <param name="ConditionList">条件</param>
        /// <returns></returns>
        public int UpdateList(string TableName, string ColumnList, string ConditionList)
        {
            return dal.UpdateList(TableName,ColumnList,ConditionList);
        }

        //添加语句
        public int InsertList(string JSDM, string DQMBDM, int STATE, string CREATETIME, string CREATEUSER, string CLSJ, string CLR, string SPZTDM, string JHFADM, string CJBM, string SPYJ)
        {
            return dal.InsertList(JSDM, DQMBDM, STATE, CREATETIME, CREATEUSER, CLSJ, CLR, SPZTDM, JHFADM, CJBM, SPYJ);
        }
		#endregion  Method
	}
}

