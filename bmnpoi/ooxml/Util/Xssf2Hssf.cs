﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace NPOI.Util
{
    public class Xssf2Hssf
    {
        private int lastColumn = 0;
        private Dictionary<int, ICellStyle> styleDic = new Dictionary<int, ICellStyle>();

        public  void transformHSSF(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew)
        {
            ISheet sheetNew, sheetOld;
            //workbookNew.setMissingCellPolicy(workbookOld.getMissingCellPolicy());  
            for (int i = 0; i < workbookOld.NumberOfSheets; i++)
            {
                sheetOld = workbookOld.GetSheetAt(i);
                sheetNew = workbookNew.CreateSheet(sheetOld.SheetName);
                this.transform(workbookOld, workbookNew, sheetOld, sheetNew);
            }
        }

        private void transform(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew,
                ISheet sheetOld, ISheet sheetNew)
        {
            
            sheetNew.DisplayFormulas = sheetOld.DisplayFormulas;
            sheetNew.DisplayGridlines = sheetOld.DisplayGridlines;
            sheetNew.DisplayGuts = sheetOld.DisplayGuts;
            sheetNew.DisplayRowColHeadings = sheetOld.DisplayRowColHeadings;
            sheetNew.DisplayZeros = sheetOld.DisplayZeros;
            sheetNew.FitToPage = sheetOld.FitToPage;
            //  
            // TODO::sheetNew.setForceFormulaRecalculation(sheetOld.getForceFormulaRecalculation());  
            sheetNew.HorizontallyCenter = sheetOld.HorizontallyCenter;
            sheetNew.SetMargin(MarginType.BottomMargin, sheetOld.GetMargin(MarginType.BottomMargin));
            sheetNew.SetMargin(MarginType.FooterMargin, sheetOld.GetMargin(MarginType.FooterMargin));
            sheetNew.SetMargin(MarginType.HeaderMargin, sheetOld.GetMargin(MarginType.HeaderMargin));
            sheetNew.SetMargin(MarginType.LeftMargin, sheetOld.GetMargin(MarginType.LeftMargin));
            sheetNew.SetMargin(MarginType.RightMargin, sheetOld.GetMargin(MarginType.RightMargin));
            sheetNew.SetMargin(MarginType.TopMargin, sheetOld.GetMargin(MarginType.TopMargin));
            sheetNew.IsPrintGridlines = sheetOld.IsPrintGridlines;
            sheetNew.IsRightToLeft = sheetOld.IsRightToLeft;
            sheetNew.RowSumsBelow = sheetOld.RowSumsBelow;
            sheetNew.RowSumsRight = sheetOld.RowSumsRight;
            sheetNew.VerticallyCenter = sheetOld.VerticallyCenter;
            IRow rowNew;
            foreach (IRow row in sheetOld)
            {
                rowNew = sheetNew.CreateRow(row.RowNum);
                if (rowNew != null)
                    this.transform(workbookOld, workbookNew, row, rowNew);
            }

            for (int i = 0; i < this.lastColumn; i++)
            {
                //sheetOld.co
                //sheetNew.SetColumnWidth(i, sheetOld.GetColumnWidth(i));
                sheetNew.SetColumnHidden(i, sheetOld.IsColumnHidden(i));
            }

            for (int i = 0; i < sheetOld.NumMergedRegions; i++)
            {
                CellRangeAddress merged = sheetOld.GetMergedRegion(i);
                sheetNew.AddMergedRegion(merged);
            }
        }

        private void transform(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew,
                IRow rowOld, IRow rowNew)
        {
            ICell cellNew;
            rowNew.Height = rowOld.Height;

            foreach (ICell cell in rowOld)
            {
                cellNew = rowNew.CreateCell(cell.ColumnIndex, cell.CellType);
                if (cellNew != null)
                    this.transform(workbookOld, workbookNew, cell, cellNew);
            }
            this.lastColumn = Math.Max(this.lastColumn, rowOld.LastCellNum);
        }

        private void transform(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew,
                ICell cellOld, ICell cellNew)
        {
            cellNew.CellComment = cellOld.CellComment;
            int hash = cellOld.CellStyle.GetHashCode();

            if (this.styleDic != null && !this.styleDic.ContainsKey(hash))
            {
                this.transform(workbookOld, workbookNew, hash, cellOld.CellStyle,
                        (HSSFCellStyle)workbookNew.CreateCellStyle());
            }
            cellNew.CellStyle = this.styleDic[hash];
           

            switch (cellOld.CellType)
            {
                case CellType.Blank:
                    break;
                case CellType.Boolean:
                    cellNew.SetCellValue(cellOld.BooleanCellValue);
                    break;
                case CellType.Error:
                    cellNew.SetCellValue(cellOld.ErrorCellValue);
                    break;
                case CellType.Formula:
                    cellNew.SetCellFormula(cellOld.CellFormula);
                    break;
                case CellType.Numeric:
                    cellNew.SetCellValue(cellOld.NumericCellValue);
                    break;
                case CellType.String:
                    cellNew.SetCellValue(cellOld.StringCellValue);
                    break;
                default:
                    break;
            }
        }

        private void transform(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew,
                int hash, ICellStyle styleOld, ICellStyle styleNew)
        {
            styleNew.Alignment = styleOld.Alignment;
            styleNew.BorderBottom = styleOld.BorderBottom;
            styleNew.BorderLeft = styleOld.BorderLeft;
            styleNew.BorderRight = styleOld.BorderRight;
            styleNew.BorderTop = styleOld.BorderTop;
            styleNew.DataFormat = styleOld.DataFormat;

            styleNew.FillBackgroundColor = styleOld.FillBackgroundColor;
            styleNew.FillForegroundColor = styleOld.FillForegroundColor;
            styleNew.LeftBorderColor = styleOld.LeftBorderColor;
            styleNew.RightBorderColor = styleOld.RightBorderColor;
            styleNew.TopBorderColor = styleOld.TopBorderColor;
            styleNew.BorderDiagonalColor = styleOld.BorderDiagonalColor;
            styleNew.BottomBorderColor = styleOld.BottomBorderColor;
            //styleNew.FontIndex = styleOld.FontIndex;
            //styleNew.FillForegroundColorColor = styleOld.FillBackgroundColorColor;
            //styleNew.FillForegroundColorColor = styleOld.FillForegroundColorColor;
            styleNew.FillPattern = styleOld.FillPattern;
            //styleNew.FillBackgroundColorColor.RGBK
            styleNew.ShrinkToFit = styleOld.ShrinkToFit;
            styleNew.BorderDiagonalLineStyle = styleOld.BorderDiagonalLineStyle;
            
            
            styleNew.SetFont(this.transform(workbookNew, styleOld.GetFont(workbookOld)));
            styleNew.IsHidden = styleOld.IsHidden;
            styleNew.Indention = styleOld.Indention;//.setIndention(styleOld.getIndention());
            styleNew.IsLocked = styleOld.IsLocked;//.setLocked(styleOld.getLocked());
            styleNew.VerticalAlignment = styleOld.VerticalAlignment;//.setVerticalAlignment(styleOld.getVerticalAlignment());
            styleNew.WrapText = styleOld.WrapText;//.setWrapText(styleOld.getWrapText());
            this.styleDic.Add(hash, styleNew);
        }

        //private short transform(XSSFWorkbook workbookOld, HSSFWorkbook workbookNew,
        //        short index)
        //{
        //    DataFormat formatOld = workbookOld.createDataFormat();
        //    DataFormat formatNew = workbookNew.createDataFormat();
        //    return formatNew.getFormat(formatOld.getFormat(index));
        //}

        private IFont transform(HSSFWorkbook workbookNew, IFont fontOld)
        {
            IFont fontNew = workbookNew.CreateFont();
            fontNew.Boldweight = fontOld.Boldweight;//.setBoldweight(fontOld.getBoldweight());
            fontNew.Charset = fontOld.Charset;
                          
            fontNew.Color = fontOld.Color;//.setColor(fontOld.getColor());
            fontNew.FontName = fontOld.FontName;//.setFontName(fontOld.getFontName());
            fontNew.FontHeight = fontOld.FontHeight;//.setFontHeight(fontOld.getFontHeight());
            fontNew.IsItalic = fontOld.IsItalic;//.setItalic(fontOld.getItalic());
            fontNew.IsStrikeout = fontOld.IsStrikeout;//.setStrikeout(fontOld.getStrikeout());
            fontNew.TypeOffset = fontOld.TypeOffset;//.setTypeOffset(fontOld.getTypeOffset());
            fontNew.Underline = fontOld.Underline;//.setUnderline(fontOld.getUnderline());
           
            return fontNew;
        }
    }
}
