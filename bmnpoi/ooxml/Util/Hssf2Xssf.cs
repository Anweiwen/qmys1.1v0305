﻿/* ====================================================================
   zd：Hssf2Xssf对象为转换xls=>xlsx格式。
==================================================================== */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System.IO;

namespace NPOI.Util
{
    public class Hssf2Xssf
    {
        private int lastColumn = 0;
        private Dictionary<int, ICellStyle> styleDic = new Dictionary<int, ICellStyle>();

        public  void transformXSSF(HSSFWorkbook workbookOld, XSSFWorkbook workbookNew)
        {
            ISheet sheetNew, sheetOld;
            for (int i = 0; i < workbookOld.NumberOfSheets; i++)
            {
                sheetOld = workbookOld.GetSheetAt(i);
                Console.Out.WriteLine("正在转换第{0}个【"+sheetOld.SheetName+"】...",i+1);
                sheetNew = workbookNew.CreateSheet(sheetOld.SheetName);
                this.transform(workbookOld, workbookNew, sheetOld, sheetNew);
            }
        }


        public void transformDir(String srcDir, String destDir)
        {
            DirectoryInfo di = new DirectoryInfo(srcDir);
            FileInfo[] fls = di.GetFiles("*.xls");
            foreach (FileInfo fi in fls)
            {
                Console.Out.WriteLine("转换文件：" + fi.Name + "...");
                //zd:xls=>xlsx功能：
                using (var streamRead = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read))
                using (var streamWrite = new FileStream(destDir+fi.Name + "x", FileMode.Create, FileAccess.Write))
                {
                    try
                    {
                        var book = new HSSFWorkbook(streamRead);
                        var newBook = new XSSFWorkbook();
                        var convert = new Hssf2Xssf();
                        Console.Out.WriteLine(book.NumberOfSheets + "个Sheet页待转换，请稍等....");
                        convert.transformXSSF(book, newBook);
                        newBook.Write(streamWrite);
                        streamWrite.Close();
                        Console.Out.WriteLine(book.NumberOfSheets + "个Sheet页转换完成！");
                        Console.Out.WriteLine("文件：" + fi.Name + "转换完成！");
                    }
                    catch (NPOI.POIFS.FileSystem.OfficeXmlFileException oex)
                    {
                        if (oex.ParamName.Equals("XSSF"))
                        {
                            Console.Out.WriteLine("警告：该文件为Excel2007格式文件，扩展名不对！！！");
                            Console.Out.WriteLine("尝试转换中...");
                            var newbook = new XSSFWorkbook(new FileStream(fi.FullName, FileMode.Open, FileAccess.Read));
                            newbook.Write(streamWrite);
                            streamWrite.Close();
                        }
                        Console.Out.WriteLine("文件：" + fi.Name + "转换完成！");
                    }
                    catch (Exception ex)
                    {
                        Console.Out.WriteLine("文件：" + fi.Name + "转换失败：" + ex.Message);
                    }
                }

            }
            Console.Out.WriteLine("所有文件转换完毕！");
        }

        private void transform(HSSFWorkbook workbookOld, XSSFWorkbook workbookNew,
                ISheet sheetOld, ISheet sheetNew)
        {
            sheetNew.DisplayFormulas = sheetOld.DisplayFormulas;
            sheetNew.DisplayGridlines = sheetOld.DisplayGridlines;
            sheetNew.DisplayGuts = sheetOld.DisplayGuts;
            sheetNew.DisplayRowColHeadings = sheetOld.DisplayRowColHeadings;
            sheetNew.DisplayZeros = sheetOld.DisplayZeros;
            sheetNew.FitToPage = sheetOld.FitToPage;
            if (sheetOld.Protect) {
                sheetNew.ProtectSheet("boomlink");
            }
            if (sheetOld.IsExistsheetext)
            {
                sheetNew.TabColorIndex = sheetOld.TabColorIndex;
            }
            
            sheetNew.ForceFormulaRecalculation = sheetOld.ForceFormulaRecalculation;
            if (sheetOld.PaneInformation!=null&&sheetOld.PaneInformation.IsFreezePane())
            {
                sheetNew.CreateFreezePane(sheetOld.PaneInformation.VerticalSplitPosition,sheetOld.PaneInformation.HorizontalSplitPosition);
            }

            sheetNew.HorizontallyCenter = sheetOld.HorizontallyCenter;
            sheetNew.SetMargin(MarginType.BottomMargin, sheetOld.GetMargin(MarginType.BottomMargin));
            sheetNew.SetMargin(MarginType.FooterMargin, sheetOld.GetMargin(MarginType.FooterMargin));
            sheetNew.SetMargin(MarginType.HeaderMargin, sheetOld.GetMargin(MarginType.HeaderMargin));
            sheetNew.SetMargin(MarginType.LeftMargin, sheetOld.GetMargin(MarginType.LeftMargin));
            sheetNew.SetMargin(MarginType.RightMargin, sheetOld.GetMargin(MarginType.RightMargin));
            sheetNew.SetMargin(MarginType.TopMargin, sheetOld.GetMargin(MarginType.TopMargin));
            sheetNew.IsPrintGridlines = sheetOld.IsPrintGridlines;
            sheetNew.IsRightToLeft = sheetOld.IsRightToLeft;
            sheetNew.RowSumsBelow = sheetOld.RowSumsBelow;
            sheetNew.RowSumsRight = sheetOld.RowSumsRight;
            sheetNew.VerticallyCenter = sheetOld.VerticallyCenter;
            IRow rowNew;
            foreach (IRow row in sheetOld)
            {
                rowNew = sheetNew.CreateRow(row.RowNum);
                if (rowNew != null)
                    this.transform(workbookOld, workbookNew, row, rowNew);
            }

            for (int i = 0; i < this.lastColumn; i++)
            {
                sheetNew.SetColumnWidth(i, sheetOld.GetColumnWidth(i));
                sheetNew.SetColumnHidden(i, sheetOld.IsColumnHidden(i));
            }

            for (int i = 0; i < sheetOld.NumMergedRegions; i++)
            {
                CellRangeAddress merged = sheetOld.GetMergedRegion(i);
                sheetNew.AddMergedRegion(merged);
            }
        }

        private void transform(HSSFWorkbook workbookOld, XSSFWorkbook workbookNew,
                IRow rowOld, IRow rowNew)
        {
            ICell cellNew;
            rowNew.Height = rowOld.Height;
            rowNew.Hidden = rowOld.Hidden;
           
            foreach (ICell cell in rowOld)
            {
                cellNew = rowNew.CreateCell(cell.ColumnIndex, cell.CellType);
                if (cellNew != null)
                    this.transform(workbookOld, workbookNew, cell, cellNew);
            }
            this.lastColumn = Math.Max(this.lastColumn, rowOld.LastCellNum);
        }

        private void transform(HSSFWorkbook workbookOld, XSSFWorkbook workbookNew,
                ICell cellOld, ICell cellNew)
        {
            cellNew.CellComment = cellOld.CellComment;
            if (cellOld.Hyperlink != null)
            {
                cellNew.Hyperlink = new XSSFHyperlink(cellOld.Hyperlink.Type);
                cellNew.Hyperlink.Address = cellOld.Hyperlink.Address;
                cellNew.Hyperlink.FirstColumn = cellOld.Hyperlink.FirstColumn;
                cellNew.Hyperlink.FirstRow = cellOld.Hyperlink.FirstRow;
                cellNew.Hyperlink.Label = cellOld.Hyperlink.Label;
                cellNew.Hyperlink.LastColumn = cellOld.Hyperlink.LastColumn;
                cellNew.Hyperlink.LastRow = cellOld.Hyperlink.LastRow;
            }
            int hash = cellOld.CellStyle.GetHashCode();

            if (this.styleDic != null && !this.styleDic.ContainsKey(hash))
            {
                this.transform(workbookOld, workbookNew, hash, cellOld.CellStyle,
                        (XSSFCellStyle)workbookNew.CreateCellStyle());
            }
            cellNew.CellStyle = this.styleDic[hash];
           
            switch (cellOld.CellType)
            {
                case CellType.Blank:
                    break;
                case CellType.Boolean:
                    cellNew.SetCellValue(cellOld.BooleanCellValue);
                    break;
                case CellType.Error:
                    cellNew.SetCellValue(cellOld.ErrorCellValue);
                    break;
                case CellType.Formula:
                    cellNew.SetCellFormula(cellOld.CellFormula);
                    break;
                case CellType.Numeric:
                    cellNew.SetCellValue(cellOld.NumericCellValue);
                    break;
                case CellType.String:
                    cellNew.SetCellValue(cellOld.StringCellValue);
                    break;
                default:
                    break;
            }
        }

        private void transform(HSSFWorkbook workbookOld, XSSFWorkbook workbookNew,
                int hash, ICellStyle styleOld, XSSFCellStyle styleNew)
        {
            styleNew.Alignment = styleOld.Alignment;
            styleNew.BorderBottom = styleOld.BorderBottom;
            styleNew.BorderLeft = styleOld.BorderLeft;
            styleNew.BorderRight = styleOld.BorderRight;
            styleNew.BorderTop = styleOld.BorderTop;
            styleNew.DataFormat = styleOld.DataFormat;
            //styleNew.FillBackgroundColor = styleOld.FillBackgroundColor;
            styleNew.FillForegroundColor = styleOld.FillForegroundColor;
            styleNew.LeftBorderColor = styleOld.LeftBorderColor;
            styleNew.RightBorderColor = styleOld.RightBorderColor;

            styleNew.TopBorderColor = styleOld.TopBorderColor;
            styleNew.BorderDiagonalColor = styleOld.BorderDiagonalColor;
            styleNew.BottomBorderColor = styleOld.BottomBorderColor;
            styleNew.FillPattern = styleOld.FillPattern;
            styleNew.ShrinkToFit = styleOld.ShrinkToFit;
            styleNew.BorderDiagonalLineStyle = styleOld.BorderDiagonalLineStyle;

            styleNew.SetFont(this.transform(workbookNew, styleOld.GetFont(workbookOld)));
            styleNew.IsHidden = styleOld.IsHidden;
            styleNew.IsLocked = styleOld.IsLocked;
            styleNew.Indention = styleOld.Indention;
            styleNew.IsLocked = styleOld.IsLocked;
            styleNew.VerticalAlignment = styleOld.VerticalAlignment;
            styleNew.WrapText = styleOld.WrapText;
            this.styleDic.Add(hash, styleNew);
        }

        private IFont transform(XSSFWorkbook workbookNew, IFont fontOld)
        {
            IFont fontNew = workbookNew.CreateFont();
            fontNew.Boldweight = fontOld.Boldweight;
            fontNew.Charset = fontOld.Charset;
            fontNew.Color = fontOld.Color;
            fontNew.FontName = fontOld.FontName;
            fontNew.FontHeight = fontOld.FontHeight;
            fontNew.FontHeightInPoints = fontOld.FontHeightInPoints;
            fontNew.IsItalic = fontOld.IsItalic;
            fontNew.IsStrikeout = fontOld.IsStrikeout;
            fontNew.TypeOffset = fontOld.TypeOffset;
            fontNew.Underline = fontOld.Underline;
            return fontNew;
        }
    }
}
