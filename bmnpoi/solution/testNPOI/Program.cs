﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.Util;
using NPOI.HSSF;
using NPOI.XSSF;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace testNPOI
{
    class Program
    {
        static void Main(string[] args)
        {
            var convert = new Hssf2Xssf();
            convert.transformDir(@"../../../xls/", @"../../../xls/");
            //DirectoryInfo di = new DirectoryInfo(@"../../../xls/");
            //FileInfo[] fls = di.GetFiles("*.xls");
            //foreach (FileInfo fi in fls)
            //{
            //    Console.Out.WriteLine("转换文件：" + fi.Name + "...");
            //    //zd测试xls=>xlsx功能：
            //    using (var streamRead = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read))
            //    using (var streamWrite = new FileStream(fi.FullName + "x", FileMode.Create, FileAccess.Write))
            //    {
            //        try
            //        {
            //            var book = new HSSFWorkbook(streamRead);
            //            var newBook = new XSSFWorkbook();
            //            var convert = new Hssf2Xssf();
            //            Console.Out.WriteLine(book.NumberOfSheets + "个Sheet页待转换，请稍等....");
            //            convert.transformXSSF(book, newBook);
            //            //streamWrite.Seek(0, SeekOrigin.Begin);
            //            newBook.Write(streamWrite);
            //            streamWrite.Close();
            //            Console.Out.WriteLine(book.NumberOfSheets + "个Sheet页转换完成！");
            //            Console.Out.WriteLine("文件：" + fi.Name + "转换完成！");
            //        }
            //        catch (NPOI.POIFS.FileSystem.OfficeXmlFileException oex)
            //        {
            //            if (oex.ParamName.Equals("XSSF"))
            //            {
            //                var newbook = new XSSFWorkbook(new FileStream(fi.FullName, FileMode.Open, FileAccess.Read));
            //                newbook.Write(streamWrite);
            //                streamWrite.Close();
            //            }
            //            Console.Out.WriteLine("文件：" + fi.Name + "转换完成！");
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.Out.WriteLine("文件：" + fi.Name + "转换失败：" + ex.Message);
            //        }
            //    }
               
            //}
            //Console.Out.WriteLine("所有文件转换完毕！");
            Console.ReadKey();
        }
    }
}
