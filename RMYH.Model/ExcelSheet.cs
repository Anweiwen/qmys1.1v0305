﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RMYH.Model
{
    public class ExcelSheet
    {
        public string SHEETNAME { get; set; }

        public List<ExcelRow> Rows { get; set; } 
    }
}
