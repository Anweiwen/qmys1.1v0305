using System;
namespace RMYH.Model
{
	/// <summary>
	/// TB_ZDSXB:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class TB_ZDSXBModel
	{
		public TB_ZDSXBModel()
		{}
		#region Model
		private string _xmdm;
		private string _mkdm;
		private string _tablename;
		private string _fieldname;
		private string _zdzwm;
		private int? _sfxs;
		private int? _sjlx;
		private int? _jygs;
		private int? _srfs;
		private int? _srkj;
		private string _sql;
		private int? _len;
		private int? _isonly;
		private int? _disporder;
		private int? _nulls;
		private string _xszd;
		private string _bczd;
		private int? _sfbc;
		private string _defaultvalue;
		private int? _primarykey;
		private int? _columnwidth;
		private int? _sfhzld;
		private string _withto;
		private string _indexzh;
		private string _formatdisp;
		private int? _sfhsdw;
		/// <summary>
		/// 
		/// </summary>
		public string XMDM
		{
			set{ _xmdm=value;}
			get{return _xmdm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MKDM
		{
			set{ _mkdm=value;}
			get{return _mkdm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TABLENAME
		{
			set{ _tablename=value;}
			get{return _tablename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FIELDNAME
		{
			set{ _fieldname=value;}
			get{return _fieldname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ZDZWM
		{
			set{ _zdzwm=value;}
			get{return _zdzwm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SFXS
		{
			set{ _sfxs=value;}
			get{return _sfxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SJLX
		{
			set{ _sjlx=value;}
			get{return _sjlx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? JYGS
		{
			set{ _jygs=value;}
			get{return _jygs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SRFS
		{
			set{ _srfs=value;}
			get{return _srfs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SRKJ
		{
			set{ _srkj=value;}
			get{return _srkj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SQL
		{
			set{ _sql=value;}
			get{return _sql;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LEN
		{
			set{ _len=value;}
			get{return _len;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ISONLY
		{
			set{ _isonly=value;}
			get{return _isonly;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DISPORDER
		{
			set{ _disporder=value;}
			get{return _disporder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NULLS
		{
			set{ _nulls=value;}
			get{return _nulls;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string XSZD
		{
			set{ _xszd=value;}
			get{return _xszd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BCZD
		{
			set{ _bczd=value;}
			get{return _bczd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SFBC
		{
			set{ _sfbc=value;}
			get{return _sfbc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DEFAULTVALUE
		{
			set{ _defaultvalue=value;}
			get{return _defaultvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PRIMARYKEY
		{
			set{ _primarykey=value;}
			get{return _primarykey;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? COLUMNWIDTH
		{
			set{ _columnwidth=value;}
			get{return _columnwidth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SFHZLD
		{
			set{ _sfhzld=value;}
			get{return _sfhzld;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string WITHTO
		{
			set{ _withto=value;}
			get{return _withto;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string INDEXZH
		{
			set{ _indexzh=value;}
			get{return _indexzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FORMATDISP
		{
			set{ _formatdisp=value;}
			get{return _formatdisp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SFHSDW
		{
			set{ _sfhsdw=value;}
			get{return _sfhsdw;}
		}
		#endregion Model

	}
}

