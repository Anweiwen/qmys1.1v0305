using System;
namespace RMYH.Model
{
	/// <summary>
	/// TB_SYSMENU:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class TB_SYSMENUModel
	{
		public TB_SYSMENUModel()
		{}
		#region Model
		private int _id;
		private int? _parid;
		private string _pagepath;
		private string _title;
		private int? _sequence;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PARID
		{
			set{ _parid=value;}
			get{return _parid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PAGEPATH
		{
			set{ _pagepath=value;}
			get{return _pagepath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TITLE
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SEQUENCE
		{
			set{ _sequence=value;}
			get{return _sequence;}
		}
		#endregion Model

	}
}

