using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using Sybase.Data.AseClient;
using System.Configuration;
using System.Collections.Generic;
using DB;

namespace PHD.DB
{
    /// <summary> 
    /// 数据访问基础类(基于Oracle)
    /// 可以用户可以修改满足自己项目的需要。
    /// </summary>
    public class DbHelperOra
    {
        //数据库连接字符串(web.config来配置)，可以动态更改DBConection.ConnectionString支持多数据库.		
        public static string ConnectionString = "";

        #region 公用方法

        public static int GetMaxID(string FieldName, string TableName)
        {
            string strsql = "select max(" + FieldName + ")+1 from " + TableName;
            object obj = DbHelperOra.GetSingle(strsql);
            if (obj == null)
            {
                return 1;
            }
            else
            {
                return int.Parse(obj.ToString());
            }
        }
        public static bool Exists(string strSql)
        {
            object obj = DbHelperOra.GetSingle(strSql);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = int.Parse(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool Exists(string strSql, params  AseParameter[] cmdParms)
        {
            object obj = DbHelperOra.GetSingle(strSql, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = int.Parse(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region  执行简单SQL语句

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                using (AseCommand cmd = new AseCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static void ExecuteSqlTran(List<object> SQLStringList)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                connection.Open();
                AseCommand cmd = new AseCommand();
                cmd.Connection = connection;
                AseTransaction tx = connection.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    tx.Commit();
                }
                catch (Sybase.Data.AseClient.AseException E)
                {
                    tx.Rollback();
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// 执行带一个存储过程参数的的SQL语句。
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <param name="content">参数内容,比如一个字段是格式复杂的文章，有特殊符号，可以通过这个方式添加</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, string content)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                AseCommand cmd = new AseCommand(SQLString, connection);
                Sybase.Data.AseClient.AseParameter myParameter = new Sybase.Data.AseClient.AseParameter("@content",AseDbType.NVarChar);
                myParameter.Value = content;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (Sybase.Data.AseClient.AseException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// 向数据库里插入图像格式的字段(和上面情况类似的另一种实例)
        /// </summary>
        /// <param name="strSQL">SQL语句</param>
        /// <param name="fs">图像字节,数据库的字段类型为image的情况</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSqlInsertImg(string strSQL, byte[] fs)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                AseCommand cmd = new AseCommand(strSQL, connection);
                Sybase.Data.AseClient.AseParameter myParameter = new Sybase.Data.AseClient.AseParameter("@fs",AseDbType.Image);
                myParameter.Value = fs;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (Sybase.Data.AseClient.AseException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                using (AseCommand cmd = new AseCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException e)
                    {
                        connection.Close();
                        throw new Exception(e.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
            }
        }
        /// <summary>
        /// 执行查询语句，返回OracleDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>OracleDataReader</returns>
        public static AseDataReader ExecuteReader(string strSQL)
        {
            AseConnection connection = new AseConnection(DBConection.ConnectionString);
            AseCommand cmd = new AseCommand(strSQL, connection);
            try
            {
                connection.Open();
                AseDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (Sybase.Data.AseClient.AseException e)
            {
                throw new Exception(e.Message);
            }


        }
        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                DataSet ds = new DataSet();
                
                try
                {
                    connection.Open();
                    AseDataAdapter command = new AseDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }
        }


        #endregion

        #region 执行带参数的SQL语句

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, params AseParameter[] cmdParms)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                using (AseCommand cmd = new AseCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
            }
        }


        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">SQL语句的哈希表（key为sql语句，value是该语句的OracleParameter[]）</param>
        public static void ExecuteSqlTran(Hashtable SQLStringList)
        {
            using (AseConnection conn = new AseConnection(DBConection.ConnectionString))
            {
                conn.Open();
                using (AseTransaction trans = conn.BeginTransaction())
                {
                    AseCommand cmd = new AseCommand();
                    try
                    {
                        //循环
                        foreach (DictionaryEntry myDE in SQLStringList)
                        {
                            string cmdText = myDE.Key.ToString();
                            AseParameter[] cmdParms = (AseParameter[])myDE.Value;
                            PrepareCommand(cmd, conn, trans, cmdText, cmdParms);
                            int val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            trans.Commit();
                        }
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }


        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString, params AseParameter[] cmdParms)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                using (AseCommand cmd = new AseCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException e)
                    {
                        throw new Exception(e.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回OracleDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>OracleDataReader</returns>
        public static AseDataReader ExecuteReader(string SQLString, params AseParameter[] cmdParms)
        {
            AseConnection connection = new AseConnection(DBConection.ConnectionString);
            AseCommand cmd = new AseCommand();
            try
            {
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                AseDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                return myReader;
            }
            catch (Sybase.Data.AseClient.AseException e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString, params AseParameter[] cmdParms)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                AseCommand cmd = new AseCommand();
                
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                cmd.ExecuteReader();
                using (AseDataAdapter da = new AseDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        
                        cmd.Parameters.Clear();
                    }
                    catch (Sybase.Data.AseClient.AseException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                    return ds;
                }
            }
        }


        private static void PrepareCommand(AseCommand cmd, AseConnection conn, AseTransaction trans, string cmdText,AseParameter [] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (AseParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }


        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static int ExecuteSql(string[] SQLString, ArrayList arrcmdParms)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                using (AseCommand cmd = new AseCommand())
                {
                    int rtn = 0;
                    try
                    {                       
                        connection.Open();
                        cmd.Connection = connection;
                        AseTransaction tx = connection.BeginTransaction();
                        cmd.Transaction = tx;
                        try
                        {
                            if (arrcmdParms != null)
                            {
                                for (int n = 0; n < SQLString.Length; n++)
                                {
                                    foreach (AseParameter parm in (AseParameter[])arrcmdParms[n])
                                        cmd.Parameters.Add(parm);
                                    cmd.CommandText = SQLString[n];
                                    cmd.ExecuteNonQuery();
                                    cmd.Parameters.Clear();

                                }
                            }
                            else
                            {
                                for (int n = 0; n < SQLString.Length; n++)
                                {
                                    cmd.CommandText = SQLString[n];
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            tx.Commit();
                            rtn = 1;
                        }
                        catch (Sybase.Data.AseClient.AseException E)
                        {
                            tx.Rollback();
                            throw new Exception(E.Message);
                        }
                        finally
                        {
                            cmd.Dispose();
                            connection.Close();
                        }
                    }
                    catch(Exception e)
                    {
                        throw e;
                    }
                    return rtn;
                }
            }
        }
        #endregion

        #region 存储过程操作

        /// <summary>
        /// 执行存储过程 返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleDataReader</returns>
        public static AseDataReader RunProcedure(string storedProcName, IDataParameter[] parameters)
        {
            AseConnection connection = new AseConnection(DBConection.ConnectionString);
            AseDataReader returnReader;
            connection.Open();
            AseCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.CommandType = CommandType.StoredProcedure;
            returnReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            return returnReader;
        }


        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="tableName">DataSet结果中的表名</param>
        /// <returns>DataSet</returns>
        public static DataSet RunProcedure(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                AseDataAdapter sqlDA = new AseDataAdapter();
                sqlDA.SelectCommand = BuildQueryCommand(connection, storedProcName, parameters);
                sqlDA.Fill(dataSet, tableName);
                connection.Close();
                return dataSet;
            }
        }


        /// <summary>
        /// 构建 OracleCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleCommand</returns>
        private static AseCommand BuildQueryCommand(AseConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            AseCommand command = new AseCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (AseParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        /// <summary>
        /// 执行存储过程，返回影响的行数		
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="rowsAffected">影响的行数</param>
        /// <returns></returns>
        public static int RunProcedure(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                int result;
                connection.Open();
                AseCommand command = BuildIntCommand(connection, storedProcName, parameters);
                rowsAffected = command.ExecuteNonQuery();
                result = (int)command.Parameters["ReturnValue"].Value;
                //Connection.Close();
                return result;
            }
        }

        /// <summary>
        /// 创建 OracleCommand 对象实例(用来返回一个整数值)	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleCommand 对象实例</returns>
        private static AseCommand BuildIntCommand(AseConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            AseCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.Parameters.Add(new AseParameter("ReturnValue",
                AseDbType.Integer, 4, ParameterDirection.ReturnValue,
                false, 0, 0, string.Empty, DataRowVersion.Default, null));
            return command;
        }


            /// <summary>
        /// 执行存储过程，返回一个需要的参数
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="output">需要返回的存储过程参数</param>
        /// <returns></returns>
        public static string RunProcedure_V(string storedProcName, IDataParameter[] parameters, string output)
        {
            using (AseConnection connection = new AseConnection(DBConection.ConnectionString))
            {
                string result = "";
                try
                {
                    connection.Open();
                    AseCommand command = BuildQueryCommand(connection, storedProcName, parameters);
                    command.ExecuteNonQuery();
                    result = command.Parameters[output].Value.ToString();
                    //Connection.Close();
                }
                catch (Exception exp)
                {
                    string sss = exp.Message;
                }
                return result;
            }
        }

        #endregion

    }
}
