﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Globalization;
using Sybase.Data.AseClient;
using PHD.DB;
using DB;

/// <summary>
///REFLECT 的摘要说明
/// </summary>
public class REFLECT
{
    private static readonly Common.Logging.ILog logger = Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

	public REFLECT()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}


    /// <summary>
    /// 反射获取服务端数据
    /// </summary>
    /// <param name="funName">方法名</param>
    /// <param name="obj">WebService对象</param>
    /// <param name="param">参数字段</param>
    /// <returns></returns>
    public static object getData(string funName, object obj, Dictionary<string, object> param)
    {
        List<string> list = new List<string>(); object res = new object();
        foreach (var key in param.Keys)
        {
            list.Add(param[key].ToString().Trim());
        }
        res = obj.GetType().GetMethod(funName).Invoke(obj, list.ToArray());
        return res;
    }

    /// <summary>
    /// 按时间字段分组
    /// </summary>
    /// <param name="obj">查询返回的结果集</param>
    /// <param name="colName">时间字段列名称</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> getListByKey(object obj, string colName)
    {
        Dictionary<string, List<object>> map = new Dictionary<string, List<object>>();
        object[] arrObj = (object[])obj;
        Type type = obj.GetType().IsArray ? obj.GetType().GetElementType() : obj.GetType();  //获取类型
        PropertyInfo propertyInfo = type.GetProperty(colName); //获取指定名称的属性
        try
        {
            if (obj.GetType().IsArray)
            {
                for (int i = 0; arrObj != null && i < arrObj.Length; i++)
                {
                    string colTime = propertyInfo.GetValue(arrObj[i], null).ToString();
                    if (colTime != "")
                    {
                        string timeKey = ConvertDate(colTime);
                        if (map.ContainsKey(timeKey))
                        {
                            List<object> lists = (List<object>)map[timeKey];
                            lists.Add(arrObj[i]);
                        }
                        else
                        {
                            List<object> lists = new List<object>();
                            lists.Add(arrObj[i]);
                            map.Add(timeKey, lists);
                        }
                    }
                }
            }
            else
            {
                string timeKey = DateTime.Parse(propertyInfo.GetValue(obj, null).ToString()).ToString("yyyyMM");
                if (map.ContainsKey(timeKey))
                {
                    List<object> lists = (List<object>)map[timeKey];
                    lists.Add(obj);
                }
                else
                {
                    List<object> lists = new List<object>();
                    lists.Add(obj);
                    map.Add(timeKey, lists);
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return map;
    }

    /// <summary>
    /// 拼接插入sql
    /// </summary>
    /// <param name="obj">查询结果返回的对象</param>
    /// <param name="tableName">表名</param>
    /// <param name="timeCol">指定时间字段</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createInsertSql(Dictionary<string, List<object>> res, string timeCol)
    {
        string tableName = "";
        Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

        foreach (string key in res.Keys)
        {
            List<object> list = res[key];
            for (int i = 0; i < list.Count; i++)
            {
                //字段部分
                string cols = "";
                //值部分
                string vals = "";
                StringBuilder sqls = new StringBuilder();
                int len = list[i].GetType().GetProperties().Length;
                Type type = list[i].GetType();  //获取类型
                //获取要插入数据的表名称
                tableName = "TB_" + type.Name.ToUpper() + "_" + key;
                for (int m = 0; m < len; m++)
                {
                    string propertyName = type.GetProperties()[m].Name; //获取属性名称
                    cols += propertyName;
                    PropertyInfo propertyInfo = type.GetProperty(propertyName); //获取指定名称的属性
                    object colVals = propertyInfo.GetValue(list[i], null);
                    string value = colVals == null ? "" : colVals.ToString(); //获取属性值
                    value = timeCol.Equals(propertyName) ? ToDate(value) : value;//转换时间格式
                    logger.Info("属性名:" + propertyInfo);
                    logger.Info("属性值:" + value);
                    vals += "'" + value + "'";
                    if (m < len - 1)
                    {
                        cols += ",";
                        vals += ",";
                    }
                }

                sqls.Append("insert into ");
                sqls.Append(tableName);
                sqls.Append("(");
                sqls.Append(cols.ToUpper());
                sqls.Append(")");
                sqls.Append("values");
                sqls.Append("(");
                sqls.Append(vals);
                sqls.Append(")");

                list[i] = sqls.ToString();
            }
            dic.Add(tableName, list);
        }
        return dic;
    }


    /// <summary>
    /// 根据方法名,读取设置好的sql
    /// </summary>
    /// <param name="path"></param>
    public static Dictionary<string, object> LoadSql(string funname)
    {
        Dictionary<string, object> sqls = new Dictionary<string, object>();
        List<string> list = new List<string>();
        string sql = "select STATUS,SQL from TB_JC_SQL where FUNNAME like'%" + funname + "%'";
        try
        {
            DataSet da = DbHelperOra.Query(sql);
            int count = da.Tables[0].Rows.Count;
            for (int i = 0; count > 0 && i < count; i++)
            {
                sqls.Add(da.Tables[0].Rows[i]["STATUS"].ToString(), da.Tables[0].Rows[i]["SQL"].ToString());
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return sqls;
    }

    /// <summary>
    /// 装载create，delete,insert sql语句到字典中，以方便之后的带事物的数据库操作
    /// </summary>
    /// <param name="name">方法名</param>
    /// <param name="col">时间字段</param>
    /// <param name="obj">查询结果集</param>
    /// <param name="list">查询结果集sql语句集</param>
    /// <param name="ht">前台查询参数字典</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createTable(string name, string col, object obj, Dictionary<string, List<object>> list, Dictionary<string, object> ht)
    {
        Dictionary<string, List<object>> res = new Dictionary<string, List<object>>();
        string createSql = ""; string delSql = "";
        //1获取配置里的sql
        Dictionary<string, object> sqls = LoadSql(name);
        //2从结果集找出最小时间和最大时间
        //string[] times = MinMaxTime(obj, col);
        //3获取要插入数据的表名称
        object[] newObj = (object[])obj;
        try
        {
            string tableName = newObj.GetType().GetElementType().Name.ToUpper();

            //4遍历配置文件里的sql，找出对应的sql语句
            if (sqls.Count > 0)
            {
                createSql = sqls["CREATE"].ToString();
                delSql = sqls["DELETE"].ToString();
            }

            //5替换createTablesql语句中的表名，生成对应的sql语句
            foreach (string key in list.Keys)
            {
                List<object> newSqlList = new List<object>();
                string isExistSql = "select name from sysobjects where type='U' and name='" + key + "'";//查询自定义表
                DataSet da = DbHelperOra.Query(isExistSql);
                if (da.Tables[0].Rows.Count == 0)
                {
                    string newCreateSql = createSql.Replace("@" + tableName + "@", key);//替换create 的表名
                    newSqlList.Add(newCreateSql);
                }

                ////5替换delSql
                //if (delSql != "")
                //{
                //    string newDelSql = delSql.Replace("@" + tableName + "@", key);//替换del 的表名
                //    //替换除时间字段的其他字段
                //    foreach (string k in ht.Keys)
                //    {
                //        newDelSql = newDelSql.Replace(k, ht[k].ToString());
                //    }
                //    //替换时间最小值,最大值
                //    newDelSql = newDelSql.Replace("minTime", times[0]).Replace("maxTime", times[1]);

                //    newSqlList.Add(newDelSql);
                //}
                newSqlList.AddRange(list[key]);
                res.Add(key, newSqlList);
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }

        return res;
    }


    /// <summary>
    ///转换时间字段格式
    /// </summary>
    /// <param name="strDate"></param>
    /// <returns></returns>
    public static string ToDate(string strDate)
    {
        DateTime dtDate;
        if (DateTime.TryParse(strDate, out dtDate))
        {
            return dtDate.ToString("yyyy-MM-dd HH:mm:ss");
        }
        else
        {
            return strDate;
        }
    }

    /// <summary>
    /// 转化日期格式为年月
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static string ConvertDate(string date)
    {
        string time = "";
        try
        {
            return DateTime.Parse(date).ToString("yyyyMM");
        }
        catch (Exception e)
        {
            time = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).ToString();
            return DateTime.Parse(time).ToString("yyyyMM");
        }
    }


    /// <summary>
    /// 根据结果集找出最小时间和最大时间
    /// </summary>
    /// <returns></returns>
    public static string[] MinMaxTime(object obj, string col)
    {
        string[] time = new string[2];
        bool isArr = obj.GetType().IsArray;
        List<object> list = new List<object>();
        if (isArr)
        {
            object[] newObj = (object[])obj;
            //将返回的结果集中的空对象去除，再找出最小值和最大值
            for (int i = 0; i < newObj.Length; i++)
            {
                string date = obj.GetType().GetElementType().GetProperty(col).GetValue(newObj[i], null).ToString();
                if (date != "") { list.Add(newObj[i]); }
            }
            newObj = list.ToArray();
            //按时间字段从小到大排序
            newObj = newObj.OrderBy(b => col).ToArray();
            int len = newObj.Length;
            string minTime = obj.GetType().GetElementType().GetProperty(col).GetValue(newObj[0], null).ToString();  //获取结果集的最小时间
            string maxTime = obj.GetType().GetElementType().GetProperty(col).GetValue(newObj[len - 1], null).ToString();  //获取结果集的最大时间
            time[0] = ToDate(minTime);
            time[1] = ToDate(maxTime);
        }
        else
        {
            string mTime = obj.GetType().GetProperty(col).GetValue(obj, null).ToString();  //获取结果集的最小时间
            time[0] = ToDate(mTime);
            time[1] = ToDate(mTime);
        }
        return time;
    }
}