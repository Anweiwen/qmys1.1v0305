﻿using System;
using System.Data;
using System.Collections.Generic;
using Quartz;
using Quartz.Impl;
using PHD.DB;
using PHDS.WS_PHD;

namespace PHD
{
    /// <summary>
    /// 实现IJob接口
    /// </summary>
    public class Phd : IJob
    {
        //使用Common.Logging.dll日志接口实现日志记录
        private static readonly Common.Logging.ILog logger = Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region IJob 成员

        /// <summary>
        /// 执行PHD采集的定时任务，每小时取数一次
        /// </summary>
        /// <param name="context"></param>
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                string dt = DateTime.Now.ToString();
                logger.Info("PHDJob 任务开始运行,时间:" + dt);
                WS_PHD phd = new WS_PHD();
                List<string> DcsList = new List<string>();
                string dcsSql = "select DCSWH from OPC_WH";
                DataSet da=DbHelperOra.Query(dcsSql);
                int count = da.Tables[0].Rows.Count;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        DcsList.Add(da.Tables[0].Rows[0][i].ToString());
                    }
                    //调用时间段、多位号查询方法
                    object[] obj = phd.WS_MultiTagAndTime(DcsList.ToArray(), dt, dt);
                    //按时间字段，将查询结果分组
                    Dictionary<string, List<object>> result = REFLECT.getListByKey(obj, "Timestamp");
                    //将查询结果集拼接成insert语句
                    var insertSqls = REFLECT.createInsertSql(result, "");
                    //装载配置文件的创建表和删除表数据的sql语句，先执行create，再执行delete,最后执行insert
                    result = REFLECT.createTable(obj, insertSqls);
                    foreach (string key in result.Keys)
                    {
                        DbHelperOra.ExecuteSqlTran(result[key]);
                    }
                }
                //List<object> sqls=new List<object>();
                //string insetSql = "INSERT INTO dbo.OPC_DCS ( DCSWH, CTIME, VAL, ISSUC ) VALUES ( 'PRP1_04FI101.PV', '" + dt + "', 27998.27148438, 1 )";
                //sqls.Add(insetSql);
                //DbHelperOra.ExecuteSqlTran(sqls);
                logger.Info("PHDJob任务运行结束");
            }
            catch (Exception ex)
            {
                logger.Error("PHDJob 运行异常", ex);
            }

        }

        #endregion
    }
}
    

